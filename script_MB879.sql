UPDATE OR INSERT INTO INFORMES (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, FORMATO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD, DESTINO_FTP, CODREPORTE)
                        VALUES ('MB879_001', 'RELACION DE INVENTARIO CON EL STOCK EN REFERENCIAS', 'realiza una comparacion del saldo de inventario a fecha de corte y compara con el stock de cada referencia arroja cual es el que esta por debajo del stock', 'SELECT P_Referencia,
       Nombre_Referencia,
       P_Bodega,
       Cod_Linea,
       Nombre_Linea,
       Nombre_Bodega,
       P_Ponderado,
       Stock,
       Diferencia,
       Inventario,
       Saldo,
       Valor
FROM Pz_Inventario_Stock(:Fecha_Corte)', 'N', 'GESTION', NULL, 'N', 'PERSONALIZADOS', 'N', 'LIBRE', 5, 'N', 1, '', NULL)
                      MATCHING (CODIGO);


COMMIT WORK;