/* Proceso para blanquear saldos de inventario  */
UPDATE OR INSERT INTO PROCESOS (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, CODGRUPO, NIVEL, TABLERO, PRIORIDAD)
                        VALUES ('PER_LOGIST', 'BLANQUEAR SALDOS DE INVENTARIO EN RUTINA LOGISTICA PARA ANALIZAR DE NUEVO', 'Permite blanquear los saldos en logistica para volver a realizar el analisis segun movimientos con corte al dia de ayer
cerrar previamente dicha ventana', 'DELETE FROM RESUMEN_INVENTARIO;', 'N', 'GESTION', 'RUTINAS PERSONALIZADAS', 5, 'N', 1)
                      MATCHING (CODIGO);


COMMIT WORK;


/* Procedimientos para actualizar listas de precios */
/******************************************************************************/
/***                           Stored procedures                            ***/
/******************************************************************************/



SET TERM ^ ;

CREATE OR ALTER PROCEDURE PZ_DISTRITODO_LISTA_MOV (
    TIPO_ CHAR(5),
    PREFIJO_ CHAR(5),
    NUMERO_ CHAR(10))
AS
BEGIN
  EXIT;
END^





CREATE OR ALTER PROCEDURE PZ_DISTRITODO_LISTA_PRECIOS (
    FECHA_DESDE_ DATE,
    FECHA_HASTA_ DATE)
AS
BEGIN
  EXIT;
END^





CREATE OR ALTER PROCEDURE PZ_DISTRITODO_LISTA_PRECIOS_TER (
    FECHA_DESDE_ DATE,
    FECHA_HASTA_ DATE)
AS
BEGIN
  EXIT;
END^





CREATE OR ALTER PROCEDURE PZ_DISTRITODO_LISTA_REVERSAR (
    TIPO_ CHAR(5),
    PREFIJO_ CHAR(5),
    NUMERO_ CHAR(10))
AS
BEGIN
  EXIT;
END^






SET TERM ; ^



/******************************************************************************/
/***                           Stored procedures                            ***/
/******************************************************************************/



SET TERM ^ ;

CREATE OR ALTER PROCEDURE PZ_DISTRITODO_LISTA_MOV (
    TIPO_ CHAR(5),
    PREFIJO_ CHAR(5),
    NUMERO_ CHAR(10))
AS
DECLARE VARIABLE V_Tercero    CHAR(15);
DECLARE VARIABLE V_Lista      CHAR(5);
DECLARE VARIABLE V_Referencia CHAR(20);
DECLARE VARIABLE V_Renglon    INTEGER;
DECLARE VARIABLE V_Bruto      DOUBLE PRECISION;
DECLARE VARIABLE V_Precio     DOUBLE PRECISION;
BEGIN

 SELECT TRIM(Codtercero)
 FROM Comprobantes
 WHERE (TRIM(Tipo) = :Tipo_)
       AND (TRIM(Prefijo) = :Prefijo_)
       AND (TRIM(Numero) = :Numero_)
 INTO V_Tercero;

 SELECT TRIM(Codlista)
 FROM Terceros
 WHERE (TRIM(Codigo) = :V_Tercero)
 INTO V_Lista;

 FOR SELECT TRIM(Codreferencia),
            (Bruto - Descuento)
     FROM Tr_Inventario
     WHERE (TRIM(Tipo) = :Tipo_)
           AND (TRIM(Prefijo) = :Prefijo_)
           AND (TRIM(Numero) = :Numero_)
     INTO V_Referencia,
          V_Bruto
 DO
 BEGIN

  SELECT COALESCE(Precio,0)
  FROM Precios
  WHERE (TRIM(Codlista) = :V_Lista)
        AND (TRIM(Codreferencia) = :V_Referencia)
  INTO V_Precio;

  IF (V_Bruto <> V_Precio) THEN
   UPDATE Precios
   SET Precio = :V_Bruto
   WHERE (TRIM(Codlista) = :V_Lista)
         AND (TRIM(Codreferencia) = :V_Referencia);
 END

END^


CREATE OR ALTER PROCEDURE PZ_DISTRITODO_LISTA_PRECIOS (
    FECHA_DESDE_ DATE,
    FECHA_HASTA_ DATE)
AS
DECLARE VARIABLE V_Ubicacion             VARCHAR(20);
DECLARE VARIABLE V_Ubicacion_Nueva       VARCHAR(20);
DECLARE VARIABLE V_Fecha_Ultima_Compra   DATE;
DECLARE VARIABLE V_Tipo_Ultima_Compra    VARCHAR(5);
DECLARE VARIABLE V_Prefijo_Ultima_Compra VARCHAR(5);
DECLARE VARIABLE V_Numero_Ultima_Compra  VARCHAR(10);
DECLARE VARIABLE V_Bruto_Ultima_Compra   DOUBLE PRECISION;
DECLARE VARIABLE V_Fecha_Anterior        DATE;
DECLARE VARIABLE V_Tipo_Anterior         VARCHAR(5);
DECLARE VARIABLE V_Prefijo_Anterior      VARCHAR(5);
DECLARE VARIABLE V_Numero_Anterior       VARCHAR(10);
DECLARE VARIABLE V_Bruto_Anterior        DOUBLE PRECISION;
DECLARE VARIABLE V_Referencia            VARCHAR(20);
DECLARE VARIABLE V_Dif                   DOUBLE PRECISION;
DECLARE VARIABLE V_Secuencia             INTEGER;
BEGIN
 /* Este procedimiento se ejecutaba por aparte, son rutinas y parametros diferentes.
 Pero para evitar errores en orden de ejecución, se consolidan.
 Los parametros de fechas solo aplican para este. */
 EXECUTE PROCEDURE Pz_Distritodo_Lista_Precios_Ter(:Fecha_Desde_,:Fecha_Hasta_);

 /* Referencias con mvto */
 FOR SELECT DISTINCT TRIM(Inv.Codreferencia),
                     TRIM(Ref.Ubicacion)
     FROM Tr_Inventario Inv
     JOIN Referencias Ref ON (Ref.Codigo = Inv.Codreferencia)
     WHERE (Ref.Valoriza = 'S')
     ORDER BY 1
     INTO V_Referencia,
          V_Ubicacion
 DO
 BEGIN
  V_Dif = NULL;
  V_Ubicacion_Nueva = NULL;
  V_Fecha_Ultima_Compra = NULL;
  V_Tipo_Ultima_Compra = NULL;
  V_Prefijo_Ultima_Compra = NULL;
  V_Numero_Ultima_Compra = NULL;
  V_Bruto_Ultima_Compra = NULL;
  V_Fecha_Anterior = NULL;
  V_Tipo_Anterior = NULL;
  V_Prefijo_Anterior = NULL;
  V_Numero_Anterior = NULL;
  V_Bruto_Anterior = NULL;

  /* Ubicamos la última compra */
  SELECT FIRST 1 Fecha,
                 TRIM(Tipo),
                 TRIM(Prefijo),
                 TRIM(Numero),
                 Bruto,
                 Secuencia
  FROM Kardex_Ponderado_Simple(:V_Referencia)
  WHERE Cambia = 'S'
        AND Bruto > 0.0001
  ORDER BY Secuencia DESC
  INTO V_Fecha_Ultima_Compra,
       V_Tipo_Ultima_Compra,
       V_Prefijo_Ultima_Compra,
       V_Numero_Ultima_Compra,
       V_Bruto_Ultima_Compra,
       V_Secuencia;

  /* Ubicamos el ajuste o compra inmediatamente anterior a la ultima compra */
  IF (TRIM(COALESCE(V_Tipo_Ultima_Compra,'')) <> '') THEN
  BEGIN

   SELECT FIRST 1 Fecha,
                  Tipo,
                  Prefijo,
                  Numero,
                  Bruto
   FROM Kardex_Ponderado_Simple(:V_Referencia)
   WHERE Cambia = 'S'
         AND Secuencia <> :V_Secuencia
         AND Bruto > 0.0001
   ORDER BY Secuencia DESC
   INTO V_Fecha_Anterior,
        V_Tipo_Anterior,
        V_Prefijo_Anterior,
        V_Numero_Anterior,
        V_Bruto_Anterior;

   /* Aseguramos que las compras o entradas anteriores tengan datos, si no se asigna los de ultima compra */
   IF (TRIM(COALESCE(V_Tipo_Anterior,'')) = '') THEN
   BEGIN
    V_Fecha_Anterior = V_Fecha_Ultima_Compra;
    V_Tipo_Anterior = V_Tipo_Ultima_Compra;
    V_Prefijo_Anterior = V_Prefijo_Ultima_Compra;
    V_Numero_Anterior = V_Numero_Ultima_Compra;
    V_Bruto_Anterior = V_Bruto_Ultima_Compra;
   END

   /* Ejecutamos Cambios si y solo si la diferencia entre el ultimo valor de compra y el ultimo ajuste o compra es positivo */
   V_Ubicacion_Nueva = TRIM(TRIM(V_Tipo_Ultima_Compra) || TRIM(V_Prefijo_Ultima_Compra) || TRIM(V_Numero_Ultima_Compra));

   IF (COALESCE(V_Ubicacion,'') <> TRIM(V_Ubicacion_Nueva)) THEN
   BEGIN

    /* Marcamos la referencia como ya procesada */
    UPDATE Referencias
    SET Ubicacion = :V_Ubicacion_Nueva
    WHERE TRIM(Codigo) = :V_Referencia;

    /* Calculo anterior:
    Cambiamos valor en lista de precios si la diferencia es positiva
    V_Dif = V_Bruto_Ultima_Compra - V_Bruto_Anterior;

    IF (V_Dif > 0) THEN
    BEGIN
     UPDATE Precios
     SET Precio = Precio + ROUND(:V_Dif)
     WHERE Codlista LIKE 'T%'
           AND TRIM(Codreferencia) = TRIM(:V_Referencia);
    */

    /* Calculo nuevo(05-01-2023):
    Formula:
    precio ultima compra / precio compra anterior = porcentaje de alza
    precio de venta * porcentaje de alza = precio de venta actualizado.
    Cambiamos valor en lista de precios si la diferencia es positiva.
    */
    V_Dif = V_Bruto_Ultima_Compra / V_Bruto_Anterior;

    IF (V_Dif > 1) THEN
    BEGIN
     UPDATE Precios
     SET Precio = Precio * :V_Dif
     WHERE Codlista LIKE 'T%'
           AND TRIM(Codreferencia) = TRIM(:V_Referencia);
    END
   END
  END
 END
END^



CREATE OR ALTER PROCEDURE PZ_DISTRITODO_LISTA_PRECIOS_TER (
    FECHA_DESDE_ DATE,
    FECHA_HASTA_ DATE)
AS
DECLARE VARIABLE V_Lista_Max     INTEGER;
DECLARE VARIABLE V_Tercero       VARCHAR(15);
DECLARE VARIABLE V_Lista_Tercero VARCHAR(5);
DECLARE VARIABLE V_Referencia    VARCHAR(20);
DECLARE VARIABLE V_Precio        DOUBLE PRECISION;
DECLARE VARIABLE V_Fecha         DATE;
DECLARE VARIABLE V_Registros     INTEGER;
BEGIN
 /* Hallamos el # max de listas de precios que empiezan por T */
 SELECT SUBSTRING(MAX(Codlista) FROM 2)
 FROM Terceros
 WHERE Codlista LIKE 'T%'
 INTO V_Lista_Max;
 V_Lista_Max = COALESCE(V_Lista_Max,0);

 /* Recorremos todos los terceros que tienen mvto */
 FOR SELECT DISTINCT C.Codtercero
     FROM Comprobantes C
     JOIN Documentos D ON (C.Tipo = D.Codigo)
     WHERE ((C.Fecha >= :Fecha_Desde_)
           AND (C.Fecha <= :Fecha_Hasta_))
           AND (D.Grupo = 'VENTA')
           AND (D.Inventario = 'SALIDA')
           AND (D.Signo = 'CREDITO')
           AND (C.Codtercero NOT IN ('001','002','222222222'))
     INTO V_Tercero
 DO
 BEGIN
  /* Lista de precios por tercero */
  SELECT Codlista
  FROM Terceros
  WHERE Codigo = :V_Tercero
  INTO V_Lista_Tercero;

  /* Si lista de precios esta nula o no empieza por T, se incrementa v_lista_max + 1
  y este se asigna a v_lista_tercero */
  IF (V_Lista_Tercero IS NULL) THEN
  BEGIN
   V_Lista_Max = V_Lista_Max + 1;
   V_Lista_Tercero = 'T' || LPAD(V_Lista_Max,4,0);
  END
  ELSE
  IF (LEFT(V_Lista_Tercero,1) <> 'T') THEN
  BEGIN
   V_Lista_Max = V_Lista_Max + 1;
   V_Lista_Tercero = 'T' || LPAD(V_Lista_Max,4,0);
  END

  /* Actualizar o insertar lista de precios y terceros con base a v_lista_tercero */
  UPDATE OR INSERT INTO Listas (Codigo,Nombre,Fecha,Calcula_Impuestos,Activa,Recrear,Sondear,Grupo)
  VALUES (:V_Lista_Tercero,:V_Tercero,CURRENT_DATE,'S','S','N','S','VENTA');

  UPDATE Terceros
  SET Codlista = :V_Lista_Tercero
  WHERE Codigo = :V_Tercero;

  /* Recorremos todas las referencias que tienen mvto por ese tercero */
  FOR SELECT DISTINCT M.Codreferencia
      FROM Comprobantes C
      JOIN Documentos D ON (C.Tipo = D.Codigo)
      JOIN Tr_Inventario M USING (Tipo,Prefijo,Numero)
      WHERE ((C.Fecha >= :Fecha_Desde_)
            AND (C.Fecha <= :Fecha_Hasta_))
            AND (D.Grupo = 'VENTA')
            AND (D.Inventario = 'SALIDA')
            AND (D.Signo = 'CREDITO')
            AND C.Codtercero = :V_Tercero
      INTO V_Referencia
  DO
  BEGIN
   /* Si la referencia no se encuentra en la lista de precios, se actualiza */
   SELECT COUNT(Codlista)
   FROM Precios
   WHERE (Codlista = :V_Lista_Tercero)
         AND (Codreferencia = :V_Referencia)
   INTO V_Registros;

   IF (V_Registros = 0) THEN
   BEGIN
    /* Se busca el último precio por referencia y tercero */
    SELECT FIRST 1 P.Bruto - P.Descuento,
                   C.Fecha
    FROM Tr_Inventario P
    JOIN Comprobantes C USING (Tipo,Prefijo,Numero)
    INNER JOIN Documentos D ON (P.Tipo = D.Codigo)
    WHERE P.Salida > 0
          AND (D.Grupo = 'VENTA')
          AND (D.Inventario = 'SALIDA')
          AND (D.Signo = 'CREDITO')
          AND C.Codtercero = :V_Tercero
          AND P.Codreferencia = :V_Referencia
    ORDER BY P.Renglon DESC
    INTO V_Precio,
         V_Fecha;

    INSERT INTO Precios (Codlista,Codreferencia,Precio)
    VALUES (:V_Lista_Tercero,:V_Referencia,:V_Precio);
   END
  END
 END
END^


CREATE OR ALTER PROCEDURE PZ_DISTRITODO_LISTA_REVERSAR (
    TIPO_ CHAR(5),
    PREFIJO_ CHAR(5),
    NUMERO_ CHAR(10))
AS
DECLARE VARIABLE V_Referencia            CHAR(20);
DECLARE VARIABLE V_Bruto                 DOUBLE PRECISION;
DECLARE VARIABLE V_Fecha_Ultima_Compra   DATE;
DECLARE VARIABLE V_Tipo_Ultima_Compra    VARCHAR(5);
DECLARE VARIABLE V_Prefijo_Ultima_Compra VARCHAR(5);
DECLARE VARIABLE V_Numero_Ultima_Compra  VARCHAR(10);
DECLARE VARIABLE V_Bruto_Ultima_Compra   DOUBLE PRECISION;
DECLARE VARIABLE V_Dif                   DOUBLE PRECISION;
BEGIN

 /* SP para reversar valor en lista de precios de manera manual.
 Cuando el proveedor erra el valor y se debe reversar al aumento real.
 SP para ejecutarse desde un formato. */

 /* Recorremos el doc */
 FOR SELECT TRIM(Codreferencia),
            Bruto
     FROM Tr_Inventario
     WHERE (TRIM(Tipo) = :Tipo_)
           AND (TRIM(Prefijo) = :Prefijo_)
           AND (TRIM(Numero) = :Numero_)
     INTO V_Referencia,
          V_Bruto
 DO
 BEGIN
  V_Fecha_Ultima_Compra = NULL;
  V_Tipo_Ultima_Compra = NULL;
  V_Prefijo_Ultima_Compra = NULL;
  V_Numero_Ultima_Compra = NULL;
  V_Bruto_Ultima_Compra = NULL;
  V_Dif = NULL;

  /* Ubicamos la última compra */
  SELECT FIRST 1 Fecha,
                 TRIM(Tipo),
                 TRIM(Prefijo),
                 TRIM(Numero),
                 Bruto
  FROM Kardex_Ponderado_Simple(:V_Referencia)
  WHERE Cambia = 'S'
        AND Bruto > 0.0001
  ORDER BY Secuencia DESC
  INTO V_Fecha_Ultima_Compra,
       V_Tipo_Ultima_Compra,
       V_Prefijo_Ultima_Compra,
       V_Numero_Ultima_Compra,
       V_Bruto_Ultima_Compra;

  /* Reversamos el valor */
  V_Dif = V_Bruto_Ultima_Compra - V_Bruto;

  UPDATE Precios
  SET Precio = Precio - IIF(ROUND(:V_Dif) < 0,ROUND(:V_Dif) * -1,ROUND(:V_Dif))
  WHERE Codlista LIKE 'T%'
        AND TRIM(Codreferencia) = TRIM(:V_Referencia);

 END
END^



SET TERM ; ^


COMMIT WORK;

/* Proceso para ejecutar dos de los procedimientos de actualizar listas de precios, los otros dos van en los formatos */
UPDATE OR INSERT INTO PROCESOS (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, CODGRUPO, NIVEL, TABLERO, PRIORIDAD)
                        VALUES ('MC807_001', 'INCREMENTA VALOR LISTAS DE PRECIOS X TERCERO', 'Incrementas las listas de precios basado en la factura de compra', 'EXECUTE PROCEDURE Pz_Distritodo_Lista_Precios(:Fecha_Desde,:Fecha_Hasta);', 'N', 'GESTION', 'PERSONALIZADOS', 5, 'S', 1)
                      MATCHING (CODIGO);


COMMIT WORK;

/* Informe de consumos con proveedor, realizado con base a informe estándar AUXCNM01A */
UPDATE OR INSERT INTO INFORMES (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, FORMATO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD, DESTINO_FTP, CODREPORTE)
                        VALUES ('MC807_001', 'CONSUMOS DE PRODUCTOS DETALLADO CON PROVEEDOR - RANGO DE FECHA', 'Muestra la relación de consumos de inventario, discriminando documento, fecha, salida, total venta, total consumo, porcentaje de utilidad por referencia y totales. Mas proveedor(codigo alterno ref.).', 'SELECT C.*,
       R.Codigo2 Proveedor
FROM Consumos_Rango(:_Desde,:_Hasta) C
JOIN Referencias R ON (R.Codigo = C.Codreferencia)', 'N', 'GESTION', NULL, 'N', 'PERSONALIZADOS', 'N', 'LIBRE', 5, 'N', 1, NULL, NULL)
                      ;


COMMIT WORK;

/*insertamos apis en interface rest*/

UPDATE OR INSERT INTO INTERFACE_REST (CLAVE, CONSULTA, METHOD_HTTP, DESCRIPCION, REGISTRO)
                              VALUES ('Get_Referencias', 'SELECT Codigo, Codigo2, Nombre, Nombre2, Ubicacion, Unidad_Compra, Stock, Tiempo, Tiempo_Maximo, Costo, Precio, Peso, Alto, Ancho, Fondo, Medida_Rastreo, Valoriza, Costea, Saldos, Alterna, Balanza, Ensamble, Es_Ensamble, Lote, Serial, Categoria, Pymo, Portafolio, Descripcion, Ean_128, Url_Foto, Bloquear_Esquema, Activa, Crm, Nota, Foto, Analisis_Costos, Interes, Grados, Volumen, Rentabilidad, Subpartida, Codlinea, Codmedida, Cod_Esqimpuesto, Cod_Esqretencion, Cod_Esqcontable, Cod_Esqcontable2, Codgenero, Codmarca FROM Rest_Select_Referencia(:Codigo)', 'POST', 'Obtiene informacion de la Referencia', 4962192749843726838)
                            MATCHING (CLAVE);
UPDATE OR INSERT INTO INTERFACE_REST (CLAVE, CONSULTA, METHOD_HTTP, DESCRIPCION, REGISTRO)
                              VALUES ('Get_Terceros', 'SELECT Codigo, Naturaleza, Codigo2, Dv, Empresa, Nom1, Nom2, Apl1, Apl2, Razon_Comercial, Codigo_Postal, Barrio, Direccion, Envio, Telefono, Movil, Email, Gerente, Website, Fecha, Habeas_Data, Codsociedad, Codidentidad, Codmunicipio, Codactividad, Codzona, Codlista, Codpersonal, Codpais, Codlista_Compra, Codtipologia, Nombre FROM Rest_Select_Terceros(:Codigo)', 'POST', 'Obtiene informacion del Tercero', 313851328775202758)
                            MATCHING (CLAVE);
UPDATE OR INSERT INTO INTERFACE_REST (CLAVE, CONSULTA, METHOD_HTTP, DESCRIPCION, REGISTRO)
                              VALUES ('Set_Gestion_Primario', 'EXECUTE PROCEDURE Rest_Insert_Gestion_Primario (:Tipo,:Prefijo,:Numero,:Fecha,:Vence,:Tercero,:Vendedor,:Lista,:Banco,:Usuario,:Centro,:Bodega,:Referencia,:Entrada,:Salida,:Unitario,:Porc_descuento,:Nota)', 'POST', 'Inserta un nuevo documento primario en Gestion', -1595035324359283302)
                            MATCHING (CLAVE);
UPDATE OR INSERT INTO INTERFACE_REST (CLAVE, CONSULTA, METHOD_HTTP, DESCRIPCION, REGISTRO)
                              VALUES ('Set_Gestion_Primario_B', 'EXECUTE PROCEDURE Rest_insert_Gestion_Primario_G(:Tipo,:Prefijo,:Numero,:Fecha,:Vence,:Tercero,:Vendedor,:Lista,:Banco,:Usuario,:Nota,:Detalle,:Bloqueado,:Enviado)', 'POST', 'Inserta un nuevo documento primario en Gestion', -59849748488584313)
                            MATCHING (CLAVE);
UPDATE OR INSERT INTO INTERFACE_REST (CLAVE, CONSULTA, METHOD_HTTP, DESCRIPCION, REGISTRO)
                              VALUES ('Set_Termina_Gestion_Primario', 'EXECUTE PROCEDURE Rest_Termina_Comprobante(:Tipo,:Prefijo,:Numero)', 'POST', 'Termina el comprobante', -1684516391492084998)
                            MATCHING (CLAVE);
UPDATE OR INSERT INTO INTERFACE_REST (CLAVE, CONSULTA, METHOD_HTTP, DESCRIPCION, REGISTRO)
                              VALUES ('Get_Precios_Tercero', 'SELECT Codigo_Referencia,Nombre_Referencia,Precio FROM Rest_Select_Precios_Tercero(:Codtercero, :Codreferencia)', 'POST', 'Obtiene Informacion de los precios de una o todas las referencias de un tercero especifico', -4538433240696143590)
                            MATCHING (CLAVE);
UPDATE OR INSERT INTO INTERFACE_REST (CLAVE, CONSULTA, METHOD_HTTP, DESCRIPCION, REGISTRO)
                              VALUES ('Set_Terceros', 'EXECUTE PROCEDURE Rest_Insert_Terceros(:Codigo, :Dv, :Naturaleza, :Nom1, :Nom2, :Apl1, :Apl2, :Empresa, :Razon_Comercial, :Direccion, :Telefono, :Movil, :Email, :Codigo_Postal, :Gerente, :Codidentidad, :Codsociedad, :Codpais, :Codactividad, :Codzona, :Codmunicipio)', 'POST', 'Inserta un nuevo Tercero', 6041199247570830422)
                            MATCHING (CLAVE);
UPDATE OR INSERT INTO INTERFACE_REST (CLAVE, CONSULTA, METHOD_HTTP, DESCRIPCION, REGISTRO)
                              VALUES ('Get_Gestion_Primario', 'SELECT Ctipo, Cprefijo, Cnumero, Fecha, Plazo, Vence, Nota, Tercero, Sucursal, Vendedor, Lista, Banco, Bruto, Descuentos, Impuestos, Retenciones, Total, Sede, Concepto, Trm FROM Rest_Select_Gestion_Primario(:Tipo, :Prefijo, :Numero)', 'POST', 'Obtiene informacion de un documento primario en Gestion', -581306840052394038)
                            MATCHING (CLAVE);
UPDATE OR INSERT INTO INTERFACE_REST (CLAVE, CONSULTA, METHOD_HTTP, DESCRIPCION, REGISTRO)
                              VALUES ('Set_Sucursales', 'EXECUTE PROCEDURE Rest_Insert_Sucursales(:Tercero_, :Sucursal_, :Nombre_, :Direccion_, Telefono_, :Movil_, :Fax_, :Email_, : Codmunicipio_, :Codzona)', 'POST', 'Inserta una nueva sucursal por tercero', -111733114996993674)
                            MATCHING (CLAVE);
UPDATE OR INSERT INTO INTERFACE_REST (CLAVE, CONSULTA, METHOD_HTTP, DESCRIPCION, REGISTRO)
                              VALUES ('Get_Sucursales', 'SELECT S_Codigo, S_Nombre, S_Direccion, S_Telefono, S_Movil, S_Email, Nombre_Municipio, Nombre_Zona FROM Rest_Select_Suursales_tercero(:Tercero, :Sucursal)', 'POST', 'Obtiene Informacion de una o todas las sucursales de un tercero especifico', -199368733061537706)
                            MATCHING (CLAVE);


COMMIT WORK;