/******************************************************************************/
/***                               IMPORTADOS 				    ***/
/******************************************************************************/

UPDATE OR INSERT INTO IMPORTADOS (ID, DESCRIPCION, ARCHIVO, TABLA, ESQUEMA, SQL, ORDEN, ACTIVO, MODIFICAR)
                          VALUES (1, 'ENCABEZADO DE FACTURAS', 'FV*.TXT', 'COMPROBANTES', '
Format=Delimited(|)
ColNameHeader=False
MaxScanRows=0
Col1=Tipo Char
Col2=Prefijo Char
Col3=Numero Char
Col4=NA_Fecha Char
Col5=NA_Vence Char
Col6=NA_Codtercero Char
Col7=NA_NotaComp Char
Col8=NA_Bloqueado Char
Col9=NA_Usuario Char
Col10=NA_Escenario Char
Col11=NA_Bodega Char
Col12=NA_Centro Char
Col13=NA_Referencia Char
Col14=NA_Banco Char
Col15=NA_Salida Char
Col16=NA_Unitario Char
Col17=NA_NotaMov Char
Col18=NA_MegaNota Char
Col19=NA_Dto Char
Col20=NA_Autorizacion Char
CharacterSet=ANSI', NULL, 1, 'S', 'S')
                        MATCHING (ID);
UPDATE OR INSERT INTO IMPORTADOS (ID, DESCRIPCION, ARCHIVO, TABLA, ESQUEMA, SQL, ORDEN, ACTIVO, MODIFICAR)
                          VALUES (2, 'DETALLE DE LAS FACTURAS', 'FV*.TXT', 'TR_INVENTARIO', '
Format=Delimited(|)
ColNameHeader=False
MaxScanRows=0
Col1=Tipo Char
Col2=Prefijo Char
Col3=Numero Char
Col4=NA_Fecha Char
Col5=NA_Vence Char
Col6=NA_Codtercero Char
Col7=NA_NotaComp Char
Col8=NA_Bloqueado Char
Col9=NA_Usuario Char
Col10=NA_Escenario Char
Col11=NA_Bodega Char
Col12=NA_Centro Char
Col13=NA_Referencia Char
Col14=NA_Banco Char
Col15=NA_Salida Char
Col16=NA_Unitario Char
Col17=NA_NotaMov Char
Col18=NA_MegaNota Char
Col19=NA_Dto Char
Col20=NA_Autorizacion Char
CharacterSet=ANSI', NULL, 2, 'S', 'N')
                        MATCHING (ID);
UPDATE OR INSERT INTO IMPORTADOS (ID, DESCRIPCION, ARCHIVO, TABLA, ESQUEMA, SQL, ORDEN, ACTIVO, MODIFICAR)
                          VALUES (4, 'ANEXOS DE LAS FACTURAS', 'FV*.TXT', 'ANEXO_GESTION', '
Format=Delimited(|)
ColNameHeader=False
MaxScanRows=0
Col1=Tipo Char
Col2=Prefijo Char
Col3=Numero Char
Col4=NA_Fecha Char
Col5=NA_Vence Char
Col6=NA_Codtercero Char
Col7=NA_NotaComp Char
Col8=NA_Bloqueado Char
Col9=NA_Usuario Char
Col10=NA_Escenario Char
Col11=NA_Bodega Char
Col12=NA_Centro Char
Col13=NA_Referencia Char
Col14=NA_Banco Char
Col15=NA_Salida Char
Col16=NA_Unitario Char
Col17=NA_NotaMov Char
Col18=NA_MegaNota Char
Col19=NA_Dto Char
Col20=NA_Autorizacion Char
CharacterSet=ANSI', NULL, 4, 'S', 'S')
                        MATCHING (ID);
UPDATE OR INSERT INTO IMPORTADOS (ID, DESCRIPCION, ARCHIVO, TABLA, ESQUEMA, SQL, ORDEN, ACTIVO, MODIFICAR)
                          VALUES (3, 'MEGA NOTA DE LAS FACTURAS', 'FV*.TXT', 'TR_NOTAS', '
Format=Delimited(|)
ColNameHeader=False
MaxScanRows=0
Col1=Tipo Char
Col2=Prefijo Char
Col3=Numero Char
Col4=NA_Fecha Char
Col5=NA_Vence Char
Col6=NA_Codtercero Char
Col7=NA_NotaComp Char
Col8=NA_Bloqueado Char
Col9=NA_Usuario Char
Col10=NA_Escenario Char
Col11=NA_Bodega Char
Col12=NA_Centro Char
Col13=NA_Referencia Char
Col14=NA_Banco Char
Col15=NA_Salida Char
Col16=NA_Unitario Char
Col17=NA_NotaMov Char
Col18=NA_MegaNota Char
Col19=NA_Dto Char
Col20=NA_Autorizacion Char
CharacterSet=ANSI', NULL, 3, 'S', 'S')
                        MATCHING (ID);


COMMIT WORK;

/******************************************************************************/
/***                               IMPORTADOS MODELO 			    ***/
/******************************************************************************/

UPDATE OR INSERT INTO IMPORTADOS_MODELO (ID, ID_IMPORTADO, CAMPO, ACTIVO, SQL_CONVERSION)
                                 VALUES (1, 1, 'NA_Fecha', 'S', 'SELECT SUBSTRING(:Valor FROM 4 FOR 2) || ''/'' || SUBSTRING(:Valor FROM 1 FOR 2) || ''/'' || SUBSTRING(:Valor FROM 7)  AS Fecha
FROM Rdb$Database')
                               MATCHING (ID);
UPDATE OR INSERT INTO IMPORTADOS_MODELO (ID, ID_IMPORTADO, CAMPO, ACTIVO, SQL_CONVERSION)
                                 VALUES (21, 2, 'NA_Dto', 'S', 'SELECT ''0'' AS Descuento
FROM Rdb$Database')
                               MATCHING (ID);
UPDATE OR INSERT INTO IMPORTADOS_MODELO (ID, ID_IMPORTADO, CAMPO, ACTIVO, SQL_CONVERSION)
                                 VALUES (2, 1, 'NA_Vence', 'S', 'SELECT SUBSTRING(:Valor FROM 4 FOR 2) || ''/'' || SUBSTRING(:Valor FROM 1 FOR 2) || ''/'' || SUBSTRING(:Valor FROM 7)  AS Vence
FROM Rdb$Database')
                               MATCHING (ID);
UPDATE OR INSERT INTO IMPORTADOS_MODELO (ID, ID_IMPORTADO, CAMPO, ACTIVO, SQL_CONVERSION)
                                 VALUES (22, 4, 'NA_Autorizacion', 'S', 'SELECT IIF(TRIM(:Valor)='''', ''NULL'', :Valor) AS Orden
FROM Rdb$Database')
                               MATCHING (ID);
UPDATE OR INSERT INTO IMPORTADOS_MODELO (ID, ID_IMPORTADO, CAMPO, ACTIVO, SQL_CONVERSION)
                                 VALUES (3, 1, 'NA_NotaComp', 'S', 'SELECT IIF(TRIM(:Valor) = '''', ''-'', LEFT(:Valor, 80)) AS Nota
FROM Rdb$Database')
                               MATCHING (ID);
UPDATE OR INSERT INTO IMPORTADOS_MODELO (ID, ID_IMPORTADO, CAMPO, ACTIVO, SQL_CONVERSION)
                                 VALUES (4, 1, 'NA_Bloqueado', 'S', 'SELECT IIF(TRIM(:Valor) = '''', ''R'', :Valor) AS Bloqueado
FROM Rdb$Database')
                               MATCHING (ID);
UPDATE OR INSERT INTO IMPORTADOS_MODELO (ID, ID_IMPORTADO, CAMPO, ACTIVO, SQL_CONVERSION)
                                 VALUES (5, 1, 'NA_Codtercero', 'S', 'SELECT
LEFT(:Valor, 15) AS CodTercero
FROM Rdb$Database')
                               MATCHING (ID);
UPDATE OR INSERT INTO IMPORTADOS_MODELO (ID, ID_IMPORTADO, CAMPO, ACTIVO, SQL_CONVERSION)
                                 VALUES (23, 1, 'NA_Dto', 'S', 'SELECT IIF(TRIM(:Valor) = '''', ''0'', :Valor) AS Copago
FROM Rdb$Database')
                               MATCHING (ID);
UPDATE OR INSERT INTO IMPORTADOS_MODELO (ID, ID_IMPORTADO, CAMPO, ACTIVO, SQL_CONVERSION)
                                 VALUES (6, 1, 'NA_Bloqueado', 'S', 'SELECT ''PV'' AS CodVendedor
FROM RDB$DATABASE')
                               MATCHING (ID);
UPDATE OR INSERT INTO IMPORTADOS_MODELO (ID, ID_IMPORTADO, CAMPO, ACTIVO, SQL_CONVERSION)
                                 VALUES (7, 1, 'NA_Bloqueado', 'S', 'SELECT ''3PRE'' AS CodLista
FROM RDB$DATABASE')
                               MATCHING (ID);
UPDATE OR INSERT INTO IMPORTADOS_MODELO (ID, ID_IMPORTADO, CAMPO, ACTIVO, SQL_CONVERSION)
                                 VALUES (8, 1, 'NA_Banco', 'S', 'SELECT IIF(TRIM(:Valor)=''NA'', ''NULL'', LEFT(:Valor, 15)) AS CodBanco
FROM Rdb$Database')
                               MATCHING (ID);
UPDATE OR INSERT INTO IMPORTADOS_MODELO (ID, ID_IMPORTADO, CAMPO, ACTIVO, SQL_CONVERSION)
                                 VALUES (9, 1, 'NA_Usuario', 'S', 'SELECT IIF(TRIM(:Valor)='''', ''SUPERVISOR'', LEFT(:Valor, 10)) AS CodUsuario
FROM Rdb$Database')
                               MATCHING (ID);
UPDATE OR INSERT INTO IMPORTADOS_MODELO (ID, ID_IMPORTADO, CAMPO, ACTIVO, SQL_CONVERSION)
                                 VALUES (10, 1, 'NA_Escenario', 'S', 'SELECT IIF(TRIM(:Valor)='''', ''NA'', LEFT(:Valor, 10)) AS CodESCENARIO
FROM Rdb$Database')
                               MATCHING (ID);
UPDATE OR INSERT INTO IMPORTADOS_MODELO (ID, ID_IMPORTADO, CAMPO, ACTIVO, SQL_CONVERSION)
                                 VALUES (11, 2, 'NA_Bloqueado', 'S', 'SELECT GEN_ID(Gen_Tr_Inventario, 1) AS Renglon
FROM Rdb$Database')
                               MATCHING (ID);
UPDATE OR INSERT INTO IMPORTADOS_MODELO (ID, ID_IMPORTADO, CAMPO, ACTIVO, SQL_CONVERSION)
                                 VALUES (12, 2, 'NA_Centro', 'S', 'SELECT ''01'' AS CodCentro
FROM Rdb$Database')
                               MATCHING (ID);
UPDATE OR INSERT INTO IMPORTADOS_MODELO (ID, ID_IMPORTADO, CAMPO, ACTIVO, SQL_CONVERSION)
                                 VALUES (13, 2, 'NA_Bodega', 'S', 'SELECT IIF(TRIM(:Valor)='''', ''BG'', LEFT(:Valor, 5)) AS CodBodega
FROM Rdb$Database')
                               MATCHING (ID);
UPDATE OR INSERT INTO IMPORTADOS_MODELO (ID, ID_IMPORTADO, CAMPO, ACTIVO, SQL_CONVERSION)
                                 VALUES (14, 2, 'NA_Referencia', 'S', 'SELECT IIF(TRIM(:Valor)='''', ''NA'', LEFT(:Valor, 20)) AS CodReferencia
FROM Rdb$Database')
                               MATCHING (ID);
UPDATE OR INSERT INTO IMPORTADOS_MODELO (ID, ID_IMPORTADO, CAMPO, ACTIVO, SQL_CONVERSION)
                                 VALUES (15, 2, 'NA_Salida', 'S', 'SELECT IIF(TRIM(:Valor) = '''', ''1'', :Valor) AS Salida
FROM Rdb$Database')
                               MATCHING (ID);
UPDATE OR INSERT INTO IMPORTADOS_MODELO (ID, ID_IMPORTADO, CAMPO, ACTIVO, SQL_CONVERSION)
                                 VALUES (16, 2, 'NA_Unitario', 'S', 'SELECT IIF(TRIM(:Valor) = '''', ''0'', :Valor) AS Unitario
FROM Rdb$Database')
                               MATCHING (ID);
UPDATE OR INSERT INTO IMPORTADOS_MODELO (ID, ID_IMPORTADO, CAMPO, ACTIVO, SQL_CONVERSION)
                                 VALUES (17, 2, 'NA_Unitario', 'S', 'SELECT IIF(TRIM(:Valor) = '''', ''0'', :Valor) AS Bruto
FROM Rdb$Database')
                               MATCHING (ID);
UPDATE OR INSERT INTO IMPORTADOS_MODELO (ID, ID_IMPORTADO, CAMPO, ACTIVO, SQL_CONVERSION)
                                 VALUES (18, 2, 'NA_NotaMov', 'S', 'SELECT ''-'' AS Nota
FROM Rdb$Database')
                               MATCHING (ID);
UPDATE OR INSERT INTO IMPORTADOS_MODELO (ID, ID_IMPORTADO, CAMPO, ACTIVO, SQL_CONVERSION)
                                 VALUES (19, 3, 'NA_MegaNota', 'S', 'SELECT IIF(TRIM(:Valor)='''', ''.'', :Valor) AS Nota
FROM Rdb$Database')
                               MATCHING (ID);
UPDATE OR INSERT INTO IMPORTADOS_MODELO (ID, ID_IMPORTADO, CAMPO, ACTIVO, SQL_CONVERSION)
                                 VALUES (20, 2, 'NA_Usuario', 'S', 'SELECT IIF(TRIM(:Valor)='''', ''SUPERVISOR'', LEFT(:Valor, 10)) AS CodUsuario
FROM Rdb$Database')
                               MATCHING (ID);


COMMIT WORK;
