UPDATE Documentos
SET Formato1 = 'MC715_003.FR3'
WHERE Formato1 = 'MC715_001.FR3';

UPDATE Documentos
SET Formato = 'MC715_003.FR3'
WHERE Formato = 'MC715_001.FR3';

UPDATE OR INSERT INTO INFORMES (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD, DESTINO_FTP, CODREPORTE)
                        VALUES ('MC715_004', 'RELACION DE VENTAS AGRUPADO POR NOMBRE DE SUCURSAL', NULL, 'SELECT Nombre_Sucursal,
       COALESCE(Tipo, '''') Tipo,
       COALESCE(Prefijo, '''') Prefijo,
       COALESCE(Numero, '''') Numero,
       Fecha,
       Cantidad,
       Bruto,
       Descuento,
       Devolucion Devoluciones,
       Neto,
       Gravado,   
       No_gravado,
       Retencion,
       Neto + Gravado + No_Gravado + Retencion Total
FROM Fx_Dinamico(:_Desde, :_Hasta, ''VENTA'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''N'', ''N'', ''N'', ''N'', ''N'', ''N'', ''S'', ''N'', ''N'', ''N'', ''N'', ''N'', ''N'')
WHERE Nombre_Sucursal IS NOT NULL
ORDER BY 1', 'N', 'GESTION', 'N', 'SALDOS EN VENTAS', 'N', 'LIBRE', 5, 'N', 1, NULL, NULL)
                      MATCHING (CODIGO);

COMMIT WORK;

UPDATE OR INSERT INTO INFORMES (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD, DESTINO_FTP, CODREPORTE)
                        VALUES ('MC715_005', 'INFORME DE VENTAS X CENTRO DE COSTO', 'Mismos datos del auxiliar contable', 'SELECT Centro,
       Cuenta,
       Fecha,
       Tipo,
       Prefijo,
       Numero,
       Tercero,
       Debito,
       Credito,
       Base,
       Nota,
       Nombre_Centro,
       Nombre_Cuenta,
       Nombre_Tercero
FROM Conta_Dinamico(:Fecha_Desde, :Fecha_Hasta, ''41'', ''42950501'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''S'', ''S'', ''N'', ''N'', ''N'', ''N'', ''N'', ''N'', ''N'', ''N'')
WHERE Tipo IS NOT NULL
ORDER BY 1, 2, 3', 'N', 'GESTION', 'N', 'PERSONALIZADOS', 'N', 'LIBRE', 5, 'N', 1, NULL, NULL)
                      MATCHING (CODIGO);


COMMIT WORK;
