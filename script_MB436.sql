UPDATE OR INSERT INTO INFORMES (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, FORMATO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD, DESTINO_FTP, CODREPORTE)
                        VALUES ('MB436_004', 'CONTRATOS POR CLIENTE PARA ENVIO MAILING', 'GENERA LOS CONTRATOS VIGENTES PARA ENVIAR POR RUTINA MAILING', 'SELECT Zona_Cliente,
                   Cod_Tercero AS CODIGO,
                   Nombre_Cliente as NOMBRE,
                   Cel AS CEL,
                   Telefono,
                   Direccion,
                   Ciudad AS CIUDAD,
                   Email
            FROM Pz_Info_Contratos(:_Desde, :Codtercero)', 'N', 'GESTION', NULL, 'N', 'MAILING', 'N', 'LIBRE', 5, 'N', 1, NULL, 'MB436_004')
                      MATCHING (CODIGO);


COMMIT WORK;

UPDATE OR INSERT INTO REPORTES (CODIGO, NOMBRE, NOTA, ARCHIVO, ADJUNTO, NATIVO, MODULO, PARAMETROS, PUBLICAR, CODGRUPO, VALIDA_TRANSITO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD)
                        VALUES ('MB436_004', 'CONTRATOS', NULL, 'MB436_004.FR3', NULL, 'N', 'GESTION', NULL, 'N', 'NA', 'N', 'N', 'LIBRE', 5, 'N', 1)
                      MATCHING (CODIGO);


COMMIT WORK;

UPDATE OR INSERT INTO REPORTES (CODIGO, NOMBRE, NOTA, ARCHIVO, ADJUNTO, NATIVO, MODULO, PARAMETROS, PUBLICAR, CODGRUPO, VALIDA_TRANSITO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD)
                        VALUES ('MB436_003', 'HISTORICO DE CONTRATOS VIGENTES', 'MUESTRA LOS CONTRATOS VIGENTES POR TODOS LOS CLIENTES DETALLADO CON DIAS A VENCER', 'MB436_003.FR3', NULL, 'N', 'GESTION', 'FECHA_DESDE', 'N', 'PERSONALIZADOS', 'N', 'N', 'LIBRE', 5, 'N', 1)
                      MATCHING (CODIGO);


COMMIT WORK;

SET TERM ^ ;

CREATE OR ALTER PROCEDURE Pz_Info_Contratos (
    Fecha_Desde DATE,
    Codtercero  VARCHAR(15))
RETURNS (
    Zona_Cliente   VARCHAR(5),
    Cod_Tercero    VARCHAR(15),
    Nombre_Cliente VARCHAR(163),
    Cel            VARCHAR(50),
    Telefono       VARCHAR(50),
    Direccion      VARCHAR(80),
    Ciudad         VARCHAR(80),
    Email          VARCHAR(80),
    Cliente_Tiempo VARCHAR(200),
    Antiguedad     INTEGER)
AS
DECLARE VARIABLE V_Municipio      VARCHAR(5);
DECLARE VARIABLE V_Fecha_C        DATE;
DECLARE VARIABLE V_Antiguedad     INTEGER;
DECLARE VARIABLE V_Cliente_Tiempo VARCHAR(100);
BEGIN

  FOR SELECT DISTINCT Codtercero
      FROM Comprobantes
      WHERE (Tipo = 'CTRA')
            AND Fecha >= :Fecha_Desde
            AND Codtercero LIKE TRIM(COALESCE(:Codtercero, '%')) || '%'
            AND Vence >= CURRENT_DATE
      INTO Cod_Tercero

  DO
  BEGIN

    -----DATOS DEL CLIENTE
    SELECT Nombre,
           Telefono,
           Movil,
           Direccion,
           Codzona,
           Email,
           Codmunicipio

    FROM Terceros
    WHERE Codigo = :Cod_Tercero
    INTO Nombre_Cliente,
         Telefono,
         Cel,
         Direccion,
         Zona_Cliente,
         Email,
         V_Municipio;

    --NOMBRE CIUDAD
    SELECT Nombre
    FROM Municipios
    WHERE Codigo = :V_Municipio
    INTO Ciudad;

    SELECT FIRST 1 Fecha
    FROM Comprobantes
    WHERE Codtercero = :Cod_Tercero

    INTO V_Fecha_C;

    --PONGO LA FECHA QUE BUSCA Y NO ENCUENTRA EN CERO
    V_Fecha_C = COALESCE(V_Fecha_C, CURRENT_DATE);
    V_Antiguedad = CURRENT_DATE - V_Fecha_C;

    IF (V_Antiguedad >= 730) THEN
      V_Cliente_Tiempo = 'CLIENTE CON MAS DE DOS AÑOS';

    ELSE
    IF (V_Antiguedad > 0) THEN
      V_Cliente_Tiempo = 'CLIENTE CON MENOS DE DOS AÑOS';
    ELSE
      V_Cliente_Tiempo = 'CLIENTE SIN CONTRATO';
    Antiguedad = :V_Antiguedad;
    Cliente_Tiempo = :V_Cliente_Tiempo;
    SUSPEND;
  END
END^

SET TERM ; ^

COMMIT WORK;

SET TERM ^ ;

CREATE OR ALTER PROCEDURE Pz_Info_Contratos_Tercero (
    Fecha_Desde CHAR(10),
    Codtercero  VARCHAR(15))
RETURNS (
    Fecha_Factura        DATE,
    Vencimiento_Contrato DATE,
    Dias_Vencidos        INTEGER,
    Dias_Por_Vencer      INTEGER,
    Tipo                 VARCHAR(5),
    Prefijo              VARCHAR(5),
    Numero               VARCHAR(10),
    Lista                VARCHAR(5),
    Centro               VARCHAR(5),
    Oficina              VARCHAR(80),
    Metros_Cuadrados     NUMERIC(17,4))
AS
DECLARE VARIABLE V_Centro      CHAR(5);
DECLARE VARIABLE V_Oficina     VARCHAR(80);
DECLARE VARIABLE Tipo_Inmueble VARCHAR(80);
DECLARE VARIABLE V_Municipio   VARCHAR(5);
DECLARE VARIABLE V_Fecha_C     DATE;
BEGIN

  FOR SELECT Fecha,
             Vence,
             Tipo,
             Prefijo,
             Numero,
             Codlista

      FROM Comprobantes
      WHERE (Tipo = 'CTRA')
            AND Fecha >= :Fecha_Desde
            AND TRIM(Codtercero) = TRIM(:Codtercero)
            AND Vence >= CURRENT_DATE
            AND Bloqueado = 'S'
      ORDER BY Vence

      INTO Fecha_Factura,
           Vencimiento_Contrato,
           Tipo,
           Prefijo,
           Numero,
           Lista

  DO
  BEGIN

    SELECT FIRST 1 Codcentro
    FROM Tr_Inventario
    WHERE Tipo = :Tipo
          AND Prefijo = :Prefijo
          AND Numero = :Numero
    INTO Centro;

    --SE TRABAJA CON CENTROS PARA TRAER LA INFORMACION ADICIONAL COO DENSIDAD QUE ES EL METRAJE Y EL NOMBRE QUE ES EL NOMBRE DEL INMUEBLE Y EL TIPO DE NEGOCIO
    SELECT Nombre,
           Densidad
    FROM Centros
    WHERE Codigo = :Centro

    INTO V_Oficina,
         Metros_Cuadrados;
    Oficina = SUBSTRING(V_Oficina FROM 1 FOR POSITION('(' IN V_Oficina) - 1);

    ----buscaremos los dias vencidos despues del documento

    Dias_Vencidos = CURRENT_DATE - Vencimiento_Contrato;

    Dias_Por_Vencer = 0;
    IF (Dias_Vencidos < 0) THEN


    BEGIN
      Dias_Vencidos = 0;

      Dias_Por_Vencer = Vencimiento_Contrato - CURRENT_DATE;

    END

    SUSPEND;

  END
END^

SET TERM ; ^

COMMIT WORK;

SET TERM ^ ;

CREATE OR ALTER PROCEDURE Pz_Total_Info_Contratos (
    Fecha_Desde DATE)
RETURNS (
    Zona_Cliente         VARCHAR(5),
    Cod_Tercero          VARCHAR(15),
    Nombre_Cliente       VARCHAR(163),
    Cel                  VARCHAR(50),
    Telefono             VARCHAR(50),
    Direccion            VARCHAR(80),
    Ciudad               VARCHAR(80),
    Email                VARCHAR(80),
    Cliente_Tiempo       VARCHAR(200),
    Antiguedad           INTEGER,
    Fecha_Factura        DATE,
    Vencimiento_Contrato DATE,
    Dias_Vencidos        INTEGER,
    Dias_Por_Vencer      INTEGER,
    Tipo_Doc             VARCHAR(5),
    Prefijo_Doc          VARCHAR(5),
    Numero_Doc           VARCHAR(10),
    Lista                VARCHAR(5),
    Centro               VARCHAR(5),
    Oficina              VARCHAR(80),
    Metros_Cuadrados     NUMERIC(17,4))
AS
BEGIN

  FOR SELECT Zona_Cliente,
             Cod_Tercero,
             Nombre_Cliente,
             Cel,
             Telefono,
             Direccion,
             Ciudad,
             Email,
             Cliente_Tiempo,
             Antiguedad
      FROM Pz_Info_Contratos(:Fecha_Desde, :Cod_Tercero)
      INTO :Zona_Cliente,
           :Cod_Tercero,
           :Nombre_Cliente,
           :Cel,
           :Telefono,
           :Direccion,
           :Ciudad,
           :Email,
           :Cliente_Tiempo,
           :Antiguedad

  DO
  BEGIN

    FOR SELECT Fecha_Factura,
               Vencimiento_Contrato,
               Dias_Vencidos,
               Dias_Por_Vencer,
               Tipo,
               Prefijo,
               Numero,
               Lista,
               Centro,
               Oficina,
               Metros_Cuadrados
        FROM Pz_Info_Contratos_Tercero(:Fecha_Desde, :Cod_Tercero)
        INTO :Fecha_Factura,
             :Vencimiento_Contrato,
             :Dias_Vencidos,
             :Dias_Por_Vencer,
             Tipo_Doc,
             Prefijo_Doc,
             Numero_Doc,
             :Lista,
             :Centro,
             :Oficina,
             :Metros_Cuadrados

    DO
    BEGIN

      SUSPEND;
    END
  END
END^

SET TERM ; ^

COMMIT WORK;



