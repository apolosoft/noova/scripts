UPDATE OR INSERT INTO INFORMES (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD, DESTINO_FTP, CODREPORTE)
                        VALUES ('RA1425_002', 'INFORME DE COMPRAS', 'INFORME DE COMPRAS SOLO FACTURAS DE COMPRAS', 'SELECT DISTINCT C.Codtercero AS Nit_Proveedor,
                (SELECT Nombre_Tercero
                 FROM Fn_Nombre_Tercero(C.Codtercero)) Nombre_Proveedor,
                R.Tipo || R.Prefijo || CAST(R.Numero AS INT) AS Numero_Factura,
                C.Fecha Fecha_Factura,
                R.Codreferencia AS Codigo_Item,
                (SELECT Nombre_Referencia
                 FROM Fn_Nombre_Referencia(R.Codreferencia)) Nombre_Item,
                (R.Entrada) Cantidad_Item,
                ((R.Entrada + R.Salida) * R.Bruto) Costo_Item_Compra,
                Dt.Tarifa Iva
FROM Tr_Inventario R
INNER JOIN Comprobantes C ON (R.Tipo = C.Tipo AND R.Prefijo = C.Prefijo AND R.Numero = C.Numero)
INNER JOIN Documentos D ON (R.Tipo = D.Codigo)
LEFT JOIN Referencias Re ON (R.Codreferencia = Re.Codigo)
LEFT JOIN Esqimpuestos Ei ON (Ei.Codigo = Re.Cod_Esqimpuesto)
LEFT JOIN Esquema_Impuestos Ee ON (Ee.Codesquema = Ei.Codigo)
LEFT JOIN Data_Impuestos Dt ON (Dt.Codtipoimpuesto = Ee.Tipo_Impuesto) AND Dt.Ano = EXTRACT(YEAR FROM CURRENT_DATE)
WHERE C.Fecha >= :_Desde
      AND C.Fecha <= :_Hasta
      AND D.Inventario <> ''NO''
      AND D.C_Codtercero = ''S''
      AND (R.Entrada <> 0 OR R.Salida <> 0)
      AND D.Grupo = ''COMPRA''
      AND D.Codigo LIKE ''FC%''', 'N', 'GESTION', 'N', 'SALDOS DE INVENTARIO', 'N', 'LIBRE', 4, 'S', 1, NULL, NULL)
                      MATCHING (CODIGO);
UPDATE OR INSERT INTO INFORMES (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD, DESTINO_FTP, CODREPORTE)
                        VALUES ('RA1425_003', 'INFORME DE CONSUMOS', 'INFORME DE CONSUMOS', 'SELECT DISTINCT A.Tipo || A.Prefijo || A.Numero Documento,
                A.Codreferencia Codigo_Item,
                A.Nombre_Referencia Nombre_Item,
                COALESCE(A.Nombre_Lote, ''''),
                --F.Ultimo_Costo
                -- 0 Costo_Ultimo,
                A.Ponderado Costo_Promedio,
                Dt.Tarifa AS Iva,
                COALESCE(A.Salidas, 0) AS Cantidad_Item,
                A.Codbodega Codigo_Bodega,
                A.Nombre_Bodega,
                Co.Fecha,
                Tr.Nota Unidad_Descarga
FROM Consumos_Rango(:_Desde, :_Hasta) A
--JOIN Fx_Auxiliar_Inventario(:_Desde, :_Hasta) F ON (F.Referencia = A.Codreferencia) AND (A.Tipo = F.Tipo AND A.Prefijo = F.Prefijo AND A.Numero = F.Numero AND A.Renglon = F.Renglon)
JOIN Comprobantes Co ON (Co.Tipo = A.Tipo AND Co.Prefijo = A.Prefijo AND Co.Numero = A.Numero)
JOIN Tr_Inventario Tr ON (Tr.Tipo = A.Tipo AND Tr.Prefijo = A.Prefijo AND Tr.Numero = A.Numero AND A.Renglon = Tr.Renglon)
JOIN Referencias R ON (A.Codreferencia = R.Codigo) AND (R.Codigo = Tr.Codreferencia)
JOIN Bodegas B ON (A.Codbodega = B.Codigo) AND (B.Codigo = Tr.Codbodega)
JOIN Lineas L ON (L.Codigo = R.Codlinea)
JOIN Esqimpuestos Ei ON (Ei.Codigo = R.Cod_Esqimpuesto)
JOIN Esquema_Impuestos Esi ON (Esi.Codesquema = Ei.Codigo)-- AND Esi.Grupo = ''VENTA''
JOIN Data_Impuestos Dt ON (Dt.Codtipoimpuesto = Esi.Tipo_Impuesto) AND Dt.Ano = EXTRACT(YEAR FROM CURRENT_DATE)
WHERE COALESCE(A.Salidas, 0) > 0
      AND R.Valoriza = ''S''
      AND R.Saldos = ''S''
ORDER BY 9, 10, 2', 'N', 'GESTION', 'N', 'SALDOS DE INVENTARIO', 'N', 'LIBRE', 4, 'S', 1, NULL, NULL)
                      MATCHING (CODIGO);
UPDATE OR INSERT INTO INFORMES (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD, DESTINO_FTP, CODREPORTE)
                        VALUES ('RA1425_006', 'INFORME DE DEVOLUCIONES', 'INFORME DE DEVOLUCIONES', 'SELECT C.Tercero Nit_Proveedor,
       Te.Nombre Nombre_Proveedor,
       C.Referencia Codigo_Item,
       Re.Nombre Nombre_Item,
       ABS(C.Cantidad) Cantidad,
       Co.Nota Concepto_Devolucion,
       Co.Tipo || Co.Prefijo || Co.Numero Documento,
       C.Fecha
FROM Fx_Dinamico(:_Desde, :_Hasta, ''COMPRA'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''N'', ''N'', ''N'', ''S'', ''N'', ''N'', ''N'', ''S'', ''N'', ''N'', ''N'', ''N'', ''N'') C
JOIN Documentos D ON (C.Tipo = D.Codigo)
JOIN Comprobantes Co ON (Co.Tipo = C.Tipo AND Co.Prefijo = C.Prefijo AND Co.Numero = C.Numero)
JOIN Terceros Te ON (C.Tercero = Te.Codigo)
JOIN Referencias Re ON (Re.Codigo = C.Referencia)
WHERE D.Codigo NOT IN (''FC1'', ''FC2'')
      AND D.Signo = ''CREDITO''
      AND D.Inventario = ''SALIDA''', 'N', 'GESTION', 'N', 'SALDOS DE INVENTARIO', 'N', 'LIBRE', 4, 'S', 1, NULL, NULL)
                      MATCHING (CODIGO);
UPDATE OR INSERT INTO INFORMES (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD, DESTINO_FTP, CODREPORTE)
                        VALUES ('RA1425_001', 'INFORME INVENTARIO', 'INFORME DE INVENTARIO', 'SELECT DISTINCT I.Referencia Codigo_Item,
                R.Nombre Nombre_Item,
                I.Bodega Codigo_Bodega,
                B.Nombre Nombre_Bodega,
                -- COALESCE(Ai.Ultimo_Costo, 0) Costo_Ultimo,
                I.Ponderado Costo_Promedio,
                Dt.Tarifa Iva,
                (I.Entradas - I.Salidas) Cantidad_Item,
                I.Actualizado
FROM Fx_Inventario_Fecha(CURRENT_DATE) I
LEFT JOIN Referencias R ON (I.Referencia = R.Codigo)
LEFT JOIN Esqimpuestos Ei ON (Ei.Codigo = R.Cod_Esqimpuesto)
LEFT JOIN Esquema_Impuestos Ee ON (Ee.Codesquema = Ei.Codigo)
LEFT JOIN Data_Impuestos Dt ON (Dt.Codtipoimpuesto = Ee.Tipo_Impuesto) AND Dt.Ano = EXTRACT(YEAR FROM CURRENT_DATE)
LEFT JOIN Bodegas B ON (I.Bodega = B.Codigo)
LEFT JOIN Lineas L ON (R.Codlinea = L.Codigo)
--INNER JOIN Fx_Auxiliar_Inventario(CURRENT_DATE, CURRENT_DATE) Ai ON (Ai.Referencia = R.Codigo) AND (Ai.Bodega = B.Codigo)
WHERE I.Bodega LIKE TRIM(:Bodega) || ''%''
      AND R.Codlinea LIKE TRIM(:Linea) || ''%''
ORDER BY 3, 2', 'N', 'GESTION', 'N', 'SALDOS DE INVENTARIO', 'N', 'LIBRE', 4, 'S', 1, NULL, NULL)
                      MATCHING (CODIGO);
UPDATE OR INSERT INTO INFORMES (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD, DESTINO_FTP, CODREPORTE)
                        VALUES ('RA1425_004', 'INFORME DE ORDENES DE COMPRAS', 'INFORME DE ORDENES DE COMPRAS SOLO OC Y REM', 'EXECUTE BLOCK (
    Desde DATE = :_Desde,
    Hasta DATE = :_Hasta)
RETURNS (
    Tipo CHAR(5),
    Prefijo CHAR(5),
    Numero CHAR(10),
    Fecha DATE,
    Referencia CHAR(20),
    Entrada NUMERIC(17,4),
    Nombre_Referencia CHAR(80),
    Valor NUMERIC(17,4),
    Tipo_Destino CHAR(5),
    Prefijo_Destino CHAR(5),
    Numero_Destino CHAR(10),
    Fecha_Dest DATE,
    Valor_Dest NUMERIC(17,4),
    Entrada_Dest NUMERIC(17,4),
    Tercero CHAR(15),
    Nombre_Tercero CHAR(164))
AS
DECLARE Vrenglon INTEGER;
DECLARE Vsql CHAR(1000);

BEGIN
  -- buscar los documentos origen (OC1, REM)
  -- buscar el renglon en el campo orden de los destinos (facturas)
  -- si no lo encuentra debe mostrarlo SUSPEND
  -- si lo encuentra validar que el tipo de documento no este en los documentos origenes (OC1, REM)

  Vsql = ''SELECT DISTINCT
             Tipo,
             Prefijo,
             LPAD(TRIM(NUMERO) ,10) AS NUMERO,
             C.Fecha,
             Renglon,
             Codreferencia,
             (SELECT Nombre_Referencia
              FROM Fn_Nombre_Referencia(Codreferencia)),
              T.Entrada,
              T.Entrada*(T.Unitario-T.Descuento)
      FROM Tr_Inventario T
      JOIN Comprobantes C USING (Tipo, Prefijo, Numero)
      JOIN Documentos D on (c.tipo=d.codigo)
      JOIN Rutas R ON (T.Tipo = R.Origen)
      WHERE D.Grupo = ''''COMPRA''''
        And C.Fecha >= '''''' || Desde || '''''' AND  C.fecha <= '''''' || Hasta || '''''''';
  FOR EXECUTE STATEMENT(Vsql)
          INTO Tipo, Prefijo, Numero, Fecha, Vrenglon, Referencia, Nombre_Referencia, Entrada, Valor
  DO
  BEGIN
    Vsql = '' SELECT DISTINCT T.Tipo,
                    T.Prefijo,
                    LPAD(TRIM(NUMERO) ,10) AS NUMERO,
                    C.Fecha,
                    T.Entrada,
                    T.Entrada*(T.Unitario-T.Descuento),
                    C.CodTercero,
                    (SELECT NOMBRE_TERCERO
                    FROM FN_NOMBRE_TERCERO (C.CODTERCERO))
  FROM Tr_Inventario T
  JOIN Comprobantes C USING (Tipo, Prefijo, Numero)
  JOIN Rutas R ON (T.Tipo = R.Destino)
  WHERE T.Orden = '''''' || Vrenglon || '''''' AND R.Destino NOT LIKE ''''FC%'''' AND T.Tipo <> R.Origen '';
    FOR EXECUTE STATEMENT(Vsql)
            INTO Tipo_Destino, Prefijo_Destino, Numero_Destino, Fecha_Dest, Entrada_Dest, Valor_Dest, Tercero, Nombre_Tercero
    DO
      SUSPEND;
  END
END', 'N', 'GESTION', 'N', 'SALDOS DE INVENTARIO', 'N', 'LIBRE', 4, 'S', 1, NULL, NULL)
                      MATCHING (CODIGO);
UPDATE OR INSERT INTO INFORMES (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD, DESTINO_FTP, CODREPORTE)
                        VALUES ('RA1425_005', 'INFORME DE VENCIMIENTOS', 'INFORME DE VENCIMIENTOS', 'SELECT F.Referencia Codigo_Item,
       R.Nombre Nombre_Item,
       F.Bodega Codigo_Bodega,
       B.Nombre Nombre_Bodega,
       F.Saldo Cantidad_Item,
       F.Lote,
       F.Vencimiento Fecha_Vencimiento,
       Estado
FROM Fx_Inventario_Lotes F
INNER JOIN Referencias R ON (F.Referencia = R.Codigo)
INNER JOIN Bodegas B ON (F.Bodega = B.Codigo)
WHERE TRIM(R.Lote) = ''S'' -- Solo referencias que tengan Lotes
      AND F.Lote <> ''NA'' -- Lotes distintos a No Aplica
      AND F.Saldo > 0 -- Referencias que tengan Stock
      AND Vencimiento IS NOT NULL', 'N', 'GESTION', 'N', 'SALDOS DE INVENTARIO', 'N', 'LIBRE', 4, 'S', 1, NULL, NULL)
                      MATCHING (CODIGO);

COMMIT WORK;
