UPDATE LINEAS
SET GRUPO = 'FAMILIA'
WHERE CODIGO = '03';


UPDATE OR INSERT INTO DESTINOS (CODIGO, NOMBRE, FTP)
                        VALUES ('COSMOS', 'COSMOS (GRUPO FAMILIA)', 'filetx.grupofamilia.com
dist_agh
&%Los.compromisos:no.van:conmigo6%&*
CO-CBIA-DTR-0117_:A:M:D
S
HOY
990')
                      MATCHING (CODIGO);


COMMIT WORK;



UPDATE OR INSERT INTO AUTOMATOR (CODIGO, NOMBRE, MODO, SQL_CONSULTA, ACTIVA, DELIMITADOR, CODDESTINO)
                         VALUES ('1', 'CO-CBIA-DTR-0117_MC_:A:M:D', 'FTP', '-- Maestro Clientes

SELECT *
FROM Pz_Autom_Cosmos_Clientes(:ayer-45 ,:ayer,''FAMILIA'')
--FROM Pz_Autom_Cosmos_Clientes(''01.08.2022'' ,''30.09.2022'',''FAMILIA'')', 'S', '|', 'COSMOS')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO AUTOMATOR (CODIGO, NOMBRE, MODO, SQL_CONSULTA, ACTIVA, DELIMITADOR, CODDESTINO)
                         VALUES ('2', 'CO-CBIA-DTR-0117_M_:A:M:D', 'FTP', '-- Transaccional Ventas

SELECT *
FROM Pz_Autom_Cosmos_Ventas(:ayer-45 ,:ayer,''FAMILIA'')
--FROM Pz_Autom_Cosmos_Ventas(''01.08.2022'' ,''30.09.2022'',''FAMILIA'')', 'S', '|', 'COSMOS')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO AUTOMATOR (CODIGO, NOMBRE, MODO, SQL_CONSULTA, ACTIVA, DELIMITADOR, CODDESTINO)
                         VALUES ('3', 'CO-CBIA-DTR-0117_I_:A:M:D', 'FTP', '-- Transaccional Inventarios

SELECT *
FROM Pz_Autom_Cosmos_Inventario(:ayer-45 ,:ayer,''FAMILIA'')
--FROM Pz_Autom_Cosmos_Inventario(''01.08.2022'' ,''30.09.2022'',''FAMILIA'')', 'S', '|', 'COSMOS')
                       MATCHING (CODIGO);


COMMIT WORK;


/********************* STORED PROCEDURES ***********************************************************/


SET TERM ^ ;

CREATE OR ALTER PROCEDURE PZ_AUTOM_COSMOS_CLIENTES (
    DESDE_ DATE,
    HASTA_ DATE,
    GRUPO_LINEA_ VARCHAR(20))
RETURNS (
    "Cliente_Cod" VARCHAR(15),
    "Cliente_Nombre" VARCHAR(163),
    "Cliente_Dir" VARCHAR(80),
    "Cliente_Tel" VARCHAR(50),
    "Cliente_FechaCrea" VARCHAR(1),
    "Cliente_FechaDesact" VARCHAR(1),
    "Cliente_Periodicidad" VARCHAR(1),
    "Cliente_MunCod" VARCHAR(5),
    "Cliente_MunDesc" VARCHAR(80),
    "Cliente_DeptoCod" VARCHAR(5),
    "Cliente_DeptoDesc" VARCHAR(80),
    "Cliente_CanalCod" VARCHAR(10),
    "Cliente_CanalDesc" VARCHAR(80),
    "Cliente_SubCanalCod" VARCHAR(1),
    "Cliente_SubCanalDesc" VARCHAR(1),
    "Cliente_GPS_Lat" VARCHAR(1),
    "Cliente_GPS_Long" VARCHAR(1),
    "Cliente_Barrio" VARCHAR(20),
    "Cliente_Contacto_Cedula" VARCHAR(10),
    "Cliente_Contacto_Nombre" VARCHAR(80),
    "Cliente_SectorCod" VARCHAR(1),
    "Cliente_SectorDesc" VARCHAR(1),
    "Cliente_RutaCod" VARCHAR(6),
    "Cliente_RutaDesc" VARCHAR(80),
    "PlanComercial_Cod" VARCHAR(1),
    "PlanComercial_Desc" VARCHAR(1),
    "MaestrasClientes_FechaGeneracio" VARCHAR(19))
AS
BEGIN
  SUSPEND;
END^





CREATE OR ALTER PROCEDURE PZ_AUTOM_COSMOS_INVENTARIO (
    DESDE_ DATE,
    HASTA_ DATE,
    GRUPO_LINEA_ VARCHAR(20))
RETURNS (
    "Nit_Distribuidor" VARCHAR(15),
    "Inventario_Fecha" VARCHAR(19),
    "Prod_Dist_Cod" VARCHAR(20),
    "Prod_Dist_Desc" VARCHAR(80),
    "Prod_Prov_Cod" VARCHAR(20),
    "Prd_UnidadInventario" VARCHAR(5),
    "Cant_Disponible" DOUBLE PRECISION,
    "Cant_Stock" DOUBLE PRECISION,
    "Costo_Disponible" DOUBLE PRECISION,
    "Costo_Stock" DOUBLE PRECISION,
    "Prov_Cod" VARCHAR(15),
    "Bodega_Cod" VARCHAR(5),
    "Bodega_Desc" VARCHAR(80),
    "Agencia_Cod" VARCHAR(15),
    "Agencia_Desc" VARCHAR(50),
    "Inventario_Estado" VARCHAR(10))
AS
BEGIN
  SUSPEND;
END^





CREATE OR ALTER PROCEDURE PZ_AUTOM_COSMOS_VENTAS (
    DESDE_ DATE,
    HASTA_ DATE,
    GRUPO_LINEA_ VARCHAR(20))
RETURNS (
    "Nit_Distribuidor" VARCHAR(15),
    "Factura_Numero" VARCHAR(15),
    "Factura_Fecha_Movimiento" VARCHAR(19),
    "Prod_Dist_Cod" VARCHAR(20),
    "Prod_Dist_Desc" VARCHAR(80),
    "Prod_Prov_Cod" VARCHAR(20),
    "Cant_Vendida" DOUBLE PRECISION,
    "Prod_UnidadVenta" VARCHAR(5),
    "Precio_Unit" DOUBLE PRECISION,
    "Precio_Total" DOUBLE PRECISION,
    "Costo" DOUBLE PRECISION,
    "Descuento" DOUBLE PRECISION,
    "Transaccion_Codigo" CHAR(2),
    "Transaccion_Descripccion" VARCHAR(10),
    "CausalTransaccion_Codigo" CHAR(1),
    "CausalTransaccion_Descripccion" CHAR(1),
    "Vend_Cedula" VARCHAR(15),
    "Vend_Nombre" VARCHAR(83),
    "Vend_Tel" VARCHAR(50),
    "Vend_Dir" VARCHAR(80),
    "Cliente_Cod" VARCHAR(15),
    "Cliente_Nombre" VARCHAR(220),
    "Cliente_Dir" VARCHAR(80),
    "Cliente_Tel" VARCHAR(50),
    "Cliente_FechaCrea" CHAR(1),
    "Cliente_FechaDesact" CHAR(1),
    "Cliente_Periodicidad" CHAR(1),
    "Cliente_RutaCod" VARCHAR(6),
    "Cliente_RutaDesc" VARCHAR(80),
    "Cliente_MunCod" VARCHAR(6),
    "Cliente_MunDesc" VARCHAR(80),
    "Sup_Cedula" CHAR(15),
    "Sup_Nombre" CHAR(83),
    "Sup_Dir" CHAR(80),
    "Sup_Tel" CHAR(50),
    "Cliente_CanalCod" VARCHAR(10),
    "Cliente_CanalDesc" VARCHAR(80),
    "Cliente_DeptoCod" VARCHAR(2),
    "Cliente_DeptoDesc" VARCHAR(80),
    "Prov_Cod" VARCHAR(15),
    "Prov_Desc" VARCHAR(163),
    "Cliente_SectorCod" CHAR(1),
    "Cliente_SectorDesc" CHAR(1),
    "Factura_Estado" CHAR(1),
    "PlanComercial_Cod" CHAR(1),
    "PlanComercial_Desc" CHAR(1),
    "Cliente_SubCanalCod" CHAR(1),
    "Cliente_SubCanalDesc" CHAR(1),
    "Agencia_Cod" VARCHAR(15),
    "Agencia_Desc" VARCHAR(50),
    "Bodega_Cod" VARCHAR(5),
    "Bodega_Desc" VARCHAR(80),
    "Cliente_GPS_Lat" CHAR(1),
    "Cliente_GPS_Long" CHAR(1),
    "Factura_GPS_Lat" CHAR(1),
    "Factura_GPS_Long" CHAR(1),
    "Cliente_Barrio" VARCHAR(20),
    "Cliente_Contacto_Cedula" CHAR(1),
    "Cliente_Contacto_Nombre" CHAR(1),
    "Ventas_FechaGeneracion" VARCHAR(19))
AS
BEGIN
  SUSPEND;
END^






SET TERM ; ^



/******************************************************************************/
/***                           Stored procedures                            ***/
/******************************************************************************/



SET TERM ^ ;

CREATE OR ALTER PROCEDURE PZ_AUTOM_COSMOS_CLIENTES (
    DESDE_ DATE,
    HASTA_ DATE,
    GRUPO_LINEA_ VARCHAR(20))
RETURNS (
    "Cliente_Cod" VARCHAR(15),
    "Cliente_Nombre" VARCHAR(163),
    "Cliente_Dir" VARCHAR(80),
    "Cliente_Tel" VARCHAR(50),
    "Cliente_FechaCrea" VARCHAR(1),
    "Cliente_FechaDesact" VARCHAR(1),
    "Cliente_Periodicidad" VARCHAR(1),
    "Cliente_MunCod" VARCHAR(5),
    "Cliente_MunDesc" VARCHAR(80),
    "Cliente_DeptoCod" VARCHAR(5),
    "Cliente_DeptoDesc" VARCHAR(80),
    "Cliente_CanalCod" VARCHAR(10),
    "Cliente_CanalDesc" VARCHAR(80),
    "Cliente_SubCanalCod" VARCHAR(1),
    "Cliente_SubCanalDesc" VARCHAR(1),
    "Cliente_GPS_Lat" VARCHAR(1),
    "Cliente_GPS_Long" VARCHAR(1),
    "Cliente_Barrio" VARCHAR(20),
    "Cliente_Contacto_Cedula" VARCHAR(10),
    "Cliente_Contacto_Nombre" VARCHAR(80),
    "Cliente_SectorCod" VARCHAR(1),
    "Cliente_SectorDesc" VARCHAR(1),
    "Cliente_RutaCod" VARCHAR(6),
    "Cliente_RutaDesc" VARCHAR(80),
    "PlanComercial_Cod" VARCHAR(1),
    "PlanComercial_Desc" VARCHAR(1),
    "MaestrasClientes_FechaGeneracio" VARCHAR(19))
AS
DECLARE VARIABLE V_Tercero VARCHAR(15);
DECLARE VARIABLE V_Nombre_Tercero VARCHAR(163);
DECLARE VARIABLE V_Direccion VARCHAR(80);
DECLARE VARIABLE V_Telefono VARCHAR(50);
DECLARE VARIABLE V_Municipio VARCHAR(5);
DECLARE VARIABLE V_Nombre_Municipio VARCHAR(80);
DECLARE VARIABLE V_Departamento VARCHAR(10);
DECLARE VARIABLE V_Nombre_Departamento VARCHAR(80);
DECLARE VARIABLE V_Tipologia VARCHAR(10);
DECLARE VARIABLE V_Nombre_Tipologia VARCHAR(80);
DECLARE VARIABLE V_Barrio VARCHAR(20);
DECLARE VARIABLE V_Contacto VARCHAR(10);
DECLARE VARIABLE V_Nombre_Contacto VARCHAR(80);
DECLARE VARIABLE V_Razon VARCHAR(80);
DECLARE VARIABLE V_Cliente_Rutacod VARCHAR(6);
DECLARE VARIABLE V_Cliente_Rutadesc VARCHAR(80);
BEGIN

  "Cliente_FechaCrea" = '';
  "Cliente_FechaDesact" = '';
  "Cliente_Periodicidad" = '';
  "Cliente_SubCanalCod" = '';
  "Cliente_SubCanalDesc" = '';
  "Cliente_GPS_Lat" = '';
  "Cliente_GPS_Long" = '';
  "Cliente_SectorCod" = '';
  "Cliente_SectorDesc" = '';
  "PlanComercial_Cod" = '';
  "PlanComercial_Desc" = '';
  "MaestrasClientes_FechaGeneracio" = EXTRACT(YEAR FROM CURRENT_DATE) || '-' || LPAD(EXTRACT(MONTH FROM CURRENT_DATE), 2, '0') || '-' || LPAD(EXTRACT(DAY FROM CURRENT_DATE), 2, '0') || ' ' || SUBSTRING(CURRENT_TIME FROM 1 FOR 8);

  FOR SELECT DISTINCT D.Tercero,
                      D.Nombre_Tercero,
                      D.Municipio,
                      D.Nombre_Municipio
      FROM Fx_Dinamico(:Desde_, :Hasta_, 'VENTA', '%', '%', '%', '%', '%', '%', '%', '%', '%', '%', '%', 'S', 'N', 'N', 'S', 'S', 'N', 'N', 'S', 'S', 'S', 'N', 'N', 'N') D
      WHERE Linea IN (SELECT Codigo
                      FROM Lineas
                      WHERE Grupo = :Grupo_Linea_)
      INTO V_Tercero,
           V_Nombre_Tercero,
           V_Municipio,
           V_Nombre_Municipio
  DO
  BEGIN

    "Cliente_Cod" = TRIM(V_Tercero);
    "Cliente_Nombre" = TRIM(V_Nombre_Tercero) || IIF(COALESCE(V_Razon, '') = '', '', ' - ' || TRIM(V_Razon));
    "Cliente_MunCod" = TRIM(V_Municipio);
    IF (POSITION('-', V_Nombre_Municipio) > 0) THEN
      "Cliente_MunDesc" = SUBSTRING(V_Nombre_Municipio FROM 1 FOR POSITION('-' IN V_Nombre_Municipio) - 1);
    ELSE
      "Cliente_MunDesc" = TRIM(V_Nombre_Municipio);

    -- Datos Tercero
    SELECT COALESCE(T.Direccion, ''),
           IIF(TRIM(COALESCE(Telefono, '')) = '', TRIM(COALESCE(Movil, '')), IIF(TRIM(COALESCE(Movil, '')) = '', TRIM(COALESCE(Telefono, '')), TRIM(Telefono) || ' - ' || TRIM(Movil))) Telefono,
           COALESCE(T.Codtipologia, '01'),
           COALESCE(T.Barrio, ''),
           IIF(TRIM(COALESCE(T.Razon_Comercial, '')) = '', T.Empresa, T.Razon_Comercial),
           TRIM(COALESCE(T.Codzona, ''))
    FROM Terceros T
    WHERE Codigo = :V_Tercero
    INTO V_Direccion,
         V_Telefono,
         V_Tipologia,
         V_Barrio,
         V_Razon,
         V_Cliente_Rutacod;

    "Cliente_Dir" = TRIM(V_Direccion);
    "Cliente_Tel" = TRIM(V_Telefono);
    "Cliente_CanalCod" = TRIM(V_Tipologia);
    "Cliente_Barrio" = TRIM(V_Barrio);
    "Cliente_RutaCod" = TRIM(V_Cliente_Rutacod);

    --Zonas
    SELECT Nombre
    FROM Zonas
    WHERE Codigo = :V_Cliente_Rutacod
    INTO V_Cliente_Rutadesc;
    "Cliente_RutaDesc" = TRIM(V_Cliente_Rutadesc);

    -- Tipologia
    SELECT TRIM(COALESCE(Tp.Nombre, ''))
    FROM Tipologias Tp
    WHERE Tp.Codigo = :V_Tipologia
    INTO V_Nombre_Tipologia;
    "Cliente_CanalDesc" = TRIM(V_Nombre_Tipologia);

    -- Departamento
    V_Departamento = SUBSTRING(V_Municipio FROM 1 FOR 2);
    "Cliente_DeptoCod" = TRIM(V_Departamento);

    SELECT M.Nombre
    FROM Municipios M
    WHERE M.Codigo = :V_Departamento
    INTO V_Nombre_Departamento;

    "Cliente_DeptoDesc" = TRIM(V_Nombre_Departamento);

    -- Contactos
    SELECT FIRST 1 C.Codigo,
                   C.Nombre
    FROM Contactos C
    WHERE C.Codtercero = :V_Tercero
          AND C.Activo = 'S'
    INTO V_Contacto,
         V_Nombre_Contacto;

    "Cliente_Contacto_Cedula" = '';
    "Cliente_Contacto_Nombre" = '';

    SUSPEND;
  END

END^


CREATE OR ALTER PROCEDURE PZ_AUTOM_COSMOS_INVENTARIO (
    DESDE_ DATE,
    HASTA_ DATE,
    GRUPO_LINEA_ VARCHAR(20))
RETURNS (
    "Nit_Distribuidor" VARCHAR(15),
    "Inventario_Fecha" VARCHAR(19),
    "Prod_Dist_Cod" VARCHAR(20),
    "Prod_Dist_Desc" VARCHAR(80),
    "Prod_Prov_Cod" VARCHAR(20),
    "Prd_UnidadInventario" VARCHAR(5),
    "Cant_Disponible" DOUBLE PRECISION,
    "Cant_Stock" DOUBLE PRECISION,
    "Costo_Disponible" DOUBLE PRECISION,
    "Costo_Stock" DOUBLE PRECISION,
    "Prov_Cod" VARCHAR(15),
    "Bodega_Cod" VARCHAR(5),
    "Bodega_Desc" VARCHAR(80),
    "Agencia_Cod" VARCHAR(15),
    "Agencia_Desc" VARCHAR(50),
    "Inventario_Estado" VARCHAR(10))
AS
DECLARE VARIABLE V_Empresa VARCHAR(15);
DECLARE VARIABLE V_Razon_Comercial VARCHAR(50);
DECLARE VARIABLE V_Fecha DATE;
DECLARE VARIABLE V_Referencia VARCHAR(20);
DECLARE VARIABLE V_Nombre_Referencia VARCHAR(80);
DECLARE VARIABLE V_Referencia_Codigo2 VARCHAR(20);
DECLARE VARIABLE V_Medida VARCHAR(5);
DECLARE VARIABLE V_Saldo DOUBLE PRECISION;
DECLARE VARIABLE V_Costo DOUBLE PRECISION;
DECLARE VARIABLE V_Bodega VARCHAR(5);
DECLARE VARIABLE V_Nombre_Bodega VARCHAR(80);
DECLARE VARIABLE V_Estado VARCHAR(10);
BEGIN

  -- Tercero Empresa
  SELECT T.Codigo,
         IIF(TRIM(COALESCE(T.Razon_Comercial, '')) = '', T.Empresa, T.Razon_Comercial)
  FROM Terceros T
  WHERE T.Datos_Empresa = 'S'
  INTO V_Empresa,
       V_Razon_Comercial;

  "Nit_Distribuidor" = TRIM(V_Empresa);
  "Prov_Cod" = '890900161';
  "Agencia_Cod" = TRIM(V_Empresa);
  "Agencia_Desc" = TRIM(V_Razon_Comercial);

  -- Recorrido por Referencia
  FOR SELECT R.Fecha,
             I.Referencia,
             (I.Entradas - I.Salidas),
             I.Bodega
      FROM Fx_Inventario_Fecha(:Hasta_) I
      JOIN Fx_Dinamico(:Desde_, :Hasta_, 'VENTA', '%', '%', '%', '%', '%', '%', '%', '%', '%', '%', '%', 'N', 'N', 'N', 'S', 'S', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'S') R ON (R.Referencia = I.Referencia)
      WHERE Linea IN (SELECT Codigo
                      FROM Lineas
                      WHERE Grupo = :Grupo_Linea_)
      INTO V_Fecha,
           V_Referencia,
           V_Saldo,
           V_Bodega

  DO
  BEGIN

    "Inventario_Fecha" = EXTRACT(YEAR FROM V_Fecha) || '-' || LPAD(EXTRACT(MONTH FROM V_Fecha), 2, '0') || '-' || LPAD(EXTRACT(DAY FROM V_Fecha), 2, '0') || ' 00:00:00';
    "Prod_Dist_Cod" = TRIM(V_Referencia);

    -- Datos Referencia
    SELECT R.Nombre,
           R.Codigo2,
           R.Codmedida,
           R.Activa
    FROM Referencias R
    WHERE R.Codigo = :V_Referencia
    INTO V_Nombre_Referencia,
         V_Referencia_Codigo2,
         V_Medida,
         V_Estado;

    "Prod_Dist_Desc" = TRIM(V_Nombre_Referencia);
    "Prod_Prov_Cod" = IIF(TRIM(COALESCE(V_Referencia_Codigo2, '')) = '', TRIM(COALESCE(V_Referencia, '')), TRIM(COALESCE(V_Referencia_Codigo2, '')));
    "Prd_UnidadInventario" = TRIM(V_Medida);

    "Inventario_Estado" = 'ACTIVO';
    IF (V_Estado = 'N') THEN
      "Inventario_Estado" = 'INACTIVO';

    -- Saldos
    "Cant_Disponible" = V_Saldo;
    "Cant_Stock" = V_Saldo;

    -- Costos
    V_Costo = (SELECT Ponderado
               FROM Fn_Ponderado(:V_Referencia, :Hasta_)) * :V_Saldo;

    "Costo_Disponible" = V_Costo;
    "Costo_Stock" = V_Costo;

    -- Bodega
    SELECT B.Nombre
    FROM Bodegas B
    WHERE B.Codigo = :V_Bodega
    INTO V_Nombre_Bodega;

    "Bodega_Cod" = TRIM(V_Bodega);
    "Bodega_Desc" = TRIM(V_Nombre_Bodega);

    SUSPEND;
  END

END^


CREATE OR ALTER PROCEDURE PZ_AUTOM_COSMOS_VENTAS (
    DESDE_ DATE,
    HASTA_ DATE,
    GRUPO_LINEA_ VARCHAR(20))
RETURNS (
    "Nit_Distribuidor" VARCHAR(15),
    "Factura_Numero" VARCHAR(15),
    "Factura_Fecha_Movimiento" VARCHAR(19),
    "Prod_Dist_Cod" VARCHAR(20),
    "Prod_Dist_Desc" VARCHAR(80),
    "Prod_Prov_Cod" VARCHAR(20),
    "Cant_Vendida" DOUBLE PRECISION,
    "Prod_UnidadVenta" VARCHAR(5),
    "Precio_Unit" DOUBLE PRECISION,
    "Precio_Total" DOUBLE PRECISION,
    "Costo" DOUBLE PRECISION,
    "Descuento" DOUBLE PRECISION,
    "Transaccion_Codigo" CHAR(2),
    "Transaccion_Descripccion" VARCHAR(10),
    "CausalTransaccion_Codigo" CHAR(1),
    "CausalTransaccion_Descripccion" CHAR(1),
    "Vend_Cedula" VARCHAR(15),
    "Vend_Nombre" VARCHAR(83),
    "Vend_Tel" VARCHAR(50),
    "Vend_Dir" VARCHAR(80),
    "Cliente_Cod" VARCHAR(15),
    "Cliente_Nombre" VARCHAR(220),
    "Cliente_Dir" VARCHAR(80),
    "Cliente_Tel" VARCHAR(50),
    "Cliente_FechaCrea" CHAR(1),
    "Cliente_FechaDesact" CHAR(1),
    "Cliente_Periodicidad" CHAR(1),
    "Cliente_RutaCod" VARCHAR(6),
    "Cliente_RutaDesc" VARCHAR(80),
    "Cliente_MunCod" VARCHAR(6),
    "Cliente_MunDesc" VARCHAR(80),
    "Sup_Cedula" CHAR(15),
    "Sup_Nombre" CHAR(83),
    "Sup_Dir" CHAR(80),
    "Sup_Tel" CHAR(50),
    "Cliente_CanalCod" VARCHAR(10),
    "Cliente_CanalDesc" VARCHAR(80),
    "Cliente_DeptoCod" VARCHAR(2),
    "Cliente_DeptoDesc" VARCHAR(80),
    "Prov_Cod" VARCHAR(15),
    "Prov_Desc" VARCHAR(163),
    "Cliente_SectorCod" CHAR(1),
    "Cliente_SectorDesc" CHAR(1),
    "Factura_Estado" CHAR(1),
    "PlanComercial_Cod" CHAR(1),
    "PlanComercial_Desc" CHAR(1),
    "Cliente_SubCanalCod" CHAR(1),
    "Cliente_SubCanalDesc" CHAR(1),
    "Agencia_Cod" VARCHAR(15),
    "Agencia_Desc" VARCHAR(50),
    "Bodega_Cod" VARCHAR(5),
    "Bodega_Desc" VARCHAR(80),
    "Cliente_GPS_Lat" CHAR(1),
    "Cliente_GPS_Long" CHAR(1),
    "Factura_GPS_Lat" CHAR(1),
    "Factura_GPS_Long" CHAR(1),
    "Cliente_Barrio" VARCHAR(20),
    "Cliente_Contacto_Cedula" CHAR(1),
    "Cliente_Contacto_Nombre" CHAR(1),
    "Ventas_FechaGeneracion" VARCHAR(19))
AS
DECLARE VARIABLE V_Emp_Nit VARCHAR(15);
DECLARE VARIABLE V_Emp_Codigo VARCHAR(15);
DECLARE VARIABLE V_Emp_Razon VARCHAR(50);
DECLARE VARIABLE V_Prefijo VARCHAR(5);
DECLARE VARIABLE V_Numero VARCHAR(10);
DECLARE VARIABLE V_Fecha DATE;
DECLARE VARIABLE V_Codigo2 VARCHAR(20);
DECLARE VARIABLE V_Salida DOUBLE PRECISION;
DECLARE VARIABLE V_Razon VARCHAR(50);
DECLARE VARIABLE V_Nombre_Ter VARCHAR(163);
DECLARE VARIABLE V_Prod_Dist_Cod VARCHAR(20);
DECLARE VARIABLE V_Prod_Dist_Desc VARCHAR(80);
DECLARE VARIABLE V_Cant_Vendida DOUBLE PRECISION;
DECLARE VARIABLE V_Precio_Total DOUBLE PRECISION;
DECLARE VARIABLE V_Descuento DOUBLE PRECISION;
DECLARE VARIABLE V_Vend_Cedula VARCHAR(15);
DECLARE VARIABLE V_Vend_Nombre VARCHAR(83);
DECLARE VARIABLE V_Cliente_Cod VARCHAR(15);
DECLARE VARIABLE V_Cliente_Muncod VARCHAR(6);
DECLARE VARIABLE V_Cliente_Mundesc VARCHAR(80);
DECLARE VARIABLE V_Bodega_Cod VARCHAR(5);
DECLARE VARIABLE V_Bodega_Desc VARCHAR(80);
DECLARE VARIABLE V_Factura_Numero VARCHAR(15);
DECLARE VARIABLE V_Prod_Unidadventa VARCHAR(5);
DECLARE VARIABLE V_Vend_Tel VARCHAR(50);
DECLARE VARIABLE V_Vend_Dir VARCHAR(80);
DECLARE VARIABLE V_Cliente_Dir VARCHAR(80);
DECLARE VARIABLE V_Cliente_Tel VARCHAR(50);
DECLARE VARIABLE V_Cliente_Rutacod VARCHAR(6);
DECLARE VARIABLE V_Cliente_Rutadesc VARCHAR(80);
DECLARE VARIABLE V_Cliente_Canalcod VARCHAR(10);
DECLARE VARIABLE V_Cliente_Canaldesc VARCHAR(80);
DECLARE VARIABLE V_Cliente_Barrio VARCHAR(20);
DECLARE VARIABLE V_Cliente_Deptocod VARCHAR(2);
DECLARE VARIABLE V_Cliente_Deptodesc VARCHAR(80);
DECLARE VARIABLE V_Prod_Prov_Cod VARCHAR(20);
DECLARE VARIABLE V_Precio_Unit DOUBLE PRECISION;
DECLARE VARIABLE V_Costo DOUBLE PRECISION;
BEGIN
  SELECT TRIM(Codigo) Codigo,
         IIF(TRIM(COALESCE(Razon_Comercial, '')) = '', Empresa, Razon_Comercial)
  FROM Terceros
  WHERE Datos_Empresa = 'S'
  INTO V_Emp_Nit,
       V_Emp_Razon;

  "Sup_Cedula" = '';
  "Sup_Nombre" = '';
  "Sup_Dir" = '';
  "Sup_Tel" = '';
  "Prov_Cod" = '890900161';
  "Cliente_SectorCod" = '';
  "Cliente_SectorDesc" = '';
  "Factura_Estado" = '0';
  "PlanComercial_Cod" = '';
  "PlanComercial_Desc" = '';
  "Cliente_SubCanalCod" = '';
  "Cliente_SubCanalDesc" = '';
  "CausalTransaccion_Codigo" = '';
  "CausalTransaccion_Descripccion" = '';
  "Cliente_FechaCrea" = '';
  "Cliente_FechaDesact" = '';
  "Cliente_Periodicidad" = '';
  "Cliente_GPS_Lat" = '';
  "Cliente_GPS_Long" = '';
  "Factura_GPS_Lat" = '';
  "Factura_GPS_Long" = '';
  "Cliente_Contacto_Cedula" = '';
  "Cliente_Contacto_Nombre" = '';
  "Nit_Distribuidor" = TRIM(V_Emp_Nit);
  "Agencia_Cod" = TRIM(V_Emp_Nit);
  "Agencia_Desc" = TRIM(V_Emp_Razon);

  FOR SELECT DISTINCT Prefijo,
                      Numero,
                      Fecha,
                      Referencia,
                      Nombre_Referencia,
                      ABS(Cantidad),
                      Bruto + ABS(Devolucion),
                      ABS(Descuento),
                      Salida,
                      Vendedor,
                      Nombre_Vendedor,
                      Tercero,
                      Nombre_Tercero,
                      Municipio,
                      Nombre_Municipio,
                      Bodega,
                      Nombre_Bodega
      FROM Fx_Dinamico(:Desde_, :Hasta_, 'VENTA', '%', '%', '%', '%', '%', '%', '%', '%', '%', '%', '%', 'S', 'N', 'N', 'S', 'S', 'N', 'N', 'S', 'S', 'S', 'N', 'N', 'N')
      WHERE Linea IN (SELECT Codigo
                      FROM Lineas
                      WHERE Grupo = :Grupo_Linea_)
      INTO V_Prefijo,
           V_Numero,
           V_Fecha,
           V_Prod_Dist_Cod,
           V_Prod_Dist_Desc,
           V_Cant_Vendida,
           V_Precio_Total,
           V_Descuento,
           V_Salida,
           V_Vend_Cedula,
           V_Vend_Nombre,
           V_Cliente_Cod,
           V_Nombre_Ter,
           V_Cliente_Muncod,
           V_Cliente_Mundesc,
           V_Bodega_Cod,
           V_Bodega_Desc
  DO
  BEGIN
    IF (TRIM(V_Prefijo) = '_') THEN
      V_Factura_Numero = V_Numero;
    ELSE
      V_Factura_Numero = TRIM(V_Prefijo) || TRIM(V_Numero);

    --Referencias
    SELECT Codigo2,
           Codmedida
    FROM Referencias
    WHERE Codigo = :V_Prod_Dist_Cod
    INTO V_Codigo2,
         V_Prod_Unidadventa;

    --Personal
    SELECT IIF(TRIM(COALESCE(Telefono, '')) = '', TRIM(COALESCE(Movil, '')), IIF(TRIM(COALESCE(Movil, '')) = '', TRIM(COALESCE(Telefono, '')), TRIM(Telefono) || ' - ' || TRIM(Movil))) ,
           COALESCE(Direccion, '')
    FROM Personal
    WHERE Codigo = :V_Vend_Cedula
    INTO V_Vend_Tel,
         V_Vend_Dir;

    --Supervisor
    SELECT FIRST 1 Codigo,
           Nombre,
           IIF(TRIM(COALESCE(Telefono, '')) = '', TRIM(COALESCE(Movil, '')), IIF(TRIM(COALESCE(Movil, '')) = '', TRIM(COALESCE(Telefono, '')), TRIM(Telefono) || ' - ' || TRIM(Movil))),
           COALESCE(Direccion, '') Direccion
    FROM Personal
    WHERE Codigo = Codigo2
    INTO "Sup_Cedula",
         "Sup_Nombre",
         "Sup_Tel",
         "Sup_Dir";

    --Terceros
    SELECT Razon_Comercial,
           COALESCE(Direccion, ''),
           IIF(TRIM(COALESCE(Telefono, '')) = '', TRIM(COALESCE(Movil, '')), IIF(TRIM(COALESCE(Movil, '')) = '', TRIM(COALESCE(Telefono, '')), TRIM(Telefono) || ' - ' || TRIM(Movil))) Telefono,
           TRIM(COALESCE(Codzona, '')),
           COALESCE(Codtipologia, ''),
           COALESCE(Barrio, '')
    FROM Terceros
    WHERE Codigo = :V_Cliente_Cod
    INTO V_Razon,
         V_Cliente_Dir,
         V_Cliente_Tel,
         V_Cliente_Rutacod,
         V_Cliente_Canalcod,
         V_Cliente_Barrio;

    --Tercero Familia
    SELECT TRIM(Nombre)
    FROM Terceros
    WHERE Codigo = :"Prov_Cod"
    INTO "Prov_Desc";

    --Zonas
    SELECT TRIM(Nombre)
    FROM Zonas
    WHERE Codigo = :V_Cliente_Rutacod
    INTO V_Cliente_Rutadesc;

    --Tipologia
    SELECT Nombre
    FROM Tipologias
    WHERE Codigo = :V_Cliente_Canalcod
    INTO V_Cliente_Canaldesc;
    V_Cliente_Canaldesc = COALESCE(V_Cliente_Canaldesc, '');

    --Departamento
    V_Cliente_Deptocod = SUBSTRING(V_Cliente_Muncod FROM 1 FOR 2);
    SELECT Nombre
    FROM Municipios
    WHERE Codigo = :V_Cliente_Deptocod
    INTO V_Cliente_Deptodesc;

    V_Cliente_Rutacod = TRIM(V_Cliente_Rutacod);

    IF (COALESCE(V_Codigo2, '') = '') THEN
      V_Prod_Prov_Cod = V_Prod_Dist_Cod;
    ELSE
      V_Prod_Prov_Cod = V_Codigo2;

    IF (V_Cant_Vendida = 0) THEN
      V_Precio_Unit = 0;
    ELSE
      V_Precio_Unit = V_Precio_Total / V_Cant_Vendida;

    SELECT Ponderado * :V_Cant_Vendida
    FROM Fn_Ponderado(:V_Prod_Dist_Cod, :Hasta_)
    INTO V_Costo;

    IF (V_Salida > 0) THEN
      "Transaccion_Codigo" = 'VT';
    ELSE
      "Transaccion_Codigo" = 'DV';

    IF (V_Salida > 0) THEN
      "Transaccion_Descripccion" = 'VENTA';
    ELSE
      "Transaccion_Descripccion" = 'DEVOLUCION';

    IF (COALESCE(V_Razon, '') = '') THEN
      "Cliente_Nombre" = TRIM(V_Nombre_Ter);
    ELSE
      "Cliente_Nombre" = TRIM(V_Nombre_Ter) || '-' || TRIM(COALESCE(V_Razon, ''));

    IF (POSITION('-', V_Cliente_Mundesc) > 0) THEN
      V_Cliente_Mundesc = SUBSTRING(V_Cliente_Mundesc FROM 1 FOR POSITION('-' IN V_Cliente_Mundesc) - 1);
    ELSE
      V_Cliente_Mundesc = TRIM(V_Cliente_Mundesc);

    "Prod_Dist_Cod" = TRIM(V_Prod_Dist_Cod);
    "Prod_Dist_Desc" = TRIM(V_Prod_Dist_Desc);
    "Cant_Vendida" = V_Cant_Vendida;
    "Precio_Total" = V_Precio_Total;
    "Descuento" = V_Descuento;
    "Vend_Cedula" = TRIM(V_Vend_Cedula);
    "Vend_Nombre" = TRIM(V_Vend_Nombre);
    "Cliente_Cod" = TRIM(V_Cliente_Cod);
    "Cliente_MunCod" = TRIM(V_Cliente_Muncod);
    "Cliente_MunDesc" = TRIM(V_Cliente_Mundesc);
    "Bodega_Cod" = TRIM(V_Bodega_Cod);
    "Bodega_Desc" = TRIM(V_Bodega_Desc);
    "Factura_Numero" = TRIM(V_Factura_Numero);
    "Factura_Fecha_Movimiento" = EXTRACT(YEAR FROM V_Fecha) || '-' || LPAD(EXTRACT(MONTH FROM V_Fecha), 2, '0') || '-' || LPAD(EXTRACT(DAY FROM V_Fecha), 2, '0') || ' 00:00:00';
    "Prod_UnidadVenta" = TRIM(V_Prod_Unidadventa);
    "Vend_Tel" = TRIM(V_Vend_Tel);
    "Vend_Dir" = TRIM(V_Vend_Dir);
    "Cliente_Dir" = TRIM(V_Cliente_Dir);
    "Cliente_Tel" = TRIM(V_Cliente_Tel);
    "Cliente_RutaCod" = TRIM(V_Cliente_Rutacod);
    "Cliente_CanalCod" = TRIM(V_Cliente_Canalcod);
    "Cliente_Barrio" = TRIM(V_Cliente_Barrio);
    "Cliente_RutaDesc" = TRIM(V_Cliente_Rutadesc);
    "Cliente_CanalDesc" = TRIM(V_Cliente_Canaldesc);
    "Cliente_DeptoCod" = TRIM(V_Cliente_Deptocod);
    "Cliente_DeptoDesc" = TRIM(V_Cliente_Deptodesc);
    "Prod_Prov_Cod" = TRIM(V_Prod_Prov_Cod);
    "Precio_Unit" = V_Precio_Unit;
    "Costo" = V_Costo;

    "Ventas_FechaGeneracion" = EXTRACT(YEAR FROM CURRENT_DATE) || '-' || LPAD(EXTRACT(MONTH FROM CURRENT_DATE), 2, '0') || '-' || LPAD(EXTRACT(DAY FROM CURRENT_DATE), 2, '0') || ' ' || SUBSTRING(CURRENT_TIME FROM 1 FOR 8);

    SUSPEND;
  END

END^



SET TERM ; ^




