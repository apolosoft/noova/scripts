UPDATE OR INSERT INTO INFORMES (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, FORMATO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD, DESTINO_FTP, CODREPORTE)
                        VALUES ('MA567_015', 'SALDOS INVENTARIO DE PRODUCTOS POR UNA BODEGA - FECHA CORTE - CATEGORIAS - V 8.0', NULL, 'SELECT I.Referencia,
       R.Nombre2,
       R.Codigo2 AS "CODIGO CORTO",
       R.Subpartida AS "COLOR",
       R.Ubicacion Marca,
       I.Entradas,
       I.Salidas,
       I.Ponderado,
       I.Ultimo,
       I.Nombre_Referencia,
       L.Nombre AS Nombre_Linea,
       (I.Entradas - I.Salidas) AS Saldo
FROM Fx_Inventario_Bodega_Fecha(:Fecha_Corte, :Bodega) I
JOIN Referencias R ON (I.Referencia = R.Codigo)
JOIN Lineas L ON (R.Codlinea = L.Codigo)
GROUP BY 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11', 'N', 'GESTION', :h00000000_00000000, 'N', 'SALDOS DE INVENTARIO', 'N', 'LIBRE', 5, 'N', 1, NULL, NULL)
                      MATCHING (CODIGO);

COMMIT WORK;

UPDATE OR INSERT INTO INFORMES (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, FORMATO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD, DESTINO_FTP, CODREPORTE)
                        VALUES ('MA567_016', 'INVENTARIO VALORIZADO AGRUPADO POR UBICACION', NULL, 'SELECT P.Referencia,
       R.Nombre AS "NOMBRE REFERENCIA",
       R.codigo2 AS "CODIGO CORTO",
       R.subpartida AS "COLOR",
       R.Codlinea,
       L.Nombre AS "NOMBRE LINEA",
       P.Bodega,
       B.Nombre AS "NOMBRE BODEGA",
       P1.Ultimo_Costo AS "ULTIMO COSTO",
       P.Ponderado,
       P.Saldo,
       P.Valor,
       R.Ubicacion
FROM (SELECT P.Ponderado,
             P.Bodega,
             P.Referencia,
             SUM(P.Entradas - P.Salidas) AS Saldo,
             SUM(P.Otras_Entradas - P.Otras_Salidas) AS Otro_Saldo,
             SUM((P.Entradas - P.Salidas) * P.Ponderado) AS Valor
      FROM Fx_Inventario_Fecha(:Fecha_Corte) P
      INNER JOIN Referencias R1 ON (P.Referencia = R1.Codigo)
      WHERE R1.Saldos = ''S''
      GROUP BY Bodega, Referencia, Ponderado
      HAVING SUM(P.Entradas - P.Salidas) <> 0) P
INNER JOIN(SELECT Referencia,
                  Ultimo_Costo,
                  Ultimo_Ponderado
           FROM Fx_Ponderados_Lista_Fecha(:Fecha_Corte)) P1 ON (P1.Referencia = P.Referencia)
INNER JOIN Referencias R ON (R.Codigo = P.Referencia)
INNER JOIN Lineas L ON (R.Codlinea = L.Codigo)
INNER JOIN Bodegas B ON (P.Bodega = B.Codigo)
ORDER BY 5, 3, 2', 'N', 'GESTION', :h00000000_00000000, 'N', 'SALDOS DE INVENTARIO', 'N', 'LIBRE', 5, 'N', 1, NULL, NULL)
                      MATCHING (CODIGO);


COMMIT WORK;
