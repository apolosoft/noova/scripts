/*INFORME PERSONALIZADO COMPRAS Y VENTAS*/

UPDATE OR INSERT INTO INFORMES (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, FORMATO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD, DESTINO_FTP, CODREPORTE)
                        VALUES ('CLG404_01', 'ANALISIS COMPARATIVO ULTIMA COMPRA Y VENTA (REF. A HOY, PONDERADO, SALDO, MARCA)', 'Total entradas y total salidas por cada referencia, con promedio, ultimo ponderado, precio de ultima compra y venta, saldo de inventario y marca', 'SELECT pl.referencia,
       pl.nombre,
       pl.ultimo_costo as Ultima_compra,
       (SELECT bruto
        FROM pz_ultima_venta(pl.referencia, :_hasta)) AS Ultima_venta,
       COALESCE(r.costo, '''') AS costo,
       COALESCE(r.precio, '''') AS precio,        
        pl.ultimo_ponderado AS Ponderado,
       (pl.entradas - pl.salidas) AS saldo,
       COALESCE(m.nombre, '''') AS marca,
       pl.entradas,
       pl.salidas,
       pl.desde,
       pl.hasta
FROM fx_ponderados_lista_rango(:_desde, :_hasta) pl
LEFT JOIN referencias r
      ON (pl.referencia = r.codigo)
LEFT JOIN marcas m
      ON (r.codmarca = m.codigo)', 'N', 'GESTION', :NULL, 'N', 'PERSONALIZADOS', 'N', 'LIBRE', 5, 'N', 1, NULL, NULL)
                      MATCHING (CODIGO);


COMMIT WORK;