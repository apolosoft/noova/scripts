UPDATE OR INSERT INTO INFORMES (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD, DESTINO_FTP, CODREPORTE)
                        VALUES ('LF787_012', 'TRANSFERENCIA NOMINA CUENTA BANCO AGRARIO', 'TRANSFERENCIA NOMINA CUENTA BANCO AGRARIO', 'SELECT LPAD(Banco, 4, ''00'') || LPAD(Beneficiario, 15, '' '') || RPAD(Tipo_Identificacion, 1, '''') || LPAD(COALESCE(Cuenta, '' ''), 17, '' '') || Tipo_Cuenta || RPAD(Nombre_Beneficiario, 30, '' '') || LPAD(ROUND(Valor), 12, 0) || ''.00'' || RPAD(Referencia, 42) Plano
FROM (SELECT TRIM(P.Codpersonal) Beneficiario,
             TRIM(E.Nombre) Nombre_Beneficiario,
             TRIM(T.Codidentidad) Tipo_Identificacion,
             TRIM(E.Codentidad) Banco,
             REPLACE(TRIM(E.Banco), ''-'', '''') Cuenta,
             IIF(E.Codtipocuenta = ''A'', ''4'', ''3'') AS Tipo_Cuenta,
             SUM(ROUND(P.Adicion)) Valor,
             TRIM(N.Nombre) Referencia
      FROM Planillas P
      JOIN Personal E ON (P.Codpersonal = E.Codigo)
      JOIN Terceros T ON (T.Codigo = E.Codigo)
      JOIN Nominas N ON (P.Codnomina = N.Codigo)
      JOIN Rubros R ON (P.Codrubro = R.Codigo)
      WHERE P.Codnomina = :Nomina
            AND R.Codconjunto = ''T_PAGAR''
            AND COALESCE(TRIM(E.Banco), '''') <> ''''
            AND E.Codentidad = ''40''
      GROUP BY 1, 2, 3, 4, 5, 6, 8)', 'N', 'NOMINA', 'N', 'PAGOS', 'N', 'LIBRE', 5, 'N', 1, NULL, NULL)
                      MATCHING (CODIGO);

COMMIT WORK;

/* Informe personalizado con base a CONTABLE3E, se realiza cambio en la nota del juego de inventario por nombre de la cuenta. */
UPDATE OR INSERT INTO INFORMES (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, FORMATO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD, DESTINO_FTP, CODREPORTE)
                        VALUES ('LF787_013', 'INTERFACE DE ERP - EXPORTA A TXT PARA SUBIR POR RUTINA PLANO - RANGO FECHA', 'Permite en un rango de fechas mes, exportar todo el movimiento contable a archivo plano TXT, para luego subirlo automáticamente a cualquier empresa o contabilidad mediante la rutina Plano a Contable', 'SELECT C.Tipo,
       C.Prefijo,
       C.Numero,
       '''' AS Secuencia,
       C1.Fecha,
       C.Codcuenta AS Cuenta,
       C.Codtercero AS Tercero,
       C.Codcentro AS Centro,
       C.Nota AS Detalle,
       C.Debito,
       C.Credito,
       C.Base,
       ''N'' AS Aplica,
       C.Tiporef AS Tipoanexo,
       C.Prefijoref AS Prefijoanexo,
       C.Numeroref AS Numeroanexo,
       ''SUPERVISOR'' AS Usuario,
       '''' AS Signo,
       '''' AS Cuentacobrar,
       '''' AS Cuentapagar,
       (SELECT Nombre_Tercero
        FROM Fn_Nombre_Tercero(C.Codtercero)) AS "NOMBRE TERCERO",
       (SELECT Nombre_Centro
        FROM Fn_Nombre_Centro(C.Codcentro)) AS "NOMBRE CENTRO",
       CURRENT_DATE AS Interfaceno
FROM Comovi C
INNER JOIN Cocomp C1 ON ((C.Tipo = C1.Tipo) AND (C.Prefijo = C1.Prefijo) AND (C.Numero = C1.Numero))
WHERE TRIM(C.Codcuenta) <> ''''
      AND ((C1.Fecha >= :Fecha_Desde)
      AND (C1.Fecha <= :Fecha_Hasta))

UNION ALL

SELECT C.Tipo,
       C.Prefijo,
       C.Numero,
       '''' AS Secuencia,
       C.Fecha,
       C.Codcuenta AS Cuenta,
       C.Codtercero AS Tercero,
       C.Codcentro AS Centro,
       C.Detalle,
       C.Debito,
       C.Credito,
       C.Base,
       ''N'' AS Aplica,
       '''' AS Tipoanexo,
       '''' AS Prefijoanexo,
       '''' AS Numeroanexo,
       ''SUPERVISOR'' AS Usuario,
       '''' AS Signo,
       '''' AS Cuentacobrar,
       '''' AS Cuentapagar,
       (SELECT Nombre_Tercero
        FROM Fn_Nombre_Tercero(C.Codtercero)) AS "NOMBRE TERCERO",
       (SELECT Nombre_Centro
        FROM Fn_Nombre_Centro(C.Codcentro)) AS "NOMBRE CENTRO",
       CURRENT_DATE AS Interfaceno
FROM Reg_Contable C
INNER JOIN Comprobantes C1 ON ((C.Tipo = C1.Tipo) AND (C.Prefijo = C1.Prefijo) AND (C.Numero = C1.Numero))
WHERE TRIM(C.Codcuenta) <> ''''
      AND ((C.Fecha >= :Fecha_Desde)
      AND (C.Fecha <= :Fecha_Hasta))

UNION ALL

SELECT C.Tipo,
       C.Prefijo,
       C.Numero,
       '''' AS Secuencia,
       C.Fecha,
       C.Codcuenta AS Cuenta,
       C.Codtercero AS Tercero,
       C.Codcentro AS Centro,
       (SELECT TRIM(Nombre_Cuenta)
        FROM Fn_Nombre_Cuenta(C.Codcuenta)) AS Detalle,
       C.Debito,
       C.Credito,
       C.Base,
       ''N'' AS Aplica,
       '''' AS Tipoanexo,
       '''' AS Prefijoanexo,
       '''' AS Numeroanexo,
       ''SUPERVISOR'' AS Usuario,
       '''' AS Signo,
       '''' AS Cuentacobrar,
       '''' AS Cuentapagar,
       (SELECT Nombre_Tercero
        FROM Fn_Nombre_Tercero(C.Codtercero)) AS "NOMBRE TERCERO",
       (SELECT Nombre_Centro
        FROM Fn_Nombre_Centro(C.Codcentro)) AS "NOMBRE CENTRO",
       CURRENT_DATE AS Interfaceno
FROM Reg_Juego C
INNER JOIN Comprobantes C1 ON ((C.Tipo = C1.Tipo) AND (C.Prefijo = C1.Prefijo) AND (C.Numero = C1.Numero))
WHERE TRIM(C.Codcuenta) <> ''''
      AND ((C1.Fecha >= :Fecha_Desde)
      AND (C1.Fecha <= :Fecha_Hasta))

UNION ALL

SELECT C.Tipo,
       C.Prefijo,
       C.Codnomina,
       '''' AS Secuencia,
       C1.Fecha,
       C.Codcuenta AS Cuenta,
       C.Codtercero AS Tercero,
       C.Codcentro AS Centro,
       C.Detalle,
       C.Debito,
       C.Credito,
       C.Base,
       ''N'' AS Aplica,
       '''' AS Tipoanexo,
       '''' AS Prefijoanexo,
       '''' AS Numeroanexo,
       ''SUPERVISOR'' AS Usuario,
       '''' AS Signo,
       '''' AS Cuentacobrar,
       '''' AS Cuentapagar,
       (SELECT TRIM(Nombre_Tercero)
        FROM Fn_Nombre_Tercero(C.Codtercero)) AS "NOMBRE TERCERO",
       (SELECT Nombre_Centro
        FROM Fn_Nombre_Centro(C.Codcentro)) AS "NOMBRE CENTRO",
       CURRENT_DATE AS Interfaceno
FROM Reg_Nomina C
INNER JOIN Nominas C1 ON (C.Codnomina = C1.Codigo)
WHERE TRIM(C.Codcuenta) <> ''''
      AND ((C1.Fecha >= :Fecha_Desde)
      AND (C1.Fecha <= :Fecha_Hasta))
ORDER BY 1,2,3', 'N', 'GESTION', NULL, 'N', 'RUTINAS MIGRACIONES', 'N', 'LIBRE', 5, 'N', 1, NULL, NULL)
                      MATCHING (CODIGO);


COMMIT WORK;

/* PILA ALADINO */

SET TERM ^ ;

CREATE OR ALTER PROCEDURE Pz_Pila_02
RETURNS (
    C_01 CHAR(2),
    C_02 INTEGER,
    C_03 CHAR(2),
    C_04 CHAR(16),
    C_05 CHAR(2),
    C_06 CHAR(2),
    C_07 CHAR(1),
    C_08 CHAR(1),
    C_09 CHAR(5),
    C_10 CHAR(5),
    C_11 CHAR(20),
    C_12 CHAR(30),
    C_13 CHAR(20),
    C_14 CHAR(30),
    C_15 CHAR(1),
    C_16 CHAR(1),
    C_17 CHAR(1),
    C_18 CHAR(1),
    C_19 CHAR(1),
    C_20 CHAR(1),
    C_21 CHAR(1),
    C_22 CHAR(1),
    C_23 CHAR(1),
    C_24 CHAR(1),
    C_25 CHAR(1),
    C_26 CHAR(1),
    C_27 CHAR(1),
    C_28 CHAR(1),
    C_29 CHAR(1),
    C_30 INTEGER,
    C_31 CHAR(6),
    C_32 CHAR(6),
    C_33 CHAR(6),
    C_34 CHAR(6),
    C_35 CHAR(6),
    C_36 DOUBLE PRECISION,
    C_37 DOUBLE PRECISION,
    C_38 DOUBLE PRECISION,
    C_39 DOUBLE PRECISION,
    C_40 DOUBLE PRECISION,
    C_41 CHAR(1),
    C_42 DOUBLE PRECISION,
    C_43 DOUBLE PRECISION,
    C_44 DOUBLE PRECISION,
    C_45 DOUBLE PRECISION,
    C_46 VARCHAR(7),
    C_47 DOUBLE PRECISION,
    C_48 DOUBLE PRECISION,
    C_49 DOUBLE PRECISION,
    C_50 DOUBLE PRECISION,
    C_51 DOUBLE PRECISION,
    C_52 DOUBLE PRECISION,
    C_53 DOUBLE PRECISION,
    C_54 VARCHAR(7),
    C_55 DOUBLE PRECISION,
    C_56 DOUBLE PRECISION,
    C_57 CHAR(15),
    C_58 DOUBLE PRECISION,
    C_59 CHAR(15),
    C_60 DOUBLE PRECISION,
    C_61 VARCHAR(9),
    C_62 CHAR(10),
    C_63 DOUBLE PRECISION,
    C_64 VARCHAR(7),
    C_65 DOUBLE PRECISION,
    C_66 VARCHAR(7),
    C_67 DOUBLE PRECISION,
    C_68 VARCHAR(7),
    C_69 DOUBLE PRECISION,
    C_70 VARCHAR(7),
    C_71 DOUBLE PRECISION,
    C_72 VARCHAR(7),
    C_73 DOUBLE PRECISION,
    C_74 CHAR(2),
    C_75 CHAR(16),
    C_76 CHAR(1),
    C_77 CHAR(6),
    C_78 CHAR(1),
    C_79 CHAR(1),
    C_80 CHAR(10),
    C_81 CHAR(10),
    C_82 CHAR(10),
    C_83 CHAR(10),
    C_84 CHAR(10),
    C_85 CHAR(10),
    C_86 CHAR(10),
    C_87 CHAR(10),
    C_88 CHAR(10),
    C_89 CHAR(10),
    C_90 CHAR(10),
    C_91 CHAR(10),
    C_92 CHAR(10),
    C_93 CHAR(10),
    C_94 CHAR(10),
    C_95 DOUBLE PRECISION,
    C_96 DOUBLE PRECISION,
    C_97 CHAR(10),
    C_98 CHAR(7))
AS
DECLARE VARIABLE Tipo          CHAR(2);
DECLARE VARIABLE Codpersonal   CHAR(15);
DECLARE VARIABLE V_Sena        CHAR(1);
DECLARE VARIABLE V_Pensionado  CHAR(1);
DECLARE VARIABLE V_Parcial     CHAR(1);
DECLARE VARIABLE V_Lectivo     CHAR(1);
DECLARE VARIABLE V_Ciudad      VARCHAR(5);
DECLARE VARIABLE V_Exonerado   CHAR(1);
DECLARE VARIABLE V_Unico       CHAR(1);
DECLARE VARIABLE V_Alto_Riesgo CHAR(1);
DECLARE VARIABLE T_36          DOUBLE PRECISION;
DECLARE VARIABLE T_37          DOUBLE PRECISION;
DECLARE VARIABLE T_38          DOUBLE PRECISION;
DECLARE VARIABLE V_C_54        DOUBLE PRECISION;
DECLARE VARIABLE V_C_64        DOUBLE PRECISION;
DECLARE VARIABLE V_C_66        DOUBLE PRECISION;
DECLARE VARIABLE V_C_68        DOUBLE PRECISION;
DECLARE VARIABLE V_C_70        DOUBLE PRECISION;
DECLARE VARIABLE V_C_72        DOUBLE PRECISION;
DECLARE VARIABLE V_C_61        DOUBLE PRECISION;
DECLARE VARIABLE V_C_46        DOUBLE PRECISION;
DECLARE VARIABLE T_39          DOUBLE PRECISION;
DECLARE VARIABLE V_C_80        DATE;
DECLARE VARIABLE V_C_81        DATE;
DECLARE VARIABLE V_C_82        DATE;
DECLARE VARIABLE V_C_83        DATE;
DECLARE VARIABLE V_C_84        DATE;
DECLARE VARIABLE V_C_85        DATE;
DECLARE VARIABLE V_C_86        DATE;
DECLARE VARIABLE V_C_87        DATE;
DECLARE VARIABLE V_C_88        DATE;
DECLARE VARIABLE V_C_89        DATE;
DECLARE VARIABLE V_C_90        DATE;
DECLARE VARIABLE V_C_91        DATE;
DECLARE VARIABLE V_C_92        DATE;
DECLARE VARIABLE V_C_93        DATE;
DECLARE VARIABLE V_C_94        DATE;
DECLARE VARIABLE C_00          DOUBLE PRECISION;
DECLARE VARIABLE B_00          DOUBLE PRECISION;
DECLARE VARIABLE B_01          DOUBLE PRECISION;
DECLARE VARIABLE B_02          DOUBLE PRECISION;
DECLARE VARIABLE B_03          DOUBLE PRECISION;
DECLARE VARIABLE B_04          DOUBLE PRECISION;
DECLARE VARIABLE B_05          DOUBLE PRECISION;
DECLARE VARIABLE B_06          DOUBLE PRECISION;
DECLARE VARIABLE B_07          DOUBLE PRECISION;
DECLARE VARIABLE B_08          DOUBLE PRECISION;
DECLARE VARIABLE B_09          DOUBLE PRECISION;
DECLARE VARIABLE B_10          DOUBLE PRECISION;
DECLARE VARIABLE B_11          DOUBLE PRECISION;
DECLARE VARIABLE B_12          DOUBLE PRECISION;
DECLARE VARIABLE B_13          DOUBLE PRECISION;
DECLARE VARIABLE B_14          DOUBLE PRECISION;
DECLARE VARIABLE B_15          DOUBLE PRECISION;
DECLARE VARIABLE B_16          DOUBLE PRECISION;
DECLARE VARIABLE B_17          DOUBLE PRECISION;
DECLARE VARIABLE B_18          DOUBLE PRECISION;
DECLARE VARIABLE B_19          DOUBLE PRECISION;
DECLARE VARIABLE V_Actividad   CHAR(5);
DECLARE VARIABLE V_Actividad_C CHAR(5);
DECLARE VARIABLE V_Actividad_T CHAR(5);
DECLARE VARIABLE V_Subp        INTEGER;
DECLARE VARIABLE V_Subpl       INTEGER;
DECLARE VARIABLE V_Desde       DATE;
BEGIN
  SELECT TRIM(Codmunicipio), TRIM(Codactividad)
  FROM Terceros
  WHERE Datos_Empresa = 'S'
  INTO V_Ciudad, V_Actividad_T;

  FOR SELECT Desde, Tipo, Codpersonal, C_00, C_01, C_02, C_03, C_04, C_05, C_06,
             C_07, C_08, C_09, C_10, C_11, C_12, C_13, C_14, C_15, C_16, C_17,
             C_18, C_19, C_20, C_21, C_22, C_23, C_24, C_25, C_26, C_27, C_28,
             C_29, C_30, C_31, C_32, C_33, C_34, C_35, C_36, C_37, C_38, C_39,
             C_40, C_41, C_42, C_43, C_44, C_45, C_46, C_47, C_48, C_49, C_50,
             C_51, C_52, C_53, C_54, C_55, C_56, C_57, C_58, C_59, C_60, C_61,
             C_62, C_63, C_64, C_65, C_66, C_67, C_68, C_69, C_70, C_71, C_72,
             C_73, C_74, C_75, C_76, C_77, C_78, C_79, C_80, C_81, C_82, C_83,
             C_84, C_85, C_86, C_87, C_88, C_89, C_90, C_91, C_92, C_93, C_94,
             C_95, C_96, B_00, B_01, B_02, B_03, B_04, B_05, B_06, B_07, B_08,
             B_09, B_10, B_11, B_12, B_13, B_14, B_15, B_16, B_17, B_18, B_19
      FROM Pila
      WHERE C_36 + C_37 + C_38 + C_39 > 0
      INTO V_Desde, Tipo, Codpersonal, C_00, C_01, C_02, C_03, C_04, C_05, C_06,
           C_07, C_08, C_09, C_10, C_11, C_12, C_13, C_14, C_15, C_16, C_17,
           C_18, C_19, C_20, C_21, C_22, C_23, C_24, C_25, C_26, C_27, C_28,
           C_29, C_30, C_31, C_32, C_33, C_34, C_35, C_36, C_37, C_38, C_39,
           C_40, C_41, C_42, C_43, C_44, C_45, V_C_46, C_47, C_48, C_49, C_50,
           C_51, C_52, C_53, V_C_54, C_55, C_56, C_57, C_58, C_59, C_60, V_C_61,
           C_62, C_63, V_C_64, C_65, V_C_66, C_67, V_C_68, C_69, V_C_70, C_71,
           V_C_72, C_73, C_74, C_75, C_76, C_77, C_78, C_79, V_C_80, V_C_81,
           V_C_82, V_C_83, V_C_84, V_C_85, V_C_86, V_C_87, V_C_88, V_C_89,
           V_C_90, V_C_91, V_C_92, V_C_93, V_C_94, C_95, C_96, B_00, B_01, B_02,
           B_03, B_04, B_05, B_06, B_07, B_08, B_09, B_10, B_11, B_12, B_13,
           B_14, B_15, B_16, B_17, B_18, B_19
  DO
  BEGIN

    -- Datos de Personal
    SELECT Alto_Riesgo
    FROM Personal
    WHERE Codigo = :Codpersonal
    INTO V_Alto_Riesgo;
    V_Alto_Riesgo = COALESCE(V_Alto_Riesgo, ' ');

    -- Constantes Global (Actividad)
    V_Actividad = V_Actividad_T;
    V_Subp = NULL;
    V_Actividad_c = NULL;
    SELECT Valor, TRIM(Texto)
    FROM Constantesvalor
    WHERE Codconstante = 'C45_SUBP'
          AND Ano = EXTRACT(YEAR FROM :V_Desde)
    INTO V_Subp, V_Actividad_c;
    V_Subp = COALESCE(V_Subp, 0);
    IF (TRIM(COALESCE(V_Actividad_c, '')) <> '') THEN
      V_Actividad = V_Actividad_c;

    -- Constante local
    V_Subpl = NULL;
    V_Actividad_C = NULL;
    SELECT Valor, TRIM(Texto)
    FROM Constantes_Personal
    WHERE Codconstante = 'C45_SUBP'
          AND Ano = EXTRACT(YEAR FROM :V_Desde)
          AND Codpersonal = :Codpersonal
    INTO V_Subpl, V_Actividad_C;
    V_Subp = COALESCE(V_Subpl, V_Subp);
    IF (TRIM(COALESCE(V_Actividad_C, '')) <> '') THEN
      V_Actividad = V_Actividad_C;

    -- Totales
    SELECT SUM(C_36), SUM(C_37), SUM(C_38), SUM(C_39)
    FROM Pila
    WHERE Codpersonal = :Codpersonal
    INTO T_36, T_37, T_38, T_39;

    -- C_05 Tipo Empleado
    IF (TRIM(C_05) = '') THEN
      C_05 = '1';
    ELSE
      C_05 = TRIM(C_05);

    -- Sena
    V_Sena = 'N';
    IF (C_05 IN ('12', '19')) THEN
      V_Sena = 'S';

    -- Tiempo parcial
    V_Parcial = 'N';
    IF (C_05 IN ('51', '68')) THEN
      V_Parcial = 'S';

    -- Sena Lectivo
    V_Lectivo = 'N';
    IF (C_05 = '12') THEN
      V_Lectivo = 'S';

    -- Pensionado
    V_Pensionado = 'N';
    IF (C_06 > 0) THEN
      V_Pensionado = 'S';

    -- Unico Registro
    V_Unico = 'N';
    IF (C_36 = T_36) THEN
      V_Unico = 'S';

    -- Exonerado salud, SENA e ICBF
    V_Exonerado = 'N';
    IF (C_76 = 'S') THEN
      V_Exonerado = 'S';

    IF (V_Sena = 'S') THEN
      C_09 = V_Ciudad;

    IF (C_27 = 'X' AND
        B_17 > 0) THEN
      C_27 = 'L';

    C_28 = '';
    IF (C_48 > 0) THEN
      C_28 = 'X';

    -- C_30 IRL: Dias de incapacidad por accidente de trabajo o enfermedad laboral.
    IF (C_30 <> C_38) THEN
      C_30 = 0;
    C_30 = CAST(C_30 AS CHAR(2));

    C_32 = COALESCE(C_32, '');
    C_33 = COALESCE(C_33, '');
    C_34 = COALESCE(C_34, '');
    C_35 = COALESCE(C_35, '');

    -- C_36 Numero de dias cotizados a pension
    IF (V_Sena = 'S' OR V_Pensionado = 'S') THEN
      C_36 = 0;
    ELSE
    IF (C_36 > 0) THEN
      IF (V_C_46 = 0) THEN
        C_36 = 0;
      ELSE
      IF (V_Parcial = 'S') THEN
        C_36 = B_14;
      ELSE
        C_36 = C_36;
    ELSE
      C_36 = 0;
    C_36 = CAST(ROUND(C_36) AS CHAR(2));

    -- C_37 N?mero de d?as cotizados a salud
    IF (C_37 > 0) THEN
    BEGIN
      IF (V_Parcial = 'S') THEN
        C_37 = B_15;
    END
    ELSE
      C_37 = 0;
    C_37 = CAST(ROUND(C_37) AS CHAR(2));

    -- C_38 N?mero de d?as cotizados a Riesgos Laborales
    IF (C_38 > 0) THEN
    BEGIN
      IF (V_Parcial = 'S') THEN
        C_38 = 30;
      ELSE
      IF (V_Lectivo = 'S') THEN
        C_38 = 0;
    END
    ELSE
      C_38 = 0;
    C_38 = CAST(ROUND(C_38) AS CHAR(2));

    -- C_39 N?mero de d?as cotizados a Caja de Compensaci?n Familiar
    IF (V_Sena = 'S') THEN
      C_39 = 0;
    ELSE
    IF (C_39 > 0) THEN
    BEGIN
      IF (V_Parcial = 'S') THEN
        C_39 = B_16;
    END                                               
    ELSE
      C_39 = 0;
    C_39 = CAST(ROUND(C_39) AS CHAR(2));

    C_40 = CAST(ROUND(C_40) AS CHAR(9));

    -- C_41
    IF (V_Sena = 'S' OR (V_Parcial = 'S')) THEN
      C_41 = '';
    ELSE
    IF (TRIM(COALESCE(C_41, '')) = '') THEN
      C_41 = 'F';

    -- C_42 IBC pensi?n
    IF (V_Sena = 'S' OR V_Pensionado = 'S') THEN
      C_42 = 0;
    ELSE
    IF  ((C_42+ B_19) > (C_00 * 25)) THEN
       C_42 = C_42;   
    ELSE
    IF (V_Unico = 'S') THEN
      C_42 = C_42 + B_19;
    ELSE
    IF (T_36 <> (B_12 - B_17)) THEN
      IF (Tipo = 'RU') THEN
        C_42 = IIF(C_40 < C_00, C_00, C_40) / 30 * C_36 + B_19;
      ELSE
        C_42 = (IIF(C_40 < C_00, C_00, C_40) / 30 * C_36) ;
    ELSE
      IF (Tipo = 'RU') THEN
        C_42 = IIF(C_40 < C_00, C_00, C_40) / 30 * C_36 + B_19;
      ELSE
      C_42 = C_40 / 30 * C_36;
    C_42 = CAST(ROUND(CEILING(ROUND(C_42, 2))) AS CHAR(9));

    -- C_43 IBC salud

    IF  ((C_43+ B_19) > (C_00 * 25)) THEN
       C_43 = C_43;   
    ELSE
    IF (V_Unico = 'S') THEN
      C_43 = C_43 + B_19;
    ELSE
    IF (T_37 <> (B_12 - B_17)) THEN
      IF (Tipo = 'RU') THEN
        C_43 = (IIF(C_40 < C_00, C_00, C_40) / 30 * C_37) + B_19;
      ELSE
        C_43 = (IIF(C_40 < C_00, C_00, C_40) / 30 * C_37) ;
    ELSE
      IF (Tipo = 'RU') THEN
        C_43 = (IIF(C_40 < C_00, C_00, C_40) / 30 * C_37) + B_19;
      ELSE
        C_43 = C_40 / 30 * C_37;
    C_43 = CAST(ROUND(CEILING(ROUND(C_43, 2))) AS CHAR(9));

    -- C_44 IBC Riesgos Laborales
    IF (V_Lectivo = 'S') THEN
      C_44 = 0;
    ELSE
    IF  ((C_44+ B_19) > (C_00 * 25)) THEN
       C_44 = C_44;   
    ELSE
    IF (V_Unico = 'S') THEN
      C_44 = C_44 + B_19;
    ELSE
    IF (T_36 <> (B_12 - B_17)) THEN
      IF (Tipo = 'RU') THEN
        C_44 = (IIF(C_40 < C_00, C_00, C_40) / 30 * C_38) + B_19;
      ELSE
       C_44 = (IIF(C_40 < C_00, C_00, C_40) / 30 * C_38) ;
    ELSE
      IF (Tipo = 'RU') THEN
        C_44 = (IIF(C_40 < C_00, C_00, C_40) / 30 * C_38) + B_19;
      ELSE
        C_44 = C_40 / 30 * C_38;
    C_44 = CAST(ROUND(CEILING(ROUND(C_44, 2))) AS CHAR(9));

     -- C_45 IBC CCF
    IF (V_Sena = 'S') THEN
      C_45 = 0;
    ELSE
    IF (V_Unico = 'S') THEN
      C_45 = C_45 + B_19 + B_18;
    ELSE
    IF (T_36 <> (B_12 - B_17)) THEN
      IF (Tipo = 'RU') THEN
        C_45 = (IIF(C_40 < C_00, C_00, C_40) / 30 * C_39) + B_19 + B_18;
      ELSE
        C_45 = (C_45 - (IIF(C_40 < C_00, C_00, C_40) / 30 * (B_12 - B_17))) / (T_39 - (B_12 - B_17)) * C_39;
    ELSE
      IF (Tipo = 'RU') THEN
        C_45 = (IIF(C_40 < C_00, C_00, C_40) / 30 * C_39) + B_19 + B_18;
      ELSE
        C_45 = IIF(C_40 < C_00, C_00, C_40) / 30 * C_39;
    C_45 = CAST(ROUND(CEILING(ROUND(C_45, 2))) AS CHAR(9));

    -- C_46 Tarifa de aportes pensiones,
    IF (V_Sena = 'S' OR V_Pensionado = 'S') THEN
      V_C_46 = 0;
    ELSE
    IF (C_24 = 'X') THEN
      V_C_46 = B_00 / 100;
    ELSE
      V_C_46 = V_C_46 / 100;
    C_46 = CAST(V_C_46 AS CHAR(7));

    -- C_47 Cotizaci?n a pensiones
    C_47 = CAST(ROUND((SELECT Tope
                       FROM Sys_Redondea(:C_42 * :V_C_46, 100))) AS CHAR(9));

    -- C_48
    C_48 = CAST(ROUND((SELECT Tope
                       FROM Sys_Redondea(:C_48, 100))) AS CHAR(9));

    -- C_49
    C_48 = CAST(ROUND((SELECT Tope
                       FROM Sys_Redondea(:C_49, 100))) AS CHAR(9));

    -- C_50 Total cotizaci?n Sistema General de Pensiones
    C_50 = CAST(ROUND(C_47 + C_48 + C_49) AS CHAR(9));

    -- C_51 Aportes a fondo de solidaridad pensional
    IF (C_40 + B_19 < 4 * C_00) THEN
      C_51 = 0;
    ELSE
    IF (C_24 = 'X') THEN
      C_51 = 0;
    ELSE
    IF (C_36 = T_36) THEN
      C_51 = C_42;
    ELSE
      C_51 = C_42;
    IF (C_51 <> 0) THEN
    BEGIN
      C_51 = (SELECT Tope
              FROM Sys_Redondea(ROUND((CEILING(:C_51) * :B_02 / 100) / 2), 100));
    END
    C_51 = CAST(ROUND(C_51) AS CHAR(9));

    -- C_52 Aportes a subcuenta de subsistencia.
    C_52 = CAST(ROUND((SELECT Tope
                       FROM Sys_Redondea(:C_51 + (:C_52 / :T_36 * :C_36), 100))) AS CHAR(9));

    -- C_53
    C_53 = CAST(ROUND((SELECT Tope
                       FROM Sys_Redondea(:C_53, 100))) AS CHAR(9));

    -- C_76 Cotizante exonerado de pago de aporte salud, SENA e ICBF
    IF (V_Sena = 'S' OR V_Parcial = 'S') THEN
      C_76 = 'N';
    ELSE
    IF ((C_40 + B_19) >= C_00 * 10) THEN
      C_76 = 'N';

    -- C_54 Tarifa salud
    V_C_54 = CASE
               WHEN V_Parcial = 'S' THEN 0.00
               WHEN V_Sena = 'S' THEN V_C_54
               WHEN C_40 + B_19 >= C_00 * 10 THEN V_C_54
               WHEN C_24 = 'X' AND C_76 = 'S' THEN 0.00
               WHEN C_24 = 'X' AND C_76 = 'N' THEN B_01
               WHEN C_40 + B_19 >= C_00 * 10 AND C_24 = 'X' THEN B_01
               WHEN C_40 + B_19 >= C_00 * 10 THEN V_C_54
               WHEN V_Exonerado = 'S' THEN B_04
               ELSE V_C_54
             END;
    C_54 = CAST(V_C_54 / 100 AS CHAR(7));

    -- C_55 Cotizaci?n salud
    C_55 = CAST(ROUND((SELECT Tope
                       FROM Sys_Redondea(:C_43 * :V_C_54 / 100, 100))) AS CHAR(9));

    -- C_56
    C_56 = CAST(ROUND((SELECT Tope
                       FROM Sys_Redondea(:C_56, 100))) AS CHAR(9));

    C_58 = CAST(ROUND(:C_58) AS CHAR(9));
    C_60 = CAST(ROUND(:C_60) AS CHAR(9));

    -- C_61 Tarifa de aportes a Riesgos Laborales
    IF (V_Lectivo = 'S') THEN
      V_C_61 = 0;
    ELSE
    IF ((C_24 = 'X' OR C_25 = 'X' OR C_26 = 'X' OR C_27 = 'X' OR C_27 = 'L' OR C_30 > 0) AND
        (Tipo <> 'RU')) THEN
      V_C_61 = 0;
    ELSE
      V_C_61 = V_C_61 / 100;
    C_61 = CAST(V_C_61 AS CHAR(9));

    C_62 = CAST(C_62 AS CHAR(9));

    -- C_63 Cotizacion Riesgos Laborales
    C_63 = CAST(ROUND((SELECT Tope
                       FROM Sys_Redondea(:C_44 * :V_C_61, 100))) AS CHAR(9));

    -- C_64 Tarifa de aportes CCF
    IF (V_Sena = 'S') THEN
      V_C_64 = 0;
    ELSE
    IF ((C_24 = 'X' OR C_25 = 'X' OR C_26 = 'X' OR C_30 > 0) AND
        (Tipo <> 'RU')) THEN
      V_C_64 = 0;
    ELSE
      V_C_64 = V_C_64 / 100;
    C_64 = CAST(V_C_64 AS CHAR(7));

    -- C_65 Valor aporte CCF
    C_65 = CAST(ROUND((SELECT Tope
                       FROM Sys_Redondea(:C_45 * :V_C_64, 100))) AS CHAR(9));

    -- C_95 IBC Otros parafiscales
    IF (C_69 = 0 OR C_24 = 'X' OR C_25 = 'X') THEN
      C_95 = 0;
    ELSE
    IF (V_Sena = 'S' OR V_Parcial = 'S') THEN
      C_95 = 0;
    ELSE                     
    IF  ((C_95+ B_19 + B_18) > (C_00 * 25)) THEN
       C_95 = C_95;   
    ELSE
    IF (V_Unico = 'S') THEN
      C_95 = C_95 + B_19 + B_18;
    ELSE
    IF (Tipo = 'RU') THEN
      C_95 = (IIF(C_40 < C_00, C_00, C_40) / T_36 * C_36) + B_19 + B_18 + B_13;
    ELSE
      C_95 = IIF(C_40 < C_00, C_00, C_40) / T_36 * C_36;
    C_95 = CAST(ROUND(CEILING(ROUND(C_95))) AS CHAR(9));

    -- C_66 Tarifa de aportes SENA
    IF (C_95 = 0 OR C_24 = 'X' OR C_25 = 'X' OR C_26 = 'X' OR C_30 > 0 OR V_Sena = 'S' OR V_Parcial = 'S') THEN
      V_C_66 = 0;
    ELSE
      V_C_66 = V_C_66 / 100;
    C_66 = CAST(V_C_66 AS CHAR(7));

    -- C_67 Valor aportes SENA
    C_67 = CAST(ROUND((SELECT Tope
                       FROM Sys_Redondea(:C_95 * :V_C_66, 100))) AS CHAR(9));

    -- C_68 Tarifa aportes ICBF
    IF (C_95 = 0 OR C_24 = 'X' OR C_25 = 'X' OR C_26 = 'X' OR C_30 > 0 OR V_Sena = 'S' OR V_Parcial = 'S') THEN
      V_C_68 = 0;
    ELSE
      V_C_68 = V_C_68 / 100;
    C_68 = CAST(V_C_68 AS CHAR(7));

    -- C_69 Valor aporte ICBF
    C_69 = CAST(ROUND((SELECT Tope
                       FROM Sys_Redondea(:C_95 * :V_C_68, 100))) AS CHAR(9));

    -- C_70 Tarifa aportes ESAP
    V_C_70 = V_C_70 / 100;
    C_70 = CAST(V_C_70 AS CHAR(7));

    -- C_71 Valor aporte ESAP
    C_71 = CAST(ROUND(C_71) AS CHAR(9));

    -- C_72 Tarifa aportes MEN
    V_C_72 = V_C_72 / 100;
    C_72 = CAST(V_C_72 AS CHAR(7));

    -- C_73 Valor aporte MEN
    C_73 = CAST(ROUND(C_73) AS CHAR(9));

    -- C_77 Codigo ARL
    IF (V_Lectivo = 'S') THEN
      C_77 = '';
    ELSE
      C_77 = COALESCE(C_77, '');

    -- C_78 Clase de riesgo
    IF (V_Lectivo = 'S') THEN
      C_78 = 0;
    --   ELSE
    --    IF ((C_24 = 'X' OR C_25 = 'X' OR C_26 = 'X' OR C_27 = 'X' OR C_30 > 0) AND
    --        (Tipo <> 'RU')) THEN
    --     C_78 = 0;

    -- C_79 tarifa especial pensiones

    IF (V_Alto_Riesgo = 'S') THEN
      C_79 = '1';
    ELSE
      C_79 = ' ';

    -- C_80 Fecha de ingreso
    C_80 = EXTRACT(YEAR FROM V_C_80) || '-' || SUBSTRING(EXTRACT(MONTH FROM V_C_80) + 100 FROM 2) || '-' || SUBSTRING(EXTRACT(DAY FROM V_C_80) + 100 FROM 2);

    -- C_81 Fecha de retiro
    C_81 = EXTRACT(YEAR FROM V_C_81) || '-' || SUBSTRING(EXTRACT(MONTH FROM V_C_81) + 100 FROM 2) || '-' || SUBSTRING(EXTRACT(DAY FROM V_C_81) + 100 FROM 2);

    -- C_82 Fecha de retiro
    C_82 = EXTRACT(YEAR FROM V_C_82) || '-' || SUBSTRING(EXTRACT(MONTH FROM V_C_82) + 100 FROM 2) || '-' || SUBSTRING(EXTRACT(DAY FROM V_C_82) + 100 FROM 2);

    -- C_83 Fecha Inicio SLN
    C_83 = EXTRACT(YEAR FROM V_C_83) || '-' || SUBSTRING(EXTRACT(MONTH FROM V_C_83) + 100 FROM 2) || '-' || SUBSTRING(EXTRACT(DAY FROM V_C_83) + 100 FROM 2);

    -- C_84 Fecha fin SLN
    C_84 = EXTRACT(YEAR FROM V_C_84) || '-' || SUBSTRING(EXTRACT(MONTH FROM V_C_84) + 100 FROM 2) || '-' || SUBSTRING(EXTRACT(DAY FROM V_C_84) + 100 FROM 2);

    -- C_85 Fecha inicio IGE
    C_85 = EXTRACT(YEAR FROM V_C_85) || '-' || SUBSTRING(EXTRACT(MONTH FROM V_C_85) + 100 FROM 2) || '-' || SUBSTRING(EXTRACT(DAY FROM V_C_85) + 100 FROM 2);

    -- C_86 Fecha fin IGE
    C_86 = EXTRACT(YEAR FROM V_C_86) || '-' || SUBSTRING(EXTRACT(MONTH FROM V_C_86) + 100 FROM 2) || '-' || SUBSTRING(EXTRACT(DAY FROM V_C_86) + 100 FROM 2);

    -- C_87 Fecha inicio LMA
    C_87 = EXTRACT(YEAR FROM V_C_87) || '-' || SUBSTRING(EXTRACT(MONTH FROM V_C_87) + 100 FROM 2) || '-' || SUBSTRING(EXTRACT(DAY FROM V_C_87) + 100 FROM 2);

    -- C_88 Fecha fin LMA
    C_88 = EXTRACT(YEAR FROM V_C_88) || '-' || SUBSTRING(EXTRACT(MONTH FROM V_C_88) + 100 FROM 2) || '-' || SUBSTRING(EXTRACT(DAY FROM V_C_88) + 100 FROM 2);

    -- C_89 Fecha inicio VAC - LR
    C_89 = EXTRACT(YEAR FROM V_C_89) || '-' || SUBSTRING(EXTRACT(MONTH FROM V_C_89) + 100 FROM 2) || '-' || SUBSTRING(EXTRACT(DAY FROM V_C_89) + 100 FROM 2);

    -- C_90 Fecha fin VAC - LR
    C_90 = EXTRACT(YEAR FROM V_C_90) || '-' || SUBSTRING(EXTRACT(MONTH FROM V_C_90) + 100 FROM 2) || '-' || SUBSTRING(EXTRACT(DAY FROM V_C_90) + 100 FROM 2);

    -- C_91 Fecha inicio VCT
    C_91 = EXTRACT(YEAR FROM V_C_91) || '-' || SUBSTRING(EXTRACT(MONTH FROM V_C_91) + 100 FROM 2) || '-' || SUBSTRING(EXTRACT(DAY FROM V_C_91) + 100 FROM 2);

    -- C_92 Fecha fin VCT
    C_92 = EXTRACT(YEAR FROM V_C_92) || '-' || SUBSTRING(EXTRACT(MONTH FROM V_C_92) + 100 FROM 2) || '-' || SUBSTRING(EXTRACT(DAY FROM V_C_92) + 100 FROM 2);

    -- C_93 Fecha inicio IRL
    C_93 = EXTRACT(YEAR FROM V_C_93) || '-' || SUBSTRING(EXTRACT(MONTH FROM V_C_93) + 100 FROM 2) || '-' || SUBSTRING(EXTRACT(DAY FROM V_C_93) + 100 FROM 2);

    -- C_94 Fecha fin IRL
    C_94 = EXTRACT(YEAR FROM V_C_94) || '-' || SUBSTRING(EXTRACT(MONTH FROM V_C_94) + 100 FROM 2) || '-' || SUBSTRING(EXTRACT(DAY FROM V_C_94) + 100 FROM 2);

    -- C_96 Numero de horas laboradas
    C_96 = ROUND(C_37 * 8);

    C_97 = ' ';

    -- C_98 Sub Actividad
    IF (V_Lectivo = 'S') THEN
      C_98 = '0000000';
    ELSE
      C_98 = C_78 || SUBSTRING(V_Actividad FROM 1 FOR 4) || SUBSTRING(CAST(V_Subp + 100 AS CHAR(3)) FROM 2 FOR 2);

    SUSPEND;
  END

END^

SET TERM ; ^

COMMIT WORK;
