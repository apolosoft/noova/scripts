UPDATE OR INSERT INTO REPORTES (CODIGO, NOMBRE, NOTA, ARCHIVO, ADJUNTO, NATIVO, MODULO, PARAMETROS, PUBLICAR, CODGRUPO, VALIDA_TRANSITO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD)
                        VALUES ('MA894_009', 'SALDOS INVENTARIO CON UNIDAD DE MEDIDA', NULL, 'MA894_009.FR3', NULL, 'N', 'GESTION', 'FECHA_CORTE,BODEGA,LINEA', 'N', 'SALDOS DE INVENTARIO', 'N', 'N', 'LIBRE', 5, 'N', 1)
                      MATCHING (CODIGO);

COMMIT WORK;

UPDATE OR INSERT INTO Reportes (Codigo, Nombre, Nota, Archivo, Adjunto, Nativo,
                                Modulo, Parametros, Publicar, Codgrupo,
                                Valida_Transito, Rango, Categoria, Nivel,
                                Tablero, Prioridad)
VALUES ('MA894_009', 'SALDOS INVENTARIO CON UNIDAD DE MEDIDA', NULL,
        'MA894_009.FR3', NULL, 'N', 'GESTION', 'FECHA_CORTE,BODEGA,LINEA', 'N',
        'SALDOS DE INVENTARIO', 'N', 'N', 'LIBRE', 5, 'N', 1)
MATCHING (Codigo);

COMMIT WORK;

UPDATE OR INSERT INTO Reportes (Codigo, Nombre, Nota, Archivo, Adjunto, Nativo,
                                Modulo, Parametros, Publicar, Codgrupo,
                                Valida_Transito, Rango, Categoria, Nivel,
                                Tablero, Prioridad)
VALUES ('MA894_001', 'MOVIMIENTO FE AUXILIAR Y SALDOS DE VENTAS - DINAMICO', 'Permite obtener todos los auxiliares requeridos de FE,segun parametros predefinidos en la region de filtro
Luego de Filtrar podra activar vista previa o imprimir En su defecto clic en Informe',
        'MA894_001.FR3', NULL, 'N', 'GESTION', NULL, 'N', 'SALDOS EN VENTAS',
        'N', 'N', 'AUXILIAR VENTAS', 5, 'N', 1)
MATCHING (Codigo);

COMMIT WORK;

UPDATE Documentos R
SET R.Formato = 'MA894_002.FR3'
WHERE UPPER(R.Formato) = UPPER('PERFV3INTERFE_QR.fr3');
UPDATE Documentos R
SET R.Formato1 = 'MA894_002.FR3'
WHERE UPPER(R.Formato1) = UPPER('PERFV3INTERFE_QR.fr3');
UPDATE Documentos R
SET R.Formato2 = 'MA894_002.FR3'
WHERE UPPER(R.Formato2) = UPPER('PERFV3INTERFE_QR.fr3');
UPDATE Documentos R
SET R.Formato3 = 'MA894_002.FR3'
WHERE UPPER(R.Formato3) = UPPER('PERFV3INTERFE_QR.fr3');

UPDATE Documentos R
SET R.Formato = 'MA894_003.FR3'
WHERE UPPER(R.Formato) IN (UPPER('PERFVINTERFE_QR.fr3'), UPPER('PERFVINTERFEqr.fr3'));
UPDATE Documentos R
SET R.Formato1 = 'MA894_003.FR3'
WHERE UPPER(R.Formato1) IN (UPPER('PERFVINTERFE_QR.fr3'), UPPER('PERFVINTERFEqr.fr3'));
UPDATE Documentos R
SET R.Formato2 = 'MA894_003.FR3'
WHERE UPPER(R.Formato2) IN (UPPER('PERFVINTERFE_QR.fr3'), UPPER('PERFVINTERFEqr.fr3'));
UPDATE Documentos R
SET R.Formato3 = 'MA894_003.FR3'
WHERE UPPER(R.Formato3) IN (UPPER('PERFVINTERFE_QR.fr3'), UPPER('PERFVINTERFEqr.fr3'));
