/*------------- Informe Comision vendedores -------------------*/ 

UPDATE OR INSERT INTO INFORMES (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD, DESTINO_FTP, CODREPORTE)
                        VALUES ('01COMIVEN', 'RELACION DE COMISION DE VENDEDORES', NULL, 'EXECUTE BLOCK (
    Fecha_Desde DATE = :Fecha_Desde,
    Fecha_Hasta DATE = :Fecha_Hasta)
RETURNS (
    Vtipo        CHAR(5),
    Vprefijo     CHAR(5),
    Vnumero      CHAR(10),
    Vfecha       DATE,
    Vtiporef     CHAR(5),
    Vprefijoref  CHAR(5),
    Vnumeroref   CHAR(10),
    Vfecha_Fv    DATE,
    Vvalor_Fv    NUMERIC(17,4),
    Vdias        INTEGER,
    Vtercero     CHAR(15),
    Vvendedor    CHAR(15),
    Vnomvendedor CHAR(83),
    Vbase_I      NUMERIC(17,4),
    Vingreso     NUMERIC(17,4),
    Vimp         NUMERIC(17,4),
    Vretencion   NUMERIC(17,4),
    Vpronto_Pago NUMERIC(17,4),
    Vbase_Com    NUMERIC(17,4),
    Vpor         NUMERIC(17,4),
    Vcomision    NUMERIC(17,4))
AS

DECLARE Vpor1  NUMERIC(17,4);
DECLARE Vpor2  NUMERIC(17,4);
DECLARE Vdias1 NUMERIC(17,4);
DECLARE Vdias2 NUMERIC(17,4);
DECLARE Vdias3 NUMERIC(17,4);
DECLARE Vpor3  NUMERIC(17,4);
BEGIN
  FOR SELECT Tipo,
             Prefijo,
             Numero,
             Fecha,
             Codtercero,
             Codvendedor
      FROM Comprobantes
      WHERE Tipo = ''RC''
            AND Fecha BETWEEN :Fecha_Desde AND :Fecha_Hasta
      ORDER BY 6, 5, 4, 1, 2, 3
      INTO Vtipo,
           Vprefijo,
           Vnumero,
           Vfecha,
           Vtercero,
           Vvendedor
  DO
  BEGIN
    -- Tesoreria
    FOR SELECT Tiporef,
               Prefijoref,
               Numeroref,
               SUM(Ingreso),
               SUM(Pronto_Pago)
        FROM Tr_Tesoreria

        WHERE Tipo = :Vtipo
              AND Prefijo = :Vprefijo
              AND Numero = :Vnumero
        GROUP BY 1, 2, 3
        INTO Vtiporef,
             Vprefijoref,
             Vnumeroref,
             Vingreso,
             Vpronto_Pago
    DO
    BEGIN
      -- Documento Principal
      SELECT Fecha,
             Total,
             Impuestos
      FROM Comprobantes
      WHERE Tipo = :Vtiporef
            AND Prefijo = :Vprefijoref
            AND Numero = :Vnumeroref
      INTO Vfecha_Fv,
           Vvalor_Fv,
           Vimp;

      -- Retenciones
      SELECT SUM(Debito)
      FROM Reg_Retenciones
      WHERE Tipo = :Vtipo
            AND Prefijo = :Vprefijo
            AND Numero = :Vnumero
            AND Tiporef = :Vtiporef
            AND Prefijoref = :Vprefijoref
            AND Numeroref = :Vnumeroref
      INTO Vretencion;

      -- Comisiones
      SELECT Co.Pventas,
             Co.Precaudos,
             Co.Nventas,
             Co.Nrecaudos,
             Co.Pdescuento,
             Co.Ndescuento
      FROM Comisiones Co
      WHERE Co.Codpersonal = :Vvendedor
            AND Co.Codlinea = ''COMI''
      INTO Vpor1,
           Vpor2,
           Vdias1,
           Vdias2,
           Vpor3,
           Vdias3;

      Vdias = :Vfecha - :Vfecha_Fv;

      IF (Vretencion IS NULL) THEN
        Vretencion = 0;
      IF (Vimp > 0) THEN
        Vbase_I = Vimp * 100 / 19;
      ELSE
        Vbase_I = Vvalor_Fv;
      Vbase_Com = Vingreso - Vimp - Vpronto_Pago - Vretencion;
      --  Vbase_Com = Vingreso / 1.19 - Vpronto_pago - Vretencion;

      IF (Vdias <= Vdias1) THEN
        Vpor = Vpor1;
      ELSE
      IF (Vdias <= Vdias2) THEN
        Vpor = Vpor2;
      ELSE
      IF (Vdias <= Vdias3) THEN
        Vpor = Vpor3;
      ELSE
        Vpor = 0;
      Vcomision = Vbase_Com * Vpor / 100;
      SUSPEND;
    END
  END
END', 'N', 'GESTION', 'N', 'SALDOS DE VENDEDORES', 'N', 'LIBRE', 5, 'N', 10, NULL, NULL)
                      MATCHING (CODIGO);


COMMIT WORK;

