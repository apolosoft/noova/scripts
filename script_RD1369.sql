UPDATE OR INSERT INTO INFORMES (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD, DESTINO_FTP, CODREPORTE)
                        VALUES ('RD1369_001', 'RELACION DE TESORERIA PARA PAGOS Y RECAUDOS CON RP POR TERCERO - RANGO FECHA', 'Relacion resumida de tesoreria, con todo el movimiento y la afectacion presupuestal por uno o todos los registros presupuestales rp, segun rango de fecha.', 'SELECT d.tesoreria,
       (SELECT TRIM(nombre_documento)
        FROM fn_nombre_documento(c.tipo)) AS "NOMBRE DOCUMENTO",
       TRIM(c.tipo) AS tipo,
       TRIM(c.prefijo) AS prefijo,
       CAST(c.numero AS INT) AS numero,
       TRIM(t.tiporef) AS tiporef,
       TRIM(t.prefijoref) AS prefijoref,
       CAST(t.numeroref AS INT) AS numeroref,
       c.fecha,
       c1.codrp,
       c.codtercero AS tercero,
       (SELECT TRIM(nombre_tercero)
        FROM fn_nombre_tercero(c.codtercero)) AS "NOMBRE TERCERO",
       t.abono,
       c.total,
       t.valor AS saldo_pendiente,
       tr.valor,
       t.documento,
       t.codbanco,
       tr.codp_ppto AS codrubro,
       TRIM(r.nombre) AS "REGISTRO PRESUPUESTAL",
       TRIM(p.nombre) AS "RUBRO PRESUPUESTAL",
       TRIM(cd.nombre) AS "CERTIFICADO DISPONIBILIDAD",
       tr.codcentro,
       TRIM(c2.nombre) AS "NOMBRE CENTRO",
       TRIM(b.nombre) AS caja_banco,
       c.codvendedor AS codordenador,
       (SELECT TRIM(nombre_empleado)
        FROM fn_nombre_empleado(c.codvendedor)) AS "NOMBRE ORDENADOR",
       c.codusuario,
       (SELECT TRIM(nombre_usuario)
        FROM fn_nombre_usuario(c.codusuario)) AS "NOMBRE USUARIO"
FROM tr_abonos t
INNER JOIN comprobantes c
      ON (c.tipo = t.tipo) AND
      (c.prefijo = t.prefijo) AND
      (c.numero = t.numero)
LEFT JOIN comprobantes c1
      ON (c1.tipo = t.tiporef) AND
      (c1.prefijo = t.prefijoref) AND
      (c1.numero = t.numeroref)
INNER JOIN documentos d
      ON (t.tipo = d.codigo)
INNER JOIN bancos b
      ON (t.codbanco = b.codigo)
JOIN tr_rp tr
      ON (c1.codrp = tr.codrp)
INNER JOIN rp r
      ON (tr.codrp = r.codigo)
INNER JOIN centros c2
      ON (tr.codcentro = c2.codigo)
INNER JOIN p_ppto p
      ON (tr.codp_ppto = p.codigo)
INNER JOIN cdp cd
      ON (r.codcdp = cd.codigo)
WHERE TRIM(t.procesar) = ''S'' AND
      ((c.fecha >= :_desde) AND
      (c.fecha <= :_hasta))
ORDER BY 2, 3, 4, 5, 6, 7, 8', 'N', 'GESTION', 'N', 'PERSONALIZADOS', 'N', 'LIBRE', 5, 'N', 1, NULL, NULL)
                      MATCHING (CODIGO);


COMMIT WORK;
