UPDATE OR INSERT INTO PARAMETROS (CODIGO, NOMBRE, PUNTERO)
                          VALUES ('REFERENCIA', 'PRODUCTO', 'REFERENCIAS')
                        MATCHING (CODIGO);


COMMIT WORK;

UPDATE OR INSERT INTO PARAMETROS (CODIGO, NOMBRE, PUNTERO)
                          VALUES ('SUCURSAL', 'PLACA', 'SUCURSALES')
                        MATCHING (CODIGO);


COMMIT WORK;

UPDATE OR INSERT INTO INFORMES (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD, DESTINO_FTP, CODREPORTE)
                        VALUES ('RB1367_001', 'INFORME DE VENTAS', 'INFORME DE VENTAS ESTACION DE SERVICIO DON JOSE', 'SELECT Fecha,
       TRIM(COALESCE(C.Nota, '''')) Kilometraje,
       TRIM(C.Tipo) || '' '' || TRIM(C.Numero) Factura,
       TRIM(R.Nombre) Producto,
       TRIM(COALESCE(Codsucursal, '''')) Placa,
       Salida Cantidad,
       Unitario Valor_Und,
       C.Total
FROM Comprobantes C
JOIN Tr_Inventario Tr ON C.Tipo = Tr.Tipo AND
      C.Prefijo = Tr.Prefijo AND
      C.Numero = Tr.Numero
JOIN Referencias R ON R.Codigo = Tr.Codreferencia
WHERE Fecha >= :Fecha_Desde
      AND Fecha <= :Fecha_Hasta
      AND C.Codtercero = :Cliente
      AND C.Codsucursal LIKE ''%'' || :Placa || ''%''
      AND R.Nombre LIKE ''%'' || :Producto || ''%''
ORDER BY 1', 'N', 'GESTION', 'N', 'INFORMES', 'N', 'LIBRE', 5, 'N', 1, NULL, NULL)
                      MATCHING (CODIGO);


COMMIT WORK;
