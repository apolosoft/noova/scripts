UPDATE OR INSERT INTO Importados_Modelo (Id, Id_Importado, Campo, Activo, Sql_Conversion)
VALUES (80, 0, 'NA_Identidad', 'S', 'SELECT IIF(TRIM(:Valor) = ''R'', ''31'', IIF(TRIM(:Valor) = ''C'', ''13'', ''31'')) AS Codidentidad
FROM Rdb$Database')
MATCHING (Id);
COMMIT WORK;

UPDATE OR INSERT INTO Importados_Modelo (Id, Id_Importado, Campo, Activo, Sql_Conversion)
VALUES (224, 0, 'NA_Codigo', 'S',
        'SELECT IIF(POSITION(''-'' IN :Valor) > 0, SUBSTRING(:Valor FROM POSITION(''-'' IN :Valor) + 1), '''') AS DV FROM Rdb$Database')
MATCHING (Id);
COMMIT WORK;

UPDATE OR INSERT INTO Importados_Modelo (Id, Id_Importado, Campo, Activo, Sql_Conversion)
VALUES (73, 0, 'NA_Codigo', 'S',
        'SELECT IIF(POSITION(''-'' IN :Valor) > 0, SUBSTRING(:Valor FROM 1 FOR POSITION(''-'' IN :Valor) - 1), :Valor) AS CODIGO FROM Rdb$Database')
MATCHING (Id);
COMMIT WORK;

UPDATE OR INSERT INTO Importados_Modelo (Id, Id_Importado, Campo, Activo, Sql_Conversion)
VALUES (79, 0, 'NA_Identidad', 'S', 'SELECT IIF(TRIM(:Valor) = ''R'', ''01'', IIF(TRIM(:Valor) = ''C'', ''01'', ''03'')) AS CODSOCIEDAD FROM Rdb$Database')
MATCHING (Id);
COMMIT WORK;

/*Sincronizador*/
UPDATE OR INSERT INTO IMPORTADOS (ID, DESCRIPCION, ARCHIVO, TABLA, ESQUEMA, SQL, ORDEN, ACTIVO, MODIFICAR)
                          VALUES (10, 'DESCUENTOS DE COMPROBANTES DE EGRESO LQFL1', 'CEFLEDTOLQFL1*.TXT', 'REG_RETENCIONES', '
Format=Delimited(;)
ColNameHeader=False
MaxScanRows=0
Col1=Tipo Char
Col2=Prefijo Char
Col3=NA_Numero Char
Col4=NA_CodConcepto Char
Col5=NA_Base Char
Col6=NA_Valor Char
Col7=Tiporef Char
Col8=Prefijoref Char
Col9=NA_Numeroref Char
Col10=NA_CodCentro Char
CharacterSet=ANSI', NULL, 11, 'S', 'N')
                        MATCHING (ID);

COMMIT WORK;

UPDATE OR INSERT INTO IMPORTADOS (ID, DESCRIPCION, ARCHIVO, TABLA, ESQUEMA, SQL, ORDEN, ACTIVO, MODIFICAR)
                          VALUES (33, 'DESCUENTOS DE COMPROBANTES DE EGRESO LQFLE', 'CEFLEDTOLQFLE*.TXT', 'REG_RETENCIONES', 'Format=Delimited(;)
ColNameHeader=False
MaxScanRows=0
Col1=Tipo Char
Col2=Prefijo Char
Col3=NA_Numero Char
Col4=NA_CodConcepto Char
Col5=NA_Base Char
Col6=NA_Valor Char
Col7=Tiporef Char
Col8=Prefijoref Char
Col9=NA_Numeroref Char
Col10=NA_CodCentro Char
CharacterSet=ANSI', NULL, 34, 'S', 'N')
                        MATCHING (ID);

COMMIT WORK;


UPDATE OR INSERT INTO IMPORTADOS_MODELO (ID, ID_IMPORTADO, CAMPO, ACTIVO, SQL_CONVERSION)
                                 VALUES (454, 10, 'NA_Numeroref', 'S', 'SELECT R.COD_ESQCONTABLE AS ESQ_CONTABLE
FROM TR_INVENTARIO T
INNER JOIN REFERENCIAS R ON (T.CODREFERENCIA=R.CODIGO)
WHERE T.TIPO=''LQFL1'' AND T.PREFIJO=''LQFE''  AND T.NUMERO=TRIM(:VALOR)')
                               MATCHING (ID);
UPDATE OR INSERT INTO IMPORTADOS_MODELO (ID, ID_IMPORTADO, CAMPO, ACTIVO, SQL_CONVERSION)
                                 VALUES (455, 10, 'NA_Valor', 'S', 'SELECT ''0'' AS DEBITO
FROM Rdb$Database')
                               MATCHING (ID);
UPDATE OR INSERT INTO IMPORTADOS_MODELO (ID, ID_IMPORTADO, CAMPO, ACTIVO, SQL_CONVERSION)
                                 VALUES (456, 10, 'NA_CodConcepto', 'S', 'SELECT ''S'' AS PUBLICAR
FROM Rdb$Database')
                               MATCHING (ID);
UPDATE OR INSERT INTO IMPORTADOS_MODELO (ID, ID_IMPORTADO, CAMPO, ACTIVO, SQL_CONVERSION)
                                 VALUES (457, 10, 'NA_CodConcepto', 'S', 'SELECT FIRST 1 ASUMIR
FROM TRIBUTARIA_RETENCIONES
WHERE CODTIPORETENCION=TRIM(:VALOR)')
                               MATCHING (ID);
UPDATE OR INSERT INTO IMPORTADOS_MODELO (ID, ID_IMPORTADO, CAMPO, ACTIVO, SQL_CONVERSION)
                                 VALUES (458, 10, 'NA_Base', 'S', 'SELECT IIF(TRIM(:Valor) = '''', ''0'', :Valor) AS BASE
FROM Rdb$Database')
                               MATCHING (ID);
UPDATE OR INSERT INTO IMPORTADOS_MODELO (ID, ID_IMPORTADO, CAMPO, ACTIVO, SQL_CONVERSION)
                                 VALUES (459, 10, 'NA_CodConcepto', 'S', 'SELECT ''S'' AS RETENCION_MANUAL
FROM Rdb$Database')
                               MATCHING (ID);
UPDATE OR INSERT INTO IMPORTADOS_MODELO (ID, ID_IMPORTADO, CAMPO, ACTIVO, SQL_CONVERSION)
                                 VALUES (460, 10, 'NA_Valor', 'S', 'SELECT IIF(TRIM(:Valor) = '''', ''0'', :Valor) AS CREDITO
FROM Rdb$Database')
                               MATCHING (ID);
UPDATE OR INSERT INTO IMPORTADOS_MODELO (ID, ID_IMPORTADO, CAMPO, ACTIVO, SQL_CONVERSION)
                                 VALUES (461, 10, 'NA_CodConcepto', 'S', 'SELECT IIF(TRIM(:Valor) = '''', ''NULO'', :Valor) AS TIPO_RETENCION
FROM Rdb$Database')
                               MATCHING (ID);
UPDATE OR INSERT INTO IMPORTADOS_MODELO (ID, ID_IMPORTADO, CAMPO, ACTIVO, SQL_CONVERSION)
                                 VALUES (462, 10, 'NA_CodConcepto', 'S', 'SELECT CODIGO_FE
FROM TIPORETENCIONES
WHERE CODIGO=TRIM(:VALOR)')
                               MATCHING (ID);
UPDATE OR INSERT INTO IMPORTADOS_MODELO (ID, ID_IMPORTADO, CAMPO, ACTIVO, SQL_CONVERSION)
                                 VALUES (463, 10, 'NA_Numero', 'S', 'SELECT Renglon
FROM TR_ABONOS
WHERE TIPO=''CEFLE'' AND PREFIJO=''_'' AND NUMERO=TRIM(:VALOR)')
                               MATCHING (ID);
UPDATE OR INSERT INTO IMPORTADOS_MODELO (ID, ID_IMPORTADO, CAMPO, ACTIVO, SQL_CONVERSION)
                                 VALUES (464, 10, 'NA_Numero', 'S', 'SELECT TRIM(:Valor) AS NUMERO
FROM Rdb$Database')
                               MATCHING (ID);
UPDATE OR INSERT INTO IMPORTADOS_MODELO (ID, ID_IMPORTADO, CAMPO, ACTIVO, SQL_CONVERSION)
                                 VALUES (465, 10, 'NA_CodConcepto', 'S', 'SELECT GEN_ID(GEN_REG_RETENCIONES_ID, 1) AS Item
FROM Rdb$Database')
                               MATCHING (ID);
UPDATE OR INSERT INTO IMPORTADOS_MODELO (ID, ID_IMPORTADO, CAMPO, ACTIVO, SQL_CONVERSION)
                                 VALUES (466, 10, 'NA_CodCentro', 'S', 'SELECT IIF(TRIM(:Valor) = '''', ''42011'', :Valor) AS CENTRO
FROM Rdb$Database')
                               MATCHING (ID);
UPDATE OR INSERT INTO IMPORTADOS_MODELO (ID, ID_IMPORTADO, CAMPO, ACTIVO, SQL_CONVERSION)
                                 VALUES (467, 10, 'NA_Numeroref', 'S', 'SELECT TRIM(:Valor) AS NUMEROREF
FROM Rdb$Database')
                               MATCHING (ID);

COMMIT WORK;

UPDATE OR INSERT INTO IMPORTADOS_MODELO (ID, ID_IMPORTADO, CAMPO, ACTIVO, SQL_CONVERSION)
                                 VALUES (445, 33, 'NA_CodConcepto', 'S', 'SELECT CODIGO_FE
FROM TIPORETENCIONES
WHERE CODIGO=TRIM(:VALOR)')
                               MATCHING (ID);
UPDATE OR INSERT INTO IMPORTADOS_MODELO (ID, ID_IMPORTADO, CAMPO, ACTIVO, SQL_CONVERSION)
                                 VALUES (307, 33, 'NA_Valor', 'S', 'SELECT IIF(TRIM(:Valor) = '''', ''0'', :Valor) AS CREDITO
FROM Rdb$Database')
                               MATCHING (ID);
UPDATE OR INSERT INTO IMPORTADOS_MODELO (ID, ID_IMPORTADO, CAMPO, ACTIVO, SQL_CONVERSION)
                                 VALUES (308, 33, 'NA_CodConcepto', 'S', 'SELECT IIF(TRIM(:Valor) = '''', ''NULO'', :Valor) AS TIPO_RETENCION
FROM Rdb$Database')
                               MATCHING (ID);
UPDATE OR INSERT INTO IMPORTADOS_MODELO (ID, ID_IMPORTADO, CAMPO, ACTIVO, SQL_CONVERSION)
                                 VALUES (137, 33, 'NA_Valor', 'S', 'SELECT ''0'' AS DEBITO
FROM Rdb$Database')
                               MATCHING (ID);
UPDATE OR INSERT INTO IMPORTADOS_MODELO (ID, ID_IMPORTADO, CAMPO, ACTIVO, SQL_CONVERSION)
                                 VALUES (138, 33, 'NA_CodConcepto', 'S', 'SELECT ''S'' AS PUBLICAR
FROM Rdb$Database')
                               MATCHING (ID);
UPDATE OR INSERT INTO IMPORTADOS_MODELO (ID, ID_IMPORTADO, CAMPO, ACTIVO, SQL_CONVERSION)
                                 VALUES (139, 33, 'NA_CodConcepto', 'S', 'SELECT FIRST 1 ASUMIR
FROM TRIBUTARIA_RETENCIONES
WHERE CODTIPORETENCION=TRIM(:VALOR)')
                               MATCHING (ID);
UPDATE OR INSERT INTO IMPORTADOS_MODELO (ID, ID_IMPORTADO, CAMPO, ACTIVO, SQL_CONVERSION)
                                 VALUES (140, 33, 'NA_Base', 'S', 'SELECT IIF(TRIM(:Valor) = '''', ''0'', :Valor) AS BASE
FROM Rdb$Database')
                               MATCHING (ID);
UPDATE OR INSERT INTO IMPORTADOS_MODELO (ID, ID_IMPORTADO, CAMPO, ACTIVO, SQL_CONVERSION)
                                 VALUES (141, 33, 'NA_CodConcepto', 'S', 'SELECT ''S'' AS RETENCION_MANUAL
FROM Rdb$Database')
                               MATCHING (ID);
UPDATE OR INSERT INTO IMPORTADOS_MODELO (ID, ID_IMPORTADO, CAMPO, ACTIVO, SQL_CONVERSION)
                                 VALUES (135, 33, 'NA_Numeroref', 'S', 'SELECT R.COD_ESQCONTABLE AS ESQ_CONTABLE
FROM TR_INVENTARIO T
INNER JOIN REFERENCIAS R ON (T.CODREFERENCIA=R.CODIGO)
WHERE T.TIPO=''LQFLE'' AND T.PREFIJO=''LQFL''  AND T.NUMERO=TRIM(:VALOR)')
                               MATCHING (ID);
UPDATE OR INSERT INTO IMPORTADOS_MODELO (ID, ID_IMPORTADO, CAMPO, ACTIVO, SQL_CONVERSION)
                                 VALUES (446, 33, 'NA_Numero', 'S', 'SELECT Renglon
FROM TR_ABONOS
WHERE TIPO=''CEFLE'' AND PREFIJO=''_'' AND NUMERO=TRIM(:VALOR)')
                               MATCHING (ID);
UPDATE OR INSERT INTO IMPORTADOS_MODELO (ID, ID_IMPORTADO, CAMPO, ACTIVO, SQL_CONVERSION)
                                 VALUES (450, 33, 'NA_Numero', 'S', 'SELECT TRIM(:Valor) AS NUMERO
FROM Rdb$Database')
                               MATCHING (ID);
UPDATE OR INSERT INTO IMPORTADOS_MODELO (ID, ID_IMPORTADO, CAMPO, ACTIVO, SQL_CONVERSION)
                                 VALUES (451, 33, 'NA_CodConcepto', 'S', 'SELECT GEN_ID(GEN_REG_RETENCIONES_ID, 1) AS Item
FROM Rdb$Database')
                               MATCHING (ID);
UPDATE OR INSERT INTO IMPORTADOS_MODELO (ID, ID_IMPORTADO, CAMPO, ACTIVO, SQL_CONVERSION)
                                 VALUES (453, 33, 'NA_Numeroref', 'S', 'SELECT TRIM(:Valor) AS NUMEROREF
FROM Rdb$Database')
                               MATCHING (ID);
UPDATE OR INSERT INTO IMPORTADOS_MODELO (ID, ID_IMPORTADO, CAMPO, ACTIVO, SQL_CONVERSION)
                                 VALUES (452, 33, 'NA_CodCentro', 'S', 'SELECT IIF(TRIM(:Valor) = '''', ''42011'', :Valor) AS CENTRO
FROM Rdb$Database')
                               MATCHING (ID);

COMMIT WORK;