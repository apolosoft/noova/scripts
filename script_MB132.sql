/* INFORMES DE INVENTARIO, COMPRAS, ORDENES DE COMPRA, CONSUMOS, DEVOLUCIONES, VENCIMIENTOS */
UPDATE OR INSERT INTO INFORMES (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD, DESTINO_FTP, CODREPORTE)
VALUES ('MB132_002', 'INFORME DE COMPRAS', 'INFORME DE COMPRAS SOLO FC', 'SELECT DISTINCT C.Codtercero AS Nit_Proveedor,
(SELECT Nombre_Tercero
FROM Fn_Nombre_Tercero(C.Codtercero)) Nombre_Proveedor,
R.Tipo || R.Prefijo || CAST(R.Numero AS INT) AS Numero_Factura,
C.Fecha Fecha_Factura,
R.Codreferencia AS Codigo_Item,
(SELECT Nombre_Referencia
FROM Fn_Nombre_Referencia(R.Codreferencia)) Nombre_Item,
(R.Entrada) Cantidad_Item,
((R.Entrada + R.Salida) * R.Bruto) Costo_Item_Compra,
Dt.Tarifa Iva
FROM Tr_Inventario R
INNER JOIN Comprobantes C ON (R.Tipo = C.Tipo AND R.Prefijo = C.Prefijo AND R.Numero = C.Numero)
INNER JOIN Documentos D ON (R.Tipo = D.Codigo)
LEFT JOIN Referencias Re ON (R.Codreferencia = Re.Codigo)
LEFT JOIN Esqimpuestos Ei ON (Ei.Codigo = Re.Cod_Esqimpuesto)
LEFT JOIN Esquema_Impuestos Ee ON (Ee.Codesquema = Ei.Codigo)
LEFT JOIN Data_Impuestos Dt ON (Dt.Codtipoimpuesto = Ee.Tipo_Impuesto) AND Dt.Ano = EXTRACT(YEAR FROM CURRENT_DATE)
WHERE C.Fecha >= :_Desde
AND C.Fecha <= :_Hasta
AND D.Inventario <> ''NO''
AND D.C_Codtercero = ''S''
AND (R.Entrada <> 0 OR R.Salida <> 0)
AND D.Grupo = ''COMPRA''
AND D.Codigo LIKE ''FC%''', 'N', 'GESTION', 'N', 'SALDOS DE INVENTARIO', 'N', 'LIBRE', 4, 'S', 1, NULL, NULL)
MATCHING (CODIGO);

UPDATE OR INSERT INTO INFORMES (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD, DESTINO_FTP, CODREPORTE)
VALUES ('MB132_003', 'INFORME DE CONSUMOS', 'INFORME DE CONSUMOS', 'SELECT DISTINCT A.Tipo || A.Prefijo || A.Numero Documento,
A.Codreferencia Codigo_Item,
A.Nombre_Referencia Nombre_Item,
COALESCE(A.Nombre_Lote, ''''),
--F.Ultimo_Costo
-- 0 Costo_Ultimo,
A.Ponderado Costo_Promedio,
Dt.Tarifa AS Iva,
COALESCE(A.Salidas, 0) AS Cantidad_Item,
A.Codbodega Codigo_Bodega,
A.Nombre_Bodega,
Co.Fecha,
Tr.Nota Unidad_Descarga
FROM Consumos_Rango(:_Desde, :_Hasta) A
--JOIN Fx_Auxiliar_Inventario(:_Desde, :_Hasta) F ON (F.Referencia = A.Codreferencia) AND (A.Tipo = F.Tipo AND A.Prefijo = F.Prefijo AND A.Numero = F.Numero AND A.Renglon = F.Renglon)
JOIN Comprobantes Co ON (Co.Tipo = A.Tipo AND Co.Prefijo = A.Prefijo AND Co.Numero = A.Numero)
JOIN Tr_Inventario Tr ON (Tr.Tipo = A.Tipo AND Tr.Prefijo = A.Prefijo AND Tr.Numero = A.Numero AND A.Renglon = Tr.Renglon)
JOIN Referencias R ON (A.Codreferencia = R.Codigo) AND (R.Codigo = Tr.Codreferencia)
JOIN Bodegas B ON (A.Codbodega = B.Codigo) AND (B.Codigo = Tr.Codbodega)
JOIN Lineas L ON (L.Codigo = R.Codlinea)
JOIN Esqimpuestos Ei ON (Ei.Codigo = R.Cod_Esqimpuesto)
JOIN Esquema_Impuestos Esi ON (Esi.Codesquema = Ei.Codigo)-- AND Esi.Grupo = ''VENTA''
JOIN Data_Impuestos Dt ON (Dt.Codtipoimpuesto = Esi.Tipo_Impuesto) AND Dt.Ano = EXTRACT(YEAR FROM CURRENT_DATE)
WHERE COALESCE(A.Salidas, 0) > 0
AND R.Valoriza = ''S''
AND R.Saldos = ''S''
ORDER BY 9, 10, 2', 'N', 'GESTION', 'N', 'SALDOS DE INVENTARIO', 'N', 'LIBRE', 4, 'S', 1, NULL, NULL)
MATCHING (CODIGO);

UPDATE OR INSERT INTO INFORMES (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD, DESTINO_FTP, CODREPORTE)
VALUES ('MB132_006', 'INFORME DE DEVOLUCIONES', 'INFORME DE DEVOLUCIONES', 'SELECT C.Tercero Nit_Proveedor,
Te.Nombre Nombre_Proveedor,
C.Referencia Codigo_Item,
Re.Nombre Nombre_Item,
ABS(C.Cantidad) Cantidad,
Co.Nota Concepto_Devolucion,
Co.Tipo || Co.Prefijo || Co.Numero Documento,
C.Fecha
FROM Fx_Dinamico(:_Desde, :_Hasta, ''COMPRA'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''N'', ''N'', ''N'', ''S'', ''N'', ''N'', ''N'', ''S'', ''N'', ''N'', ''N'', ''N'', ''N'') C
JOIN Documentos D ON (C.Tipo = D.Codigo)
JOIN Comprobantes Co ON (Co.Tipo = C.Tipo AND Co.Prefijo = C.Prefijo AND Co.Numero = C.Numero)
JOIN Terceros Te ON (C.Tercero = Te.Codigo)
JOIN Referencias Re ON (Re.Codigo = C.Referencia)
WHERE D.Codigo NOT IN (''FC1'', ''FC2'')
AND D.Signo = ''CREDITO''
AND D.Inventario = ''SALIDA''', 'N', 'GESTION', 'N', 'SALDOS DE INVENTARIO', 'N', 'LIBRE', 4, 'S', 1, NULL, NULL)
MATCHING (CODIGO);

UPDATE OR INSERT INTO INFORMES (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD, DESTINO_FTP, CODREPORTE)
VALUES ('MB132_001', 'INFORME INVENTARIO', 'INFORME DE INVENTARIO', 'SELECT DISTINCT I.Referencia Codigo_Item,
R.Nombre Nombre_Item,
I.Bodega Codigo_Bodega,
B.Nombre Nombre_Bodega,
-- COALESCE(Ai.Ultimo_Costo, 0) Costo_Ultimo,
I.Ponderado Costo_Promedio,
Dt.Tarifa Iva,
(I.Entradas - I.Salidas) Cantidad_Item,
I.Actualizado
FROM Fx_Inventario_Fecha(CURRENT_DATE) I
LEFT JOIN Referencias R ON (I.Referencia = R.Codigo)
LEFT JOIN Esqimpuestos Ei ON (Ei.Codigo = R.Cod_Esqimpuesto)
LEFT JOIN Esquema_Impuestos Ee ON (Ee.Codesquema = Ei.Codigo)
LEFT JOIN Data_Impuestos Dt ON (Dt.Codtipoimpuesto = Ee.Tipo_Impuesto) AND Dt.Ano = EXTRACT(YEAR FROM CURRENT_DATE)
LEFT JOIN Bodegas B ON (I.Bodega = B.Codigo)
LEFT JOIN Lineas L ON (R.Codlinea = L.Codigo)
--INNER JOIN Fx_Auxiliar_Inventario(CURRENT_DATE, CURRENT_DATE) Ai ON (Ai.Referencia = R.Codigo) AND (Ai.Bodega = B.Codigo)
WHERE I.Bodega LIKE TRIM(:Bodega) || ''%''
AND R.Codlinea LIKE TRIM(:Linea) || ''%''
ORDER BY 3, 2', 'N', 'GESTION', 'N', 'SALDOS DE INVENTARIO', 'N', 'LIBRE', 4, 'S', 1, NULL, NULL)
MATCHING (CODIGO);

UPDATE OR INSERT INTO INFORMES (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD, DESTINO_FTP, CODREPORTE)
VALUES ('MB132_005', 'INFORME DE VENCIMIENTOS', 'INFORME DE VENCIMIENTOS', 'SELECT F.Referencia Codigo_Item,
R.Nombre Nombre_Item,
F.Bodega Codigo_Bodega,
B.Nombre Nombre_Bodega,
F.Saldo Cantidad_Item,
F.Lote,
F.Vencimiento Fecha_Vencimiento,
Estado
FROM Fx_Inventario_Lotes F
INNER JOIN Referencias R ON (F.Referencia = R.Codigo)
INNER JOIN Bodegas B ON (F.Bodega = B.Codigo)
WHERE TRIM(R.Lote) = ''S'' -- Solo referencias que tengan Lotes
AND F.Lote <> ''NA'' -- Lotes distintos a No Aplica
AND F.Saldo > 0 -- Referencias que tengan Stock
AND Vencimiento IS NOT NULL', 'N', 'GESTION', 'N', 'SALDOS DE INVENTARIO', 'N', 'LIBRE', 4, 'S', 1, NULL, NULL)
MATCHING (CODIGO);

COMMIT WORK;

UPDATE OR INSERT INTO INFORMES (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD, DESTINO_FTP, CODREPORTE)
                        VALUES ('MB132_004', 'INFORME DE ORDENES DE COMPRAS', 'INFORME DE ORDENES DE COMPRAS SOLO OC Y REM', 'SELECT DISTINCT Tercero,
       Nombre_Tercero,
       Tipo,
       Prefijo,
       Numero,
       Fecha,
       Referencia,
       Nombre_Referencia,
       Solicitado,
       Despachado,
       Pendiente,
       Unitario,
       Valor
FROM Pz_Fx_Transito_Pendientes(:Desde)', 'N', 'GESTION', 'N', 'SALDOS DE INVENTARIO', 'N', 'LIBRE', 4, 'S', 1, NULL, NULL)
                      MATCHING (CODIGO);


COMMIT WORK;

UPDATE OR INSERT INTO INFORMES (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, FORMATO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD, DESTINO_FTP, CODREPORTE)
                        VALUES ('MB132_008', 'RELACION DETALLADA DE VENTAS MENOS DEVOLUCIONES CON NOTAS', 'Relación detallada de ventas en un rango de fecha dado.', 'SELECT ''+VENTAS'' AS Clase,
       (P.Cantidad) AS Cantidad,
       P.*,
       ((P.Cantidad * P.Bruto) + COALESCE(P.Gravado,0) + COALESCE(P.No_Gravado,0) - COALESCE(P.Retencion,0) - (P.Cantidad * P.Descuento)) AS Totales,
       P.Tipo,
       P.Prefijo,
       LPAD(TRIM(P.Numero),10) AS Numero,
       (SELECT Nombre_Documento
        FROM Fn_Nombre_Documento(P.Tipo)) AS "NOMBRE_DOCUMENTO",
       (SELECT TRIM(Nota)
        FROM Comprobantes
        WHERE Tipo = P.Tipo
              AND Prefijo = P.Prefijo
              AND Numero = P.Numero) Nota_Comprobante,
       (SELECT TRIM(Nota)
        FROM Tr_Inventario
        WHERE Renglon = P.Renglon) Nota_Movimiento
FROM Fx_Auxiliar_Pos(:Fecha_Desde,:Fecha_Hasta) P
JOIN Documentos D ON (P.Tipo = D.Codigo)
WHERE D.Grupo = ''VENTA''
      AND D.Es_Compra_Venta = ''S''
      AND D.Signo = ''CREDITO''

UNION ALL

SELECT ''-DEVOLUCION'' AS Clase,
       ((P.Cantidad) * -1) AS Cantidad,
       P.*,
       (((P.Cantidad * P.Bruto) + COALESCE(P.Gravado,0) + COALESCE(P.No_Gravado,0) - COALESCE(P.Retencion,0) - (P.Cantidad * P.Descuento)) * -1) AS Totales,
       P.Tipo,
       P.Prefijo,
       LPAD(TRIM(P.Numero),10) AS Numero,
       (SELECT Nombre_Documento
        FROM Fn_Nombre_Documento(P.Tipo)) AS "NOMBRE_DOCUMENTO",
       (SELECT TRIM(Nota)
        FROM Comprobantes
        WHERE Tipo = P.Tipo
              AND Prefijo = P.Prefijo
              AND Numero = P.Numero) Nota_Comprobante,
       (SELECT TRIM(Nota)
        FROM Tr_Inventario
        WHERE Renglon = P.Renglon) Nota_Movimiento
FROM Fx_Auxiliar_Pos(:Fecha_Desde,:Fecha_Hasta) P
JOIN Documentos D ON (P.Tipo = D.Codigo)
WHERE D.Grupo = ''VENTA''
      AND D.Es_Compra_Venta = ''S''
      AND D.Signo = ''DEBITO''', 'N', 'GESTION', NULL, 'N', 'SALDOS EN VENTAS', 'N', 'LIBRE', 5, 'N', 1, NULL, NULL)
                      MATCHING (CODIGO);


COMMIT WORK;


UPDATE OR INSERT INTO INFORMES (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, FORMATO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD, DESTINO_FTP, CODREPORTE)
                        VALUES ('MB132_009', 'RELACION DETALLADA DE COMPRAS CON NOTAS', 'Relación detallada de compras en un rango de fecha dado.', 'SELECT ''+COMPRAS'' AS Clase,
       P.*,
       ((P.Cantidad * P.Bruto) + COALESCE(P.Gravado,0) + COALESCE(P.No_Gravado,0) - COALESCE(P.Retencion,0) - (P.Cantidad * P.Descuento)) AS Totales,
       P.Tipo,
       P.Prefijo,
       LPAD(TRIM(P.Numero),10) AS Numero,
       (SELECT Nombre_Documento
        FROM Fn_Nombre_Documento(P.Tipo)) AS "NOMBRE_DOCUMENTO",
       (SELECT TRIM(Nota)
        FROM Comprobantes
        WHERE Tipo = P.Tipo
              AND Prefijo = P.Prefijo
              AND Numero = P.Numero) Nota_Comprobante,
       (SELECT TRIM(Nota)
        FROM Tr_Inventario
        WHERE Renglon = P.Renglon) Nota_Movimiento
FROM Fx_Auxiliar_Pos(:Fecha_Desde,:Fecha_Hasta) P
JOIN Documentos D ON (P.Tipo = D.Codigo)
WHERE D.Grupo = ''COMPRA''
      AND D.Es_Compra_Venta = ''S''
      AND D.Signo = ''DEBITO''

UNION ALL

SELECT ''-DEVOLUCION'' AS Clase,
       P.*,
       ((P.Cantidad * P.Bruto) + COALESCE(P.Gravado,0) + COALESCE(P.No_Gravado,0) - COALESCE(P.Retencion,0) - (P.Cantidad * P.Descuento)) AS Totales,
       P.Tipo,
       P.Prefijo,
       LPAD(TRIM(P.Numero),10) AS Numero,
       (SELECT Nombre_Documento
        FROM Fn_Nombre_Documento(P.Tipo)) AS "NOMBRE_DOCUMENTO",
       (SELECT TRIM(Nota)
        FROM Comprobantes
        WHERE Tipo = P.Tipo
              AND Prefijo = P.Prefijo
              AND Numero = P.Numero) Nota_Comprobante,
       (SELECT TRIM(Nota)
        FROM Tr_Inventario
        WHERE Renglon = P.Renglon) Nota_Movimiento
FROM Fx_Auxiliar_Pos(:Fecha_Desde,:Fecha_Hasta) P
JOIN Documentos D ON (P.Tipo = D.Codigo)
WHERE D.Grupo = ''COMPRA''
      AND D.Es_Compra_Venta = ''S''
      AND D.Signo = ''CREDITO''', 'N', 'GESTION', NULL, 'N', 'SALDOS EN COMPRAS', 'N', 'LIBRE', 5, 'N', 1, NULL, NULL)
                      MATCHING (CODIGO);


COMMIT WORK;





/******************************************************************************/
/***                           Stored procedures                            ***/
/******************************************************************************/



SET TERM ^ ;

CREATE OR ALTER PROCEDURE PZ_FX_TRANSITO_PENDIENTES (
    DESDE_ DATE)
RETURNS (
    TERCERO CHAR(15),
    NOMBRE_TERCERO CHAR(164),
    TIPO CHAR(5),
    PREFIJO CHAR(5),
    NUMERO CHAR(10),
    FECHA DATE,
    REFERENCIA CHAR(20),
    NOMBRE_REFERENCIA CHAR(80),
    SOLICITADO NUMERIC(17,4),
    DESPACHADO NUMERIC(17,4),
    PENDIENTE NUMERIC(17,4),
    UNITARIO NUMERIC(17,4),
    VALOR NUMERIC(17,4))
AS
BEGIN
  SUSPEND;
END^






SET TERM ; ^



/******************************************************************************/
/***                           Stored procedures                            ***/
/******************************************************************************/



SET TERM ^ ;

CREATE OR ALTER PROCEDURE PZ_FX_TRANSITO_PENDIENTES (
    DESDE_ DATE)
RETURNS (
    TERCERO CHAR(15),
    NOMBRE_TERCERO CHAR(164),
    TIPO CHAR(5),
    PREFIJO CHAR(5),
    NUMERO CHAR(10),
    FECHA DATE,
    REFERENCIA CHAR(20),
    NOMBRE_REFERENCIA CHAR(80),
    SOLICITADO NUMERIC(17,4),
    DESPACHADO NUMERIC(17,4),
    PENDIENTE NUMERIC(17,4),
    UNITARIO NUMERIC(17,4),
    VALOR NUMERIC(17,4))
AS
DECLARE Vsql      CHAR(1000);
DECLARE Orden     INTEGER;
DECLARE VARIABLE V_Origen  CHAR(5);
DECLARE VARIABLE V_Destino CHAR(5);

BEGIN

  FOR SELECT DISTINCT Origen,
                      Destino
      FROM Rutas

      INTO V_Origen,
           V_Destino
  DO
  BEGIN
    FOR SELECT Tercero
        FROM Fx_Transito_Resumen(:V_Origen, :Desde_)
        INTO Tercero
    DO
    BEGIN
      FOR SELECT Tipo,
                 Prefijo,
                 Numero,
                 Fecha,
                 Codreferencia,
                 Solicitado,
                 Despachado,
                 Pendiente,
                 Unitario,
                 Pendiente * Unitario

          FROM Fx_Transito_Referencias(:Tercero, :V_Origen, :V_Destino, :Desde_)
          INTO Tipo,
               Prefijo,
               Numero,
               Fecha,
               Referencia,
               Solicitado,
               Despachado,
               Pendiente,
               Unitario,
               Valor
      DO
      BEGIN
        SELECT Nombre_Referencia
        FROM Fn_Nombre_Referencia(:Referencia)
        INTO Nombre_Referencia;

        SELECT Nombre_Tercero
        FROM Fn_Nombre_Tercero(:Tercero)
        INTO Nombre_Tercero;

        SUSPEND;
      END
    END

  END
END^



SET TERM ; ^

COMMIT WORK;


/******************************************************************************/
/***                   Archivo Plano Bancolombia                            ***/
/******************************************************************************/

UPDATE OR INSERT INTO INFORMES (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, FORMATO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD, DESTINO_FTP, CODREPORTE)
                        VALUES ('MB132_007', 'ARCHIVO PLANO PAGO BANCOLOMBIA', NULL, 'SELECT ''1'' || LPAD(TRIM(T.Codigo), 15, ''0'') || ''I'' || LPAD('''', 15, '' '') || ''225'' || RPAD(''NOMINA'', 10, '' '') ||
EXTRACT(YEAR FROM CURRENT_DATE) || RIGHT(EXTRACT(MONTH FROM CURRENT_DATE) + 100, 2) ||
RIGHT(EXTRACT(DAY FROM CURRENT_DATE) + 100, 2) || RIGHT(TRIM(N.Codigo), 2) ||
EXTRACT(YEAR FROM CURRENT_DATE) || RIGHT(EXTRACT(MONTH FROM CURRENT_DATE) + 100, 2) ||
RIGHT(EXTRACT(DAY FROM CURRENT_DATE) + 100, 2) ||
LPAD((SELECT COUNT(*)
       FROM Planillas P
       WHERE P.Codnomina = N.Codigo
       AND P.Codrubro = :Rubro), 6, 0) || LPAD(''0'', 17, ''0'') ||
LPAD((SELECT SUM(ROUND(Adicion))
        FROM Planillas P
        JOIN PERSONAL E ON (P.codpersonal=E.CODIGO)
        WHERE P.Codnomina = N.Codigo
        AND P.Codrubro = :Rubro
        AND COALESCE(TRIM(E.BANCO),'''')<>''''  ), 15, 0) || ''00'' ||
LPAD((SELECT TRIM(Titular)
        FROM Bancos
        WHERE Codigo = ''BC''), 11, 0) || ''D'' || LPAD('''', 149, '' '')
FROM Terceros T
JOIN Nominas N ON (N.Codigo = :Nomina)
WHERE Datos_Empresa = ''S''

UNION ALL

SELECT ''6'' || RPAD(TRIM(P.Codpersonal), 15, '' '') || RPAD(TRIM(E.Nombre), 30, '' '') ||
LPAD(COALESCE(trim(E.CODENTIDAD),0), 9, 0) ||
RPAD(COALESCE(TRIM(E.Banco),'' ''), 17, '' '') ||
 '' '' || IIF(E.CODTIPOCUENTA=''C'',''27'',''37'') || LPAD(ROUND(P.Adicion), 15, 0) || ''00'' || EXTRACT(YEAR FROM CURRENT_DATE) ||
 RIGHT(EXTRACT(MONTH FROM CURRENT_DATE) + 100, 2) || RIGHT(EXTRACT(DAY FROM CURRENT_DATE) + 100, 2) ||
 RPAD(N.Nombre, 21, '' '') || '' '' || LPAD(0, 5, 0) || RPAD('' '', 15, '' '') ||
 RPAD(IIF(E.EMAIL containing ''@'', COALESCE(E.Email, '' ''),''''), 80, '' '') || RPAD('' '', 15, '' '') || RPAD('' '', 27, '' '')
FROM Planillas P
JOIN Personal E ON (P.Codpersonal = E.Codigo)
JOIN Nominas N ON (P.Codnomina = N.Codigo)
WHERE P.Codnomina = :Nomina
      AND P.Codrubro = :Rubro
      AND COALESCE(TRIM(E.BANCO),'''')<>''''', 'N', 'NOMINA', NULL, 'N', 'PERSONALIZADOS', 'N', 'LIBRE', 5, 'N', 1, NULL, NULL)
                      MATCHING (CODIGO);


COMMIT WORK;