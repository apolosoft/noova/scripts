/************************************************************/
/****                 MODULO GESTION                     ****/
/************************************************************/

/* EXPORTADOS GESTION BEGIN */

DELETE FROM EXPORTADOS
WHERE PT = 'NOOVA';

UPDATE PTA SET 
    XML_TEMPLATE = '<?xml version="1.0" encoding="UTF-8"?>
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:noov="http://Noova/">
    <soapenv:Header>
        <noov:AuthHeader>
            <!--Usuario base64:-->
            <noov:Username>UsuarioNoova</noov:Username>
            <!--Password base64:-->
            <noov:Password>ClaveNoova</noov:Password>
        </noov:AuthHeader>
    </soapenv:Header>
    <!--950 Gestion 03/NOV/2022 v1-->
    <soapenv:Body>
        <noov:SetFacFacturas>
            <noov:dtoFacFacturasInput>
            ges:comprobante
            <noov:lDetalle>
               ges:detalle
            </noov:lDetalle>
            <noov:lImpuestos>
               ges:impuestos
            </noov:lImpuestos>
            ges:sector_salud
            ges:tasa_cambio
            <noov:Waitpdf>
                <noov:Nvwai_tiem>2880</noov:Nvwai_tiem>
            </noov:Waitpdf>
            </noov:dtoFacFacturasInput>
        </noov:SetFacFacturas>
    </soapenv:Body>
</soapenv:Envelope>'
WHERE (CODIGO = 'NOOVA');

COMMIT WORK;

UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (261, 'ges:comprobante', 'DOC', '-- Comprobante Gestion Factura 950 03/NOV/2022 v1

WITH V_Trm
AS (SELECT Valor AS Trm
    FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero))

SELECT "noov:Nvfac_orig",
       "noov:Nvemp_nnit",
       "noov:Nvres_nume",
       "noov:Nvfac_tipo",
       "noov:Nvres_pref",
       "noov:Nvfac_nume",
       "noov:Nvfac_fech",
       "noov:Nvfac_cdet",
       "noov:Nvfac_venc",
       "noov:Nvsuc_codi",
       "noov:Nvmon_codi",
       "noov:Nvfor_codi",
       "noov:Nvven_nomb",
       "noov:Nvfac_fpag",
       "noov:Nvfac_conv",
       "noov:Nvcli_cper",
       "noov:Nvcli_cdoc",
       "noov:Nvcli_docu",
       "noov:Nvcli_pais",
       "noov:Nvcli_depa",
       "noov:Nvcli_ciud",
       "noov:Nvcli_loca",
       "noov:Nvcli_dire",
       "noov:Nvcli_ntel",
       "noov:Nvcli_regi",
       "noov:Nvcli_fisc",
       "noov:Nvcli_nomb",
       "noov:Nvcli_pnom",
       "noov:Nvcli_snom",
       "noov:Nvcli_apel",
       "noov:Nvcli_mail",
       "noov:Nvema_copi",
       "noov:Nvfac_obse",
       "noov:Nvfac_orde",
       "noov:Nvfac_remi",
       "noov:Nvfac_rece",
       "noov:Nvfac_entr",
       "noov:Nvfac_ccos",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_stot" / Trm, 2)) "noov:Nvfac_stot",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc" / Trm, 2)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_anti" / Trm, 2)) "noov:Nvfac_anti",
       "noov:Nvfac_carg",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_tota" / Trm, 2)) "noov:Nvfac_tota",
       (SELECT Valor
        FROM Redondeo_Dian(Com_Total / Trm, 2)) "noov:Nvfac_totp",
       (SELECT Valor
        FROM Pz_Fe_Round(:Tipo, :Prefijo, :Numero, Trm)) "noov:Nvfac_roun",
       "noov:Nvfac_vcop",
       "noov:Nvfac_timp",
       "noov:Nvfac_coid",
       "noov:Nvfac_obsb",
       "noov:Nvcli_ncon",
       "noov:Nvfor_oper"
FROM Pz_Fe_Comprobantes(:Tipo, :Prefijo, :Numero, :Emisor, :Receptor)
JOIN V_Trm ON (1 = 1)', 'S', 5, 'NOOVA', 'FACTURA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (262, 'ges:detalle', 'DOC', '-- Detalle Gestion Factura

WITH V_Trm
AS (SELECT Valor AS Trm
    FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero))

SELECT Renglon,
       Consecutivo,
       "noov:Nvpro_codi",
       "noov:Nvpro_nomb",
       "noov:Nvuni_desc",
       "noov:Nvfac_cant",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_valo" / Trm, 2)) "noov:Nvfac_valo",
       "noov:Nvfac_pdes",
      (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc" / Trm, 4)) "noov:Nvfac_desc",
       "noov:Nvdet_pref",
      (SELECT Valor
        FROM Redondeo_Dian(Stot_Red / Trm, 2)) "noov:Nvfac_stot",
       "noov:Nvimp_cdia",
       "noov:Nvdet_piva",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvdet_viva" / Trm, 2)) "noov:Nvdet_viva",
       "noov:Nvdet_tcod",
       "noov:Nvpro_cean",
       "noov:Nvdet_entr",
       "noov:Nvuni_quan",
       "noov:Nvdet_nota",
       "noov:Nvdet_padr",
       "noov:Nvdet_marc",
       "noov:Nvdet_mode"
FROM Pz_Fe_Detalle(:Tipo, :Prefijo, :Numero)
JOIN V_Trm ON (1 = 1)', 'S', 10, 'NOOVA', 'FACTURA', 'noov:DTOFacDetallefacturas', NULL, 'noov:Nvfac_dcop', NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (263, 'ges:imp_detalle', 'DOC', '-- Impuesto Detalle Factura

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_desc",
       "noov:Nvimp_oper",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base", 2)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo", 2)) "noov:Nvimp_valo"
FROM Pz_Fe_Impuestos_Detalle(:Tipo, :Prefijo, :Numero, :Renglon)', 'S', 15, 'NOOVA', 'FACTURA', 'noov:lImpuestosDetalle', 'noov:DTOFacImpuestosdetallefacturas', NULL, 'ges:detalle')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (264, 'ges:mandatario', 'DOC', '-- Mandatario Gestion Factura

SELECT
       CASE T.Naturaleza
         WHEN ''J'' THEN 1
         WHEN ''N'' THEN 2
         ELSE ''''
       END "noov:Nvman_cper",
       Codidentidad "noov:Nvman_cdoc",
       TRIM(C.Codtercero) || IIF(TRIM(T.Codidentidad) = ''31'', IIF(T.Dv IS NULL, '''', ''-'' || TRIM(T.Dv)), '''') "noov:Nvman_docu",
       T.Email "noov:Nvman_mail",
       T.Direccion "noov:Nvman_dire",
       S.Codigo_Fe "noov:Nvman_regi",
       COALESCE(T.Empresa, '''') "noov:Nvman_nomb",
       COALESCE(T.Nom1, '''') "noov:Nvman_pnom",
       COALESCE(T.Nom2, '''') "noov:Nvman_snom",
       TRIM(COALESCE(T.Apl1, '''')) || '' '' || TRIM(COALESCE(T.Apl2, '''')) "noov:Nvman_apel",
       COALESCE(T.Telefono, '''') "noov:Nvman_ntel"
FROM Tr_Inventario M
JOIN Centros C ON (M.Codcentro = C.Codigo)
LEFT JOIN Terceros T ON (C.Codtercero = T.Codigo)
LEFT JOIN Sociedades S ON (T.Codsociedad = S.Codigo)
WHERE Tipo = :Tipo
      AND Prefijo = :Prefijo
      AND Numero = :Numero
      AND Renglon = :Renglon
      AND COALESCE(C.Codtercero, '''') <> ''''', 'S', 20, 'NOOVA', 'FACTURA', 'noov:Nvdet_mand', NULL, NULL, 'ges:detalle')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (265, 'ges:impuestos', 'DOC', '-- Impuestos Gestion Factura

WITH V_Trm
AS (SELECT Valor AS Trm
    FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero))

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_oper",
       "noov:Nvimp_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base" / Trm, 2)) "noov:Nvimp_base",
       "noov:Nvimp_porc" / IIF(Es_Tarifa = ''N'', Trm, 1) "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo" / Trm, 2)) "noov:Nvimp_valo"
FROM Pz_Fe_Impuestos_Sum(:Tipo, :Prefijo, :Numero)
JOIN V_Trm ON (1 = 1)
WHERE "noov:Nvimp_cdia" NOT IN (''07'', ''99'')', 'S', 25, 'NOOVA', 'FACTURA', 'noov:DTOFacImpuestosfacturas', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (266, 'ges:tasa_cambio', 'DOC', '--Tasa Cambio Gestion Factura

SELECT FIRST 1 Codigo "noov:Nvtas_sour",
               ''COP'' "noov:Nvtas_targ",
               Valor "noov:Nvtas_calc",
               Fecha "noov:Nvtas_fech"
FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero)
WHERE Codigo <> ''COP''', 'S', 30, 'NOOVA', 'FACTURA', 'noov:TasaCambio', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (267, 'ges:sector_salud', 'DOC', '-- Sector Salud Gestion Factura

SELECT SUBSTRING("noov:Nvfac_fech" FROM 1 FOR 10) "noov:Nvsal_fini",
       SUBSTRING("noov:Nvfac_fech" FROM 1 FOR 10) "noov:Nvsal_ffin"
FROM Pz_Fe_Comprobantes(:Tipo, :Prefijo, :Numero, :Emisor, :Receptor)
WHERE "noov:Nvfor_oper" = ''SS-CUFE''', 'S', 35, 'NOOVA', 'FACTURA', 'noov:SectorSalud', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (268, 'ges:proveedor', 'DOC', 'SELECT 1
FROM Rdb$Database
WHERE 1 = 0', 'S', 40, 'NOOVA', 'FACTURA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (281, 'ges:comprobante', 'DOC', '-- Comprobante Gestion Exportacion

WITH V_Trm
AS (SELECT Valor AS Trm
    FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero))

SELECT "noov:Nvfac_orig",
       "noov:Nvemp_nnit",
       "noov:Nvres_nume",
       "noov:Nvfac_tipo",
       "noov:Nvres_pref",
       "noov:Nvfac_nume",
       "noov:Nvfac_fech",
       "noov:Nvfac_cdet",
       "noov:Nvfac_venc",
       "noov:Nvsuc_codi",
       "noov:Nvmon_codi",
       "noov:Nvfor_codi",
       "noov:Nvven_nomb",
       "noov:Nvfac_fpag",
       "noov:Nvfac_conv",
       "noov:Nvcli_cper",
       "noov:Nvcli_cdoc",
       "noov:Nvcli_docu",
       "noov:Nvcli_pais",
       "noov:Nvcli_depa",
       "noov:Nvcli_ciud",
       "noov:Nvcli_loca",
       "noov:Nvcli_dire",
       "noov:Nvcli_ntel",
       "noov:Nvcli_regi",
       "noov:Nvcli_fisc",
       "noov:Nvcli_nomb",
       "noov:Nvcli_pnom",
       "noov:Nvcli_snom",
       "noov:Nvcli_apel",
       "noov:Nvcli_mail",
       "noov:Nvema_copi",
       "noov:Nvfac_obse",
       "noov:Nvfac_orde",
       "noov:Nvfac_remi",
       "noov:Nvfac_rece",
       "noov:Nvfac_entr",
       "noov:Nvfac_ccos",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_stot" / Trm, 2)) "noov:Nvfac_stot",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc" / Trm, 2)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_anti" / Trm, 2)) "noov:Nvfac_anti",
       "noov:Nvfac_carg",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_tota" / Trm, 2)) "noov:Nvfac_tota",
       (SELECT Valor
        FROM Redondeo_Dian(Com_Total / Trm, 2)) "noov:Nvfac_totp",
       (SELECT Valor
        FROM Pz_Fe_Round(:Tipo, :Prefijo, :Numero, Trm)) "noov:Nvfac_roun",
       "noov:Nvfac_vcop",
       "noov:Nvfac_timp",
       "noov:Nvfac_coid",
       "noov:Nvfac_obsb",
       "noov:Nvcli_ncon"
FROM Pz_Fe_Comprobantes(:Tipo, :Prefijo, :Numero, :Emisor, :Receptor)
JOIN V_Trm ON (1 = 1)', 'S', 5, 'NOOVA', 'EXPORTACION', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (282, 'ges:detalle', 'DOC', '-- Detalle Gestion Exportacion

WITH V_Trm
AS (SELECT Valor AS Trm
    FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero))

SELECT Renglon,
       Consecutivo,
       "noov:Nvpro_codi",
       "noov:Nvpro_nomb",
       "noov:Nvuni_desc",
       "noov:Nvfac_cant",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_valo" / Trm, 2)) "noov:Nvfac_valo",
       "noov:Nvfac_pdes",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc" / Trm, 2)) "noov:Nvfac_desc",
       "noov:Nvdet_pref",
       (SELECT Valor
        FROM Redondeo_Dian(Stot_Red / Trm, 2)) "noov:Nvfac_stot",
       "noov:Nvimp_cdia",
       "noov:Nvdet_piva",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvdet_viva" / Trm, 2)) "noov:Nvdet_viva",
       "noov:Nvdet_tcod",
       "noov:Nvpro_cean",
       "noov:Nvdet_entr",
       "noov:Nvuni_quan",
       "noov:Nvdet_nota",
       "noov:Nvdet_padr",
       "noov:Nvdet_marc",
       "noov:Nvdet_mode"
FROM Pz_Fe_Detalle(:Tipo, :Prefijo, :Numero)
JOIN V_Trm ON (1 = 1)', 'S', 10, 'NOOVA', 'EXPORTACION', 'noov:DTOFacDetallefacturas', NULL, 'noov:Nvfac_dcop', NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (283, 'ges:adicional', 'DOC', '-- Adicionales Gestion Exportacion

WITH V_Trm
AS (SELECT Valor AS Trm
    FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero))

SELECT 1 "noov:Nvfac_atri",
       (SELECT Valor
        FROM Redondeo_Dian(stot / Trm, 2)) "noov:Nvfac_vatr",
       ''ValorUSD'' "noov:Nvfac_natr"
FROM Pz_Fe_Detalle_Base(:Tipo, :Prefijo, :Numero)
JOIN V_Trm ON (1 = 1)
WHERE Renglon = :Renglon

UNION ALL

SELECT 2 "noov:Nvfac_atri",
       (SELECT Valor
        FROM Redondeo_Dian(stot, 2)) "noov:Nvfac_vatr",
       ''ValorCOP'' "noov:Nvfac_natr"
FROM Pz_Fe_Detalle_base(:Tipo, :Prefijo, :Numero)
WHERE Renglon = :Renglon', 'S', 15, 'NOOVA', 'EXPORTACION', 'noov:lAdicionalDetalle', 'noov:DTOFacAdicionalesdetallefacturas', NULL, 'ges:detalle')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (284, 'ges:impuestos', 'DOC', '-- Impuestos Gestion Exportacion

WITH V_Trm
AS (SELECT Valor AS Trm
    FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero))

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_oper",
       "noov:Nvimp_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base" / Trm, 2)) "noov:Nvimp_base",
       "noov:Nvimp_porc" / IIF(Es_Tarifa = ''N'', Trm, 1) "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo" / Trm, 2)) "noov:Nvimp_valo"
FROM Pz_Fe_Impuestos_Sum(:Tipo, :Prefijo, :Numero)
JOIN V_Trm ON (1 = 1)
WHERE "noov:Nvimp_cdia" NOT IN (''07'', ''99'')', 'S', 25, 'NOOVA', 'EXPORTACION', 'noov:DTOFacImpuestosfacturas', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (285, 'ges:tasa_cambio', 'DOC', '-- Tasa Cambio Gestion Exportacion

SELECT FIRST 1 Codigo "noov:Nvtas_sour",
               ''COP'' "noov:Nvtas_targ",
               Valor "noov:Nvtas_calc",
               Fecha "noov:Nvtas_fech"
FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero)
WHERE Codigo <> ''COP''', 'S', 30, 'NOOVA', 'EXPORTACION', 'noov:TasaCambio', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (286, 'ges:proveedor', 'DOC', 'SELECT 1
FROM Rdb$Database
WHERE 1 = 0', 'S', 40, 'NOOVA', 'EXPORTACION', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (301, 'ges:comprobante', 'DOC', '-- Comprobante Gestion Contingencia

WITH V_Trm
AS (SELECT Valor AS Trm
    FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero))

SELECT "noov:Nvfac_orig",
       "noov:Nvemp_nnit",
       "noov:Nvres_nume",
       "noov:Nvfac_tipo",
       "noov:Nvres_pref",
       "noov:Nvfac_nume",
       "noov:Nvfac_fech",
       "noov:Nvfac_cdet",
       "noov:Nvfac_venc",
       "noov:Nvsuc_codi",
       "noov:Nvmon_codi",
       "noov:Nvfor_codi",
       "noov:Nvven_nomb",
       "noov:Nvfac_fpag",
       "noov:Nvfac_conv",
       "noov:Nvcli_cper",
       "noov:Nvcli_cdoc",
       "noov:Nvcli_docu",
       "noov:Nvcli_pais",
       "noov:Nvcli_depa",
       "noov:Nvcli_ciud",
       "noov:Nvcli_loca",
       "noov:Nvcli_dire",
       "noov:Nvcli_ntel",
       "noov:Nvcli_regi",
       "noov:Nvcli_fisc",
       "noov:Nvcli_nomb",
       "noov:Nvcli_pnom",
       "noov:Nvcli_snom",
       "noov:Nvcli_apel",
       "noov:Nvcli_mail",
       "noov:Nvema_copi",
       "noov:Nvfac_obse",
       "noov:Nvfac_orde",
       "noov:Nvfac_remi",
       "noov:Nvfac_rece",
       "noov:Nvfac_entr",
       "noov:Nvfac_ccos",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_stot" / Trm, 2)) "noov:Nvfac_stot",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc" / Trm, 2)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_anti" / Trm, 2)) "noov:Nvfac_anti",
       "noov:Nvfac_carg",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_tota" / Trm, 2)) "noov:Nvfac_tota",
       (SELECT Valor
        FROM Redondeo_Dian(Com_Total / Trm, 2)) "noov:Nvfac_totp",
       (SELECT Valor
        FROM Pz_Fe_Round(:Tipo, :Prefijo, :Numero, Trm)) "noov:Nvfac_roun",
       "noov:Nvfac_vcop",
       "noov:Nvfac_timp",
       "noov:Nvfac_coid",
       "noov:Nvfac_obsb",
       "noov:Nvcli_ncon",
       "noov:Nvfor_oper"
FROM Pz_Fe_Comprobantes(:Tipo, :Prefijo, :Numero, :Emisor, :Receptor)
JOIN V_Trm ON (1 = 1)', 'S', 5, 'NOOVA', 'CONTINGENCIA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (302, 'ges:detalle', 'DOC', '-- Detalle Gestion Contingencia

WITH V_Trm
AS (SELECT Valor AS Trm
    FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero))

SELECT Renglon,
       Consecutivo,
       "noov:Nvpro_codi",
       "noov:Nvpro_nomb",
       "noov:Nvuni_desc",
       "noov:Nvfac_cant",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_valo" / Trm, 2)) "noov:Nvfac_valo",
       "noov:Nvfac_pdes",
      (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc" / Trm, 2)) "noov:Nvfac_desc",
       "noov:Nvdet_pref",
      (SELECT Valor
        FROM Redondeo_Dian(Stot_Red / Trm, 2)) "noov:Nvfac_stot",
       "noov:Nvimp_cdia",
       "noov:Nvdet_piva",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvdet_viva" / Trm, 2)) "noov:Nvdet_viva",
       "noov:Nvdet_tcod",
       "noov:Nvpro_cean",
       "noov:Nvdet_entr",
       "noov:Nvuni_quan",
       "noov:Nvdet_nota",
       "noov:Nvdet_padr",
       "noov:Nvdet_marc",
       "noov:Nvdet_mode"
FROM Pz_Fe_Detalle(:Tipo, :Prefijo, :Numero)
JOIN V_Trm ON (1 = 1)', 'S', 10, 'NOOVA', 'CONTINGENCIA', 'noov:DTOFacDetallefacturas', NULL, 'noov:Nvfac_dcop', NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (303, 'ges:imp_detalle', 'DOC', '-- Impuesto Detalle Contingencia

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_desc",
       "noov:Nvimp_oper",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base", 2)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo", 2)) "noov:Nvimp_valo"
FROM Pz_Fe_Impuestos_Detalle(:Tipo, :Prefijo, :Numero, :Renglon)', 'S', 15, 'NOOVA', 'CONTINGENCIA', 'noov:lImpuestosDetalle', NULL, NULL, 'ges:detalle')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (304, 'ges:mandatario', 'DOC', '-- Mandatario Gestion Contingencia

SELECT
       CASE T.Naturaleza
         WHEN ''J'' THEN 1
         WHEN ''N'' THEN 2
         ELSE ''''
       END "noov:Nvman_cper",
       Codidentidad "noov:Nvman_cdoc",
       TRIM(C.Codtercero) || IIF(TRIM(T.Codidentidad) = ''31'', IIF(T.Dv IS NULL, '''', ''-'' || TRIM(T.Dv)), '''') "noov:Nvman_docu",
       T.Email "noov:Nvman_mail",
       T.Direccion "noov:Nvman_dire",
       S.Codigo_Fe "noov:Nvman_regi",
       COALESCE(T.Empresa, '''') "noov:Nvman_nomb",
       COALESCE(T.Nom1, '''') "noov:Nvman_pnom",
       COALESCE(T.Nom2, '''') "noov:Nvman_snom",
       TRIM(COALESCE(T.Apl1, '''')) || '' '' || TRIM(COALESCE(T.Apl2, '''')) "noov:Nvman_apel",
       COALESCE(T.Telefono, '''') "noov:Nvman_ntel"
FROM Tr_Inventario M
JOIN Centros C ON (M.Codcentro = C.Codigo)
LEFT JOIN Terceros T ON (C.Codtercero = T.Codigo)
LEFT JOIN Sociedades S ON (T.Codsociedad = S.Codigo)
WHERE Tipo = :Tipo
      AND Prefijo = :Prefijo
      AND Numero = :Numero
      AND Renglon = :Renglon
      AND COALESCE(C.Codtercero, '''') <> ''''', 'S', 20, 'NOOVA', 'CONTINGENCIA', 'noov:Nvdet_mand', NULL, NULL, 'ges:detalle')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (305, 'ges:impuestos', 'DOC', '-- Impuestos Gestion Contingencia

WITH V_Trm
AS (SELECT Valor AS Trm
    FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero))

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_oper",
       "noov:Nvimp_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base" / Trm, 2)) "noov:Nvimp_base",
       "noov:Nvimp_porc" / IIF(Es_Tarifa = ''N'', Trm, 1) "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo" / Trm, 2)) "noov:Nvimp_valo"
FROM pz_fe_impuestos_sum(:Tipo, :Prefijo, :Numero)
JOIN V_Trm ON (1 = 1)
WHERE "noov:Nvimp_cdia" NOT IN (''07'', ''99'')', 'S', 25, 'NOOVA', 'CONTINGENCIA', 'noov:DTOFacImpuestosfacturas', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (306, 'ges:tasa_cambio', 'DOC', '-- Tasa Cambio Gestion Contingencia

SELECT FIRST 1 Codigo "noov:Nvtas_sour",
               ''COP'' "noov:Nvtas_targ",
               Valor "noov:Nvtas_calc",
               Fecha "noov:Nvtas_fech"
FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero)
WHERE Codigo <> ''COP''', 'S', 30, 'NOOVA', 'CONTINGENCIA', 'noov:TasaCambio', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (307, 'ges:sector_salud', 'DOC', '-- Sector Salud Gestion Contingencia

SELECT SUBSTRING("noov:Nvfac_fech" FROM 1 FOR 10) "noov:Nvsal_fini",
       SUBSTRING("noov:Nvfac_fech" FROM 1 FOR 10) "noov:Nvsal_ffin"
FROM Pz_Fe_Comprobantes(:Tipo, :Prefijo, :Numero, :Emisor, :Receptor)
WHERE "noov:Nvfor_oper" = ''SS-CUFE''', 'S', 35, 'NOOVA', 'CONTINGENCIA', 'noov:SectorSalud', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (308, 'ges:proveedor', 'DOC', 'SELECT 1
FROM Rdb$Database
WHERE 1 = 0', 'S', 40, 'NOOVA', 'CONTINGENCIA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (321, 'ges:comprobante', 'DOC', '-- Comprobante Gestion Nota Debito

WITH V_Trm
AS (SELECT Valor AS Trm
    FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero))

SELECT "noov:Nvfac_orig",
       "noov:Nvemp_nnit",
       "noov:Nvfac_tipo",
       "noov:Nvres_pref",
       "noov:Nvfac_nume",
       "noov:Nvfac_fech",
       "noov:Nvfac_cdet",
       "noov:Nvfac_venc",
       "noov:Nvsuc_codi",
       "noov:Nvmon_codi",
       "noov:Nvfor_codi",
       "noov:Nvven_nomb",
       "noov:Nvfac_fpag",
       "noov:Nvfac_conv",
       "noov:Nvcli_cper",
       "noov:Nvcli_cdoc",
       "noov:Nvcli_docu",
       "noov:Nvcli_pais",
       "noov:Nvcli_depa",
       "noov:Nvcli_ciud",
       "noov:Nvcli_loca",
       "noov:Nvcli_dire",
       "noov:Nvcli_ntel",
       "noov:Nvcli_regi",
       "noov:Nvcli_fisc",
       "noov:Nvcli_nomb",
       "noov:Nvcli_pnom",
       "noov:Nvcli_snom",
       "noov:Nvcli_apel",
       "noov:Nvcli_mail",
       "noov:Nvema_copi",
       "noov:Nvfac_obse",
       "noov:Nvfac_orde",
       "noov:Nvfac_remi",
       "noov:Nvfac_rece",
       "noov:Nvfac_entr",
       "noov:Nvfac_ccos",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_stot" / Trm, 2)) "noov:Nvfac_stot",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc" / Trm, 2)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_anti" / Trm, 2)) "noov:Nvfac_anti",
       "noov:Nvfac_carg",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_tota" / Trm, 2)) "noov:Nvfac_tota",
       (SELECT Valor
        FROM Redondeo_Dian(Com_Total / Trm, 2)) "noov:Nvfac_totp",
       (SELECT Valor
        FROM Pz_Fe_Round(:Tipo, :Prefijo, :Numero, Trm)) "noov:Nvfac_roun",
       "noov:Nvfac_vcop",
       "noov:Nvfac_timp",
       "noov:Nvfac_coid",
       "noov:Nvfac_numb",
       "noov:Nvfac_fecb",
       "noov:Nvfac_obsb",
       "noov:Nvcli_ncon",
       "noov:Nvfac_tcru",
       "noov:Nvcon_codi",
       "noov:Nvcon_desc"
FROM Pz_Fe_Comprobantes(:Tipo, :Prefijo, :Numero, :Emisor, :Receptor)
JOIN V_Trm ON (1 = 1)', 'S', 5, 'NOOVA', 'NOTA DEBITO', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (322, 'ges:detalle', 'DOC', '-- Detalle Gestion Nota Debito

WITH V_Trm
AS (SELECT Valor AS Trm
    FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero))

SELECT Renglon,
       Consecutivo,
       "noov:Nvpro_codi",
       "noov:Nvpro_nomb",
       "noov:Nvuni_desc",
       "noov:Nvfac_cant",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_valo" / Trm, 2)) "noov:Nvfac_valo",
       "noov:Nvfac_pdes",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc" / Trm, 2)) "noov:Nvfac_desc",
       "noov:Nvdet_pref",
      (SELECT Valor
        FROM Redondeo_Dian(Stot_Red / Trm, 2)) "noov:Nvfac_stot",
       "noov:Nvimp_cdia",
       "noov:Nvdet_piva",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvdet_viva" / Trm, 2)) "noov:Nvdet_viva",
       "noov:Nvdet_tcod",
       "noov:Nvpro_cean",
       "noov:Nvdet_entr",
       "noov:Nvuni_quan",
       "noov:Nvdet_nota",
       "noov:Nvdet_padr",
       "noov:Nvdet_marc",
       "noov:Nvdet_mode"
FROM Pz_Fe_Detalle(:Tipo, :Prefijo, :Numero)
JOIN V_Trm ON (1 = 1)', 'S', 10, 'NOOVA', 'NOTA DEBITO', 'noov:DTOFacDetallefacturas', NULL, 'noov:Nvfac_dcop', NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (323, 'ges:imp_detalle', 'DOC', '-- Impuesto Detalle Gestion Nota Debito

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_desc",
       "noov:Nvimp_oper",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base", 2)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo", 2)) "noov:Nvimp_valo"
FROM Pz_Fe_Impuestos_Detalle(:Tipo, :Prefijo, :Numero, :Renglon)', 'S', 15, 'NOOVA', 'NOTA DEBITO', 'noov:lImpuestosDetalle', 'noov:DTOFacImpuestosdetallefacturas', NULL, 'ges:detalle')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (324, 'ges:impuestos', 'DOC', '-- Impuestos Gestion Nota Debito

WITH V_Trm
AS (SELECT Valor AS Trm
    FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero))

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_oper",
       "noov:Nvimp_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base" / Trm, 2)) "noov:Nvimp_base",
       "noov:Nvimp_porc" / IIF(Es_Tarifa = ''N'', Trm, 1) "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo" / Trm, 2)) "noov:Nvimp_valo"
FROM Pz_Fe_Impuestos_Sum(:Tipo, :Prefijo, :Numero)
JOIN V_Trm ON (1 = 1)
WHERE "noov:Nvimp_cdia" NOT IN (''07'', ''99'')', 'S', 25, 'NOOVA', 'NOTA DEBITO', 'noov:DTOFacImpuestosfacturas', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (325, 'ges:tasa_cambio', 'DOC', '-- Tasa Cambio Gestion Nota Debito

SELECT FIRST 1 Codigo "noov:Nvtas_sour",
               ''COP'' "noov:Nvtas_targ",
               Valor "noov:Nvtas_calc",
               Fecha "noov:Nvtas_fech"
FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero)
WHERE Codigo <> ''COP''', 'S', 30, 'NOOVA', 'NOTA DEBITO', 'noov:TasaCambio', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (326, 'ges:proveedor', 'DOC', 'SELECT 1
FROM Rdb$Database
WHERE 1 = 0', 'S', 40, 'NOOVA', 'NOTA DEBITO', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (341, 'ges:comprobante', 'DOC', '-- Comprobante Gestion Nota Credito

WITH V_Trm
AS (SELECT Valor AS Trm
    FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero))

SELECT "noov:Nvfac_orig",
       "noov:Nvemp_nnit",
       "noov:Nvres_nume",
       "noov:Nvfac_tipo",
       "noov:Nvres_pref",
       "noov:Nvfac_nume",
       "noov:Nvfac_fech",
       "noov:Nvfac_cdet",
       "noov:Nvfac_venc",
       "noov:Nvsuc_codi",
       "noov:Nvmon_codi",
       IIF("noov:Nvfor_codi" = ''13'', ''2'', "noov:Nvfor_codi") "noov:Nvfor_codi",
       "noov:Nvven_nomb",
       "noov:Nvfac_fpag",
       "noov:Nvfac_conv",
       "noov:Nvcli_cper",
       "noov:Nvcli_cdoc",
       "noov:Nvcli_docu",
       "noov:Nvcli_pais",
       "noov:Nvcli_depa",
       "noov:Nvcli_ciud",
       "noov:Nvcli_loca",
       "noov:Nvcli_dire",
       "noov:Nvcli_ntel",
       "noov:Nvcli_regi",
       "noov:Nvcli_fisc",
       "noov:Nvcli_nomb",
       "noov:Nvcli_pnom",
       "noov:Nvcli_snom",
       "noov:Nvcli_apel",
       "noov:Nvcli_mail",
       "noov:Nvema_copi",
       "noov:Nvfac_obse",
       "noov:Nvfac_orde",
       "noov:Nvfac_remi",
       "noov:Nvfac_rece",
       "noov:Nvfac_entr",
       "noov:Nvfac_ccos",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_stot" / Trm, 2)) "noov:Nvfac_stot",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc" / Trm, 2)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_anti" / Trm, 2)) "noov:Nvfac_anti",
       "noov:Nvfac_carg",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_tota" / Trm, 2)) "noov:Nvfac_tota",
       (SELECT Valor
        FROM Redondeo_Dian(Com_Total / Trm, 2)) "noov:Nvfac_totp",
       (SELECT Valor
        FROM Pz_Fe_Round(:Tipo, :Prefijo, :Numero, Trm)) "noov:Nvfac_roun",
       "noov:Nvfac_vcop",
       "noov:Nvfac_timp",
       "noov:Nvfac_coid",
       "noov:Nvfac_numb",
       "noov:Nvfac_fecb",
       "noov:Nvfac_obsb",
       "noov:Nvcli_ncon",
       "noov:Nvfac_tcru",
       "noov:Nvcon_codi",
       "noov:Nvcon_desc"
FROM Pz_Fe_Comprobantes(:Tipo, :Prefijo, :Numero, :Emisor, :Receptor)
JOIN V_Trm ON (1 = 1)', 'S', 5, 'NOOVA', 'NOTA CREDITO', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (342, 'ges:detalle', 'DOC', '-- Detalle Gestion Nota Credito

WITH V_Trm
AS (SELECT Valor AS Trm
    FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero))

SELECT Renglon,
       Consecutivo,
       "noov:Nvpro_codi",
       "noov:Nvpro_nomb",
       "noov:Nvuni_desc",
       "noov:Nvfac_cant",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_valo" / Trm, 2)) "noov:Nvfac_valo",
       "noov:Nvfac_pdes",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc" / Trm, 2)) "noov:Nvfac_desc",
       "noov:Nvdet_pref",
       (SELECT Valor
        FROM Redondeo_Dian(Stot_Red / Trm, 2)) "noov:Nvfac_stot",
       "noov:Nvimp_cdia",
       "noov:Nvdet_piva",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvdet_viva" / Trm, 2)) "noov:Nvdet_viva",
       "noov:Nvdet_tcod",
       "noov:Nvpro_cean",
       "noov:Nvdet_entr",
       "noov:Nvuni_quan",
       "noov:Nvdet_nota",
       "noov:Nvdet_padr",
       "noov:Nvdet_marc",
       "noov:Nvdet_mode"
FROM Pz_Fe_Detalle(:Tipo, :Prefijo, :Numero)
JOIN V_Trm ON (1 = 1)', 'S', 10, 'NOOVA', 'NOTA CREDITO', 'noov:DTOFacDetallefacturas', NULL, 'noov:Nvfac_dcop', '')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (343, 'ges:imp_detalle', 'DOC', '-- Impuesto Detalle Gestion Nota Credito

WITH V_Trm
AS (SELECT Valor AS Trm
    FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero))

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_desc",
       "noov:Nvimp_oper",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base" / Trm, 2)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo" / Trm, 2)) "noov:Nvimp_valo"
FROM Pz_Fe_Impuestos_Detalle(:Tipo, :Prefijo, :Numero, :Renglon)
JOIN V_Trm ON (1 = 1)', 'S', 15, 'NOOVA', 'NOTA CREDITO', 'noov:lImpuestosDetalle', 'noov:DTOFacImpuestosdetallefacturas', NULL, 'ges:detalle')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (344, 'ges:impuestos', 'DOC', '-- Impuestos Gestion Nota Credito

WITH V_Trm
AS (SELECT Valor AS Trm
    FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero))

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_oper",
       "noov:Nvimp_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base" / Trm, 2)) "noov:Nvimp_base",
       "noov:Nvimp_porc" / IIF(Es_Tarifa = ''N'', Trm, 1) "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo" / Trm, 2)) "noov:Nvimp_valo"
FROM Pz_Fe_Impuestos_Sum(:Tipo, :Prefijo, :Numero)
JOIN V_Trm ON (1 = 1)
WHERE "noov:Nvimp_cdia" NOT IN (''07'', ''99'')', 'S', 25, 'NOOVA', 'NOTA CREDITO', 'noov:DTOFacImpuestosfacturas', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (345, 'ges:tasa_cambio', 'DOC', '-- Tasa Cambio Gestion Nota Credito

SELECT FIRST 1 Codigo "noov:Nvtas_sour",
               ''COP'' "noov:Nvtas_targ",
               Valor "noov:Nvtas_calc",
               Fecha "noov:Nvtas_fech"
FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero)
WHERE Codigo <> ''COP''', 'S', 30, 'NOOVA', 'NOTA CREDITO', 'noov:TasaCambio', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (346, 'ges:sector_salud', 'DOC', 'SELECT 1
FROM Rdb$Database
WHERE 1 = 0', 'S', 35, 'NOOVA', 'NOTA CREDITO', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (347, 'ges:proveedor', 'DOC', 'SELECT 1
FROM Rdb$Database
WHERE 1 = 0', 'S', 40, 'NOOVA', 'NOTA CREDITO', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (361, 'ges:comprobante', 'DOC', '-- Comprobante Gestion Doc Soporte

WITH V_Trm
AS (SELECT Valor AS Trm
    FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero))

SELECT "noov:Nvfac_orig",
       "noov:Nvemp_nnit",
       "noov:Nvres_nume",
       "noov:Nvfac_tipo",
       "noov:Nvres_pref",
       "noov:Nvfac_nume",
       "noov:Nvfac_fech",
       "noov:Nvfac_cdet",
       "noov:Nvfac_venc",
       "noov:Nvsuc_codi",
       "noov:Nvmon_codi",
       "noov:Nvfor_codi",
       "noov:Nvven_nomb",
       "noov:Nvfac_fpag",
       "noov:Nvfac_conv",
       "noov:Nvema_copi",
       "noov:Nvfac_obse",
       "noov:Nvfac_orde",
       "noov:Nvfac_remi",
       "noov:Nvfac_rece",
       "noov:Nvfac_entr",
       "noov:Nvfac_ccos",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_stot" / Trm, 2)) "noov:Nvfac_stot",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc" / Trm, 2)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_anti" / Trm, 2)) "noov:Nvfac_anti",
       "noov:Nvfac_carg",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_tota" / Trm, 2)) "noov:Nvfac_tota",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_totp" / Trm, 2)) "noov:Nvfac_totp",
       (SELECT Valor
        FROM Pz_Fe_Round(:Tipo, :Prefijo, :Numero, Trm)) "noov:Nvfac_roun",
       "noov:Nvfac_vcop",
       "noov:Nvfac_timp",
       "noov:Nvfac_coid",
       "noov:Nvfac_obsb",
       "noov:Nvfor_oper"
FROM Pz_Fe_Comprobantes(:Tipo, :Prefijo, :Numero, :Emisor, :Receptor)
JOIN V_Trm ON (1 = 1)', 'S', 5, 'NOOVA', 'DOCUMENTO SOPORTE', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (362, 'ges:proveedor', 'DOC', '-- Proveedor Doc Soporte

SELECT "noov:Nvpro_cper",
       "noov:Nvpro_cdoc",
       "noov:Nvpro_docu",
       "noov:Nvpro_dive",
       "noov:Nvpro_pais",
       "noov:Nvpro_depa",
       "noov:Nvpro_ciud",
       "noov:Nvpro_loca",
       "noov:Nvpro_dire",
       "noov:Nvpro_ntel",
       "noov:Nvpro_regi",
       "noov:Nvpro_fisc",
       "noov:Nvpro_nomb",
       "noov:Nvpro_pnom",
       "noov:Nvpro_snom",
       "noov:Nvpro_apel",
       "noov:Nvpro_mail",
       "noov:Nvpro_ncon"
FROM Pz_Fe_Comprobantes(:Tipo, :Prefijo, :Numero, :Emisor, :Receptor)', 'S', 10, 'NOOVA', 'DOCUMENTO SOPORTE', 'noov:Proveedor', '', NULL, 'ges:comprobante')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (363, 'ges:detalle', 'DOC', '-- Detalle Gestion Doc Soporte

WITH V_Trm
AS (SELECT Valor AS Trm
    FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero))

SELECT Renglon,
       Consecutivo,
       "noov:Nvpro_codi",
       "noov:Nvpro_nomb",
       "noov:Nvuni_desc",
       "noov:Nvfac_cant",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_valo" / Trm , 2)) "noov:Nvfac_valo",
       "noov:Nvfac_pdes",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc" / Trm , 2)) "noov:Nvfac_desc",
       "noov:Nvdet_pref",
       (SELECT Valor
        FROM Redondeo_Dian(Stot_Red / Trm , 2)) "noov:Nvfac_stot",
       ''00'' "noov:Nvimp_cdia",
       0 "noov:Nvdet_piva",
       0 "noov:Nvdet_viva",
       "noov:Nvdet_tcod",
       "noov:Nvpro_cean",
       "noov:Nvdet_entr",
       "noov:Nvuni_quan",
       "noov:Nvdet_nota",
       "noov:Nvdet_padr",
       "noov:Nvdet_marc",
       "noov:Nvdet_mode"
FROM Pz_Fe_Detalle(:Tipo, :Prefijo, :Numero)
JOIN V_Trm ON (1 = 1)', 'S', 15, 'NOOVA', 'DOCUMENTO SOPORTE', 'noov:DTOFacDetallefacturas', NULL, 'noov:Nvfac_dcop', NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (364, 'ges:impuestos', 'DOC', '-- Impuestos Gestion Documento Soporte

WITH V_Trm
AS (SELECT Valor AS Trm
    FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero))

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_oper",
       "noov:Nvimp_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base" / Trm, 2)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo" / Trm, 2)) "noov:Nvimp_valo"
FROM Pz_Fe_Impuestos_Sum(:Tipo, :Prefijo, :Numero)
JOIN V_Trm ON (1 = 1)
WHERE "noov:Nvimp_oper" = ''R'' And "noov:Nvimp_cdia" NOT IN (''07'', ''99'')', 'S', 25, 'NOOVA', 'DOCUMENTO SOPORTE', 'noov:DTOFacImpuestosfacturas', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (365, 'ges:tasa_cambio', 'DOC', '-- Tasa Cambio Gestion Documento Soporte

SELECT FIRST 1 Codigo "noov:Nvtas_sour",
               ''COP'' "noov:Nvtas_targ",
               Valor "noov:Nvtas_calc",
               Fecha "noov:Nvtas_fech"
FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero)
WHERE Codigo <> ''COP''', 'S', 30, 'NOOVA', 'DOCUMENTO SOPORTE', 'noov:TasaCambio', NULL, NULL, NULL)
                        MATCHING (ID);UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (366, 'ges:sector_salud', 'DOC', 'SELECT 1
FROM Rdb$Database
WHERE 1 = 0', 'S', 35, 'NOOVA', 'DOCUMENTO SOPORTE', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (381, 'ges:comprobante', 'DOC', '-- Comprobante Gestion Nota Ajuste a DS

WITH V_Trm
AS (SELECT Valor AS Trm
    FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero))

SELECT "noov:Nvfac_orig",
       "noov:Nvemp_nnit",
       "noov:Nvres_nume",
       "noov:Nvfac_tipo",
       "noov:Nvres_pref",
       "noov:Nvfac_nume",
       "noov:Nvfac_fech",
       "noov:Nvfac_cdet",
       "noov:Nvfac_venc",
       "noov:Nvsuc_codi",
       "noov:Nvmon_codi",
       IIF("noov:Nvfor_codi" = ''13'', ''2'', "noov:Nvfor_codi") "noov:Nvfor_codi",
       "noov:Nvven_nomb",
       "noov:Nvfac_fpag",
       "noov:Nvfac_conv",
       "noov:Nvema_copi",
       "noov:Nvfac_obse",
       "noov:Nvfac_orde",
       "noov:Nvfac_remi",
       "noov:Nvfac_rece",
       "noov:Nvfac_entr",
       "noov:Nvfac_ccos",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_stot"/Trm, 2)) "noov:Nvfac_stot",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc"/Trm, 2)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_anti"/Trm, 2)) "noov:Nvfac_anti",
       "noov:Nvfac_carg",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_tota"/Trm, 2)) "noov:Nvfac_tota",
       IIF("noov:Nvfac_tipo" IN (''DS'', ''CS''),
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_totp"/Trm, 2)),
       (SELECT Valor
        FROM Redondeo_Dian(Com_Total/Trm, 2))) "noov:Nvfac_totp",
       (SELECT Valor
        FROM Pz_Fe_Round(:Tipo, :Prefijo, :Numero, Trm)) "noov:Nvfac_roun",
       "noov:Nvfac_vcop",
       "noov:Nvfac_timp",
       "noov:Nvfac_coid",
       "noov:Nvfac_numb",
       "noov:Nvfac_fecb",
       "noov:Nvfac_obsb",
       "noov:Nvcli_ncon",
       "noov:Nvfac_tcru",
       "noov:Nvcon_codi",
       "noov:Nvcon_desc"
FROM Pz_Fe_Comprobantes(:Tipo, :Prefijo, :Numero, :Emisor, :Receptor)
JOIN V_Trm ON (1 = 1)', 'S', 5, 'NOOVA', 'NOTA DE AJUSTE A DS', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (382, 'ges:proveedor', 'DOC', '-- Proveedor Nota Ajuste a DS

SELECT "noov:Nvpro_cper",
       "noov:Nvpro_cdoc",
       "noov:Nvpro_docu",
       "noov:Nvpro_dive",
       "noov:Nvpro_pais",
       "noov:Nvpro_depa",
       "noov:Nvpro_ciud",
       "noov:Nvpro_zipc",
       "noov:Nvpro_loca",
       "noov:Nvpro_dire",
       "noov:Nvpro_ntel",
       "noov:Nvpro_regi",
       "noov:Nvpro_fisc",
       "noov:Nvpro_nomb",
       "noov:Nvpro_pnom",
       "noov:Nvpro_snom",
       "noov:Nvpro_apel",
       "noov:Nvpro_mail",
       "noov:Nvpro_ncon"
FROM Pz_Fe_Comprobantes(:Tipo, :Prefijo, :Numero, :Emisor, :Receptor)', 'S', 20, 'NOOVA', 'NOTA DE AJUSTE A DS', 'noov:Proveedor', NULL, NULL, 'ges:comprobante')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (383, 'ges:detalle', 'DOC', '-- Detalle Gestion Nota Ajuste a DS

WITH V_Trm
AS (SELECT Valor AS Trm
    FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero))

SELECT Renglon,
       Consecutivo,
       "noov:Nvpro_codi",
       "noov:Nvpro_nomb",
       "noov:Nvuni_desc",
       "noov:Nvfac_cant",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_valo"/Trm, 2)) "noov:Nvfac_valo",
       "noov:Nvfac_pdes",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc"/Trm, 2)) "noov:Nvfac_desc",
       "noov:Nvdet_pref",
       (SELECT Valor
        FROM Redondeo_Dian(Stot_Red/Trm, 2)) "noov:Nvfac_stot",
       ''00'' "noov:Nvimp_cdia",
       0 "noov:Nvdet_piva",
       0 "noov:Nvdet_viva",
       "noov:Nvdet_tcod",
       "noov:Nvpro_cean",
       "noov:Nvdet_entr",
       "noov:Nvuni_quan",
       "noov:Nvdet_nota",
       "noov:Nvdet_padr",
       "noov:Nvdet_marc",
       "noov:Nvdet_mode"
FROM Pz_Fe_Detalle(:Tipo, :Prefijo, :Numero)
JOIN V_Trm ON (1 = 1)', 'S', 10, 'NOOVA', 'NOTA DE AJUSTE A DS', 'noov:DTOFacDetallefacturas', NULL, 'noov:Nvfac_dcop', NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (384, 'ges:impuestos', 'DOC', '-- Impuestos Gestion Nota Ajuste a DS

WITH V_Trm
AS (SELECT Valor AS Trm
    FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero))

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_oper",
       "noov:Nvimp_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base"/Trm, 2)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo"/Trm, 2)) "noov:Nvimp_valo"
FROM Pz_Fe_Impuestos_Sum(:Tipo, :Prefijo, :Numero)
JOIN V_Trm ON (1 = 1)
WHERE "noov:Nvimp_oper" = ''R'' And "noov:Nvimp_cdia" NOT IN (''07'', ''99'')', 'S', 25, 'NOOVA', 'NOTA DE AJUSTE A DS', 'noov:DTOFacImpuestosfacturas', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (385, 'ges:tasa_cambio', 'DOC', '-- Tasa Cambio Gestion Nota de Ajuste a DS

SELECT FIRST 1 Codigo "noov:Nvtas_sour",
               ''COP'' "noov:Nvtas_targ",
               Valor "noov:Nvtas_calc",
               Fecha "noov:Nvtas_fech"
FROM Pz_Fe_Trm(:Tipo, :Prefijo, :Numero)
WHERE Codigo <> ''COP''', 'S', 20, 'NOOVA', 'NOTA DE AJUSTE A DS', 'noov:TasaCambio', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (386, 'ges:sector_salud', 'DOC', 'SELECT 1
FROM Rdb$Database
WHERE 1 = 0', 'S', 25, 'NOOVA', 'NOTA DE AJUSTE A DS', NULL, NULL, NULL, NULL)
                        MATCHING (ID);


COMMIT WORK;


/******************************************************************************/
/***                           Stored procedures                            ***/
/******************************************************************************/



SET TERM ^ ;

CREATE OR ALTER PROCEDURE PZ_FE_COMPROBANTES (
    TIPO_ VARCHAR(5),
    PREFIJO_ VARCHAR(5),
    NUMERO_ VARCHAR(10),
    EMISOR_ VARCHAR(15),
    RECEPTOR_ VARCHAR(15))
RETURNS (
    "noov:Nvfac_orig" CHAR(1),
    "noov:Nvemp_nnit" VARCHAR(15),
    "noov:Nvres_nume" VARCHAR(20),
    "noov:Nvfac_tipo" VARCHAR(5),
    "noov:Nvfac_tcru" CHAR(1),
    "noov:Nvres_pref" VARCHAR(5),
    "noov:Nvfac_nume" VARCHAR(15),
    "noov:Nvfac_fech" VARCHAR(20),
    "noov:Nvfac_cdet" INTEGER,
    "noov:Nvfac_venc" VARCHAR(20),
    "noov:Nvsuc_codi" CHAR(1),
    "noov:Nvmon_codi" VARCHAR(5),
    "noov:Nvfor_codi" VARCHAR(2),
    "noov:Nvven_nomb" VARCHAR(84),
    "noov:Nvfac_fpag" VARCHAR(5),
    "noov:Nvfac_conv" CHAR(1),
    "noov:Nvcli_cper" CHAR(1),
    "noov:Nvcli_cdoc" VARCHAR(5),
    "noov:Nvcli_docu" VARCHAR(15),
    "noov:Nvcli_pais" VARCHAR(5),
    "noov:Nvcli_depa" VARCHAR(80),
    "noov:Nvcli_ciud" VARCHAR(100),
    "noov:Nvcli_loca" VARCHAR(10),
    "noov:Nvcli_dire" VARCHAR(80),
    "noov:Nvcli_ntel" VARCHAR(200),
    "noov:Nvcli_regi" VARCHAR(5),
    "noov:Nvcli_fisc" VARCHAR(30),
    "noov:Nvcli_nomb" VARCHAR(80),
    "noov:Nvcli_pnom" VARCHAR(20),
    "noov:Nvcli_snom" VARCHAR(20),
    "noov:Nvcli_apel" VARCHAR(40),
    "noov:Nvcli_mail" VARCHAR(300),
    "noov:Nvema_copi" VARCHAR(50),
    "noov:Nvfac_obse" VARCHAR(4000),
    "noov:Nvfac_orde" VARCHAR(40),
    "noov:Nvfac_remi" VARCHAR(40),
    "noov:Nvfac_rece" VARCHAR(40),
    "noov:Nvfac_entr" VARCHAR(40),
    "noov:Nvfac_ccos" VARCHAR(40),
    "noov:Nvfac_stot" DOUBLE PRECISION,
    "noov:Nvfac_desc" DOUBLE PRECISION,
    "noov:Nvfac_anti" DOUBLE PRECISION,
    "noov:Nvfac_carg" DOUBLE PRECISION,
    "noov:Nvfac_tota" DOUBLE PRECISION,
    "noov:Nvfac_totp" DOUBLE PRECISION,
    "noov:Nvfac_vcop" DOUBLE PRECISION,
    "noov:Nvfac_timp" DOUBLE PRECISION,
    "noov:Nvfac_coid" CHAR(1),
    "noov:Nvfac_obsb" CHAR(1),
    "noov:Nvcli_ncon" VARCHAR(80),
    "noov:Nvcon_codi" VARCHAR(2),
    "noov:Nvcon_desc" VARCHAR(200),
    "noov:Nvfac_numb" VARCHAR(20),
    "noov:Nvfac_fecb" VARCHAR(20),
    "noov:Nvfor_oper" VARCHAR(20),
    "noov:Nvpro_cper" CHAR(1),
    "noov:Nvpro_cdoc" VARCHAR(5),
    "noov:Nvpro_docu" VARCHAR(15),
    "noov:Nvpro_pais" VARCHAR(5),
    "noov:Nvpro_depa" VARCHAR(80),
    "noov:Nvpro_ciud" VARCHAR(100),
    "noov:Nvpro_loca" VARCHAR(10),
    "noov:Nvpro_dire" VARCHAR(80),
    "noov:Nvpro_ntel" VARCHAR(200),
    "noov:Nvpro_regi" VARCHAR(5),
    "noov:Nvpro_fisc" VARCHAR(30),
    "noov:Nvpro_nomb" VARCHAR(80),
    "noov:Nvpro_pnom" VARCHAR(20),
    "noov:Nvpro_snom" VARCHAR(20),
    "noov:Nvpro_apel" VARCHAR(40),
    "noov:Nvpro_mail" VARCHAR(300),
    "noov:Nvpro_ncon" VARCHAR(80),
    V_DOCU_REF VARCHAR(20),
    COM_TOTAL DOUBLE PRECISION)
AS
BEGIN
  SUSPEND;
END^





CREATE OR ALTER PROCEDURE PZ_FE_DETALLE (
    TIPO_ CHAR(5),
    PREFIJO_ CHAR(5),
    NUMERO_ CHAR(10))
RETURNS (
    RENGLON INTEGER,
    CONSECUTIVO INTEGER,
    "noov:Nvpro_codi" CHAR(20),
    "noov:Nvpro_nomb" CHAR(198),
    "noov:Nvuni_desc" CHAR(5),
    "noov:Nvfac_cant" DOUBLE PRECISION,
    "noov:Nvfac_valo" DOUBLE PRECISION,
    "noov:Nvimp_cdia" CHAR(5),
    "noov:Nvfac_pdes" DOUBLE PRECISION,
    "noov:Nvfac_desc" DOUBLE PRECISION,
    "noov:Nvdet_pref" CHAR(2),
    "noov:Nvfac_stot" DOUBLE PRECISION,
    "noov:Nvdet_piva" DOUBLE PRECISION,
    "noov:Nvdet_viva" DOUBLE PRECISION,
    "noov:Nvdet_tcod" CHAR(1),
    "noov:Nvpro_cean" CHAR(1),
    "noov:Nvdet_entr" CHAR(1),
    "noov:Nvuni_quan" DOUBLE PRECISION,
    "noov:Nvdet_nota" CHAR(200),
    "noov:Nvdet_padr" INTEGER,
    "noov:Nvdet_marc" CHAR(20),
    "noov:Nvdet_mode" CHAR(20),
    STOT_RED DOUBLE PRECISION)
AS
BEGIN
  SUSPEND;
END^





CREATE OR ALTER PROCEDURE PZ_FE_DETALLE_BASE (
    TIPO_ CHAR(5),
    PREFIJO_ CHAR(5),
    NUMERO_ CHAR(10))
RETURNS (
    RENGLON INTEGER,
    REFERENCIA CHAR(20),
    NOM_REFERENCIA CHAR(80),
    CANTIDAD DOUBLE PRECISION,
    BRUTO DOUBLE PRECISION,
    PORC_DESCUENTO DOUBLE PRECISION,
    DESCUENTO DOUBLE PRECISION,
    AIU DOUBLE PRECISION,
    VALOR_AIU DOUBLE PRECISION,
    STOT DOUBLE PRECISION,
    NOTA CHAR(200),
    MEDIDA CHAR(5),
    LINEA CHAR(5),
    DSI CHAR(1),
    STOT_RED DOUBLE PRECISION)
AS
BEGIN
  SUSPEND;
END^





CREATE OR ALTER PROCEDURE PZ_FE_IMPUESTOS (
    TIPO_ CHAR(5),
    PREFIJO_ CHAR(5),
    NUMERO_ CHAR(10))
RETURNS (
    "noov:Nvimp_cdia" CHAR(5),
    "noov:Nvimp_desc" VARCHAR(80),
    "noov:Nvimp_oper" CHAR(1),
    "noov:Nvimp_base" DOUBLE PRECISION,
    "noov:Nvimp_porc" DOUBLE PRECISION,
    "noov:Nvimp_valo" DOUBLE PRECISION,
    ES_TARIFA CHAR(1),
    CLASE VARCHAR(20))
AS
BEGIN
  SUSPEND;
END^





CREATE OR ALTER PROCEDURE PZ_FE_IMPUESTOS_DETALLE (
    TIPO_ CHAR(5),
    PREFIJO_ CHAR(5),
    NUMERO_ CHAR(10),
    RENGLON_ INTEGER)
RETURNS (
    "noov:Nvimp_cdia" CHAR(5),
    "noov:Nvimp_desc" VARCHAR(80),
    "noov:Nvimp_oper" CHAR(1),
    "noov:Nvimp_base" DOUBLE PRECISION,
    "noov:Nvimp_porc" DOUBLE PRECISION,
    "noov:Nvimp_valo" DOUBLE PRECISION)
AS
BEGIN
  SUSPEND;
END^





CREATE OR ALTER PROCEDURE PZ_FE_IMPUESTOS_RENGLON (
    TIPO_ CHAR(5),
    PREFIJO_ CHAR(5),
    NUMERO_ CHAR(10),
    RENGLON_ INTEGER)
RETURNS (
    "noov:Nvimp_cdia" CHAR(5),
    "noov:Nvimp_desc" VARCHAR(80),
    "noov:Nvimp_oper" CHAR(1),
    "noov:Nvimp_base" DOUBLE PRECISION,
    "noov:Nvimp_porc" DOUBLE PRECISION,
    "noov:Nvimp_valo" DOUBLE PRECISION,
    ES_TARIFA CHAR(1),
    CLASE VARCHAR(20))
AS
BEGIN
  SUSPEND;
END^





CREATE OR ALTER PROCEDURE PZ_FE_IMPUESTOS_SUM (
    TIPO_ CHAR(5),
    PREFIJO_ CHAR(5),
    NUMERO_ CHAR(10))
RETURNS (
    "noov:Nvimp_cdia" CHAR(5),
    "noov:Nvimp_desc" VARCHAR(80),
    "noov:Nvimp_oper" CHAR(1),
    "noov:Nvimp_base" DOUBLE PRECISION,
    "noov:Nvimp_porc" DOUBLE PRECISION,
    "noov:Nvimp_valo" DOUBLE PRECISION,
    VALOR_RED DOUBLE PRECISION,
    ES_TARIFA CHAR(1),
    CLASE VARCHAR(20))
AS
BEGIN
  SUSPEND;
END^





CREATE OR ALTER PROCEDURE PZ_FE_ROUND (
    TIPO_ VARCHAR(5),
    PREFIJO_ VARCHAR(5),
    NUMERO_ VARCHAR(10),
    TRM_ DOUBLE PRECISION)
RETURNS (
    VALOR DOUBLE PRECISION)
AS
BEGIN
  SUSPEND;
END^





CREATE OR ALTER PROCEDURE PZ_FE_TRM (
    TIPO_ CHAR(5),
    PREFIJO_ CHAR(5),
    NUMERO_ CHAR(10))
RETURNS (
    CODIGO CHAR(5),
    VALOR DOUBLE PRECISION,
    FECHA CHAR(10))
AS
BEGIN
  SUSPEND;
END^






SET TERM ; ^



/******************************************************************************/
/***                           Stored procedures                            ***/
/******************************************************************************/



SET TERM ^ ;

CREATE OR ALTER PROCEDURE PZ_FE_COMPROBANTES (
    TIPO_ VARCHAR(5),
    PREFIJO_ VARCHAR(5),
    NUMERO_ VARCHAR(10),
    EMISOR_ VARCHAR(15),
    RECEPTOR_ VARCHAR(15))
RETURNS (
    "noov:Nvfac_orig" CHAR(1),
    "noov:Nvemp_nnit" VARCHAR(15),
    "noov:Nvres_nume" VARCHAR(20),
    "noov:Nvfac_tipo" VARCHAR(5),
    "noov:Nvfac_tcru" CHAR(1),
    "noov:Nvres_pref" VARCHAR(5),
    "noov:Nvfac_nume" VARCHAR(15),
    "noov:Nvfac_fech" VARCHAR(20),
    "noov:Nvfac_cdet" INTEGER,
    "noov:Nvfac_venc" VARCHAR(20),
    "noov:Nvsuc_codi" CHAR(1),
    "noov:Nvmon_codi" VARCHAR(5),
    "noov:Nvfor_codi" VARCHAR(2),
    "noov:Nvven_nomb" VARCHAR(84),
    "noov:Nvfac_fpag" VARCHAR(5),
    "noov:Nvfac_conv" CHAR(1),
    "noov:Nvcli_cper" CHAR(1),
    "noov:Nvcli_cdoc" VARCHAR(5),
    "noov:Nvcli_docu" VARCHAR(15),
    "noov:Nvcli_pais" VARCHAR(5),
    "noov:Nvcli_depa" VARCHAR(80),
    "noov:Nvcli_ciud" VARCHAR(100),
    "noov:Nvcli_loca" VARCHAR(10),
    "noov:Nvcli_dire" VARCHAR(80),
    "noov:Nvcli_ntel" VARCHAR(200),
    "noov:Nvcli_regi" VARCHAR(5),
    "noov:Nvcli_fisc" VARCHAR(30),
    "noov:Nvcli_nomb" VARCHAR(80),
    "noov:Nvcli_pnom" VARCHAR(20),
    "noov:Nvcli_snom" VARCHAR(20),
    "noov:Nvcli_apel" VARCHAR(40),
    "noov:Nvcli_mail" VARCHAR(300),
    "noov:Nvema_copi" VARCHAR(50),
    "noov:Nvfac_obse" VARCHAR(4000),
    "noov:Nvfac_orde" VARCHAR(40),
    "noov:Nvfac_remi" VARCHAR(40),
    "noov:Nvfac_rece" VARCHAR(40),
    "noov:Nvfac_entr" VARCHAR(40),
    "noov:Nvfac_ccos" VARCHAR(40),
    "noov:Nvfac_stot" DOUBLE PRECISION,
    "noov:Nvfac_desc" DOUBLE PRECISION,
    "noov:Nvfac_anti" DOUBLE PRECISION,
    "noov:Nvfac_carg" DOUBLE PRECISION,
    "noov:Nvfac_tota" DOUBLE PRECISION,
    "noov:Nvfac_totp" DOUBLE PRECISION,
    "noov:Nvfac_vcop" DOUBLE PRECISION,
    "noov:Nvfac_timp" DOUBLE PRECISION,
    "noov:Nvfac_coid" CHAR(1),
    "noov:Nvfac_obsb" CHAR(1),
    "noov:Nvcli_ncon" VARCHAR(80),
    "noov:Nvcon_codi" VARCHAR(2),
    "noov:Nvcon_desc" VARCHAR(200),
    "noov:Nvfac_numb" VARCHAR(20),
    "noov:Nvfac_fecb" VARCHAR(20),
    "noov:Nvfor_oper" VARCHAR(20),
    "noov:Nvpro_cper" CHAR(1),
    "noov:Nvpro_cdoc" VARCHAR(5),
    "noov:Nvpro_docu" VARCHAR(15),
    "noov:Nvpro_dive" VARCHAR(1),
    "noov:Nvpro_pais" VARCHAR(5),
    "noov:Nvpro_depa" VARCHAR(80),
    "noov:Nvpro_ciud" VARCHAR(100),
    "noov:Nvpro_zipc" VARCHAR(6),
    "noov:Nvpro_loca" VARCHAR(10),
    "noov:Nvpro_dire" VARCHAR(80),
    "noov:Nvpro_ntel" VARCHAR(200),
    "noov:Nvpro_regi" VARCHAR(5),
    "noov:Nvpro_fisc" VARCHAR(30),
    "noov:Nvpro_nomb" VARCHAR(80),
    "noov:Nvpro_pnom" VARCHAR(20),
    "noov:Nvpro_snom" VARCHAR(20),
    "noov:Nvpro_apel" VARCHAR(40),
    "noov:Nvpro_mail" VARCHAR(300),
    "noov:Nvpro_ncon" VARCHAR(80),
    V_DOCU_REF VARCHAR(20),
    COM_TOTAL DOUBLE PRECISION)
AS
DECLARE VARIABLE V_Docu            VARCHAR(20);
DECLARE VARIABLE V_Caiu            INTEGER;
DECLARE VARIABLE V_Dsi             INTEGER;
DECLARE VARIABLE V_Mand            INTEGER;
DECLARE VARIABLE V_Cod_Vendedor    VARCHAR(15);
DECLARE VARIABLE V_Tesoreria       VARCHAR(10);
DECLARE VARIABLE V_Formapago       VARCHAR(5);
DECLARE VARIABLE V_Formapago_Fe    VARCHAR(5);
DECLARE VARIABLE V_Codbanco        VARCHAR(15);
DECLARE VARIABLE V_Codbanco_Fe     VARCHAR(5);
DECLARE VARIABLE V_Naturaleza      CHAR(1);
DECLARE VARIABLE V_Codsociedad     VARCHAR(5);
DECLARE VARIABLE V_Email_Ter       VARCHAR(50);
DECLARE VARIABLE V_Codsucursal     VARCHAR(15);
DECLARE VARIABLE V_Email_Suc       VARCHAR(50);
DECLARE VARIABLE V_Email_Otro      VARCHAR(100);
DECLARE VARIABLE V_Nota            VARCHAR(80);
DECLARE VARIABLE V_Meganota        VARCHAR(4000);
DECLARE VARIABLE V_Copag           INTEGER;
DECLARE VARIABLE V_Copago          DOUBLE PRECISION;
DECLARE VARIABLE V_Impuestos       DOUBLE PRECISION;
DECLARE VARIABLE V_Pais            VARCHAR(5);
DECLARE VARIABLE V_Municipio       VARCHAR(5);
DECLARE VARIABLE V_Tipo_Ref        VARCHAR(5);
DECLARE VARIABLE V_Prefijo_Ref     VARCHAR(5);
DECLARE VARIABLE V_Numero_Ref      VARCHAR(10);
DECLARE VARIABLE V_Fecha_Ref       VARCHAR(20);
DECLARE VARIABLE V_Fecha_Fac       VARCHAR(20);
DECLARE VARIABLE V_Cufe_Ref        VARCHAR(100);
DECLARE VARIABLE V_Valida_Fe       CHAR(1);
DECLARE VARIABLE V_Concepto_Nc     VARCHAR(2);
DECLARE VARIABLE V_Nom_Concepto_Nc VARCHAR(200);
DECLARE VARIABLE V_Trm_Codigo      VARCHAR(5);
DECLARE VARIABLE V_Stot_Red        DOUBLE PRECISION;
DECLARE VARIABLE V_Nvfac_Stot      DOUBLE PRECISION;
DECLARE VARIABLE V_Descuento_Det   DOUBLE PRECISION;
DECLARE VARIABLE V_Siif            VARCHAR(80);
DECLARE VARIABLE V_Fecre           INTEGER;
DECLARE VARIABLE V_Dscor           VARCHAR(50);
DECLARE VARIABLE V_Postal_Code     VARCHAR(6);
BEGIN
  --Trm
  SELECT Codigo
  FROM Pz_Fe_Trm(:Tipo_, :Prefijo_, :Numero_)
  INTO V_Trm_Codigo;

  -- Si es NC o ND se toman datos del doc referencia
  SELECT Tiporef,
         Prefijoref,
         Numeroref
  FROM Notas_Gestion
  WHERE Tipo = :Tipo_
        AND Prefijo = :Prefijo_
        AND Numero = :Numero_
  INTO V_Tipo_Ref,
       V_Prefijo_Ref,
       V_Numero_Ref;

  V_Tipo_Ref = COALESCE(V_Tipo_Ref, '');
  V_Prefijo_Ref = COALESCE(V_Prefijo_Ref, '');
  V_Numero_Ref = COALESCE(V_Numero_Ref, '');

  SELECT EXTRACT(YEAR FROM Fecha) || '-' || RIGHT(EXTRACT(MONTH FROM Fecha) + 100, 2) || '-' || RIGHT(EXTRACT(DAY FROM Fecha) + 100, 2) || 'T' || COALESCE(SUBSTRING((SUBSTRING(Impreso FROM 12 FOR 15)) FROM 1 FOR 8), '00:00:00')
  FROM Comprobantes
  WHERE Tipo = :V_Tipo_Ref
        AND Prefijo = :V_Prefijo_Ref
        AND Numero = :V_Numero_Ref
  INTO V_Fecha_Ref;
  -- V_Fecha_Ref = COALESCE(V_Fecha_Ref, '');

  SELECT Cufe,
         EXTRACT(YEAR FROM Fecha_Envio) || '-' || RIGHT(EXTRACT(MONTH FROM Fecha_Envio) + 100, 2) || '-' || RIGHT(EXTRACT(DAY FROM Fecha_Envio) + 100, 2) || 'T' || COALESCE(SUBSTRING((SUBSTRING(Fecha_Envio FROM 12 FOR 15)) FROM 1 FOR 8), '00:00:00')
  FROM Facturas
  WHERE Tipo = :V_Tipo_Ref
        AND Prefijo = :V_Prefijo_Ref
        AND Numero = :V_Numero_Ref
  INTO V_Cufe_Ref,
       V_Fecha_Fac;
  V_Cufe_Ref = COALESCE(V_Cufe_Ref, '');
  V_Fecha_Ref = COALESCE(V_Fecha_Ref, V_Fecha_Fac);

  SELECT Codigo_Fe
  FROM Documentos
  WHERE Codigo = :V_Tipo_Ref
  INTO V_Docu_Ref;
  V_Docu_Ref = COALESCE(V_Docu_Ref, '');

  --Comprobantes
  SELECT COALESCE(Copago, 0),
         EXTRACT(YEAR FROM Fecha) || '-' || RIGHT(EXTRACT(MONTH FROM Fecha) + 100, 2) || '-' || RIGHT(EXTRACT(DAY FROM Fecha) + 100, 2) || 'T00:00:00',
         EXTRACT(YEAR FROM Vence) || '-' || RIGHT(EXTRACT(MONTH FROM Vence) + 100, 2) || '-' || RIGHT(EXTRACT(DAY FROM Vence) + 100, 2) || 'T00:00:00',
         Codvendedor,
         Codbanco,
         Codsucursal,
         Nota,
         Concepto
  FROM Comprobantes
  WHERE Tipo = :Tipo_
        AND Prefijo = :Prefijo_
        AND Numero = :Numero_
  INTO V_Copago,
       "noov:Nvfac_fech",
       "noov:Nvfac_venc",
       V_Cod_Vendedor,
       V_Codbanco,
       V_Codsucursal,
       V_Nota,
       V_Concepto_Nc;
  V_Copago = COALESCE(V_Copago, 0);

  --Documentos
  SELECT TRIM(Codigo_Fe),
         Tesoreria,
         Valida_Fe
  FROM Documentos
  WHERE Codigo = :Tipo_
  INTO V_Docu,
       V_Tesoreria,
       V_Valida_Fe;

  --Resolucion
  SELECT Numero
  FROM Lee_Resolucion(:Tipo_, :Prefijo_, :Numero_)
  INTO "noov:Nvres_nume";
  "noov:Nvres_nume" = COALESCE("noov:Nvres_nume", '0');

  -- Cantidad detalles
  SELECT COUNT(1)
  FROM Pz_Fe_Detalle_Base(:Tipo_, :Prefijo_, :Numero_)
  INTO "noov:Nvfac_cdet";

  --AIU
  SELECT COUNT(1)
  FROM Pz_Fe_Detalle_Base(:Tipo_, :Prefijo_, :Numero_)
  WHERE Linea STARTING WITH 'AIU'
  INTO V_Caiu;

  --Mandatos
  SELECT COUNT(1)
  FROM Tr_Inventario D
  JOIN Centros C ON (D.Codcentro = C.Codigo)
  WHERE D.Tipo = :Tipo_
        AND D.Prefijo = :Prefijo_
        AND D.Numero = :Numero_
        AND TRIM(COALESCE(C.Codtercero, '')) <> ''
  INTO V_Mand;

  --DSI
  SELECT COUNT(1)
  FROM Tr_Inventario
  WHERE Tipo = :Tipo_
        AND Prefijo = :Prefijo_
        AND Numero = :Numero_
        AND Dsi = 'S'
  INTO V_Dsi;

  --Valida si maneja copago o anticipo
  SELECT COUNT(1)
  FROM Terceros T
  JOIN Contactos C ON (C.Codtercero = T.Codigo)
  WHERE T.Datos_Empresa = 'S'
        AND C.Codcargo = 'COPAG'
  INTO V_Copag;

  --Contacto
  SELECT FIRST 1 TRIM(Nombre)
  FROM Contactos
  WHERE Codtercero = :Receptor_
        AND Codcargo = 'CONTC'
  INTO "noov:Nvcli_ncon";

  --Contacto SIIF nacion
  SELECT FIRST 1 '#$' || TRIM(Nombre) || ';' || TRIM(Email) || '$#'
  FROM Contactos
  WHERE Codtercero = :Receptor_
        AND Codcargo = 'SIIF'
  INTO V_Siif;

  "noov:Nvcli_ncon" = COALESCE("noov:Nvcli_ncon", '');

  --Otro Email
  SELECT FIRST 1 Email
  FROM Contactos
  WHERE Codtercero = :Receptor_
        AND Codcargo = 'ZZZ'
  INTO V_Email_Otro;
  V_Email_Otro = COALESCE(V_Email_Otro, '');

  --Contacto FECRE Facturacion solo credito
  SELECT COUNT(1)
  FROM Contactos
  WHERE Codtercero = :Emisor_
        AND Codcargo = 'FECRE'
  INTO V_Fecre;

  --Contacto DSCOR correo Documentos Soporte
  SELECT FIRST 1 Email
  FROM Contactos
  WHERE Codtercero = :Emisor_
        AND Codcargo = 'DSCOR'
  INTO V_Dscor;
  V_Dscor = COALESCE(V_Dscor, 'documentos.soporte.mekano@gmail.com');

  --Vendedor
  SELECT TRIM(Nombre_Empleado)
  FROM Fn_Nombre_Empleado(:V_Cod_Vendedor)
  INTO "noov:Nvven_nomb";

  "noov:Nvven_nomb" = COALESCE("noov:Nvven_nomb", '');

  --Forma Pago
  SELECT FIRST 1 TRIM(Codformaspago) Forma_De_Pago
  FROM Reg_Pagos
  WHERE Tipo = :Tipo_
        AND Prefijo = :Prefijo_
        AND Numero = :Numero_
  ORDER BY Valor DESC
  INTO V_Formapago;

  IF (:V_Formapago IS NOT NULL) THEN
    SELECT TRIM(Codigo_Fe)
    FROM Formaspago
    WHERE Codigo = :V_Formapago
    INTO V_Formapago_Fe;

  V_Formapago_Fe = COALESCE(V_Formapago_Fe, '');

  --Banco
  IF (V_Codbanco IS NOT NULL) THEN
    SELECT Codigo_Fe
    FROM Bancos
    WHERE Codigo = :V_Codbanco
    INTO V_Codbanco_Fe;

  V_Codbanco_Fe = COALESCE(V_Codbanco_Fe, '');

  --Terceros
  SELECT Naturaleza,
         Codidentidad,
         TRIM(Codigo) || IIF(TRIM(Codidentidad) = '31', IIF(Dv IS NULL, '', '-' || TRIM(Dv)), ''),
         TRIM(COALESCE(Direccion, '')),
         IIF(COALESCE(TRIM(Telefono), '') = '', '', TRIM(Telefono) || ', ') || COALESCE(TRIM(Movil), ''),
         Codsociedad,
         COALESCE(TRIM(Empresa), ''),
         COALESCE(TRIM(Nom1), ''),
         COALESCE(TRIM(Nom2), ''),
         COALESCE(TRIM(Apl1), '') || ' ' || COALESCE(TRIM(Apl2), ''),
         COALESCE(TRIM(Email), ''),
         COALESCE(Codpais, ''),
         COALESCE(Codmunicipio, ''),
         COALESCE(Codigo_Postal, SUBSTRING(Codmunicipio FROM 1 FOR 2) || '0001')
  FROM Terceros
  WHERE Codigo = :Receptor_
  INTO V_Naturaleza,
       "noov:Nvcli_cdoc",
       "noov:Nvcli_docu",
       "noov:Nvcli_dire",
       "noov:Nvcli_ntel",
       V_Codsociedad,
       "noov:Nvcli_nomb",
       "noov:Nvcli_pnom",
       "noov:Nvcli_snom",
       "noov:Nvcli_apel",
       V_Email_Ter,
       V_Pais,
       V_Municipio,
       V_Postal_Code;

  -- Sociedad
  SELECT COALESCE(Codigo_Fe, ''),
         IIF(UPPER(TRIM(Nombre)) LIKE '%TRIBUTACION SIMPLE%', 'O-47', IIF(Autoretenedor = 'S', 'O-15' || IIF(Jerarquia > 5, ';O-13', ''), IIF(Jerarquia > 5, 'O-13', 'R-99-PN')))
  FROM Sociedades
  WHERE Codigo = :V_Codsociedad
  INTO "noov:Nvcli_regi",
       "noov:Nvcli_fisc";

  --Sucursales
  SELECT COALESCE(Email, '')
  FROM Sucursales
  WHERE Codigo = :V_Codsucursal
  INTO V_Email_Suc;

  --Pais
  SELECT TRIM(Codigo_Fe)
  FROM Paises
  WHERE Codigo = :V_Pais
  INTO "noov:Nvcli_pais";

  --Departamento
  SELECT TRIM(Nombre)
  FROM Municipios
  WHERE Codigo = SUBSTRING(:V_Municipio FROM 1 FOR 2)
  INTO "noov:Nvcli_depa";

  --Ciudad
  SELECT TRIM(Nombre) || IIF(TRIM(:"noov:Nvcli_pais") = 'CO', '@' || TRIM(CAST(:V_Municipio AS CHAR(5))), '')
  FROM Municipios
  WHERE Codigo = :V_Municipio
  INTO "noov:Nvcli_ciud";

  --Mega Nota
  SELECT LEFT(Nota, 4000)
  FROM Tr_Notas
  WHERE Tipo = :Tipo_
        AND Prefijo = :Prefijo_
        AND Numero = :Numero_
  INTO V_Meganota;
  V_Meganota = COALESCE(V_Meganota, '');

  -- Valida si maneja SIIF Nacion
  IF (TRIM(COALESCE(V_Siif, '')) <> '') THEN
    IF (POSITION(V_Siif, V_Meganota) = 0) THEN
    BEGIN
      V_Meganota = TRIM(V_Meganota) || ASCII_CHAR(13) || TRIM(V_Siif);

      UPDATE OR INSERT INTO Tr_Notas (Tipo, Prefijo, Numero, Nota)
      VALUES (:Tipo_, :Prefijo_, :Numero_, TRIM(:V_Meganota));
    END

  --Anexos
  SELECT Orden,
         Remision,
         Recepcion,
         Ean,
         Centro
  FROM Anexo_Gestion
  WHERE Tipo = :Tipo_
        AND Prefijo = :Prefijo_
        AND Numero = :Numero_
  INTO "noov:Nvfac_orde",
       "noov:Nvfac_remi",
       "noov:Nvfac_rece",
       "noov:Nvfac_entr",
       "noov:Nvfac_ccos";

  --Conceptos Nota Credito
  IF (V_Docu = 'NOTA CREDITO') THEN
    SELECT Nombre
    FROM Notas_Cre
    WHERE Codigo = :V_Concepto_Nc
    INTO V_Nom_Concepto_Nc;

  IF (V_Docu = 'NOTA DEBITO') THEN
    SELECT Nombre
    FROM Notas_Deb
    WHERE Codigo = :V_Concepto_Nc
    INTO V_Nom_Concepto_Nc;

  IF (V_Docu = 'NOTA DE AJUSTE A DS') THEN
    SELECT Nombre
    FROM Notas_Ds
    WHERE Codigo = :V_Concepto_Nc
    INTO V_Nom_Concepto_Nc;

  "noov:Nvfac_orde" = COALESCE("noov:Nvfac_orde", '');
  "noov:Nvfac_remi" = COALESCE("noov:Nvfac_remi", '');
  "noov:Nvfac_rece" = COALESCE("noov:Nvfac_rece", '');
  "noov:Nvfac_entr" = COALESCE("noov:Nvfac_entr", '');
  "noov:Nvfac_ccos" = COALESCE("noov:Nvfac_ccos", '');

  "noov:Nvfac_orig" = 'E';
  "noov:Nvemp_nnit" = :Emisor_;
  "noov:Nvfac_tipo" = CASE TRIM(V_Docu)
                        WHEN 'FACTURA' THEN 'FV'
                        WHEN 'CONTINGENCIA' THEN 'FC'
                        WHEN 'EXPORTACION' THEN 'FE'
                        WHEN 'NOTA CREDITO' THEN CASE TRIM(V_Docu_Ref)
                                                   WHEN 'EXPORTACION' THEN 'CE'
                                                   WHEN 'CONTINGENCIA' THEN 'CC'
                                                   ELSE 'CV'
                                                 END
                        WHEN 'NOTA DEBITO' THEN CASE TRIM(V_Docu_Ref)
                                                  WHEN 'EXPORTACION' THEN 'DE'
                                                  WHEN 'CONTINGENCIA' THEN 'DC'
                                                  ELSE 'DV'
                                                END
                        WHEN 'DOCUMENTO SOPORTE' THEN 'DS'
                        WHEN 'NOTA DE AJUSTE A DS' THEN 'CS'

                      END;

  "noov:Nvres_pref" = TRIM(Prefijo_);
  IF (TRIM(Prefijo_) = '_') THEN
    "noov:Nvres_pref" = '';
  "noov:Nvfac_nume" = TRIM("noov:Nvres_pref") || TRIM(Numero_);

  "noov:Nvsuc_codi" = '1';
  "noov:Nvmon_codi" = V_Trm_Codigo;
  "noov:Nvfor_codi" = CASE TRIM(V_Docu)
                        WHEN 'FACTURA' THEN CASE
                                              WHEN V_Caiu <> 0 THEN '11'
                                              WHEN V_Mand <> 0 THEN '12'
                                              ELSE '1'
                                            END
                        WHEN 'CONTINGENCIA' THEN CASE
                                                   WHEN V_Caiu <> 0 THEN '15'
                                                   WHEN V_Mand <> 0 THEN '16'
                                                   ELSE '7'
                                                 END
                        WHEN 'EXPORTACION' THEN IIF("noov:Nvcli_pais" = 'CO', '10', '4')
                        WHEN 'NOTA CREDITO' THEN CASE TRIM(V_Docu_Ref)
                                                   WHEN 'EXPORTACION' THEN '5'
                                                   WHEN 'FACTURA' THEN IIF(V_Caiu > 0, '13', '2')
                                                   WHEN 'CONTINGENCIA' THEN IIF(V_Caiu > 0, '14', '8')
                                                   ELSE '2'
                                                 END
                        WHEN 'NOTA DEBITO' THEN CASE TRIM(V_Docu_Ref)
                                                  WHEN 'EXPORTACION' THEN '6'
                                                  WHEN 'FACTURA' THEN '3'
                                                  ELSE '3'
                                                END
                        WHEN 'DOCUMENTO SOPORTE' THEN '20'
                        WHEN 'NOTA DE AJUSTE A DS' THEN '21'
                      END;

  "noov:Nvfac_fpag" = CASE
                        WHEN TRIM(V_Formapago_Fe) <> '' THEN V_Formapago_Fe
                        WHEN TRIM(V_Codbanco_Fe) <> '' THEN V_Codbanco_Fe
                        ELSE 'ZZZ'
                      END;

  -- SI tiene contacto FECRE, se valida fechas
  IF (V_Fecre = 0) THEN
  BEGIN
    IF (V_Tesoreria = 'NO') THEN
      "noov:Nvfac_conv" = '2';
    ELSE
      "noov:Nvfac_conv" = '1';
  END
  ELSE
  BEGIN
    IF ("noov:Nvfac_fech" <> "noov:Nvfac_venc") THEN
      "noov:Nvfac_conv" = '2';
    ELSE
      "noov:Nvfac_conv" = '1';
  END

  IF (V_Naturaleza = 'N') THEN
    "noov:Nvcli_cper" = '2';
  ELSE
    "noov:Nvcli_cper" = '1';

  "noov:Nvcli_loca" = '';
  "noov:Nvcli_mail" = TRIM(V_Email_Ter) || IIF(COALESCE(V_Email_Suc, '') = '', IIF(COALESCE(V_Email_Otro, '') = '', '', ';' || TRIM(V_Email_Otro)), IIF(TRIM(V_Email_Ter) = TRIM(V_Email_Suc), '', ';' || TRIM(V_Email_Suc)));
  "noov:Nvema_copi" = '';

  "noov:Nvfac_obse" = SUBSTRING(IIF(TRIM(V_Nota) IN ('-', '.'), '', TRIM(V_Nota)) || IIF(TRIM(V_Meganota) IS NULL OR TRIM(V_Meganota) = 'Por favor no olvide presionar F3 para guardar la Nota', '', IIF(TRIM(V_Nota) IN ('-', '.'), '', ', ') || TRIM(V_Meganota)) FROM 1 FOR 4000);

  SELECT SUM(IIF("noov:Nvfac_pdes" = 100, "noov:Nvfac_desc", 0))
  FROM Pz_Fe_Detalle(:Tipo_, :Prefijo_, :Numero_)
  INTO V_Descuento_Det;

  V_Descuento_Det = COALESCE(V_Descuento_Det, 0);

  SELECT SUM("noov:Nvfac_stot")
  FROM Pz_Fe_Detalle(:Tipo_, :Prefijo_, :Numero_)
  INTO V_Nvfac_Stot;

  "noov:Nvfac_stot" = V_Nvfac_Stot;

  "noov:Nvfac_desc" = 0;
  "noov:Nvfac_anti" = 0;

  IF (V_Copag = 0) THEN
    "noov:Nvfac_desc" = V_Copago;
  ELSE
    "noov:Nvfac_anti" = V_Copago;

  "noov:Nvfac_desc" = "noov:Nvfac_desc" + V_Descuento_Det;
  "noov:Nvfac_carg" = 0;
  "noov:Nvfac_tota" = "noov:Nvfac_stot";

  --IMPUESTOS
  SELECT SUM("noov:Nvimp_valo")
  FROM Pz_Fe_Impuestos_Sum(:Tipo_, :Prefijo_, :Numero_)
  WHERE "noov:Nvimp_oper" = 'S'
  INTO V_Impuestos;

  V_Impuestos = COALESCE(V_Impuestos, 0);

  SELECT SUM(Stot_Red)
  FROM Pz_Fe_Detalle_Base(:Tipo_, :Prefijo_, :Numero_)
  INTO V_Stot_Red;

  "noov:Nvfac_totp" = V_Stot_Red + V_Impuestos - "noov:Nvfac_desc";

  "noov:Nvfor_oper" = '';
  IF ("noov:Nvfac_anti" > 0) THEN
  BEGIN
    IF (V_Docu = 'NOTA CREDITO') THEN
    BEGIN
      "noov:Nvfac_desc" = "noov:Nvfac_anti";
      "noov:Nvfac_anti" = 0;
    END
    ELSE
      "noov:Nvfac_totp" = "noov:Nvfac_totp" - "noov:Nvfac_anti";
    "noov:Nvfor_oper" = 'SS-CUFE';
  END

  -- DSI
  IF (V_Dsi > 0 AND
      TRIM(V_Docu) = 'CONTINGENCIA') THEN
    "noov:Nvfor_oper" = '20';

  --  SELECT Valor
  --  FROM Redondeo_Dian(:"noov:Nvfac_totp", 2)
  --  INTO "noov:Nvfac_totp";

  SELECT SUM(Valor_Red)
  FROM Pz_Fe_Impuestos_Sum(:Tipo_, :Prefijo_, :Numero_)
  WHERE "noov:Nvimp_oper" = 'S'
  INTO V_Impuestos;
  V_Impuestos = COALESCE(V_Impuestos, 0);

  /* Total Comprobantes, para cuadrar valor exacto
mekano vs xml, diferencia se lleva por redondeo */
  SELECT (Bruto + Impuestos - Descuentos - Copago)
  FROM Comprobantes
  WHERE (Tipo = :Tipo_)
        AND (Prefijo = :Prefijo_)
        AND (Numero = :Numero_)
  INTO Com_Total;

  "noov:Nvfac_vcop" = 0;
  "noov:Nvfac_timp" = 0;
  "noov:Nvfac_coid" = '';
  "noov:Nvfac_numb" = '';
  "noov:Nvfac_obsb" = '';
  "noov:Nvcon_codi" = '';
  "noov:Nvcon_desc" = '';
  "noov:Nvfac_tcru" = '';
  "noov:Nvfac_numb" = '';

  IF (V_Docu IN ('NOTA CREDITO', 'NOTA DEBITO', 'NOTA DE AJUSTE A DS')) THEN
  BEGIN
    "noov:Nvcon_codi" = V_Concepto_Nc;
    "noov:Nvcon_desc" = TRIM(V_Nom_Concepto_Nc);
    IF (V_Valida_Fe = 'N') THEN
      "noov:Nvfac_tcru" = 'L';
    ELSE
      "noov:Nvfac_tcru" = 'R';

    IF (TRIM(V_Prefijo_Ref) <> '_') THEN
      "noov:Nvfac_numb" = TRIM(V_Prefijo_Ref);
    "noov:Nvfac_numb" = TRIM("noov:Nvfac_numb") || TRIM(V_Numero_Ref);
    "noov:Nvfac_fecb" = TRIM(COALESCE(V_Fecha_Ref, "noov:Nvfac_fech"));
  END

  IF (V_Docu IN ('DOCUMENTO SOPORTE', 'NOTA DE AJUSTE A DS')) THEN
  BEGIN
    "noov:Nvpro_cper" = "noov:Nvcli_cper";
    "noov:Nvpro_cdoc" = "noov:Nvcli_cdoc";
    "noov:Nvpro_docu" = TRIM(Receptor_);
    "noov:Nvpro_dive" = '';

    IF ("noov:Nvpro_cdoc" = '31') THEN
      "noov:Nvpro_dive" = RIGHT("noov:Nvcli_docu", 1);

    "noov:Nvpro_pais" = "noov:Nvcli_pais";
    "noov:Nvpro_depa" = "noov:Nvcli_depa";
    "noov:Nvpro_ciud" = "noov:Nvcli_ciud";
    "noov:Nvpro_zipc" = V_Postal_Code;
    "noov:Nvpro_loca" = "noov:Nvcli_loca";
    "noov:Nvpro_dire" = "noov:Nvcli_dire";
    "noov:Nvpro_ntel" = "noov:Nvcli_ntel";
    "noov:Nvpro_regi" = "noov:Nvcli_regi";
    "noov:Nvpro_fisc" = "noov:Nvcli_fisc";
    "noov:Nvpro_nomb" = "noov:Nvcli_nomb";
    "noov:Nvpro_pnom" = "noov:Nvcli_pnom";
    "noov:Nvpro_snom" = "noov:Nvcli_snom";
    "noov:Nvpro_apel" = "noov:Nvcli_apel";
    "noov:Nvpro_mail" = V_Dscor;
    "noov:Nvpro_ncon" = "noov:Nvcli_ncon";
    "noov:Nvcli_cper" = '';
    "noov:Nvcli_cdoc" = '';
    "noov:Nvcli_docu" = '';
    "noov:Nvcli_pais" = '';
    "noov:Nvcli_depa" = '';
    "noov:Nvcli_ciud" = '';
    "noov:Nvcli_loca" = '';
    "noov:Nvcli_dire" = '';
    "noov:Nvcli_ntel" = '';
    "noov:Nvcli_regi" = '';
    "noov:Nvcli_fisc" = '';
    "noov:Nvcli_nomb" = '';
    "noov:Nvcli_pnom" = '';
    "noov:Nvcli_snom" = '';
    "noov:Nvcli_apel" = '';
    "noov:Nvcli_mail" = '';
    "noov:Nvcli_ncon" = '';
  END
  SUSPEND;

END^


CREATE OR ALTER PROCEDURE PZ_FE_DETALLE (
    TIPO_ CHAR(5),
    PREFIJO_ CHAR(5),
    NUMERO_ CHAR(10))
RETURNS (
    RENGLON INTEGER,
    CONSECUTIVO INTEGER,
    "noov:Nvpro_codi" CHAR(20),
    "noov:Nvpro_nomb" CHAR(198),
    "noov:Nvuni_desc" CHAR(5),
    "noov:Nvfac_cant" DOUBLE PRECISION,
    "noov:Nvfac_valo" DOUBLE PRECISION,
    "noov:Nvimp_cdia" CHAR(5),
    "noov:Nvfac_pdes" DOUBLE PRECISION,
    "noov:Nvfac_desc" DOUBLE PRECISION,
    "noov:Nvdet_pref" CHAR(2),
    "noov:Nvfac_stot" DOUBLE PRECISION,
    "noov:Nvdet_piva" DOUBLE PRECISION,
    "noov:Nvdet_viva" DOUBLE PRECISION,
    "noov:Nvdet_tcod" CHAR(1),
    "noov:Nvpro_cean" CHAR(1),
    "noov:Nvdet_entr" CHAR(1),
    "noov:Nvuni_quan" DOUBLE PRECISION,
    "noov:Nvdet_nota" CHAR(200),
    "noov:Nvdet_padr" INTEGER,
    "noov:Nvdet_marc" CHAR(20),
    "noov:Nvdet_mode" CHAR(20),
    STOT_RED DOUBLE PRECISION)
AS
DECLARE VARIABLE V_Nota      CHAR(200);
DECLARE VARIABLE V_Bruto     DOUBLE PRECISION;
DECLARE VARIABLE V_Valor_Aiu DOUBLE PRECISION;
DECLARE VARIABLE V_Linea     CHAR(5);
DECLARE VARIABLE V_Dsi       CHAR(1);
DECLARE VARIABLE V_Num_Linea INTEGER;
DECLARE VARIABLE V_Trm_Valor DOUBLE PRECISION;
DECLARE VARIABLE V_Valo      DOUBLE PRECISION;
DECLARE VARIABLE V_Desc      DOUBLE PRECISION;
BEGIN

  V_Num_Linea = 0;
  FOR SELECT Renglon,
             Referencia,
             Nom_Referencia,
             Nota,
             Medida,
             Cantidad,
             Bruto,
             Porc_Descuento,
             Descuento,
             Valor_Aiu,
             Stot_Red,
             Linea,
             Dsi
      FROM Pz_Fe_Detalle_Base(:Tipo_, :Prefijo_, :Numero_)
      INTO Renglon,
           "noov:Nvpro_codi",
           "noov:Nvpro_nomb",
           V_Nota,
           "noov:Nvuni_desc",
           "noov:Nvfac_cant",
           V_Bruto,
           "noov:Nvfac_pdes",
           "noov:Nvfac_desc",
           V_Valor_Aiu,
           "noov:Nvfac_stot",
           V_Linea,
           V_Dsi
  DO
  BEGIN
    Consecutivo = 1;
    --impuestos
    "noov:Nvimp_cdia" = '00';
    "noov:Nvdet_piva" = 0;
    "noov:Nvdet_viva" = 0;

    IF (V_Linea = 'INC' AND
        V_Bruto > 0 AND
        V_Num_Linea > 0) THEN
      SELECT SKIP 1 "noov:Nvimp_cdia",
                    "noov:Nvimp_porc",
                    "noov:Nvimp_valo"
      FROM Pz_Fe_Impuestos_Renglon(:Tipo_, :Prefijo_, :Numero_, :Renglon)
      WHERE "noov:Nvimp_oper" = 'S'
      ORDER BY "noov:Nvimp_cdia"
      INTO "noov:Nvimp_cdia",
           "noov:Nvdet_piva",
           "noov:Nvdet_viva";

    ELSE
      SELECT FIRST 1 "noov:Nvimp_cdia",
                     "noov:Nvimp_porc",
                     "noov:Nvimp_valo"
      FROM Pz_Fe_Impuestos_Renglon(:Tipo_, :Prefijo_, :Numero_, :Renglon)
      WHERE "noov:Nvimp_oper" = 'S'
      ORDER BY "noov:Nvimp_cdia"
      INTO "noov:Nvimp_cdia",
           "noov:Nvdet_piva",
           "noov:Nvdet_viva";

    "noov:Nvimp_cdia" = COALESCE("noov:Nvimp_cdia", '00');
    "noov:Nvdet_piva" = COALESCE("noov:Nvdet_piva", 0);
    "noov:Nvdet_viva" = COALESCE("noov:Nvdet_viva", 0);

    "noov:Nvpro_nomb" = SUBSTRING(TRIM("noov:Nvpro_nomb") || IIF(TRIM(V_Nota) = '-', '', ' ' || TRIM(COALESCE(V_Nota, ''))) FROM 1 FOR 198);
    "noov:Nvfac_valo" = V_Bruto;

    IF (V_Linea = 'INC' AND
        V_Bruto = 0) THEN
      "noov:Nvfac_valo" = "noov:Nvdet_viva" / "noov:Nvfac_cant";

    IF (V_Valor_Aiu > 0) THEN
      "noov:Nvimp_cdia" = '00';

    "noov:Nvdet_pref" = '';
    IF (V_Bruto = "noov:Nvfac_desc") THEN
      "noov:Nvdet_pref" = '01';

    IF (V_Valor_Aiu > 0) THEN
    BEGIN
      "noov:Nvdet_piva" = 0;
      "noov:Nvdet_viva" = 0;
    END
    "noov:Nvdet_tcod" = '';
    "noov:Nvpro_cean" = '';
    "noov:Nvdet_entr" = '';
    "noov:Nvuni_quan" = 0;

    IF (TRIM(V_Linea) IN ('AIU_A', 'AIU')) THEN
      "noov:Nvdet_nota" = 'Contrato de servicios AIU por concepto de: Servicios';
    ELSE
      "noov:Nvdet_nota" = TRIM(V_Nota) || IIF(V_Dsi = 'S', ' - Bienes Cubiertos', '');

    "noov:Nvdet_padr" = 0;
    "noov:Nvdet_marc" = '';
    "noov:Nvdet_mode" = '';
    IF (V_Linea = 'INC' AND
        V_Bruto > 0) THEN
      V_Num_Linea = V_Num_Linea + 1;

    V_Valo = (SELECT Valor
              FROM Redondeo_Dian(:"noov:Nvfac_valo", 2));
    V_Desc = (SELECT Valor
              FROM Redondeo_Dian(:"noov:Nvfac_desc", 2));

    -- "noov:Nvfac_stot" = (V_Valo * "noov:Nvfac_cant") - V_Desc;

    IF ("noov:Nvfac_pdes" = 100) THEN
      "noov:Nvfac_desc" = "noov:Nvfac_stot";

    IF ("noov:Nvimp_cdia" = '22') THEN
      "noov:Nvfac_stot" = 0;

    SELECT Valor
    FROM Redondeo_Dian(:"noov:Nvfac_stot", 2)
    INTO Stot_Red;

    SUSPEND;
  END
END^


CREATE OR ALTER PROCEDURE PZ_FE_DETALLE_BASE (
    TIPO_ CHAR(5),
    PREFIJO_ CHAR(5),
    NUMERO_ CHAR(10))
RETURNS (
    RENGLON INTEGER,
    REFERENCIA CHAR(20),
    NOM_REFERENCIA CHAR(80),
    CANTIDAD DOUBLE PRECISION,
    BRUTO DOUBLE PRECISION,
    PORC_DESCUENTO DOUBLE PRECISION,
    DESCUENTO DOUBLE PRECISION,
    AIU DOUBLE PRECISION,
    VALOR_AIU DOUBLE PRECISION,
    STOT DOUBLE PRECISION,
    NOTA CHAR(200),
    MEDIDA CHAR(5),
    LINEA CHAR(5),
    DSI CHAR(1),
    STOT_RED DOUBLE PRECISION)
AS
DECLARE VARIABLE V_Medida    CHAR(5);
DECLARE VARIABLE V_Renglon   INTEGER;
DECLARE VARIABLE V_Trm_Valor DOUBLE PRECISION;
DECLARE VARIABLE V_Bruto     DOUBLE PRECISION;
DECLARE VARIABLE V_Valo      DOUBLE PRECISION;
DECLARE VARIABLE V_Desc      DOUBLE PRECISION;
BEGIN
  FOR SELECT Renglon,
             Codreferencia,
             Entrada + Salida,
             Bruto,
             Porcentaje_Descuento,
             Descuento,
             Aiu,
             Nota,
             Dsi
      FROM Tr_Inventario
      WHERE Tipo = :Tipo_
            AND Prefijo = :Prefijo_
            AND Numero = :Numero_
      INTO :Renglon,
           :Referencia,
           :Cantidad,
           :Bruto,
           :Porc_Descuento,
           :Descuento,
           :Aiu,
           :Nota,
           :Dsi
  DO
  BEGIN

    Cantidad = COALESCE(Cantidad, 0);
    Bruto = COALESCE(Bruto, 0);
    Aiu = COALESCE(Aiu, 0);

    V_Medida = NULL;
    Medida = NULL;

    --Referencias
    SELECT R.Nombre,
           R.Codmedida,
           R.Codlinea
    FROM Referencias R
    WHERE Codigo = :Referencia
    INTO Nom_Referencia,
         V_Medida,
         Linea;

    IF (Bruto * Cantidad > 0 OR (Linea = 'INC' AND
        Bruto * Cantidad = 0)) THEN
    BEGIN

      IF (V_Medida IS NOT NULL) THEN
        SELECT Codigo_Fe
        FROM Medidas
        WHERE Codigo = :V_Medida
        INTO Medida;

      Medida = COALESCE(Medida, '94');

      IF (Porc_Descuento = 0) THEN
        IF (Bruto = 0) THEN
          Porc_Descuento = 0;
        ELSE
          Porc_Descuento = Descuento * 100 / Bruto;

      Descuento = :Bruto * :Porc_Descuento / 100;

      IF (Aiu = 0) THEN
        Aiu = 100;
      Valor_Aiu = (Bruto - Descuento) * Aiu / 100;

      IF (Aiu = 100) THEN
        Valor_Aiu = 0;

      Bruto = Bruto - Valor_Aiu;
      Stot = Cantidad * (Bruto - Descuento);

      IF (Porc_Descuento = 100) THEN
        Stot = Cantidad * Bruto;
      /*
IF (Linea = 'INC' AND Bruto = 0)
THEN
Stot = 0;
*/
      IF (Valor_Aiu > 0) THEN
      BEGIN
        V_Renglon = Renglon;
        SELECT GEN_ID(Gen_Tr_Inventario, 1)
        FROM Rdb$Database
        INTO Renglon;
      END
      Descuento = Descuento * Cantidad;

      V_Valo = (SELECT Valor
                FROM Redondeo_Dian(:Bruto, 2));
      V_Desc = (SELECT Valor
                FROM Redondeo_Dian(:Descuento, 2));

      Stot_Red = (V_Valo * Cantidad) - V_Desc;

      IF (Porc_Descuento = 100) THEN
        Stot_Red = V_Valo * Cantidad;

      SUSPEND;

      IF (Valor_Aiu > 0) THEN
      BEGIN
        Renglon = V_Renglon;
        Bruto = Valor_Aiu;
        V_Valo = (SELECT Valor
                  FROM Redondeo_Dian(:Bruto, 2));
        Porc_Descuento = 0;
        Descuento = 0;
        Stot = Valor_Aiu * Cantidad;
        Stot_Red = (V_Valo * Cantidad);
        Valor_Aiu = 0;
        Aiu = 0;

        SUSPEND;
      END
      IF (Linea = 'INC' AND
          Bruto > 0) THEN
      BEGIN
        /* SELECT "noov:Nvimp_valo"
FROM Pz_Fe_Impuestos_Detalle(:Tipo_, :Prefijo_, :Numero_, :Renglon)
INTO V_Bruto;

V_Bruto = COALESCE(V_Bruto, 0);

IF (V_Bruto = 0) THEN
Stot = 0;
ELSE
Stot = V_Bruto * Cantidad;

*/

        SUSPEND;

      END
    END
  END
END^


CREATE OR ALTER PROCEDURE PZ_FE_IMPUESTOS (
    TIPO_ CHAR(5),
    PREFIJO_ CHAR(5),
    NUMERO_ CHAR(10))
RETURNS (
    "noov:Nvimp_cdia" CHAR(5),
    "noov:Nvimp_desc" VARCHAR(80),
    "noov:Nvimp_oper" CHAR(1),
    "noov:Nvimp_base" DOUBLE PRECISION,
    "noov:Nvimp_porc" DOUBLE PRECISION,
    "noov:Nvimp_valo" DOUBLE PRECISION,
    ES_TARIFA CHAR(1),
    CLASE VARCHAR(20))
AS
DECLARE VARIABLE V_Renglon   INTEGER;
DECLARE VARIABLE V_Trm_Valor DOUBLE PRECISION;
BEGIN
  /*
SELECT Valor
FROM Pz_Fe_Trm(:Tipo_, :Prefijo_, :Numero_)
INTO V_Trm_Valor;
*/
  FOR SELECT Renglon
      FROM Tr_Inventario
      WHERE Tipo = :Tipo_
            AND Prefijo = :Prefijo_
            AND Numero = :Numero_
      ORDER BY Renglon
      INTO :V_Renglon
  DO
  BEGIN

    /* Iniciar las varibles */
    "noov:Nvimp_cdia" = NULL;
    "noov:Nvimp_desc" = NULL;
    "noov:Nvimp_base" = 0;
    "noov:Nvimp_porc" = 0;
    "noov:Nvimp_valo" = 0;

    FOR SELECT "noov:Nvimp_cdia",
               "noov:Nvimp_desc",
               "noov:Nvimp_oper",
               "noov:Nvimp_base",
               "noov:Nvimp_porc",
               "noov:Nvimp_valo",
               Es_Tarifa,
               Clase
        FROM Pz_Fe_Impuestos_Renglon(:Tipo_, :Prefijo_, :Numero_, :V_Renglon)
        INTO :"noov:Nvimp_cdia",
             :"noov:Nvimp_desc",
             :"noov:Nvimp_oper",
             :"noov:Nvimp_base",
             :"noov:Nvimp_porc",
             :"noov:Nvimp_valo",
             Es_Tarifa,
             Clase

    DO
    BEGIN
      SUSPEND;
    END
  END
END^


CREATE OR ALTER PROCEDURE PZ_FE_IMPUESTOS_DETALLE (
    TIPO_ CHAR(5),
    PREFIJO_ CHAR(5),
    NUMERO_ CHAR(10),
    RENGLON_ INTEGER)
RETURNS (
    "noov:Nvimp_cdia" CHAR(5),
    "noov:Nvimp_desc" VARCHAR(80),
    "noov:Nvimp_oper" CHAR(1),
    "noov:Nvimp_base" DOUBLE PRECISION,
    "noov:Nvimp_porc" DOUBLE PRECISION,
    "noov:Nvimp_valo" DOUBLE PRECISION)
AS
DECLARE VARIABLE V_Ano           INTEGER;
DECLARE VARIABLE V_Cantidad      DOUBLE PRECISION;
DECLARE VARIABLE V_Bruto         DOUBLE PRECISION;
DECLARE VARIABLE V_Descuento     DOUBLE PRECISION;
DECLARE VARIABLE V_Aiu           DOUBLE PRECISION;
DECLARE VARIABLE V_Tarifa        DOUBLE PRECISION;
DECLARE VARIABLE V_Base          DOUBLE PRECISION;
DECLARE VARIABLE V_Valor         DOUBLE PRECISION;
DECLARE VARIABLE V_Tipo_Impuesto CHAR(5);
DECLARE VARIABLE V_Es_Tarifa     CHAR(1);
DECLARE VARIABLE V_Valor_Nominal DOUBLE PRECISION;
DECLARE VARIABLE V_Trm_Valor     DOUBLE PRECISION;
BEGIN
  /*
SELECT Valor
FROM Pz_Fe_Trm(:Tipo_, :Prefijo_, :Numero_)
INTO V_Trm_Valor;
*/
  /* IMPUESTOS */
  "noov:Nvimp_oper" = 'S';

  SELECT EXTRACT(YEAR FROM Fecha)
  FROM Comprobantes
  WHERE Tipo = :Tipo_
        AND Prefijo = :Prefijo_
        AND Numero = :Numero_
  INTO :V_Ano;

  FOR SELECT Cantidad,
             Bruto,
             Descuento,
             Aiu
      FROM Pz_Fe_Detalle_Base(:Tipo_, :Prefijo_, :Numero_)
      WHERE Renglon = :Renglon_
      INTO :V_Cantidad,
           :V_Bruto,
           :V_Descuento,
           :V_Aiu
  DO
  BEGIN

    V_Cantidad = COALESCE(V_Cantidad, 0);
    V_Bruto = COALESCE(V_Bruto, 0);
    V_Descuento = COALESCE(V_Descuento, 0);
    V_Aiu = COALESCE(V_Aiu, 0);

    IF (V_Aiu = 0) THEN
      V_Aiu = 100;

    V_Tipo_Impuesto = NULL;
    V_Es_Tarifa = NULL;

    FOR SELECT SKIP 1 Tipo_Impuesto,
                      Es_Tarifa,
                      SUM(Debito + Credito)
        FROM Reg_Impuestos
        WHERE Tipo = :Tipo_
              AND Prefijo = :Prefijo_
              AND Numero = :Numero_
              AND Renglon = :Renglon_
        GROUP BY Codigo_Fe, Tipo_Impuesto, Es_Tarifa
        INTO :V_Tipo_Impuesto,
             :V_Es_Tarifa,
             :V_Valor_Nominal
    DO
    BEGIN
      V_Valor_Nominal = COALESCE(V_Valor_Nominal, 0);
      V_Tarifa = NULL;

      SELECT Tarifa
      FROM Data_Impuestos
      WHERE Codtipoimpuesto = :V_Tipo_Impuesto
            AND Ano = :V_Ano
      INTO :V_Tarifa;

      V_Tarifa = COALESCE(V_Tarifa, 0);
      -- V_Tarifa = CAST(V_Tarifa AS NUMERIC(17,4));

      V_Base = (V_Cantidad * V_Bruto * V_Aiu / 100) - V_Descuento;
      -- v_base = v_cantidad * (v_bruto - v_descuento) * v_aiu / 100;

      IF (V_Es_Tarifa = 'S') THEN
        V_Valor = V_Base * V_Tarifa / 100;

      IF (V_Es_Tarifa = 'N') THEN
      BEGIN
        IF (V_Cantidad = 0) THEN
          V_Tarifa = 0;
        ELSE
          V_Tarifa = V_Valor_Nominal / V_Cantidad;
        V_Valor = V_Valor_Nominal;
      END

      SELECT Codigo_Fe,
             SUBSTRING(Nombre FROM 1 FOR 40)
      FROM Tipoimpuestos
      WHERE Codigo = :V_Tipo_Impuesto
      INTO :"noov:Nvimp_cdia",
           :"noov:Nvimp_desc";

      IF ("noov:Nvimp_cdia" = '22') THEN
        V_Base = 0;

      "noov:Nvimp_desc" = CASE
                            WHEN TRIM("noov:Nvimp_cdia") = '02' THEN 'Impto al Consumo Departamental Nominal'
                            WHEN TRIM("noov:Nvimp_cdia") = '22' THEN 'Impto al Consumo de Bolsa Plastica'
                            WHEN TRIM("noov:Nvimp_cdia") = 'ZZ' THEN 'Otros tributos, tasas, contribuciones'
                            WHEN TRIM("noov:Nvimp_cdia") = '24' THEN 'Impuesto Nacional a los Combustibles'
                            WHEN TRIM("noov:Nvimp_cdia") = '25' THEN 'Sobretasa a los combustibles'
                            ELSE "noov:Nvimp_desc"
                          END;

      "noov:Nvimp_base" = V_Base;
      "noov:Nvimp_porc" = V_Tarifa;
      "noov:Nvimp_valo" = V_Valor;

      IF ("noov:Nvimp_cdia" <> '22' AND
          NOT("noov:Nvimp_cdia" = '04' AND
          V_Tarifa = 0)) THEN
        SUSPEND;

    END

  END

END^


CREATE OR ALTER PROCEDURE PZ_FE_IMPUESTOS_RENGLON (
    TIPO_ CHAR(5),
    PREFIJO_ CHAR(5),
    NUMERO_ CHAR(10),
    RENGLON_ INTEGER)
RETURNS (
    "noov:Nvimp_cdia" CHAR(5),
    "noov:Nvimp_desc" VARCHAR(80),
    "noov:Nvimp_oper" CHAR(1),
    "noov:Nvimp_base" DOUBLE PRECISION,
    "noov:Nvimp_porc" DOUBLE PRECISION,
    "noov:Nvimp_valo" DOUBLE PRECISION,
    ES_TARIFA CHAR(1),
    CLASE VARCHAR(20))
AS
DECLARE VARIABLE V_Ano              INTEGER;
DECLARE VARIABLE V_Cantidad         DOUBLE PRECISION;
DECLARE VARIABLE V_Bruto            DOUBLE PRECISION;
DECLARE VARIABLE V_Porc_Descuento   DOUBLE PRECISION;
DECLARE VARIABLE V_Descuento        DOUBLE PRECISION;
DECLARE VARIABLE V_Aiu              DOUBLE PRECISION;
DECLARE VARIABLE V_Referencia       CHAR(20);
DECLARE VARIABLE V_Esquema_Impuesto CHAR(5);
DECLARE VARIABLE V_Tarifa           DOUBLE PRECISION;
DECLARE VARIABLE V_Base             DOUBLE PRECISION;
DECLARE VARIABLE V_Valor            DOUBLE PRECISION;
DECLARE VARIABLE V_Tipo_Impuesto    CHAR(5);
DECLARE VARIABLE V_Tipo_Retencion   CHAR(5);
DECLARE VARIABLE V_Valor_Nominal    DOUBLE PRECISION;
DECLARE VARIABLE V_Trm_Valor        DOUBLE PRECISION;
DECLARE VARIABLE V_Num_Linea        INTEGER;
BEGIN

  SELECT EXTRACT(YEAR FROM Fecha)
  FROM Comprobantes
  WHERE Tipo = :Tipo_
        AND Prefijo = :Prefijo_
        AND Numero = :Numero_
  INTO :V_Ano;

  V_Num_Linea = 0;
  FOR SELECT Cantidad,
             Bruto,
             Porc_Descuento,
             Descuento,
             Aiu,
             Referencia,
             Stot_Red
      FROM Pz_Fe_Detalle_Base(:Tipo_, :Prefijo_, :Numero_)
      WHERE Renglon = :Renglon_
      INTO :V_Cantidad,
           :V_Bruto,
           :V_Porc_Descuento,
           :V_Descuento,
           :V_Aiu,
           :V_Referencia,
           :V_Base
  DO
  BEGIN

    IF (V_Num_Linea = 0) THEN
    BEGIN
      V_Cantidad = COALESCE(V_Cantidad, 0);
      V_Bruto = COALESCE(V_Bruto, 0);
      V_Descuento = COALESCE(V_Descuento, 0);
      V_Aiu = COALESCE(V_Aiu, 0);

      IF (V_Aiu = 0) THEN
        V_Aiu = 100;

      V_Esquema_Impuesto = NULL;
      V_Tipo_Impuesto = NULL;

      V_Tipo_Impuesto = NULL;
      Es_Tarifa = NULL;

      /* IMPUESTOS */

      "noov:Nvimp_oper" = 'S';

      FOR SELECT Tipo_Impuesto,
                 Es_Tarifa,
                 SUM(Debito + Credito)
          FROM Reg_Impuestos
          WHERE Tipo = :Tipo_
                AND Prefijo = :Prefijo_
                AND Numero = :Numero_
                AND Renglon = :Renglon_
                AND Codigo_Fe <> '00'
          GROUP BY Tipo_Impuesto, Es_Tarifa
          INTO :V_Tipo_Impuesto,
               :Es_Tarifa,
               :V_Valor_Nominal
      DO
      BEGIN
        V_Valor_Nominal = COALESCE(V_Valor_Nominal, 0);
        V_Tarifa = NULL;

        SELECT Tarifa
        FROM Data_Impuestos
        WHERE Codtipoimpuesto = :V_Tipo_Impuesto
              AND Ano = :V_Ano
        INTO :V_Tarifa;

        V_Tarifa = COALESCE(V_Tarifa, 0);

        /*IF (V_Porc_Descuento=100) THEN
V_Base = (V_Cantidad * V_Bruto * V_Aiu / 100);
ELSE
V_Base = (V_Cantidad * V_Bruto * V_Aiu / 100) - V_Descuento; */

        IF (Es_Tarifa = 'S') THEN
          V_Valor = V_Base * V_Tarifa / 100;

        IF (Es_Tarifa = 'N') THEN
        BEGIN
          IF (V_Cantidad = 0) THEN
            V_Tarifa = 0;
          ELSE
            V_Tarifa = V_Valor_Nominal / V_Cantidad;
          V_Valor = V_Valor_Nominal;
        END

        SELECT Codigo_Fe,
               SUBSTRING(Nombre FROM 1 FOR 40)
        FROM Tipoimpuestos
        WHERE Codigo = :V_Tipo_Impuesto
        INTO :"noov:Nvimp_cdia",
             :"noov:Nvimp_desc";

        IF ("noov:Nvimp_cdia" = '22') THEN
        BEGIN
          V_Base = 0;
          V_Num_Linea = 1;
        END

        "noov:Nvimp_desc" = CASE
                              WHEN TRIM("noov:Nvimp_cdia") = '02' THEN 'Impto al Consumo Departamental Nominal'
                              WHEN TRIM("noov:Nvimp_cdia") = '22' THEN 'Impto al Consumo de Bolsa Plastica'
                              WHEN TRIM("noov:Nvimp_cdia") = 'ZZ' THEN 'Otros tributos, tasas, contribuciones'
                              WHEN TRIM("noov:Nvimp_cdia") = '24' THEN 'Impuesto Nacional a los Combustibles'
                              WHEN TRIM("noov:Nvimp_cdia") = '25' THEN 'Sobretasa a los combustibles'
                              WHEN V_Tarifa = 19 THEN 'Impuesto sobre la Ventas 19%'
                              WHEN V_Tarifa = 5 THEN 'Impuesto sobre la Ventas 5%'
                              WHEN V_Tarifa = 0 THEN 'Impuesto sobre la Ventas Exento'
                              ELSE "noov:Nvimp_desc"
                            END;

        "noov:Nvimp_base" = V_Base;
        "noov:Nvimp_porc" = V_Tarifa;
        "noov:Nvimp_valo" = V_Valor;

        IF (NOT("noov:Nvimp_cdia" = '04' AND
            V_Tarifa = 0)) THEN
          SUSPEND;

      END

      /* RETENCIONES */

      "noov:Nvimp_oper" = 'R';
      Es_Tarifa = 'S';

      V_Tipo_Retencion = NULL;
      V_Base = 0;
      V_Valor = 0;

      FOR SELECT Tipo_Retencion,
                 SUM(Base),
                 SUM(Debito + Credito)
          FROM Reg_Retenciones
          WHERE Tipo = :Tipo_
                AND Prefijo = :Prefijo_
                AND Numero = :Numero_
                AND Renglon = :Renglon_
                AND Publicar = 'S'
          GROUP BY Tipo_Retencion
          INTO :V_Tipo_Retencion,
               :V_Base,
               :V_Valor

      DO
      BEGIN

        "noov:Nvimp_cdia" = NULL;
        "noov:Nvimp_desc" = NULL;
        "noov:Nvimp_base" = 0;
        "noov:Nvimp_porc" = 0;
        "noov:Nvimp_valo" = 0;
        V_Tarifa = 0;

        V_Base = COALESCE(V_Base, 0);
        V_Valor = COALESCE(V_Valor, 0);

        SELECT Codigo_Fe,
               SUBSTRING(Nombre FROM 1 FOR 40),
               Clase
        FROM Tiporetenciones
        WHERE Codigo = :V_Tipo_Retencion
        INTO :"noov:Nvimp_cdia",
             :"noov:Nvimp_desc",
             :Clase;

        SELECT Tarifa
        FROM Data_Retenciones
        WHERE Codtiporetencion = :V_Tipo_Retencion
              AND Ano = :V_Ano
        INTO :V_Tarifa;

        V_Tarifa = COALESCE(V_Tarifa, 0);

        "noov:Nvimp_base" = V_Base;
        "noov:Nvimp_porc" = V_Tarifa;
        "noov:Nvimp_valo" = V_Base * V_Tarifa / 100;

        SUSPEND;
      END
    END
  END
END^


CREATE OR ALTER PROCEDURE PZ_FE_IMPUESTOS_SUM (
    TIPO_ CHAR(5),
    PREFIJO_ CHAR(5),
    NUMERO_ CHAR(10))
RETURNS (
    "noov:Nvimp_cdia" CHAR(5),
    "noov:Nvimp_desc" VARCHAR(80),
    "noov:Nvimp_oper" CHAR(1),
    "noov:Nvimp_base" DOUBLE PRECISION,
    "noov:Nvimp_porc" DOUBLE PRECISION,
    "noov:Nvimp_valo" DOUBLE PRECISION,
    VALOR_RED DOUBLE PRECISION,
    ES_TARIFA CHAR(1),
    CLASE VARCHAR(20))
AS
DECLARE VARIABLE V_Renglon   INTEGER;
DECLARE VARIABLE V_Trm_Valor DOUBLE PRECISION;
BEGIN
  FOR SELECT "noov:Nvimp_cdia",
             "noov:Nvimp_desc",
             "noov:Nvimp_oper",
             SUM("noov:Nvimp_base"),
             "noov:Nvimp_porc",
             SUM("noov:Nvimp_valo"),
             Es_Tarifa,
             Clase
      FROM Pz_Fe_Impuestos(:Tipo_, :Prefijo_, :Numero_)
      WHERE Es_Tarifa = 'S'
      GROUP BY 1, 2, 3, 5, 7, 8
      INTO :"noov:Nvimp_cdia",
           :"noov:Nvimp_desc",
           :"noov:Nvimp_oper",
           :"noov:Nvimp_base",
           :"noov:Nvimp_porc",
           :"noov:Nvimp_valo",
           Es_Tarifa,
           Clase
  DO
  BEGIN
    SELECT Valor
    FROM Redondeo_Dian(:"noov:Nvimp_valo", 2)
    INTO Valor_Red;
    SUSPEND;
  END

  FOR SELECT "noov:Nvimp_cdia",
             "noov:Nvimp_desc",
             "noov:Nvimp_oper",
             SUM("noov:Nvimp_base"),
             SUM("noov:Nvimp_porc"),
             SUM("noov:Nvimp_valo"),
             Es_Tarifa,
             Clase
      FROM Pz_Fe_Impuestos(:Tipo_, :Prefijo_, :Numero_)
      WHERE Es_Tarifa = 'N'
      GROUP BY 1, 2, 3, 7, 8
      INTO :"noov:Nvimp_cdia",
           :"noov:Nvimp_desc",
           :"noov:Nvimp_oper",
           :"noov:Nvimp_base",
           :"noov:Nvimp_porc",
           :"noov:Nvimp_valo",
           Es_Tarifa,
           Clase
  DO
  BEGIN
    SELECT Valor
    FROM Redondeo_Dian(:"noov:Nvimp_valo", 2)
    INTO Valor_Red;
    SUSPEND;
  END

END^


CREATE OR ALTER PROCEDURE PZ_FE_ROUND (
    TIPO_ VARCHAR(5),
    PREFIJO_ VARCHAR(5),
    NUMERO_ VARCHAR(10),
    TRM_ DOUBLE PRECISION)
RETURNS (
    VALOR NUMERIC(12,2))
AS
DECLARE VARIABLE V_Valor_Mekano DOUBLE PRECISION;
DECLARE VARIABLE V_Valor_Totp   DOUBLE PRECISION;
DECLARE VARIABLE V_Valor_Stot   DOUBLE PRECISION;
DECLARE VARIABLE V_Valor_Desc   DOUBLE PRECISION;
DECLARE VARIABLE V_Valor_Imp    DOUBLE PRECISION;
DECLARE VARIABLE V_Dif_Mek_Xml  DOUBLE PRECISION;
DECLARE VARIABLE V_Dif_Xml      DOUBLE PRECISION;
DECLARE VARIABLE V_Tipo         VARCHAR(20);
BEGIN
  --CODIGO_FE
  SELECT TRIM(Codigo_Fe)
  FROM Documentos
  WHERE Codigo = :Tipo_
  INTO V_Tipo;

  SELECT Com_Total,
         "noov:Nvfac_totp",
         "noov:Nvfac_stot",
         "noov:Nvfac_desc" + "noov:Nvfac_anti"
  FROM Pz_Fe_Comprobantes(:Tipo_, :Prefijo_, :Numero_, '%', '%')
  INTO V_Valor_Mekano,
       V_Valor_Totp,
       V_Valor_Stot,
       V_Valor_Desc;

  -- Impuestos
  SELECT SUM(Valor_Red)
  FROM Pz_Fe_Impuestos_Sum(:Tipo_, :Prefijo_, :Numero_)
  WHERE "noov:Nvimp_oper" = 'S'
  INTO V_Valor_Imp;
  V_Valor_Imp = COALESCE(V_Valor_Imp, 0);

  SELECT Valor
  FROM Redondeo_Dian(:V_Valor_Mekano / :Trm_, 2)
  INTO V_Valor_Mekano;

  SELECT Valor
  FROM Redondeo_Dian(:V_Valor_Totp / :Trm_, 2)
  INTO V_Valor_Totp;

  SELECT Valor
  FROM Redondeo_Dian(:V_Valor_Stot / :Trm_, 2)
  INTO V_Valor_Stot;

  SELECT Valor
  FROM Redondeo_Dian(:V_Valor_Desc / :Trm_, 2)
  INTO V_Valor_Desc;

  SELECT Valor
  FROM Redondeo_Dian(:V_Valor_Imp / :Trm_, 2)
  INTO V_Valor_Imp;

  -- Diferencia Mekano vs XML
  V_Dif_Mek_Xml = :V_Valor_Mekano - :V_Valor_Totp;

  -- Valor XML
  V_Dif_Xml = :V_Valor_Totp - :V_Valor_Stot - :V_Valor_Imp + COALESCE(:V_Valor_Desc, 0);

  IF (V_Tipo IN ('DOCUMENTO SOPORTE','NOTA DE AJUSTE A DS')) THEN
    Valor = V_Dif_Xml;
  ELSE
    Valor = V_Dif_Mek_Xml + V_Dif_Xml;

  IF (Valor > 5000 OR Valor < -5000) THEN
    Valor = 0;

  Valor = COALESCE(Valor, 0);
  SUSPEND;

END^


CREATE OR ALTER PROCEDURE PZ_FE_TRM (
    TIPO_ CHAR(5),
    PREFIJO_ CHAR(5),
    NUMERO_ CHAR(10))
RETURNS (
    CODIGO CHAR(5),
    VALOR DOUBLE PRECISION,
    FECHA CHAR(10))
AS
DECLARE VARIABLE V_Docu    CHAR(20);
DECLARE VARIABLE V_Tipo    CHAR(5);
DECLARE VARIABLE V_Prefijo CHAR(5);
DECLARE VARIABLE V_Numero  CHAR(10);
DECLARE VARIABLE V_Trm     DOUBLE PRECISION;
DECLARE VARIABLE V_Fecha   DATE;
DECLARE VARIABLE V_Lista   CHAR(5);
BEGIN
  -- Se valida el tipo de documento
  SELECT Codigo_Fe
  FROM Documentos
  WHERE Codigo = :Tipo_
  INTO V_Docu;

  V_Tipo = :Tipo_;
  V_Prefijo = :Prefijo_;
  V_Numero = :Numero_;

  IF (TRIM(V_Docu) IN ('NOTA CREDITO', 'NOTA DEBITO')) THEN
    SELECT Tiporef,
           Prefijoref,
           Numeroref
    FROM Notas_Gestion
    WHERE Tipo = :Tipo_
          AND Prefijo = :Prefijo_
          AND Numero = :Numero_
    INTO V_Tipo,
         V_Prefijo,
         V_Numero;

  SELECT Trm,
         Fecha,
         Codlista
  FROM Comprobantes
  WHERE Tipo = :V_Tipo
        AND Prefijo = :V_Prefijo
        AND Numero = :V_Numero
  INTO V_Trm,
       V_Fecha,
       V_Lista;

  SELECT Codmoneda
  FROM Listas
  WHERE Codigo = :V_Lista
  INTO Codigo;

  IF (TRIM(COALESCE(Codigo, '')) = '') THEN
    Codigo = 'COP';

  SELECT FIRST 1 Valor
  FROM Tasas
  WHERE Valor > 0
        AND Codmoneda = :Codigo
        AND Fecha <= :V_Fecha
  ORDER BY Fecha DESC
  INTO Valor;

  Valor = COALESCE(Valor, 1);

  IF (V_Trm > 0) THEN
    Valor = V_Trm;

  IF (Codigo = 'COP') THEN
    Valor = 1;

  Fecha = EXTRACT(YEAR FROM V_Fecha) || '-' || RIGHT(EXTRACT(MONTH FROM V_Fecha) + 100, 2) || '-' || RIGHT(EXTRACT(DAY FROM V_Fecha) + 100, 2);
  SUSPEND;

END^



SET TERM ; ^

COMMIT WORK;

/* EXPORTADOS GESTION END */

/* INFORME CXPG040104 */
-- Se modifica el join personal para que tome lo documentos sin vendedor

UPDATE INFORMES SET 
    SQL_CONSULTA = 'SELECT R1.Fecha, R1.Vence,
       (R1.Vence - R1.Fecha) AS Plazo, R1.Mora AS "DIAS MORA",
       TRIM(R1.Tipo) || '' '' || TRIM(R1.Prefijo) || '': '' || TRIM(R1.Numero) AS Documento,
       TRIM(R1.Nombre) || ''  -  '' || TRIM(R1.Tercero) AS Tercero,
       SUBSTRING(TRIM(T.Codmunicipio) FROM 1 FOR 2) || '' - '' || TRIM(M1.Nombre) AS Depto,
       TRIM(M.Nombre) || '' : '' || TRIM(T.Codmunicipio) AS Municipio,
       TRIM(Z.Nombre) || '':'' || TRIM(T.Codzona) AS Zona,
       TRIM(P.Nombre) || '' : '' || C.Codvendedor AS "NOMBRE VENDEDOR",
       R1.Corriente AS "SIN VENCER", R1.Edad_0 AS "VENCE HOY",
       R1.Edad_1 AS "A EDAD 1", R1.Edad_2 AS "A EDAD 2",
       R1.Edad_3 AS "A EDAD 3", R1.Edad_4 AS "EDAD SUPERIOR",
       R1.Saldo AS "SALDO X COBRAR", TRIM(R1.Tipo) AS Tipo,
       TRIM(R1.Prefijo) AS Prefijo, LPAD(TRIM(R1.Numero), 10) AS Numero,
       R1.Corte
FROM Fx_Cartera_Fecha_Edades(:Fecha_Corte, :Edad) R1
INNER JOIN Comprobantes C ON ((R1.Tipo = C.Tipo) AND (R1.Prefijo = C.Prefijo) AND (R1.Numero = C.Numero))
INNER JOIN Terceros T ON (R1.Tercero = T.Codigo)
LEFT JOIN Personal P ON (C.Codvendedor = P.Codigo)
INNER JOIN Municipios M ON (M.Codigo = T.Codmunicipio)
LEFT JOIN Municipios M1 ON (SUBSTRING(TRIM(T.Codmunicipio) FROM 1 FOR 2) = M1.Codigo)
INNER JOIN Zonas Z ON (T.Codzona = Z.Codigo)
WHERE R1.Grupo = ''COMPRA''
      AND TRIM(T.Codzona) LIKE :Zona
      AND TRIM(COALESCE(C.Codvendedor,'''')) LIKE :Vendedor
      AND TRIM(T.Codmunicipio) LIKE :Municipio
      AND (R1.Saldo) NOT BETWEEN -1 AND 1
ORDER BY 2, 1'
WHERE (CODIGO = 'CXPG040104');


COMMIT WORK;



/*  DOCUMENTO SOPORTE  */


/* Facturas */
UPDATE Documentos
SET Codigo_Fe = 'FACTURA'
WHERE Codigo IN (SELECT DISTINCT Fac.Tipo
                 FROM Facturas Fac
                 JOIN Documentos Doc ON (Doc.Codigo = Fac.Tipo)
                 WHERE (Doc.Grupo = 'VENTA')
                       AND (Doc.Signo = 'CREDITO')
                       AND (Doc.Inventario = 'SALIDA')
                       AND (TRIM(Doc.Nombre) NOT CONTAINING ('NOTA'))
                       AND (Doc.Codigo_Fe = 'NOTA DE AJUSTE A DS'));

/* Notas Crédito */
UPDATE Documentos
SET Codigo_Fe = 'NOTA CREDITO'
WHERE Codigo IN (SELECT DISTINCT Fac.Tipo
                 FROM Facturas Fac
                 JOIN Documentos Doc ON (Doc.Codigo = Fac.Tipo)
                 WHERE (Doc.Grupo = 'VENTA')
                       AND (Doc.Signo = 'DEBITO')
                       AND (Doc.Inventario = 'ENTRADA')
                       AND (Doc.Codigo_Fe = 'NOTA DE AJUSTE A DS'));

/* Notas Débito */
UPDATE Documentos
SET Codigo_Fe = 'NOTA DEBITO'
WHERE Codigo IN (SELECT DISTINCT Fac.Tipo
                 FROM Facturas Fac
                 JOIN Documentos Doc ON (Doc.Codigo = Fac.Tipo)
                 WHERE (Doc.Grupo = 'VENTA')
                       AND (Doc.Signo = 'CREDITO')
                       AND (Doc.Inventario = 'SALIDA')
                       AND TRIM(Doc.Nombre) CONTAINING ('NOTA')
                       AND (Doc.Codigo_Fe = 'NOTA DE AJUSTE A DS'));

/* Facturas Exportación Gestión */
UPDATE Documentos
SET Codigo_Fe = 'EXPORTACION'
WHERE Codigo IN (SELECT DISTINCT Fac.Tipo
                 FROM Facturas Fac
                 JOIN Comprobantes Com USING (Tipo,Prefijo,Numero)
                 JOIN Listas Lis ON (Lis.Codigo = Com.Codlista)
                 JOIN Documentos Doc ON (Doc.Codigo = Fac.Tipo)
                 WHERE (Doc.Grupo = 'VENTA')
                       AND (Doc.Signo = 'CREDITO')
                       AND (Doc.Inventario = 'SALIDA')
                       AND (Doc.Codigo_Fe = 'NOTA DE AJUSTE A DS')
                       AND (TRIM(COALESCE(Lis.Codmoneda,'')) <> ''));

/* Facturas Exportación Contable */
UPDATE Documentos
SET Codigo_Fe = 'EXPORTACION'
WHERE Codigo IN (SELECT DISTINCT Fac.Tipo
                 FROM Facturas Fac
                 JOIN Auto_Comp Com USING (Tipo,Prefijo,Numero)
                 JOIN Terceros Ter ON (Ter.Codigo = Com.Codtercero)
                 LEFT JOIN Listas Lis ON (Lis.Codigo = Ter.Codlista)
                 JOIN Documentos Doc ON (Doc.Codigo = Fac.Tipo)
                 WHERE (Doc.Grupo = 'VENTA')
                       AND (Doc.Signo = 'CREDITO')
                       AND (Doc.Inventario = 'SALIDA')
                       AND (Doc.Codigo_Fe = 'NOTA DE AJUSTE A DS')
                       AND (TRIM(COALESCE(Lis.Codmoneda,'')) <> ''));

/* Nomina */
UPDATE Documentos
SET Codigo_Fe = 'FACTURA'
WHERE Codigo = 'NE';

COMMIT WORK;

/* NA */
UPDATE Documentos Doc
SET Doc.Codigo_Fe = 'NA'
WHERE (Doc.Codigo_Fe = 'NOTA DE AJUSTE A DS');

COMMIT WORK;
-----------------------------
EXECUTE BLOCK
AS
DECLARE VARIABLE V_Cont   INTEGER;
DECLARE VARIABLE V_Conta  INTEGER;
DECLARE VARIABLE V_Perfil CHAR(10);
BEGIN
  -- Valida el modula de FE
  SELECT COUNT(1)
  FROM Pta
  WHERE Codigo = 'NOOVAC'
        AND Activo = 'S'
  INTO V_Conta;

  --DSE1
  SELECT COUNT(1)
  FROM Documentos
  WHERE Codigo = 'DSE1'
  INTO V_Cont;
  IF (V_Cont = 0) THEN
  BEGIN
    INSERT INTO Documentos (Codigo, Nombre, Nombre_Fe, Descripcion, Formato,
                            Formato1, Formato2, Formato3, Formato4, Activo,
                            Activo_Contabilidad, Niif, Excluir_Niif, Signo,
                            Grupo, Es_Compra_Venta, Es_Activo, Tesoreria,
                            Inventario, Cartera, Nomina, Registro_Pago,
                            Impuestos, Retenciones, Comision, Contabilidad,
                            C_Vence, C_Condicionado, C_Codtercero,
                            C_Codvendedor, C_Codlista, C_Nota, C_Codrp,
                            C_Codingreso, C_Codpoliza, C_Codreferido, M_Aplica,
                            M_Cuota, M_Documento, M_Descuento, M_Interes,
                            M_Pronto_Pago, M_Codtercero, M_Nota, Afecta,
                            Valoriza, Costea, Valida_Saldos, Con_Impuestos,
                            Codinteres_Debito, Codinteres_Credito,
                            Codpronto_Pagos, Codcopago, Publica_Retenciones,
                            Base_Dia, Pooling, Duracion, Ensamble,
                            Es_Produccion, Puntos, Rastreo, Exportar, Compartir,
                            Dimensiones, Es_Presupuesto, Analisis_Costos,
                            Rotacion, Deterioro, Logistica, Mapeado, Nativo,
                            Impuesto_Alterno, Juego, Codigo_Fe, Modificado, Aiu,
                            Dia_Sin_Iva, Copago, Anexo, Valida_Fe)
    VALUES ('DSE1',
            'SOPORTE ELECTRONICO EN COMPRAS NO OBLIGADOS A FACTURAR (+Cartera)',
            'DOCUMENTO SOPORTE EN ADQUISICIONES EFECTUADAS A NO OBLIGADOS A FACTURAR',
            'Afecta compras aumenta CxP. Retención será calculada y publicada',
            'F_DSE_GES_01A.FR3', 'F_DSE_GES_01A.FR3', 'FORDS1MEO320.FR3',
            'FORDS1CAR320.FR3', 'F_DS_MEC_01B.fr3', 'S', 'N', 'N', 'N',
            'DEBITO', 'COMPRA', 'S', 'N', 'NO', 'ENTRADA', 'S', 'N', 'N', 'S',
            'S', 'N', 'S', 'S', 'N', 'S', 'S', 'S', 'S', 'N', 'N', 'N', 'N',
            'N', 'N', 'N', 'N', 'N', 'N', 'N', 'S', 'S', 'S', 'N', 'S', 'N',
            NULL, NULL, NULL, NULL, 'S', 'N', 0, 365, 'N', 'N', 'N', 'N', 'N',
            'S', 'N', 'N', 'N', 'S', 'N', '', 'S', 'N', 'N', NULL,
            'DOCUMENTO SOPORTE', '2021-07-22', 'N', 'N', 'N', 'N', 'S');

    FOR SELECT DISTINCT Codperfil
        FROM Per_Documentos
        WHERE Coddocumento IN ('DS', 'DS1', 'DS2', 'EQ', 'EQ1', 'EQ2')
        INTO V_Perfil
    DO
    BEGIN
      INSERT INTO Per_Documentos (Codperfil, Coddocumento, Activo)
      VALUES (:V_Perfil, 'DSE1', 'S');
    END

    --VAL_DOCUMENTOS
    INSERT INTO Val_Documentos (Coddocumento, Codprefijo, Codescenario,
                                Codvendedor, Codlista, Codtercero, Codbodega,
                                Codbanco, Codcentro, Codreferencia, Notac,
                                Notam)
    SELECT 'DSE1',
           '_',
           Codescenario,
           Codvendedor,
           Codlista,
           Codtercero,
           Codbodega,
           Codbanco,
           Codcentro,
           Codreferencia,
           Notac,
           Notam
    FROM Val_Documentos
    WHERE Coddocumento = 'DS1';

  END
  ELSE
  IF (V_Conta = 0) THEN
    UPDATE Documentos
    SET Codigo_Fe = 'DOCUMENTO SOPORTE', Formato = 'F_DSE_GES_01A.FR3',
        Formato1 = 'F_DSE_GES_01A.FR3',
        Nombre = 'SOPORTE ELECTRONICO EN COMPRAS NO OBLIGADOS A FACTURAR (+Cartera)', Nombre_Fe = 'DOCUMENTO SOPORTE EN ADQUISICIONES EFECTUADAS A NO OBLIGADOS A FACTURAR'
    WHERE Codigo = 'DSE1'
          AND TRIM(COALESCE(Formato1, '')) = '';
  ELSE
    UPDATE Documentos
    SET Codigo_Fe = 'DOCUMENTO SOPORTE', Formato = 'F_DSE_CON_01A.FR3',
        Formato4 = 'F_DSE_CON_01A.FR3',
        Nombre = 'SOPORTE ELECTRONICO EN COMPRAS NO OBLIGADOS A FACTURAR ', Nombre_Fe = 'DOCUMENTO SOPORTE EN ADQUISICIONES EFECTUADAS A NO OBLIGADOS A FACTURAR'
    WHERE Codigo = 'DSE1'
          AND TRIM(COALESCE(Formato4, '')) = '';

  --DSE2
  SELECT COUNT(1)
  FROM Documentos
  WHERE Codigo = 'DSE2'
  INTO V_Cont;
  IF (V_Cont = 0) THEN
  BEGIN
    INSERT INTO Documentos (Codigo, Nombre, Nombre_Fe, Descripcion, Formato,
                            Formato1, Formato2, Formato3, Formato4, Activo,
                            Activo_Contabilidad, Niif, Excluir_Niif, Signo,
                            Grupo, Es_Compra_Venta, Es_Activo, Tesoreria,
                            Inventario, Cartera, Nomina, Registro_Pago,
                            Impuestos, Retenciones, Comision, Contabilidad,
                            C_Vence, C_Condicionado, C_Codtercero,
                            C_Codvendedor, C_Codlista, C_Nota, C_Codrp,
                            C_Codingreso, C_Codpoliza, C_Codreferido, M_Aplica,
                            M_Cuota, M_Documento, M_Descuento, M_Interes,
                            M_Pronto_Pago, M_Codtercero, M_Nota, Afecta,
                            Valoriza, Costea, Valida_Saldos, Con_Impuestos,
                            Codinteres_Debito, Codinteres_Credito,
                            Codpronto_Pagos, Codcopago, Publica_Retenciones,
                            Base_Dia, Pooling, Duracion, Ensamble,
                            Es_Produccion, Puntos, Rastreo, Exportar, Compartir,
                            Dimensiones, Es_Presupuesto, Analisis_Costos,
                            Rotacion, Deterioro, Logistica, Mapeado, Nativo,
                            Impuesto_Alterno, Juego, Codigo_Fe, Modificado, Aiu,
                            Dia_Sin_Iva, Copago, Anexo, Valida_Fe)
    VALUES ('DSE2',
            'SOPORTE ELECTRONICO EN COMPRAS NO OBLIGADOS A FACTURAR (Contado)',
            'DOCUMENTO SOPORTE EN ADQUISICIONES EFECTUADAS A NO OBLIGADOS A FACTURAR',
            'Pago de contado, afecta tesorería. Retención será calculada y publicada',
            'F_DSE_GES_01A.FR3', 'F_DSE_GES_01A.FR3', 'FORDS2MEO320.FR3',
            'FORDS2CAR320.FR3', 'FORDSCONT_2A.FR3', 'S', 'N', 'N', 'N',
            'DEBITO', 'COMPRA', 'S', 'N', 'CONTADO', 'ENTRADA', 'N', 'N', 'N',
            'S', 'S', 'N', 'S', 'N', 'N', 'S', 'S', 'S', 'S', 'N', 'N', 'N',
            'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'S', 'S', 'S', 'N', 'S',
            'N', NULL, NULL, NULL, NULL, 'S', 'N', 0, 365, 'N', 'N', 'N', 'N',
            'N', 'S', 'N', 'N', 'N', 'S', 'N', '', 'S', 'N', 'N', NULL,
            'DOCUMENTO SOPORTE', '2021-07-22', 'N', 'N', 'N', 'N', 'S');

    FOR SELECT DISTINCT Codperfil
        FROM Per_Documentos
        WHERE Coddocumento IN ('DS', 'DS1', 'DS2', 'EQ', 'EQ1', 'EQ2')
        INTO V_Perfil
    DO
    BEGIN
      INSERT INTO Per_Documentos (Codperfil, Coddocumento, Activo)
      VALUES (:V_Perfil, 'DSE2', 'S');
    END

    --VAL_DOCUMENTOS
    INSERT INTO Val_Documentos (Coddocumento, Codprefijo, Codescenario,
                                Codvendedor, Codlista, Codtercero, Codbodega,
                                Codbanco, Codcentro, Codreferencia, Notac,
                                Notam)
    SELECT 'DSE2',
           '_',
           Codescenario,
           Codvendedor,
           Codlista,
           Codtercero,
           Codbodega,
           Codbanco,
           Codcentro,
           Codreferencia,
           Notac,
           Notam
    FROM Val_Documentos
    WHERE Coddocumento = 'DS2';
  END
  ELSE
  IF (V_Conta = 0) THEN
    UPDATE Documentos
    SET Codigo_Fe = 'DOCUMENTO SOPORTE', Formato = 'F_DSE_GES_01A.FR3',
        Formato1 = 'F_DSE_GES_01A.FR3',
        Nombre = 'SOPORTE ELECTRONICO EN COMPRAS NO OBLIGADOS A FACTURAR (Contado)', Nombre_Fe = 'DOCUMENTO SOPORTE EN ADQUISICIONES EFECTUADAS A NO OBLIGADOS A FACTURAR'
    WHERE Codigo = 'DSE2'  AND TRIM(COALESCE(Formato1, '')) = '';
  ELSE
    UPDATE Documentos
    SET Codigo_Fe = 'DOCUMENTO SOPORTE', Formato = 'F_DSE_CON_01A.FR3',
        Formato4 = 'F_DSE_CON_01A.FR3',
        Nombre = 'SOPORTE ELECTRONICO EN COMPRAS NO OBLIGADOS A FACTURAR (Contado)', Nombre_Fe = 'DOCUMENTO SOPORTE EN ADQUISICIONES EFECTUADAS A NO OBLIGADOS A FACTURAR'
    WHERE Codigo = 'DSE2'  AND TRIM(COALESCE(Formato4, '')) = '';

  --DSE
  SELECT COUNT(1)
  FROM Documentos
  WHERE Codigo = 'DSE'
  INTO V_Cont;
  IF (V_Cont = 0) THEN
  BEGIN
    INSERT INTO Documentos (Codigo, Nombre, Nombre_Fe, Descripcion, Formato,
                            Formato1, Formato2, Formato3, Formato4, Activo,
                            Activo_Contabilidad, Niif, Excluir_Niif, Signo,
                            Grupo, Es_Compra_Venta, Es_Activo, Tesoreria,
                            Inventario, Cartera, Nomina, Registro_Pago,
                            Impuestos, Retenciones, Comision, Contabilidad,
                            C_Vence, C_Condicionado, C_Codtercero,
                            C_Codvendedor, C_Codlista, C_Nota, C_Codrp,
                            C_Codingreso, C_Codpoliza, C_Codreferido, M_Aplica,
                            M_Cuota, M_Documento, M_Descuento, M_Interes,
                            M_Pronto_Pago, M_Codtercero, M_Nota, Afecta,
                            Valoriza, Costea, Valida_Saldos, Con_Impuestos,
                            Codinteres_Debito, Codinteres_Credito,
                            Codpronto_Pagos, Codcopago, Publica_Retenciones,
                            Base_Dia, Pooling, Duracion, Ensamble,
                            Es_Produccion, Puntos, Rastreo, Exportar, Compartir,
                            Dimensiones, Es_Presupuesto, Analisis_Costos,
                            Rotacion, Deterioro, Logistica, Mapeado, Nativo,
                            Impuesto_Alterno, Juego, Codigo_Fe, Modificado, Aiu,
                            Dia_Sin_Iva, Copago, Anexo, Valida_Fe)
    VALUES ('DSE', 'SOPORTE ELECTRONICO EN COMPRAS NO OBLIGADOS A FACTURAR ',
            'DOCUMENTO SOPORTE EN ADQUISICIONES EFECTUADAS A NO OBLIGADOS A FACTURAR',
            'Aplica sólo en Módulo Contable. Util para compras y gastos a crédito (+CxP). Retención será calculada y publicada',
            'F_DSE_CON_01A.FR3', '', '', '', 'F_DSE_CON_01A.FR3', 'N', 'S', 'N',
            'N', 'DEBITO', 'COMPRA', 'S', 'N', 'NO', 'ENTRADA', 'S', 'N', 'N',
            'S', 'S', 'N', 'N', 'S', 'N', 'S', 'S', 'S', 'S', 'N', 'N', 'N',
            'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'S', 'S', 'S', 'N', 'S',
            'N', NULL, NULL, NULL, NULL, 'S', 'N', 0, 0, 'N', 'N', 'N', 'N',
            'N', 'S', 'N', 'N', 'N', 'N', 'N', '', 'S', 'N', 'N', NULL,
            'DOCUMENTO SOPORTE', '2021-07-22', 'N', 'N', 'N', 'N', 'S');

    FOR SELECT DISTINCT Codperfil
        FROM Per_Documentos
        WHERE Coddocumento IN ('DS', 'DS1', 'DS2', 'EQ', 'EQ1', 'EQ2')
        INTO V_Perfil
    DO
    BEGIN
      INSERT INTO Per_Documentos (Codperfil, Coddocumento, Activo)
      VALUES (:V_Perfil, 'DSE', 'S');
    END

    --VAL_DOCUMENTOS
    INSERT INTO Val_Documentos (Coddocumento, Codprefijo, Codescenario,
                                Codvendedor, Codlista, Codtercero, Codbodega,
                                Codbanco, Codcentro, Codreferencia, Notac,
                                Notam)
    SELECT 'DSE',
           '_',
           Codescenario,
           Codvendedor,
           Codlista,
           Codtercero,
           Codbodega,
           Codbanco,
           Codcentro,
           Codreferencia,
           Notac,
           Notam
    FROM Val_Documentos
    WHERE Coddocumento = 'DS';
  END
  ELSE
  IF (V_Conta > 0) THEN
    UPDATE Documentos
    SET Codigo_Fe = 'DOCUMENTO SOPORTE', Formato = 'F_DSE_CON_01A.FR3',
        Formato4 = 'F_DSE_CON_01A.FR3',
        Nombre = 'SOPORTE ELECTRONICO EN COMPRAS NO OBLIGADOS A FACTURAR ', Nombre_Fe = 'DOCUMENTO SOPORTE EN ADQUISICIONES EFECTUADAS A NO OBLIGADOS A FACTURAR'
    WHERE Codigo = 'DSE'  AND TRIM(COALESCE(Formato4, '')) = '';
  ELSE
    UPDATE Documentos
    SET Codigo_Fe = 'DOCUMENTO SOPORTE', Formato = 'F_DSE_GES_01A.FR3',
        Formato1 = 'F_DSE_GES_01A.FR3',
        Nombre = 'SOPORTE ELECTRONICO EN COMPRAS NO OBLIGADOS A FACTURAR ', Nombre_Fe = 'DOCUMENTO SOPORTE EN ADQUISICIONES EFECTUADAS A NO OBLIGADOS A FACTURAR'
    WHERE Codigo = 'DSE'  AND TRIM(COALESCE(Formato1, '')) = '';

  --NOTAS
  --DSNC1
  SELECT COUNT(1)
  FROM Documentos
  WHERE Codigo = 'DSNC1'
  INTO V_Cont;
  IF (V_Cont = 0) THEN
  BEGIN
    INSERT INTO Documentos (Codigo, Nombre, Nombre_Fe, Descripcion, Formato,
                            Formato1, Formato2, Formato3, Formato4, Activo,
                            Activo_Contabilidad, Niif, Excluir_Niif, Signo,
                            Grupo, Es_Compra_Venta, Es_Activo, Tesoreria,
                            Inventario, Cartera, Nomina, Registro_Pago,
                            Impuestos, Retenciones, Comision, Contabilidad,
                            C_Vence, C_Condicionado, C_Codtercero,
                            C_Codvendedor, C_Codlista, C_Nota, C_Codrp,
                            C_Codingreso, C_Codpoliza, C_Codreferido, M_Aplica,
                            M_Cuota, M_Documento, M_Descuento, M_Interes,
                            M_Pronto_Pago, M_Codtercero, M_Nota, Afecta,
                            Valoriza, Costea, Valida_Saldos, Con_Impuestos,
                            Codinteres_Debito, Codinteres_Credito,
                            Codpronto_Pagos, Codcopago, Publica_Retenciones,
                            Base_Dia, Pooling, Duracion, Ensamble,
                            Es_Produccion, Puntos, Rastreo, Exportar, Compartir,
                            Dimensiones, Es_Presupuesto, Analisis_Costos,
                            Rotacion, Deterioro, Logistica, Mapeado, Nativo,
                            Impuesto_Alterno, Juego, Codigo_Fe, Modificado, Aiu,
                            Dia_Sin_Iva, Copago, Anexo, Valida_Fe)
    VALUES ('DSNC1', 'NOTA DE AJUSTE SOPORTE ELECTRONICO COMPRAS (-CxP)',
            'NOTA DE AJUSTE DEL DOCUMENTO SOPORTE EN ADQUISICIONES EFECTUADAS A SUJETOS NO OBLIGADOS A EXPEDIR FACTURA O DOCUMENTO EQUIVALENTE',
            'Util para manejar Devoluciones o notas para disminuir la CxP. Retención será calculada y publicada',
            'F_NDS_GES_01A.FR3', 'F_NDS_GES_01A.FR3', 'F_NDS_GES_01A.FR3',
            'F_NDS_GES_01A.FR3', NULL, 'S', 'N', 'N', 'N', 'CREDITO', 'COMPRA',
            'S', 'N', 'NO', 'SALIDA', 'S', 'N', 'N', 'S', 'S', 'N', 'S', 'N',
            'N', 'S', 'S', 'S', 'S', 'N', 'N', 'N', 'N', 'S', 'N', 'N', 'N',
            'N', 'N', 'N', 'S', 'S', 'N', 'N', 'S', 'N', NULL, NULL, NULL, NULL,
            'S', 'N', 0, 365, 'N', 'N', 'N', 'N', 'N', 'S', 'N', 'N', 'N', 'N',
            'N', NULL, 'S', 'N', 'N', 'N', 'NOTA DE AJUSTE A DS', '2020-08-17',
            'N', 'N', 'N', 'N', 'S');

    FOR SELECT DISTINCT Codperfil
        FROM Per_Documentos
        WHERE Coddocumento IN ('DS', 'DS1', 'DS2', 'EQ', 'EQ1', 'EQ2')
        INTO V_Perfil
    DO
    BEGIN
      INSERT INTO Per_Documentos (Codperfil, Coddocumento, Activo)
      VALUES (:V_Perfil, 'DSNC1', 'S');
    END

    --VAL_DOCUMENTOS
    INSERT INTO Val_Documentos (Coddocumento, Codprefijo, Codescenario,
                                Codvendedor, Codlista, Codtercero, Codbodega,
                                Codbanco, Codcentro, Codreferencia, Notac,
                                Notam)
    SELECT 'DSNC1',
           '_',
           Codescenario,
           Codvendedor,
           Codlista,
           Codtercero,
           Codbodega,
           Codbanco,
           Codcentro,
           Codreferencia,
           Notac,
           Notam
    FROM Val_Documentos
    WHERE Coddocumento = 'NP1';

  END
  ELSE
    UPDATE Documentos
    SET Codigo_Fe = 'NOTA DE AJUSTE A DS', Formato = 'F_NDS_GES_01A.FR3',
        Formato1 = 'F_NDS_GES_01A.FR3',
        Nombre = 'NOTA DE AJUSTE SOPORTE ELECTRONICO COMPRAS (-CxP)', Nombre_Fe = 'NOTA DE AJUSTE DEL DOCUMENTO SOPORTE EN ADQUISICIONES EFECTUADAS A SUJETOS NO OBLIGADOS A EXPEDIR FACTURA O DOCUMENTO EQUIVALENTE'
    WHERE Codigo = 'DSNC1';

  --DSNC2
  SELECT COUNT(1)
  FROM Documentos
  WHERE Codigo = 'DSNC2'
  INTO V_Cont;
  IF (V_Cont = 0) THEN
  BEGIN

    INSERT INTO Documentos (Codigo, Nombre, Nombre_Fe, Descripcion, Formato,
                            Formato1, Formato2, Formato3, Formato4, Activo,
                            Activo_Contabilidad, Niif, Excluir_Niif, Signo,
                            Grupo, Es_Compra_Venta, Es_Activo, Tesoreria,
                            Inventario, Cartera, Nomina, Registro_Pago,
                            Impuestos, Retenciones, Comision, Contabilidad,
                            C_Vence, C_Condicionado, C_Codtercero,
                            C_Codvendedor, C_Codlista, C_Nota, C_Codrp,
                            C_Codingreso, C_Codpoliza, C_Codreferido, M_Aplica,
                            M_Cuota, M_Documento, M_Descuento, M_Interes,
                            M_Pronto_Pago, M_Codtercero, M_Nota, Afecta,
                            Valoriza, Costea, Valida_Saldos, Con_Impuestos,
                            Codinteres_Debito, Codinteres_Credito,
                            Codpronto_Pagos, Codcopago, Publica_Retenciones,
                            Base_Dia, Pooling, Duracion, Ensamble,
                            Es_Produccion, Puntos, Rastreo, Exportar, Compartir,
                            Dimensiones, Es_Presupuesto, Analisis_Costos,
                            Rotacion, Deterioro, Logistica, Mapeado, Nativo,
                            Impuesto_Alterno, Juego, Codigo_Fe, Modificado, Aiu,
                            Dia_Sin_Iva, Copago, Anexo, Valida_Fe)
    VALUES ('DSNC2',
            'NOTA DE AJUSTE SOPORTE ELECTRONICO COMPRAS (Sobre DSE2 de Contado)',
            'NOTA DE AJUSTE DEL DOCUMENTO SOPORTE EN ADQUISICIONES EFECTUADAS A SUJETOS NO OBLIGADOS A EXPEDIR FACTURA O DOCUMENTO EQUIVALENTE',
            'Util para devolver compras realizadas de Contado. Retención será calculada y publicada',
            'F_NDS_GES_01A.FR3', 'F_NDS_GES_01A.FR3', 'F_NDS_GES_01A.FR3',
            'F_NDS_GES_01A.FR3', NULL, 'S', 'N', 'N', 'N', 'CREDITO', 'COMPRA',
            'S', 'N', 'CONTADO', 'SALIDA', 'N', 'N', 'N', 'S', 'S', 'N', 'S',
            'N', 'N', 'S', 'S', 'S', 'S', 'N', 'N', 'N', 'N', 'S', 'N', 'N',
            'S', 'N', 'N', 'N', 'S', 'S', 'N', 'N', 'S', 'N', NULL, NULL, NULL,
            NULL, 'S', 'N', 0, 365, 'N', 'N', 'N', 'N', 'N', 'S', 'N', 'N', 'N',
            'N', 'N', NULL, 'S', 'N', 'N', 'N', 'NOTA DE AJUSTE A DS',
            '2020-05-21', 'N', 'N', 'N', 'N', 'S');

    FOR SELECT DISTINCT Codperfil
        FROM Per_Documentos
        WHERE Coddocumento IN ('DS', 'DS1', 'DS2', 'EQ', 'EQ1', 'EQ2')
        INTO V_Perfil
    DO
    BEGIN
      INSERT INTO Per_Documentos (Codperfil, Coddocumento, Activo)
      VALUES (:V_Perfil, 'DSNC2', 'S');
    END

    --VAL_DOCUMENTOS
    INSERT INTO Val_Documentos (Coddocumento, Codprefijo, Codescenario,
                                Codvendedor, Codlista, Codtercero, Codbodega,
                                Codbanco, Codcentro, Codreferencia, Notac,
                                Notam)
    SELECT 'DSNC2',
           '_',
           Codescenario,
           Codvendedor,
           Codlista,
           Codtercero,
           Codbodega,
           Codbanco,
           Codcentro,
           Codreferencia,
           Notac,
           Notam
    FROM Val_Documentos
    WHERE Coddocumento = 'NP2';

  END
  ELSE
    UPDATE Documentos
    SET Codigo_Fe = 'NOTA DE AJUSTE A DS', Formato = 'F_NDS_GES_01A.FR3',
        Formato1 = 'F_NDS_GES_01A.FR3',
        Nombre = 'NOTA DE AJUSTE SOPORTE ELECTRONICO COMPRAS (Sobre DSE2 de Contado)', Nombre_Fe = 'NOTA DE AJUSTE DEL DOCUMENTO SOPORTE EN ADQUISICIONES EFECTUADAS A SUJETOS NO OBLIGADOS A EXPEDIR FACTURA O DOCUMENTO EQUIVALENTE'
    WHERE Codigo = 'DSNC2';

  --DSNC
  SELECT COUNT(1)
  FROM Documentos
  WHERE Codigo = 'DSNC'
  INTO V_Cont;
  IF (V_Cont = 0) THEN
  BEGIN
    INSERT INTO Documentos (Codigo, Nombre, Nombre_Fe, Descripcion, Formato,
                            Formato1, Formato2, Formato3, Formato4, Activo,
                            Activo_Contabilidad, Niif, Excluir_Niif, Signo,
                            Grupo, Es_Compra_Venta, Es_Activo, Tesoreria,
                            Inventario, Cartera, Nomina, Registro_Pago,
                            Impuestos, Retenciones, Comision, Contabilidad,
                            C_Vence, C_Condicionado, C_Codtercero,
                            C_Codvendedor, C_Codlista, C_Nota, C_Codrp,
                            C_Codingreso, C_Codpoliza, C_Codreferido, M_Aplica,
                            M_Cuota, M_Documento, M_Descuento, M_Interes,
                            M_Pronto_Pago, M_Codtercero, M_Nota, Afecta,
                            Valoriza, Costea, Valida_Saldos, Con_Impuestos,
                            Codinteres_Debito, Codinteres_Credito,
                            Codpronto_Pagos, Codcopago, Publica_Retenciones,
                            Base_Dia, Pooling, Duracion, Ensamble,
                            Es_Produccion, Puntos, Rastreo, Exportar, Compartir,
                            Dimensiones, Es_Presupuesto, Analisis_Costos,
                            Rotacion, Deterioro, Logistica, Mapeado, Nativo,
                            Impuesto_Alterno, Juego, Codigo_Fe, Modificado, Aiu,
                            Dia_Sin_Iva, Copago, Anexo, Valida_Fe)
    VALUES ('DSNC', 'NOTA DE AJUSTE SOPORTE ELECTRONICO COMPRAS',
            'NOTA DE AJUSTE DEL DOCUMENTO SOPORTE EN ADQUISICIONES EFECTUADAS A SUJETOS NO OBLIGADOS A EXPEDIR FACTURA O DOCUMENTO EQUIVALENTE',
            'Util para manejar Devoluciones o notas para disminuir la CxP. Retención será calculada y publicada',
            'F_NDS_CON_01A.FR3', NULL, NULL, NULL, 'F_NDS_CON_01A.FR3', 'S',
            'N', 'N', 'N', 'CREDITO', 'COMPRA', 'S', 'N', 'NO', 'SALIDA', 'S',
            'N', 'N', 'S', 'S', 'N', 'S', 'N', 'N', 'S', 'S', 'S', 'S', 'N',
            'N', 'N', 'N', 'S', 'N', 'N', 'N', 'N', 'N', 'N', 'S', 'S', 'N',
            'N', 'S', 'N', NULL, NULL, NULL, NULL, 'S', 'N', 0, 365, 'N', 'N',
            'N', 'N', 'N', 'S', 'N', 'N', 'N', 'N', 'N', NULL, 'S', 'N', 'N',
            'N', 'NOTA DE AJUSTE A DS', '2020-08-17', 'N', 'N', 'N', 'N', 'S');

    FOR SELECT DISTINCT Codperfil
        FROM Per_Documentos
        WHERE Coddocumento IN ('DS', 'DS1', 'DS2', 'EQ', 'EQ1', 'EQ2')
        INTO V_Perfil
    DO
    BEGIN
      INSERT INTO Per_Documentos (Codperfil, Coddocumento, Activo)
      VALUES (:V_Perfil, 'DSNC', 'S');
    END

    --VAL_DOCUMENTOS
    INSERT INTO Val_Documentos (Coddocumento, Codprefijo, Codescenario,
                                Codvendedor, Codlista, Codtercero, Codbodega,
                                Codbanco, Codcentro, Codreferencia, Notac,
                                Notam)
    SELECT 'DSNC',
           '_',
           Codescenario,
           Codvendedor,
           Codlista,
           Codtercero,
           Codbodega,
           Codbanco,
           Codcentro,
           Codreferencia,
           Notac,
           Notam
    FROM Val_Documentos
    WHERE Coddocumento = 'NP1';

  END
  ELSE
    UPDATE Documentos
    SET Codigo_Fe = 'NOTA DE AJUSTE A DS', Formato = 'F_NDS_CON_01A.FR3',
        Formato4 = 'F_NDS_CON_01A.FR3',
        Nombre = 'NOTA DE AJUSTE SOPORTE ELECTRONICO COMPRAS', Nombre_Fe = 'NOTA DE AJUSTE DEL DOCUMENTO SOPORTE EN ADQUISICIONES EFECTUADAS A SUJETOS NO OBLIGADOS A EXPEDIR FACTURA O DOCUMENTO EQUIVALENTE'
    WHERE Codigo = 'DSNC';

  ----------------
  V_Cont = 0;
  SELECT COUNT(1)
  FROM Auto_Patrones
  WHERE Tipo = 'DSE1'
  INTO V_Cont;

  IF (V_Cont > 0) THEN
    UPDATE Documentos
    SET Codigo_Fe = 'DOCUMENTO SOPORTE', Formato = 'F_DSE_CON_01A.FR3', Formato4 = 'F_DSE_CON_01A.FR3'
    WHERE Codigo = 'DSE1';

  SELECT COUNT(1)
  FROM Auto_Patrones
  WHERE Tipo = 'DSNC1'
  INTO V_Cont;

  IF (V_Cont > 0) THEN
    UPDATE Documentos
    SET Codigo_Fe = 'NOTA DE AJUSTE A DS', Formato = 'F_NDS_CON_01A.FR3', Formato4 = 'F_NDS_CON_01A.FR3'
    WHERE Codigo = 'DSNC1';
END; 

COMMIT WORK;

EXECUTE BLOCK
AS
DECLARE VARIABLE V_Cont INTEGER;

BEGIN
  UPDATE OR INSERT INTO Prefijos (Codigo, Nombre)
  VALUES ('NDS', 'NOTA DE AJUSTE A DS');

  --DSNC1
  SELECT COUNT(1)
  FROM Val_Documentos
  WHERE Coddocumento = 'DSNC1'
  INTO V_Cont;

  IF (V_Cont > 0) THEN
    UPDATE Val_Documentos
    SET Codprefijo = 'NDS'
    WHERE Coddocumento = 'DSNC1';
  ELSE
    INSERT INTO Val_Documentos (Coddocumento, Codprefijo, Codescenario,
                                Codvendedor, Codlista, Codtercero, Codbodega,
                                Codbanco, Codcentro, Codreferencia, Notac,
                                Notam)
    SELECT 'DSNC1',
           'NDS',
           Codescenario,
           Codvendedor,
           Codlista,
           Codtercero,
           Codbodega,
           Codbanco,
           Codcentro,
           Codreferencia,
           Notac,
           Notam
    FROM Val_Documentos
    WHERE Coddocumento = 'NP1';

  SELECT COUNT(1)
  FROM Val_Documentos
  WHERE Coddocumento = 'DSNC2'
  INTO V_Cont;

  IF (V_Cont > 0) THEN
    UPDATE Val_Documentos
    SET Codprefijo = 'NDS'
    WHERE Coddocumento = 'DSNC2';
  ELSE
    INSERT INTO Val_Documentos (Coddocumento, Codprefijo, Codescenario,
                                Codvendedor, Codlista, Codtercero, Codbodega,
                                Codbanco, Codcentro, Codreferencia, Notac,
                                Notam)
    SELECT 'DSNC2',
           'NDS',
           Codescenario,
           Codvendedor,
           Codlista,
           Codtercero,
           Codbodega,
           Codbanco,
           Codcentro,
           Codreferencia,
           Notac,
           Notam
    FROM Val_Documentos
    WHERE Coddocumento = 'NP2';

  SELECT COUNT(1)
  FROM Val_Documentos
  WHERE Coddocumento = 'DSNC'
  INTO V_Cont;

  IF (V_Cont > 0) THEN
    UPDATE Val_Documentos
    SET Codprefijo = 'NDS'
    WHERE Coddocumento = 'DSNC';
  ELSE
    INSERT INTO Val_Documentos (Coddocumento, Codprefijo, Codescenario,
                                Codvendedor, Codlista, Codtercero, Codbodega,
                                Codbanco, Codcentro, Codreferencia, Notac,
                                Notam)
    SELECT 'DSNC',
           'NDS',
           Codescenario,
           Codvendedor,
           Codlista,
           Codtercero,
           Codbodega,
           Codbanco,
           Codcentro,
           Codreferencia,
           Notac,
           Notam
    FROM Val_Documentos
    WHERE Coddocumento = 'NP1';
END;

COMMIT WORK;

UPDATE OR INSERT INTO NOTAS_DS (CODIGO, NOMBRE)
                        VALUES ('1', 'Devolucion parcial de los bienes y/o no aceptacion parcial del servicio')
                      MATCHING (CODIGO);
UPDATE OR INSERT INTO NOTAS_DS (CODIGO, NOMBRE)
                        VALUES ('2', 'Anulacion del Documento Soporte en Adquisiciones efectuadas a sujetos no obligados a expedir factura de venta o documento equivalente')
                      MATCHING (CODIGO);
UPDATE OR INSERT INTO NOTAS_DS (CODIGO, NOMBRE)
                        VALUES ('3', 'Rebaja o descuento parcial o total')
                      MATCHING (CODIGO);
UPDATE OR INSERT INTO NOTAS_DS (CODIGO, NOMBRE)
                        VALUES ('4', 'Ajuste de precio')
                      MATCHING (CODIGO);
UPDATE OR INSERT INTO NOTAS_DS (CODIGO, NOMBRE)
                        VALUES ('5', 'Otros')
                      MATCHING (CODIGO);


COMMIT WORK;

-- Correccion descripciones

UPDATE DOCUMENTOS SET 
    DESCRIPCION = 'Afecta compras aumenta CxP. Retencion sera calculada y publicada',
    IMPUESTOS = 'N', CON_IMPUESTOS = 'N'
WHERE (CODIGO = 'DSE1');

UPDATE DOCUMENTOS SET 
    DESCRIPCION = 'Pago de contado, afecta tesoreria. Retencion sera calculada y publicada',
    IMPUESTOS = 'N', CON_IMPUESTOS = 'N'
WHERE (CODIGO = 'DSE2');

UPDATE DOCUMENTOS SET 
    DESCRIPCION = 'Aplica solo en Modulo Contable. Util para compras y gastos a credito (+CxP). Retencion sera calculada y publicada',
    IMPUESTOS = 'N', CON_IMPUESTOS = 'N'
WHERE (CODIGO = 'DSE');

UPDATE DOCUMENTOS SET 
    DESCRIPCION = 'Util para manejar Devoluciones o notas para disminuir la CxP. Retencion sera calculada y publicada',
    IMPUESTOS = 'N', CON_IMPUESTOS = 'N'
WHERE (CODIGO = 'DSNC1');

UPDATE DOCUMENTOS SET 
    DESCRIPCION = 'Util para devolver compras realizadas de Contado. Retencion sera calculada y publicada',
    IMPUESTOS = 'N', CON_IMPUESTOS = 'N'
WHERE (CODIGO = 'DSNC2');

UPDATE DOCUMENTOS SET 
    DESCRIPCION = 'Util para manejar Devoluciones o notas para disminuir la CxP. Retencion sera calculada y publicada',
    ACTIVO = 'N',
    ACTIVO_CONTABILIDAD = 'S',
    IMPUESTOS = 'N', CON_IMPUESTOS = 'N'
WHERE (CODIGO = 'DSNC');

COMMIT WORK;


EXECUTE BLOCK
AS
DECLARE VARIABLE V_Cont INTEGER;
BEGIN
  SELECT COUNT(1)
  FROM Pta
  WHERE Codigo = 'NOOVAC'
        AND Activo = 'S'
  INTO V_Cont;

  IF (V_Cont > 0) THEN
    UPDATE Documentos
    SET Impuestos = 'S'
    WHERE Codigo IN ('DSE', 'DSE1', 'DSE2', 'DSNC', 'DSNC1', 'DSNC2');

  UPDATE Documentos
  SET Valida_Fe = 'S'
  WHERE Codigo IN ('DSNC', 'DSNC1', 'DSNC2');
END;

COMMIT WORK;

UPDATE Documentos
    SET Impuestos = 'S'
    WHERE Codigo = 'DSE';

COMMIT WORK;

/* Cargos */
UPDATE OR INSERT INTO CARGOS (CODIGO, NOMBRE, MODIFICADO)
                      VALUES ('FECRE', 'FACTURACION CONTADO CUANDO SOLO FE CREDITO', '2022-10-05')
                    MATCHING (CODIGO);

UPDATE OR INSERT INTO CARGOS (CODIGO, NOMBRE, MODIFICADO)
                      VALUES ('DSCOR', 'CORREO PARA DOCUMENTO SOPORTE', '2022-10-05')
                    MATCHING (CODIGO);

COMMIT WORK;


/************************************************************/
/****                 MODULO CONTABLE                    ****/
/************************************************************/

/* EXPORTADOS CONTABLE BEGIN */

/* EXPORTADOS CONTABLE  */

DELETE FROM EXPORTADOS
WHERE PT = 'NOOVAC';

UPDATE PTA SET 
    XML_TEMPLATE = '<?xml version="1.0" encoding="UTF-8"?>
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:noov="http://Noova/">
    <soapenv:Header>
        <noov:AuthHeader>
            <!--Usuario base64:-->
            <noov:Username>UsuarioNoova</noov:Username>
            <!--Password base64:-->
            <noov:Password>ClaveNoova</noov:Password>
        </noov:AuthHeader>
    </soapenv:Header>
    <!--950 Contable 31/OCT/2022 v1-->
    <soapenv:Body>
        <noov:SetFacFacturas>
            <noov:dtoFacFacturasInput>
            con:comprobante
            <noov:lDetalle>
                con:detalle
            </noov:lDetalle>
            <noov:lImpuestos>
                con:impuestos
            </noov:lImpuestos>
            con:tasa_cambio
            <noov:Waitpdf>
                <noov:Nvwai_tiem>2880</noov:Nvwai_tiem>
            </noov:Waitpdf>
            </noov:dtoFacFacturasInput>
        </noov:SetFacFacturas>
    </soapenv:Body>
</soapenv:Envelope>'
WHERE (CODIGO = 'NOOVAC');

COMMIT WORK;

UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (461, 'con:comprobante', 'DOC', '-- Comprobante Contable Factura 950 31/OCT/2022 v1

WITH V_Trm
AS (SELECT Valor AS Trm
    FROM Pz_Fe_Auto_Trm(:Tipo,:Prefijo,:Numero))

SELECT "noov:Nvfac_orig",
       "noov:Nvemp_nnit",
       "noov:Nvres_nume",
       "noov:Nvfac_tipo",
       "noov:Nvres_pref",
       "noov:Nvfac_nume",
       "noov:Nvfac_fech",
       "noov:Nvfac_cdet",
       "noov:Nvfac_venc",
       "noov:Nvsuc_codi",
       "noov:Nvmon_codi",
       "noov:Nvfor_codi",
       "noov:Nvven_nomb",
       "noov:Nvfac_fpag",
       "noov:Nvfac_conv",
       "noov:Nvcli_cper",
       "noov:Nvcli_cdoc",
       "noov:Nvcli_docu",
       "noov:Nvcli_pais",
       "noov:Nvcli_depa",
       "noov:Nvcli_ciud",
       "noov:Nvcli_loca",
       "noov:Nvcli_dire",
       "noov:Nvcli_ntel",
       "noov:Nvcli_regi",
       "noov:Nvcli_fisc",
       "noov:Nvcli_nomb",
       "noov:Nvcli_pnom",
       "noov:Nvcli_snom",
       "noov:Nvcli_apel",
       "noov:Nvcli_mail",
       "noov:Nvema_copi",
       "noov:Nvfac_obse",
       "noov:Nvfac_orde",
       "noov:Nvfac_remi",
       "noov:Nvfac_rece",
       "noov:Nvfac_entr",
       "noov:Nvfac_ccos",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_stot" / Trm,2)) "noov:Nvfac_stot",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc" / Trm,2)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_anti" / Trm,2)) "noov:Nvfac_anti",
       "noov:Nvfac_carg",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_tota" / Trm,2)) "noov:Nvfac_tota",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_totp" / Trm,2)) "noov:Nvfac_totp",
       --Ajuste al redondeo
       --totp
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_totp" / Trm,2)) -
       --stot
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_stot" / Trm,2)) -
       --Impuestos
       COALESCE((SELECT Valor
                 FROM Redondeo_Dian((SELECT SUM((SELECT Valor
                                                 FROM Redondeo_Dian("noov:Nvimp_valo",2)))
                                     FROM Pz_Fe_Auto_Imp(:Tipo,:Prefijo,:Numero)
                                     WHERE "noov:Nvimp_oper" = ''S'') / Trm,2)),0) +
       --descuentos
       COALESCE((SELECT Valor
                 FROM Redondeo_Dian("noov:Nvfac_desc" / Trm,2)),0) + COALESCE((SELECT Valor
                                                                               FROM Redondeo_Dian("noov:Nvfac_anti" / Trm,2)),0) "noov:Nvfac_roun",
       "noov:Nvfac_vcop",
       "noov:Nvfac_timp",
       "noov:Nvfac_coid",
       "noov:Nvfac_obsb",
       "noov:Nvcli_ncon",
       '''' "noov:Nvfor_oper"
FROM Pz_Fe_Auto_Comp(:Tipo,:Prefijo,:Numero,:Emisor,:Receptor)
JOIN V_Trm ON (1 = 1)', 'S', 5, 'NOOVAC', 'FACTURA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (462, 'con:detalle', 'DOC', '-- Detalle Contable Factura

WITH V_Trm
AS (SELECT Valor AS Trm
    FROM Pz_Fe_Auto_Trm(:Tipo, :Prefijo, :Numero))

SELECT Consecutivo,
       "noov:Nvpro_codi",
       "noov:Nvpro_nomb",
       "noov:Nvuni_desc",
       "noov:Nvfac_cant",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_valo" / Trm, 2)) "noov:Nvfac_valo",
       "noov:Nvfac_pdes",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc" / Trm, 2)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian(Stot_Red / Trm, 2)) "noov:Nvfac_stot",
       "noov:Nvimp_cdia",
       "noov:Nvdet_piva",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvdet_viva" / Trm, 2)) "noov:Nvdet_viva",
       "noov:Nvdet_tcod",
       "noov:Nvpro_cean",
       "noov:Nvdet_entr",
       "noov:Nvuni_quan",
       "noov:Nvdet_nota",
       "noov:Nvdet_padr",
       "noov:Nvdet_marc",
       "noov:Nvdet_mode"
FROM Pz_Fe_Auto_Det(:Tipo, :Prefijo, :Numero)
JOIN V_Trm ON (1 = 1)', 'S', 10, 'NOOVAC', 'FACTURA', 'noov:DTOFacDetallefacturas', NULL, 'noov:Nvfac_dcop', NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (463, 'con:impuestos', 'DOC', '-- Impuestos Contable Factura

WITH V_Trm
AS (SELECT Valor AS Trm
    FROM Pz_Fe_Auto_Trm(:Tipo, :Prefijo, :Numero))

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_oper",
       "noov:Nvimp_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base" / Trm, 2)) "noov:Nvimp_base",
       "noov:Nvimp_porc" / IIF(Es_Tarifa = ''N'', Trm, 1) "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo" / Trm, 2)) "noov:Nvimp_valo"
FROM Pz_Fe_Auto_Imp(:Tipo, :Prefijo, :Numero)
JOIN V_Trm ON (1 = 1)
WHERE "noov:Nvimp_cdia" NOT IN (''07'', ''99'')', 'S', 15, 'NOOVAC', 'FACTURA', 'noov:DTOFacImpuestosfacturas', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (464, 'con:mandatario', 'DOC', '-- Mandatario CONTABLE FACTURA

SELECT
       CASE T.Naturaleza
         WHEN ''J'' THEN 1
         WHEN ''N'' THEN 2
         ELSE ''''
       END "noov:Nvman_cper",
       Codidentidad "noov:Nvman_cdoc",
       TRIM(C.Codtercero) || IIF(TRIM(T.Codidentidad) = ''31'', IIF(T.Dv IS NULL, '''', ''-'' || TRIM(T.Dv)), '''') "noov:Nvman_docu",
       T.Email "noov:Nvman_mail",
       T.Direccion "noov:Nvman_dire",
       S.Codigo_Fe "noov:Nvman_regi",
       COALESCE(T.Empresa, '''') "noov:Nvman_nomb",
       COALESCE(T.Nom1, '''') "noov:Nvman_pnom",
       COALESCE(T.Nom2, '''') "noov:Nvman_snom",
       TRIM(COALESCE(T.Apl1, '''')) || '' '' || TRIM(COALESCE(T.Apl2, '''')) "noov:Nvman_apel",
       COALESCE(T.Telefono, '''') "noov:Nvman_ntel"
FROM Auto_Comp M
JOIN Centros C ON (M.Codcentro = C.Codigo)
LEFT JOIN Terceros T ON (C.Codtercero = T.Codigo)
LEFT JOIN Sociedades S ON (T.Codsociedad = S.Codigo)
WHERE Tipo = :Tipo
      AND Prefijo = :Prefijo
      AND Numero = :Numero
      AND COALESCE(C.Codtercero, '''') <> ''''', 'S', 20, 'NOOVAC', 'FACTURA', 'noov:Nvdet_mand', NULL, NULL, 'con:detalle')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (465, 'con:tasa_cambio', 'DOC', '--Tasa Cambio Contable Factura

SELECT FIRST 1 Codigo "noov:Nvtas_sour",
               ''COP'' "noov:Nvtas_targ",
               Valor "noov:Nvtas_calc",
               Fecha "noov:Nvtas_fech"
FROM Pz_Fe_Auto_Trm(:Tipo, :Prefijo, :Numero)
WHERE Codigo <> ''COP''', 'S', 25, 'NOOVAC', 'FACTURA', 'noov:TasaCambio', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (481, 'con:comprobante', 'DOC', '-- Comprobante Contable Exportacion

WITH V_Trm
AS (SELECT Valor AS Trm
    FROM Pz_Fe_Auto_Trm(:Tipo, :Prefijo, :Numero))

SELECT "noov:Nvfac_orig",
       "noov:Nvemp_nnit",
       "noov:Nvres_nume",
       "noov:Nvfac_tipo",
       "noov:Nvres_pref",
       "noov:Nvfac_nume",
       "noov:Nvfac_fech",
       "noov:Nvfac_cdet",
       "noov:Nvfac_venc",
       "noov:Nvsuc_codi",
       "noov:Nvmon_codi",
       "noov:Nvfor_codi",
       "noov:Nvven_nomb",
       "noov:Nvfac_fpag",
       "noov:Nvfac_conv",
       "noov:Nvcli_cper",
       "noov:Nvcli_cdoc",
       "noov:Nvcli_docu",
       "noov:Nvcli_pais",
       "noov:Nvcli_depa",
       "noov:Nvcli_ciud",
       "noov:Nvcli_loca",
       "noov:Nvcli_dire",
       "noov:Nvcli_ntel",
       "noov:Nvcli_regi",
       "noov:Nvcli_fisc",
       "noov:Nvcli_nomb",
       "noov:Nvcli_pnom",
       "noov:Nvcli_snom",
       "noov:Nvcli_apel",
       "noov:Nvcli_mail",
       "noov:Nvema_copi",
       "noov:Nvfac_obse",
       "noov:Nvfac_orde",
       "noov:Nvfac_remi",
       "noov:Nvfac_rece",
       "noov:Nvfac_entr",
       "noov:Nvfac_ccos",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_stot" / Trm, 2)) "noov:Nvfac_stot",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc" / Trm, 2)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_anti" / Trm, 2)) "noov:Nvfac_anti",
       "noov:Nvfac_carg",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_tota" / Trm, 2)) "noov:Nvfac_tota",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_totp" / Trm, 2)) "noov:Nvfac_totp",
       --Ajuste al redondeo
       --totp
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_totp" / Trm, 2)) -
       --stot
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_stot" / Trm, 2)) -
       --Impuestos
       COALESCE((SELECT Valor
                 FROM Redondeo_Dian((SELECT SUM("noov:Nvimp_valo")
                                     FROM Pz_Fe_Auto_Imp(:Tipo, :Prefijo, :Numero)
                                     WHERE "noov:Nvimp_oper" = ''S'') / Trm, 2)), 0) +
       --descuentos
       COALESCE((SELECT Valor
                 FROM Redondeo_Dian("noov:Nvfac_desc" / Trm, 2)), 0) + COALESCE((SELECT Valor
                                                                                 FROM Redondeo_Dian("noov:Nvfac_anti" / Trm, 2)), 0) "noov:Nvfac_roun",
       "noov:Nvfac_vcop",
       "noov:Nvfac_timp",
       "noov:Nvfac_coid",
       "noov:Nvfac_obsb",
       "noov:Nvcli_ncon",
      '''' "noov:Nvfor_oper"
FROM Pz_Fe_Auto_Comp(:Tipo, :Prefijo, :Numero, :Emisor, :Receptor)
JOIN V_Trm ON (1 = 1)', 'S', 5, 'NOOVAC', 'EXPORTACION', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (482, 'con:detalle', 'DOC', '-- Detalle Contable Exportacion

WITH V_Trm
AS (SELECT Valor AS Trm
    FROM Pz_Fe_Auto_Trm(:Tipo, :Prefijo, :Numero))

SELECT Consecutivo,
       "noov:Nvpro_codi",
       "noov:Nvpro_nomb",
       "noov:Nvuni_desc",
       "noov:Nvfac_cant",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_valo" / Trm, 2)) "noov:Nvfac_valo",
       "noov:Nvfac_pdes",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc" / Trm, 2)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian(Stot_Red / Trm, 2)) "noov:Nvfac_stot",
       "noov:Nvimp_cdia",
       "noov:Nvdet_piva",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvdet_viva" / Trm, 2)) "noov:Nvdet_viva",
       "noov:Nvdet_tcod",
       "noov:Nvpro_cean",
       "noov:Nvdet_entr",
       "noov:Nvuni_quan",
       "noov:Nvdet_nota",
       "noov:Nvdet_padr",
       "noov:Nvdet_marc",
       "noov:Nvdet_mode"
FROM Pz_Fe_Auto_Det(:Tipo, :Prefijo, :Numero)
JOIN V_Trm ON (1 = 1)', 'S', 10, 'NOOVAC', 'EXPORTACION', 'noov:DTOFacDetallefacturas', NULL, 'noov:Nvfac_dcop', NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (483, 'con:impuestos', 'DOC', '--Impuestos CONTABLE EXPORTACION

WITH V_Trm
AS (SELECT Valor AS Trm
    FROM Pz_Fe_Auto_Trm(:Tipo, :Prefijo, :Numero))

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_oper",
       "noov:Nvimp_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base" / Trm, 2)) "noov:Nvimp_base",
       "noov:Nvimp_porc" / IIF(Es_Tarifa = ''N'', Trm, 1) "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo" / Trm, 2)) "noov:Nvimp_valo"
FROM Pz_Fe_Auto_Imp(:Tipo, :Prefijo, :Numero)
JOIN V_Trm ON (1 = 1)
WHERE "noov:Nvimp_cdia" NOT IN (''07'', ''99'')', 'S', 15, 'NOOVAC', 'EXPORTACION', 'noov:DTOFacImpuestosfacturas', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (484, 'con:tasa_cambio', 'DOC', '--Tasa Cambio CONTABLE EXPORTACION

SELECT FIRST 1 Codigo "noov:Nvtas_sour",
               ''COP'' "noov:Nvtas_targ",
               Valor "noov:Nvtas_calc",
               Fecha "noov:Nvtas_fech"
FROM Pz_Fe_Auto_Trm(:Tipo, :Prefijo, :Numero)
WHERE Codigo <> ''COP''', 'S', 20, 'NOOVAC', 'EXPORTACION', 'noov:TasaCambio', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (501, 'con:comprobante', 'DOC', '--Comprobante CONTABLE CONTINGENCIA

WITH V_Trm
AS (SELECT Valor AS Trm
    FROM Pz_Fe_Auto_Trm(:Tipo, :Prefijo, :Numero))

SELECT "noov:Nvfac_orig",
       "noov:Nvemp_nnit",
       "noov:Nvres_nume",
       "noov:Nvfac_tipo",
       "noov:Nvres_pref",
       "noov:Nvfac_nume",
       "noov:Nvfac_fech",
       "noov:Nvfac_cdet",
       "noov:Nvfac_venc",
       "noov:Nvsuc_codi",
       "noov:Nvmon_codi",
       "noov:Nvfor_codi",
       "noov:Nvven_nomb",
       "noov:Nvfac_fpag",
       "noov:Nvfac_conv",
       "noov:Nvcli_cper",
       "noov:Nvcli_cdoc",
       "noov:Nvcli_docu",
       "noov:Nvcli_pais",
       "noov:Nvcli_depa",
       "noov:Nvcli_ciud",
       "noov:Nvcli_loca",
       "noov:Nvcli_dire",
       "noov:Nvcli_ntel",
       "noov:Nvcli_regi",
       "noov:Nvcli_fisc",
       "noov:Nvcli_nomb",
       "noov:Nvcli_pnom",
       "noov:Nvcli_snom",
       "noov:Nvcli_apel",
       "noov:Nvcli_mail",
       "noov:Nvema_copi",
       "noov:Nvfac_obse",
       "noov:Nvfac_orde",
       "noov:Nvfac_remi",
       "noov:Nvfac_rece",
       "noov:Nvfac_entr",
       "noov:Nvfac_ccos",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_stot" / Trm, 2)) "noov:Nvfac_stot",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc" / Trm, 2)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_anti" / Trm, 2)) "noov:Nvfac_anti",
       "noov:Nvfac_carg",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_tota" / Trm, 2)) "noov:Nvfac_tota",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_totp" / Trm, 2)) "noov:Nvfac_totp",
       --Ajuste al redondeo
       --totp
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_totp" / Trm, 2)) -
       --stot
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_stot" / Trm, 2)) -
       --Impuestos
       COALESCE((SELECT Valor
                 FROM Redondeo_Dian((SELECT SUM("noov:Nvimp_valo")
                                     FROM Pz_Fe_Auto_Imp(:Tipo, :Prefijo, :Numero)
                                     WHERE "noov:Nvimp_oper" = ''S'') / Trm, 2)), 0) +
       --descuentos
       COALESCE((SELECT Valor
                 FROM Redondeo_Dian("noov:Nvfac_desc" / Trm, 2)), 0) + COALESCE((SELECT Valor
                                                                                 FROM Redondeo_Dian("noov:Nvfac_anti" / Trm, 2)), 0) "noov:Nvfac_roun",
       "noov:Nvfac_vcop",
       "noov:Nvfac_timp",
       "noov:Nvfac_coid",
       "noov:Nvfac_obsb",
       "noov:Nvcli_ncon",
       '''' "noov:Nvfor_oper"
FROM Pz_Fe_Auto_Comp(:Tipo, :Prefijo, :Numero, :Emisor, :Receptor)
JOIN V_Trm ON (1 = 1)', 'S', 5, 'NOOVAC', 'CONTINGENCIA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (502, 'con:detalle', 'DOC', '--Detalle CONTABLE CONTINGENCIA

WITH V_Trm
AS (SELECT Valor AS Trm
    FROM Pz_Fe_Auto_Trm(:Tipo, :Prefijo, :Numero))

SELECT Consecutivo,
       "noov:Nvpro_codi",
       "noov:Nvpro_nomb",
       "noov:Nvuni_desc",
       "noov:Nvfac_cant",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_valo" / Trm, 2)) "noov:Nvfac_valo",
       "noov:Nvfac_pdes",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc" / Trm, 2)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian(Stot_Red / Trm, 2)) "noov:Nvfac_stot",
       "noov:Nvimp_cdia",
       "noov:Nvdet_piva",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvdet_viva" / Trm, 2)) "noov:Nvdet_viva",
       "noov:Nvdet_tcod",
       "noov:Nvpro_cean",
       "noov:Nvdet_entr",
       "noov:Nvuni_quan",
       "noov:Nvdet_nota",
       "noov:Nvdet_padr",
       "noov:Nvdet_marc",
       "noov:Nvdet_mode"
FROM Pz_Fe_Auto_Det(:Tipo, :Prefijo, :Numero)
JOIN V_Trm ON (1 = 1)', 'S', 10, 'NOOVAC', 'CONTINGENCIA', 'noov:DTOFacDetallefacturas', NULL, 'noov:Nvfac_dcop', NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (503, 'con:impuestos', 'DOC', '--Impuestos CONTABLE CONTINGENCIA

WITH V_Trm
AS (SELECT Valor AS Trm
    FROM Pz_Fe_Auto_Trm(:Tipo, :Prefijo, :Numero))

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_oper",
       "noov:Nvimp_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base" / Trm, 2)) "noov:Nvimp_base",
       "noov:Nvimp_porc" / IIF(Es_Tarifa = ''N'', Trm, 1) "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo" / Trm, 2)) "noov:Nvimp_valo"
FROM Pz_Fe_Auto_Imp(:Tipo, :Prefijo, :Numero)
JOIN V_Trm ON (1 = 1)
WHERE "noov:Nvimp_cdia" NOT IN (''07'', ''99'')', 'S', 15, 'NOOVAC', 'CONTINGENCIA', 'noov:DTOFacImpuestosfacturas', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (504, 'con:mandatario', 'DOC', '-- Mandatario CONTABLE CONTINGENCIA

SELECT
       CASE T.Naturaleza
         WHEN ''J'' THEN 1
         WHEN ''N'' THEN 2
         ELSE ''''
       END "noov:Nvman_cper",
       Codidentidad "noov:Nvman_cdoc",
       TRIM(C.Codtercero) || IIF(TRIM(T.Codidentidad) = ''31'', IIF(T.Dv IS NULL, '''', ''-'' || TRIM(T.Dv)), '''') "noov:Nvman_docu",
       T.Email "noov:Nvman_mail",
       T.Direccion "noov:Nvman_dire",
       S.Codigo_Fe "noov:Nvman_regi",
       COALESCE(T.Empresa, '''') "noov:Nvman_nomb",
       COALESCE(T.Nom1, '''') "noov:Nvman_pnom",
       COALESCE(T.Nom2, '''') "noov:Nvman_snom",
       TRIM(COALESCE(T.Apl1, '''')) || '' '' || TRIM(COALESCE(T.Apl2, '''')) "noov:Nvman_apel",
       COALESCE(T.Telefono, '''') "noov:Nvman_ntel"
FROM Auto_Comp M
JOIN Centros C ON (M.Codcentro = C.Codigo)
LEFT JOIN Terceros T ON (C.Codtercero = T.Codigo)
LEFT JOIN Sociedades S ON (T.Codsociedad = S.Codigo)
WHERE Tipo = :Tipo
      AND Prefijo = :Prefijo
      AND Numero = :Numero
      AND COALESCE(C.Codtercero, '''') <> ''''', 'S', 20, 'NOOVAC', 'CONTINGENCIA', 'noov:Nvdet_mand', NULL, NULL, 'con:detalle')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (521, 'con:comprobante', 'DOC', '-- Comprobante Contable Nota Debito

WITH V_Trm
AS (SELECT Valor AS Trm
    FROM Pz_Fe_Auto_Trm(:Tipo, :Prefijo, :Numero))

SELECT "noov:Nvfac_orig",
       "noov:Nvemp_nnit",
       "noov:Nvfac_tipo",
       "noov:Nvres_pref",
       "noov:Nvfac_nume",
       "noov:Nvfac_fech",
       "noov:Nvfac_cdet",
       "noov:Nvfac_venc",
       "noov:Nvsuc_codi",
       "noov:Nvmon_codi",
       "noov:Nvfor_codi",
       "noov:Nvven_nomb",
       "noov:Nvfac_fpag",
       "noov:Nvfac_conv",
       "noov:Nvcli_cper",
       "noov:Nvcli_cdoc",
       "noov:Nvcli_docu",
       "noov:Nvcli_pais",
       "noov:Nvcli_depa",
       "noov:Nvcli_ciud",
       "noov:Nvcli_loca",
       "noov:Nvcli_dire",
       "noov:Nvcli_ntel",
       "noov:Nvcli_regi",
       "noov:Nvcli_fisc",
       "noov:Nvcli_nomb",
       "noov:Nvcli_pnom",
       "noov:Nvcli_snom",
       "noov:Nvcli_apel",
       "noov:Nvcli_mail",
       "noov:Nvema_copi",
       "noov:Nvfac_obse",
       "noov:Nvfac_orde",
       "noov:Nvfac_remi",
       "noov:Nvfac_rece",
       "noov:Nvfac_entr",
       "noov:Nvfac_ccos",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_stot" / Trm, 2)) "noov:Nvfac_stot",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc" / Trm, 2)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_anti" / Trm, 2)) "noov:Nvfac_anti",
       "noov:Nvfac_carg",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_tota" / Trm, 2)) "noov:Nvfac_tota",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_totp" / Trm, 2)) "noov:Nvfac_totp",
       --Ajuste al redondeo

       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_totp" / Trm, 2)) -
       --stot
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_stot" / Trm, 2)) -
       --Impuestos
       COALESCE((SELECT Valor
                 FROM Redondeo_Dian((SELECT SUM("noov:Nvimp_valo")
                                     FROM pz_fe_auto_imp(:Tipo, :Prefijo, :Numero)
                                     WHERE "noov:Nvimp_oper" = ''S'') / Trm, 2)), 0) +
       --descuentos
       COALESCE((SELECT Valor
                 FROM Redondeo_Dian(("noov:Nvfac_desc" + (SELECT SUM(IIF("noov:Nvfac_pdes" = 100, "noov:Nvfac_desc", 0))
                                                          FROM Pz_Fe_Detalle(:Tipo, :Prefijo, :Numero))) / Trm, 2)), 0) "noov:Nvfac_roun",
       "noov:Nvfac_vcop",
       "noov:Nvfac_timp",
       "noov:Nvfac_coid",
       "noov:Nvfac_numb",
       "noov:Nvfac_fecb",
       "noov:Nvfac_obsb",
       "noov:Nvcli_ncon",
       "noov:Nvfac_tcru",
       "noov:Nvcon_codi",
       "noov:Nvcon_desc"
FROM Pz_Fe_Auto_Comp(:Tipo, :Prefijo, :Numero, :Emisor, :Receptor)
JOIN V_Trm ON (1 = 1)', 'S', 5, 'NOOVAC', 'NOTA DEBITO', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (522, 'con:detalle', 'DOC', '-- Detalle Contable Nota Debito

WITH V_Trm
AS (SELECT Valor AS Trm
    FROM Pz_Fe_Auto_Trm(:Tipo, :Prefijo, :Numero))

SELECT Consecutivo,
       "noov:Nvpro_codi",
       "noov:Nvpro_nomb",
       "noov:Nvuni_desc",
       "noov:Nvfac_cant",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_valo" / Trm, 2)) "noov:Nvfac_valo",
       "noov:Nvfac_pdes",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc" / Trm, 2)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian(Stot_Red / Trm, 2)) "noov:Nvfac_stot",
       "noov:Nvimp_cdia",
       "noov:Nvdet_piva",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvdet_viva" / Trm, 2)) "noov:Nvdet_viva",
       "noov:Nvdet_tcod",
       "noov:Nvpro_cean",
       "noov:Nvdet_entr",
       "noov:Nvuni_quan",
       "noov:Nvdet_nota",
       "noov:Nvdet_padr",
       "noov:Nvdet_marc",
       "noov:Nvdet_mode"
FROM Pz_Fe_Auto_Det(:Tipo, :Prefijo, :Numero)
JOIN V_Trm ON (1 = 1)', 'S', 10, 'NOOVAC', 'NOTA DEBITO', 'noov:DTOFacDetallefacturas', NULL, 'noov:Nvfac_dcop', NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (523, 'con:impuestos', 'DOC', '-- Impuestos Contable Nota Debito

WITH V_Trm
AS (SELECT Valor AS Trm
    FROM Pz_Fe_Auto_Trm(:Tipo, :Prefijo, :Numero))

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_oper",
       "noov:Nvimp_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base" / Trm, 2)) "noov:Nvimp_base",
       "noov:Nvimp_porc" / IIF(Es_Tarifa = ''N'', Trm, 1) "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo" / Trm, 2)) "noov:Nvimp_valo"
FROM Pz_Fe_Auto_Imp(:Tipo, :Prefijo, :Numero)
JOIN V_Trm ON (1 = 1)
WHERE "noov:Nvimp_cdia" NOT IN (''07'', ''99'')', 'S', 15, 'NOOVAC', 'NOTA DEBITO', 'noov:DTOFacImpuestosfacturas', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (524, 'con:tasa_cambio', 'DOC', '-- Tasa Cambio Contable Nota Debito

SELECT FIRST 1 Codigo "noov:Nvtas_sour",
               ''COP'' "noov:Nvtas_targ",
               Valor "noov:Nvtas_calc",
               Fecha "noov:Nvtas_fech"
FROM Pz_Fe_Auto_Trm(:Tipo, :Prefijo, :Numero)
WHERE Codigo <> ''COP''', 'S', 20, 'NOOVAC', 'NOTA DEBITO', 'noov:TasaCambio', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (525, 'con:proveedor', 'DOC', '-- Proveedor Doc Soporte

SELECT "noov:Nvpro_cper",
       "noov:Nvpro_cdoc",
       "noov:Nvpro_docu",
       "noov:Nvpro_dive",
       "noov:Nvpro_pais",
       "noov:Nvpro_depa",
       "noov:Nvpro_ciud",
       "noov:Nvpro_loca",
       "noov:Nvpro_dire",
       "noov:Nvpro_ntel",
       "noov:Nvpro_regi",
       "noov:Nvpro_fisc",
       "noov:Nvpro_nomb",
       "noov:Nvpro_pnom",
       "noov:Nvpro_snom",
       "noov:Nvpro_apel",
       "noov:Nvpro_mail",
       "noov:Nvpro_ncon"
FROM Pz_Fe_Auto_Comp(:Tipo, :Prefijo, :Numero, :Emisor, :Receptor)  
WHERE "noov:Nvpro_docu" <> ''''', 'S', 40, 'NOOVAC', 'NOTA DEBITO', 'noov:Proveedor', NULL, NULL, 'con:comprobante')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (541, 'con:comprobante', 'DOC', '-- Comprobante Contable Nota Credito

WITH V_Trm
AS (SELECT Valor AS Trm
    FROM Pz_Fe_Auto_Trm(:Tipo, :Prefijo, :Numero))

SELECT "noov:Nvfac_orig",
       "noov:Nvemp_nnit",
       "noov:Nvres_nume",
       "noov:Nvfac_tipo",
       "noov:Nvres_pref",
       "noov:Nvfac_nume",
       "noov:Nvfac_fech",
       "noov:Nvfac_cdet",
       "noov:Nvfac_venc",
       "noov:Nvsuc_codi",
       "noov:Nvmon_codi",
       IIF("noov:Nvfor_codi" = ''13'', ''2'', "noov:Nvfor_codi") "noov:Nvfor_codi",
       "noov:Nvven_nomb",
       "noov:Nvfac_fpag",
       "noov:Nvfac_conv",
       "noov:Nvcli_cper",
       "noov:Nvcli_cdoc",
       "noov:Nvcli_docu",
       "noov:Nvcli_pais",
       "noov:Nvcli_depa",
       "noov:Nvcli_ciud",
       "noov:Nvcli_loca",
       "noov:Nvcli_dire",
       "noov:Nvcli_ntel",
       "noov:Nvcli_regi",
       "noov:Nvcli_fisc",
       "noov:Nvcli_nomb",
       "noov:Nvcli_pnom",
       "noov:Nvcli_snom",
       "noov:Nvcli_apel",
       "noov:Nvcli_mail",
       "noov:Nvema_copi",
       "noov:Nvfac_obse",
       "noov:Nvfac_orde",
       "noov:Nvfac_remi",
       "noov:Nvfac_rece",
       "noov:Nvfac_entr",
       "noov:Nvfac_ccos",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_stot" / Trm, 2)) "noov:Nvfac_stot",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc" / Trm, 2)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_anti" / Trm, 2)) "noov:Nvfac_anti",
       "noov:Nvfac_carg",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_tota" / Trm, 2)) "noov:Nvfac_tota",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_totp" / Trm, 2)) "noov:Nvfac_totp",
       --Ajuste al redondeo

       --totp
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_totp" / Trm, 2)) -
       --stot
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_stot" / Trm, 2)) -
       --Impuestos
       COALESCE((SELECT Valor
                 FROM Redondeo_Dian((SELECT SUM("noov:Nvimp_valo")
                                     FROM pz_fe_auto_imp(:Tipo, :Prefijo, :Numero)
                                     WHERE "noov:Nvimp_oper" = ''S'') / Trm, 2)), 0) +
       --descuentos
       COALESCE((SELECT Valor
                 FROM Redondeo_Dian("noov:Nvfac_desc" / Trm, 2)), 0) + COALESCE((SELECT Valor
                                                                                 FROM Redondeo_Dian("noov:Nvfac_anti" / Trm, 2)), 0) "noov:Nvfac_roun",
       "noov:Nvfac_vcop",
       "noov:Nvfac_timp",
       "noov:Nvfac_coid",
       "noov:Nvfac_numb",
       "noov:Nvfac_fecb",
       "noov:Nvfac_obsb",
       "noov:Nvcli_ncon",
       "noov:Nvfac_tcru",
       "noov:Nvcon_codi",
       "noov:Nvcon_desc"
FROM Pz_Fe_Auto_Comp(:Tipo, :Prefijo, :Numero, :Emisor, :Receptor)
JOIN V_Trm ON (1 = 1)', 'S', 5, 'NOOVAC', 'NOTA CREDITO', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (542, 'con:detalle', 'DOC', '-- Detalle Contable Nota Credito

WITH V_Trm
AS (SELECT Valor AS Trm
    FROM Pz_Fe_Auto_Trm(:Tipo, :Prefijo, :Numero))

SELECT Consecutivo,
       "noov:Nvpro_codi",
       "noov:Nvpro_nomb",
       "noov:Nvuni_desc",
       "noov:Nvfac_cant",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_valo" / Trm, 2)) "noov:Nvfac_valo",
       "noov:Nvfac_pdes",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc" / Trm, 2)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian(Stot_Red / Trm, 2)) "noov:Nvfac_stot",
       "noov:Nvimp_cdia",
       "noov:Nvdet_piva",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvdet_viva" / Trm, 2)) "noov:Nvdet_viva",
       "noov:Nvdet_tcod",
       "noov:Nvpro_cean",
       "noov:Nvdet_entr",
       "noov:Nvuni_quan",
       "noov:Nvdet_nota",
       "noov:Nvdet_padr",
       "noov:Nvdet_marc",
       "noov:Nvdet_mode"
FROM Pz_Fe_Auto_Det(:Tipo, :Prefijo, :Numero)
JOIN V_Trm ON (1 = 1)', 'S', 10, 'NOOVAC', 'NOTA CREDITO', 'noov:DTOFacDetallefacturas', NULL, 'noov:Nvfac_dcop', NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (543, 'con:impuestos', 'DOC', '-- Impuestos Contable Nota Credito

WITH V_Trm
AS (SELECT Valor AS Trm
    FROM Pz_Fe_Auto_Trm(:Tipo, :Prefijo, :Numero))

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_oper",
       "noov:Nvimp_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base" / Trm, 2)) "noov:Nvimp_base",
       "noov:Nvimp_porc" / IIF(Es_Tarifa = ''N'', Trm, 1) "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo" / Trm, 2)) "noov:Nvimp_valo"
FROM Pz_Fe_Auto_Imp(:Tipo, :Prefijo, :Numero)
JOIN V_Trm ON (1 = 1)
WHERE "noov:Nvimp_cdia" NOT IN (''07'', ''99'')', 'S', 15, 'NOOVAC', 'NOTA CREDITO', 'noov:DTOFacImpuestosfacturas', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (544, 'con:tasa_cambio', 'DOC', '-- Tasa Cambio Contable Nota Credito

SELECT FIRST 1 Codigo "noov:Nvtas_sour",
               ''COP'' "noov:Nvtas_targ",
               Valor "noov:Nvtas_calc",
               Fecha "noov:Nvtas_fech"
FROM Pz_Fe_Auto_Trm(:Tipo, :Prefijo, :Numero)
WHERE Codigo <> ''COP''', 'S', 20, 'NOOVAC', 'NOTA CREDITO', 'noov:TasaCambio', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (545, 'con:proveedor', 'DOC', '-- Proveedor Doc Soporte

SELECT "noov:Nvpro_cper",
       "noov:Nvpro_cdoc",
       "noov:Nvpro_docu",
       "noov:Nvpro_dive",
       "noov:Nvpro_pais",
       "noov:Nvpro_depa",
       "noov:Nvpro_ciud",
       "noov:Nvpro_loca",
       "noov:Nvpro_dire",
       "noov:Nvpro_ntel",
       "noov:Nvpro_regi",
       "noov:Nvpro_fisc",
       "noov:Nvpro_nomb",
       "noov:Nvpro_pnom",
       "noov:Nvpro_snom",
       "noov:Nvpro_apel",
       "noov:Nvpro_mail",
       "noov:Nvpro_ncon"
FROM Pz_Fe_Auto_Comp(:Tipo, :Prefijo, :Numero, :Emisor, :Receptor)  
WHERE "noov:Nvpro_docu" <> ''''', 'S', 40, 'NOOVAC', 'NOTA CREDITO', 'noov:Proveedor', NULL, NULL, 'con:comprobante')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (561, 'con:comprobante', 'DOC', '-- Comprobante Contable Doc Soporte

WITH V_Trm
AS (SELECT Valor AS Trm
    FROM Pz_Fe_Auto_Trm(:Tipo, :Prefijo, :Numero))

SELECT "noov:Nvfac_orig",
       "noov:Nvemp_nnit",
       "noov:Nvres_nume",
       "noov:Nvfac_tipo",
       "noov:Nvres_pref",
       "noov:Nvfac_nume",
       "noov:Nvfac_fech",
       "noov:Nvfac_cdet",
       "noov:Nvfac_venc",
       "noov:Nvsuc_codi",
       "noov:Nvmon_codi",
       "noov:Nvfor_codi",
       "noov:Nvven_nomb",
       "noov:Nvfac_fpag",
       "noov:Nvfac_conv",
       "noov:Nvema_copi",
       "noov:Nvfac_obse",
       "noov:Nvfac_orde",
       "noov:Nvfac_remi",
       "noov:Nvfac_rece",
       "noov:Nvfac_entr",
       "noov:Nvfac_ccos",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_stot" / Trm, 2)) "noov:Nvfac_stot",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc" / Trm, 2)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_anti" / Trm, 2)) "noov:Nvfac_anti",
       "noov:Nvfac_carg",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_tota" / Trm, 2)) "noov:Nvfac_tota",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_totp" / Trm, 2)) "noov:Nvfac_totp",
       --Ajuste al redondeo
       --totp
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_totp" / Trm, 2)) -
       --stot
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_stot" / Trm, 2)) -
       --Impuestos
       COALESCE((SELECT Valor
                 FROM Redondeo_Dian((SELECT SUM("noov:Nvimp_valo")
                                     FROM pz_fe_auto_imp(:Tipo, :Prefijo, :Numero)
                                     WHERE "noov:Nvimp_oper" = ''S'') / Trm, 2)), 0) +
       --descuentos
       COALESCE((SELECT Valor
                 FROM Redondeo_Dian("noov:Nvfac_desc" / Trm, 2)), 0) + COALESCE((SELECT Valor
                                                                           FROM Redondeo_Dian("noov:Nvfac_anti" / Trm, 2)) , 0) "noov:Nvfac_roun",
       "noov:Nvfac_vcop",
       "noov:Nvfac_timp",
       "noov:Nvfac_coid",
       "noov:Nvfac_obsb",
       '''' "noov:Nvfor_oper"
FROM Pz_Fe_Auto_Comp(:Tipo, :Prefijo, :Numero, :Emisor, :Receptor)
JOIN V_Trm ON (1 = 1)', 'S', 5, 'NOOVAC', 'DOCUMENTO SOPORTE', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (562, 'con:proveedor', 'DOC', '-- Proveedor Doc Soporte

SELECT "noov:Nvpro_cper",
       "noov:Nvpro_cdoc",
       "noov:Nvpro_docu",
       "noov:Nvpro_dive",
       "noov:Nvpro_pais",
       "noov:Nvpro_depa",
       "noov:Nvpro_ciud",
       "noov:Nvpro_loca",
       "noov:Nvpro_dire",
       "noov:Nvpro_ntel",
       "noov:Nvpro_regi",
       "noov:Nvpro_fisc",
       "noov:Nvpro_nomb",
       "noov:Nvpro_pnom",
       "noov:Nvpro_snom",
       "noov:Nvpro_apel",
       "noov:Nvpro_mail",
       "noov:Nvpro_ncon"
FROM Pz_Fe_Auto_Comp(:Tipo, :Prefijo, :Numero, :Emisor, :Receptor)', 'S', 40, 'NOOVAC', 'DOCUMENTO SOPORTE', 'noov:Proveedor', NULL, '', 'con:comprobante')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (563, 'con:detalle', 'DOC', '-- Detalle Contable Doc Soporte

WITH V_Trm
AS (SELECT Valor AS Trm
    FROM Pz_Fe_Auto_Trm(:Tipo, :Prefijo, :Numero))

SELECT Consecutivo,
       "noov:Nvpro_codi",
       "noov:Nvpro_nomb",
       "noov:Nvuni_desc",
       "noov:Nvfac_cant",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_valo" / Trm, 2)) "noov:Nvfac_valo",
       "noov:Nvfac_pdes",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc" / Trm, 2)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian(Stot_Red / Trm, 2)) "noov:Nvfac_stot",
       ''00'' "noov:Nvimp_cdia",
       0 "noov:Nvdet_piva",
       0 "noov:Nvdet_viva",
       "noov:Nvdet_tcod",
       "noov:Nvpro_cean",
       "noov:Nvdet_entr",
       "noov:Nvuni_quan",
       "noov:Nvdet_nota",
       "noov:Nvdet_padr",
       "noov:Nvdet_marc",
       "noov:Nvdet_mode"
FROM Pz_Fe_Auto_Det(:Tipo, :Prefijo, :Numero)
JOIN V_Trm ON (1 = 1)', 'S', 15, 'NOOVAC', 'DOCUMENTO SOPORTE', 'noov:DTOFacDetallefacturas', NULL, 'noov:Nvfac_dcop', NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (564, 'con:impuestos', 'DOC', '-- Impuestos Contable Documento Soporte

WITH V_Trm
AS (SELECT Valor AS Trm
    FROM Pz_Fe_Auto_Trm(:Tipo, :Prefijo, :Numero))

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_oper",
       "noov:Nvimp_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base" / Trm, 2)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo" / Trm, 2)) "noov:Nvimp_valo"
FROM Pz_Fe_Auto_Imp(:Tipo, :Prefijo, :Numero)
JOIN V_Trm ON (1 = 1)
WHERE "noov:Nvimp_oper" = ''R'' And "noov:Nvimp_cdia" NOT IN (''07'', ''99'')', 'S', 25, 'NOOVAC', 'DOCUMENTO SOPORTE', 'noov:DTOFacImpuestosfacturas', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (565, 'con:tasa_cambio', 'DOC', '--Tasa Cambio Contable Documento Soporte

SELECT FIRST 1 Codigo "noov:Nvtas_sour",
               ''COP'' "noov:Nvtas_targ",
               Valor "noov:Nvtas_calc",
               Fecha "noov:Nvtas_fech"
FROM Pz_Fe_Auto_Trm(:Tipo, :Prefijo, :Numero)
WHERE Codigo <> ''COP''', 'S', 30, 'NOOVAC', 'DOCUMENTO SOPORTE', 'noov:TasaCambio', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (581, 'con:comprobante', 'DOC', '-- Comprobante Contable Nota Ajuste a DS

WITH V_Trm
AS (SELECT Valor AS Trm
    FROM Pz_Fe_Auto_Trm(:Tipo, :Prefijo, :Numero))

SELECT "noov:Nvfac_orig",
       "noov:Nvemp_nnit",
       "noov:Nvres_nume",
       "noov:Nvfac_tipo",
       "noov:Nvres_pref",
       "noov:Nvfac_nume",
       "noov:Nvfac_fech",
       "noov:Nvfac_cdet",
       "noov:Nvfac_venc",
       "noov:Nvsuc_codi",
       "noov:Nvmon_codi",
       IIF("noov:Nvfor_codi" = ''13'', ''2'', "noov:Nvfor_codi") "noov:Nvfor_codi",
       "noov:Nvven_nomb",
       "noov:Nvfac_fpag",
       "noov:Nvfac_conv",
       "noov:Nvema_copi",
       "noov:Nvfac_obse",
       "noov:Nvfac_orde",
       "noov:Nvfac_remi",
       "noov:Nvfac_rece",
       "noov:Nvfac_entr",
       "noov:Nvfac_ccos",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_stot"/Trm, 2)) "noov:Nvfac_stot",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc"/Trm, 2)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_anti"/Trm, 2)) "noov:Nvfac_anti",
       "noov:Nvfac_carg",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_tota"/Trm, 2)) "noov:Nvfac_tota",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_totp"/Trm, 2)) "noov:Nvfac_totp",
       --Ajuste al redondeo

       --totp
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_totp"/Trm, 2)) -
       --stot
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_stot"/Trm, 2)) -
       --Impuestos
       COALESCE((SELECT Valor
                 FROM Redondeo_Dian((SELECT SUM("noov:Nvimp_valo")
                                     FROM Pz_Fe_Auto_Imp(:Tipo, :Prefijo, :Numero)
                                     WHERE "noov:Nvimp_oper" = ''S'')/Trm, 2)), 0) +
       --descuentos
       COALESCE((SELECT Valor
                 FROM Redondeo_Dian("noov:Nvfac_desc"/Trm, 2)), 0) + COALESCE((SELECT Valor
                                                                           FROM Redondeo_Dian("noov:Nvfac_anti"/Trm, 2)), 0) "noov:Nvfac_roun",
       "noov:Nvfac_vcop",
       "noov:Nvfac_timp",
       "noov:Nvfac_coid",
       "noov:Nvfac_numb",
       "noov:Nvfac_fecb",
       "noov:Nvfac_obsb",
       "noov:Nvcli_ncon",
       "noov:Nvfac_tcru",
       "noov:Nvcon_codi",
       "noov:Nvcon_desc"
FROM Pz_Fe_Auto_Comp(:Tipo, :Prefijo, :Numero, :Emisor, :Receptor)
JOIN V_Trm ON (1 = 1)', 'S', 5, 'NOOVAC', 'NOTA DE AJUSTE A DS', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (582, 'con:proveedor', 'DOC', '-- Proveedor Nota Ajuste a DS

SELECT "noov:Nvpro_cper",
       "noov:Nvpro_cdoc",
       "noov:Nvpro_docu",
       "noov:Nvpro_dive",
       "noov:Nvpro_pais",
       "noov:Nvpro_depa",
       "noov:Nvpro_ciud",
       "noov:Nvpro_zipc",
       "noov:Nvpro_loca",
       "noov:Nvpro_dire",
       "noov:Nvpro_ntel",
       "noov:Nvpro_regi",
       "noov:Nvpro_fisc",
       "noov:Nvpro_nomb",
       "noov:Nvpro_pnom",
       "noov:Nvpro_snom",
       "noov:Nvpro_apel",
       "noov:Nvpro_mail",
       "noov:Nvpro_ncon"
FROM Pz_Fe_Auto_Comp(:Tipo, :Prefijo, :Numero, :Emisor, :Receptor)', 'S', 10, 'NOOVAC', 'NOTA DE AJUSTE A DS', 'noov:Proveedor', NULL, NULL, 'con:comprobante')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (583, 'con:detalle', 'DOC', '-- Detalle Contable Nota Ajuste a DS

WITH V_Trm
AS (SELECT Valor AS Trm
    FROM Pz_Fe_Auto_Trm(:Tipo, :Prefijo, :Numero))

SELECT Consecutivo,
       "noov:Nvpro_codi",
       "noov:Nvpro_nomb",
       "noov:Nvuni_desc",
       "noov:Nvfac_cant",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_valo"/Trm, 2)) "noov:Nvfac_valo",
       "noov:Nvfac_pdes",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc"/Trm, 2)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian(Stot_Red/Trm, 2)) "noov:Nvfac_stot",
       ''00'' "noov:Nvimp_cdia",
       0 "noov:Nvdet_piva",
       0  "noov:Nvdet_viva",
       "noov:Nvdet_tcod",
       "noov:Nvpro_cean",
       "noov:Nvdet_entr",
       "noov:Nvuni_quan",
       "noov:Nvdet_nota",
       "noov:Nvdet_padr",
       "noov:Nvdet_marc",
       "noov:Nvdet_mode"
FROM Pz_Fe_Auto_Det(:Tipo, :Prefijo, :Numero)
JOIN V_Trm ON (1 = 1)', 'S', 10, 'NOOVAC', 'NOTA DE AJUSTE A DS', 'noov:DTOFacDetallefacturas', NULL, 'noov:Nvfac_dcop', NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (584, 'con:impuestos', 'DOC', '-- Impuestos Contable Nota de Ajuste a DS

WITH V_Trm
AS (SELECT Valor AS Trm
    FROM Pz_Fe_Auto_Trm(:Tipo, :Prefijo, :Numero))

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_oper",
       "noov:Nvimp_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base"/Trm, 2)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo"/Trm, 2)) "noov:Nvimp_valo"
FROM Pz_Fe_Auto_Imp(:Tipo, :Prefijo, :Numero)
JOIN V_Trm ON (1 = 1)
WHERE "noov:Nvimp_oper" = ''R'' And "noov:Nvimp_cdia" NOT IN (''07'', ''99'')', 'S', 25, 'NOOVAC', 'NOTA DE AJUSTE A DS', 'noov:DTOFacImpuestosfacturas', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (585, 'con:tasa_cambio', 'DOC', '--Tasa Cambio Contable Nota de Ajuste a DS

SELECT FIRST 1 Codigo "noov:Nvtas_sour",
               ''COP'' "noov:Nvtas_targ",
               Valor "noov:Nvtas_calc",
               Fecha "noov:Nvtas_fech"
FROM Pz_Fe_Auto_Trm(:Tipo, :Prefijo, :Numero)
WHERE Codigo <> ''COP''', 'S', 20, 'NOOVAC', 'NOTA DE AJUSTE A DS', 'noov:TasaCambio', NULL, NULL, NULL)
                        MATCHING (ID);


COMMIT WORK;


/******************************************************************************/
/***          Generated by IBExpert 2022.3.4.1 06/07/2022 14:26:13          ***/
/******************************************************************************/

/******************************************************************************/
/***      Following SET SQL DIALECT is just for the Database Comparer       ***/
/******************************************************************************/
SET SQL DIALECT 3;



/******************************************************************************/
/***                           Stored procedures                            ***/
/******************************************************************************/



SET TERM ^ ;

CREATE OR ALTER PROCEDURE PZ_FE_AUTO_COMP (
    TIPO_ CHAR(5),
    PREFIJO_ CHAR(5),
    NUMERO_ CHAR(10),
    EMISOR_ VARCHAR(15),
    RECEPTOR_ VARCHAR(15))
RETURNS (
    "noov:Nvfac_orig" CHAR(1),
    "noov:Nvemp_nnit" VARCHAR(15),
    "noov:Nvres_nume" VARCHAR(20),
    "noov:Nvfac_tipo" VARCHAR(5),
    "noov:Nvfac_tcru" CHAR(1),
    "noov:Nvres_pref" VARCHAR(5),
    "noov:Nvfac_nume" VARCHAR(15),
    "noov:Nvfac_fech" VARCHAR(20),
    "noov:Nvfac_cdet" INTEGER,
    "noov:Nvfac_venc" VARCHAR(20),
    "noov:Nvsuc_codi" CHAR(1),
    "noov:Nvmon_codi" VARCHAR(5),
    "noov:Nvfor_codi" VARCHAR(2),
    "noov:Nvven_nomb" VARCHAR(84),
    "noov:Nvfac_fpag" VARCHAR(5),
    "noov:Nvfac_conv" CHAR(1),
    "noov:Nvcli_cper" CHAR(1),
    "noov:Nvcli_cdoc" VARCHAR(5),
    "noov:Nvcli_docu" VARCHAR(15),
    "noov:Nvcli_pais" VARCHAR(5),
    "noov:Nvcli_depa" VARCHAR(80),
    "noov:Nvcli_ciud" VARCHAR(100),
    "noov:Nvcli_loca" VARCHAR(10),
    "noov:Nvcli_dire" VARCHAR(80),
    "noov:Nvcli_ntel" VARCHAR(200),
    "noov:Nvcli_regi" VARCHAR(5),
    "noov:Nvcli_fisc" VARCHAR(30),
    "noov:Nvcli_nomb" VARCHAR(80),
    "noov:Nvcli_pnom" VARCHAR(20),
    "noov:Nvcli_snom" VARCHAR(20),
    "noov:Nvcli_apel" VARCHAR(40),
    "noov:Nvcli_mail" VARCHAR(300),
    "noov:Nvema_copi" VARCHAR(50),
    "noov:Nvfac_obse" VARCHAR(4000),
    "noov:Nvfac_orde" VARCHAR(40),
    "noov:Nvfac_remi" VARCHAR(40),
    "noov:Nvfac_rece" VARCHAR(40),
    "noov:Nvfac_entr" VARCHAR(40),
    "noov:Nvfac_ccos" VARCHAR(40),
    "noov:Nvfac_stot" DOUBLE PRECISION,
    "noov:Nvfac_desc" DOUBLE PRECISION,
    "noov:Nvfac_anti" DOUBLE PRECISION,
    "noov:Nvfac_carg" DOUBLE PRECISION,
    "noov:Nvfac_tota" DOUBLE PRECISION,
    "noov:Nvfac_totp" DOUBLE PRECISION,
    "noov:Nvfac_roun" NUMERIC(17,4),
    "noov:Nvfac_vcop" DOUBLE PRECISION,
    "noov:Nvfac_timp" DOUBLE PRECISION,
    "noov:Nvfac_coid" CHAR(1),
    "noov:Nvfac_obsb" CHAR(1),
    "noov:Nvcli_ncon" VARCHAR(80),
    "noov:Nvcon_codi" VARCHAR(2),
    "noov:Nvcon_desc" VARCHAR(200),
    "noov:Nvfac_numb" VARCHAR(20),
    "noov:Nvfac_fecb" VARCHAR(20),
    "noov:Nvfor_oper" VARCHAR(20),
    "noov:Nvpro_cper" CHAR(1),
    "noov:Nvpro_cdoc" VARCHAR(5),
    "noov:Nvpro_dive" VARCHAR(1),
    "noov:Nvpro_docu" VARCHAR(15),
    "noov:Nvpro_pais" VARCHAR(5),
    "noov:Nvpro_depa" VARCHAR(80),
    "noov:Nvpro_ciud" VARCHAR(100),
    "noov:Nvpro_loca" VARCHAR(10),
    "noov:Nvpro_dire" VARCHAR(80),
    "noov:Nvpro_ntel" VARCHAR(200),
    "noov:Nvpro_regi" VARCHAR(5),
    "noov:Nvpro_fisc" VARCHAR(30),
    "noov:Nvpro_nomb" VARCHAR(80),
    "noov:Nvpro_pnom" VARCHAR(20),
    "noov:Nvpro_snom" VARCHAR(20),
    "noov:Nvpro_apel" VARCHAR(40),
    "noov:Nvpro_mail" VARCHAR(300),
    "noov:Nvpro_ncon" VARCHAR(80),
    V_DOCU_REF VARCHAR(20))
AS
BEGIN
  SUSPEND;
END^





CREATE OR ALTER PROCEDURE PZ_FE_AUTO_DET (
    TIPO_ CHAR(5),
    PREFIJO_ CHAR(5),
    NUMERO_ CHAR(10))
RETURNS (
    CONSECUTIVO INTEGER,
    "noov:Nvpro_codi" CHAR(20),
    "noov:Nvpro_nomb" CHAR(198),
    "noov:Nvuni_desc" CHAR(5),
    "noov:Nvfac_cant" DOUBLE PRECISION,
    "noov:Nvfac_valo" DOUBLE PRECISION,
    "noov:Nvimp_cdia" CHAR(5),
    "noov:Nvfac_pdes" DOUBLE PRECISION,
    "noov:Nvfac_desc" DOUBLE PRECISION,
    "noov:Nvfac_stot" DOUBLE PRECISION,
    "noov:Nvdet_piva" DOUBLE PRECISION,
    "noov:Nvdet_viva" DOUBLE PRECISION,
    "noov:Nvdet_tcod" CHAR(1),
    "noov:Nvpro_cean" CHAR(1),
    "noov:Nvdet_entr" CHAR(1),
    "noov:Nvuni_quan" DOUBLE PRECISION,
    "noov:Nvdet_nota" CHAR(200),
    "noov:Nvdet_padr" INTEGER,
    "noov:Nvdet_marc" CHAR(20),
    "noov:Nvdet_mode" CHAR(20),
    STOT_RED DOUBLE PRECISION)
AS
BEGIN
  SUSPEND;
END^





CREATE OR ALTER PROCEDURE PZ_FE_AUTO_IMP (
    TIPO_ CHAR(5),
    PREFIJO_ CHAR(5),
    NUMERO_ CHAR(10))
RETURNS (
    "noov:Nvimp_cdia" CHAR(5),
    "noov:Nvimp_desc" VARCHAR(40),
    "noov:Nvimp_oper" CHAR(1),
    "noov:Nvimp_base" DOUBLE PRECISION,
    "noov:Nvimp_porc" DOUBLE PRECISION,
    "noov:Nvimp_valo" DOUBLE PRECISION,
    ES_TARIFA CHAR(1),
    CLASE VARCHAR(20))
AS
BEGIN
  SUSPEND;
END^





CREATE OR ALTER PROCEDURE PZ_FE_AUTO_IMP_BASE (
    TIPO_ CHAR(5),
    PREFIJO_ CHAR(5),
    NUMERO_ CHAR(10))
RETURNS (
    "noov:Nvimp_cdia" CHAR(5),
    "noov:Nvimp_desc" VARCHAR(40),
    "noov:Nvimp_oper" CHAR(1),
    "noov:Nvimp_base" DOUBLE PRECISION,
    "noov:Nvimp_porc" DOUBLE PRECISION,
    "noov:Nvimp_valo" DOUBLE PRECISION,
    ES_TARIFA CHAR(1),
    CLASE VARCHAR(20),
    REFERENCIA VARCHAR(20))
AS
BEGIN
  SUSPEND;
END^





CREATE OR ALTER PROCEDURE PZ_FE_AUTO_TRM (
    TIPO_ CHAR(5),
    PREFIJO_ CHAR(5),
    NUMERO_ CHAR(10))
RETURNS (
    CODIGO CHAR(5),
    VALOR DOUBLE PRECISION,
    FECHA CHAR(10))
AS
BEGIN
  SUSPEND;
END^






SET TERM ; ^



/******************************************************************************/
/***                           Stored procedures                            ***/
/******************************************************************************/



SET TERM ^ ;

CREATE OR ALTER PROCEDURE PZ_FE_AUTO_COMP (
    TIPO_ CHAR(5),
    PREFIJO_ CHAR(5),
    NUMERO_ CHAR(10),
    EMISOR_ VARCHAR(15),
    RECEPTOR_ VARCHAR(15))
RETURNS (
    "noov:Nvfac_orig" CHAR(1),
    "noov:Nvemp_nnit" VARCHAR(15),
    "noov:Nvres_nume" VARCHAR(20),
    "noov:Nvfac_tipo" VARCHAR(5),
    "noov:Nvfac_tcru" CHAR(1),
    "noov:Nvres_pref" VARCHAR(5),
    "noov:Nvfac_nume" VARCHAR(10),
    "noov:Nvfac_fech" VARCHAR(20),
    "noov:Nvfac_cdet" INTEGER,
    "noov:Nvfac_venc" VARCHAR(20),
    "noov:Nvsuc_codi" CHAR(1),
    "noov:Nvmon_codi" VARCHAR(5),
    "noov:Nvfor_codi" VARCHAR(2),
    "noov:Nvven_nomb" VARCHAR(84),
    "noov:Nvfac_fpag" VARCHAR(5),
    "noov:Nvfac_conv" CHAR(1),
    "noov:Nvcli_cper" CHAR(1),
    "noov:Nvcli_cdoc" VARCHAR(5),
    "noov:Nvcli_docu" VARCHAR(15),
    "noov:Nvcli_pais" VARCHAR(5),
    "noov:Nvcli_depa" VARCHAR(80),
    "noov:Nvcli_ciud" VARCHAR(100),
    "noov:Nvcli_loca" VARCHAR(10),
    "noov:Nvcli_dire" VARCHAR(80),
    "noov:Nvcli_ntel" VARCHAR(200),
    "noov:Nvcli_regi" VARCHAR(5),
    "noov:Nvcli_fisc" VARCHAR(30),
    "noov:Nvcli_nomb" VARCHAR(80),
    "noov:Nvcli_pnom" VARCHAR(20),
    "noov:Nvcli_snom" VARCHAR(20),
    "noov:Nvcli_apel" VARCHAR(40),
    "noov:Nvcli_mail" VARCHAR(300),
    "noov:Nvema_copi" VARCHAR(50),
    "noov:Nvfac_obse" VARCHAR(4000),
    "noov:Nvfac_orde" VARCHAR(40),
    "noov:Nvfac_remi" VARCHAR(40),
    "noov:Nvfac_rece" VARCHAR(40),
    "noov:Nvfac_entr" VARCHAR(40),
    "noov:Nvfac_ccos" VARCHAR(40),
    "noov:Nvfac_stot" DOUBLE PRECISION,
    "noov:Nvfac_desc" DOUBLE PRECISION,
    "noov:Nvfac_anti" DOUBLE PRECISION,
    "noov:Nvfac_carg" DOUBLE PRECISION,
    "noov:Nvfac_tota" DOUBLE PRECISION,
    "noov:Nvfac_totp" DOUBLE PRECISION,
    "noov:Nvfac_roun" NUMERIC(17,4),
    "noov:Nvfac_vcop" DOUBLE PRECISION,
    "noov:Nvfac_timp" DOUBLE PRECISION,
    "noov:Nvfac_coid" CHAR(1),
    "noov:Nvfac_obsb" CHAR(1),
    "noov:Nvcli_ncon" VARCHAR(80),
    "noov:Nvcon_codi" VARCHAR(2),
    "noov:Nvcon_desc" VARCHAR(200),
    "noov:Nvfac_numb" VARCHAR(20),
    "noov:Nvfac_fecb" VARCHAR(20),
    "noov:Nvfor_oper" VARCHAR(20),
    "noov:Nvpro_cper" CHAR(1),
    "noov:Nvpro_cdoc" VARCHAR(5),
    "noov:Nvpro_dive" VARCHAR(1),
    "noov:Nvpro_docu" VARCHAR(15),
    "noov:Nvpro_pais" VARCHAR(5),
    "noov:Nvpro_depa" VARCHAR(80),
    "noov:Nvpro_ciud" VARCHAR(100),
    "noov:Nvpro_zipc" VARCHAR(6),
    "noov:Nvpro_loca" VARCHAR(10),
    "noov:Nvpro_dire" VARCHAR(80),
    "noov:Nvpro_ntel" VARCHAR(200),
    "noov:Nvpro_regi" VARCHAR(5),
    "noov:Nvpro_fisc" VARCHAR(30),
    "noov:Nvpro_nomb" VARCHAR(80),
    "noov:Nvpro_pnom" VARCHAR(20),
    "noov:Nvpro_snom" VARCHAR(20),
    "noov:Nvpro_apel" VARCHAR(40),
    "noov:Nvpro_mail" VARCHAR(300),
    "noov:Nvpro_ncon" VARCHAR(80),
    V_DOCU_REF VARCHAR(20))
AS
BEGIN
  SUSPEND;
END^



SET TERM ; ^



/******************************************************************************/
/***                           Stored procedures                            ***/
/******************************************************************************/



SET TERM ^ ;

CREATE OR ALTER PROCEDURE PZ_FE_AUTO_COMP (
    TIPO_ CHAR(5),
    PREFIJO_ CHAR(5),
    NUMERO_ CHAR(10),
    EMISOR_ VARCHAR(15),
    RECEPTOR_ VARCHAR(15))
RETURNS (
    "noov:Nvfac_orig" CHAR(1),
    "noov:Nvemp_nnit" VARCHAR(15),
    "noov:Nvres_nume" VARCHAR(20),
    "noov:Nvfac_tipo" VARCHAR(5),
    "noov:Nvfac_tcru" CHAR(1),
    "noov:Nvres_pref" VARCHAR(5),
    "noov:Nvfac_nume" VARCHAR(10),
    "noov:Nvfac_fech" VARCHAR(20),
    "noov:Nvfac_cdet" INTEGER,
    "noov:Nvfac_venc" VARCHAR(20),
    "noov:Nvsuc_codi" CHAR(1),
    "noov:Nvmon_codi" VARCHAR(5),
    "noov:Nvfor_codi" VARCHAR(2),
    "noov:Nvven_nomb" VARCHAR(84),
    "noov:Nvfac_fpag" VARCHAR(5),
    "noov:Nvfac_conv" CHAR(1),
    "noov:Nvcli_cper" CHAR(1),
    "noov:Nvcli_cdoc" VARCHAR(5),
    "noov:Nvcli_docu" VARCHAR(15),
    "noov:Nvcli_pais" VARCHAR(5),
    "noov:Nvcli_depa" VARCHAR(80),
    "noov:Nvcli_ciud" VARCHAR(100),
    "noov:Nvcli_loca" VARCHAR(10),
    "noov:Nvcli_dire" VARCHAR(80),
    "noov:Nvcli_ntel" VARCHAR(200),
    "noov:Nvcli_regi" VARCHAR(5),
    "noov:Nvcli_fisc" VARCHAR(30),
    "noov:Nvcli_nomb" VARCHAR(80),
    "noov:Nvcli_pnom" VARCHAR(20),
    "noov:Nvcli_snom" VARCHAR(20),
    "noov:Nvcli_apel" VARCHAR(40),
    "noov:Nvcli_mail" VARCHAR(300),
    "noov:Nvema_copi" VARCHAR(50),
    "noov:Nvfac_obse" VARCHAR(4000),
    "noov:Nvfac_orde" VARCHAR(40),
    "noov:Nvfac_remi" VARCHAR(40),
    "noov:Nvfac_rece" VARCHAR(40),
    "noov:Nvfac_entr" VARCHAR(40),
    "noov:Nvfac_ccos" VARCHAR(40),
    "noov:Nvfac_stot" DOUBLE PRECISION,
    "noov:Nvfac_desc" DOUBLE PRECISION,
    "noov:Nvfac_anti" DOUBLE PRECISION,
    "noov:Nvfac_carg" DOUBLE PRECISION,
    "noov:Nvfac_tota" DOUBLE PRECISION,
    "noov:Nvfac_totp" DOUBLE PRECISION,
    "noov:Nvfac_vcop" DOUBLE PRECISION,
    "noov:Nvfac_timp" DOUBLE PRECISION,
    "noov:Nvfac_coid" CHAR(1),
    "noov:Nvfac_obsb" CHAR(1),
    "noov:Nvcli_ncon" VARCHAR(80),
    "noov:Nvcon_codi" VARCHAR(2),
    "noov:Nvcon_desc" VARCHAR(200),
    "noov:Nvfac_numb" VARCHAR(20),
    "noov:Nvfac_fecb" VARCHAR(20),
    "noov:Nvpro_cper" CHAR(1),
    "noov:Nvpro_cdoc" VARCHAR(5),
    "noov:Nvpro_dive" VARCHAR(1),
    "noov:Nvpro_docu" VARCHAR(15),
    "noov:Nvpro_pais" VARCHAR(5),
    "noov:Nvpro_depa" VARCHAR(80),
    "noov:Nvpro_ciud" VARCHAR(100),
    "noov:Nvpro_zipc" VARCHAR(6),
    "noov:Nvpro_loca" VARCHAR(10),
    "noov:Nvpro_dire" VARCHAR(80),
    "noov:Nvpro_ntel" VARCHAR(200),
    "noov:Nvpro_regi" VARCHAR(5),
    "noov:Nvpro_fisc" VARCHAR(30),
    "noov:Nvpro_nomb" VARCHAR(80),
    "noov:Nvpro_pnom" VARCHAR(20),
    "noov:Nvpro_snom" VARCHAR(20),
    "noov:Nvpro_apel" VARCHAR(40),
    "noov:Nvpro_mail" VARCHAR(300),
    "noov:Nvpro_ncon" VARCHAR(80))
AS
DECLARE VARIABLE V_Id              INTEGER;
DECLARE VARIABLE V_Plazo           INTEGER;
DECLARE VARIABLE V_Codidentidad    CHAR(5);
DECLARE VARIABLE V_Fecha           CHAR(19);
DECLARE VARIABLE V_Vence           CHAR(19);
DECLARE VARIABLE V_Aiu             INTEGER;
DECLARE VARIABLE V_Manda           INTEGER;
DECLARE VARIABLE V_Naturaleza      CHAR(1);
DECLARE VARIABLE V_Dv              CHAR(1);
DECLARE VARIABLE V_Pais            VARCHAR(5);
DECLARE VARIABLE V_Pais_Fe         VARCHAR(5);
DECLARE VARIABLE V_Muni            VARCHAR(5);
DECLARE VARIABLE V_Nombre_Depa     VARCHAR(80);
DECLARE VARIABLE V_Nombre_Ciud     VARCHAR(80);
DECLARE VARIABLE V_Docu            VARCHAR(20);
DECLARE VARIABLE V_Direccion       VARCHAR(80);
DECLARE VARIABLE V_Telefono        VARCHAR(100);
DECLARE VARIABLE V_Contc           VARCHAR(80);
DECLARE VARIABLE V_Sociedad        VARCHAR(5);
DECLARE VARIABLE V_Sociedad_Fe     VARCHAR(5);
DECLARE VARIABLE V_Sociedad_Nom    VARCHAR(80);
DECLARE VARIABLE V_Autoretenedor   CHAR(1);
DECLARE VARIABLE V_Jerarquia       INTEGER;
DECLARE VARIABLE V_Empresa         VARCHAR(80);
DECLARE VARIABLE V_Nom1            VARCHAR(20);
DECLARE VARIABLE V_Nom2            VARCHAR(20);
DECLARE VARIABLE V_Apl             VARCHAR(40);
DECLARE VARIABLE V_Email           VARCHAR(50);
DECLARE VARIABLE V_Otro_Email      VARCHAR(200);
DECLARE VARIABLE V_Dscor           VARCHAR(50);
DECLARE VARIABLE V_Nota            VARCHAR(80);
DECLARE VARIABLE V_Meganota        VARCHAR(4000);
DECLARE VARIABLE V_Orden           VARCHAR(40);
DECLARE VARIABLE V_Remision        VARCHAR(40);
DECLARE VARIABLE V_Recepcion       VARCHAR(40);
DECLARE VARIABLE V_Ean             VARCHAR(40);
DECLARE VARIABLE V_Centro          VARCHAR(40);
DECLARE VARIABLE V_Impuestos       DOUBLE PRECISION;
DECLARE VARIABLE V_Tiporef         VARCHAR(5);
DECLARE VARIABLE V_Prefijoref      VARCHAR(5);
DECLARE VARIABLE V_Fecha_Ref       CHAR(19);
DECLARE VARIABLE V_Numeroref       VARCHAR(10);
DECLARE VARIABLE V_Docuref         VARCHAR(20);
DECLARE VARIABLE V_Trm_Codigo      VARCHAR(5);
DECLARE VARIABLE V_Valida_Fe       CHAR(1);
DECLARE VARIABLE V_Concepto_Nc     CHAR(1);
DECLARE VARIABLE V_Nom_Concepto_Nc VARCHAR(200);
DECLARE VARIABLE V_Postal_Code     VARCHAR(6);
BEGIN
  --Trm
  SELECT Codigo
  FROM Pz_Fe_Auto_Trm(:Tipo_, :Prefijo_, :Numero_)
  INTO V_Trm_Codigo;

  SELECT Vence
  FROM Val_Documentos
  WHERE Coddocumento = :Tipo_
        AND Codprefijo = :Prefijo_
  INTO V_Plazo;

  SELECT Id,
         EXTRACT(YEAR FROM Fecha) || '-' || RIGHT(EXTRACT(MONTH FROM Fecha) + 100, 2) || '-' || RIGHT(EXTRACT(DAY FROM Fecha) + 100, 2) || 'T00:00:00',
         EXTRACT(YEAR FROM Fecha + COALESCE(:V_Plazo, 0)) || '-' || RIGHT(EXTRACT(MONTH FROM Fecha + COALESCE(:V_Plazo, 0)) + 100, 2) || '-' || RIGHT(EXTRACT(DAY FROM Fecha + COALESCE(:V_Plazo, 0)) + 100, 2) || 'T' || SUBSTRING(Hora FROM 1 FOR 8)
  FROM Fe_Apo_Comprobante(:Tipo_, :Prefijo_, :Numero_)
  INTO V_Id,
       V_Fecha,
       V_Vence;

  --Terceros
  SELECT Codidentidad,
         Naturaleza,
         Dv,
         Codpais,
         Codmunicipio,
         Direccion,
         COALESCE(TRIM(Telefono), '') || ', ' || COALESCE(Movil, ''),
         Codsociedad,
         Empresa,
         Nom1,
         Nom2,
         COALESCE(TRIM(Apl1), '') || ' ' || COALESCE(TRIM(Apl2), ''),
         Email,
         COALESCE(Codigo_Postal, SUBSTRING(Codmunicipio FROM 1 FOR 2) || '0001')
  FROM Terceros
  WHERE Codigo = :Receptor_
  INTO V_Codidentidad,
       V_Naturaleza,
       V_Dv,
       V_Pais,
       V_Muni,
       V_Direccion,
       V_Telefono,
       V_Sociedad,
       V_Empresa,
       V_Nom1,
       V_Nom2,
       V_Apl,
       V_Email,
       V_Postal_Code;

  --Pais
  SELECT Codigo_Fe
  FROM Paises
  WHERE Codigo = :V_Pais
  INTO V_Pais_Fe;

  --Departamento
  SELECT Nombre
  FROM Municipios
  WHERE Codigo = SUBSTRING(:V_Muni FROM 1 FOR 2)
  INTO V_Nombre_Depa;

  --Municipio
  SELECT Nombre
  FROM Municipios
  WHERE Codigo = :V_Muni
  INTO V_Nombre_Ciud;

  --AIU
  SELECT COUNT(*)
  FROM Pz_Fe_Auto_Det(:Tipo_, :Prefijo_, :Numero_) P
  WHERE (TRIM("noov:Nvpro_codi") STARTING WITH 'AIU')
        AND (P."noov:Nvfac_cant" * P."noov:Nvfac_valo") > 0
  INTO V_Aiu;

  --Mandatario
  SELECT COUNT(*)
  FROM Auto_Comp M
  JOIN Centros C ON (M.Codcentro = C.Codigo)
  WHERE Tipo = :Tipo_
        AND Prefijo = :Prefijo_
        AND Numero = :Numero_
        AND COALESCE(C.Codtercero, '') <> ''
  INTO V_Manda;

  --Documentos
  SELECT Codigo_Fe,
         Valida_Fe
  FROM Documentos
  WHERE Codigo = :Tipo_
  INTO V_Docu,
       V_Valida_Fe;

  --Documento ref
  SELECT Tiporef,
         Prefijoref,
         Numeroref
  FROM Notas_Contable
  WHERE Tipo = :Tipo_
        AND Prefijo = :Prefijo_
        AND Numero = :Numero_
  INTO V_Tiporef,
       V_Prefijoref,
       V_Numeroref;

  SELECT Codigo_Fe
  FROM Documentos
  WHERE Codigo = :V_Tiporef
  INTO V_Docuref;

  SELECT EXTRACT(YEAR FROM Fecha) || '-' || RIGHT(EXTRACT(MONTH FROM Fecha) + 100, 2) || '-' || RIGHT(EXTRACT(DAY FROM Fecha) + 100, 2) || 'T00:00:00'
  FROM Auto_Comp
  WHERE Tipo = :V_Tiporef
        AND Prefijo = :V_Prefijoref
        AND Numero = :V_Numeroref
  INTO V_Fecha_Ref;

  --Contacto
  SELECT FIRST 1 Nombre
  FROM Contactos
  WHERE Codtercero = :Receptor_
        AND Codcargo = 'CONTC'
  INTO V_Contc;
  V_Contc = COALESCE(V_Contc, '');

  --Otro email
  SELECT LIST(TRIM(Email), ';')
  FROM Contactos
  WHERE Codtercero = :Receptor_
        AND Codcargo = 'ZZZ'
  INTO V_Otro_Email;
  V_Contc = COALESCE(V_Contc, '');

  --Contacto DSCOR correo Documentos Soporte
  SELECT FIRST 1 Email
  FROM Contactos
  WHERE Codtercero = :Emisor_
        AND Codcargo = 'DSCOR'
  INTO V_Dscor;
  V_Dscor = COALESCE(V_Dscor, 'documentos.soporte.mekano@gmail.com');

  --Sociedades
  SELECT Codigo_Fe,
         Nombre,
         Autoretenedor,
         Jerarquia
  FROM Sociedades
  WHERE Codigo = :V_Sociedad
  INTO V_Sociedad_Fe,
       V_Sociedad_Nom,
       V_Autoretenedor,
       V_Jerarquia;

  --Auto Comp
  SELECT Nota,
         Concepto
  FROM Auto_Comp
  WHERE Tipo = :Tipo_
        AND Prefijo = :Prefijo_
        AND Numero = :Numero_
  INTO V_Nota,
       V_Concepto_Nc;

  --Mega nota
  SELECT LIST(TRIM(C.Nota), IIF(RIGHT(TRIM(C.Nota), 1) = '.', ASCII_CHAR(13), ' '))
  FROM Auto_Movimiento C
  JOIN Auto_Comp C1 USING (Id)
  WHERE (C1.Tipo = :Tipo_
        AND C1.Prefijo = :Prefijo_
        AND C1.Numero = :Numero_)
        AND (C.Codconcepto = 'MEGANOTA')
        AND (TRIM(COALESCE(C.Nota, '')) <> '')
  INTO V_Meganota;

  --Anexos
  SELECT Orden,
         Remision,
         Recepcion,
         Ean,
         Centro
  FROM Anexo_Contabilidad
  WHERE Id = :V_Id
  INTO V_Orden,
       V_Remision,
       V_Recepcion,
       V_Ean,
       V_Centro;

  --Impuestos
  SELECT SUM((SELECT Valor
              FROM Redondeo_Dian("noov:Nvimp_valo", 2)))
  FROM Pz_Fe_Auto_Imp(:Tipo_, :Prefijo_, :Numero_)
  WHERE "noov:Nvimp_oper" = 'S'
  INTO V_Impuestos;
  V_Impuestos = COALESCE(V_Impuestos, 0);

  -------
  "noov:Nvfac_orig" = 'E';
  "noov:Nvemp_nnit" = :Emisor_;
  "noov:Nvres_nume" = (SELECT Numero
                       FROM Lee_Resolucion(:Tipo_, :Prefijo_, :Numero_));
  "noov:Nvres_nume" = COALESCE("noov:Nvres_nume", '0');
  "noov:Nvres_pref" = TRIM(Prefijo_);
  IF (TRIM(Prefijo_) = '_') THEN
    "noov:Nvres_pref" = '';
  "noov:Nvfac_nume" = TRIM("noov:Nvres_pref") || TRIM(Numero_);
  "noov:Nvfac_fech" = V_Fecha;
  "noov:Nvfac_venc" = V_Vence;
  "noov:Nvfac_tipo" = CASE TRIM(V_Docu)
                        WHEN 'FACTURA' THEN 'FV'
                        WHEN 'CONTINGENCIA' THEN 'FC'
                        WHEN 'EXPORTACION' THEN 'FE'
                        WHEN 'NOTA CREDITO' THEN CASE TRIM(V_Docuref)
                                                   WHEN 'EXPORTACION' THEN 'CE'
                                                   WHEN 'CONTINGENCIA' THEN 'CC'
                                                   ELSE 'CV'
                                                 END
                        WHEN 'NOTA DEBITO' THEN CASE TRIM(V_Docuref)
                                                  WHEN 'EXPORTACION' THEN 'DE'
                                                  WHEN 'CONTINGENCIA' THEN 'DC'
                                                  ELSE 'DV'
                                                END
                        WHEN 'DOCUMENTO SOPORTE' THEN 'DS'
                        WHEN 'NOTA DE AJUSTE A DS' THEN 'CS'
                      END;
  "noov:Nvfac_cdet" = (SELECT COUNT(1)
                       FROM Pz_Fe_Auto_Det(:Tipo_, :Prefijo_, :Numero_)
                       WHERE ("noov:Nvfac_cant" * "noov:Nvfac_valo") > 0);
  "noov:Nvsuc_codi" = '1';
  "noov:Nvmon_codi" = V_Trm_Codigo;
  "noov:Nvfor_codi" = CASE TRIM(V_Docu)
                        WHEN 'FACTURA' THEN CASE
                                              WHEN V_Aiu <> 0 THEN '11'
                                              WHEN V_Manda <> 0 THEN '12'
                                              ELSE '1'
                                            END
                        WHEN 'CONTINGENCIA' THEN CASE
                                                   WHEN V_Aiu <> 0 THEN '15'
                                                   WHEN V_Manda <> 0 THEN '16'
                                                   ELSE '7'
                                                 END
                        WHEN 'EXPORTACION' THEN IIF("noov:Nvcli_pais" = 'CO', '10', '4')
                        WHEN 'NOTA CREDITO' THEN CASE TRIM(V_Docuref)
                                                   WHEN 'EXPORTACION' THEN '5'
                                                   WHEN 'FACTURA' THEN IIF(V_Aiu > 0, '13', '2')
                                                   WHEN 'CONTINGENCIA' THEN IIF(V_Aiu > 0, '14', '8')
                                                   ELSE '2'
                                                 END
                        WHEN 'NOTA DEBITO' THEN CASE TRIM(V_Docuref)
                                                  WHEN 'EXPORTACION' THEN '6'
                                                  WHEN 'FACTURA' THEN '3'
                                                  ELSE '3'
                                                END
                        WHEN 'DOCUMENTO SOPORTE' THEN '20'
                        WHEN 'NOTA DE AJUSTE A DS' THEN '21'
                      END;
  "noov:Nvven_nomb" = '';
  "noov:Nvfac_fpag" = 'ZZZ';
  "noov:Nvfac_conv" = IIF(COALESCE(V_Plazo, 0) <> 0, '2', '1');
  "noov:Nvcli_cper" = IIF(V_Naturaleza = 'N', 2, 1);
  "noov:Nvcli_cdoc" = V_Codidentidad;
  "noov:Nvcli_docu" = TRIM(Receptor_) || IIF(TRIM(V_Codidentidad) = '31', IIF(V_Dv IS NULL, '', '-' || TRIM(V_Dv)), '');
  "noov:Nvcli_pais" = V_Pais_Fe;
  "noov:Nvcli_depa" = V_Nombre_Depa;
  "noov:Nvcli_ciud" = TRIM(V_Nombre_Ciud) || IIF(V_Docu = 'EXPORTACION', '', '@' || TRIM(V_Muni));
  "noov:Nvcli_loca" = '';
  "noov:Nvcli_dire" = V_Direccion;
  "noov:Nvcli_ntel" = V_Telefono;
  "noov:Nvcli_regi" = V_Sociedad_Fe;
  "noov:Nvcli_fisc" = IIF(UPPER(TRIM(V_Sociedad_Nom)) LIKE '%TRIBUTACION SIMPLE%', 'O-47', IIF(V_Autoretenedor = 'S', 'O-15' || IIF(V_Jerarquia > 5, ';O-13', ''), IIF(V_Jerarquia > 5, 'O-13', 'R-99-PN')));
  "noov:Nvcli_nomb" = COALESCE(TRIM(V_Empresa), '');
  "noov:Nvcli_pnom" = COALESCE(TRIM(V_Nom1), '');
  "noov:Nvcli_snom" = COALESCE(TRIM(V_Nom2), '');
  "noov:Nvcli_apel" = V_Apl;
  "noov:Nvcli_mail" = TRIM(V_Email) || IIF(V_Otro_Email IS NULL OR TRIM(V_Otro_Email) = '', '', ';' || TRIM(V_Otro_Email));
  "noov:Nvema_copi" = '';
  "noov:Nvfac_obse" = SUBSTRING(TRIM(V_Nota) || IIF(TRIM(COALESCE(V_Meganota, '')) = '', '', ', ' || TRIM(V_Meganota)) FROM 1 FOR 4000);
  "noov:Nvfac_orde" = COALESCE(V_Orden, '');
  "noov:Nvfac_remi" = COALESCE(V_Remision, '');
  "noov:Nvfac_rece" = COALESCE(V_Recepcion, '');
  "noov:Nvfac_entr" = COALESCE(V_Ean, '');
  "noov:Nvfac_ccos" = COALESCE(V_Centro, '');
  "noov:Nvfac_stot" = (SELECT SUM("noov:Nvfac_stot")
                       FROM Pz_Fe_Auto_Det(:Tipo_, :Prefijo_, :Numero_));

  "noov:Nvfac_desc" = 0;
  "noov:Nvfac_anti" = 0;
  "noov:Nvfac_carg" = 0;
  "noov:Nvfac_tota" = "noov:Nvfac_stot";
  "noov:Nvfac_totp" = "noov:Nvfac_stot" + V_Impuestos;
  "noov:Nvfac_vcop" = 0;
  "noov:Nvfac_timp" = 0;
  "noov:Nvfac_coid" = '';
  "noov:Nvfac_obsb" = '';

  IF (V_Docu IN ('NOTA CREDITO', 'NOTA DEBITO', 'NOTA DE AJUSTE A DS')) THEN
  BEGIN
    "noov:Nvfac_tcru" = 'R';
    "noov:Nvfac_fecb" = V_Fecha_Ref;
    "noov:Nvcon_codi" = V_Concepto_Nc;
    -- Nombre concepto
    IF (V_Docu = 'NOTA CREDITO') THEN
      SELECT Nombre
      FROM Notas_Cre
      WHERE Codigo = :V_Concepto_Nc
      INTO V_Nom_Concepto_Nc;
    IF (V_Docu = 'NOTA DEBITO') THEN
      SELECT Nombre
      FROM Notas_Deb
      WHERE Codigo = :V_Concepto_Nc
      INTO V_Nom_Concepto_Nc;
    IF (V_Docu = 'NOTA DE AJUSTE A DS') THEN
      SELECT Nombre
      FROM Notas_Ds
      WHERE Codigo = :V_Concepto_Nc
      INTO V_Nom_Concepto_Nc;
    "noov:Nvcon_desc" = TRIM(V_Nom_Concepto_Nc);
    "noov:Nvfac_numb" = '';

    IF (TRIM(V_Prefijoref) <> '_') THEN
      "noov:Nvfac_numb" = TRIM(V_Prefijoref);
    "noov:Nvfac_numb" = TRIM("noov:Nvfac_numb") || TRIM(V_Numeroref);

    IF (V_Valida_Fe = 'N') THEN
    BEGIN
      "noov:Nvfac_tcru" = 'L';
      "noov:Nvfac_fecb" = V_Fecha;
      "noov:Nvfac_numb" = '';
    END
  END

  IF (V_Docu IN ('DOCUMENTO SOPORTE', 'NOTA DE AJUSTE A DS')) THEN
  BEGIN
    "noov:Nvpro_cper" = "noov:Nvcli_cper";
    "noov:Nvpro_cdoc" = "noov:Nvcli_cdoc";
    "noov:Nvpro_docu" = TRIM(Receptor_);
    "noov:Nvpro_dive" = '';

    IF ("noov:Nvpro_cdoc" = '31') THEN
      "noov:Nvpro_dive" = RIGHT("noov:Nvcli_docu", 1);

    "noov:Nvpro_pais" = "noov:Nvcli_pais";
    "noov:Nvpro_depa" = "noov:Nvcli_depa";
    "noov:Nvpro_ciud" = "noov:Nvcli_ciud";
    "noov:Nvpro_zipc" = V_Postal_Code;
    "noov:Nvpro_loca" = "noov:Nvcli_loca";
    "noov:Nvpro_dire" = "noov:Nvcli_dire";
    "noov:Nvpro_ntel" = "noov:Nvcli_ntel";
    "noov:Nvpro_regi" = "noov:Nvcli_regi";
    "noov:Nvpro_fisc" = "noov:Nvcli_fisc";
    "noov:Nvpro_nomb" = "noov:Nvcli_nomb";
    "noov:Nvpro_pnom" = "noov:Nvcli_pnom";
    "noov:Nvpro_snom" = "noov:Nvcli_snom";
    "noov:Nvpro_apel" = "noov:Nvcli_apel";
    "noov:Nvpro_mail" = V_Dscor;
    "noov:Nvpro_ncon" = "noov:Nvcli_ncon";
    "noov:Nvcli_cper" = '';
    "noov:Nvcli_cdoc" = '';
    "noov:Nvcli_docu" = '';
    "noov:Nvcli_pais" = '';
    "noov:Nvcli_depa" = '';
    "noov:Nvcli_ciud" = '';
    "noov:Nvcli_loca" = '';
    "noov:Nvcli_dire" = '';
    "noov:Nvcli_ntel" = '';
    "noov:Nvcli_regi" = '';
    "noov:Nvcli_fisc" = '';
    "noov:Nvcli_nomb" = '';
    "noov:Nvcli_pnom" = '';
    "noov:Nvcli_snom" = '';
    "noov:Nvcli_apel" = '';
    "noov:Nvcli_mail" = '';
    "noov:Nvcli_ncon" = '';
  END

  SUSPEND;
END^


CREATE OR ALTER PROCEDURE PZ_FE_AUTO_DET (
    TIPO_ CHAR(5),
    PREFIJO_ CHAR(5),
    NUMERO_ CHAR(10))
RETURNS (
    CONSECUTIVO INTEGER,
    "noov:Nvpro_codi" CHAR(20),
    "noov:Nvpro_nomb" CHAR(198),
    "noov:Nvuni_desc" CHAR(5),
    "noov:Nvfac_cant" DOUBLE PRECISION,
    "noov:Nvfac_valo" DOUBLE PRECISION,
    "noov:Nvimp_cdia" CHAR(5),
    "noov:Nvfac_pdes" DOUBLE PRECISION,
    "noov:Nvfac_desc" DOUBLE PRECISION,
    "noov:Nvfac_stot" DOUBLE PRECISION,
    "noov:Nvdet_piva" DOUBLE PRECISION,
    "noov:Nvdet_viva" DOUBLE PRECISION,
    "noov:Nvdet_tcod" CHAR(1),
    "noov:Nvpro_cean" CHAR(1),
    "noov:Nvdet_entr" CHAR(1),
    "noov:Nvuni_quan" DOUBLE PRECISION,
    "noov:Nvdet_nota" CHAR(200),
    "noov:Nvdet_padr" INTEGER,
    "noov:Nvdet_marc" CHAR(20),
    "noov:Nvdet_mode" CHAR(20),
    STOT_RED DOUBLE PRECISION)
AS
DECLARE VARIABLE Referencia     CHAR(20);
DECLARE VARIABLE Bruto          DOUBLE PRECISION;
DECLARE VARIABLE Stot           DOUBLE PRECISION;
DECLARE VARIABLE Descuento      DOUBLE PRECISION;
DECLARE VARIABLE Porc_Descuento DOUBLE PRECISION;
DECLARE VARIABLE V_Tarifa       DOUBLE PRECISION;
DECLARE VARIABLE V_Cdia         CHAR(5);
DECLARE VARIABLE Nom_Referencia VARCHAR(198);
DECLARE VARIABLE Cantidad       DOUBLE PRECISION;
DECLARE VARIABLE V_Valo         DOUBLE PRECISION;
DECLARE VARIABLE V_Desc         DOUBLE PRECISION;
DECLARE VARIABLE V_Base         DOUBLE PRECISION;
DECLARE VARIABLE Nota           VARCHAR(200);
BEGIN
 FOR SELECT Codconcepto,
            SUBSTRING(IIF(TRIM(Nota) = '',Nombre_Concepto,Nota) FROM 1 FOR 198),
            Cantidad,
            Bruto + Descuento,
            Descuento,
            Nota
     FROM Fe_Apo_Detalle(:Tipo_,:Prefijo_,:Numero_)
     INTO :Referencia,
          :Nom_Referencia,
          :Cantidad,
          :Bruto,
          :Descuento,
          :Nota
 DO
 BEGIN

  Cantidad = COALESCE(Cantidad,0);
  Bruto = COALESCE(Bruto,0);

  IF (Bruto * Cantidad > 0) THEN
  BEGIN
   Porc_Descuento = Descuento * 100 / Bruto;
   Stot = Cantidad * (Bruto - Descuento);
   Descuento = Descuento * Cantidad;

   V_Valo = (SELECT Valor
             FROM Redondeo_Dian(:Bruto,2));
   V_Desc = (SELECT Valor
             FROM Redondeo_Dian(:Descuento,2));

   Stot_Red = (V_Valo * Cantidad) - V_Desc;

   IF (Porc_Descuento = 100) THEN
    Stot_Red = V_Valo * Cantidad;

   V_Tarifa = 0;
   V_Cdia = '00';
   SELECT FIRST 1 "noov:Nvimp_porc",
                  "noov:Nvimp_cdia"

   FROM Pz_Fe_Auto_Imp_Base(:Tipo_,:Prefijo_,:Numero_)
   WHERE Referencia = :Referencia
   INTO V_Tarifa,
        V_Cdia;

   V_Base = (Bruto - Descuento) * Cantidad;
   V_Valo = V_Base * V_Tarifa / 100;

   Consecutivo = 1;
   "noov:Nvpro_codi" = Referencia;
   "noov:Nvpro_nomb" = Nom_Referencia;
   "noov:Nvuni_desc" = '94';
   "noov:Nvfac_cant" = Cantidad;
   "noov:Nvfac_valo" = Bruto;
   "noov:Nvimp_cdia" = V_Cdia;
   "noov:Nvfac_pdes" = (SELECT Valor
                        FROM Redondeo_Dian(:Porc_Descuento,2));
   "noov:Nvfac_desc" = Descuento;
   "noov:Nvfac_stot" = Stot;
   "noov:Nvdet_piva" = V_Tarifa;
   "noov:Nvdet_viva" = V_Valo;
   "noov:Nvdet_tcod" = '';
   "noov:Nvpro_cean" = '';
   "noov:Nvdet_entr" = '';
   "noov:Nvuni_quan" = 0;

   "noov:Nvdet_nota" = CASE TRIM(Referencia)
                         WHEN 'AIU_A' THEN 'Contrato de servicios AIU por concepto de: Servicios'
                         ELSE TRIM(Nota)
                       END;

   "noov:Nvdet_padr" = 0;
   "noov:Nvdet_marc" = '';
   "noov:Nvdet_mode" = '';

   SUSPEND;

  END
 END
END^



CREATE OR ALTER PROCEDURE PZ_FE_AUTO_IMP (
    TIPO_ CHAR(5),
    PREFIJO_ CHAR(5),
    NUMERO_ CHAR(10))
RETURNS (
    "noov:Nvimp_cdia" CHAR(5),
    "noov:Nvimp_desc" VARCHAR(40),
    "noov:Nvimp_oper" CHAR(1),
    "noov:Nvimp_base" DOUBLE PRECISION,
    "noov:Nvimp_porc" DOUBLE PRECISION,
    "noov:Nvimp_valo" DOUBLE PRECISION,
    ES_TARIFA CHAR(1),
    CLASE VARCHAR(20))
AS
DECLARE VARIABLE V_Cdia             VARCHAR(5);
DECLARE VARIABLE V_Fecha            DATE;
DECLARE VARIABLE V_Cantidad         DOUBLE PRECISION;
DECLARE VARIABLE V_Bruto            DOUBLE PRECISION;
DECLARE VARIABLE V_Impuesto         VARCHAR(5);
DECLARE VARIABLE V_Nombre_Imp       VARCHAR(40);
DECLARE VARIABLE V_Descuento        DOUBLE PRECISION;
DECLARE VARIABLE V_Aiu              DOUBLE PRECISION;
DECLARE VARIABLE V_Referencia       CHAR(20);
DECLARE VARIABLE V_Esquema_Impuesto CHAR(5);
DECLARE VARIABLE V_Tarifa           DOUBLE PRECISION;
DECLARE VARIABLE V_Base             DOUBLE PRECISION;
DECLARE VARIABLE V_Valo             DOUBLE PRECISION;
DECLARE VARIABLE V_Tipo_Impuesto    CHAR(5);
DECLARE VARIABLE V_Tipo_Retencion   CHAR(5);
DECLARE VARIABLE V_Valor_Nominal    DOUBLE PRECISION;
DECLARE VARIABLE V_Trm_Valor        DOUBLE PRECISION;
DECLARE VARIABLE V_Num_Linea        INTEGER;
BEGIN

  FOR SELECT "noov:Nvimp_cdia",
             "noov:Nvimp_desc",
             "noov:Nvimp_oper",
             SUM("noov:Nvimp_base"),
             "noov:Nvimp_porc",
             SUM("noov:Nvimp_valo"),
             Es_Tarifa,
             Clase
      FROM Pz_Fe_Auto_Imp_Base(:Tipo_, :Prefijo_, :Numero_)
      GROUP BY 1, 2, 3, 5, 7, 8
      ORDER BY 1
      INTO "noov:Nvimp_cdia",
           "noov:Nvimp_desc",
           "noov:Nvimp_oper",
           "noov:Nvimp_base",
           "noov:Nvimp_porc",
           "noov:Nvimp_valo",
           Es_Tarifa,
           Clase
  DO
  BEGIN
    SUSPEND;
  END
END^


CREATE OR ALTER PROCEDURE PZ_FE_AUTO_IMP_BASE (
    TIPO_ CHAR(5),
    PREFIJO_ CHAR(5),
    NUMERO_ CHAR(10))
RETURNS (
    "noov:Nvimp_cdia" CHAR(5),
    "noov:Nvimp_desc" VARCHAR(40),
    "noov:Nvimp_oper" CHAR(1),
    "noov:Nvimp_base" DOUBLE PRECISION,
    "noov:Nvimp_porc" DOUBLE PRECISION,
    "noov:Nvimp_valo" DOUBLE PRECISION,
    ES_TARIFA CHAR(1),
    CLASE VARCHAR(20),
    REFERENCIA VARCHAR(20))
AS
DECLARE VARIABLE V_Cdia             VARCHAR(5);
DECLARE VARIABLE V_Fecha            DATE;
DECLARE VARIABLE V_Cantidad         DOUBLE PRECISION;
DECLARE VARIABLE V_Bruto            DOUBLE PRECISION;
DECLARE VARIABLE V_Impuesto         VARCHAR(5);
DECLARE VARIABLE V_Nombre_Imp       VARCHAR(40);
DECLARE VARIABLE V_Descuento        DOUBLE PRECISION;
DECLARE VARIABLE V_Aiu              DOUBLE PRECISION;
DECLARE VARIABLE V_Referencia       CHAR(20);
DECLARE VARIABLE V_Esquema_Impuesto CHAR(5);
DECLARE VARIABLE V_Tarifa           DOUBLE PRECISION;
DECLARE VARIABLE V_Base             DOUBLE PRECISION;
DECLARE VARIABLE V_Valo             DOUBLE PRECISION;
DECLARE VARIABLE V_Tipo_Impuesto    CHAR(5);
DECLARE VARIABLE V_Tipo_Retencion   CHAR(5);
DECLARE VARIABLE V_Valor_Nominal    DOUBLE PRECISION;
DECLARE VARIABLE V_Trm_Valor        DOUBLE PRECISION;
DECLARE VARIABLE V_Num_Linea        INTEGER;
BEGIN
  SELECT Fecha
  FROM Auto_Comp
  WHERE Tipo = :Tipo_
        AND Prefijo = :Prefijo_
        AND Numero = :Numero_
  INTO V_Fecha;

  FOR SELECT Codigo_Fe,
             Codtipoimpuesto,
             Bruto,
             Cantidad,
             Codconcepto
      FROM Fe_Apo_Detalle(:Tipo_, :Prefijo_, :Numero_) P
      WHERE Valor * Cantidad > 0
            AND COALESCE(Codigo_Fe, '00') <> '00'
      ORDER BY 1, 2
      INTO V_Cdia,
           V_Impuesto,
           V_Bruto,
           V_Cantidad,
           Referencia
  DO
  BEGIN
    V_Impuesto = COALESCE(V_Impuesto, '');
    SELECT D.Tarifa
    FROM Data_Impuestos D
    WHERE Codtipoimpuesto = :V_Impuesto
          AND D.Ano = EXTRACT(YEAR FROM :V_Fecha)
    INTO V_Tarifa;
    V_Tarifa = COALESCE(V_Tarifa, 0);

    SELECT SUBSTRING(Nombre FROM 1 FOR 40)
    FROM Tipoimpuestos
    WHERE Codigo = :V_Impuesto
    INTO V_Nombre_Imp;

    V_Base = V_Bruto * V_Cantidad;
    V_Valo = V_Base * V_Tarifa / 100;
    V_Valo = COALESCE(V_Valo, 0);
    V_Base = COALESCE(V_Base, 0);

    "noov:Nvimp_cdia" = V_Cdia;
    "noov:Nvimp_desc" = CASE
                          WHEN TRIM("noov:Nvimp_cdia") = '02' THEN 'Impto al Consumo Departamental Nominal'
                          WHEN TRIM("noov:Nvimp_cdia") = '22' THEN 'Impto al Consumo de Bolsa Plastica'
                          WHEN TRIM("noov:Nvimp_cdia") = 'ZZ' THEN 'Otros tributos, tasas, contribuciones'
                          WHEN TRIM("noov:Nvimp_cdia") = '24' THEN 'Impuesto Nacional a los Combustibles'
                          WHEN TRIM("noov:Nvimp_cdia") = '25' THEN 'Sobretasa a los combustibles'
                          WHEN V_Tarifa = 19 THEN 'Impuesto sobre la Ventas 19%'
                          WHEN V_Tarifa = 5 THEN 'Impuesto sobre la Ventas 5%'
                          WHEN V_Tarifa = 0 THEN 'Impuesto sobre la Ventas Exento'
                          ELSE V_Nombre_Imp
                        END;
    "noov:Nvimp_oper" = 'S';
    "noov:Nvimp_base" = V_Base;
    "noov:Nvimp_porc" = V_Tarifa;
    "noov:Nvimp_valo" = V_Valo;

    SUSPEND;
  END

  FOR SELECT Codigo_Fe,
             Codtiporetencion,
             Base,
             Retenciones
      FROM Fe_Apo_Retenciones(:Tipo_, :Prefijo_, :Numero_)
      WHERE Retenciones > 0
      INTO V_Cdia,
           V_Impuesto,
           V_Base,
           V_Valo
  DO
  BEGIN
    SELECT Tarifa
    FROM Data_Retenciones D
    WHERE D.Codtiporetencion = :V_Impuesto
          AND D.Ano = EXTRACT(YEAR FROM :V_Fecha)
    INTO V_Tarifa;

    SELECT SUBSTRING(Nombre FROM 1 FOR 40)
    FROM Tiporetenciones
    WHERE Codigo = :V_Impuesto
    INTO V_Nombre_Imp;

    "noov:Nvimp_cdia" = V_Cdia;
    "noov:Nvimp_desc" = V_Nombre_Imp;
    "noov:Nvimp_oper" = 'R';
    "noov:Nvimp_base" = V_Base;
    "noov:Nvimp_porc" = V_Tarifa;
    "noov:Nvimp_valo" = V_Valo;

    SUSPEND;
  END

END^


CREATE OR ALTER PROCEDURE PZ_FE_AUTO_TRM (
    TIPO_ CHAR(5),
    PREFIJO_ CHAR(5),
    NUMERO_ CHAR(10))
RETURNS (
    CODIGO CHAR(5),
    VALOR DOUBLE PRECISION,
    FECHA CHAR(10))
AS
DECLARE VARIABLE V_Tercero    VARCHAR(15);
DECLARE VARIABLE V_Lista_Ter  VARCHAR(5);
DECLARE VARIABLE V_Moneda_Ter VARCHAR(5);
DECLARE VARIABLE V_Valor      DOUBLE PRECISION;
DECLARE VARIABLE V_Docuref    VARCHAR(20);
DECLARE VARIABLE V_Docu       VARCHAR(20);
DECLARE VARIABLE V_Tipo       VARCHAR(5);
DECLARE VARIABLE V_Prefijo    VARCHAR(5);
DECLARE VARIABLE V_Numero     VARCHAR(10);
BEGIN
  --Docu
  SELECT Codigo_Fe
  FROM Documentos
  WHERE Codigo = :Tipo_
  INTO V_Docu;

  V_Tipo = :Tipo_;
  V_Prefijo = :Prefijo_;
  V_Numero = :Numero_;

  --Comprobante
  IF (TRIM(V_Docu) IN ('NOTA CREDITO', 'NOTA DEBITO')) THEN
  BEGIN
    SELECT Tiporef,
           Prefijoref,
           Numeroref
    FROM Notas_Contable
    WHERE Tipo = :Tipo_
          AND Prefijo = :Prefijo_
          AND Numero = :Numero_
    INTO V_Tipo,
         V_Prefijo,
         V_Numero;

    SELECT Codigo_Fe
    FROM Documentos
    WHERE Codigo = :V_Tipo
    INTO V_Docuref;
  END

  SELECT Codtercero,
         Fecha
  FROM Fe_Apo_Comprobante(:V_Tipo, :V_Prefijo, :V_Numero)
  INTO V_Tercero,
       Fecha;

  --Lista tercero
  SELECT Codlista
  FROM Terceros
  WHERE Codigo = :V_Tercero
  INTO V_Lista_Ter;

  --Listas
  SELECT Codmoneda
  FROM Listas
  WHERE Codigo = :V_Lista_Ter
  INTO V_Moneda_Ter;
  V_Moneda_Ter = COALESCE(V_Moneda_Ter, 'USD');

  --Tasas
  SELECT FIRST 1 Valor
  FROM Tasas
  WHERE Codmoneda = :V_Moneda_Ter
        AND Fecha <= :Fecha
  ORDER BY Fecha DESC
  INTO V_Valor;

  Codigo = CASE V_Docu
             WHEN 'EXPORTACION' THEN V_Moneda_Ter
             WHEN 'NOTA CREDITO' THEN CASE V_Docuref
                                        WHEN 'EXPORTACION' THEN V_Moneda_Ter
                                        ELSE 'COP'
                                      END
             WHEN 'NOTA DEBITO' THEN CASE V_Docuref
                                       WHEN 'EXPORTACION' THEN V_Moneda_Ter
                                       ELSE 'COP'
                                     END
             ELSE 'COP'
           END;
  Valor = CASE V_Docu
            WHEN 'EXPORTACION' THEN V_Valor
            WHEN 'NOTA CREDITO' THEN CASE V_Docuref
                                       WHEN 'EXPORTACION' THEN V_Valor
                                       ELSE 1
                                     END
            WHEN 'NOTA DEBITO' THEN CASE V_Docuref
                                      WHEN 'EXPORTACION' THEN V_Valor
                                      ELSE 1
                                    END
            ELSE 1
          END;

  SUSPEND;

END^



SET TERM ; ^

COMMIT WORK;


/* EXPORTADOS CONTABLE END */



/* EXOGENA */

UPDATE Mm_Formatos
SET Informar = 'N', Con_Informados = 'N';

COMMIT WORK;

UPDATE Mm_Formatos
SET Cadena_Adicional = NULL
WHERE Codigo = '2276_V3';

COMMIT WORK;


/************************************************************/
/****                 MODULO NOMINA                      ****/
/************************************************************/

/* SC_NOMINA_045 */
--Nuevo expresión de Salario IBC para cálculos SS
--Manejo de empleados de alto Riesgo. */




UPDATE RUBROS SET 
    FORMULA = 'IF(ES_SENA=1 AND C30_EPS_PE=0) THEN
RESULT:=0
ELSE
IF (QUINCENA<2 OR ES_MEDT=1 OR (DIAS_TRABAJADOS-DIAS_YA_LQ=0)) THEN
RESULT:=0
ELSE
IF((ES_INTEGRA=0) AND ((SAL_IBC+HE038+DV997+DV300+ANTE_OS+SA009+SA020+ANTE_SIEG)>(25*SMLV))) THEN
RESULT:=ROUND((25*SMLV)/30*(SS012+ANTE_DIASE-DI009-DI020-ANTE_IEG))
ELSE
IF((ES_INTEGRA=1) AND (((SAL_IBC*C05_S_INT)+HE038+DV997+DV300+ANTE_OS+SA009+SA020+ANTE_SIEG)>(25*SMLV))) THEN
RESULT:=ROUND((25*SMLV)/30*(SS012+ANTE_DIASE-DI009-DI020-ANTE_IEG))
ELSE
IF(ES_INTEGRA=1) THEN
RESULT:=ROUND(((SAL_IBC*C05_S_INT)/30*(SS012+ANTE_DIASE-DI009-DI020-ANTE_IEG))+HE038+DV997+DV300+ANTE_OS+SA009+SA020+ANTE_SIEG)
ELSE
IF(SAL_IBC>=SMLV) THEN
RESULT:=ROUND((SAL_IBC/30*(SS012+ANTE_DIASE-DI009-DI020-ANTE_IEG))+HE038+DV997+DV300+ANTE_OS+SA009+SA020+ANTE_SIEG)
ELSE
RESULT:=ROUND((SMLV/30*(SS012+ANTE_DIASE))+HE038+DV997+ANTE_OS)'
WHERE (CODIGO = 'SS002') AND
      (NATIVO = 'S');

UPDATE RUBROS SET 
    FORMULA = 'IF(DIAS_TRABAJADOS-DIAS_YA_LQ=0) THEN 
   RESULT:=0
ELSE
IF(QUINCENA<2) THEN 
   RESULT:=0
ELSE
IF(ES_MEDT=1) THEN 
   RESULT:=ROUND((SMLV/4)*SS009)
ELSE
IF((ES_INTEGRA=0) AND ((SAL_IBC+HE038+DV997+DV300+ANTE_OS)>(25*SMLV))) THEN 
    RESULT:=ROUND((((25*SMLV)/30)*(DIAS_TRABAJADOS+ANTE_DIASE-DIAS_YA_LQ-DI009-DI020-ANTE_IEG))+SA009+SA020+ANTE_SIEG)
ELSE
IF((ES_INTEGRA=1) AND (((SAL_IBC*C05_S_INT)+HE038+DV997+DV300+ANTE_OS)>(25*SMLV)))  THEN 
    RESULT:=ROUND((((25*SMLV)/30)*(DIAS_TRABAJADOS+ANTE_DIASE-DIAS_YA_LQ-DI009-DI020-ANTE_IEG))+SA009+SA020+ANTE_SIEG)
ELSE
IF(ES_INTEGRA=1) THEN 
    RESULT:=ROUND((((SAL_IBC*C05_S_INT)/30)*(DIAS_TRABAJADOS+ANTE_DIASE - DIAS_YA_LQ-DI009-DI020-ANTE_IEG))+SA009+SA020+ANTE_SIEG)
ELSE
IF(SAL_IBC>=SMLV) THEN 
    RESULT:=ROUND(((SAL_IBC/30)*(DIAS_TRABAJADOS+ANTE_DIASE - DIAS_YA_LQ-DI009-DI020-ANTE_IEG))+SA009+SA020+ANTE_SIEG)
ELSE 
    RESULT:=ROUND(((SMLV/30)*(DIAS_TRABAJADOS+ANTE_DIASE- DIAS_YA_LQ)))'
WHERE (CODIGO = 'SS019') AND
      (NATIVO = 'S');

UPDATE RUBROS SET 
    FORMULA = 'IF(DIAS_TRABAJADOS-DIAS_YA_LQ=0) THEN 
   RESULT:=0
ELSE
IF(QUINCENA<2 OR ES_SENA=1) THEN
  RESULT:=0
ELSE
IF(EXONERADA=1 AND SA027+ANTE_SAL<SMLV*10) THEN 
    RESULT:=0
ELSE
IF((ES_INTEGRA=0) AND ((SAL_IBC+HE038+DV997+DV300+ANTE_OS)>(25*SMLV))) THEN 
    RESULT:=ROUND((((25*SMLV)/30)*(DIAS_TRABAJADOS - DIAS_YA_LQ+ANTE_DIAS-DI009-DI020-ANTE_IEG))+SA009+SA020+ANTE_SIEG)
ELSE
IF((ES_INTEGRA=1) AND (((SAL_IBC*C05_S_INT)+HE038+DV997+DV300+ANTE_OS)>(25*SMLV)))  THEN 
    RESULT:=ROUND((((25*SMLV)/30)*(DIAS_TRABAJADOS - DIAS_YA_LQ+ANTE_DIAS-DI009-DI020-ANTE_IEG))+SA009+SA020+ANTE_SIEG)
ELSE
  IF(ES_INTEGRA=1) THEN 
      RESULT:=ROUND((((SAL_IBC*C05_S_INT)/30)*(DIAS_TRABAJADOS - DIAS_YA_LQ+ANTE_DIAS-DI009-DI020-ANTE_IEG))+SA009+SA020+ANTE_SIEG)
  ELSE
       RESULT:=ROUND(((SAL_IBC/30)*(DIAS_TRABAJADOS - DIAS_YA_LQ+ANTE_DIAS-DI009-DI020-ANTE_IEG))+SA009+SA020+ANTE_SIEG)'
WHERE (CODIGO = 'SS018') AND
      (NATIVO = 'S');

UPDATE RUBROS SET 
    FORMULA = 'IF(DIAS_TRABAJADOS-DIAS_YA_LQ=0) THEN 
   RESULT:=0
ELSE
IF(DIAS=0) THEN 
   RESULT:=0
ELSE
IF(QUINCENA<2 OR ES_MEDT=1) THEN 
   RESULT:=0
ELSE
IF((ES_INTEGRA=0) AND (SAL_IBC>(25*SMLV))) THEN 
    RESULT:=ROUND((((25*SMLV)/30)*(DIAS_TRABAJADOS+ANTE_DIASE - DIAS_YA_LQ-DI009-DI020-ANTE_IEG))+SA009+SA020+ANTE_SIEG)
ELSE
IF((ES_INTEGRA=1) AND ((SAL_IBC*C05_S_INT)>(25*SMLV)))  THEN 
    RESULT:=ROUND((((25*SMLV)/30)*(DIAS_TRABAJADOS+ANTE_DIASE - DIAS_YA_LQ-DI009-DI020-ANTE_IEG))+SA009+SA020+ANTE_SIEG)
ELSE
IF(ES_INTEGRA=1) THEN 
    RESULT:=ROUND((((SAL_IBC*C05_S_INT)/30)*(DIAS_TRABAJADOS+ANTE_DIASE - DIAS_YA_LQ-DI009-DI020-ANTE_IEG))+SA009+SA020+ANTE_SIEG)
ELSE
IF(SAL_IBC>=SMLV) THEN 
    RESULT:=ROUND(((SAL_IBC/30)*(DIAS_TRABAJADOS+ANTE_DIASE - DIAS_YA_LQ-DI009-DI020-ANTE_IEG))+SA009+SA020+ANTE_SIEG)
ELSE 
    RESULT:=ROUND(((SMLV/30)*(DIAS_TRABAJADOS+ANTE_DIASE- DIAS_YA_LQ)))'
WHERE (CODIGO = 'SS017') AND
      (NATIVO = 'S');



UPDATE RUBROS SET 
    FORMULA = 'IF(DIAS_TRABAJADOS-DIAS_YA_LQ=0) THEN 
   RESULT:=0
ELSE
IF(QUINCENA<2) THEN 
   RESULT:=0
ELSE
IF(ES_ARL=1) THEN 
    RESULT:=BASICO
ELSE
IF(ES_MEDT=1) THEN 
   RESULT:=SMLV
ELSE
IF((ES_INTEGRA=0) AND (SAL_IBC>(25*SMLV))) THEN 
    RESULT:=ROUND((((25*SMLV)/30)*(DIAS_TRABAJADOS+ANTE_DIASE- DIAS_YA_LQ-DI009-DI020-ANTE_IEG))+SA009+SA020+ANTE_SIEG)
ELSE
IF((ES_INTEGRA=1) AND ((SAL_IBC*C05_S_INT)>(25*SMLV)))  THEN 
    RESULT:=ROUND((((25*SMLV)/30)*(DIAS_TRABAJADOS+ANTE_DIASE- DIAS_YA_LQ-DI009-DI020-ANTE_IEG))+SA009+SA020+ANTE_SIEG)
ELSE
IF(ES_INTEGRA=1) THEN 
    RESULT:=ROUND((((SAL_IBC*C05_S_INT)/30)*(DIAS_TRABAJADOS+ANTE_DIASE- DIAS_YA_LQ-DI009-DI020-ANTE_IEG))+SA009+SA020+ANTE_SIEG)
ELSE
  IF(SAL_IBC>=SMLV) THEN 
     RESULT:=ROUND(((SAL_IBC/30)*(DIAS_TRABAJADOS+ANTE_DIASE- DIAS_YA_LQ-DI009-DI020-ANTE_IEG))+SA009+SA020+ANTE_SIEG)
  ELSE 
      RESULT:=ROUND(((SMLV/30)*(DIAS_TRABAJADOS+ANTE_DIASE- DIAS_YA_LQ)))'
WHERE (CODIGO = 'SS015') AND
      (NATIVO = 'S');

UPDATE RUBROS SET 
    FORMULA = 'IF((DIAS_TRABAJADOS-DIAS_YA_LQ=0) AND (LD073=0)) THEN
  RESULT:=0
ELSE
IF(QUINCENA<2 OR ES_SENA=1) THEN
  RESULT:=0
ELSE
IF(EXONERADA=1 AND SA027+ANTE_SAL<SMLV*10) THEN 
    RESULT:=0
ELSE
IF((DIAS_TRABAJADOS-DIAS_YA_LQ=0) AND (LD073>1)) THEN
  RESULT:=LD073
ELSE
IF((ES_INTEGRA=0) AND ((SAL_IBC+HE038+DV997+DV300+ANTE_OS+DV040+ANTE_VA+LD073)>(25*SMLV))) THEN
    RESULT:=ROUND(((25*SMLV)/30)*(SS013+ANTE_DIAS))
ELSE
IF((ES_INTEGRA=1) AND (((SAL_IBC*C05_S_INT)+ANTE_OS+DV040+ANTE_VA+LD073)>(25*SMLV)))  THEN 
    RESULT:=ROUND(((25*SMLV)/30)*(SS013+ANTE_DIAS))
ELSE
  IF(ES_INTEGRA=1) THEN 
      RESULT:=ROUND(((SAL_IBC*C05_S_INT/30)*(SS013+ANTE_DIAS))+HE038+DV997+DV300+ANTE_OS+DV040+ANTE_VA+LD073)
  ELSE
       RESULT:=ROUND(((SAL_IBC/30)*(SS013+ANTE_DIAS))+HE038+DV997+DV300+ANTE_OS+DV040+ANTE_VA+LD073)'
WHERE (CODIGO = 'SS023') AND
      (NATIVO = 'S');


UPDATE RUBROS SET 
    FORMULA = 'IF(DIAS_TRABAJADOS-DIAS_YA_LQ=0) THEN
RESULT:=0
ELSE
IF (QUINCENA<2 OR ES_MEDT=1) THEN
RESULT:=0
ELSE
IF(EXONERADA=1 AND SA027+ANTE_SAL<SMLV*10 AND ES_SENA=0 AND C30_EPS_PE>0) THEN
RESULT:=0
ELSE
IF((ES_INTEGRA=0) AND ((SAL_IBC+HE038+DV997+DV300+ANTE_OS+SA009+SA020+ANTE_SIEG)>(25*SMLV))) THEN
RESULT:=ROUND((25*SMLV)/30*(SS012+ANTE_DIASE-DI009-DI020-ANTE_IEG))
ELSE
IF((ES_INTEGRA=1) AND (((SAL_IBC*C05_S_INT)+HE038+DV997+DV300+ANTE_OS+SA009+SA020+ANTE_SIEG)>(25*SMLV))) THEN
RESULT:=ROUND((25*SMLV)/30*(SS012+ANTE_DIASE-DI009-DI020-ANTE_IEG))
ELSE
IF(ES_INTEGRA=1) THEN
RESULT:=ROUND(((SAL_IBC*C05_S_INT)/30*(SS012+ANTE_DIASE-DI009-DI020-ANTE_IEG))+HE038+DV997+DV300+ANTE_OS+SA009+SA020+ANTE_SIEG)
ELSE
IF(SAL_IBC>=SMLV) THEN
RESULT:=ROUND((SAL_IBC/30*(SS012+ANTE_DIASE-DI009-DI020-ANTE_IEG))+HE038+DV997+DV300+ANTE_OS+SA009+SA020+ANTE_SIEG)
ELSE
RESULT:=ROUND((SMLV/30*(SS012+ANTE_DIASE))+HE038+DV997+DV300+ANTE_OS)'
WHERE (CODIGO = 'SS022') AND
      (NATIVO = 'S');

UPDATE RUBROS SET 
    FORMULA = 'IF(DIAS_TRABAJADOS-DIAS_YA_LQ=0) THEN 
   RESULT:=0
ELSE
IF(QUINCENA<2) THEN
  RESULT:=0
ELSE
IF(ES_ARL=1) THEN 
    RESULT:=ROUND((BASICO/30)*(SS010+ANTE_DARL))
ELSE
IF((ES_INTEGRA=0) AND ((SAL_IBC+HE038+DV997+DV300+ANTE_OS)>(25*SMLV))) THEN 
    RESULT:=ROUND((25*SMLV)/30*(SS010+ANTE_DARL))
ELSE
IF((ES_INTEGRA=1) AND (((SAL_IBC*C05_S_INT)+HE038+DV997+DV300+ANTE_OS)>(25*SMLV)))  THEN 
    RESULT:=ROUND((25*SMLV)/30*(SS010+ANTE_DARL))
ELSE
IF(ES_INTEGRA=1) THEN 
    RESULT:=ROUND(((SAL_IBC*C05_S_INT)/30*(SS010+ANTE_DARL))+HE038+DV997+DV300+ANTE_OS)
ELSE
IF(SAL_IBC>=SMLV) THEN 
    RESULT:=ROUND(((SAL_IBC/30)*(SS010+ANTE_DARL))+HE038+DV997+DV300+ANTE_OS)
ELSE 
    RESULT:=ROUND(((SMLV/30)*(SS010+ANTE_DARL))+HE038+DV997+DV300+ANTE_OS)'
WHERE (CODIGO = 'SS020') AND
      (NATIVO = 'S');


COMMIT WORK;



UPDATE RUBROS SET 
    FORMULA = 'U_CES'
WHERE (CODIGO = 'BA071') AND
      (NATIVO = 'S');


COMMIT WORK;


EXECUTE BLOCK
AS
DECLARE VARIABLE Vpila CHAR(4);
DECLARE VARIABLE Vnativo CHAR(1);
DECLARE VARIABLE Vsql CHAR(1000);
BEGIN

  FOR SELECT Columna_pila, nativo
      FROM Constantes C
      WHERE C.Codigo = 'C34_AFP_EM' AND
            C.Nativo = 'S'
      INTO Vpila, Vnativo
  DO
  BEGIN
    Vsql = 'UPDATE OR INSERT INTO CONSTANTES (CODIGO, NOMBRE, COLUMNA_PILA, NATIVO)
        VALUES (''C37_AFP_EM'',''PORCENTAJE APORTE AFP EMPRESA ALTO RIESGO'',''' || Vpila || ''',''' || Vnativo || ''')';
    EXECUTE STATEMENT Vsql;
    Vsql = 'UPDATE OR INSERT INTO CONSTANTESVALOR (CODCONSTANTE, ANO, VALOR)
        VALUES (''C37_AFP_EM'',''2022'',''22'')';
    EXECUTE STATEMENT Vsql;
  END
END; 

COMMIT WORK;


EXECUTE BLOCK
AS
DECLARE VARIABLE Vnativo CHAR(1);
DECLARE VARIABLE Vsql CHAR(1000);
BEGIN

  FOR SELECT Nativo
      FROM Constantes C
      WHERE C.Codigo = 'C20_PR_VAC' AND
            C.Nativo = 'S'
      INTO Vnativo
  DO
  BEGIN
    Vsql = 'UPDATE OR INSERT INTO CONSTANTES (CODIGO, NOMBRE, NATIVO)
        VALUES (''C19_PR_VAC'',''PROVISION VACACIONES ALTO RIESGO'',''' || Vnativo || ''')';
    EXECUTE STATEMENT Vsql;
    Vsql = 'UPDATE OR INSERT INTO CONSTANTESVALOR (CODCONSTANTE, ANO, VALOR)
        VALUES (''C19_PR_VAC'',''2022'',''12'')';
    EXECUTE STATEMENT Vsql;
  END
END; 

COMMIT WORK;


UPDATE RUBROS SET 
    FORMULA = 'IF(ES_SENA=1 OR ES_ARL=1) THEN
  RESULT:=0
ELSE
IF(ES_ALTOR=1) THEN
  RESULT:=ROUND((SAL_BASICO/30*DI015+HE024+HE025)/C19_PR_VAC)
ELSE 
  RESULT:=ROUND((SAL_BASICO/30*DI015+HE024+HE025)/C20_PR_VAC)'
WHERE (CODIGO = 'PR063') AND
      (NATIVO = 'S');


COMMIT WORK;

UPDATE RUBROS SET 
    FORMULA = 'IF(QUINCENA<2 OR ES_SENA=1 OR ES_PENSION=1) THEN
  RESULT:=0
ELSE
IF(ES_ALTOR=1) THEN
  RESULT:=SS024*C37_AFP_EM/100+SS051
ELSE
  RESULT:=SS024*C34_AFP_EM/100+SS051'
WHERE (CODIGO = 'SS034') AND
      (NATIVO = 'S');


COMMIT WORK;


/* SC_NOMINA_046 */

UPDATE CONSTANTES SET COLUMNA_PILA=NULL WHERE CODIGO='C37_AFP_EM' AND NATIVO='S';
COMMIT;

/* SC_NOMINA_047 */

UPDATE RUBROS SET 
    FORMULA = 'IF(DIAS_TRABAJADOS-DIAS_YA_LQ=0) THEN 
   RESULT:=0
ELSE
IF(QUINCENA<2) THEN 
   RESULT:=0
ELSE
IF(ES_MEDT=1) THEN 
   RESULT:=ROUND((SMLV/4)*SS009)
ELSE
IF((ES_INTEGRA=0) AND ((SAL_IBC+HE038+DV997+DV300+ANTE_OS)>(25*SMLV))) THEN 
    RESULT:=ROUND((((25*SMLV)/30)*(DIAS_TRABAJADOS+ANTE_DIASE-DIAS_YA_LQ-DI009-DI020-ANTE_IEG))+SA009+SA020+ANTE_SIEG)
ELSE
IF((ES_INTEGRA=1) AND (((SAL_IBC*C05_S_INT)+HE038+DV997+DV300+ANTE_OS)>(25*SMLV)))  THEN 
    RESULT:=ROUND((((25*SMLV)/30)*(DIAS_TRABAJADOS+ANTE_DIASE-DIAS_YA_LQ-DI009-DI020-ANTE_IEG))+SA009+SA020+ANTE_SIEG)
ELSE
IF(ES_INTEGRA=1) THEN 
    RESULT:=ROUND((((SAL_IBC*C05_S_INT)/30)*(DIAS_TRABAJADOS+ANTE_DIASE - DIAS_YA_LQ-DI009-DI020-ANTE_IEG))+SA009+SA020+ANTE_SIEG)
ELSE
IF(SAL_BASICO>=SMLV) THEN 
    RESULT:=ROUND(((SAL_IBC/30)*(DIAS_TRABAJADOS+ANTE_DIASE - DIAS_YA_LQ-DI009-DI020-ANTE_IEG))+SA009+SA020+ANTE_SIEG)
ELSE 
    RESULT:=ROUND(((SMLV/30)*(DIAS_TRABAJADOS+ANTE_DIASE- DIAS_YA_LQ)))'
WHERE (CODIGO = 'SS019') AND
      (NATIVO = 'S');

UPDATE RUBROS SET 
    FORMULA = 'IF(DIAS_TRABAJADOS-DIAS_YA_LQ=0) THEN 
   RESULT:=0
ELSE
IF(QUINCENA<2 OR ES_SENA=1 OR ES_MEDT=1) THEN
  RESULT:=0
ELSE
IF(EXONERADA=1 AND SA027+ANTE_SAL<SMLV*10) THEN 
    RESULT:=0
ELSE
IF(SAL_BASICO<SMLV) THEN 
   RESULT:=ROUND(((SMLV/30)*(DIAS_TRABAJADOS - DIAS_YA_LQ+ANTE_DIAS-DI009-DI020-ANTE_IEG))+SA009+SA020+ANTE_SIEG)
ELSE
IF((ES_INTEGRA=0) AND ((SAL_IBC+HE038+DV997+DV300+ANTE_OS)>(25*SMLV))) THEN 
    RESULT:=ROUND((((25*SMLV)/30)*(DIAS_TRABAJADOS - DIAS_YA_LQ+ANTE_DIAS-DI009-DI020-ANTE_IEG))+SA009+SA020+ANTE_SIEG)
ELSE
IF((ES_INTEGRA=1) AND (((SAL_IBC*C05_S_INT)+HE038+DV997+DV300+ANTE_OS)>(25*SMLV)))  THEN 
    RESULT:=ROUND((((25*SMLV)/30)*(DIAS_TRABAJADOS - DIAS_YA_LQ+ANTE_DIAS-DI009-DI020-ANTE_IEG))+SA009+SA020+ANTE_SIEG)
ELSE
  IF(ES_INTEGRA=1) THEN 
      RESULT:=ROUND((((SAL_IBC*C05_S_INT)/30)*(DIAS_TRABAJADOS - DIAS_YA_LQ+ANTE_DIAS-DI009-DI020-ANTE_IEG))+SA009+SA020+ANTE_SIEG)
  ELSE
       RESULT:=ROUND(((SAL_IBC/30)*(DIAS_TRABAJADOS - DIAS_YA_LQ+ANTE_DIAS-DI009-DI020-ANTE_IEG))+SA009+SA020+ANTE_SIEG)'
WHERE (CODIGO = 'SS018') AND
      (NATIVO = 'S');

UPDATE RUBROS SET 
    FORMULA = 'IF(DIAS_TRABAJADOS-DIAS_YA_LQ=0) THEN 
   RESULT:=0
ELSE
IF(DIAS=0) THEN 
   RESULT:=0
ELSE
IF(QUINCENA<2 OR ES_MEDT=1) THEN 
   RESULT:=0
ELSE
IF((ES_INTEGRA=0) AND (SAL_IBC>(25*SMLV))) THEN 
    RESULT:=ROUND((((25*SMLV)/30)*(DIAS_TRABAJADOS+ANTE_DIASE - DIAS_YA_LQ-DI009-DI020-ANTE_IEG))+SA009+SA020+ANTE_SIEG)
ELSE
IF((ES_INTEGRA=1) AND ((SAL_IBC*C05_S_INT)>(25*SMLV)))  THEN 
    RESULT:=ROUND((((25*SMLV)/30)*(DIAS_TRABAJADOS+ANTE_DIASE - DIAS_YA_LQ-DI009-DI020-ANTE_IEG))+SA009+SA020+ANTE_SIEG)
ELSE
IF(ES_INTEGRA=1) THEN 
    RESULT:=ROUND((((SAL_IBC*C05_S_INT)/30)*(DIAS_TRABAJADOS+ANTE_DIASE - DIAS_YA_LQ-DI009-DI020-ANTE_IEG))+SA009+SA020+ANTE_SIEG)
ELSE
IF(SAL_BASICO>=SMLV) THEN 
    RESULT:=ROUND(((SAL_IBC/30)*(DIAS_TRABAJADOS+ANTE_DIASE-DIAS_YA_LQ-DI009-DI020-ANTE_IEG))+SA009+SA020+ANTE_SIEG)
ELSE 
    RESULT:=ROUND(((SMLV/30)*(DIAS_TRABAJADOS+ANTE_DIASE-DIAS_YA_LQ)))'
WHERE (CODIGO = 'SS017') AND
      (NATIVO = 'S');


UPDATE RUBROS SET 
    FORMULA = 'IF(DIAS_TRABAJADOS-DIAS_YA_LQ=0) THEN 
   RESULT:=0
ELSE
IF(QUINCENA<2) THEN 
   RESULT:=0
ELSE
IF(ES_ARL=1) THEN 
    RESULT:=BASICO
ELSE
IF(ES_MEDT=1) THEN 
   RESULT:=SMLV
ELSE
IF((ES_INTEGRA=0) AND (SAL_IBC>(25*SMLV))) THEN 
    RESULT:=ROUND((((25*SMLV)/30)*(DIAS_TRABAJADOS+ANTE_DIASE- DIAS_YA_LQ-DI009-DI020-ANTE_IEG))+SA009+SA020+ANTE_SIEG)
ELSE
IF((ES_INTEGRA=1) AND ((SAL_IBC*C05_S_INT)>(25*SMLV)))  THEN 
    RESULT:=ROUND((((25*SMLV)/30)*(DIAS_TRABAJADOS+ANTE_DIASE- DIAS_YA_LQ-DI009-DI020-ANTE_IEG))+SA009+SA020+ANTE_SIEG)
ELSE
IF(ES_INTEGRA=1) THEN 
    RESULT:=ROUND((((SAL_IBC*C05_S_INT)/30)*(DIAS_TRABAJADOS+ANTE_DIASE- DIAS_YA_LQ-DI009-DI020-ANTE_IEG))+SA009+SA020+ANTE_SIEG)
ELSE
  IF(SAL_BASICO>=SMLV) THEN 
     RESULT:=ROUND(((SAL_IBC/30)*(DIAS_TRABAJADOS+ANTE_DIASE- DIAS_YA_LQ-DI009-DI020-ANTE_IEG))+SA009+SA020+ANTE_SIEG)
  ELSE 
      RESULT:=ROUND(((SMLV/30)*(DIAS_TRABAJADOS+ANTE_DIASE- DIAS_YA_LQ)))'
WHERE (CODIGO = 'SS015') AND
      (NATIVO = 'S');

UPDATE RUBROS SET 
    FORMULA = 'IF((DIAS_TRABAJADOS-DIAS_YA_LQ=0) AND (LD073=0)) THEN
  RESULT:=0
ELSE
IF(QUINCENA<2 OR ES_SENA=1 OR ES_MEDT=1) THEN
  RESULT:=0
ELSE
IF(EXONERADA=1 AND SA027+ANTE_SAL<SMLV*10) THEN 
    RESULT:=0
ELSE
IF((DIAS_TRABAJADOS-DIAS_YA_LQ=0) AND (LD073>1)) THEN
  RESULT:=LD073
ELSE
IF(SAL_BASICO<SMLV) THEN 
   RESULT:=ROUND(((SMLV/30)*(SS013+ANTE_DIAS))+HE038+DV997+DV300+ANTE_OS+DV040+ANTE_VA+LD073)
ELSE
IF((ES_INTEGRA=0) AND ((SAL_IBC+HE038+DV997+DV300+ANTE_OS+DV040+ANTE_VA+LD073)>(25*SMLV))) THEN
    RESULT:=ROUND(((25*SMLV)/30)*(SS013+ANTE_DIAS))
ELSE
IF((ES_INTEGRA=1) AND (((SAL_IBC*C05_S_INT)+ANTE_OS+DV040+ANTE_VA+LD073)>(25*SMLV)))  THEN 
    RESULT:=ROUND(((25*SMLV)/30)*(SS013+ANTE_DIAS))
ELSE
  IF(ES_INTEGRA=1) THEN 
      RESULT:=ROUND(((SAL_IBC*C05_S_INT/30)*(SS013+ANTE_DIAS))+HE038+DV997+DV300+ANTE_OS+DV040+ANTE_VA+LD073)
  ELSE
       RESULT:=ROUND(((SAL_IBC/30)*(SS013+ANTE_DIAS))+HE038+DV997+DV300+ANTE_OS+DV040+ANTE_VA+LD073)'
WHERE (CODIGO = 'SS023') AND
      (NATIVO = 'S');

UPDATE RUBROS SET 
    FORMULA = 'IF((DIAS_TRABAJADOS-DIAS_YA_LQ=0) OR (QUINCENA<2))THEN 
   RESULT:=0
ELSE
IF(ES_MEDT=1) THEN 
   RESULT:=(((SMLV/4)*SS009)+HE038+DV997+DV300+ANTE_OS)
ELSE
IF((ES_INTEGRA=0) AND ((SAL_IBC+HE038+DV997+DV300+ANTE_OS)>(25*SMLV))) THEN 
    RESULT:=ROUND(((25*SMLV)/30*(SS014+ANTE_DAFP-DI009-DI020-ANTE_IEG))+HE038+DV997+DV300+ANTE_OS+SA009+SA020+ANTE_SIEG)
ELSE
IF((ES_INTEGRA=1) AND (((SAL_IBC*C05_S_INT)+HE038+DV997+DV300+ANTE_OS)>(25*SMLV)))  THEN 
    RESULT:=ROUND(((25*SMLV)/30*(SS014+ANTE_DAFP-DI009-DI020-ANTE_IEG))+HE038+DV997+DV300+ANTE_OS+SA009+SA020+ANTE_SIEG)
ELSE
IF(ES_INTEGRA=1) THEN 
    RESULT:=ROUND(((SAL_IBC*C05_S_INT)/30*(SS014+ANTE_DAFP-DI009-DI020-ANTE_IEG))+HE038+DV997+DV300+ANTE_OS+SA009+SA020+ANTE_SIEG)
ELSE
IF(SAL_BASICO>=SMLV) THEN 
    RESULT:=ROUND((SAL_IBC/30*(SS014+ANTE_DAFP-DI009-DI020-ANTE_IEG))+HE038+DV997+DV300+ANTE_OS+SA009+SA020+ANTE_SIEG)
ELSE 
    RESULT:=ROUND((SMLV/30*(SS014+ANTE_DAFP))+HE038+DV997+DV300+ANTE_OS)'
WHERE (CODIGO = 'SS024') AND
      (NATIVO = 'S');

UPDATE RUBROS SET 
    FORMULA = 'IF(DIAS_TRABAJADOS-DIAS_YA_LQ=0) THEN
RESULT:=0
ELSE
IF (QUINCENA<2 OR ES_MEDT=1) THEN
RESULT:=0
ELSE
IF(EXONERADA=1 AND SA027+ANTE_SAL<SMLV*10 AND ES_SENA=0 AND C30_EPS_PE>0) THEN
RESULT:=0
ELSE
IF((ES_INTEGRA=0) AND ((SAL_IBC+HE038+DV997+DV300+ANTE_OS+SA009+SA020+ANTE_SIEG)>(25*SMLV))) THEN
RESULT:=ROUND((25*SMLV)/30*(SS012+ANTE_DIASE-DI009-DI020-ANTE_IEG))
ELSE
IF((ES_INTEGRA=1) AND (((SAL_IBC*C05_S_INT)+HE038+DV997+DV300+ANTE_OS+SA009+SA020+ANTE_SIEG)>(25*SMLV))) THEN
RESULT:=ROUND((25*SMLV)/30*(SS012+ANTE_DIASE-DI009-DI020-ANTE_IEG))
ELSE
IF(ES_INTEGRA=1) THEN
RESULT:=ROUND(((SAL_IBC*C05_S_INT)/30*(SS012+ANTE_DIASE-DI009-DI020-ANTE_IEG))+HE038+DV997+DV300+ANTE_OS+SA009+SA020+ANTE_SIEG)
ELSE
IF(SAL_BASICO>=SMLV) THEN
RESULT:=ROUND((SAL_IBC/30*(SS012+ANTE_DIASE-DI009-DI020-ANTE_IEG))+HE038+DV997+DV300+ANTE_OS+SA009+SA020+ANTE_SIEG)
ELSE
RESULT:=ROUND((SMLV/30*(SS012+ANTE_DIASE))+HE038+DV997+DV300+ANTE_OS)'
WHERE (CODIGO = 'SS022') AND
      (NATIVO = 'S');

UPDATE RUBROS SET 
    FORMULA = 'IF(DIAS_TRABAJADOS-DIAS_YA_LQ=0) THEN 
   RESULT:=0
ELSE
IF(QUINCENA<2) THEN
  RESULT:=0
ELSE
IF(ES_ARL=1) THEN 
    RESULT:=ROUND((BASICO/30)*(SS010+ANTE_DARL))
ELSE
IF((ES_INTEGRA=0) AND ((SAL_IBC+HE038+DV997+DV300+ANTE_OS)>(25*SMLV))) THEN 
    RESULT:=ROUND((25*SMLV)/30*(SS010+ANTE_DARL))
ELSE
IF((ES_INTEGRA=1) AND (((SAL_IBC*C05_S_INT)+HE038+DV997+DV300+ANTE_OS)>(25*SMLV)))  THEN 
    RESULT:=ROUND((25*SMLV)/30*(SS010+ANTE_DARL))
ELSE
IF(ES_INTEGRA=1) THEN 
    RESULT:=ROUND(((SAL_IBC*C05_S_INT)/30*(SS010+ANTE_DARL))+HE038+DV997+DV300+ANTE_OS)
ELSE
IF(SAL_BASICO>=SMLV) THEN 
    RESULT:=ROUND(((SAL_IBC/30)*(SS010+ANTE_DARL))+HE038+DV997+DV300+ANTE_OS)
ELSE 
    RESULT:=ROUND(((SMLV/30)*(SS010+ANTE_DARL))+HE038+DV997+DV300+ANTE_OS)'
WHERE (CODIGO = 'SS020') AND
      (NATIVO = 'S');


COMMIT WORK;



UPDATE RUBROS SET 
    NATIVO = 'N'
WHERE (CODIGO = 'HM030');

UPDATE RUBROS SET 
    NATIVO = 'N'
WHERE (CODIGO = 'DT217');

UPDATE RUBROS SET 
    NATIVO = 'N'
WHERE (CODIGO = 'DT218');

UPDATE RUBROS SET 
    NATIVO = 'N'
WHERE (CODIGO = 'DT219');

UPDATE RUBROS SET 
    NATIVO = 'N'
WHERE (CODIGO = 'DT220');

UPDATE RUBROS SET 
    NATIVO = 'N'
WHERE (CODIGO = 'DV109');

UPDATE RUBROS SET 
    NATIVO = 'N'
WHERE (CODIGO = 'DV110');

UPDATE RUBROS SET 
    NATIVO = 'N'
WHERE (CODIGO = 'DV997');

UPDATE RUBROS SET 
    NATIVO = 'N'
WHERE (CODIGO = 'DV200');

UPDATE RUBROS SET 
    NATIVO = 'N'
WHERE (CODIGO = 'DV201');

UPDATE RUBROS SET 
    NATIVO = 'N'
WHERE (CODIGO = 'DV202');

UPDATE RUBROS SET 
    NATIVO = 'N'
WHERE (CODIGO = 'DV203');

UPDATE RUBROS SET 
    NATIVO = 'N'
WHERE (CODIGO = 'DV204');

UPDATE RUBROS SET 
    NATIVO = 'N'
WHERE (CODIGO = 'DV205');

UPDATE RUBROS SET 
    NATIVO = 'N'
WHERE (CODIGO = 'DV206');

UPDATE RUBROS SET 
    NATIVO = 'N'
WHERE (CODIGO = 'DV207');

UPDATE RUBROS SET 
    NATIVO = 'N'
WHERE (CODIGO = 'DV208');

UPDATE RUBROS SET 
    NATIVO = 'N'
WHERE (CODIGO = 'DV209');

UPDATE RUBROS SET 
    NATIVO = 'N'
WHERE (CODIGO = 'DV210');

UPDATE RUBROS SET 
    NATIVO = 'N'
WHERE (CODIGO = 'DV100');

UPDATE RUBROS SET 
    NATIVO = 'N'
WHERE (CODIGO = 'DV101');

UPDATE RUBROS SET 
    NATIVO = 'N'
WHERE (CODIGO = 'DV102');

UPDATE RUBROS SET 
    NATIVO = 'N'
WHERE (CODIGO = 'DV103');

UPDATE RUBROS SET 
    NATIVO = 'N'
WHERE (CODIGO = 'DV104');

UPDATE RUBROS SET 
    NATIVO = 'N'
WHERE (CODIGO = 'DV105');

UPDATE RUBROS SET 
    NATIVO = 'N'
WHERE (CODIGO = 'DV106');

UPDATE RUBROS SET 
    NATIVO = 'N'
WHERE (CODIGO = 'DV107');

UPDATE RUBROS SET 
    NATIVO = 'N'
WHERE (CODIGO = 'DV108');

UPDATE RUBROS SET 
    NATIVO = 'N'
WHERE (CODIGO = 'DT200');

UPDATE RUBROS SET 
    NATIVO = 'N'
WHERE (CODIGO = 'DT204');

UPDATE RUBROS SET 
    NATIVO = 'N'
WHERE (CODIGO = 'DT205');

UPDATE RUBROS SET 
    NATIVO = 'N'
WHERE (CODIGO = 'DT206');

UPDATE RUBROS SET 
    NATIVO = 'N'
WHERE (CODIGO = 'DT998');

UPDATE RUBROS SET 
    NATIVO = 'N'
WHERE (CODIGO = 'LD996');

UPDATE RUBROS SET 
    NATIVO = 'N'
WHERE (CODIGO = 'HM035');

UPDATE RUBROS SET 
    NATIVO = 'N'
WHERE (CODIGO = 'LD999');

UPDATE RUBROS SET 
    NATIVO = 'N'
WHERE (CODIGO = 'DV999');

UPDATE RUBROS SET 
    NATIVO = 'N'
WHERE (CODIGO = 'DV998');

UPDATE RUBROS SET 
    NATIVO = 'N'
WHERE (CODIGO = 'DT999');

UPDATE RUBROS SET 
    NATIVO = 'N'
WHERE (CODIGO = 'HM034');

UPDATE RUBROS SET 
    NATIVO = 'N'
WHERE (CODIGO = 'DT207');

UPDATE RUBROS SET 
    NATIVO = 'N'
WHERE (CODIGO = 'DT208');

UPDATE RUBROS SET 
    NATIVO = 'N'
WHERE (CODIGO = 'DT203');

UPDATE RUBROS SET 
    NATIVO = 'N'
WHERE (CODIGO = 'DT202');

UPDATE RUBROS SET 
    NATIVO = 'N'
WHERE (CODIGO = 'DT201');

UPDATE RUBROS SET 
    NATIVO = 'N'
WHERE (CODIGO = 'DT209');

UPDATE RUBROS SET 
    NATIVO = 'N'
WHERE (CODIGO = 'DT210');

UPDATE RUBROS SET 
    NATIVO = 'N'
WHERE (CODIGO = 'DT211');

UPDATE RUBROS SET 
    NATIVO = 'N'
WHERE (CODIGO = 'DT212');

UPDATE RUBROS SET 
    NATIVO = 'N'
WHERE (CODIGO = 'DT213');

UPDATE RUBROS SET 
    NATIVO = 'N'
WHERE (CODIGO = 'DT214');

UPDATE RUBROS SET 
    NATIVO = 'N'
WHERE (CODIGO = 'DT215');

UPDATE RUBROS SET 
    NATIVO = 'N'
WHERE (CODIGO = 'DT216');

UPDATE RUBROS SET 
    NATIVO = 'N'
WHERE (CODIGO = 'LD998');

UPDATE RUBROS SET 
    NATIVO = 'N'
WHERE (CODIGO = 'LD997');


COMMIT WORK;


/* EXPRESIONES */ 
UPDATE OR INSERT INTO EXPRESIONES (CODIGO, NOMBRE, SQL_BLOQUE, ACTIVO, NATIVO)
                           VALUES ('SAL_BASICO', 'SALARIO BASICO PROMEDIO', '--BASICO PRORRATEADO IBC

SELECT Empleado, Valor
FROM Pz_Ex_Sal_Basico', 'S', 'S')
                         MATCHING (CODIGO);

COMMIT WORK;

UPDATE OR INSERT INTO EXPRESIONES (CODIGO, NOMBRE, SQL_BLOQUE, ACTIVO, NATIVO)
                           VALUES ('SAL_IBC', 'SALARIO BASE PARA IBC', '--BASICO PRORRATEADO IBC
EXECUTE BLOCK
RETURNS (
    Empleado CHAR(15),
    Valor    NUMERIC(17,4))
AS
DECLARE V_Nomina CHAR(5);
BEGIN
  SELECT Rdb$Get_Context(''USER_SESSION'', ''G_NOMINA'')
  FROM Rdb$Database
  INTO V_Nomina;

  FOR SELECT Empleado, Valor
      FROM Pz_Ex_Sal_Ibc(:V_Nomina)
      INTO Empleado, Valor
  DO
    SUSPEND;
END', 'S', 'S')
                         MATCHING (CODIGO);

COMMIT WORK;

UPDATE OR INSERT INTO EXPRESIONES (CODIGO, NOMBRE, SQL_BLOQUE, ACTIVO, NATIVO)
                           VALUES ('U_CES', 'ULTIMAS CESANTIAS CALCULAS BASE PARA INTERESES', '--TRAE EL ULTIMO VALOR DEL RUBRO SOLICITADO
EXECUTE BLOCK
RETURNS (
    Empleado CHAR(15),
    Valor NUMERIC(17,4))
AS
DECLARE V_nomina CHAR(5);

DECLARE V_liq CHAR(10);
DECLARE V_fecha_nom DATE;
DECLARE V_desde_nom DATE;
DECLARE V_fecha_ing DATE;
DECLARE M_hasta DATE;
DECLARE V_hasta_nom DATE;
DECLARE V_rubro1 CHAR(5) = ''LD071'';
DECLARE V_rubro2 CHAR(5) = '''';
DECLARE V_rubro3 CHAR(5) = '''';
DECLARE V_esquema CHAR(1);

BEGIN
  SELECT Rdb$get_context(''USER_SESSION'', ''G_NOMINA'')
  FROM Rdb$database
  INTO V_nomina;

  SELECT Fecha,
         Desde,
         Hasta,
         Codliquidacion,
         Esquema
  FROM Nominas
  WHERE Codigo = :V_nomina
  INTO V_fecha_nom,
       V_desde_nom,
       V_hasta_nom,
       V_liq,
       V_esquema;

  FOR SELECT sl.Codpersonal,
             MAX(S.Desde)
      FROM TMP_Empleados Sl
      JOIN Salarios S ON (Sl.Codpersonal = S.Codpersonal)
      WHERE S.Columna_desde = ''INGRESO''
      GROUP BY 1
      INTO Empleado,
           V_fecha_ing
  DO
  BEGIN

    SELECT MAX(N.Fecha)
    FROM Nominas N
    JOIN Planillas S ON (N.Codigo = S.Codnomina)
    WHERE S.Codrubro IN (:V_rubro1, :V_rubro2, :V_rubro3) AND
          N.Codigo <> :V_nomina AND
          N.Fecha <= :V_fecha_nom AND
          N.Fecha >= :V_fecha_ing AND
          S.Codpersonal = :Empleado
    INTO M_hasta;

    IF (M_hasta IS NULL) THEN
      Valor = 0;
    ELSE
      SELECT SUM(S.Adicion)
      FROM Planillas S
      JOIN Nominas N ON (S.Codnomina = N.Codigo)
      WHERE N.Fecha = :M_hasta AND
            N.Codigo < :V_nomina AND
            S.Codrubro IN (:V_rubro1, :V_rubro2, :V_rubro3) AND
            S.Codpersonal = :Empleado
      INTO Valor;

    --  IF (Valor IS NOT NULL) THEN
    Valor = COALESCE(Valor, 0);
    SUSPEND;
  END
END', 'S', 'S');

COMMIT WORK;

UPDATE OR INSERT INTO EXPRESIONES (CODIGO, NOMBRE, SQL_BLOQUE, ACTIVO, NATIVO)
                           VALUES ('ES_ALTOR', 'VALIDACION SI EMPLEADO ES DE ALTO RIESGO', '--ES EMPLEADO ALTO RIESGO (1=SI, 0=NO)
EXECUTE BLOCK
RETURNS (
    Empleado CHAR(15),
    Valor NUMERIC(17,4))
AS
DECLARE V_Nomina    CHAR(5);

DECLARE V_Liq       CHAR(10);
DECLARE V_Riesgo      CHAR(2);
DECLARE V_Desde_Nom DATE;
DECLARE V_Hasta_Nom DATE;

BEGIN
  SELECT Rdb$Get_Context(''USER_SESSION'', ''G_NOMINA'')
  FROM Rdb$Database
  INTO V_Nomina;

  SELECT Desde, Hasta, Codliquidacion
  FROM Nominas
  WHERE Codigo = :V_Nomina
  INTO V_Desde_Nom, V_Hasta_Nom, V_Liq;

  FOR SELECT S.Codpersonal, P.Alto_Riesgo
      FROM TMP_Empleados S
      JOIN Personal P ON (S.Codpersonal = P.Codigo)
      INTO Empleado,
           V_Riesgo
  DO
  BEGIN
    Valor = 0;
    IF (V_Riesgo=''S'') THEN
      Valor = 1;
    SUSPEND;
  END
END', 'S', 'S')
                         ;

COMMIT WORK;


UPDATE OR INSERT INTO EXPRESIONES (CODIGO, NOMBRE, SQL_BLOQUE, ACTIVO, NATIVO)
                           VALUES ('ANTE_DSML', 'AJUSTE DE QUINCENA ANTERIOR DE INFERIORES AL MINIMO PARA AFP RUBRO - SS050', '-- SUMATORIA RUBROS DEL MISMO MES
EXECUTE BLOCK
RETURNS (
    Empleado CHAR(15),
    Valor    NUMERIC(17,4))
AS
DECLARE V_Nomina    CHAR(5);
DECLARE V_Fecha_Nom DATE;
DECLARE V_Desde_Nom DATE;
DECLARE V_Hasta_Nom DATE;
DECLARE V_Liq       CHAR(10);
DECLARE V_Rubro1    CHAR(5) = ''SS050'';
DECLARE V_Rubro2    CHAR(5) = '''';
DECLARE V_Rubro3    CHAR(5) = '''';

BEGIN
  SELECT Rdb$Get_Context(''USER_SESSION'', ''G_NOMINA'')
  FROM Rdb$Database
  INTO V_Nomina;

  SELECT Fecha,
         Desde,
         Hasta,
         Codliquidacion
  FROM Nominas
  WHERE Codigo = :V_Nomina
  INTO V_Fecha_Nom, V_Desde_Nom, V_Hasta_Nom, V_Liq;
  
  FOR SELECT  Codpersonal
    FROM TMP_EMPLEADOS
    INTO Empleado 
  DO
  BEGIN
    SELECT SUM(S.Adicion + S.Deduccion)
    FROM Planillas S
    JOIN Nominas N ON (S.Codnomina = N.Codigo)
    WHERE EXTRACT(MONTH FROM N.Fecha) = EXTRACT(MONTH FROM :V_Fecha_Nom)
          AND EXTRACT(YEAR FROM N.Fecha) = EXTRACT(YEAR FROM :V_Fecha_Nom)
          AND S.Codrubro IN (:V_Rubro1, :V_Rubro2, :V_Rubro3)
          AND S.Codpersonal = :Empleado  AND
          N.Fecha < :V_Fecha_Nom
    INTO Valor;

    Valor = COALESCE(Valor, 0);
    SUSPEND;
  END
END', 'S', 'S')
                         ;
UPDATE OR INSERT INTO EXPRESIONES (CODIGO, NOMBRE, SQL_BLOQUE, ACTIVO, NATIVO)
                           VALUES ('SMLV', 'SALARIO MINIMO LEGAL VIGENTE', '-- SALARIO MINIMO LEGAL VIGENTE SEGUN A?O LIQUIDACION
EXECUTE BLOCK
RETURNS (
    Empleado CHAR(15),
    Valor NUMERIC(17,4))
AS
DECLARE V_Nomina CHAR(5);
DECLARE V_Hasta DATE;

BEGIN
  SELECT Rdb$Get_Context(''USER_SESSION'', ''G_NOMINA'')
  FROM Rdb$Database
  INTO V_Nomina;

  SELECT Hasta
  FROM Nominas
  WHERE Codigo = :V_Nomina
  INTO V_Hasta;

  SELECT Valor
  FROM Constantesvalor
  WHERE Codconstante = ''C01_SMLV'' AND
        Ano = EXTRACT(YEAR FROM :V_Hasta)
  INTO Valor;

  FOR SELECT Pl.Codpersonal
      FROM Tmp_Empleados Pl
      INTO Empleado
  DO
  BEGIN
    SUSPEND;
  END
END', 'S', 'S')
                         ;
UPDATE OR INSERT INTO EXPRESIONES (CODIGO, NOMBRE, SQL_BLOQUE, ACTIVO, NATIVO)
                           VALUES ('ANTE_SAL', 'SALARIO ANTERIORES PERIODOS EN EL MISMO MES - SA027', '-- SUMATORIA RUBROS DEL MISMO MES
EXECUTE BLOCK
RETURNS (
    Empleado CHAR(15),
    Valor    NUMERIC(17,4))
AS
DECLARE V_Nomina CHAR(5);
DECLARE V_Fecha_Nom DATE;
DECLARE V_Desde_Nom DATE;
DECLARE V_Hasta_Nom DATE;
DECLARE V_Liq CHAR(10);
DECLARE V_Rubro1 CHAR(5) = ''SA027'';
DECLARE V_Rubro2 CHAR(5) = '''';
DECLARE V_Rubro3 CHAR(5) = '''';

BEGIN
  SELECT Rdb$Get_Context(''USER_SESSION'', ''G_NOMINA'')
  FROM Rdb$Database
  INTO V_Nomina;

  SELECT Fecha,
         Desde,
         Hasta,
         Codliquidacion
  FROM Nominas
  WHERE Codigo = :V_Nomina
  INTO V_Fecha_Nom,
       V_Desde_Nom,
       V_Hasta_Nom,
       V_Liq;

  FOR SELECT  Codpersonal
    FROM TMP_EMPLEADOS
      INTO Empleado
  DO
  BEGIN

    SELECT SUM(S.Adicion + S.Deduccion)
    FROM Planillas S
    JOIN Nominas N ON (S.Codnomina = N.Codigo)
    WHERE EXTRACT(MONTH FROM N.Fecha) = EXTRACT(MONTH FROM :V_Fecha_Nom)
          AND EXTRACT(YEAR FROM N.Fecha) = EXTRACT(YEAR FROM :V_Fecha_Nom)
          AND S.Codrubro IN (:V_Rubro1, :V_Rubro2, :V_Rubro3)
          AND S.Codpersonal = :Empleado
          AND N.Fecha < :V_Fecha_Nom
    INTO Valor;

    Valor = COALESCE(Valor, 0);
    SUSPEND;
  END
END', 'S', 'S')
                         ;
UPDATE OR INSERT INTO EXPRESIONES (CODIGO, NOMBRE, SQL_BLOQUE, ACTIVO, NATIVO)
                           VALUES ('ANTE_SP', 'DESCUENTO SOLIDARIDAD PENSIONAL REALIZADO EN PERIODOS ANTERIORES EN EL MISMO MES', '-- SUMATORIA RUBROS DEL MISMO MES
EXECUTE BLOCK
RETURNS (
    Empleado CHAR(15),
    Valor    NUMERIC(17,4))
AS
DECLARE V_Nomina    CHAR(5);
DECLARE V_Fecha_Nom DATE;
DECLARE V_Desde_Nom DATE;
DECLARE V_Hasta_Nom DATE;
DECLARE V_Liq       CHAR(10);
DECLARE V_Rubro1    CHAR(5) = ''DT035'';
DECLARE V_Rubro2    CHAR(5) = ''VA035'';
DECLARE V_Rubro3    CHAR(5) = '''';

BEGIN
  SELECT Rdb$Get_Context(''USER_SESSION'', ''G_NOMINA'')
  FROM Rdb$Database
  INTO V_Nomina;

  SELECT Fecha,
         Desde,
         Hasta,
         Codliquidacion
  FROM Nominas
  WHERE Codigo = :V_Nomina
  INTO V_Fecha_Nom, V_Desde_Nom, V_Hasta_Nom, V_Liq;
  
  FOR SELECT  Codpersonal
    FROM TMP_EMPLEADOS
    INTO Empleado 
  DO
  BEGIN
    SELECT SUM(S.Adicion + S.Deduccion)
    FROM Planillas S
    JOIN Nominas N ON (S.Codnomina = N.Codigo)
    WHERE EXTRACT(MONTH FROM N.Fecha) = EXTRACT(MONTH FROM :V_Fecha_Nom)
          AND EXTRACT(YEAR FROM N.Fecha) = EXTRACT(YEAR FROM :V_Fecha_Nom)
          AND S.Codrubro IN (:V_Rubro1, :V_Rubro2, :V_Rubro3)
          AND S.Codpersonal = :Empleado  AND
          N.Fecha < :V_Fecha_Nom
    INTO Valor;

    Valor = COALESCE(Valor, 0);
    SUSPEND;
  END
END', 'S', 'S')
                         ;
UPDATE OR INSERT INTO EXPRESIONES (CODIGO, NOMBRE, SQL_BLOQUE, ACTIVO, NATIVO)
                           VALUES ('ANTE_SLN', 'DIAS EN EL MES DE QUINCENA ANTERIORES DE SLN - DI001 - DI004 - DI012', '-- SUMATORIA RUBROS DEL MISMO MES
EXECUTE BLOCK
RETURNS (
    Empleado CHAR(15),
    Valor NUMERIC(17,4))
AS
DECLARE V_Nomina    CHAR(5);
DECLARE V_Fecha_Nom DATE;
DECLARE V_Desde_Nom DATE;
DECLARE V_Hasta_Nom DATE;
DECLARE V_Liq       CHAR(10);
DECLARE V_Rubro1    CHAR(5) = ''DI001'';
DECLARE V_Rubro2    CHAR(5) = ''DI004'';
DECLARE V_Rubro3    CHAR(5) = ''DI012'';

BEGIN
  SELECT Rdb$Get_Context(''USER_SESSION'', ''G_NOMINA'')
  FROM Rdb$Database
  INTO V_Nomina;

  SELECT Fecha, Desde, Hasta, Codliquidacion
  FROM Nominas
  WHERE Codigo = :V_Nomina
  INTO V_Fecha_Nom, V_Desde_Nom, V_Hasta_Nom, V_Liq;

  FOR SELECT  Codpersonal
    FROM TMP_EMPLEADOS
    INTO Empleado    
  DO
  BEGIN

    SELECT SUM(S.Adicion + S.Deduccion)
    FROM Planillas S
    JOIN Nominas N ON (S.Codnomina = N.Codigo)
    WHERE EXTRACT(MONTH FROM N.Fecha) = EXTRACT(MONTH FROM :V_Fecha_Nom) AND
          EXTRACT(YEAR FROM N.Fecha) = EXTRACT(YEAR FROM :V_Fecha_Nom) AND
          S.Codrubro IN (:V_Rubro1, :V_Rubro2, :V_Rubro3) AND
          S.Codpersonal = :Empleado AND
          N.Fecha < :V_Fecha_Nom
    INTO Valor;

    Valor = COALESCE(Valor, 0);
    SUSPEND;
  END
END', 'S', 'S')
                         ;
UPDATE OR INSERT INTO EXPRESIONES (CODIGO, NOMBRE, SQL_BLOQUE, ACTIVO, NATIVO)
                           VALUES ('QUINCENA', 'NUMERO DE QUNCENA (1 o 2)', '-- QUINCENA (1 o 2)
EXECUTE BLOCK
RETURNS (
    Empleado CHAR(15),
    Valor    INTEGER)
AS
DECLARE V_Nomina     CHAR(5);
DECLARE V_Desde_Nom  DATE;
DECLARE V_Fecha_Ret  DATE;
DECLARE V_Fecha_Ing  DATE;
DECLARE V_Hasta_Nom  DATE;
DECLARE V_Liq        CHAR(10);
DECLARE V_Coti       CHAR(1);
DECLARE V_Dias       INTEGER;
DECLARE V_Valor      INTEGER;
DECLARE V_Empleado   CHAR(15);
DECLARE V_Periodo    CHAR(5);
DECLARE V_Ultimo_Dia INTEGER;

BEGIN
  SELECT Rdb$Get_Context(''USER_SESSION'', ''G_NOMINA'')
  FROM Rdb$Database
  INTO V_Nomina;

  SELECT Desde,
         Hasta,
         Codliquidacion,
         Dias
  FROM Nominas
  WHERE Codigo = :V_Nomina
  INTO V_Desde_Nom,
       V_Hasta_Nom,
       V_Liq,
       V_Dias;
  SELECT Cotidiana
  FROM Liquidaciones
  WHERE Codigo = :V_Liq
  INTO :V_Coti;

  SELECT FIRST 1 Codpersonal
  FROM Tmp_Empleados
  INTO V_Empleado;

  SELECT Codtipoperiodo
  FROM Personal
  WHERE Codigo = :V_Empleado
  INTO V_Periodo;

  SELECT EXTRACT(DAY FROM DATEADD(MONTH, 1, CAST(''01.'' || EXTRACT(MONTH FROM :V_Hasta_Nom) || ''.'' || EXTRACT(YEAR FROM :V_Hasta_Nom) AS DATE)) - 1)
  FROM Nominas
  WHERE Codigo = :V_Nomina
  INTO V_Ultimo_Dia;

  IF (V_Coti = ''S'') THEN
    V_Valor = CASE :V_Periodo
                WHEN ''5'' THEN 2
                WHEN ''4'' THEN IIF(EXTRACT(DAY FROM V_Desde_Nom) > 15, 2, 1)
                ELSE IIF(EXTRACT(DAY FROM V_Hasta_Nom) = V_Ultimo_Dia, 2, 1)
              END;
  ELSE
    V_Valor = 2;
  Valor = V_Valor;
  FOR SELECT Codpersonal
      FROM Tmp_Empleados
      INTO Empleado
  DO
  BEGIN
    SELECT MAX(S.Hasta)
    FROM Salarios S
    WHERE S.Codpersonal = :Empleado
          AND S.Columna_Hasta = ''RETIRO''
    INTO V_Fecha_Ret;

    SELECT MAX(S.Desde)
    FROM Salarios S
    WHERE S.Codpersonal = :Empleado
          AND S.Columna_Desde = ''INGRESO''
    INTO V_Fecha_Ing;

    IF (V_Fecha_Ret IS NOT NULL AND
        V_Fecha_Ret BETWEEN V_Desde_Nom AND V_Hasta_Nom AND
        V_Fecha_Ing < COALESCE(V_Fecha_Ret, V_Fecha_Ing)) THEN
      Valor = 2;
    SUSPEND;
    Valor = V_Valor;
  END
END', 'S', 'S')
                         ;
UPDATE OR INSERT INTO EXPRESIONES (CODIGO, NOMBRE, SQL_BLOQUE, ACTIVO, NATIVO)
                           VALUES ('ANTE_OS', 'VALORES OTROS CONCEPTOS SALARIABLES ANTERIORES EN EL MISMO MES - HE038 - DV997', '-- SUMATORIA RUBROS DEL MISMO MES
EXECUTE BLOCK
RETURNS (
    Empleado CHAR(15),
    Valor NUMERIC(17,4))
AS
DECLARE V_Nomina    CHAR(5);
DECLARE V_Fecha_Nom DATE;
DECLARE V_Desde_Nom DATE;
DECLARE V_Hasta_Nom DATE;
DECLARE V_Liq       CHAR(10);
DECLARE V_Rubro1    CHAR(5) = ''HE038'';
DECLARE V_Rubro2    CHAR(5) = ''DV997'';
DECLARE V_Rubro3    CHAR(5) = ''DV300'';

BEGIN
  SELECT Rdb$Get_Context(''USER_SESSION'', ''G_NOMINA'')
  FROM Rdb$Database
  INTO V_Nomina;

  SELECT Fecha, Desde, Hasta, Codliquidacion
  FROM Nominas
  WHERE Codigo = :V_Nomina
  INTO V_Fecha_Nom, V_Desde_Nom, V_Hasta_Nom, V_Liq;

  FOR SELECT  Codpersonal
    FROM TMP_EMPLEADOS
    INTO Empleado    
  DO
  BEGIN

    SELECT SUM(S.Adicion + S.Deduccion)
    FROM Planillas S
    JOIN Nominas N ON (S.Codnomina = N.Codigo)
    WHERE EXTRACT(MONTH FROM N.Fecha) = EXTRACT(MONTH FROM :V_Fecha_Nom) AND
          EXTRACT(YEAR FROM N.Fecha) = EXTRACT(YEAR FROM :V_Fecha_Nom) AND
          S.Codrubro IN (:V_Rubro1, :V_Rubro2, :V_Rubro3) AND
          S.Codpersonal = :Empleado AND
          N.Fecha < :V_Fecha_Nom
    INTO Valor;

    Valor = COALESCE(Valor, 0);
    SUSPEND;
  END
END', 'S', 'S')
                         ;

UPDATE OR INSERT INTO EXPRESIONES (CODIGO, NOMBRE, SQL_BLOQUE, ACTIVO, NATIVO)
                           VALUES ('ANTE_DIAS', 'DIAS OTROS PARAFISCALES QUINCENA ANTERIOR - SS013', '-- SUMATORIA RUBROS DEL MISMO MES DEL ULTIMO INGRESO EN ADELANTE
EXECUTE BLOCK
RETURNS (
    Empleado CHAR(15),
    Valor NUMERIC(17,4))
AS
DECLARE V_Nomina CHAR(5);
DECLARE V_Fecha_Nom DATE;
DECLARE V_Desde_Nom DATE;
DECLARE V_Hasta_Nom DATE;
DECLARE V_Fecha_Ing DATE;
DECLARE V_Liq CHAR(10);
DECLARE V_Dias INTEGER;
DECLARE V_Rubro1 CHAR(5) = ''SS013'';
DECLARE V_Rubro2 CHAR(5) = '''';
DECLARE V_Rubro3 CHAR(5) = '''';

BEGIN
  SELECT Rdb$Get_Context(''USER_SESSION'', ''G_NOMINA'')
  FROM Rdb$Database
  INTO V_Nomina;

  SELECT Fecha,
         Desde,
         Hasta,
         Codliquidacion,
         Dias
  FROM Nominas
  WHERE Codigo = :V_Nomina
  INTO V_Fecha_Nom, V_Desde_Nom, V_Hasta_Nom, V_Liq, V_Dias;

  FOR SELECT  Codpersonal
    FROM TMP_EMPLEADOS
    INTO Empleado    
  DO
  BEGIN
    IF (V_Dias = 30) THEN
      Valor = 0;
    ELSE
    BEGIN
      SELECT MAX(S.Desde)
      FROM Salarios S
      WHERE S.Codpersonal = :Empleado AND
            S.Columna_Desde = ''INGRESO''
      INTO V_Fecha_Ing;

      SELECT SUM(S.Adicion + S.Deduccion)
      FROM Planillas S
      JOIN Nominas N ON (S.Codnomina = N.Codigo)
      WHERE EXTRACT(MONTH FROM N.Fecha) = EXTRACT(MONTH FROM :V_Fecha_Nom) AND
            EXTRACT(YEAR FROM N.Fecha) = EXTRACT(YEAR FROM :V_Fecha_Nom) AND
            S.Codrubro IN (:V_Rubro1, :V_Rubro2, :V_Rubro3) AND
            S.Codpersonal = :Empleado AND
            N.Fecha < :V_Fecha_Nom AND
            N.Hasta >= :V_Fecha_Ing
      INTO Valor;
      Valor = COALESCE(Valor, 0);
    END
    SUSPEND;
  END
END', 'S', 'S')
                         ;
UPDATE OR INSERT INTO EXPRESIONES (CODIGO, NOMBRE, SQL_BLOQUE, ACTIVO, NATIVO)
                           VALUES ('ANTE_DIASE', 'DIAS OTROS EPS QUINCENA ANTERIOR PARA APORTE - SS012', '-- SUMATORIA RUBROS DEL MISMO MES DEL ULTIMO INGRESO EN ADELANTE
EXECUTE BLOCK
RETURNS (
    Empleado CHAR(15),
    Valor NUMERIC(17,4))
AS
DECLARE V_Nomina CHAR(5);
DECLARE V_Fecha_Nom DATE;
DECLARE V_Desde_Nom DATE;
DECLARE V_Hasta_Nom DATE;
DECLARE V_Fecha_Ing DATE;
DECLARE V_Liq CHAR(10);
DECLARE V_Dias INTEGER;
DECLARE V_Rubro1 CHAR(5) = ''SS012'';
DECLARE V_Rubro2 CHAR(5) = '''';
DECLARE V_Rubro3 CHAR(5) = '''';

BEGIN
  SELECT Rdb$Get_Context(''USER_SESSION'', ''G_NOMINA'')
  FROM Rdb$Database
  INTO V_Nomina;

  SELECT Fecha,
         Desde,
         Hasta,
         Codliquidacion,
         Dias
  FROM Nominas
  WHERE Codigo = :V_Nomina
  INTO V_Fecha_Nom, V_Desde_Nom, V_Hasta_Nom, V_Liq, V_Dias;

  FOR SELECT  Codpersonal
    FROM TMP_EMPLEADOS
    INTO Empleado    
  DO
  BEGIN
    IF (V_Dias = 30) THEN
      Valor = 0;
    ELSE
    BEGIN
      SELECT MAX(S.Desde)
      FROM Salarios S
      WHERE S.Codpersonal = :Empleado AND
            S.Columna_Desde = ''INGRESO''
      INTO V_Fecha_Ing;

      SELECT SUM(S.Adicion + S.Deduccion)
      FROM Planillas S
      JOIN Nominas N ON (S.Codnomina = N.Codigo)
      WHERE EXTRACT(MONTH FROM N.Fecha) = EXTRACT(MONTH FROM :V_Fecha_Nom) AND
            EXTRACT(YEAR FROM N.Fecha) = EXTRACT(YEAR FROM :V_Fecha_Nom) AND
            S.Codrubro IN (:V_Rubro1, :V_Rubro2, :V_Rubro3) AND
            S.Codpersonal = :Empleado AND
            N.Fecha < :V_Fecha_Nom AND
            N.Hasta >= :V_Fecha_Ing
      INTO Valor;
      Valor = COALESCE(Valor, 0);
    END
    SUSPEND;
  END
END', 'S', 'S')
                         ;
UPDATE OR INSERT INTO EXPRESIONES (CODIGO, NOMBRE, SQL_BLOQUE, ACTIVO, NATIVO)
                           VALUES ('SAL_BASICO', 'SALARIO BASICO PROMEDIO', '--BASICO PRORRATEADO IBC

SELECT Empleado, Valor
FROM Pz_Ex_Sal_Basico', 'S', 'S')
                         ;
UPDATE OR INSERT INTO EXPRESIONES (CODIGO, NOMBRE, SQL_BLOQUE, ACTIVO, NATIVO)
                           VALUES ('ANTE_DARL', 'DIAS ARL PERIODO ANTERIOR EN EL MISMO MES - SS010', '-- SUMATORIA RUBROS DEL MISMO MES DEL ULTIMO INGRESO EN ADELANTE
EXECUTE BLOCK
RETURNS (
    Empleado CHAR(15),
    Valor NUMERIC(17,4))
AS
DECLARE V_Nomina CHAR(5);
DECLARE V_Fecha_Nom DATE;
DECLARE V_Desde_Nom DATE;
DECLARE V_Hasta_Nom DATE;
DECLARE V_Fecha_Ing DATE;
DECLARE V_Liq CHAR(10);
DECLARE V_Dias INTEGER;
DECLARE V_Rubro1 CHAR(5) = ''SS010'';
DECLARE V_Rubro2 CHAR(5) = '''';
DECLARE V_Rubro3 CHAR(5) = '''';

BEGIN
  SELECT Rdb$Get_Context(''USER_SESSION'', ''G_NOMINA'')
  FROM Rdb$Database
  INTO V_Nomina;

  SELECT Fecha,
         Desde,
         Hasta,
         Codliquidacion,
         Dias
  FROM Nominas
  WHERE Codigo = :V_Nomina
  INTO V_Fecha_Nom, V_Desde_Nom, V_Hasta_Nom, V_Liq, V_Dias;

  FOR SELECT  Codpersonal
    FROM TMP_EMPLEADOS
    INTO Empleado    
  DO
  BEGIN
    IF (V_Dias = 30) THEN
      Valor = 0;
    ELSE
    BEGIN
      SELECT MAX(S.Desde)
      FROM Salarios S
      WHERE S.Codpersonal = :Empleado AND
            S.Columna_Desde = ''INGRESO''
      INTO V_Fecha_Ing;

      SELECT SUM(S.Adicion + S.Deduccion)
      FROM Planillas S
      JOIN Nominas N ON (S.Codnomina = N.Codigo)
      WHERE EXTRACT(MONTH FROM N.Fecha) = EXTRACT(MONTH FROM :V_Fecha_Nom) AND
            EXTRACT(YEAR FROM N.Fecha) = EXTRACT(YEAR FROM :V_Fecha_Nom) AND
            S.Codrubro IN (:V_Rubro1, :V_Rubro2, :V_Rubro3) AND
            S.Codpersonal = :Empleado AND
            N.Fecha < :V_Fecha_Nom AND
            N.Hasta >= :V_Fecha_Ing
      INTO Valor;
      Valor = COALESCE(Valor, 0);
    END
    SUSPEND;
  END
END', 'S', 'S')
                         ;
UPDATE OR INSERT INTO EXPRESIONES (CODIGO, NOMBRE, SQL_BLOQUE, ACTIVO, NATIVO)
                           VALUES ('ANTE_DMT', 'DIAS DI021 EMPLEADOS TIPO 51 ? 68 PERIODO ANTERIOR MISMO MES - SS022', '-- SUMATORIA RUBROS DEL MISMO MES DEL ULTIMO INGRESO EN ADELANTE
EXECUTE BLOCK
RETURNS (
    Empleado CHAR(15),
    Valor NUMERIC(17,4))
AS
DECLARE V_Nomina CHAR(5);
DECLARE V_Fecha_Nom DATE;
DECLARE V_Desde_Nom DATE;
DECLARE V_Hasta_Nom DATE;
DECLARE V_Fecha_Ing DATE;
DECLARE V_Liq CHAR(10);
DECLARE V_Dias INTEGER;
DECLARE V_Rubro1 CHAR(5) = ''DI022'';
DECLARE V_Rubro2 CHAR(5) = '''';
DECLARE V_Rubro3 CHAR(5) = '''';

BEGIN
  SELECT Rdb$Get_Context(''USER_SESSION'', ''G_NOMINA'')
  FROM Rdb$Database
  INTO V_Nomina;

  SELECT Fecha,
         Desde,
         Hasta,
         Codliquidacion,
         Dias
  FROM Nominas
  WHERE Codigo = :V_Nomina
  INTO V_Fecha_Nom, V_Desde_Nom, V_Hasta_Nom, V_Liq, V_Dias;

  FOR SELECT  Codpersonal
    FROM TMP_EMPLEADOS
    INTO Empleado    
  DO
  BEGIN
    IF (V_Dias = 30) THEN
      Valor = 0;
    ELSE
    BEGIN
      SELECT MAX(S.Desde)
      FROM Salarios S
      WHERE S.Codpersonal = :Empleado AND
            S.Columna_Desde = ''INGRESO''
      INTO V_Fecha_Ing;

      SELECT SUM(S.Adicion + S.Deduccion)
      FROM Planillas S
      JOIN Nominas N ON (S.Codnomina = N.Codigo)
      WHERE EXTRACT(MONTH FROM N.Fecha) = EXTRACT(MONTH FROM :V_Fecha_Nom) AND
            EXTRACT(YEAR FROM N.Fecha) = EXTRACT(YEAR FROM :V_Fecha_Nom) AND
            S.Codrubro IN (:V_Rubro1, :V_Rubro2, :V_Rubro3) AND
            S.Codpersonal = :Empleado AND
            N.Fecha < :V_Fecha_Nom AND
            N.Hasta >= :V_Fecha_Ing
      INTO Valor;
      Valor = COALESCE(Valor, 0);
    END
    SUSPEND;
  END
END', 'S', 'S')
                         ;
UPDATE OR INSERT INTO EXPRESIONES (CODIGO, NOMBRE, SQL_BLOQUE, ACTIVO, NATIVO)
                           VALUES ('ANTE_SIEG', 'SALARIO ANTERIORES DE INCAPACIDAD AL 66% MISMO MES - SA009 - SA020', '-- SUMATORIA RUBROS DEL MISMO MES
EXECUTE BLOCK
RETURNS (
    Empleado CHAR(15),
    Valor    NUMERIC(17,4))
AS
DECLARE V_Nomina CHAR(5);
DECLARE V_Fecha_Nom DATE;
DECLARE V_Desde_Nom DATE;
DECLARE V_Hasta_Nom DATE;
DECLARE V_Liq CHAR(10);
DECLARE V_Rubro1 CHAR(5) = ''SA009'';
DECLARE V_Rubro2 CHAR(5) = ''SA020'';
DECLARE V_Rubro3 CHAR(5) = '''';

BEGIN
  SELECT Rdb$Get_Context(''USER_SESSION'', ''G_NOMINA'')
  FROM Rdb$Database
  INTO V_Nomina;

  SELECT Fecha,
         Desde,
         Hasta,
         Codliquidacion
  FROM Nominas
  WHERE Codigo = :V_Nomina
  INTO V_Fecha_Nom,
       V_Desde_Nom,
       V_Hasta_Nom,
       V_Liq;

  FOR SELECT  Codpersonal
    FROM TMP_EMPLEADOS
      INTO Empleado
  DO
  BEGIN

    SELECT SUM(S.Adicion + S.Deduccion)
    FROM Planillas S
    JOIN Nominas N ON (S.Codnomina = N.Codigo)
    WHERE EXTRACT(MONTH FROM N.Fecha) = EXTRACT(MONTH FROM :V_Fecha_Nom)
          AND EXTRACT(YEAR FROM N.Fecha) = EXTRACT(YEAR FROM :V_Fecha_Nom)
          AND S.Codrubro IN (:V_Rubro1, :V_Rubro2, :V_Rubro3)
          AND S.Codpersonal = :Empleado
          AND N.Fecha < :V_Fecha_Nom
    INTO Valor;

    Valor = COALESCE(Valor, 0);
    SUSPEND;
  END
END', 'S', 'S')
                         ;
UPDATE OR INSERT INTO EXPRESIONES (CODIGO, NOMBRE, SQL_BLOQUE, ACTIVO, NATIVO)
                           VALUES ('ANTE_DCCF', 'DIAS OTROS CCF PERIODO ANTERIOR PARA APORTE - SS011', '-- SUMATORIA RUBROS DEL MISMO MES DEL ULTIMO INGRESO EN ADELANTE
EXECUTE BLOCK
RETURNS (
    Empleado CHAR(15),
    Valor NUMERIC(17,4))
AS
DECLARE V_Nomina CHAR(5);
DECLARE V_Fecha_Nom DATE;
DECLARE V_Desde_Nom DATE;
DECLARE V_Hasta_Nom DATE;
DECLARE V_Fecha_Ing DATE;
DECLARE V_Liq CHAR(10);
DECLARE V_Dias INTEGER;
DECLARE V_Rubro1 CHAR(5) = ''SS011'';
DECLARE V_Rubro2 CHAR(5) = '''';
DECLARE V_Rubro3 CHAR(5) = '''';

BEGIN
  SELECT Rdb$Get_Context(''USER_SESSION'', ''G_NOMINA'')
  FROM Rdb$Database
  INTO V_Nomina;

  SELECT Fecha,
         Desde,
         Hasta,
         Codliquidacion,
         Dias
  FROM Nominas
  WHERE Codigo = :V_Nomina
  INTO V_Fecha_Nom, V_Desde_Nom, V_Hasta_Nom, V_Liq, V_Dias;

  FOR SELECT  Codpersonal
    FROM TMP_EMPLEADOS
    INTO Empleado    
  DO
  BEGIN
    IF (V_Dias = 30) THEN
      Valor = 0;
    ELSE
    BEGIN
      SELECT MAX(S.Desde)
      FROM Salarios S
      WHERE S.Codpersonal = :Empleado AND
            S.Columna_Desde = ''INGRESO''
      INTO V_Fecha_Ing;

      SELECT SUM(S.Adicion + S.Deduccion)
      FROM Planillas S
      JOIN Nominas N ON (S.Codnomina = N.Codigo)
      WHERE EXTRACT(MONTH FROM N.Fecha) = EXTRACT(MONTH FROM :V_Fecha_Nom) AND
            EXTRACT(YEAR FROM N.Fecha) = EXTRACT(YEAR FROM :V_Fecha_Nom) AND
            S.Codrubro IN (:V_Rubro1, :V_Rubro2, :V_Rubro3) AND
            S.Codpersonal = :Empleado AND
            N.Fecha < :V_Fecha_Nom AND
            N.Hasta >= :V_Fecha_Ing
      INTO Valor;
      Valor = COALESCE(Valor, 0);
    END
    SUSPEND;
  END
END', 'S', 'S')
                         ;
UPDATE OR INSERT INTO EXPRESIONES (CODIGO, NOMBRE, SQL_BLOQUE, ACTIVO, NATIVO)
                           VALUES ('ANTE_VA', 'VALOR VACACIONES EN DINERO ANTERIORES EN EL MISMO MES - DV040', '-- SUMATORIA RUBROS DEL MISMO MES
EXECUTE BLOCK
RETURNS (
    Empleado CHAR(15),
    Valor NUMERIC(17,4))
AS
DECLARE V_Nomina    CHAR(5);
DECLARE V_Fecha_Nom DATE;
DECLARE V_Desde_Nom DATE;
DECLARE V_Hasta_Nom DATE;
DECLARE V_Liq       CHAR(10);
DECLARE V_Rubro1    CHAR(5) = ''DV040'';
DECLARE V_Rubro2    CHAR(5) = '''';
DECLARE V_Rubro3    CHAR(5) = '''';

BEGIN
  SELECT Rdb$Get_Context(''USER_SESSION'', ''G_NOMINA'')
  FROM Rdb$Database
  INTO V_Nomina;

  SELECT Fecha, Desde, Hasta, Codliquidacion
  FROM Nominas
  WHERE Codigo = :V_Nomina
  INTO V_Fecha_Nom, V_Desde_Nom, V_Hasta_Nom, V_Liq;

  FOR SELECT  Codpersonal
    FROM TMP_EMPLEADOS
    INTO Empleado    
  DO
  BEGIN

    SELECT SUM(S.Adicion + S.Deduccion)
    FROM Planillas S
    JOIN Nominas N ON (S.Codnomina = N.Codigo)
    WHERE EXTRACT(MONTH FROM N.Fecha) = EXTRACT(MONTH FROM :V_Fecha_Nom) AND
          EXTRACT(YEAR FROM N.Fecha) = EXTRACT(YEAR FROM :V_Fecha_Nom) AND
          S.Codrubro IN (:V_Rubro1, :V_Rubro2, :V_Rubro3) AND
          S.Codpersonal = :Empleado AND
          N.Fecha < :V_Fecha_Nom
    INTO Valor;

    Valor = COALESCE(Valor, 0);
    SUSPEND;
  END
END', 'S', 'S')
                         ;
UPDATE OR INSERT INTO EXPRESIONES (CODIGO, NOMBRE, SQL_BLOQUE, ACTIVO, NATIVO)
                           VALUES ('ANTE_DAFP', 'DIAS OTROS AFP PERIODO ANTERIOR PARA APORTE - SS014', '-- SUMATORIA RUBROS DEL MISMO MES DEL ULTIMO INGRESO EN ADELANTE
EXECUTE BLOCK
RETURNS (
    Empleado CHAR(15),
    Valor NUMERIC(17,4))
AS
DECLARE V_Nomina CHAR(5);
DECLARE V_Fecha_Nom DATE;
DECLARE V_Desde_Nom DATE;
DECLARE V_Hasta_Nom DATE;
DECLARE V_Fecha_Ing DATE;
DECLARE V_Liq CHAR(10);
DECLARE V_Dias INTEGER;
DECLARE V_Rubro1 CHAR(5) = ''SS014'';
DECLARE V_Rubro2 CHAR(5) = '''';
DECLARE V_Rubro3 CHAR(5) = '''';

BEGIN
  SELECT Rdb$Get_Context(''USER_SESSION'', ''G_NOMINA'')
  FROM Rdb$Database
  INTO V_Nomina;

  SELECT Fecha,
         Desde,
         Hasta,
         Codliquidacion,
         Dias
  FROM Nominas
  WHERE Codigo = :V_Nomina
  INTO V_Fecha_Nom, V_Desde_Nom, V_Hasta_Nom, V_Liq, V_Dias;

  FOR SELECT  Codpersonal
    FROM TMP_EMPLEADOS
    INTO Empleado    
  DO
  BEGIN
    IF (V_Dias = 30) THEN
      Valor = 0;
    ELSE
    BEGIN
      SELECT MAX(S.Desde)
      FROM Salarios S
      WHERE S.Codpersonal = :Empleado AND
            S.Columna_Desde = ''INGRESO''
      INTO V_Fecha_Ing;

      SELECT SUM(S.Adicion + S.Deduccion)
      FROM Planillas S
      JOIN Nominas N ON (S.Codnomina = N.Codigo)
      WHERE EXTRACT(MONTH FROM N.Fecha) = EXTRACT(MONTH FROM :V_Fecha_Nom) AND
            EXTRACT(YEAR FROM N.Fecha) = EXTRACT(YEAR FROM :V_Fecha_Nom) AND
            S.Codrubro IN (:V_Rubro1, :V_Rubro2, :V_Rubro3) AND
            S.Codpersonal = :Empleado AND
            N.Fecha < :V_Fecha_Nom AND
            N.Hasta >= :V_Fecha_Ing
      INTO Valor;
      Valor = COALESCE(Valor, 0);
    END
    SUSPEND;
  END
END', 'S', 'S')
                         ;
UPDATE OR INSERT INTO EXPRESIONES (CODIGO, NOMBRE, SQL_BLOQUE, ACTIVO, NATIVO)
                           VALUES ('ANTE_IEG', 'DIAS INCAPACIDAD AL 66% DI009 - DI021 MISMO PERIODO EN EL MES - DI009 - DI020', '-- SUMATORIA RUBROS DEL MISMO MES DEL ULTIMO INGRESO EN ADELANTE
EXECUTE BLOCK
RETURNS (
    Empleado CHAR(15),
    Valor NUMERIC(17,4))
AS
DECLARE V_Nomina CHAR(5);
DECLARE V_Fecha_Nom DATE;
DECLARE V_Desde_Nom DATE;
DECLARE V_Hasta_Nom DATE;
DECLARE V_Fecha_Ing DATE;
DECLARE V_Liq CHAR(10);
DECLARE V_Dias INTEGER;
DECLARE V_Rubro1 CHAR(5) = ''DI009'';
DECLARE V_Rubro2 CHAR(5) = ''DI020'';
DECLARE V_Rubro3 CHAR(5) = '''';

BEGIN
  SELECT Rdb$Get_Context(''USER_SESSION'', ''G_NOMINA'')
  FROM Rdb$Database
  INTO V_Nomina;

  SELECT Fecha,
         Desde,
         Hasta,
         Codliquidacion,
         Dias
  FROM Nominas
  WHERE Codigo = :V_Nomina
  INTO V_Fecha_Nom, V_Desde_Nom, V_Hasta_Nom, V_Liq, V_Dias;

  FOR SELECT  Codpersonal
    FROM TMP_EMPLEADOS
    INTO Empleado    
  DO
  BEGIN
    IF (V_Dias = 30) THEN
      Valor = 0;
    ELSE
    BEGIN
      SELECT MAX(S.Desde)
      FROM Salarios S
      WHERE S.Codpersonal = :Empleado AND
            S.Columna_Desde = ''INGRESO''
      INTO V_Fecha_Ing;

      SELECT SUM(S.Adicion + S.Deduccion)
      FROM Planillas S
      JOIN Nominas N ON (S.Codnomina = N.Codigo)
      WHERE EXTRACT(MONTH FROM N.Fecha) = EXTRACT(MONTH FROM :V_Fecha_Nom) AND
            EXTRACT(YEAR FROM N.Fecha) = EXTRACT(YEAR FROM :V_Fecha_Nom) AND
            S.Codrubro IN (:V_Rubro1, :V_Rubro2, :V_Rubro3) AND
            S.Codpersonal = :Empleado AND
            N.Fecha < :V_Fecha_Nom AND
            N.Hasta >= :V_Fecha_Ing
      INTO Valor;
      Valor = COALESCE(Valor, 0);
    END
    SUSPEND;
  END
END', 'S', 'S')
                         ;
UPDATE OR INSERT INTO EXPRESIONES (CODIGO, NOMBRE, SQL_BLOQUE, ACTIVO, NATIVO)
                           VALUES ('ANTE_DLA', 'DIAS LABORADOS PERIODO ANTERIOR EN EL MISMO MES - DI005', '-- SUMATORIA RUBROS DEL MISMO MES DEL ULTIMO INGRESO EN ADELANTE
EXECUTE BLOCK
RETURNS (
    Empleado CHAR(15),
    Valor NUMERIC(17,4))
AS
DECLARE V_Nomina CHAR(5);
DECLARE V_Fecha_Nom DATE;
DECLARE V_Desde_Nom DATE;
DECLARE V_Hasta_Nom DATE;
DECLARE V_Fecha_Ing DATE;
DECLARE V_Liq CHAR(10);
DECLARE V_Dias INTEGER;
DECLARE V_Rubro1 CHAR(5) = ''DI005'';
DECLARE V_Rubro2 CHAR(5) = '''';
DECLARE V_Rubro3 CHAR(5) = '''';

BEGIN
  SELECT Rdb$Get_Context(''USER_SESSION'', ''G_NOMINA'')
  FROM Rdb$Database
  INTO V_Nomina;

  SELECT Fecha,
         Desde,
         Hasta,
         Codliquidacion,
         Dias
  FROM Nominas
  WHERE Codigo = :V_Nomina
  INTO V_Fecha_Nom, V_Desde_Nom, V_Hasta_Nom, V_Liq, V_Dias;

  FOR SELECT  Codpersonal
    FROM TMP_EMPLEADOS
    INTO Empleado    
  DO
  BEGIN
    IF (V_Dias = 30) THEN
      Valor = 0;
    ELSE
    BEGIN
      SELECT MAX(S.Desde)
      FROM Salarios S
      WHERE S.Codpersonal = :Empleado AND
            S.Columna_Desde = ''INGRESO''
      INTO V_Fecha_Ing;

      SELECT SUM(S.Adicion + S.Deduccion)
      FROM Planillas S
      JOIN Nominas N ON (S.Codnomina = N.Codigo)
      WHERE EXTRACT(MONTH FROM N.Fecha) = EXTRACT(MONTH FROM :V_Fecha_Nom) AND
            EXTRACT(YEAR FROM N.Fecha) = EXTRACT(YEAR FROM :V_Fecha_Nom) AND
            S.Codrubro IN (:V_Rubro1, :V_Rubro2, :V_Rubro3) AND
            S.Codpersonal = :Empleado AND
            N.Fecha < :V_Fecha_Nom AND
            N.Hasta >= :V_Fecha_Ing
      INTO Valor;
      Valor = COALESCE(Valor, 0);
    END
    SUSPEND;
  END
END', 'S', 'S')
                         ;
UPDATE OR INSERT INTO EXPRESIONES (CODIGO, NOMBRE, SQL_BLOQUE, ACTIVO, NATIVO)
                           VALUES ('ANTE_LIC', 'DIAS LICENCIAS REMUNERADAS PERIODO ANTERIOR EN EL MISMO MES - DI008-DI021-DI023', '-- SUMATORIA RUBROS DEL MISMO MES DEL ULTIMO INGRESO EN ADELANTE
EXECUTE BLOCK
RETURNS (
    Empleado CHAR(15),
    Valor NUMERIC(17,4))
AS
DECLARE V_Nomina CHAR(5);
DECLARE V_Fecha_Nom DATE;
DECLARE V_Desde_Nom DATE;
DECLARE V_Hasta_Nom DATE;
DECLARE V_Fecha_Ing DATE;
DECLARE V_Liq CHAR(10);
DECLARE V_Dias INTEGER;
DECLARE V_Rubro1 CHAR(5) = ''DI008'';
DECLARE V_Rubro2 CHAR(5) = ''DI021'';
DECLARE V_Rubro3 CHAR(5) = ''DI023'';

BEGIN
  SELECT Rdb$Get_Context(''USER_SESSION'', ''G_NOMINA'')
  FROM Rdb$Database
  INTO V_Nomina;

  SELECT Fecha,
         Desde,
         Hasta,
         Codliquidacion,
         Dias
  FROM Nominas
  WHERE Codigo = :V_Nomina
  INTO V_Fecha_Nom, V_Desde_Nom, V_Hasta_Nom, V_Liq, V_Dias;

  FOR SELECT  Codpersonal
    FROM TMP_EMPLEADOS
    INTO Empleado    
  DO
  BEGIN
    IF (V_Dias = 30) THEN
      Valor = 0;
    ELSE
    BEGIN
      SELECT MAX(S.Desde)
      FROM Salarios S
      WHERE S.Codpersonal = :Empleado AND
            S.Columna_Desde = ''INGRESO''
      INTO V_Fecha_Ing;

      SELECT SUM(S.Adicion + S.Deduccion)
      FROM Planillas S
      JOIN Nominas N ON (S.Codnomina = N.Codigo)
      WHERE EXTRACT(MONTH FROM N.Fecha) = EXTRACT(MONTH FROM :V_Fecha_Nom) AND
            EXTRACT(YEAR FROM N.Fecha) = EXTRACT(YEAR FROM :V_Fecha_Nom) AND
            S.Codrubro IN (:V_Rubro1, :V_Rubro2, :V_Rubro3) AND
            S.Codpersonal = :Empleado AND
            N.Fecha < :V_Fecha_Nom AND
            N.Hasta >= :V_Fecha_Ing
      INTO Valor;
      Valor = COALESCE(Valor, 0);
    END
    SUSPEND;
  END
END', 'S', 'S')
                         ;

COMMIT WORK;


/* SC_NOMINA_049 */

UPDATE RUBROS SET 
    FORMULA = 'IF((DIAS_TRABAJADOS-DIAS_YA_LQ=0) AND (LD073=0)) THEN RESULT:=0 ELSE 
IF(QUINCENA<2 OR ES_SENA=1) THEN RESULT:=0 ELSE
IF((DIAS_TRABAJADOS-DIAS_YA_LQ=0) AND (LD073>1)) THEN RESULT:=LD073 ELSE 
IF(ES_MEDT=1) THEN RESULT:=(SMLV/4)*SS009 ELSE 
IF(SAL_BASICO<SMLV) THEN RESULT:=ROUND(((SMLV/30)*(DIAS_TRABAJADOS-DIAS_YA_LQ+ANTE_DIAS-DI009-DI020-ANTE_IEG))+SA009+SA020+ANTE_SIEG) ELSE
IF((ES_INTEGRA=0) AND ((SAL_IBC+HE038+DV997+DV300+ANTE_OS)>(25*SMLV))) THEN RESULT:=ROUND((((25*SMLV)/30)*(DIAS_TRABAJADOS-DIAS_YA_LQ+ANTE_DIAS-DI009-DI020-ANTE_IEG))+SA009+SA020+ANTE_SIEG) ELSE
IF((ES_INTEGRA=1) AND (((SAL_IBC*C05_S_INT)+HE038+DV997+DV300+ANTE_OS)>(25*SMLV))) THEN RESULT:=ROUND((((25*SMLV)/30)*(DIAS_TRABAJADOS-DIAS_YA_LQ+ANTE_DIAS-DI009-DI020-ANTE_IEG))+SA009+SA020+ANTE_SIEG) ELSE
IF(ES_INTEGRA=1) THEN RESULT:=ROUND((((SAL_IBC*C05_S_INT)/30)*(DIAS_TRABAJADOS - DIAS_YA_LQ+ANTE_DIAS-DI009-DI020-ANTE_IEG))+SA009+SA020+ANTE_SIEG) ELSE
RESULT:=ROUND(((SAL_IBC/30)*(DIAS_TRABAJADOS-DIAS_YA_LQ+ANTE_DIAS-DI009-DI020-ANTE_IEG))+SA009+SA020+ANTE_SIEG)'
WHERE (CODIGO = 'SS016') AND
      (NATIVO = 'S');

UPDATE RUBROS SET 
    FORMULA = 'IF((DIAS_TRABAJADOS-DIAS_YA_LQ=0) AND (LD073=0)) THEN
  RESULT:=0
ELSE
IF(QUINCENA<2 OR ES_SENA=1) THEN
 RESULT:=0
ELSE
IF((DIAS_TRABAJADOS-DIAS_YA_LQ=0) AND (LD073>1)) THEN
  RESULT:=LD073
ELSE
IF(ES_MEDT=1) THEN
  RESULT:=(SMLV/4)*SS009
ELSE
IF(SAL_BASICO<SMLV) THEN 
   RESULT:=ROUND(((SMLV/30)*(SS011+ANTE_DCCF))+HE038+DV997+DV300+ANTE_OS+DV040+ANTE_VA+LD073)
ELSE
IF((ES_INTEGRA=0) AND ((SAL_IBC+HE038+DV997+DV300+ANTE_OS+DV040+ANTE_VA+LD073)>(25*SMLV))) THEN
   RESULT:=ROUND((25*SMLV)/30*(SS011+ANTE_DCCF))
ELSE
IF((ES_INTEGRA=1) AND (((SAL_IBC*C05_S_INT)+HE038+DV997+DV300+ANTE_OS+DV040+ANTE_VA+LD073)>(25*SMLV)))  THEN
   RESULT:=ROUND((25*SMLV)/30*(SS011+ANTE_DCCF))
ELSE
IF(ES_INTEGRA=1) THEN
   RESULT:=ROUND(((SAL_IBC*C05_S_INT)/30*(SS011+ANTE_DCCF))+HE038+DV997+DV300+ANTE_OS+DV040+ANTE_VA+LD073)
ELSE
   RESULT:=ROUND(((SAL_IBC/30)*(SS011+ANTE_DCCF))+HE038+DV997+DV300+ANTE_OS+DV040+ANTE_VA+LD073)'
WHERE (CODIGO = 'SS021') AND
      (NATIVO = 'S');


COMMIT WORK;



/* EXPORTADOS FONDO_SP */

UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1365, 'nom:fondosp', 'DOC', 'SELECT "noov:Nvfsp_porc",
       "noov:Nvfsp_dedu",
       "noov:Nvfsp_posb",
       "noov:Nvfsp_desb"
FROM Pz_Ne_Fondo_Sp(:Receptor, :Fecha_Desde, :Fecha_Hasta)
WHERE "noov:Nvfsp_dedu" + "noov:Nvfsp_desb" > 0', 'S', 365, 'NOOVA_NE', 'FACTURA', 'noov:FondoSP', NULL, NULL, NULL)
                        MATCHING (ID);

COMMIT WORK;

/* PLANO BANCOS DAVIDIENDA */ 

UPDATE INFORMES SET 
    SQL_CONSULTA = 'SELECT ''TR'' Registro,
       P.Codpersonal,
       COALESCE(TRIM(P1.Banco), '' '') AS Cuenta,
       CASE TRIM(P1.Codtipocuenta)
         WHEN ''A'' THEN ''CA''
         WHEN ''DP'' THEN ''DP''
         WHEN ''NE'' THEN ''CA''
         WHEN ''C'' THEN ''CC''
         ELSE ''  ''
       END AS Tipo_Cuenta,
       COALESCE(''0'' || P1.Codentidad, 0) AS Banco,
       CAST(P.Adicion AS INT) AS Valor,
       CASE P1.Codidentidad
         WHEN ''31'' THEN ''01''
         WHEN ''13'' THEN ''02''
         WHEN ''12'' THEN ''03''
         WHEN ''21'' THEN ''04''
         WHEN ''41'' THEN ''05''
       END AS Tipo_Identificacion

FROM Planillas P
INNER JOIN Personal P1 ON (P.Codpersonal = P1.Codigo)
JOIN Rubros R ON (P.Codrubro = R.Codigo)
WHERE P.Codnomina = :Nomina AND
      R.Codconjunto = ''T_PAGAR'' AND
      COALESCE(TRIM(P1.Banco), '''') <> '''''
WHERE (CODIGO = 'NOM020050');

UPDATE INFORMES SET 
    SQL_CONSULTA = 'WITH Vt_Saldos
AS (SELECT SUM(P.Adicion) AS Valor,
           COUNT(P.Codrubro) AS Cantidad
    FROM Planillas P
    INNER JOIN Personal P1 ON (P.Codpersonal = P1.Codigo)
    JOIN Rubros R ON (P.Codrubro = R.Codigo)
    WHERE P.Codnomina = :Nomina AND
          R.Codconjunto = ''T_PAGAR'' AND
          COALESCE(TRIM(P1.Banco), '''') <> ''''
    GROUP BY P.Codnomina)
SELECT ''RC'' || LPAD(TRIM(T.Codigo), 16, ''0'') || ''00000000'' || LPAD(TRIM(SUBSTRING(B.Titular FROM 2)), 16, ''0'') ||
       CASE LEFT(B.Titular, 1)
         WHEN ''A'' THEN ''CA''
         WHEN ''C'' THEN ''CC''
         ELSE ''CA''
       END || ''000051'' || LPAD(CAST(Valor AS INT), 16, 0) || ''00'' || LPAD(Cantidad, 6, 0) || EXTRACT(YEAR FROM CURRENT_DATE) || LPAD(EXTRACT(MONTH FROM CURRENT_DATE), 2, 0) || LPAD(EXTRACT(DAY FROM CURRENT_DATE), 2, 0) || LPAD(EXTRACT(HOUR FROM CURRENT_TIMESTAMP), 2, 0) || LPAD(EXTRACT(MINUTE FROM CURRENT_TIMESTAMP), 2, 0) || LPAD(EXTRACT(SECOND FROM CURRENT_TIMESTAMP), 2, 0) || ''00009999'' || LPAD(''0'', 16, ''0'') ||
       CASE T.Codidentidad
         WHEN ''31'' THEN ''01''
         WHEN ''13'' THEN ''02''
         WHEN ''12'' THEN ''03''
         WHEN ''21'' THEN ''04''
         WHEN ''41'' THEN ''05''
       END || LPAD(''0'', 56, ''0'') AS Dato
FROM Terceros T, Vt_Saldos, Bancos B
WHERE Datos_Empresa = ''S'' AND
      B.Codigo = :Banco

UNION

SELECT ''TR'' || LPAD(TRIM(P.Codpersonal), 16, 0) || LPAD(''0'', 16, ''0'') || LPAD(COALESCE(TRIM(P1.Banco), '' ''), 16, 0) ||
       CASE TRIM(P1.Codtipocuenta)
         WHEN ''A'' THEN ''CA''
         WHEN ''NE'' THEN ''CA''
         WHEN ''DP'' THEN ''DP''
         WHEN ''C'' THEN ''CC''
         ELSE ''  ''
       END || LPAD(TRIM(COALESCE(''0'' || P1.Codentidad, 0)), 6, 0) || LPAD(CAST(P.Adicion AS INT), 16, 0) || ''00000000'' ||
       CASE P1.Codidentidad
         WHEN ''31'' THEN ''01''
         WHEN ''13'' THEN ''02''
         WHEN ''12'' THEN ''03''
         WHEN ''21'' THEN ''04''
         WHEN ''41'' THEN ''05''
       END || ''19999'' || LPAD(''0'', 81, ''0'')

FROM Planillas P
INNER JOIN Personal P1 ON (P.Codpersonal = P1.Codigo)
JOIN Rubros R ON (P.Codrubro = R.Codigo)
WHERE P.Codnomina = :Nomina AND
      R.Codconjunto = ''T_PAGAR'' AND
      COALESCE(TRIM(P1.Banco), '''') <> '''''
WHERE (CODIGO = 'NOM020055');

COMMIT WORK;

-- Planos BBVA
UPDATE OR INSERT INTO INFORMES (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD, DESTINO_FTP, CODREPORTE)
                        VALUES ('NOM020060', 'TRANSFERENCIA NOMINA A CUENTA BBVA -INFORME VoBo ARCHIVO PLANO', 'Requisitos: 
En Personal Asignar campos Entidad Bancaria, Tipo de Cuenta y Numero de cuenta.', '-- BBVA
SELECT
       CASE P1.Codidentidad
         WHEN ''13'' THEN ''01''
         WHEN ''22'' THEN ''02''
         WHEN ''31'' THEN ''03''
         WHEN ''12'' THEN ''04''
         WHEN ''41'' THEN ''05''
       END AS Tipo_Identificacion,
       P.Codpersonal,
       ''1'' Forma_Pago,
       COALESCE(P1.Codentidad, 0) AS Banco,

       IIF(P1.Codentidad = ''13'', COALESCE(TRIM(P1.Banco), ''''), '''') AS Cuenta_Bbva,
       IIF(P1.Codentidad = ''13'', '''',
       CASE TRIM(P1.Codtipocuenta)
         WHEN ''A'' THEN ''02 (AHORRO)''
         WHEN ''C'' THEN ''01 (CORRIENTE)''
         ELSE ''00''
       END) AS Tipo_Cuenta,
       IIF(P1.Codentidad = ''13'', '''', COALESCE(TRIM(P1.Banco), '''')) AS Cuenta,

       SUM(P.Adicion) AS Valor,
       P1.Nombre,
       P1.Direccion,
       M.Nombre Direccion2,
       P1.Email,
       TRIM(N.Nombre) Concepto

FROM Planillas P
JOIN Nominas N ON P.Codnomina = N.Codigo
JOIN Personal P1 ON (P.Codpersonal = P1.Codigo)
JOIN Rubros R ON (P.Codrubro = R.Codigo)
JOIN Municipios M ON (P1.Codmunicipio = M.Codigo)
WHERE P.Codnomina = :Nomina
      AND R.Codconjunto = ''T_PAGAR''
      AND COALESCE(TRIM(P1.Banco), '''') <> ''''
GROUP BY 1, 2, 3, 4, 5, 6, 7, 9, 10, 11, 12, 13', 'S', 'NOMINA', 'N', 'PAGOS', 'N', 'LIBRE', 5, 'N', 1, NULL, NULL)
                      MATCHING (CODIGO);
UPDATE OR INSERT INTO INFORMES (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD, DESTINO_FTP, CODREPORTE)
                        VALUES ('NOM020065', 'TRANSFERENCIA NOMINA A CUENTA BBVA -EXPORTAR A ARCHIVO PLANO', 'Requisitos: 
En Personal Asignar campos Entidad Bancaria, Tipo de Cuenta y Numero de cuenta.', '-- BBVA
SELECT Tipo_Identificacion || LPAD(TRIM(Codpersonal), 15, ''0'') || ''01'' || LPAD(TRIM(Banco), 4, 0) || LPAD(Cuenta_Bbva, 16, 0) ||
    Tipo_Cuenta || LPAD(Cuenta, 17, 0) || LPAD(TRUNC(Valor), 13, 0) || SUBSTRING((Valor - TRUNC(Valor)) FROM 3 FOR 2) ||
    ''00000000'' || ''0000'' || RPAD(TRIM(Nombre), 36, '' '') || RPAD(TRIM(Direccion), 36, '' '') || RPAD(TRIM(Direccion2), 36, '' '') ||
    RPAD(TRIM(Email), 48, '' '') || Concepto
FROM (SELECT
             CASE P1.Codidentidad
               WHEN ''13'' THEN ''01''
               WHEN ''22'' THEN ''02''
               WHEN ''31'' THEN ''03''
               WHEN ''12'' THEN ''04''
               WHEN ''41'' THEN ''05''
             END AS Tipo_Identificacion,
             P.Codpersonal,
             ''1'' Forma_Pago,
             COALESCE(P1.Codentidad, 0) AS Banco,

             IIF(P1.Codentidad IN (''13'', ''013'', ''0013''), COALESCE(TRIM(P1.Banco), ''''), '''') AS Cuenta_Bbva,
             IIF(P1.Codentidad IN (''13'', ''013'', ''0013''), ''00'',
             CASE TRIM(P1.Codtipocuenta)
               WHEN ''A'' THEN ''02''
               WHEN ''C'' THEN ''01''
               ELSE ''00''
             END) AS Tipo_Cuenta,
             IIF(P1.Codentidad IN (''13'', ''013'', ''0013''), '''', COALESCE(TRIM(P1.Banco), '''')) AS Cuenta,

             SUM(P.Adicion) AS Valor,
             P1.Nombre,
             P1.Direccion,
             M.Nombre Direccion2,
             P1.Email,
             TRIM(N.Nombre) Concepto

      FROM Planillas P
      JOIN Nominas N ON P.Codnomina = N.Codigo
      JOIN Personal P1 ON (P.Codpersonal = P1.Codigo)
      JOIN Rubros R ON (P.Codrubro = R.Codigo)
      JOIN Municipios M ON (P1.Codmunicipio = M.Codigo)
      WHERE P.Codnomina = :Nomina
            AND R.Codconjunto = ''T_PAGAR''
            AND COALESCE(TRIM(P1.Banco), '''') <> ''''
      GROUP BY 1, 2, 3, 4, 5, 6, 7, 9, 10, 11, 12, 13)', 'S', 'NOMINA', 'N', 'PAGOS', 'N', 'LIBRE', 5, 'N', 1, NULL, NULL)
                      MATCHING (CODIGO);


COMMIT WORK;


/* Unificación MEK_PLANILLA2  */
UPDATE Personalizados
SET Personalizado = 'MEK_PLANILLA2.FR3'
WHERE Personalizado = 'MEK_PLANILLA2A.FR3';

COMMIT WORK;

/* SC_NOMINA51 */

UPDATE EXPRESIONES SET 
    SQL_BLOQUE = 'execute block
returns (
    EMPLEADO char(15),
    VALOR numeric(17,4))
as
declare V_NOMINA char(5);

declare V_LIQ char(10);
declare V_SENA char(2);
declare V_INTEGRAL char(1);
declare V_FECHA date;
declare V_DESDE date;
declare V_HASTA date;
declare M_HASTA date;
declare V_FECHA_ING date;
declare V_RUBRO1 char(5) = ''DI015'';
declare V_RUBRO2 char(5) = '''';

begin
  select RDB$GET_CONTEXT(''USER_SESSION'', ''G_NOMINA'')
  from RDB$DATABASE
  into V_NOMINA;

  select FECHA, DESDE, HASTA, CODLIQUIDACION
  from NOMINAS
  where CODIGO = :V_NOMINA
  into V_FECHA, V_DESDE, V_HASTA, V_LIQ;

  for select P.CODIGO, P.CODCOTIZANTE, P.SALARIO_INTEGRAL
      FROM Tmp_Empleados S
      join PERSONAL P on (S.CODPERSONAL = P.CODIGO)
      into EMPLEADO, V_SENA, V_INTEGRAL
  do
  begin
    select max(S.DESDE)
    from SALARIOS S
    where S.CODPERSONAL = :EMPLEADO and
          S.COLUMNA_DESDE = ''INGRESO''
    into V_FECHA_ING;
    V_FECHA_ING = iif(V_FECHA_ING > ''01/01/'' || extract(year from V_FECHA), V_FECHA_ING, ''01/01/'' || extract(year from V_FECHA));

    select sum(S.ADICION)
    from PLANILLAS S
    join NOMINAS N on (S.CODNOMINA = N.CODIGO)
    where N.FECHA >= :V_FECHA_ING and
          N.FECHA <= :V_FECHA and
          S.CODRUBRO in (:V_RUBRO1, :V_RUBRO2) and
          S.CODPERSONAL = :EMPLEADO
    into VALOR;

    if (VALOR is not null) then
    begin
      if (V_SENA in (''12'', ''19'') or V_INTEGRAL = ''S'') then
        VALOR = 0;
      suspend;
    end
  end
end'
WHERE (CODIGO = 'A_DIA') AND
      (NATIVO = 'S');

UPDATE EXPRESIONES SET 
    SQL_BLOQUE = 'execute block
returns (
    EMPLEADO char(15),
    VALOR numeric(17,4))
as
declare V_NOMINA char(5);

declare V_LIQ char(10);
declare V_SENA char(2);
declare V_INTEGRAL char(1);
declare V_FECHA date;
declare V_DESDE date;
declare V_HASTA date;
declare M_HASTA date;
declare V_FECHA_ING date;
declare V_RUBRO1 char(5) = ''PR060'';
declare V_RUBRO2 char(5) = '''';

begin
  select RDB$GET_CONTEXT(''USER_SESSION'', ''G_NOMINA'')
  from RDB$DATABASE
  into V_NOMINA;

  select FECHA, DESDE, HASTA, CODLIQUIDACION
  from NOMINAS
  where CODIGO = :V_NOMINA
  into V_FECHA, V_DESDE, V_HASTA, V_LIQ;

  for select P.CODIGO, P.CODCOTIZANTE, P.SALARIO_INTEGRAL
      FROM Tmp_Empleados S
      join PERSONAL P on (S.CODPERSONAL = P.CODIGO)
      into EMPLEADO, V_SENA, V_INTEGRAL
  do
  begin
    select max(S.DESDE)
    from SALARIOS S
    where S.CODPERSONAL = :EMPLEADO and
          S.COLUMNA_DESDE = ''INGRESO''
    into V_FECHA_ING;
    V_FECHA_ING = iif(V_FECHA_ING > ''01/01/'' || extract(year from V_FECHA), V_FECHA_ING, ''01/01/'' || extract(year from V_FECHA));

    select sum(S.ADICION)
    from PLANILLAS S
    join NOMINAS N on (S.CODNOMINA = N.CODIGO)
    where N.FECHA >= :V_FECHA_ING and
          N.FECHA <= :V_FECHA and
          S.CODRUBRO in (:V_RUBRO1, :V_RUBRO2) and
          S.CODPERSONAL = :EMPLEADO
    into VALOR;

    if (VALOR is not null) then
    begin
      if (V_SENA in (''12'', ''19'') or V_INTEGRAL = ''S'') then
        VALOR = 0;
      suspend;
    end
  end
end'
WHERE (CODIGO = 'A_PROV60') AND
      (NATIVO = 'S');

UPDATE EXPRESIONES SET 
    SQL_BLOQUE = 'execute block
returns (
    EMPLEADO char(15),
    VALOR numeric(17,4))
as
declare V_NOMINA char(5);

declare V_LIQ char(10);
declare V_SENA char(2);
declare V_INTEGRAL char(1);
declare V_FECHA date;
declare V_DESDE date;
declare V_HASTA date;
declare M_HASTA date;
declare V_FECHA_ING date;
declare V_RUBRO1 char(5) = ''PR062'';
declare V_RUBRO2 char(5) = '''';

begin
  select RDB$GET_CONTEXT(''USER_SESSION'', ''G_NOMINA'')
  from RDB$DATABASE
  into V_NOMINA;

  select FECHA, DESDE, HASTA, CODLIQUIDACION
  from NOMINAS
  where CODIGO = :V_NOMINA
  into V_FECHA, V_DESDE, V_HASTA, V_LIQ;

  for select P.CODIGO, P.CODCOTIZANTE, P.SALARIO_INTEGRAL
      FROM Tmp_Empleados S
      join PERSONAL P on (S.CODPERSONAL = P.CODIGO)
      into EMPLEADO, V_SENA, V_INTEGRAL
  do
  begin
    select max(S.DESDE)
    from SALARIOS S
    where S.CODPERSONAL = :EMPLEADO and
          S.COLUMNA_DESDE = ''INGRESO''
    into V_FECHA_ING;
    V_FECHA_ING = iif(V_FECHA_ING > ''01/01/'' || extract(year from V_FECHA), V_FECHA_ING, ''01/01/'' || extract(year from V_FECHA));

    select sum(S.ADICION)
    from PLANILLAS S
    join NOMINAS N on (S.CODNOMINA = N.CODIGO)
    where N.FECHA >= :V_FECHA_ING and
          N.FECHA <= :V_FECHA and
          S.CODRUBRO in (:V_RUBRO1, :V_RUBRO2) and
          S.CODPERSONAL = :EMPLEADO
    into VALOR;

    if (VALOR is not null) then
    begin
      if (V_SENA in (''12'', ''19'') or V_INTEGRAL = ''S'') then
        VALOR = 0;
      suspend;
    end
  end
end'
WHERE (CODIGO = 'A_PROV62') AND
      (NATIVO = 'S');


COMMIT WORK;

UPDATE RUBROS SET 
    FORMULA = 'DV040+LD073'
WHERE (CODIGO = 'SA002') AND
      (NATIVO = 'S');

UPDATE RUBROS SET 
    FORMULA = 'IF(QUINCENA<2) THEN
RESULT:=0
ELSE
IF(ES_SENA=1 OR ES_PENSION=1) THEN
RESULT:=0
ELSE
IF((ES_INTEGRA=0) AND ((SA026-(BASICO/30*DI009+DI020)+SA009+SA020+ANTE_SAL+DV300)>(25*SMLV))) THEN
RESULT:=ROUND(((25*SMLV)/30*(SS014+ANTE_DAFP-DI002-ANTE_DVAC-DI004-ANTE_SUP-DI018-DI009-DI020-ANTE_IEG)+VA080+SA009+SA020+ANTE_SIEG)*C36_AFP_SP/100)
ELSE
IF((ES_INTEGRA=1) AND (((((SA026-(BASICO/30*DI009+DI020)+SA009+SA020+ANTE_SAL))*C05_S_INT)+HE038+DV997+DV300)>(25*SMLV))) THEN
RESULT:=ROUND(((25*SMLV)/30*(SS014+ANTE_DAFP-DI002-ANTE_DVAC-DI004-ANTE_SUP-DI018-DI009-DI020-ANTE_IEG)+VA080+SA009+SA020+ANTE_SIEG)*C36_AFP_SP/100)
ELSE
IF(ES_INTEGRA=1) THEN
RESULT:=ROUND((((SAL_BASICO/30*(SS014+ANTE_DAFP-DI002-ANTE_DVAC-DI004-ANTE_SUP-DI018-DI009-DI020-ANTE_IEG))*C05_S_INT)+HE038+DV997+DV300+ANTE_OS)*C36_AFP_SP/100)
ELSE
IF((SA027+ANTE_SAL)>=SMLV*4) THEN
RESULT:=ROUND(((SAL_BASICO/30*(SS014+ANTE_DAFP-DI002-ANTE_DVAC-DI004-ANTE_SUP-DI018-DI009-DI020-ANTE_IEG))+HE038+DV997+DV300+ANTE_OS)*C36_AFP_SP/100)
ELSE
RESULT:=0'
WHERE (CODIGO = 'DT035') AND
      (NATIVO = 'S');


COMMIT WORK;

/* SC_NOMINA_052 */

UPDATE OR INSERT INTO EXPRESIONES (CODIGO, NOMBRE, SQL_BLOQUE, ACTIVO, NATIVO)
                           VALUES ('ANTE_DVAC', 'DIAS DI002 VACACIONES EN EL PERIODO DISFRUTADAS EN EL MISMO MES', '-- SUMATORIA RUBROS DEL MISMO MES DEL ULTIMO INGRESO EN ADELANTE
EXECUTE BLOCK
RETURNS (
    Empleado CHAR(15),
    Valor NUMERIC(17,4))
AS
DECLARE V_Nomina CHAR(5);
DECLARE V_Fecha_Nom DATE;
DECLARE V_Desde_Nom DATE;
DECLARE V_Hasta_Nom DATE;
DECLARE V_Fecha_Ing DATE;
DECLARE V_Liq CHAR(10);
DECLARE V_Dias INTEGER;
DECLARE V_Rubro1 CHAR(5) = ''DI002'';
DECLARE V_Rubro2 CHAR(5) = '''';
DECLARE V_Rubro3 CHAR(5) = '''';

BEGIN
  SELECT Rdb$Get_Context(''USER_SESSION'', ''G_NOMINA'')
  FROM Rdb$Database
  INTO V_Nomina;

  SELECT Fecha,
         Desde,
         Hasta,
         Codliquidacion,
         Dias
  FROM Nominas
  WHERE Codigo = :V_Nomina
  INTO V_Fecha_Nom, V_Desde_Nom, V_Hasta_Nom, V_Liq, V_Dias;

  FOR SELECT  Codpersonal
    FROM TMP_EMPLEADOS
    INTO Empleado    
  DO
  BEGIN
    IF (V_Dias = 30) THEN
      Valor = 0;
    ELSE
    BEGIN
      SELECT MAX(S.Desde)
      FROM Salarios S
      WHERE S.Codpersonal = :Empleado AND
            S.Columna_Desde = ''INGRESO''
      INTO V_Fecha_Ing;

      SELECT SUM(S.Adicion + S.Deduccion)
      FROM Planillas S
      JOIN Nominas N ON (S.Codnomina = N.Codigo)
      WHERE EXTRACT(MONTH FROM N.Fecha) = EXTRACT(MONTH FROM :V_Fecha_Nom) AND
            EXTRACT(YEAR FROM N.Fecha) = EXTRACT(YEAR FROM :V_Fecha_Nom) AND
            S.Codrubro IN (:V_Rubro1, :V_Rubro2, :V_Rubro3) AND
            S.Codpersonal = :Empleado AND
            N.Fecha < :V_Fecha_Nom AND
            N.Hasta >= :V_Fecha_Ing
      INTO Valor;
      Valor = COALESCE(Valor, 0);
    END
    SUSPEND;
  END
END', 'S', 'S')
                         MATCHING (CODIGO);
UPDATE OR INSERT INTO EXPRESIONES (CODIGO, NOMBRE, SQL_BLOQUE, ACTIVO, NATIVO)
                           VALUES ('ANTE_SUP', 'DIAS EN EL MES DE QUINCENA ANTERIORES DE SLN SOLO DI004', '-- SUMATORIA RUBROS DEL MISMO MES
EXECUTE BLOCK
RETURNS (
    Empleado CHAR(15),
    Valor NUMERIC(17,4))
AS
DECLARE V_Nomina    CHAR(5);
DECLARE V_Fecha_Nom DATE;
DECLARE V_Desde_Nom DATE;
DECLARE V_Hasta_Nom DATE;
DECLARE V_Liq       CHAR(10);
DECLARE V_Rubro1    CHAR(5) = ''DI004'';
DECLARE V_Rubro2    CHAR(5) = '''';
DECLARE V_Rubro3    CHAR(5) = '''';

BEGIN
  SELECT Rdb$Get_Context(''USER_SESSION'', ''G_NOMINA'')
  FROM Rdb$Database
  INTO V_Nomina;

  SELECT Fecha, Desde, Hasta, Codliquidacion
  FROM Nominas
  WHERE Codigo = :V_Nomina
  INTO V_Fecha_Nom, V_Desde_Nom, V_Hasta_Nom, V_Liq;

  FOR SELECT  Codpersonal
    FROM TMP_EMPLEADOS
    INTO Empleado    
  DO
  BEGIN

    SELECT SUM(S.Adicion + S.Deduccion)
    FROM Planillas S
    JOIN Nominas N ON (S.Codnomina = N.Codigo)
    WHERE EXTRACT(MONTH FROM N.Fecha) = EXTRACT(MONTH FROM :V_Fecha_Nom) AND
          EXTRACT(YEAR FROM N.Fecha) = EXTRACT(YEAR FROM :V_Fecha_Nom) AND
          S.Codrubro IN (:V_Rubro1, :V_Rubro2, :V_Rubro3) AND
          S.Codpersonal = :Empleado AND
          N.Fecha < :V_Fecha_Nom
    INTO Valor;

    Valor = COALESCE(Valor, 0);
    SUSPEND;
  END
END', 'S', 'S')
                         MATCHING (CODIGO);

COMMIT WORK;

/* SC_NOMINA_053 */

UPDATE RUBROS SET 
    FORMULA = 'IF((DIAS_TRABAJADOS-DIAS_YA_LQ=0) AND (LD073=0)) THEN
  RESULT:=0
ELSE
IF(QUINCENA<2 OR ES_SENA=1 OR ES_MEDT=1) THEN
  RESULT:=0
ELSE
IF(EXONERADA=1 AND SA027+ANTE_SAL<SMLV*10) THEN 
    RESULT:=0
ELSE
IF((DIAS_TRABAJADOS-DIAS_YA_LQ=0) AND (LD073>1)) THEN
  RESULT:=LD073
ELSE
IF(SAL_BASICO<SMLV) THEN 
   RESULT:=ROUND(((SMLV/30)*(SS013+ANTE_DIAS))+HE038+DV997+DV300+ANTE_OS+DV040+ANTE_VA+LD073)
ELSE
IF((ES_INTEGRA=0) AND ((SAL_IBC+HE038+DV997+DV300+ANTE_OS+DV040+ANTE_VA+LD073)>(25*SMLV))) THEN
    RESULT:=ROUND(((25*SMLV)/30)*(SS013+ANTE_DIAS))
ELSE
IF((ES_INTEGRA=1) AND (((SAL_IBC*C05_S_INT)+ANTE_OS+DV040+ANTE_VA+LD073)>(25*SMLV)))  THEN 
    RESULT:=ROUND(((25*SMLV)/30)*(SS013+ANTE_DIAS))
ELSE
  IF(ES_INTEGRA=1) THEN 
      RESULT:=ROUND((((SAL_IBC*C05_S_INT)/30)*(SS013+ANTE_DIAS))+HE038+DV997+DV300+ANTE_OS+DV040+ANTE_VA+LD073)
  ELSE
       RESULT:=ROUND(((SAL_IBC/30)*(SS013+ANTE_DIAS))+HE038+DV997+DV300+ANTE_OS+DV040+ANTE_VA+LD073)'
WHERE (CODIGO = 'SS023') AND
      (NATIVO = 'S');


COMMIT WORK;

UPDATE RUBROS SET 
    FORMULA = 'IF(ES_SENA=1 OR ES_ARL=1) THEN
  RESULT:=0
ELSE
IF(ES_ALTOR=1) THEN
  RESULT:=ROUND((((SAL_BASICO/30)*DI015)+HE024+HE025+HE026+HE027+HE028)/C19_PR_VAC)
ELSE 
  RESULT:=ROUND((((SAL_BASICO/30)*DI015)+HE024+HE025+HE026+HE027+HE028)/C20_PR_VAC)'
WHERE (CODIGO = 'PR063') AND
      (NATIVO = 'S');


COMMIT WORK;


UPDATE OR INSERT INTO EXPRESIONES (CODIGO, NOMBRE, SQL_BLOQUE, ACTIVO, NATIVO)
                           VALUES ('TOTA_CONSP', 'TOTAL CONSTITUTIVOS DE SALARIO CON CORTE PRIMA', '--Total cons corte prima
EXECUTE BLOCK
RETURNS (
    Empleado CHAR(15),
    Valor    NUMERIC(17,4))
AS
DECLARE V_Nomina       CHAR(5);
DECLARE V_Liq          CHAR(10);
DECLARE V_Desde_Nom    DATE;
DECLARE V_Hasta_Nom    DATE;
DECLARE V_Fecha_Nom    DATE;
DECLARE V_Fecha_Ini    DATE;
DECLARE V_Fecha_Ing    DATE;
DECLARE V_Fecha_Corte  DATE;
DECLARE V_Dias         NUMERIC(17,4);
DECLARE V_Rubro1       CHAR(5) = ''SA001'';
DECLARE V_Rubro2       CHAR(5) = '''';
DECLARE V_Rubro_Corte1 CHAR(5) = ''LD070'';
DECLARE V_Rubro_Corte2 CHAR(5) = ''LD075'';

BEGIN
  SELECT Rdb$Get_Context(''USER_SESSION'', ''G_NOMINA'')
  FROM Rdb$Database
  INTO V_Nomina;

  SELECT Desde,
         Hasta,
         Fecha,
         Codliquidacion
  FROM Nominas
  WHERE Codigo = :V_Nomina
  INTO V_Desde_Nom,
       V_Hasta_Nom,
       V_Fecha_Nom,
       V_Liq;

  FOR SELECT S.Codpersonal,
             MAX(S.Desde)
      FROM Tmp_Empleados Sl
      JOIN Salarios S ON (Sl.Codpersonal = S.Codpersonal)
      WHERE S.Columna_Desde = ''INGRESO''
      GROUP BY 1
      INTO Empleado,
           V_Fecha_Ing
  DO
  BEGIN

    V_Fecha_Ini = DATEADD(-12 MONTH TO :V_Hasta_Nom);
    IF (V_Desde_Nom > V_Fecha_Ini) THEN
      V_Fecha_Ini = :V_Desde_Nom;
    IF (V_Fecha_Ing > V_Fecha_Ini) THEN
      V_Fecha_Ini = :V_Fecha_Ing;

    SELECT MAX(N.Hasta)
    FROM Nominas N
    JOIN Planillas S ON (N.Codigo = S.Codnomina)
    WHERE S.Codrubro IN (:V_Rubro_Corte1, :V_Rubro_Corte2)
          AND N.Codigo <> :V_Nomina
          AND N.Fecha <= :V_Fecha_Nom
          AND N.Fecha >= :V_Fecha_Ing
          AND S.Codpersonal = :Empleado
    INTO V_Fecha_Corte;
    V_Fecha_Corte = COALESCE(V_Fecha_Corte, V_Fecha_Ini);

    IF (V_Fecha_Corte > V_Fecha_Ini) THEN
      V_Fecha_Ini = V_Fecha_Corte;

    SELECT SUM(S.Adicion)
    FROM Planillas S
    JOIN Nominas N ON (S.Codnomina = N.Codigo)
    WHERE N.Fecha > :V_Fecha_Ini
          AND N.Fecha <= :V_Fecha_Nom
          AND S.Codrubro IN (:V_Rubro1, :V_Rubro2)
          AND S.Codpersonal = :Empleado
    INTO Valor;
    Valor = COALESCE(Valor, 0);
    SUSPEND;
  END
END', 'S', 'S')
                         ;

COMMIT WORK;

UPDATE RUBROS SET 
    FORMULA = 'IF((TOTA_DIAS+DI015+SI050)=0) THEN 
  RESULT:=0 
ELSE
IF((TOTA_DIAS+DI015)<30) THEN 
  RESULT:=SAL_BASICO
ELSE
IF(((((TOTA_SALAP-TOTA_CONSP)+(SA027-SA001))/(TOTA_DIAS+DI015))*30)<BASICO) THEN 
  RESULT:=BASICO
ELSE   
  RESULT:=ROUND((((TOTA_SALAP-TOTA_CONSP)+(SA027-SA001)+SI101)/(TOTA_DIAS+DI015+SI050))*30)'
WHERE (CODIGO = 'LD106') AND
      (NATIVO = 'S');


COMMIT WORK;



/* INFORMES */


UPDATE OR INSERT INTO GRUPOS (CODIGO, NOMBRE)
                      VALUES ('DSE', 'DOCUMENTO SOPORTE ELECTRONICO')
                    MATCHING (CODIGO);


COMMIT WORK;


UPDATE OR INSERT INTO INFORMES (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD, DESTINO_FTP, CODREPORTE)
                        VALUES ('DSE01001', 'RELACION DE DOCUMENTOS SOPORTE EN ELECTRONICA X GESTION - RANGO FECHA', 'Relacion de Documentos Soporte Electronico', 'SELECT F.Id_Pta,
       F.Cufe,
       F.Pdf,
       F.Fecha_Envio,
       F.Modulo,
       F.Fase,
       F.Resultado,
       F.Estado,
       C.Enviado,
       C.Tipo,
       C.Prefijo,
       C.Numero,
       LPAD(TRIM(C.Numero), 10) AS Numerico,
       C.Fecha,
       C.Codtercero,
       T.Dv,
       T.Nombre AS Nombre_Cliente,
       T.Codsociedad,
       T.Codidentidad,
       C.Codusuario,
       C.Impreso,
       C.Impuestos,
       C.Total,
       F.Validacion,
       C.Concepto,
       TRIM(F.Error) AS Error,
       ''['' || CAST(REPLACE(REPLACE(F.Qrdata, ASCII_CHAR(10), ''] - [''), ASCII_CHAR(13), '' '') AS CHAR(1000)) || '']'' AS "QRDATA - DATOS DIAN XML"
FROM Comprobantes C
LEFT JOIN Facturas F ON ((F.Tipo = C.Tipo) AND
      (F.Prefijo = C.Prefijo) AND
      (C.Numero = F.Numero))
JOIN Documentos D ON (C.Tipo = D.Codigo)
JOIN Terceros T ON (T.Codigo = C.Codtercero)
WHERE (C.Fecha >= :_Desde
      AND C.Fecha <= :_Hasta)
      AND (C.Bloqueado = ''S'')
      AND (TRIM(D.Codigo_Fe) IN (''NOTA DE AJUSTE A DS'', ''DOCUMENTO SOPORTE''))
ORDER BY 1', 'N', 'ELECTRONIC', 'N', 'DSE', 'N', 'LIBRE', 5, 'N', 1, NULL, NULL)
                      MATCHING (CODIGO);


COMMIT WORK;


/************************************************************/
/****                     OTROS                          ****/
/************************************************************/


/* Validaciones */
UPDATE OR INSERT INTO VALIDACIONES (ID, TABLA, CAMPO, SQL, POSIBLES_VALORES, MODULO)
                            VALUES (1, 'TERCEROS', 'NATURALEZA', 'SELECT Naturaleza
FROM Terceros
WHERE Codigo = '':TERCERO''', 'N,J', 'GESTION')
                          MATCHING (ID);
UPDATE OR INSERT INTO VALIDACIONES (ID, TABLA, CAMPO, SQL, POSIBLES_VALORES, MODULO)
                            VALUES (2, 'TERCEROS', 'NATURALEZA', 'SELECT Naturaleza
FROM Terceros
WHERE Codigo = '':TERCERO''', 'N,J', 'CONTABLE')
                          MATCHING (ID);
UPDATE OR INSERT INTO VALIDACIONES (ID, TABLA, CAMPO, SQL, POSIBLES_VALORES, MODULO)
                            VALUES (3, 'TERCEROS', 'CODIDENTIDAD', 'SELECT Codidentidad
FROM Terceros
WHERE Codigo = '':TERCERO''', '11,12,13,21,22,31,41,42,50,91', 'GESTION')
                          MATCHING (ID);
UPDATE OR INSERT INTO VALIDACIONES (ID, TABLA, CAMPO, SQL, POSIBLES_VALORES, MODULO)
                            VALUES (4, 'TERCEROS', 'CODIDENTIDAD', 'SELECT Codidentidad
FROM Terceros
WHERE Codigo = '':TERCERO''', '11,12,13,21,22,31,41,42,50,91', 'CONTABLE')
                          MATCHING (ID);
UPDATE OR INSERT INTO VALIDACIONES (ID, TABLA, CAMPO, SQL, POSIBLES_VALORES, MODULO)
                            VALUES (7, 'TIPOIMPUESTOS', 'CODIGO_FE', 'SELECT T.Codigo_Fe
FROM Reg_Impuestos I
JOIN Tipoimpuestos T ON (I.Tipo_Impuesto = T.Codigo)
WHERE TRIM(I.Tipo) = '':TIPO''
      AND TRIM(I.Prefijo) = '':PREFIJO''
      AND TRIM(I.Numero) = '':NUMERO''', '00,01,02,03,04,21,22,23,24,ZY,ZZ', 'GESTION')
                          MATCHING (ID);
UPDATE OR INSERT INTO VALIDACIONES (ID, TABLA, CAMPO, SQL, POSIBLES_VALORES, MODULO)
                            VALUES (8, 'TIPOIMPUESTOS', 'CODIGO_FE', 'SELECT T.Codigo_Fe
FROM Fe_Apo_Impuestos('':TIPO'', '':PREFIJO'', '':NUMERO'') I
JOIN Tipoimpuestos T ON (T.Codigo = I.Codtipoimpuesto)', '00,01,02,03,04,21,22,23,24,ZY,ZZ', 'CONTABLE')
                          MATCHING (ID);
UPDATE OR INSERT INTO VALIDACIONES (ID, TABLA, CAMPO, SQL, POSIBLES_VALORES, MODULO)
                            VALUES (9, 'TIPORETENCIONES', 'CODIGO_FE', 'SELECT T.Codigo_Fe
FROM Reg_Retenciones R
JOIN Tiporetenciones T ON (R.Tipo_Retencion = T.Codigo)
WHERE TRIM(R.Tipo) = '':TIPO''
      AND TRIM(R.Prefijo) = '':PREFIJO''
      AND TRIM(R.Numero) = '':NUMERO''', '05,06,07,20,25,26,ZZ,ZY,99', 'GESTION')
                          MATCHING (ID);
UPDATE OR INSERT INTO VALIDACIONES (ID, TABLA, CAMPO, SQL, POSIBLES_VALORES, MODULO)
                            VALUES (10, 'TIPORETENCIONES', 'CODIGO_FE', 'SELECT T.Codigo_Fe
FROM Fe_Apo_Retenciones('':TIPO'', '':PREFIJO'', '':NUMERO'') I
JOIN Tiporetenciones T ON (T.Codigo = I.Codtiporetencion)', '05,06,07,20,25,26,ZZ,ZY,99', 'CONTABLE')
                          MATCHING (ID);
UPDATE OR INSERT INTO VALIDACIONES (ID, TABLA, CAMPO, SQL, POSIBLES_VALORES, MODULO)
                            VALUES (5, 'SOCIEDADES', 'CODIGO_FE', 'SELECT S.Codigo_Fe
FROM Terceros T
JOIN Sociedades S ON (T.Codsociedad = S.Codigo)
WHERE T.Codigo = '':TERCERO''', '04,05,48,49', 'GESTION')
                          MATCHING (ID);
UPDATE OR INSERT INTO VALIDACIONES (ID, TABLA, CAMPO, SQL, POSIBLES_VALORES, MODULO)
                            VALUES (6, 'SOCIEDADES', 'CODIGO_FE', 'SELECT S.Codigo_Fe
FROM Terceros T
JOIN Sociedades S ON (T.Codsociedad = S.Codigo)
WHERE T.Codigo = '':TERCERO''', '04,05,48,49', 'CONTABLE')
                          MATCHING (ID);

COMMIT WORK;


/* PROCESOS */

UPDATE OR INSERT INTO PROCESOS (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, CODGRUPO, NIVEL, TABLERO, PRIORIDAD)
                        VALUES ('P87087', 'INACTIVAR DOCUMENTOS NO USADOS EN CONTABLE O GESTION', 'Inactiva todos los documentos que nunca han sido usados tanto en contable como en gestion', 'EXECUTE BLOCK
AS
BEGIN
  UPDATE Documentos
  SET Activo_Contabilidad = ''N''
  WHERE Codigo NOT IN (SELECT DISTINCT Tipo
                       FROM Cocomp);
  UPDATE Documentos
  SET Activo = ''N''
  WHERE Codigo NOT IN (SELECT DISTINCT Tipo
                       FROM Comprobantes);
END', 'S', 'PARAMETROS', 'PROCESOS TECNICOS', 5, 'N', 1)
                      MATCHING (CODIGO);

COMMIT WORK;

/* INACTIVA PDF */ 

UPDATE PROCESOS SET 
    SQL_CONSULTA = 'UPDATE Facturas F
SET Pdf = ''N'', Resultado = 0
WHERE EXISTS(SELECT Tipo,
                    Prefijo,
                    Numero
             FROM Comprobantes C
             WHERE C.Tipo = F.Tipo AND
                   C.Prefijo = F.Prefijo AND
                   C.Numero = F.Numero AND
                   C.Fecha >= :Fecha_Desde AND
                   C.Fecha <= :Fecha_Hasta AND
                   C.Tipo = :Tipo AND
                   C.Prefijo = :Prefijo) OR
        EXISTS(SELECT Tipo,
                    Prefijo,
                    Numero
             FROM Auto_Comp C
             WHERE C.Tipo = F.Tipo AND
                   C.Prefijo = F.Prefijo AND
                   C.Numero = F.Numero AND
                   C.Fecha >= :Fecha_Desde AND
                   C.Fecha <= :Fecha_Hasta AND
                   C.Tipo = :Tipo AND
                   C.Prefijo = :Prefijo);'
WHERE (CODIGO = 'P87072');

UPDATE PROCESOS SET 
    SQL_CONSULTA = 'UPDATE Facturas
SET Pdf = ''N'', Resultado = 0
WHERE Prefijo = :B_Prefijo AND
      Numero = :C_Numero;'
WHERE (CODIGO = 'P87070');

COMMIT WORK;

UPDATE TIPORETENCIONES SET CODIGO_FE='99' WHERE CODIGO_FE='NA';

COMMIT WORK;


UPDATE Facturas
SET Pdf = 'N', Resultado = 0
WHERE TRIM(Estado) = 'Exitosa'
      AND Resultado = 2;

COMMIT WORK;

/* RECHAZADAS */ 

/******************************************************************************/
/***                           Stored procedures                            ***/
/******************************************************************************/



SET TERM ^ ;

CREATE OR ALTER PROCEDURE PZ_RECHAZADAS
AS
BEGIN
  EXIT;
END^






SET TERM ; ^



/******************************************************************************/
/***                           Stored procedures                            ***/
/******************************************************************************/



SET TERM ^ ;

CREATE OR ALTER PROCEDURE PZ_RECHAZADAS
AS
DECLARE VARIABLE V_Tipo    VARCHAR(5);
DECLARE VARIABLE V_Prefijo VARCHAR(5);
DECLARE VARIABLE V_Numero  VARCHAR(10);
DECLARE VARIABLE V_Modulo  VARCHAR(10);
BEGIN

 SELECT FIRST 1 Modulo
 FROM Pta
 WHERE (Activo = 'S')
       AND (Modulo IN ('GESTION','CONTABLE'))
 ORDER BY Codigo
 INTO V_Modulo;

 IF (TRIM(V_Modulo) = 'GESTION') THEN
 BEGIN
  FOR SELECT Fac.Tipo,
             Fac.Prefijo,
             Fac.Numero
      FROM Facturas Fac
      JOIN Comprobantes Com USING (Tipo,Prefijo,Numero)
      WHERE (TRIM(Fac.Estado) IN ('Rechazada','Firmada'))
            AND (Com.Fecha >= CURRENT_DATE - 9)
      INTO V_Tipo,
           V_Prefijo,
           V_Numero
  DO
  BEGIN
   UPDATE Comprobantes
   SET Enviado = 'N'
   WHERE Tipo = :V_Tipo
         AND Prefijo = :V_Prefijo
         AND Numero = :V_Numero;

   UPDATE Facturas
   SET Pdf = 'N',
       Resultado = 0
   WHERE Tipo = :V_Tipo
         AND Prefijo = :V_Prefijo
         AND Numero = :V_Numero;
  END
 END
 ELSE
  FOR SELECT Fac.Tipo,
             Fac.Prefijo,
             Fac.Numero
      FROM Facturas Fac
      JOIN Auto_Comp Com USING (Tipo,Prefijo,Numero)
      WHERE (TRIM(Fac.Estado) IN ('Rechazada','Firmada'))
            AND (Com.Fecha >= CURRENT_DATE - 9)
      INTO V_Tipo,
           V_Prefijo,
           V_Numero
  DO
  BEGIN
   UPDATE Auto_Comp
   SET Enviado = 'N'
   WHERE Tipo = :V_Tipo
         AND Prefijo = :V_Prefijo
         AND Numero = :V_Numero;

   UPDATE Facturas
   SET Pdf = 'N',
       Resultado = 0
   WHERE Tipo = :V_Tipo
         AND Prefijo = :V_Prefijo
         AND Numero = :V_Numero;
  END
END^



SET TERM ; ^

COMMIT WORK;


EXECUTE PROCEDURE Pz_Rechazadas;

COMMIT WORK;


/* Proceso Pz_Rechazadas */
UPDATE OR INSERT INTO PROCESOS (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, CODGRUPO, NIVEL, TABLERO, PRIORIDAD)
                        VALUES ('ELEC001', 'PROCESO PARA ORGANIZAR RECHAZADAS Y PODER RETRANSMITIR', NULL, 'EXECUTE PROCEDURE Pz_Rechazadas;', 'N', 'ELECTRONIC', 'FE', 5, 'N', 1)
                      MATCHING (CODIGO);


COMMIT WORK;


--------

UPDATE Terceros
SET Codigo_Postal = SUBSTRING(Codmunicipio FROM 1 FOR 2) || '0001';

UPDATE Terceros
SET Codigo_Postal = '110111'
WHERE Codigo_Postal in ('110001','111001');

COMMIT WORK;


-- REPORTES DE FX_DINAMICO
UPDATE REPORTES SET 
    ARCHIVO = 'R_VTA_RES_01B.FR3'
WHERE (CODIGO = 'VTAS010102');

UPDATE REPORTES SET 
    ARCHIVO = 'R_VTA_RES_01A.FR3'
WHERE (CODIGO = 'VTAS010101');

UPDATE REPORTES SET 
    ARCHIVO = 'R_VTA_RES_02B.FR3'
WHERE (CODIGO = 'VTAS010202');

UPDATE REPORTES SET 
    ARCHIVO = 'R_VTA_RES_02D.FR3'
WHERE (CODIGO = 'VTAS010206');

UPDATE REPORTES SET
    ARCHIVO = 'R_VTA_RES_02A.FR3'
WHERE (CODIGO = 'VTAS010201');

UPDATE REPORTES SET 
    ARCHIVO = 'R_VTA_RES_05A.FR3'
WHERE (CODIGO = 'VTAS010502');

UPDATE REPORTES SET 
    ARCHIVO = 'R_VTA_RES_03A.FR3'
WHERE (CODIGO = 'VTAS010304');

UPDATE REPORTES SET 
    ARCHIVO = 'R_VTA_RES_07A.FR3'
WHERE (CODIGO = 'VTAS010805');

UPDATE REPORTES SET 
    ARCHIVO = 'R_VTA_RES_04A.FR3'
WHERE (CODIGO = 'VTAS010404');

UPDATE REPORTES SET 
    ARCHIVO = 'R_VTA_RES_02C.FR3'
WHERE (CODIGO = 'VTAS010205');

UPDATE REPORTES SET 
    ARCHIVO = 'R_VTA_RES_08A.FR3'
WHERE (CODIGO = 'VTAS010905');

UPDATE REPORTES SET 
    ARCHIVO = 'R_VTA_RES_06A.FR3'
WHERE (CODIGO = 'VTAS010604');

UPDATE REPORTES SET 
    ARCHIVO = 'R_COM_RES_01B.FR3'
WHERE (CODIGO = 'COMP010102');

UPDATE REPORTES SET 
    ARCHIVO = 'R_COM_RES_02B.FR3'
WHERE (CODIGO = 'COMP010202');

UPDATE REPORTES SET 
    ARCHIVO = 'R_COM_RES_02D.FR3'
WHERE (CODIGO = 'COMP010206');

UPDATE REPORTES SET 
    ARCHIVO = 'R_COM_RES_02A.FR3'
WHERE (CODIGO = 'COMP010201');

UPDATE REPORTES SET 
    ARCHIVO = 'R_COM_RES_03A.FR3'
WHERE (CODIGO = 'COMP010304');

UPDATE REPORTES SET 
    ARCHIVO = 'R_COM_RES_04A.FR3'
WHERE (CODIGO = 'COMP010404');

UPDATE REPORTES SET 
    ARCHIVO = 'R_COM_RES_06A.FR3'
WHERE (CODIGO = 'COMP010604');

UPDATE REPORTES SET 
    ARCHIVO = 'R_COM_RES_07A.FR3'
WHERE (CODIGO = 'COMP010805');

UPDATE REPORTES SET 
    ARCHIVO = 'R_COM_RES_08A.FR3'
WHERE (CODIGO = 'COMP010905');

UPDATE REPORTES SET 
    ARCHIVO = 'R_COM_RES_01A.FR3'
WHERE (CODIGO = 'COMP010101');

UPDATE REPORTES SET 
    ARCHIVO = 'R_COM_RES_05A.FR3'
WHERE (CODIGO = 'COMP010502');

UPDATE REPORTES SET 
    ARCHIVO = 'R_COM_RES_02C.FR3'
WHERE (CODIGO = 'COMP010205');



COMMIT WORK;



/******************************************************************************/
/***                           Stored Procedures                            ***/
/******************************************************************************/

/* Nomina */
SET TERM ^ ; 

CREATE OR ALTER PROCEDURE PZ_EX_SAL_BASICO
RETURNS (
    EMPLEADO CHAR(15),
    VALOR NUMERIC(17,4))
AS
BEGIN
  SUSPEND;
END^


CREATE OR ALTER PROCEDURE PZ_EX_SAL_IBC (
    NOMINA_ VARCHAR(5))
RETURNS (
    EMPLEADO VARCHAR(15),
    VALOR DOUBLE PRECISION)
AS
BEGIN
  SUSPEND;
END^


CREATE OR ALTER PROCEDURE PZ_NE_FONDO_SP (
    RECEPTOR_ VARCHAR(15),
    FECHA_DESDE_ DATE,
    FECHA_HASTA_ DATE)
RETURNS (
    "noov:Nvfsp_porc" VARCHAR(10),
    "noov:Nvfsp_dedu" DOUBLE PRECISION,
    "noov:Nvfsp_posb" DOUBLE PRECISION,
    "noov:Nvfsp_desb" DOUBLE PRECISION)
AS
BEGIN
  SUSPEND;
END^


CREATE OR ALTER PROCEDURE PZ_SAL_BASICO (
    EMPLEADO_ VARCHAR(15),
    DESDE_ DATE,
    HASTA_ DATE,
    COTIDIANA_ CHAR(1),
    DIAS_NOM_ INTEGER)
RETURNS (
    EMPLEADO VARCHAR(15),
    VALOR DOUBLE PRECISION)
AS
BEGIN
  SUSPEND;
END^


SET TERM ; ^



/******************************************************************************/
/***                           Stored Procedures                            ***/
/******************************************************************************/

/* Nomina */
SET TERM ^ ;

CREATE OR ALTER PROCEDURE PZ_EX_SAL_BASICO
RETURNS (
    EMPLEADO CHAR(15),
    VALOR NUMERIC(17,4))
AS
DECLARE VARIABLE V_Nomina    VARCHAR(5);
DECLARE VARIABLE V_Desde_Nom DATE;
DECLARE VARIABLE V_Hasta_Nom DATE;
DECLARE VARIABLE V_Cotidiana VARCHAR(1);
DECLARE VARIABLE V_Dias_Nom  DOUBLE PRECISION;
BEGIN
  SELECT Rdb$Get_Context('USER_SESSION', 'G_NOMINA')
  FROM Rdb$Database
  INTO V_Nomina;

  SELECT N.Desde, N.Hasta, N.Dias, L.Cotidiana
  FROM Nominas N
  JOIN Liquidaciones L ON (N.Codliquidacion = L.Codigo)
  WHERE N.Codigo = :V_Nomina
  INTO V_Desde_Nom, V_Hasta_Nom, V_Dias_Nom, V_Cotidiana;

  FOR SELECT T.Codpersonal
      FROM Tmp_Empleados T
      INTO Empleado
  DO
  BEGIN
    SELECT Empleado, Valor
    FROM Pz_Sal_Basico(:Empleado, :V_Desde_Nom, :V_Hasta_Nom, :V_Cotidiana, :V_Dias_Nom)
    INTO Empleado, Valor;
    SUSPEND;
  END
END^


CREATE OR ALTER PROCEDURE PZ_EX_SAL_IBC (
    NOMINA_ VARCHAR(5))
RETURNS (
    EMPLEADO VARCHAR(15),
    VALOR DOUBLE PRECISION)
AS
DECLARE VARIABLE V_Desde_Nom  DATE;
DECLARE VARIABLE V_Hasta_Nom  DATE;
DECLARE VARIABLE V_Fecha_Ing  DATE;
DECLARE VARIABLE V_Fecha_Ret  DATE;
DECLARE VARIABLE V_Fecha_Nom  DATE;
DECLARE VARIABLE V_Dias_Nom   DOUBLE PRECISION;
DECLARE VARIABLE V_Dias1      DOUBLE PRECISION;
DECLARE VARIABLE V_Dias2      DOUBLE PRECISION;
DECLARE VARIABLE V_Basico2    DOUBLE PRECISION;
DECLARE VARIABLE V_Cotidiana  VARCHAR(1);
DECLARE VARIABLE V_Cotizante  VARCHAR(2);
DECLARE VARIABLE V_Ultimo_Dia INTEGER;
BEGIN
  SELECT N.Fecha,  N.Dias, L.Cotidiana
  FROM Nominas N
  JOIN Liquidaciones L ON (N.Codliquidacion = L.Codigo)
  WHERE N.Codigo = :Nomina_
  INTO V_Fecha_Nom,  V_Dias_Nom, V_Cotidiana;

  V_Desde_Nom = '01.' || EXTRACT(MONTH FROM V_Fecha_Nom) || '.' || EXTRACT(YEAR FROM V_Fecha_Nom);
  V_Ultimo_Dia = EXTRACT(DAY FROM DATEADD(MONTH, 1, CAST('01.' || EXTRACT(MONTH FROM :V_Fecha_Nom) || '.' || EXTRACT(YEAR FROM :V_Fecha_Nom) AS DATE)) - 1);

  IF (V_Ultimo_Dia < 30) THEN
    V_Hasta_Nom = CAST(V_Ultimo_Dia AS CHAR(2)) || '.' || EXTRACT(MONTH FROM V_Fecha_Nom) || '.' || EXTRACT(YEAR FROM V_Fecha_Nom);
  ELSE
    V_Hasta_Nom = '30.' || EXTRACT(MONTH FROM V_Fecha_Nom) || '.' || EXTRACT(YEAR FROM V_Fecha_Nom);

  FOR SELECT T.Codpersonal, P.Codcotizante
      FROM Tmp_Empleados T
      JOIN Personal P ON (T.Codpersonal = P.Codigo)
      INTO Empleado, V_Cotizante
  DO
  BEGIN
    IF (V_Cotizante = '23') THEN
    BEGIN
      Valor = 0;
      SUSPEND;
    END
    ELSE
    BEGIN
      SELECT MAX(S.Desde)
      FROM Salarios S
      WHERE S.Codpersonal = :Empleado
            AND S.Desde <= :V_Hasta_Nom
            AND S.Columna_Desde = 'INGRESO'
      INTO V_Fecha_Ing;

      V_Fecha_Ret = NULL;

      SELECT FIRST 1 S.Hasta
      FROM Salarios S
      WHERE Codpersonal = :Empleado
            AND S.Columna_Hasta = 'RETIRO'
            AND S.Hasta >= :V_Desde_Nom
            AND S.Hasta <= :V_Hasta_Nom
            AND S.Hasta >= :V_Fecha_Ing
      ORDER BY Hasta DESC
      INTO V_Fecha_Ret;
      V_Fecha_Ret = COALESCE(V_Fecha_Ret, V_Hasta_Nom);

      IF (V_Cotidiana = 'N') THEN
      BEGIN
        SELECT FIRST 1 Basico
        FROM Nom_Pila_Salarios(:Empleado, :V_Fecha_Ret, :V_Fecha_Ret)
        INTO Valor;
      END
      ELSE
      BEGIN
        IF (V_Fecha_Ing > V_Desde_Nom) THEN
          SELECT FIRST 1 Basico
          FROM Nom_Pila_Salarios(:Empleado, :V_Fecha_Ing, :V_Fecha_Ing)
          INTO Valor;
        ELSE
          SELECT FIRST 1 Basico
          FROM Nom_Pila_Salarios(:Empleado, :V_Desde_Nom, :V_Desde_Nom)
          INTO Valor;
        IF (V_Fecha_Ret < V_Hasta_Nom) THEN
          SELECT FIRST 1 Basico
          FROM Nom_Pila_Salarios(:Empleado, :V_Fecha_Ret, :V_Fecha_Ret)
          INTO V_Basico2;
        ELSE
          SELECT FIRST 1 Basico
          FROM Nom_Pila_Salarios(:Empleado, :V_Hasta_Nom, :V_Hasta_Nom)
          INTO V_Basico2;
        IF (V_Dias_Nom > 0) THEN
        BEGIN
          IF (Valor <> V_Basico2) THEN
          BEGIN
            SELECT FIRST 1 Dias
            FROM Nom_Pila_Multisalarios(:Empleado, :V_Desde_Nom, :V_Hasta_Nom)
            ORDER BY Desde
            INTO V_Dias1;

            V_Dias2 = 30 - V_Dias1;
            Valor = Valor / 30 * V_Dias1;
            V_Basico2 = V_Basico2 / 30 * V_Dias2;
            Valor = Valor + V_Basico2;
          END
        END
        ELSE
          Valor = COALESCE(V_Basico2, 0);
      END
      SUSPEND;
    END
  END
END^


CREATE OR ALTER PROCEDURE PZ_NE_FONDO_SP (
    RECEPTOR_ VARCHAR(15),
    FECHA_DESDE_ DATE,
    FECHA_HASTA_ DATE)
RETURNS (
    "noov:Nvfsp_porc" VARCHAR(10),
    "noov:Nvfsp_dedu" DOUBLE PRECISION,
    "noov:Nvfsp_posb" DOUBLE PRECISION,
    "noov:Nvfsp_desb" DOUBLE PRECISION)
AS
DECLARE VARIABLE V_Sal_Base  DOUBLE PRECISION;
DECLARE VARIABLE V_Sal_Mini  DOUBLE PRECISION;
DECLARE VARIABLE V_Codigo_Ne VARCHAR(10);
DECLARE VARIABLE V_Valor     DOUBLE PRECISION;
DECLARE VARIABLE V_Psub      DOUBLE PRECISION;
DECLARE VARIABLE V_Valor_Sub DOUBLE PRECISION;
BEGIN
  SELECT FIRST 1 Valor
  FROM Pz_Sal_Basico(:Receptor_, :Fecha_Desde_, :Fecha_Hasta_, 'S', 30)
  INTO V_Sal_Base;

  SELECT FIRST 1 Valor
  FROM Pz_Ne_Grupo_Constante(:Receptor_, EXTRACT(YEAR FROM CAST(:Fecha_Desde_ AS DATE)))
  WHERE Codgrupo_Ne = 'SMLV'
  INTO V_Sal_Mini;

  --SPDTO
  SELECT FIRST 1 Codigo_Ne,
         Valor
  FROM Pz_Ne_Grupo(:Receptor_, :Fecha_Desde_, :Fecha_Hasta_, 'SPDTO')
  INTO V_Codigo_Ne,
       V_Valor;

  --SUBDTO
  V_Psub = CASE
             WHEN :V_Sal_Base > :V_Sal_Mini * 20 THEN (SELECT Valor
                                                       FROM Pz_Ne_Grupo_Constante(:Receptor_, EXTRACT(YEAR FROM CAST(:Fecha_Desde_ AS DATE)))
                                                       WHERE Codgrupo_Ne = 'PORC_SUB5')
             WHEN :V_Sal_Base >= :V_Sal_Mini * 19 THEN (SELECT Valor
                                                        FROM Pz_Ne_Grupo_Constante(:Receptor_, EXTRACT(YEAR FROM CAST(:Fecha_Desde_ AS DATE)))
                                                        WHERE Codgrupo_Ne = 'PORC_SUB4')
             WHEN :V_Sal_Base >= :V_Sal_Mini * 18 THEN (SELECT Valor
                                                        FROM Pz_Ne_Grupo_Constante(:Receptor_, EXTRACT(YEAR FROM CAST(:Fecha_Desde_ AS DATE)))
                                                        WHERE Codgrupo_Ne = 'PORC_SUB3')
             WHEN :V_Sal_Base >= :V_Sal_Mini * 17 THEN (SELECT Valor
                                                        FROM Pz_Ne_Grupo_Constante(:Receptor_, EXTRACT(YEAR FROM CAST(:Fecha_Desde_ AS DATE)))
                                                        WHERE Codgrupo_Ne = 'PORC_SUB2')
             WHEN :V_Sal_Base >= :V_Sal_Mini * 16 THEN (SELECT Valor
                                                        FROM Pz_Ne_Grupo_Constante(:Receptor_, EXTRACT(YEAR FROM CAST(:Fecha_Desde_ AS DATE)))
                                                        WHERE Codgrupo_Ne = 'PORC_SUB1')
             ELSE 0
           END;

  SELECT FIRST 1 Valor
  FROM Pz_Ne_Grupo(:Receptor_, :Fecha_Desde_, :Fecha_Hasta_, 'SUBDTO')
  INTO V_Valor_Sub;

  "noov:Nvfsp_porc" = COALESCE(V_Codigo_Ne, 0);
  "noov:Nvfsp_dedu" = COALESCE(V_Valor, 0);
  IF (V_Valor_Sub > 0) THEN
    "noov:Nvfsp_posb" = COALESCE(V_Psub, 0) + 0.5;
  "noov:Nvfsp_posb" = COALESCE("noov:Nvfsp_posb", 0);
  "noov:Nvfsp_desb" = COALESCE(V_Valor_Sub, 0);
  SUSPEND;
END^


CREATE OR ALTER PROCEDURE PZ_SAL_BASICO (
    EMPLEADO_ VARCHAR(15),
    DESDE_ DATE,
    HASTA_ DATE,
    COTIDIANA_ CHAR(1),
    DIAS_NOM_ INTEGER)
RETURNS (
    EMPLEADO VARCHAR(15),
    VALOR DOUBLE PRECISION)
AS
DECLARE VARIABLE V_Fecha_Ing DATE;
DECLARE VARIABLE V_Fecha_Ret DATE;
DECLARE VARIABLE V_Dias1     DOUBLE PRECISION;
DECLARE VARIABLE V_Dias2     DOUBLE PRECISION;
DECLARE VARIABLE V_Basico2   DOUBLE PRECISION;
DECLARE VARIABLE V_Cotizante VARCHAR(2);
BEGIN
  Empleado = Empleado_;

  --PERSONAL
  SELECT Codcotizante
  FROM Personal
  WHERE Codigo = :Empleado
  INTO V_Cotizante;

  IF (V_Cotizante = '23') THEN
  BEGIN
    Valor = 0;
    SUSPEND;
  END
  ELSE
  BEGIN
    SELECT MAX(S.Desde)
    FROM Salarios S
    WHERE S.Codpersonal = :Empleado
          AND S.Desde <= :Hasta_
          AND S.Columna_Desde = 'INGRESO'
    INTO V_Fecha_Ing;

    V_Fecha_Ret = NULL;

    SELECT FIRST 1 S.Hasta
    FROM Salarios S
    WHERE Codpersonal = :Empleado
          AND S.Columna_Hasta = 'RETIRO'
          AND S.Hasta >= :Desde_
          AND S.Hasta <= :Hasta_
          AND S.Hasta >= :V_Fecha_Ing
    ORDER BY Hasta DESC
    INTO V_Fecha_Ret;
    V_Fecha_Ret = COALESCE(V_Fecha_Ret, Hasta_);

    IF (Cotidiana_ = 'N') THEN
    BEGIN
      SELECT FIRST 1 Basico
      FROM Nom_Pila_Salarios(:Empleado, :V_Fecha_Ret, :V_Fecha_Ret)
      INTO Valor;
    END
    ELSE
    BEGIN
      IF (V_Fecha_Ing > Desde_) THEN
        SELECT FIRST 1 Basico
        FROM Nom_Pila_Salarios(:Empleado, :V_Fecha_Ing, :V_Fecha_Ing)
        INTO Valor;
      ELSE
        SELECT FIRST 1 Basico
        FROM Nom_Pila_Salarios(:Empleado, :Desde_, :Desde_)
        INTO Valor;

      IF (V_Fecha_Ret < Hasta_) THEN
        SELECT FIRST 1 Basico
        FROM Nom_Pila_Salarios(:Empleado, :V_Fecha_Ret, :V_Fecha_Ret)
        INTO V_Basico2;
      ELSE
        SELECT FIRST 1 Basico
        FROM Nom_Pila_Salarios(:Empleado, :Hasta_, :Hasta_)
        INTO V_Basico2;

      IF (Dias_Nom_ > 0) THEN
      BEGIN
        IF (Valor <> V_Basico2) THEN
        BEGIN
          SELECT FIRST 1 Dias
          FROM Nom_Pila_Multisalarios(:Empleado, :Desde_, :Hasta_)
          ORDER BY Desde
          INTO V_Dias1;
          IF (V_Dias1 > Dias_Nom_) THEN
            V_Dias1 = V_Dias1 - Dias_Nom_;
          V_Dias2 = Dias_Nom_ - V_Dias1;
          Valor = Valor / Dias_Nom_ * V_Dias1;
          V_Basico2 = V_Basico2 / Dias_Nom_ * V_Dias2;
          Valor = Valor + V_Basico2;
        END
      END
      ELSE
        Valor = COALESCE(V_Basico2, 0);
    END
    SUSPEND;
  END
END^



SET TERM ; ^


COMMIT WORK;


SET TERM ^ ;

CREATE OR ALTER PROCEDURE PZ_NOM_NOMINA (
    NOMINA VARCHAR(5))
RETURNS (
    EMPLEADO VARCHAR(15),
    NOMBRE_EMPLEADO VARCHAR(83),
    ESQNOMINA VARCHAR(5),
    BANCO VARCHAR(80),
    DIRECCION VARCHAR(80),
    PERCENTRO VARCHAR(5),
    TEL VARCHAR(15),
    CEL VARCHAR(15),
    NOM1 VARCHAR(20),
    NOM2 VARCHAR(20),
    NOMCARGO VARCHAR(80),
    BASICO DOUBLE PRECISION,
    PENSION VARCHAR(80),
    SALUD VARCHAR(80),
    MUNICIPIO VARCHAR(80),
    RUBRO VARCHAR(5),
    NOMRUBRO VARCHAR(80),
    COLUMNA VARCHAR(4),
    VALOR DOUBLE PRECISION,
    ADICION DOUBLE PRECISION,
    DEDUCCION DOUBLE PRECISION)
AS
BEGIN
  SUSPEND;
END^






SET TERM ; ^



SET TERM ^ ;

CREATE OR ALTER PROCEDURE PZ_NOM_NOMINA (
    NOMINA VARCHAR(5))
RETURNS (
    EMPLEADO VARCHAR(15),
    NOMBRE_EMPLEADO VARCHAR(83),
    ESQNOMINA VARCHAR(5),
    BANCO VARCHAR(80),
    DIRECCION VARCHAR(80),
    PERCENTRO VARCHAR(5),
    TEL VARCHAR(15),
    CEL VARCHAR(15),
    NOM1 VARCHAR(20),
    NOM2 VARCHAR(20),
    NOMCARGO VARCHAR(80),
    BASICO DOUBLE PRECISION,
    PENSION VARCHAR(80),
    SALUD VARCHAR(80),
    MUNICIPIO VARCHAR(80),
    RUBRO VARCHAR(5),
    NOMRUBRO VARCHAR(80),
    COLUMNA VARCHAR(4),
    VALOR DOUBLE PRECISION,
    ADICION DOUBLE PRECISION,
    DEDUCCION DOUBLE PRECISION)
AS
DECLARE VARIABLE Codcargo VARCHAR(5);
DECLARE VARIABLE V_Desde  DATE;
DECLARE VARIABLE V_Hasta  DATE;
DECLARE VARIABLE V_Fecha  DATE;
DECLARE VARIABLE V_Ciudad VARCHAR(5);
BEGIN

  FOR SELECT TRIM(Empleado),
             Esquema,
             Rubro,
             TRIM(Nombre_Rubro),
             Valor,
             Adicion,
             Deduccion
      FROM Nom_Nomina(:Nomina)
      WHERE Adicion + Deduccion + Valor <> 0
      INTO Empleado,
           Esqnomina,
           Rubro,
           Nomrubro,
           Valor,
           Adicion,
           Deduccion
  DO
  BEGIN
    -- Personal
    SELECT TRIM(Nombre),
           TRIM(Banco),
           TRIM(Direccion),
           Codcentro,
           Codcargo,
           Telefono,
           Movil,
           TRIM(Nom1),
           TRIM(Nom2),
           Codmunicipio
    FROM Personal
    WHERE Codigo = :Empleado
    INTO Nombre_Empleado,
         Banco,
         Direccion,
         Percentro,
         Codcargo,
         Tel,
         Cel,
         Nom1,
         Nom2,
         V_Ciudad;

    -- Cargo
    SELECT Nombre
    FROM Cargos
    WHERE Codigo = :Codcargo
    INTO Nomcargo;

    -- Fechas nomina
    SELECT Desde,
           Hasta,
           Fecha
    FROM Nominas
    WHERE Codigo = :Nomina
    INTO V_Desde,
         V_Hasta,
         V_Fecha;

    -- Basico
    SELECT MAX(Basico)
    FROM Nom_Pila_Salarios(:Empleado, :V_Desde, :V_Hasta)
    INTO Basico;

    -- Fondo pension
    SELECT FIRST 1 T.Nombre
    FROM Aportes_Personal P
    JOIN Tipoaportes T ON (P.Codtipoaporte = T.Codigo AND
          :V_Fecha BETWEEN P.Desde AND P.Hasta)
    WHERE Codcomponente = '03'
          AND P.Codpersonal = :Empleado
    INTO Pension;

    -- EPS
    SELECT FIRST 1 T.Nombre
    FROM Aportes_Personal P
    JOIN Tipoaportes T ON (P.Codtipoaporte = T.Codigo AND
          :V_Fecha BETWEEN P.Desde AND P.Hasta)
    WHERE Codcomponente = '04'
          AND P.Codpersonal = :Empleado
    INTO Salud;

    -- Ciudad
    SELECT Nombre
    FROM Municipios
    WHERE Codigo = :V_Ciudad
    INTO Municipio;

    -- Rubros
    SELECT Columna
    FROM Rubros
    WHERE Codigo = :Rubro
    INTO Columna;

    /* Manejo especial para los rubros de dias y horas */
    IF ((COALESCE(Columna, '') >= 'C_00' AND
        COALESCE(Columna, '') <= 'C_98') OR (COALESCE(Columna, '') >= 'B_00' AND
        COALESCE(Columna, '') <= 'B_98')) THEN
    BEGIN

      IF (Adicion > 0) THEN
        IF (SUBSTRING(TRIM(Nomrubro) FROM 1 FOR 4) = 'DIAS') THEN
        BEGIN
          Valor = Adicion;
          Adicion = 0;
        END
        ELSE
        IF (SUBSTRING(TRIM(Nomrubro) FROM 1 FOR 4) <> 'HORA') THEN
          Valor = 0;

      IF (Deduccion > 0) THEN
        IF (SUBSTRING(TRIM(Nomrubro) FROM 1 FOR 4) = 'DIAS') THEN
        BEGIN
          Valor = Deduccion;
          Deduccion = 0;
        END
        ELSE
        IF (SUBSTRING(TRIM(Nomrubro) FROM 1 FOR 4) <> 'HORA') THEN
          Valor = 0;
    END

    IF (COALESCE(Columna, '') <> '') THEN
      SUSPEND;
  END
END^



SET TERM ; ^

COMMIT WORK;


/* Representacion Grafica Nomina Electronica */
UPDATE PTA SET 
    XML_PDF = '<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"
    xmlns:noov="http://Noova/">
    <soap:Header>
        <noov:AuthHeader>
            <noov:Username>UsuarioNoova</noov:Username>
            <noov:Password>ClaveNoova</noov:Password>
        </noov:AuthHeader>
    </soap:Header>
    <soap:Body>
        <noov:SetPdf>
            <noov:documento>
                <noov:Nvemp_nnit>MK_EMISOR</noov:Nvemp_nnit>
                <noov:Nvnom_cont>NumeroNoova</noov:Nvnom_cont>
                <noov:Nvnom_cune>MK_CUFE</noov:Nvnom_cune>
                <noov:Nvnom_file>PdfNoova</noov:Nvnom_file>
            </noov:documento>
        </noov:SetPdf>
    </soap:Body>
</soap:Envelope>'
WHERE (CODIGO = 'NOOVA_NE');


UPDATE Documentos Doc
SET Doc.Formato = 'F_NE_NOM_01A.FR3'
WHERE Doc.Codigo = 'NE';

/**************************/
--Sub Actividad para pila C_98
UPDATE OR INSERT INTO CONSTANTES (CODIGO, NOMBRE, COLUMNA_PILA, COLUMNA_TEXTO, CODGRUPO_NE, NATIVO)
                          VALUES ('C45_SUBP', 'SUBACTIVIDAD ECONOMICA EN IGUAL CLASE DE RIESGO', 'C_98', NULL, NULL, 'S')
                        ;

COMMIT WORK;

UPDATE OR INSERT INTO CONSTANTESVALOR (CODCONSTANTE, ANO, VALOR, TEXTO)
                               VALUES ('C45_SUBP', '2021', 0, NULL)
                             ;
UPDATE OR INSERT INTO CONSTANTESVALOR (CODCONSTANTE, ANO, VALOR, TEXTO)
                               VALUES ('C45_SUBP', '2020', 0, NULL)
                             ;
UPDATE OR INSERT INTO CONSTANTESVALOR (CODCONSTANTE, ANO, VALOR, TEXTO)
                               VALUES ('C45_SUBP', '2022', 0, NULL)
                             ;
UPDATE OR INSERT INTO CONSTANTESVALOR (CODCONSTANTE, ANO, VALOR, TEXTO)
                               VALUES ('C45_SUBP', '2019', 0, NULL)
                             ;

COMMIT WORK;

UPDATE Componentes
SET Sql = '--DETALLE PILA

SELECT C_01, C_02, C_03, C_04, C_05, C_06, C_07, C_08, C_09, C_10, C_11, C_12,
       C_13, C_14, C_15, C_16, C_17, C_18, C_19, C_20, C_21, C_22, C_23, C_24,
       C_25, C_26, C_27, C_28, C_29, C_30, C_31, C_32, C_33, C_34, C_35, C_36,
       C_37, C_38, C_39, C_40, C_41, C_42, C_43, C_44, C_45, C_46, C_47, C_48,
       C_49, C_50, C_51, C_52, C_53, C_54, C_55, C_56, C_57, C_58, C_59, C_60,
       C_61, C_62, C_63, C_64, C_65, C_66, C_67, C_68, C_69, C_70, C_71, C_72,
       C_73, C_74, C_75, C_76, C_77, C_78, C_79, C_80, C_81, C_82, C_83, C_84,
       C_85, C_86, C_87, C_88, C_89, C_90, C_91, C_92, C_93, C_94, C_95, C_96,
       C_97, C_98
FROM Pz_Pila_02_Mp
ORDER BY C_02'
WHERE Sql CONTAINING 'Pz_Pila_02_Mp'
      AND Codigo = '02';

UPDATE Componentes
SET Sql = '--DETALLE PILA

SELECT C_01, C_02, C_03, C_04, C_05, C_06, C_07, C_08, C_09, C_10, C_11, C_12,
       C_13, C_14, C_15, C_16, C_17, C_18, C_19, C_20, C_21, C_22, C_23, C_24,
       C_25, C_26, C_27, C_28, C_29, C_30, C_31, C_32, C_33, C_34, C_35, C_36,
       C_37, C_38, C_39, C_40, C_41, C_42, C_43, C_44, C_45, C_46, C_47, C_48,
       C_49, C_50, C_51, C_52, C_53, C_54, C_55, C_56, C_57, C_58, C_59, C_60,
       C_61, C_62, C_63, C_64, C_65, C_66, C_67, C_68, C_69, C_70, C_71, C_72,
       C_73, C_74, C_75, C_76, C_77, C_78, C_79, C_80, C_81, C_82, C_83, C_84,
       C_85, C_86, C_87, C_88, C_89, C_90, C_91, C_92, C_93, C_94, C_95, C_96,
       C_97, C_98
FROM Pz_Pila_02
ORDER BY C_02'
WHERE Sql NOT CONTAINING 'Pz_Pila_02_Mp'
      AND Codigo = '02';

COMMIT WORK;


CREATE OR ALTER PROCEDURE Pz_Pila_02
RETURNS (
    C_01 CHAR(2),
    C_02 INTEGER,
    C_03 CHAR(2),
    C_04 CHAR(16),
    C_05 CHAR(2),
    C_06 CHAR(2),
    C_07 CHAR(1),
    C_08 CHAR(1),
    C_09 CHAR(5),
    C_10 CHAR(5),
    C_11 CHAR(20),
    C_12 CHAR(30),
    C_13 CHAR(20),
    C_14 CHAR(30),
    C_15 CHAR(1),
    C_16 CHAR(1),
    C_17 CHAR(1),
    C_18 CHAR(1),
    C_19 CHAR(1),
    C_20 CHAR(1),
    C_21 CHAR(1),
    C_22 CHAR(1),
    C_23 CHAR(1),
    C_24 CHAR(1),
    C_25 CHAR(1),
    C_26 CHAR(1),
    C_27 CHAR(1),
    C_28 CHAR(1),
    C_29 CHAR(1),
    C_30 INTEGER,
    C_31 CHAR(6),
    C_32 CHAR(6),
    C_33 CHAR(6),
    C_34 CHAR(6),
    C_35 CHAR(6),
    C_36 DOUBLE PRECISION,
    C_37 DOUBLE PRECISION,
    C_38 DOUBLE PRECISION,
    C_39 DOUBLE PRECISION,
    C_40 DOUBLE PRECISION,
    C_41 CHAR(1),
    C_42 DOUBLE PRECISION,
    C_43 DOUBLE PRECISION,
    C_44 DOUBLE PRECISION,
    C_45 DOUBLE PRECISION,
    C_46 VARCHAR(7),
    C_47 DOUBLE PRECISION,
    C_48 DOUBLE PRECISION,
    C_49 DOUBLE PRECISION,
    C_50 DOUBLE PRECISION,
    C_51 DOUBLE PRECISION,
    C_52 DOUBLE PRECISION,
    C_53 DOUBLE PRECISION,
    C_54 VARCHAR(7),
    C_55 DOUBLE PRECISION,
    C_56 DOUBLE PRECISION,
    C_57 CHAR(15),
    C_58 DOUBLE PRECISION,
    C_59 CHAR(15),
    C_60 DOUBLE PRECISION,
    C_61 VARCHAR(9),
    C_62 CHAR(10),
    C_63 DOUBLE PRECISION,
    C_64 VARCHAR(7),
    C_65 DOUBLE PRECISION,
    C_66 VARCHAR(7),
    C_67 DOUBLE PRECISION,
    C_68 VARCHAR(7),
    C_69 DOUBLE PRECISION,
    C_70 VARCHAR(7),
    C_71 DOUBLE PRECISION,
    C_72 VARCHAR(7),
    C_73 DOUBLE PRECISION,
    C_74 CHAR(2),
    C_75 CHAR(16),
    C_76 CHAR(1),
    C_77 CHAR(6),
    C_78 CHAR(1),
    C_79 CHAR(1),
    C_80 CHAR(10),
    C_81 CHAR(10),
    C_82 CHAR(10),
    C_83 CHAR(10),
    C_84 CHAR(10),
    C_85 CHAR(10),
    C_86 CHAR(10),
    C_87 CHAR(10),
    C_88 CHAR(10),
    C_89 CHAR(10),
    C_90 CHAR(10),
    C_91 CHAR(10),
    C_92 CHAR(10),
    C_93 CHAR(10),
    C_94 CHAR(10),
    C_95 DOUBLE PRECISION,
    C_96 DOUBLE PRECISION,
    C_97 CHAR(10),
    C_98 CHAR(7))
AS
DECLARE VARIABLE Tipo          CHAR(2);
DECLARE VARIABLE Codpersonal   CHAR(15);
DECLARE VARIABLE V_Sena        CHAR(1);
DECLARE VARIABLE V_Pensionado  CHAR(1);
DECLARE VARIABLE V_Parcial     CHAR(1);
DECLARE VARIABLE V_Lectivo     CHAR(1);
DECLARE VARIABLE V_Ciudad      VARCHAR(5);
DECLARE VARIABLE V_Exonerado   CHAR(1);
DECLARE VARIABLE V_Unico       CHAR(1);
DECLARE VARIABLE V_Alto_Riesgo CHAR(1);
DECLARE VARIABLE T_36          DOUBLE PRECISION;
DECLARE VARIABLE T_37          DOUBLE PRECISION;
DECLARE VARIABLE T_38          DOUBLE PRECISION;
DECLARE VARIABLE V_C_54        DOUBLE PRECISION;
DECLARE VARIABLE V_C_64        DOUBLE PRECISION;
DECLARE VARIABLE V_C_66        DOUBLE PRECISION;
DECLARE VARIABLE V_C_68        DOUBLE PRECISION;
DECLARE VARIABLE V_C_70        DOUBLE PRECISION;
DECLARE VARIABLE V_C_72        DOUBLE PRECISION;
DECLARE VARIABLE V_C_61        DOUBLE PRECISION;
DECLARE VARIABLE V_C_46        DOUBLE PRECISION;
DECLARE VARIABLE T_39          DOUBLE PRECISION;
DECLARE VARIABLE V_C_80        DATE;
DECLARE VARIABLE V_C_81        DATE;
DECLARE VARIABLE V_C_82        DATE;
DECLARE VARIABLE V_C_83        DATE;
DECLARE VARIABLE V_C_84        DATE;
DECLARE VARIABLE V_C_85        DATE;
DECLARE VARIABLE V_C_86        DATE;
DECLARE VARIABLE V_C_87        DATE;
DECLARE VARIABLE V_C_88        DATE;
DECLARE VARIABLE V_C_89        DATE;
DECLARE VARIABLE V_C_90        DATE;
DECLARE VARIABLE V_C_91        DATE;
DECLARE VARIABLE V_C_92        DATE;
DECLARE VARIABLE V_C_93        DATE;
DECLARE VARIABLE V_C_94        DATE;
DECLARE VARIABLE C_00          DOUBLE PRECISION;
DECLARE VARIABLE B_00          DOUBLE PRECISION;
DECLARE VARIABLE B_01          DOUBLE PRECISION;
DECLARE VARIABLE B_02          DOUBLE PRECISION;
DECLARE VARIABLE B_03          DOUBLE PRECISION;
DECLARE VARIABLE B_04          DOUBLE PRECISION;
DECLARE VARIABLE B_05          DOUBLE PRECISION;
DECLARE VARIABLE B_06          DOUBLE PRECISION;
DECLARE VARIABLE B_07          DOUBLE PRECISION;
DECLARE VARIABLE B_08          DOUBLE PRECISION;
DECLARE VARIABLE B_09          DOUBLE PRECISION;
DECLARE VARIABLE B_10          DOUBLE PRECISION;
DECLARE VARIABLE B_11          DOUBLE PRECISION;
DECLARE VARIABLE B_12          DOUBLE PRECISION;
DECLARE VARIABLE B_13          DOUBLE PRECISION;
DECLARE VARIABLE B_14          DOUBLE PRECISION;
DECLARE VARIABLE B_15          DOUBLE PRECISION;
DECLARE VARIABLE B_16          DOUBLE PRECISION;
DECLARE VARIABLE B_17          DOUBLE PRECISION;
DECLARE VARIABLE B_18          DOUBLE PRECISION;
DECLARE VARIABLE B_19          DOUBLE PRECISION;
DECLARE VARIABLE V_Actividad   CHAR(5);
DECLARE VARIABLE V_Actividad_C CHAR(5);
DECLARE VARIABLE V_Actividad_T CHAR(5);
DECLARE VARIABLE V_Subp        INTEGER;
DECLARE VARIABLE V_Subpl       INTEGER;
DECLARE VARIABLE V_Desde       DATE;
BEGIN
  SELECT TRIM(Codmunicipio), TRIM(Codactividad)
  FROM Terceros
  WHERE Datos_Empresa = 'S'
  INTO V_Ciudad, V_Actividad_T;

  FOR SELECT Desde, Tipo, Codpersonal, C_00, C_01, C_02, C_03, C_04, C_05, C_06,
             C_07, C_08, C_09, C_10, C_11, C_12, C_13, C_14, C_15, C_16, C_17,
             C_18, C_19, C_20, C_21, C_22, C_23, C_24, C_25, C_26, C_27, C_28,
             C_29, C_30, C_31, C_32, C_33, C_34, C_35, C_36, C_37, C_38, C_39,
             C_40, C_41, C_42, C_43, C_44, C_45, C_46, C_47, C_48, C_49, C_50,
             C_51, C_52, C_53, C_54, C_55, C_56, C_57, C_58, C_59, C_60, C_61,
             C_62, C_63, C_64, C_65, C_66, C_67, C_68, C_69, C_70, C_71, C_72,
             C_73, C_74, C_75, C_76, C_77, C_78, C_79, C_80, C_81, C_82, C_83,
             C_84, C_85, C_86, C_87, C_88, C_89, C_90, C_91, C_92, C_93, C_94,
             C_95, C_96, B_00, B_01, B_02, B_03, B_04, B_05, B_06, B_07, B_08,
             B_09, B_10, B_11, B_12, B_13, B_14, B_15, B_16, B_17, B_18, B_19
      FROM Pila
      WHERE C_36 + C_37 + C_38 + C_39 > 0
      INTO V_Desde, Tipo, Codpersonal, C_00, C_01, C_02, C_03, C_04, C_05, C_06,
           C_07, C_08, C_09, C_10, C_11, C_12, C_13, C_14, C_15, C_16, C_17,
           C_18, C_19, C_20, C_21, C_22, C_23, C_24, C_25, C_26, C_27, C_28,
           C_29, C_30, C_31, C_32, C_33, C_34, C_35, C_36, C_37, C_38, C_39,
           C_40, C_41, C_42, C_43, C_44, C_45, V_C_46, C_47, C_48, C_49, C_50,
           C_51, C_52, C_53, V_C_54, C_55, C_56, C_57, C_58, C_59, C_60, V_C_61,
           C_62, C_63, V_C_64, C_65, V_C_66, C_67, V_C_68, C_69, V_C_70, C_71,
           V_C_72, C_73, C_74, C_75, C_76, C_77, C_78, C_79, V_C_80, V_C_81,
           V_C_82, V_C_83, V_C_84, V_C_85, V_C_86, V_C_87, V_C_88, V_C_89,
           V_C_90, V_C_91, V_C_92, V_C_93, V_C_94, C_95, C_96, B_00, B_01, B_02,
           B_03, B_04, B_05, B_06, B_07, B_08, B_09, B_10, B_11, B_12, B_13,
           B_14, B_15, B_16, B_17, B_18, B_19
  DO
  BEGIN

    -- Datos de Personal
    SELECT Alto_Riesgo
    FROM Personal
    WHERE Codigo = :Codpersonal
    INTO V_Alto_Riesgo;
    V_Alto_Riesgo = COALESCE(V_Alto_Riesgo, ' ');

    -- Constantes Global (Actividad)
    V_Actividad = V_Actividad_T;
    V_Subp = NULL;
    V_Actividad_c = NULL;
    SELECT Valor, TRIM(Texto)
    FROM Constantesvalor
    WHERE Codconstante = 'C45_SUBP'
          AND Ano = EXTRACT(YEAR FROM :V_Desde)
    INTO V_Subp, V_Actividad_c;
    V_Subp = COALESCE(V_Subp, 0);
    IF (TRIM(COALESCE(V_Actividad_c, '')) <> '') THEN
      V_Actividad = V_Actividad_c;

    -- Constante local
    V_Subpl = NULL;
    V_Actividad_C = NULL;
    SELECT Valor, TRIM(Texto)
    FROM Constantes_Personal
    WHERE Codconstante = 'C45_SUBP'
          AND Ano = EXTRACT(YEAR FROM :V_Desde)
          AND Codpersonal = :Codpersonal
    INTO V_Subpl, V_Actividad_C;
    V_Subp = COALESCE(V_Subpl, V_Subp);
    IF (TRIM(COALESCE(V_Actividad_C, '')) <> '') THEN
      V_Actividad = V_Actividad_C;

    -- Totales
    SELECT SUM(C_36), SUM(C_37), SUM(C_38), SUM(C_39)
    FROM Pila
    WHERE Codpersonal = :Codpersonal
    INTO T_36, T_37, T_38, T_39;

    -- C_05 Tipo Empleado
    IF (TRIM(C_05) = '') THEN
      C_05 = '1';
    ELSE
      C_05 = TRIM(C_05);

    -- Sena
    V_Sena = 'N';
    IF (C_05 IN ('12', '19')) THEN
      V_Sena = 'S';

    -- Tiempo parcial
    V_Parcial = 'N';
    IF (C_05 IN ('51', '68')) THEN
      V_Parcial = 'S';

    -- Sena Lectivo
    V_Lectivo = 'N';
    IF (C_05 = '12') THEN
      V_Lectivo = 'S';

    -- Pensionado
    V_Pensionado = 'N';
    IF (C_06 > 0) THEN
      V_Pensionado = 'S';

    -- Unico Registro
    V_Unico = 'N';
    IF (C_36 = T_36) THEN
      V_Unico = 'S';

    -- Exonerado salud, SENA e ICBF
    V_Exonerado = 'N';
    IF (C_76 = 'S') THEN
      V_Exonerado = 'S';

    IF (V_Sena = 'S') THEN
      C_09 = V_Ciudad;

    IF (C_27 = 'X' AND
        B_17 > 0) THEN
      C_27 = 'L';

    C_28 = '';
    IF (C_48 > 0) THEN
      C_28 = 'X';

    -- C_30 IRL: Dias de incapacidad por accidente de trabajo o enfermedad laboral.
    IF (C_30 <> C_38) THEN
      C_30 = 0;
    C_30 = CAST(C_30 AS CHAR(2));

    C_32 = COALESCE(C_32, '');
    C_33 = COALESCE(C_33, '');
    C_34 = COALESCE(C_34, '');
    C_35 = COALESCE(C_35, '');

    -- C_36 Numero de dias cotizados a pension
    IF (V_Sena = 'S' OR V_Pensionado = 'S') THEN
      C_36 = 0;
    ELSE
    IF (C_36 > 0) THEN
      IF (V_C_46 = 0) THEN
        C_36 = 0;
      ELSE
      IF (V_Parcial = 'S') THEN
        C_36 = B_14;
      ELSE
        C_36 = C_36;
    ELSE
      C_36 = 0;
    C_36 = CAST(ROUND(C_36) AS CHAR(2));

    -- C_37 N?mero de d?as cotizados a salud
    IF (C_37 > 0) THEN
    BEGIN
      IF (V_Parcial = 'S') THEN
        C_37 = B_15;
    END
    ELSE
      C_37 = 0;
    C_37 = CAST(ROUND(C_37) AS CHAR(2));

    -- C_38 N?mero de d?as cotizados a Riesgos Laborales
    IF (C_38 > 0) THEN
    BEGIN
      IF (V_Parcial = 'S') THEN
        C_38 = 30;
      ELSE
      IF (V_Lectivo = 'S') THEN
        C_38 = 0;
    END
    ELSE
      C_38 = 0;
    C_38 = CAST(ROUND(C_38) AS CHAR(2));

    -- C_39 N?mero de d?as cotizados a Caja de Compensaci?n Familiar
    IF (V_Sena = 'S') THEN
      C_39 = 0;
    ELSE
    IF (C_39 > 0) THEN
    BEGIN
      IF (V_Parcial = 'S') THEN
        C_39 = B_16;
    END
    ELSE
      C_39 = 0;
    C_39 = CAST(ROUND(C_39) AS CHAR(2));

    C_40 = CAST(ROUND(C_40) AS CHAR(9));

    -- C_41
    IF (V_Sena = 'S' OR (V_Parcial = 'S')) THEN
      C_41 = '';
    ELSE
    IF (TRIM(COALESCE(C_41, '')) = '') THEN
      C_41 = 'F';

    -- C_42 IBC pensi?n
    IF (V_Sena = 'S' OR V_Pensionado = 'S') THEN
      C_42 = 0;
    ELSE
    IF (V_Unico = 'S') THEN
      C_42 = C_42 + B_19;
    ELSE
    IF (T_36 <> (B_12 - B_17)) THEN
      IF (Tipo = 'RU') THEN
        C_42 = IIF(C_40 < C_00, C_00, C_40) / 30 * C_36 + B_19;
      ELSE
        C_42 = (C_42 - (IIF(C_40 < C_00, C_00, C_40) / 30 * (B_12 - B_17))) / (T_36 - (B_12 - B_17)) * C_36;
    ELSE
      C_42 = C_42 / T_36 * C_36;
    C_42 = CAST(ROUND(CEILING(ROUND(C_42, 2))) AS CHAR(9));

    -- C_43 IBC salud
    IF (V_Unico = 'S') THEN
      C_43 = C_43 + B_19;
    ELSE
    IF (T_37 <> (B_12 - B_17)) THEN
      IF (Tipo = 'RU') THEN
        C_43 = (IIF(C_40 < C_00, C_00, C_40) / 30 * C_37) + B_19;
      ELSE
        C_43 = (C_43 - (IIF(C_40 < C_00, C_00, C_40) / 30 * (B_12 - B_17))) / (T_37 - (B_12 - B_17)) * C_37;
    ELSE
      C_43 = C_43 / T_37 * C_37;
    C_43 = CAST(ROUND(CEILING(ROUND(C_43, 2))) AS CHAR(9));

    -- C_44 IBC Riesgos Laborales
    IF (V_Lectivo = 'S') THEN
      C_44 = 0;
    ELSE
    IF (V_Unico = 'S') THEN
      C_44 = C_44 + B_19;
    ELSE
    IF (T_36 <> (B_12 - B_17)) THEN
      IF (Tipo = 'RU') THEN
        C_44 = (IIF(C_40 < C_00, C_00, C_40) / 30 * C_38) + B_19;
      ELSE
        C_44 = (C_44 - (IIF(C_40 < C_00, C_00, C_40) / 30 * (B_12 - B_17))) / (T_38 - (B_12 - B_17)) * C_38;
    ELSE
      C_44 = C_44 / T_38 * C_38;
    C_44 = CAST(ROUND(CEILING(ROUND(C_44, 2))) AS CHAR(9));

    -- C_45 IBC CCF
    IF (V_Sena = 'S') THEN
      C_45 = 0;
    ELSE
    IF (V_Unico = 'S') THEN
      C_45 = C_45 + B_19 + B_18;
    ELSE
    IF (T_36 <> (B_12 - B_17)) THEN
      IF (Tipo = 'RU') THEN
        C_45 = (IIF(C_40 < C_00, C_00, C_40) / 30 * C_39) + B_19 + B_18;
      ELSE
        C_45 = (C_45 - (IIF(C_40 < C_00, C_00, C_40) / 30 * (B_12 - B_17))) / (T_39 - (B_12 - B_17)) * C_39;
    ELSE
      C_45 = IIF(C_40 < C_00, C_00, C_40) / T_39 * C_39;
    C_45 = CAST(ROUND(CEILING(ROUND(C_45, 2))) AS CHAR(9));

    -- C_46 Tarifa de aportes pensiones,
    IF (V_Sena = 'S' OR V_Pensionado = 'S') THEN
      V_C_46 = 0;
    ELSE
    IF (C_24 = 'X') THEN
      V_C_46 = B_00 / 100;
    ELSE
      V_C_46 = V_C_46 / 100;
    C_46 = CAST(V_C_46 AS CHAR(7));

    -- C_47 Cotizaci?n a pensiones
    C_47 = CAST(ROUND((SELECT Tope
                       FROM Sys_Redondea(:C_42 * :V_C_46, 100))) AS CHAR(9));

    -- C_48
    C_48 = CAST(ROUND((SELECT Tope
                       FROM Sys_Redondea(:C_48, 100))) AS CHAR(9));

    -- C_49
    C_48 = CAST(ROUND((SELECT Tope
                       FROM Sys_Redondea(:C_49, 100))) AS CHAR(9));

    -- C_50 Total cotizaci?n Sistema General de Pensiones
    C_50 = CAST(ROUND(C_47 + C_48 + C_49) AS CHAR(9));

    -- C_51 Aportes a fondo de solidaridad pensional
    IF (C_40 + B_19 < 4 * C_00) THEN
      C_51 = 0;
    ELSE
    IF (C_24 = 'X') THEN
      C_51 = 0;
    ELSE
    IF (C_36 = T_36) THEN
      C_51 = C_42;
    ELSE
      C_51 = C_42;
    IF (C_51 <> 0) THEN
    BEGIN
      C_51 = (SELECT Tope
              FROM Sys_Redondea(ROUND((CEILING(:C_51) * :B_02 / 100) / 2), 100));
    END
    C_51 = CAST(ROUND(C_51) AS CHAR(9));

    -- C_52 Aportes a subcuenta de subsistencia.
    C_52 = CAST(ROUND((SELECT Tope
                       FROM Sys_Redondea(:C_51 + (:C_52 / :T_36 * :C_36), 100))) AS CHAR(9));

    -- C_53
    C_53 = CAST(ROUND((SELECT Tope
                       FROM Sys_Redondea(:C_53, 100))) AS CHAR(9));

    -- C_76 Cotizante exonerado de pago de aporte salud, SENA e ICBF
    IF (V_Sena = 'S' OR V_Parcial = 'S') THEN
      C_76 = 'N';
    ELSE
    IF ((C_40 + B_19) >= C_00 * 10) THEN
      C_76 = 'N';

    -- C_54 Tarifa salud
    V_C_54 = CASE
               WHEN V_Parcial = 'S' THEN 0.00
               WHEN V_Sena = 'S' THEN V_C_54
               WHEN C_40 + B_19 >= C_00 * 10 THEN V_C_54
               WHEN C_24 = 'X' AND C_76 = 'S' THEN 0.00
               WHEN C_24 = 'X' AND C_76 = 'N' THEN B_01
               WHEN C_40 + B_19 >= C_00 * 10 AND C_24 = 'X' THEN B_01
               WHEN C_40 + B_19 >= C_00 * 10 THEN V_C_54
               WHEN V_Exonerado = 'S' THEN B_04
               ELSE V_C_54
             END;
    C_54 = CAST(V_C_54 / 100 AS CHAR(7));

    -- C_55 Cotizaci?n salud
    C_55 = CAST(ROUND((SELECT Tope
                       FROM Sys_Redondea(:C_43 * :V_C_54 / 100, 100))) AS CHAR(9));

    -- C_56
    C_56 = CAST(ROUND((SELECT Tope
                       FROM Sys_Redondea(:C_56, 100))) AS CHAR(9));

    C_58 = CAST(ROUND(:C_58) AS CHAR(9));
    C_60 = CAST(ROUND(:C_60) AS CHAR(9));

    -- C_61 Tarifa de aportes a Riesgos Laborales
    IF (V_Lectivo = 'S') THEN
      V_C_61 = 0;
    ELSE
    IF ((C_24 = 'X' OR C_25 = 'X' OR C_26 = 'X' OR C_27 = 'X' OR C_27 = 'L' OR C_30 > 0) AND
        (Tipo <> 'RU')) THEN
      V_C_61 = 0;
    ELSE
      V_C_61 = V_C_61 / 100;
    C_61 = CAST(V_C_61 AS CHAR(9));

    C_62 = CAST(C_62 AS CHAR(9));

    -- C_63 Cotizacion Riesgos Laborales
    C_63 = CAST(ROUND((SELECT Tope
                       FROM Sys_Redondea(:C_44 * :V_C_61, 100))) AS CHAR(9));

    -- C_64 Tarifa de aportes CCF
    IF (V_Sena = 'S') THEN
      V_C_64 = 0;
    ELSE
    IF ((C_24 = 'X' OR C_25 = 'X' OR C_26 = 'X' OR C_30 > 0) AND
        (Tipo <> 'RU')) THEN
      V_C_64 = 0;
    ELSE
      V_C_64 = V_C_64 / 100;
    C_64 = CAST(V_C_64 AS CHAR(7));

    -- C_65 Valor aporte CCF
    C_65 = CAST(ROUND((SELECT Tope
                       FROM Sys_Redondea(:C_45 * :V_C_64, 100))) AS CHAR(9));

    -- C_95 IBC Otros parafiscales
    IF (C_69 = 0 OR C_24 = 'X' OR C_25 = 'X') THEN
      C_95 = 0;
    ELSE
    IF (V_Sena = 'S' OR V_Parcial = 'S') THEN
      C_95 = 0;
    ELSE
    IF (V_Unico = 'S') THEN
      C_95 = C_95 + B_19 + B_18;
    ELSE
    IF (Tipo = 'RU') THEN
      C_95 = (IIF(C_40 < C_00, C_00, C_40) / T_36 * C_36) + B_19 + B_18 + B_13;
    ELSE
      C_95 = IIF(C_40 < C_00, C_00, C_40) / T_36 * C_36;
    C_95 = CAST(ROUND(CEILING(ROUND(C_95))) AS CHAR(9));

    -- C_66 Tarifa de aportes SENA
    IF (C_95 = 0 OR C_24 = 'X' OR C_25 = 'X' OR C_26 = 'X' OR C_30 > 0 OR V_Sena = 'S' OR V_Parcial = 'S') THEN
      V_C_66 = 0;
    ELSE
      V_C_66 = V_C_66 / 100;
    C_66 = CAST(V_C_66 AS CHAR(7));

    -- C_67 Valor aportes SENA
    C_67 = CAST(ROUND((SELECT Tope
                       FROM Sys_Redondea(:C_95 * :V_C_66, 100))) AS CHAR(9));

    -- C_68 Tarifa aportes ICBF
    IF (C_95 = 0 OR C_24 = 'X' OR C_25 = 'X' OR C_26 = 'X' OR C_30 > 0 OR V_Sena = 'S' OR V_Parcial = 'S') THEN
      V_C_68 = 0;
    ELSE
      V_C_68 = V_C_68 / 100;
    C_68 = CAST(V_C_68 AS CHAR(7));

    -- C_69 Valor aporte ICBF
    C_69 = CAST(ROUND((SELECT Tope
                       FROM Sys_Redondea(:C_95 * :V_C_68, 100))) AS CHAR(9));

    -- C_70 Tarifa aportes ESAP
    V_C_70 = V_C_70 / 100;
    C_70 = CAST(V_C_70 AS CHAR(7));

    -- C_71 Valor aporte ESAP
    C_71 = CAST(ROUND(C_71) AS CHAR(9));

    -- C_72 Tarifa aportes MEN
    V_C_72 = V_C_72 / 100;
    C_72 = CAST(V_C_72 AS CHAR(7));

    -- C_73 Valor aporte MEN
    C_73 = CAST(ROUND(C_73) AS CHAR(9));

    -- C_77 Codigo ARL
    IF (V_Lectivo = 'S') THEN
      C_77 = '';
    ELSE
      C_77 = COALESCE(C_77, '');

    -- C_78 Clase de riesgo
    IF (V_Lectivo = 'S') THEN
      C_78 = 0;
    --   ELSE
    --    IF ((C_24 = 'X' OR C_25 = 'X' OR C_26 = 'X' OR C_27 = 'X' OR C_30 > 0) AND
    --        (Tipo <> 'RU')) THEN
    --     C_78 = 0;

    -- C_79 tarifa especial pensiones

    IF (V_Alto_Riesgo = 'S') THEN
      C_79 = '1';
    ELSE
      C_79 = ' ';

    -- C_80 Fecha de ingreso
    C_80 = EXTRACT(YEAR FROM V_C_80) || '-' || SUBSTRING(EXTRACT(MONTH FROM V_C_80) + 100 FROM 2) || '-' || SUBSTRING(EXTRACT(DAY FROM V_C_80) + 100 FROM 2);

    -- C_81 Fecha de retiro
    C_81 = EXTRACT(YEAR FROM V_C_81) || '-' || SUBSTRING(EXTRACT(MONTH FROM V_C_81) + 100 FROM 2) || '-' || SUBSTRING(EXTRACT(DAY FROM V_C_81) + 100 FROM 2);

    -- C_82 Fecha de retiro
    C_82 = EXTRACT(YEAR FROM V_C_82) || '-' || SUBSTRING(EXTRACT(MONTH FROM V_C_82) + 100 FROM 2) || '-' || SUBSTRING(EXTRACT(DAY FROM V_C_82) + 100 FROM 2);

    -- C_83 Fecha Inicio SLN
    C_83 = EXTRACT(YEAR FROM V_C_83) || '-' || SUBSTRING(EXTRACT(MONTH FROM V_C_83) + 100 FROM 2) || '-' || SUBSTRING(EXTRACT(DAY FROM V_C_83) + 100 FROM 2);

    -- C_84 Fecha fin SLN
    C_84 = EXTRACT(YEAR FROM V_C_84) || '-' || SUBSTRING(EXTRACT(MONTH FROM V_C_84) + 100 FROM 2) || '-' || SUBSTRING(EXTRACT(DAY FROM V_C_84) + 100 FROM 2);

    -- C_85 Fecha inicio IGE
    C_85 = EXTRACT(YEAR FROM V_C_85) || '-' || SUBSTRING(EXTRACT(MONTH FROM V_C_85) + 100 FROM 2) || '-' || SUBSTRING(EXTRACT(DAY FROM V_C_85) + 100 FROM 2);

    -- C_86 Fecha fin IGE
    C_86 = EXTRACT(YEAR FROM V_C_86) || '-' || SUBSTRING(EXTRACT(MONTH FROM V_C_86) + 100 FROM 2) || '-' || SUBSTRING(EXTRACT(DAY FROM V_C_86) + 100 FROM 2);

    -- C_87 Fecha inicio LMA
    C_87 = EXTRACT(YEAR FROM V_C_87) || '-' || SUBSTRING(EXTRACT(MONTH FROM V_C_87) + 100 FROM 2) || '-' || SUBSTRING(EXTRACT(DAY FROM V_C_87) + 100 FROM 2);

    -- C_88 Fecha fin LMA
    C_88 = EXTRACT(YEAR FROM V_C_88) || '-' || SUBSTRING(EXTRACT(MONTH FROM V_C_88) + 100 FROM 2) || '-' || SUBSTRING(EXTRACT(DAY FROM V_C_88) + 100 FROM 2);

    -- C_89 Fecha inicio VAC - LR
    C_89 = EXTRACT(YEAR FROM V_C_89) || '-' || SUBSTRING(EXTRACT(MONTH FROM V_C_89) + 100 FROM 2) || '-' || SUBSTRING(EXTRACT(DAY FROM V_C_89) + 100 FROM 2);

    -- C_90 Fecha fin VAC - LR
    C_90 = EXTRACT(YEAR FROM V_C_90) || '-' || SUBSTRING(EXTRACT(MONTH FROM V_C_90) + 100 FROM 2) || '-' || SUBSTRING(EXTRACT(DAY FROM V_C_90) + 100 FROM 2);

    -- C_91 Fecha inicio VCT
    C_91 = EXTRACT(YEAR FROM V_C_91) || '-' || SUBSTRING(EXTRACT(MONTH FROM V_C_91) + 100 FROM 2) || '-' || SUBSTRING(EXTRACT(DAY FROM V_C_91) + 100 FROM 2);

    -- C_92 Fecha fin VCT
    C_92 = EXTRACT(YEAR FROM V_C_92) || '-' || SUBSTRING(EXTRACT(MONTH FROM V_C_92) + 100 FROM 2) || '-' || SUBSTRING(EXTRACT(DAY FROM V_C_92) + 100 FROM 2);

    -- C_93 Fecha inicio IRL
    C_93 = EXTRACT(YEAR FROM V_C_93) || '-' || SUBSTRING(EXTRACT(MONTH FROM V_C_93) + 100 FROM 2) || '-' || SUBSTRING(EXTRACT(DAY FROM V_C_93) + 100 FROM 2);

    -- C_94 Fecha fin IRL
    C_94 = EXTRACT(YEAR FROM V_C_94) || '-' || SUBSTRING(EXTRACT(MONTH FROM V_C_94) + 100 FROM 2) || '-' || SUBSTRING(EXTRACT(DAY FROM V_C_94) + 100 FROM 2);

    -- C_96 Numero de horas laboradas
    C_96 = ROUND(C_37 * 8);

    C_97 = ' ';

    -- C_98 Sub Actividad
    IF (V_Lectivo = 'S') THEN
      C_98 = '0000000';
    ELSE
      C_98 = C_78 || SUBSTRING(V_Actividad FROM 1 FOR 4) || SUBSTRING(CAST(V_Subp + 100 AS CHAR(3)) FROM 2 FOR 2);

    SUSPEND;
  END

END;

COMMIT WORK;

CREATE OR ALTER PROCEDURE Pz_Pila_02_Mp
RETURNS (
    C_01 CHAR(2),
    C_02 INTEGER,
    C_03 CHAR(2),
    C_04 CHAR(16),
    C_05 CHAR(2),
    C_06 CHAR(2),
    C_07 CHAR(1),
    C_08 CHAR(1),
    C_09 CHAR(5),
    C_10 CHAR(5),
    C_11 CHAR(20),
    C_12 CHAR(30),
    C_13 CHAR(20),
    C_14 CHAR(30),
    C_15 CHAR(1),
    C_16 CHAR(1),
    C_17 CHAR(1),
    C_18 CHAR(1),
    C_19 CHAR(1),
    C_20 CHAR(1),
    C_21 CHAR(1),
    C_22 CHAR(1),
    C_23 CHAR(1),
    C_24 CHAR(1),
    C_25 CHAR(1),
    C_26 CHAR(1),
    C_27 CHAR(1),
    C_28 CHAR(1),
    C_29 CHAR(1),
    C_30 INTEGER,
    C_31 CHAR(6),
    C_32 CHAR(6),
    C_33 CHAR(6),
    C_34 CHAR(6),
    C_35 CHAR(6),
    C_36 DOUBLE PRECISION,
    C_37 DOUBLE PRECISION,
    C_38 DOUBLE PRECISION,
    C_39 DOUBLE PRECISION,
    C_40 DOUBLE PRECISION,
    C_41 CHAR(1),
    C_42 DOUBLE PRECISION,
    C_43 DOUBLE PRECISION,
    C_44 DOUBLE PRECISION,
    C_45 DOUBLE PRECISION,
    C_46 VARCHAR(7),
    C_47 DOUBLE PRECISION,
    C_48 DOUBLE PRECISION,
    C_49 DOUBLE PRECISION,
    C_50 DOUBLE PRECISION,
    C_51 DOUBLE PRECISION,
    C_52 DOUBLE PRECISION,
    C_53 DOUBLE PRECISION,
    C_54 VARCHAR(7),
    C_55 DOUBLE PRECISION,
    C_56 DOUBLE PRECISION,
    C_57 CHAR(15),
    C_58 DOUBLE PRECISION,
    C_59 CHAR(15),
    C_60 DOUBLE PRECISION,
    C_61 VARCHAR(9),
    C_62 CHAR(10),
    C_63 DOUBLE PRECISION,
    C_64 VARCHAR(7),
    C_65 DOUBLE PRECISION,
    C_66 DOUBLE PRECISION,
    C_67 DOUBLE PRECISION,
    C_68 DOUBLE PRECISION,
    C_69 DOUBLE PRECISION,
    C_70 DOUBLE PRECISION,
    C_71 DOUBLE PRECISION,
    C_72 DOUBLE PRECISION,
    C_73 DOUBLE PRECISION,
    C_74 CHAR(2),
    C_75 CHAR(16),
    C_76 CHAR(1),
    C_77 CHAR(6),
    C_78 CHAR(1),
    C_79 CHAR(1),
    C_80 CHAR(10),
    C_81 CHAR(10),
    C_82 CHAR(10),
    C_83 CHAR(10),
    C_84 CHAR(10),
    C_85 CHAR(10),
    C_86 CHAR(10),
    C_87 CHAR(10),
    C_88 CHAR(10),
    C_89 CHAR(10),
    C_90 CHAR(10),
    C_91 CHAR(10),
    C_92 CHAR(10),
    C_93 CHAR(10),
    C_94 CHAR(10),
    C_95 DOUBLE PRECISION,
    C_96 DOUBLE PRECISION,
    C_97 CHAR(10),
    C_98 CHAR(7))
AS
DECLARE VARIABLE Tipo          CHAR(2);
DECLARE VARIABLE Codpersonal   CHAR(15);
DECLARE VARIABLE V_Sena        CHAR(1);
DECLARE VARIABLE V_Pensionado  CHAR(1);
DECLARE VARIABLE V_Parcial     CHAR(1);
DECLARE VARIABLE V_Lectivo     CHAR(1);
DECLARE VARIABLE V_Ciudad      VARCHAR(5);
DECLARE VARIABLE V_Exonerado   CHAR(1);
DECLARE VARIABLE V_Unico       CHAR(1);
DECLARE VARIABLE T_36          DOUBLE PRECISION;
DECLARE VARIABLE T_37          DOUBLE PRECISION;
DECLARE VARIABLE T_38          DOUBLE PRECISION;
DECLARE VARIABLE V_C_54        DOUBLE PRECISION;
DECLARE VARIABLE V_C_64        DOUBLE PRECISION;
DECLARE VARIABLE V_C_61        DOUBLE PRECISION;
DECLARE VARIABLE V_C_46        DOUBLE PRECISION;
DECLARE VARIABLE T_39          DOUBLE PRECISION;
DECLARE VARIABLE V_C_80        DATE;
DECLARE VARIABLE V_C_81        DATE;
DECLARE VARIABLE V_C_82        DATE;
DECLARE VARIABLE V_C_83        DATE;
DECLARE VARIABLE V_C_84        DATE;
DECLARE VARIABLE V_C_85        DATE;
DECLARE VARIABLE V_C_86        DATE;
DECLARE VARIABLE V_C_87        DATE;
DECLARE VARIABLE V_C_88        DATE;
DECLARE VARIABLE V_C_89        DATE;
DECLARE VARIABLE V_C_90        DATE;
DECLARE VARIABLE V_C_91        DATE;
DECLARE VARIABLE V_C_92        DATE;
DECLARE VARIABLE V_C_93        DATE;
DECLARE VARIABLE V_C_94        DATE;
DECLARE VARIABLE C_00          DOUBLE PRECISION;
DECLARE VARIABLE B_00          DOUBLE PRECISION;
DECLARE VARIABLE B_01          DOUBLE PRECISION;
DECLARE VARIABLE B_02          DOUBLE PRECISION;
DECLARE VARIABLE B_03          DOUBLE PRECISION;
DECLARE VARIABLE B_04          DOUBLE PRECISION;
DECLARE VARIABLE B_05          DOUBLE PRECISION;
DECLARE VARIABLE B_06          DOUBLE PRECISION;
DECLARE VARIABLE B_07          DOUBLE PRECISION;
DECLARE VARIABLE B_08          DOUBLE PRECISION;
DECLARE VARIABLE B_09          DOUBLE PRECISION;
DECLARE VARIABLE B_10          DOUBLE PRECISION;
DECLARE VARIABLE B_11          DOUBLE PRECISION;
DECLARE VARIABLE B_12          DOUBLE PRECISION;
DECLARE VARIABLE B_13          DOUBLE PRECISION;
DECLARE VARIABLE B_14          DOUBLE PRECISION;
DECLARE VARIABLE B_15          DOUBLE PRECISION;
DECLARE VARIABLE B_16          DOUBLE PRECISION;
DECLARE VARIABLE B_17          DOUBLE PRECISION;
DECLARE VARIABLE B_18          DOUBLE PRECISION;
DECLARE VARIABLE B_19          DOUBLE PRECISION;
DECLARE VARIABLE V_Actividad   CHAR(5);
DECLARE VARIABLE V_Actividad_C CHAR(5);
DECLARE VARIABLE V_Actividad_T CHAR(5);
DECLARE VARIABLE V_Subp        INTEGER;
DECLARE VARIABLE V_Subpl       INTEGER;
DECLARE VARIABLE V_Desde       DATE;
BEGIN
  SELECT TRIM(Codmunicipio), TRIM(Codactividad)
  FROM Terceros
  WHERE Datos_Empresa = 'S'
  INTO V_Ciudad, V_Actividad_T;

  FOR SELECT Tipo, Codpersonal, C_00, C_01, C_02, C_03, C_04, C_05, C_06, C_07,
             C_08, C_09, C_10, C_11, C_12, C_13, C_14, C_15, C_16, C_17, C_18,
             C_19, C_20, C_21, C_22, C_23, C_24, C_25, C_26, C_27, C_28, C_29,
             C_30, C_31, C_32, C_33, C_34, C_35, C_36, C_37, C_38, C_39, C_40,
             C_41, C_42, C_43, C_44, C_45, C_46, C_47, C_48, C_49, C_50, C_51,
             C_52, C_53, C_54, C_55, C_56, C_57, C_58, C_59, C_60, C_61, C_62,
             C_63, C_64, C_65, C_66, C_67, C_68, C_69, C_70, C_71, C_72, C_73,
             C_74, C_75, C_76, C_77, C_78, C_79, C_80, C_81, C_82, C_83, C_84,
             C_85, C_86, C_87, C_88, C_89, C_90, C_91, C_92, C_93, C_94, C_95,
             C_96, B_00, B_01, B_02, B_03, B_04, B_05, B_06, B_07, B_08, B_09,
             B_10, B_11, B_12, B_13, B_14, B_15, B_16, B_17, B_18, B_19
      FROM Pila
      WHERE C_36 + C_37 + C_38 + C_39 > 0
      INTO Tipo, Codpersonal, C_00, C_01, C_02, C_03, C_04, C_05, C_06, C_07,
           C_08, C_09, C_10, C_11, C_12, C_13, C_14, C_15, C_16, C_17, C_18,
           C_19, C_20, C_21, C_22, C_23, C_24, C_25, C_26, C_27, C_28, C_29,
           C_30, C_31, C_32, C_33, C_34, C_35, C_36, C_37, C_38, C_39, C_40,
           C_41, C_42, C_43, C_44, C_45, V_C_46, C_47, C_48, C_49, C_50, C_51,
           C_52, C_53, V_C_54, C_55, C_56, C_57, C_58, C_59, C_60, V_C_61, C_62,
           C_63, V_C_64, C_65, C_66, C_67, C_68, C_69, C_70, C_71, C_72, C_73,
           C_74, C_75, C_76, C_77, C_78, C_79, V_C_80, V_C_81, V_C_82, V_C_83,
           V_C_84, V_C_85, V_C_86, V_C_87, V_C_88, V_C_89, V_C_90, V_C_91,
           V_C_92, V_C_93, V_C_94, C_95, C_96, B_00, B_01, B_02, B_03, B_04,
           B_05, B_06, B_07, B_08, B_09, B_10, B_11, B_12, B_13, B_14, B_15,
           B_16, B_17, B_18, B_19
  DO
  BEGIN

    -- Constantes Global (Actividad)
    V_Actividad = V_Actividad_T;
    V_Subp = NULL;
    V_Actividad_C = NULL;
    SELECT Valor, TRIM(Texto)
    FROM Constantesvalor
    WHERE Codconstante = 'C45_SUBP'
          AND Ano = EXTRACT(YEAR FROM :V_Desde)
    INTO V_Subp, V_Actividad_C;
    V_Subp = COALESCE(V_Subp, 0);
    IF (TRIM(COALESCE(V_Actividad_C, '')) <> '') THEN
      V_Actividad = V_Actividad_C;

    -- Constante local
    V_Subpl = NULL;
    V_Actividad_C = NULL;
    SELECT Valor, TRIM(Texto)
    FROM Constantes_Personal
    WHERE Codconstante = 'C45_SUBP'
          AND Ano = EXTRACT(YEAR FROM :V_Desde)
          AND Codpersonal = :Codpersonal
    INTO V_Subpl, V_Actividad_C;
    V_Subp = COALESCE(V_Subpl, V_Subp);
    IF (TRIM(COALESCE(V_Actividad_C, '')) <> '') THEN
      V_Actividad = V_Actividad_C;

    -- Totales
    SELECT SUM(C_36), SUM(C_37), SUM(C_38), SUM(C_39)
    FROM Pila
    WHERE Codpersonal = :Codpersonal
    INTO T_36, T_37, T_38, T_39;

    -- C_05 Tipo Empleado
    IF (TRIM(C_05) = '') THEN
      C_05 = '1';
    ELSE
      C_05 = TRIM(C_05);

    -- Sena
    V_Sena = 'N';
    IF (C_05 IN ('12', '19')) THEN
      V_Sena = 'S';

    -- Tiempo parcial
    V_Parcial = 'N';
    IF (C_05 IN ('51', '68')) THEN
      V_Parcial = 'S';

    -- Sena Lectivo
    V_Lectivo = 'N';
    IF (C_05 = '12') THEN
      V_Lectivo = 'S';

    -- Pensionado
    V_Pensionado = 'N';
    IF (C_06 > 0) THEN
      V_Pensionado = 'S';

    -- Unico Registro
    V_Unico = 'N';
    IF (C_36 = T_36) THEN
      V_Unico = 'S';

    -- Exonerado salud, SENA e ICBF
    V_Exonerado = 'N';
    IF (C_76 = 'S') THEN
      V_Exonerado = 'S';

    IF (V_Sena = 'S') THEN
      C_09 = V_Ciudad;

    IF (C_27 = 'X' AND
        B_17 > 0) THEN
      C_27 = 'L';

    C_28 = '';
    IF (C_48 > 0) THEN
      C_28 = 'X';

    -- C_30 IRL: Días de incapacidad por accidente de trabajo o enfermedad laboral.
    IF (C_30 <> C_38) THEN
      C_30 = 0;
    C_30 = CAST(C_30 AS CHAR(2));

    C_32 = COALESCE(C_32, '');
    C_33 = COALESCE(C_33, '');
    C_34 = COALESCE(C_34, '');
    C_35 = COALESCE(C_35, '');

    -- C_36 Número de días cotizados a pension
    IF (V_Sena = 'S' OR V_Pensionado = 'S') THEN
      C_36 = 0;
    ELSE
    IF (C_36 > 0) THEN
      IF (V_C_46 = 0) THEN
        C_36 = 0;
      ELSE
      IF (V_Parcial = 'S') THEN
        C_36 = B_12;
      ELSE
        C_36 = C_36;
    ELSE
      C_36 = 0;
    C_36 = CAST(ROUND(C_36) AS CHAR(2));

    -- C_37 Número de días cotizados a salud
    IF (C_37 > 0) THEN
    BEGIN
      IF (V_Parcial = 'S') THEN
        C_37 = B_15;
    END
    ELSE
      C_37 = 0;
    C_37 = CAST(ROUND(C_37) AS CHAR(2));

    -- C_38 Número de días cotizados a Riesgos Laborales
    IF (C_38 > 0) THEN
    BEGIN
      IF (V_Parcial = 'S') THEN
        C_38 = 30;
      ELSE
      IF (V_Lectivo = 'S') THEN
        C_38 = 0;
    END
    ELSE
      C_38 = 0;
    C_38 = CAST(ROUND(C_38) AS CHAR(2));

    -- C_39 Número de días cotizados a Caja de Compensación Familiar
    IF (V_Sena = 'S') THEN
      C_39 = 0;
    ELSE
    IF (C_39 > 0) THEN
    BEGIN
      IF (V_Parcial = 'S') THEN
        C_39 = B_12;
    END
    ELSE
      C_39 = 0;
    C_39 = CAST(ROUND(C_39) AS CHAR(2));

    C_40 = CAST(ROUND(C_40) AS CHAR(9));

    -- C_41
    IF (V_Sena = 'S' OR (V_Parcial = 'S')) THEN
      C_41 = '';
    ELSE
    IF (TRIM(COALESCE(C_41, '')) = '') THEN
      C_41 = 'F';

    -- C_42 IBC pensión
    IF (V_Sena = 'S' OR V_Pensionado = 'S') THEN
      C_42 = 0;
    ELSE
    IF (V_Unico = 'S') THEN
      C_42 = C_42 + B_19;
    ELSE
    IF (T_36 <> B_12) THEN
      IF (Tipo = 'RU') THEN
        C_42 = C_40 / 30 * C_36 + B_19;
      ELSE
        C_42 = (C_42 - (C_40 / 30 * B_12)) / (T_36 - B_12) * C_36;
    ELSE
      C_42 = C_42 / T_36 * C_36;
    C_42 = CAST(ROUND(CEILING(ROUND(C_42, 2))) AS CHAR(9));

    -- C_43 IBC salud
    IF (V_Unico = 'S') THEN
      C_43 = C_43 + B_19;
    ELSE
    IF (T_37 <> B_12) THEN
      IF (Tipo = 'RU') THEN
        C_43 = (C_40 / 30 * C_37) + B_19;
      ELSE
        C_43 = (C_43 - (C_40 / 30 * B_12)) / (T_37 - B_12) * C_37;
    ELSE
      C_43 = C_43 / T_37 * C_37;
    C_43 = CAST(ROUND(CEILING(ROUND(C_43, 2))) AS CHAR(9));

    -- C_44 IBC Riesgos Laborales
    IF (V_Lectivo = 'S') THEN
      C_44 = 0;
    ELSE
    IF (V_Unico = 'S') THEN
      C_44 = C_44 + B_19;
    ELSE
    IF (T_36 <> B_12) THEN
      IF (Tipo = 'RU') THEN
        C_44 = (C_40 / 30 * C_38) + B_19;
      ELSE
        C_44 = (C_44 - (C_40 / 30 * B_12)) / (T_38 - B_12) * C_38;
    ELSE
      C_44 = C_44 / T_38 * C_38;
    C_44 = CAST(ROUND(CEILING(ROUND(C_44, 2))) AS CHAR(9));

    -- C_45 IBC CCF
    IF (V_Sena = 'S') THEN
      C_45 = 0;
    ELSE
    IF (V_Unico = 'S') THEN
      C_45 = C_45 + B_19 + B_18;
    ELSE
    IF (T_36 <> B_12) THEN
      IF (Tipo = 'RU') THEN
        C_45 = (C_40 / 30 * C_39) + B_19 + B_18;
      ELSE
        C_45 = (C_45 - (C_40 / 30 * B_12)) / (T_39 - B_12) * C_39;
    ELSE
      C_45 = C_45 / T_39 * C_39;
    C_45 = CAST(ROUND(CEILING(ROUND(C_45, 2))) AS CHAR(9));

    -- C_46 Tarifa de aportes pensiones,
    IF (V_Sena = 'S' OR V_Pensionado = 'S') THEN
      V_C_46 = 0;
    ELSE
    IF (C_24 = 'X') THEN
      V_C_46 = B_00 / 100;
    ELSE
      V_C_46 = V_C_46 / 100;
    C_46 = CAST(V_C_46 AS CHAR(7));

    -- C_47 Cotización a pensiones
    C_47 = CAST(ROUND((SELECT Tope
                       FROM Sys_Redondea(:C_42 * :V_C_46, 100))) AS CHAR(9));

    -- C_48
    C_48 = CAST(ROUND((SELECT Tope
                       FROM Sys_Redondea(:C_48, 100))) AS CHAR(9));

    -- C_49
    C_48 = CAST(ROUND((SELECT Tope
                       FROM Sys_Redondea(:C_49, 100))) AS CHAR(9));

    -- C_50 Total cotización Sistema General de Pensiones
    C_50 = CAST(ROUND(C_47 + C_48 + C_49) AS CHAR(9));

    -- C_51 Aportes a fondo de solidaridad pensional
    IF (C_40 < 4 * C_00) THEN
      C_51 = 0;
    ELSE
    IF (C_24 = 'X') THEN
      C_51 = 0;
    ELSE
    IF (C_36 = T_36) THEN
      C_51 = C_42 + B_19;
    ELSE
    IF (C_23 = 'X') THEN
      C_51 = (C_42 / T_36 * C_36) + B_19;
    ELSE
      C_51 = C_42 / T_36 * C_36;
    IF (C_51 <> 0) THEN
    BEGIN
      C_51 = (SELECT Tope
              FROM Sys_Redondea(ROUND((CEILING(:C_51) * :B_02 / 100) / 2), 100));
    END
    C_51 = CAST(ROUND(C_51) AS CHAR(9));

    -- C_52 Aportes a subcuenta de subsistencia.
    C_52 = CAST(ROUND((SELECT Tope
                       FROM Sys_Redondea(:C_51 + :C_52, 100))) AS CHAR(9));

    -- C_53
    C_53 = CAST(ROUND((SELECT Tope
                       FROM Sys_Redondea(:C_53, 100))) AS CHAR(9));

    -- C_54 Tarifa salud
    V_C_54 = CASE
               WHEN V_Parcial = 'S' THEN 0.00
               WHEN V_Sena = 'S' THEN V_C_54
               WHEN C_43 + B_19 >= C_00 * 10 AND C_24 = 'X' THEN B_01
               WHEN C_43 + B_19 >= C_00 * 10 THEN V_C_54
               WHEN C_24 = 'X' AND C_76 = 'S' THEN 0.00
               WHEN C_24 = 'X' AND C_76 = 'N' THEN B_01
               WHEN C_43 + B_19 >= C_00 * 10 AND C_24 = 'X' THEN B_01
               WHEN C_43 + B_19 >= C_00 * 10 THEN V_C_54
               WHEN V_Exonerado = 'S' THEN B_04
               ELSE V_C_54
             END;
    C_54 = CAST(V_C_54 / 100 AS CHAR(7));

    -- C_55 Cotización salud
    C_55 = CAST(ROUND((SELECT Tope
                       FROM Sys_Redondea(:C_43 * :V_C_54 / 100, 100))) AS CHAR(9));

    -- C_56
    C_56 = CAST(ROUND((SELECT Tope
                       FROM Sys_Redondea(:C_56, 100))) AS CHAR(9));

    C_58 = CAST(ROUND(:C_58) AS CHAR(9));
    C_60 = CAST(ROUND(:C_60) AS CHAR(9));

    -- C_61 Tarifa de aportes a Riesgos Laborales
    IF (V_Lectivo = 'S') THEN
      V_C_61 = 0;
    ELSE
    IF ((C_24 = 'X' OR C_25 = 'X' OR C_26 = 'X' OR C_27 = 'X' OR C_30 > 0) AND
        (Tipo <> 'RU')) THEN
      V_C_61 = 0;
    ELSE
      V_C_61 = V_C_61 / 100;
    C_61 = CAST(V_C_61 AS CHAR(9));

    C_62 = CAST(C_62 AS CHAR(9));

    -- C_63 Cotizacion Riesgos Laborales
    C_63 = CAST(ROUND((SELECT Tope
                       FROM Sys_Redondea(:C_44 * :V_C_61, 100))) AS CHAR(9));

    -- C_64 Tarifa de aportes CCF
    IF (V_Sena = 'S') THEN
      V_C_64 = 0;
    ELSE
    IF ((C_24 = 'X' OR C_25 = 'X' OR C_26 = 'X' OR C_30 > 0) AND
        (Tipo <> 'RU')) THEN
      V_C_64 = 0;
    ELSE
      V_C_64 = V_C_64 / 100;
    C_64 = CAST(V_C_64 AS CHAR(7));

    -- C_65 Valor aporte CCF
    C_65 = CAST(ROUND((SELECT Tope
                       FROM Sys_Redondea(:C_45 * :V_C_64, 100))) AS CHAR(9));

    -- C_66 Tarifa de aportes SENA
    IF (C_95 = 0 OR C_24 = 'X' OR C_25 = 'X' OR C_26 = 'X' OR C_30 > 0 OR V_Sena = 'S' OR V_Parcial = 'S') THEN
      C_66 = 0;
    ELSE
      C_66 = C_66 / 100;
    C_66 = CAST(C_66 AS CHAR(7));

    -- C_67 Valor aportes SENA
    C_67 = CAST(ROUND((SELECT Tope
                       FROM Sys_Redondea(:C_95 * :C_66, 100))) AS CHAR(9));

    -- C_68 Tarifa aportes ICBF
    IF (C_95 = 0 OR C_24 = 'X' OR C_25 = 'X' OR C_26 = 'X' OR C_30 > 0 OR V_Sena = 'S' OR V_Parcial = 'S') THEN
      C_68 = 0;
    ELSE
      C_68 = C_68 / 100;
    C_68 = CAST(C_68 AS CHAR(7));

    -- C_69 Valor aporte ICBF
    C_69 = CAST(ROUND((SELECT Tope
                       FROM Sys_Redondea(:C_95 * :C_68, 100))) AS CHAR(9));

    -- C_70 Tarifa aportes ESAP
    C_70 = CAST(C_70 / 100 AS CHAR(7));

    -- C_71 Valor aporte ESAP
    C_71 = CAST(ROUND(C_71) AS CHAR(9));

    -- C_72 Tarifa aportes MEN
    C_72 = CAST(C_72 / 100 AS CHAR(7));

    -- C_73 Valor aporte MEN
    C_73 = CAST(ROUND(C_73) AS CHAR(9));

    -- C_76 Cotizante exonerado de pago de aporte salud, SENA e ICBF
    IF (V_Sena = 'S' OR V_Parcial = 'S') THEN
      C_76 = 'N';
    ELSE
    IF ((C_43 + B_19) >= C_00 * 10) THEN
      C_76 = 'N';

    -- C_77 Codigo ARL
    IF (V_Lectivo = 'S') THEN
      C_77 = '';
    ELSE
      C_77 = COALESCE(C_77, '');

    -- C_78 Clase de riesgo
    IF (V_Lectivo = 'S') THEN
      C_78 = 0;
    --   ELSE
    --    IF ((C_24 = 'X' OR C_25 = 'X' OR C_26 = 'X' OR C_27 = 'X' OR C_30 > 0) AND
    --        (Tipo <> 'RU')) THEN
    --     C_78 = 0;

    -- C_79 tarifa especial pensiones
    C_79 = COALESCE(C_79, ' ');

    -- C_80 Fecha de ingreso
    C_80 = EXTRACT(YEAR FROM V_C_80) || '-' || SUBSTRING(EXTRACT(MONTH FROM V_C_80) + 100 FROM 2) || '-' || SUBSTRING(EXTRACT(DAY FROM V_C_80) + 100 FROM 2);

    -- C_81 Fecha de retiro
    C_81 = EXTRACT(YEAR FROM V_C_81) || '-' || SUBSTRING(EXTRACT(MONTH FROM V_C_81) + 100 FROM 2) || '-' || SUBSTRING(EXTRACT(DAY FROM V_C_81) + 100 FROM 2);

    -- C_82 Fecha de retiro
    C_82 = EXTRACT(YEAR FROM V_C_82) || '-' || SUBSTRING(EXTRACT(MONTH FROM V_C_82) + 100 FROM 2) || '-' || SUBSTRING(EXTRACT(DAY FROM V_C_82) + 100 FROM 2);

    -- C_83 Fecha Inicio SLN
    C_83 = EXTRACT(YEAR FROM V_C_83) || '-' || SUBSTRING(EXTRACT(MONTH FROM V_C_83) + 100 FROM 2) || '-' || SUBSTRING(EXTRACT(DAY FROM V_C_83) + 100 FROM 2);

    -- C_84 Fecha fin SLN
    C_84 = EXTRACT(YEAR FROM V_C_84) || '-' || SUBSTRING(EXTRACT(MONTH FROM V_C_84) + 100 FROM 2) || '-' || SUBSTRING(EXTRACT(DAY FROM V_C_84) + 100 FROM 2);

    -- C_85 Fecha inicio IGE
    C_85 = EXTRACT(YEAR FROM V_C_85) || '-' || SUBSTRING(EXTRACT(MONTH FROM V_C_85) + 100 FROM 2) || '-' || SUBSTRING(EXTRACT(DAY FROM V_C_85) + 100 FROM 2);

    -- C_86 Fecha fin IGE
    C_86 = EXTRACT(YEAR FROM V_C_86) || '-' || SUBSTRING(EXTRACT(MONTH FROM V_C_86) + 100 FROM 2) || '-' || SUBSTRING(EXTRACT(DAY FROM V_C_86) + 100 FROM 2);

    -- C_87 Fecha inicio LMA
    C_87 = EXTRACT(YEAR FROM V_C_87) || '-' || SUBSTRING(EXTRACT(MONTH FROM V_C_87) + 100 FROM 2) || '-' || SUBSTRING(EXTRACT(DAY FROM V_C_87) + 100 FROM 2);

    -- C_88 Fecha fin LMA
    C_88 = EXTRACT(YEAR FROM V_C_88) || '-' || SUBSTRING(EXTRACT(MONTH FROM V_C_88) + 100 FROM 2) || '-' || SUBSTRING(EXTRACT(DAY FROM V_C_88) + 100 FROM 2);

    -- C_89 Fecha inicio VAC - LR
    C_89 = EXTRACT(YEAR FROM V_C_89) || '-' || SUBSTRING(EXTRACT(MONTH FROM V_C_89) + 100 FROM 2) || '-' || SUBSTRING(EXTRACT(DAY FROM V_C_89) + 100 FROM 2);

    -- C_90 Fecha fin VAC - LR
    C_90 = EXTRACT(YEAR FROM V_C_90) || '-' || SUBSTRING(EXTRACT(MONTH FROM V_C_90) + 100 FROM 2) || '-' || SUBSTRING(EXTRACT(DAY FROM V_C_90) + 100 FROM 2);

    -- C_91 Fecha inicio VCT
    C_91 = EXTRACT(YEAR FROM V_C_91) || '-' || SUBSTRING(EXTRACT(MONTH FROM V_C_91) + 100 FROM 2) || '-' || SUBSTRING(EXTRACT(DAY FROM V_C_91) + 100 FROM 2);

    -- C_92 Fecha fin VCT
    C_92 = EXTRACT(YEAR FROM V_C_92) || '-' || SUBSTRING(EXTRACT(MONTH FROM V_C_92) + 100 FROM 2) || '-' || SUBSTRING(EXTRACT(DAY FROM V_C_92) + 100 FROM 2);

    -- C_93 Fecha inicio IRL
    C_93 = EXTRACT(YEAR FROM V_C_93) || '-' || SUBSTRING(EXTRACT(MONTH FROM V_C_93) + 100 FROM 2) || '-' || SUBSTRING(EXTRACT(DAY FROM V_C_93) + 100 FROM 2);

    -- C_94 Fecha fin IRL
    C_94 = EXTRACT(YEAR FROM V_C_94) || '-' || SUBSTRING(EXTRACT(MONTH FROM V_C_94) + 100 FROM 2) || '-' || SUBSTRING(EXTRACT(DAY FROM V_C_94) + 100 FROM 2);

    -- C_95 IBC Otros parafiscales
    IF (C_69 = 0 OR C_24 = 'X') THEN
      C_95 = 0;
    ELSE
    IF (V_Sena = 'S' OR V_Parcial = 'S') THEN
      C_95 = 0;
    ELSE
    IF (V_Unico = 'S') THEN
      C_95 = C_95 + B_19 + B_18;
    ELSE
    IF (Tipo = 'RU') THEN
      C_95 = (C_95 / T_36 * C_36) + B_19 + B_18 + B_13;
    ELSE
      C_95 = C_95 / T_36 * C_36;
    C_95 = CAST(ROUND(CEILING(ROUND(C_95))) AS CHAR(9));

    -- C_96 Número de horas laboradas
    C_96 = ROUND(C_37 * 8);

    C_97 = ' ';

    -- C_98 Sub Actividad
    IF (V_Lectivo = 'S') THEN
      C_98 = '0000000';
    ELSE
      C_98 = C_78 || SUBSTRING(V_Actividad FROM 1 FOR 4) || SUBSTRING(CAST(V_Subp + 100 AS CHAR(3)) FROM 2 FOR 2);

    SUSPEND;
  END

END;

COMMIT WORK;
/*********************************************/


UPDATE Pta P
SET P.Url_Pdf = 'https://portal.noova.com.co/apolosoftapi/services/NomNominaspdf.asmx'
WHERE P.Codigo = 'NOOVA_NE'
      AND P.Clave = '6y0MqyNhLY';


UPDATE FACTURAS_PDF SET RESULTADO='0';


COMMIT WORK;

UPDATE OR INSERT INTO PTA (CODIGO, NOMBRE, CANAL, URL_TOKEN, URL_XML, URL_PDF, USUARIO, CLAVE, XML_ENCODE, ACTIVO, MODULO, FASE)
                   VALUES ('NOOVA_RE', 'NOOVA - RECEPCION', 'SOAP', 'NA', 'https://portal.noova.com.co/apolosoftapi/services/facproveedor.asmx', 'https://portal.noova.com.co/apolosoftapi/services/facproveedor.asmx', '810000494Inte', '6y0MqyNhLY', 'N', 'N', 'RECEPCION', 2)
                 MATCHING (CODIGO);


COMMIT WORK;

/************************************************************/
/****                 MODULO RECEPCION                    ***/
/************************************************************/

UPDATE Documentos
SET Codigo_Fe = 'NA'
WHERE Codigo_Fe CONTAINING ('COMPRA') ;

COMMIT WORK;

UPDATE OR INSERT INTO PARAMETROS (CODIGO, NOMBRE, PUNTERO)
                          VALUES ('NUMERO_ID', 'NUMERO_ID', NULL)
                        MATCHING (CODIGO);


UPDATE OR INSERT INTO GRUPOS (CODIGO, NOMBRE)
                      VALUES ('RE', 'RECEPCION ELECTRONICA')
                    MATCHING (CODIGO);

 COMMIT WORK;

UPDATE OR INSERT INTO INFORMES (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD, DESTINO_FTP, CODREPORTE)
                        VALUES ('RECEP0001', 'RECEPTOR DE FACTURAS ELECTRONICAS DE COMPRA - RANGO FECHA', 'Permite consultar las compras electronicas, aquellos documentos que llegaron a la rutina Receptor
Por rango de fecha y por uno o por todos los terceros', 'SELECT C.* FROM Compras C WHERE ((C.Fecha >= :_Desde) AND (C.Fecha <= :_Hasta)) AND TRIM(C.Tercero) LIKE :Tercero', 'N', 'ELECTRONIC', 'N', 'RE', 'N', 'LIBRE', 5, 'N', 1, NULL, NULL)
                      MATCHING (CODIGO);


UPDATE OR INSERT INTO PROCESOS (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, CODGRUPO, NIVEL, TABLERO, PRIORIDAD)
                        VALUES ('REC001', 'REGRESAR FACTURAS DE COMPRA A ESTADO BANDEJA PARA VOLVER A EJECUTAR LOS EVENTOS', 'Permite regresar a su estado inicial una factura que en la DIAN no tiene los 3 eventos
luego se debe reabrir el Receptor Electronico y volver a ejecutar los respectivos eventos
Indicar ID', 'UPDATE Compras
SET Estado = ''BANDEJA''
WHERE Id = :Numero_Id;', 'N', 'ELECTRONIC', 'RE', 5, 'N', 1)
                      MATCHING (CODIGO);


COMMIT WORK;

UPDATE OR INSERT INTO PTA (CODIGO, NOMBRE, CANAL, URL_TOKEN, URL_XML, URL_PDF, USUARIO, CLAVE, XML_ENCODE, ACTIVO, MODULO, FASE)
                   VALUES ('NOOVA_RE', 'NOOVA - RECEPCION', 'SOAP', 'NA', 'https://portal.noova.com.co/apolosoftapi/services/facproveedor.asmx', 'https://portal.noova.com.co/apolosoftapi/services/facproveedor.asmx', '810000494Inte', '6y0MqyNhLY', 'N', 'N', 'RECEPCION', 2)
                 MATCHING (CODIGO);


COMMIT WORK;


/************************************************************/
/****                 REINICIAR GENERADORES               ***/
/************************************************************/


/******************************************************************************/
/***                          Drop Stored procedures                        ***/
/******************************************************************************/

EXECUTE BLOCK
AS
DECLARE VARIABLE V_Registro  INT;
DECLARE VARIABLE V_Procedure CHAR(31) = 'PZ_REINCIAR_GENERADORES';
BEGIN

 SELECT COUNT(1)
 FROM Rdb$Procedures
 WHERE (Rdb$Procedure_Name = :V_Procedure)
 INTO V_Registro;

 IF (V_Registro > 0) THEN
  EXECUTE STATEMENT 'DROP PROCEDURE ' || TRIM(V_Procedure) || ';';

END;

COMMIT WORK;

/******************************************************************************/
/***                           Stored procedures                            ***/
/******************************************************************************/



SET TERM ^ ;

CREATE OR ALTER PROCEDURE PZ_REINICIAR_GENERADORES
AS
BEGIN
  EXIT;
END^






SET TERM ; ^



/******************************************************************************/
/***                           Stored procedures                            ***/
/******************************************************************************/



SET TERM ^ ;

CREATE OR ALTER PROCEDURE PZ_REINICIAR_GENERADORES
AS
DECLARE VARIABLE Generators VARCHAR(31);
BEGIN
 FOR SELECT Rdb$Generator_Name
     FROM Rdb$Generators
     WHERE Rdb$Generator_Name NOT CONTAINING '$'
     ORDER BY 1
     INTO Generators
 DO
 BEGIN
  EXECUTE PROCEDURE Sys_Inicia_Generador(:Generators,0);
 END
END^



SET TERM ; ^

COMMIT WORK;



/******************************************************************************/
/***                           Reportes con Numerador                       ***/
/******************************************************************************/

UPDATE OR INSERT INTO REPORTES (CODIGO, NOMBRE, NOTA, ARCHIVO, ADJUNTO, NATIVO, MODULO, PARAMETROS, PUBLICAR, CODGRUPO, VALIDA_TRANSITO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD)
                        VALUES ('CUEC010102', 'LIBRO MAYOR Y BALANCES, CON NUMERADOR.', 'Muestra el Libro Mayor y Balances a cuatro columnas, por un rango de fecha dado. Con numerador.', 'CUE001_72N.FR3', NULL, 'S', 'CONTABLE', NULL, 'S', 'SALDOS CUENTAS', 'N', 'S', 'SALDO CUENTAS', 5, 'N', 1)
                      ;
UPDATE OR INSERT INTO REPORTES (CODIGO, NOMBRE, NOTA, ARCHIVO, ADJUNTO, NATIVO, MODULO, PARAMETROS, PUBLICAR, CODGRUPO, VALIDA_TRANSITO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD)
                        VALUES ('CUEC020102', 'LIBRO INVENTARIO Y BALANCES CON TERCEROS, CON NUMERADOR.', 'Muestra saldo de cuentas en forma escalonada.  Según los parámetros definidos en la región de filtro. Con numerador.', 'CUE004_72N.FR3', NULL, 'S', 'CONTABLE', NULL, 'S', 'SALDOS CUENTAS', 'N', 'S', 'SALDO CUENTAS', 5, 'N', 1)
                      ;
UPDATE OR INSERT INTO REPORTES (CODIGO, NOMBRE, NOTA, ARCHIVO, ADJUNTO, NATIVO, MODULO, PARAMETROS, PUBLICAR, CODGRUPO, VALIDA_TRANSITO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD)
                        VALUES ('CD0007', 'CAJA DIARIO POR TIPO DE DOCUMENTO, CON NUMERADOR.', 'Permite imprimir el informe de Caja Diario por cuenta y agrupado por tipo de documento, con sus respectivos debitos y creditos. En caso de querer consultar nivel auxiliar digitar 30. Con numerador.', 'CD0002N.FR3', NULL, 'S', 'CONTABLE', 'FECHA_INICIAL,FECHA_FINAL,DOCUMENTO_INICIAL,DOCUMENTO_FINAL,NODO', 'S', 'CAJA DIARIO', 'N', 'N', 'LIBRE', 5, 'N', 1)
                      ;


COMMIT WORK;


/************************************************************/
/****            Informes Diarios de Ventas               ***/
/************************************************************/

EXECUTE BLOCK
AS
DECLARE VARIABLE V_Registro INT;
BEGIN

  SELECT COUNT(1)
  FROM Reportes
  WHERE Codigo IN ('POZG030201', 'POZG030202', 'POZG030203')
  INTO V_Registro;

  IF (V_Registro = 0) THEN
  BEGIN

    DELETE FROM Reportes
    WHERE Codigo STARTING 'POZ';

    UPDATE OR INSERT INTO Reportes (Codigo, Nombre, Nota, Archivo, Adjunto,
                                    Nativo, Modulo, Parametros, Publicar,
                                    Codgrupo, Valida_Transito, Rango, Categoria,
                                    Nivel, Tablero, Prioridad)
    VALUES ('POZG030202', 'INFORME DIARIO VENTAS (DIAN) - TIRILLA TERMICA', 'Segun fecha indicada muestra informe de ventas por documento y por computador, elegir uno o todos.  
Requisitos: El codigo de forma de pago en efectivo siempre debe ser 01',
            'R_AUX_MOV_05A.FR3', NULL, 'S', 'GESTION',
            'FECHA,TIPO_DOCUMENTO,COMPUTADOR', 'N', 'COMPROBANTE Z', 'N', 'N',
            'LIBRE', 1, 'N', 1)
    MATCHING (Codigo);
    UPDATE OR INSERT INTO Reportes (Codigo, Nombre, Nota, Archivo, Adjunto,
                                    Nativo, Modulo, Parametros, Publicar,
                                    Codgrupo, Valida_Transito, Rango, Categoria,
                                    Nivel, Tablero, Prioridad)
    VALUES ('POZG030201', 'INFORME DIARIO VENTAS (DIAN) - CARTA', 'Segun fecha indicada muestra informe de ventas por documento y por computador, elegir uno o todos.  
Requisitos: El codigo de forma de pago en efectivo siempre debe ser 01',
            'R_AUX_MOV_04A.FR3', NULL, 'S', 'GESTION',
            'FECHA,TIPO_DOCUMENTO,COMPUTADOR', 'N', 'COMPROBANTE Z', 'N', 'N',
            'LIBRE', 1, 'N', 1)
    MATCHING (Codigo);
    UPDATE OR INSERT INTO Reportes (Codigo, Nombre, Nota, Archivo, Adjunto,
                                    Nativo, Modulo, Parametros, Publicar,
                                    Codgrupo, Valida_Transito, Rango, Categoria,
                                    Nivel, Tablero, Prioridad)
    VALUES ('POZG030203', 'INFORME DIARIO VENTAS (DIAN) - TIRILLA MATRIZ', 'Segun fecha indicada muestra informe de ventas por documento y por computador, elegir uno o todos.  
Requisitos: El codigo de forma de pago en efectivo siempre debe ser 01',
            'R_AUX_MOV_05B.FR3', NULL, 'S', 'GESTION',
            'FECHA,TIPO_DOCUMENTO,COMPUTADOR', 'N', 'COMPROBANTE Z', 'N', 'N',
            'LIBRE', 1, 'N', 1)
    MATCHING (Codigo);
    UPDATE OR INSERT INTO Grupos (Codigo, Nombre)
    VALUES ('COMPROBANTE Z', 'COMPROBANTE Z (INFORME DIARIO DE VENTAS)')
    MATCHING (Codigo);

  END
  ELSE
  BEGIN
    UPDATE Reportes
    SET Nombre = 'INFORME DIARIO VENTAS (DIAN) - TIRILLA TERMICA', Nota = 'Segun fecha indicada muestra informe de ventas por documento y por computador, elegir uno o todos.  
Requisitos: El codigo de forma de pago en efectivo siempre debe ser 01'
    WHERE (Codigo = 'POZG030202');

    UPDATE Reportes
    SET Nombre = 'INFORME DIARIO VENTAS (DIAN) - CARTA', Nota = 'Segun fecha indicada muestra informe de ventas por documento y por computador, elegir uno o todos.  
Requisitos: El codigo de forma de pago en efectivo siempre debe ser 01'
    WHERE (Codigo = 'POZG030201');

    UPDATE Reportes
    SET Nombre = 'INFORME DIARIO VENTAS (DIAN) - TIRILLA MATRIZ', Nota = 'Segun fecha indicada muestra informe de ventas por documento y por computador, elegir uno o todos.  
Requisitos: El codigo de forma de pago en efectivo siempre debe ser 01'
    WHERE (Codigo = 'POZG030203');
  END

END;

COMMIT WORK;

EXECUTE BLOCK
AS
DECLARE VARIABLE V_Registro INT;
BEGIN

  SELECT COUNT(1)
  FROM Reportes
  WHERE Codigo IN ('POZG030301', 'POZG030302', 'POZG030303')
  INTO V_Registro;

  IF (V_Registro = 0) THEN
  BEGIN
    UPDATE OR INSERT INTO Reportes (Codigo, Nombre, Nota, Archivo, Adjunto,
                                    Nativo, Modulo, Parametros, Publicar,
                                    Codgrupo, Valida_Transito, Rango, Categoria,
                                    Nivel, Tablero, Prioridad)
    VALUES ('POZG030301', 'VENTAS DIARIAS POR USUARIO - CARTA', 'Segun fecha indicada muestra informe de ventas por usuario y por computador, elegir uno o todos.  
Requisitos: El codigo de forma de pago en efectivo siempre debe ser 01',
            'R_AUX_MOV_06A.FR3', NULL, 'S', 'GESTION',
            'FECHA,USUARIO,TIPO_DOCUMENTO,COMPUTADOR', 'N', 'COMPROBANTE Z',
            'N', 'N', 'LIBRE', 1, 'N', 1)
    MATCHING (Codigo);
    UPDATE OR INSERT INTO Reportes (Codigo, Nombre, Nota, Archivo, Adjunto,
                                    Nativo, Modulo, Parametros, Publicar,
                                    Codgrupo, Valida_Transito, Rango, Categoria,
                                    Nivel, Tablero, Prioridad)
    VALUES ('POZG030302', 'VENTAS DIARIAS POR USUARIO - TIRILLA TERMICA', 'Segun fecha indicada muestra informe de ventas por usuario y por computador, elegir uno o todos.  
Requisitos: El codigo de forma de pago en efectivo siempre debe ser 01',
            'R_AUX_MOV_06B.FR3', NULL, 'S', 'GESTION',
            'FECHA,USUARIO,TIPO_DOCUMENTO,COMPUTADOR', 'N', 'COMPROBANTE Z',
            'N', 'N', 'LIBRE', 1, 'N', 1)
    MATCHING (Codigo);
    UPDATE OR INSERT INTO Reportes (Codigo, Nombre, Nota, Archivo, Adjunto,
                                    Nativo, Modulo, Parametros, Publicar,
                                    Codgrupo, Valida_Transito, Rango, Categoria,
                                    Nivel, Tablero, Prioridad)
    VALUES ('POZG030303', 'VENTAS DIARIAS POR USUARIO - TIRILLA MATRIZ', 'Segun fecha indicada muestra informe de ventas por usuario y por computador, elegir uno o todos.  
Requisitos: El codigo de forma de pago en efectivo siempre debe ser 01',
            'R_AUX_MOV_06C.FR3', NULL, 'S', 'GESTION',
            'FECHA,USUARIO,TIPO_DOCUMENTO,COMPUTADOR', 'N', 'COMPROBANTE Z',
            'N', 'N', 'LIBRE', 1, 'N', 1)
    MATCHING (Codigo);

  END

END; 

COMMIT WORK;

/******************************************************************************/
/***                           Stored procedures                            ***/
/******************************************************************************/

CREATE OR ALTER PROCEDURE Pz_Diario_Ventas (
    Fecha_ DATE,
    Tipo_  VARCHAR(5),
    Compu_ VARCHAR(20))
RETURNS (
    Tipo         VARCHAR(5),
    Prefijo      VARCHAR(5),
    Numero       VARCHAR(10),
    Renglon      INTEGER,
    Referencia   VARCHAR(20),
    Cantidad     DOUBLE PRECISION,
    Bruto        DOUBLE PRECISION,
    Descuento    DOUBLE PRECISION,
    Grvado       DOUBLE PRECISION,
    No_Gravado   DOUBLE PRECISION,
    Linea        VARCHAR(5),
    Usuario      VARCHAR(10),
    Computador   VARCHAR(20),
    Nombre_Linea VARCHAR(80),
    Tarifa       DOUBLE PRECISION,
    Fecha        DATE,
    Tesoreria    VARCHAR(10),
    Minimo       INTEGER,
    Maximo       INTEGER)
AS
DECLARE VARIABLE V_Personal VARCHAR(15);
DECLARE VARIABLE V_Maquina  VARCHAR(20);
DECLARE VARIABLE V_Min_Max  VARCHAR(25);
BEGIN
  V_Min_Max = '';
  FOR SELECT C.Codusuario, C.Computador, TRIM(C.Tipo), TRIM(C.Prefijo),
             TRIM(C.Numero), C.Fecha, D.Tesoreria

      FROM Comprobantes C
      JOIN Documentos D ON (C.Tipo = D.Codigo)
      WHERE C.Fecha = :Fecha_
            AND TRIM(C.Tipo) LIKE :Tipo_ || '%'
            AND TRIM(C.Computador) LIKE :Compu_ || '%'
            AND C.Bloqueado <> 'N'
            AND D.Grupo = 'VENTA'
            AND D.Es_Compra_Venta = 'S'
            AND D.Signo = 'CREDITO'
            AND D.M_Aplica <> 'S'
      ORDER BY 2, 3, 4, 5
      INTO Usuario, Computador, Tipo, Prefijo, Numero, Fecha, Tesoreria
  DO
  BEGIN
    -- TR_Inventario
    FOR SELECT Renglon, Codreferencia, Salida, Bruto * Salida,
               Descuento * Salida
        FROM Tr_Inventario
        WHERE Tipo = :Tipo
              AND Prefijo = :Prefijo
              AND Numero = :Numero
        INTO Renglon, Referencia, Cantidad, Bruto, Descuento
    DO
    BEGIN

      -- Impuestos
      Grvado = 0;
      No_Gravado = 0;
      SELECT SUM(IIF(Es_Tarifa = 'S', Credito, 0)),
             SUM(IIF(Es_Tarifa = 'N', Credito, 0))
      FROM Reg_Impuestos
      WHERE Tipo = :Tipo
            AND Prefijo = :Prefijo
            AND Numero = :Numero
            AND Renglon = :Renglon
      INTO Grvado, No_Gravado;

      --Tarifa
      Tarifa = NULL;
      SELECT FIRST 1 Tarifa
      FROM Reg_Impuestos
      WHERE Tipo = :Tipo
            AND Prefijo = :Prefijo
            AND Numero = :Numero
            AND Renglon = :Renglon
            AND Tarifa > 0
      INTO Tarifa;
      Tarifa = COALESCE(Tarifa, 0);

      -- Linea
      Linea = NULL;
      Nombre_Linea = NULL;
      SELECT Codlinea
      FROM Referencias
      WHERE Codigo = :Referencia
      INTO Linea;

      SELECT Nombre
      FROM Lineas
      WHERE Codigo = :Linea
      INTO Nombre_Linea;

      -- Usuarios
      V_Personal = NULL;
      SELECT Codpersonal
      FROM Usuarios
      WHERE Codigo = :Usuario
      INTO V_Personal;
      V_Personal = COALESCE(V_Personal, '');

      -- Computador
      V_Maquina = NULL;
      SELECT Codigo2
      FROM Personal
      WHERE Codigo = :V_Personal
      INTO V_Maquina;

      IF ((TRIM(COALESCE(V_Maquina, '')) <> '') AND
          (Computador CONTAINING 'MIAMI')) THEN
      BEGIN
        UPDATE Comprobantes
        SET Computador = :V_Maquina
        WHERE Tipo = :Tipo
              AND Prefijo = :Prefijo
              AND Numero = :Numero;
        Computador = V_Maquina;
      END

      IF (V_Min_Max <> TRIM(:Computador) || TRIM(:Prefijo)) THEN
      BEGIN
        -- Minimo y Maximo
        SELECT MIN(CAST(C.Numero AS INTEGER)), MAX(CAST(C.Numero AS INTEGER))
        FROM Comprobantes C
        JOIN Documentos D ON (C.Tipo = D.Codigo)
        WHERE C.Fecha = :Fecha_
              AND TRIM(C.Tipo) LIKE :Tipo_ || '%'
              AND C.Computador = :Computador
              AND C.Prefijo = :Prefijo
              AND C.Bloqueado <> 'N'
              AND D.Grupo = 'VENTA'
              AND D.Es_Compra_Venta = 'S'
              AND D.Signo = 'CREDITO'
              AND D.M_Aplica <> 'S'

        INTO Minimo, Maximo;
        V_Min_Max = TRIM(:Computador) || TRIM(:Prefijo);
      END

      SUSPEND;
    END
  END
END;

CREATE OR ALTER PROCEDURE Pz_Diario_Ventas_Fpag (
    Fecha_  DATE,
    Tipo_   VARCHAR(5),
    Compu_  VARCHAR(20),
    Usuario VARCHAR(10))
RETURNS (
    Forma_Pag     VARCHAR(5),
    Nombre        VARCHAR(80),
    Transacciones INTEGER,
    Valor         DOUBLE PRECISION)
AS
DECLARE VARIABLE V_Tipo      VARCHAR(5);
DECLARE VARIABLE V_Prefijo   VARCHAR(5);
DECLARE VARIABLE V_Numero    VARCHAR(10);
DECLARE VARIABLE V_Valor_Fp  DOUBLE PRECISION;
DECLARE VARIABLE V_Total     DOUBLE PRECISION;
DECLARE VARIABLE V_Tesoreria VARCHAR(10);
BEGIN
  FOR SELECT Tipo, Prefijo, Numero, Tesoreria, SUM(COALESCE(Bruto, 0) - COALESCE(Descuento, 0) + COALESCE(Grvado, 0) + COALESCE(No_Gravado, 0))

      FROM Pz_Diario_Ventas(:Fecha_, :Tipo_, :Compu_)
      WHERE Usuario LIKE TRIM(:Usuario) || '%'
      GROUP BY 1, 2, 3, 4
      INTO V_Tipo, V_Prefijo, V_Numero, V_Tesoreria, V_Total

  DO
  BEGIN
    -- Formas de pago
    V_Valor_Fp = 0;
    FOR SELECT Codformaspago, Valor
        FROM Reg_Pagos
        WHERE Tipo = :V_Tipo
              AND Prefijo = :V_Prefijo
              AND Numero = :V_Numero

        INTO Forma_Pag, Valor
    DO
    BEGIN
      Transacciones = 1;
      V_Valor_Fp = V_Valor_Fp + Valor;
      SELECT Nombre
      FROM Formaspago
      WHERE Codigo = :Forma_Pag
      INTO Nombre;
      SUSPEND;
    END
    IF (V_Valor_Fp = 0) THEN
      IF (V_Tesoreria = 'CONTADO') THEN
      BEGIN
        Transacciones = 1;
        Forma_Pag = '01';
        Nombre = 'EFECTIVO';
        Valor = V_Total;
        SUSPEND;
      END
      ELSE
      BEGIN
        Transacciones = 1;
        Forma_Pag = 'CR';
        Nombre = 'CREDITO';
        Valor = V_Total;
        SUSPEND;
      END
    ELSE
    IF (V_Total <> V_Valor_Fp) THEN
    BEGIN
      Transacciones = 0;
      Forma_Pag = '01';
      Nombre = 'EFECTIVO';
      Valor = V_Total - V_Valor_Fp;
      SUSPEND;
    END
    V_Valor_Fp = 0;
  END
END;

CREATE OR ALTER PROCEDURE Pz_Diario_Ventas_Maq (
    Fecha_  DATE,
    Tipo_   VARCHAR(5),
    Compu_  VARCHAR(20),
    Usuario VARCHAR(10))
RETURNS (
    Transaccion   VARCHAR(20),
    Computador    VARCHAR(20),
    Transacciones INTEGER,
    Valor         DOUBLE PRECISION)
AS
BEGIN
  FOR SELECT Computador,
             SUM(COALESCE(Bruto, 0) - COALESCE(Descuento, 0) + COALESCE(Grvado, 0) + COALESCE(No_Gravado, 0))
      FROM Pz_Diario_Ventas(:Fecha_, :Tipo_, :Compu_)
      WHERE Usuario LIKE TRIM(:Usuario) || '%'
      GROUP BY 1
      INTO Computador, Valor
  DO
  BEGIN
    Transaccion = 'TRANSACCIONES';
    SELECT COUNT(1)
    FROM Comprobantes C
    JOIN Documentos D ON (C.Tipo = D.Codigo)
    WHERE C.Fecha = :Fecha_
          AND TRIM(C.Tipo) LIKE :Tipo_ || '%'
          AND C.Computador = :Computador
          AND Fecha = :Fecha_
          AND C.Bloqueado <> 'N'
          AND D.Grupo = 'VENTA'
          AND D.Es_Compra_Venta = 'S'
          AND D.Signo = 'CREDITO'
          AND D.M_Aplica <> 'S'
    INTO Transacciones;
    SUSPEND;
  END
END;

COMMIT WORK;


/******************************************************************************/
/***                 Actualiza iva y retenciones                            ***/
/******************************************************************************/

/* Actualiza año nuevo en impuestos y retenciones, al igual que las bases en retenciones.
Es un EXECUTE BLOCK, se debe tener presente cambiar las siguiente variables para los próximos años V_Anio, V_Anio_Nuevo, V_Base_Servicios, V_Base_Compras, V_Base_Loterias, V_Base_Cafe y V_Base_Agricola. */

EXECUTE BLOCK
AS
DECLARE VARIABLE V_Anio           INTEGER = 2022;
DECLARE VARIABLE V_Anio_Nuevo     INTEGER = 2023;
DECLARE VARIABLE V_Codigo         CHAR(5);
DECLARE VARIABLE V_Valor          NUMERIC(17,4);
DECLARE VARIABLE V_Tarifa         NUMERIC(15,4);
DECLARE VARIABLE V_Base           NUMERIC(17,4);
DECLARE VARIABLE V_Base_Servicios NUMERIC(17,4) = 170000;
DECLARE VARIABLE V_Base_Compras   NUMERIC(17,4) = 1145000;
DECLARE VARIABLE V_Base_Loterias  NUMERIC(17,4) = 2036000;
DECLARE VARIABLE V_Base_Cafe      NUMERIC(17,4) = 6786000;
DECLARE VARIABLE V_Base_Agricola  NUMERIC(17,4) = 3902000;
BEGIN

 /* Impuestos con registro en el anio base */
 FOR SELECT Codtipoimpuesto,
            Valor,
            Tarifa
     FROM Data_Impuestos
     WHERE Ano = :V_Anio
     ORDER BY 1
     INTO V_Codigo,
          V_Valor,
          V_Tarifa

 DO
 BEGIN

  UPDATE OR INSERT INTO Data_Impuestos (Codtipoimpuesto,Ano,Valor,Tarifa)
  VALUES (:V_Codigo,:V_Anio_Nuevo,:V_Valor,:V_Tarifa);

 END

 /* Retenciones con registro en el anio base */
 FOR SELECT Codtiporetencion,
            Base,
            Valor,
            Tarifa
     FROM Data_Retenciones
     WHERE Ano = :V_Anio
     ORDER BY 1
     INTO V_Codigo,
          V_Base,
          V_Valor,
          V_Tarifa

 DO
 BEGIN

  /* Cambiamos las 5 bases principales */
  IF (V_Base = 1026000) THEN
   V_Base = V_Base_Compras;

  IF (V_Base = 152000) THEN
   V_Base = V_Base_Servicios;

  IF (V_Base = 6081000) THEN
   V_Base = V_Base_Cafe;

  IF (V_Base = 3496000) THEN
   V_Base = V_Base_Agricola;

  IF (V_Base = 1824000) THEN
   V_Base = V_Base_Loterias;

  UPDATE OR INSERT INTO Data_Retenciones (Codtiporetencion,Ano,Base,Valor,Tarifa)
  VALUES (:V_Codigo,:V_Anio_Nuevo,:V_Base,:V_Valor,:V_Tarifa);

 END

END;

COMMIT WORK;