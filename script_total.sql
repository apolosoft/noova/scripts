
/* VIGENCIA PARA ENVIAR CON CURRENT_DATE */
UPDATE OR INSERT INTO CODIGOS_FE (CODIGO, VIGENCIA)
                          VALUES ('FACTURA', 5)
                        MATCHING (CODIGO);
UPDATE OR INSERT INTO CODIGOS_FE (CODIGO, VIGENCIA)
                          VALUES ('EXPORTACION', 5)
                        MATCHING (CODIGO);
UPDATE OR INSERT INTO CODIGOS_FE (CODIGO, VIGENCIA)
                          VALUES ('POS ELECTRONICO', 5)
                        MATCHING (CODIGO);
UPDATE OR INSERT INTO CODIGOS_FE (CODIGO, VIGENCIA)
                          VALUES ('NOTA CREDITO', 5)
                        MATCHING (CODIGO);      
UPDATE OR INSERT INTO CODIGOS_FE (CODIGO, VIGENCIA)
                          VALUES ('NOTA CREDITO POS', 5)
                        MATCHING (CODIGO);                                          
UPDATE OR INSERT INTO CODIGOS_FE (CODIGO, VIGENCIA)
                          VALUES ('CONTINGENCIA', 60)
                        MATCHING (CODIGO);                                          

COMMIT WORK;

/******************************************************************************/
/***                           Stored procedures                            ***/
/******************************************************************************/



SET TERM ^ ;

CREATE OR ALTER PROCEDURE PZ_CUBO_SALDOS_BASE (
    FECHA_DESDE_ DATE,
    FECHA_HASTA_ DATE,
    CUENTA_DESDE_ CHAR(30),
    CUENTA_HASTA_ CHAR(30))
RETURNS (
    ANO INTEGER,
    MES INTEGER,
    CUENTA CHAR(30),
    CENTRO CHAR(5),
    TERCERO CHAR(15),
    INICIAL DOUBLE PRECISION,
    DEBITO DOUBLE PRECISION,
    CREDITO DOUBLE PRECISION,
    FINAL DOUBLE PRECISION,
    NOMBRE_CUENTA CHAR(80),
    NOMBRE_CENTRO CHAR(80),
    NOMBRE_TERCERO VARCHAR(164))
AS
BEGIN
  SUSPEND;
END^





CREATE OR ALTER PROCEDURE PZ_CUBO_SALDOS (
    FECHA_DESDE DATE,
    FECHA_HASTA DATE,
    CUENTA_DESDE CHAR(30),
    CUENTA_HASTA CHAR(30))
RETURNS (
    ANO INTEGER,
    MES CHAR(15),
    CUENTA CHAR(115),
    CENTRO CHAR(88),
    TERCERO CHAR(182),
    INICIAL DOUBLE PRECISION,
    DEBITO DOUBLE PRECISION,
    CREDITO DOUBLE PRECISION,
    FINAL DOUBLE PRECISION)
AS
BEGIN
  SUSPEND;
END^






SET TERM ; ^



/******************************************************************************/
/***                           Stored procedures                            ***/
/******************************************************************************/



SET TERM ^ ;

CREATE OR ALTER PROCEDURE PZ_CUBO_SALDOS_BASE (
    FECHA_DESDE_ DATE,
    FECHA_HASTA_ DATE,
    CUENTA_DESDE_ CHAR(30),
    CUENTA_HASTA_ CHAR(30))
RETURNS (
    ANO INTEGER,
    MES INTEGER,
    CUENTA CHAR(30),
    CENTRO CHAR(5),
    TERCERO CHAR(15),
    INICIAL DOUBLE PRECISION,
    DEBITO DOUBLE PRECISION,
    CREDITO DOUBLE PRECISION,
    FINAL DOUBLE PRECISION,
    NOMBRE_CUENTA CHAR(80),
    NOMBRE_CENTRO CHAR(80),
    NOMBRE_TERCERO VARCHAR(164))
AS
DECLARE VARIABLE Mes_I   INTEGER;
DECLARE VARIABLE Mes_F   INTEGER;
DECLARE VARIABLE Ano_I   INTEGER;
DECLARE VARIABLE Ano_F   INTEGER;
DECLARE VARIABLE V_Desde DATE;
DECLARE VARIABLE V_Hasta DATE;
BEGIN
  Mes_I = EXTRACT(MONTH FROM :Fecha_Desde_);
  Mes_F = EXTRACT(MONTH FROM :Fecha_Hasta_);
  Ano_I = EXTRACT(YEAR FROM :Fecha_Desde_);
  Ano_F = EXTRACT(YEAR FROM :Fecha_Hasta_);

  Ano = :Ano_I;
  Mes = :Mes_I;

  WHILE (:Ano <= :Ano_F) DO
  BEGIN
    IF (:Ano > :Ano_I) THEN
      Mes = 1;
    WHILE (:Mes <= 12) DO
    BEGIN
      SELECT Inicio,
             Fin
      FROM Sys_Inicio_Fin_Mes('01.' || RIGHT(:Mes + 100, 2) || '.' || CAST(:Ano AS CHAR(4)))
      INTO V_Desde,
           V_Hasta;

      FOR SELECT Cuenta,
                 Centro,
                 Tercero,
                 Inicial,
                 Debito,
                 Credito,
                 Inicial + Debito - Credito,
                 Nombre_Cuenta,
                 Nombre_Tercero,
                 Nombre_Centro
          FROM Conta_Dinamico(:V_Desde, :V_Hasta, :Cuenta_Desde_, :Cuenta_Hasta_, '%', '%', '%', '%', '%', '%', 'S', 'S', 'N', 'N', 'N', 'N', 'S', 'S', 'N', 'N') P
          INTO Cuenta,
               Centro,
               Tercero,
               Inicial,
               Debito,
               Credito,
               Final,
               Nombre_Cuenta,
               Nombre_Tercero,
               Nombre_Centro
      DO
      BEGIN
        SUSPEND;
      END
      IF (:Mes = :Mes_F AND
          :Ano = :Ano_F) THEN
        EXIT;

      Mes = Mes + 1;
    END
    Ano = Ano + 1;
  END
END^


CREATE OR ALTER PROCEDURE PZ_CUBO_SALDOS (
    FECHA_DESDE DATE,
    FECHA_HASTA DATE,
    CUENTA_DESDE CHAR(30),
    CUENTA_HASTA CHAR(30))
RETURNS (
    ANO INTEGER,
    MES CHAR(15),
    CUENTA CHAR(115),
    CENTRO CHAR(88),
    TERCERO CHAR(182),
    INICIAL DOUBLE PRECISION,
    DEBITO DOUBLE PRECISION,
    CREDITO DOUBLE PRECISION,
    FINAL DOUBLE PRECISION)
AS
DECLARE VARIABLE V_Nombre_Cuenta  CHAR(80);
DECLARE VARIABLE V_Nombre_Centro  CHAR(80);
DECLARE VARIABLE V_Nombre_Tercero VARCHAR(164);
DECLARE VARIABLE V_Mes            INTEGER;
BEGIN

  FOR SELECT Ano,
             Mes,
             Cuenta,
             Centro,
             Tercero,
             Nombre_Cuenta,
             Nombre_Tercero,
             Nombre_Centro,
             SUM(Inicial),
             SUM(Debito),
             SUM(Credito),
             SUM(Final)
      FROM Pz_Cubo_Saldos_Base(:Fecha_Desde, :Fecha_Hasta, :Cuenta_Desde, :Cuenta_Hasta)

      GROUP BY 1, 2, 3, 4, 5, 6, 7, 8
      ORDER BY 1, 2, 3, 4, 5, 6, 7, 8
      INTO Ano,
           V_Mes,
           Cuenta,
           Centro,
           Tercero,
           V_Nombre_Cuenta,
           V_Nombre_Tercero,
           V_Nombre_Centro,
           Inicial,
           Debito,
           Credito,
           Final
  DO
  BEGIN
    Cuenta = TRIM(:Cuenta) || ': ' || TRIM(:V_Nombre_Cuenta);
    Centro = COALESCE(TRIM(:Centro) || ': ' || TRIM(:V_Nombre_Centro), '');
    Tercero = COALESCE(TRIM(:Tercero) || ': ' || TRIM(:V_Nombre_Tercero), '');
    Mes = CASE :V_Mes
            WHEN 1 THEN '01.ENERO'
            WHEN 2 THEN '02.FEBRERO'
            WHEN 3 THEN '03.MARZO'
            WHEN 4 THEN '04.ABRIL'
            WHEN 5 THEN '05.MAYO'
            WHEN 6 THEN '06.JUNIO'
            WHEN 7 THEN '07.JULIO'
            WHEN 8 THEN '08.AGOSTO'
            WHEN 9 THEN '09.SEPTIEMBRE'
            WHEN 10 THEN '10.OCTUBRE'
            WHEN 11 THEN '11.NOVIEMBRE'
            WHEN 12 THEN '12.DICIEMBRE'
          END;

    SUSPEND;

  END
END^



SET TERM ; ^

UPDATE OR INSERT INTO CUBOS (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, FORMATO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD)
                     VALUES ('CUEC010101', 'SALDOS DE CUENTAS AUXILIARES MES POR MES - SALDO INICIAL Y FINAL', 'Muestra los saldos de las cuentas auxiliares por mes con su respectivo saldo inicial, debitos, creditos y saldo final por rango de fechas y cuentas especificas', 'SELECT Ano,
       Mes,
       Cuenta,
       Centro,
       Tercero,
       Inicial,
       Debito,
       Credito,
       Final
FROM Pz_Cubo_Saldos(:Fecha_Desde, :Fecha_Hasta, :Cuenta_Desde, :Cuenta_Hasta)', 'S', 'CONTABLE', :h00000000_00000000, 'N', 'AUXILIAR DE CUENTAS', 'N', 'LIBRE', 5, 'N', 1)
                   MATCHING (CODIGO);


COMMIT WORK;