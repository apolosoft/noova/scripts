DELETE FROM Exportados WHERE Pt = 'NOOVA_NE';

COMMIT WORK;

UPDATE PTA SET 
    XML_TEMPLATE = '<?xml version="1.0" encoding="UTF-8"?>
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
    xmlns:noov="http://noova/">
    <soapenv:Header>
        <noov:AuthHeader>
            <noov:Username>UsuarioNoova</noov:Username>
            <noov:Password>ClaveNoova</noov:Password>
        </noov:AuthHeader>
    </soapenv:Header>
    <!--971 Nomina 06/Mar/2025 v1-->
    <soapenv:Body>
        <noov:SetNomNominas>
            <noov:nomina>
                nom:nomina
                <noov:Periodo>
                    nom:periodo
                </noov:Periodo>
                <noov:InformacionGeneral>nom:inf_general</noov:InformacionGeneral>
                <noov:LNotas>nom:notas</noov:LNotas>
                <noov:Empleador>nom:empleador</noov:Empleador>
                <noov:Trabajador>nom:trabajador</noov:Trabajador>
                <noov:Pago>nom:pago</noov:Pago>
                <noov:Devengados>
                    <noov:Basico>nom:basico</noov:Basico>
                    nom:transporte
                    <noov:LHorasExtras>nom:horas_extras</noov:LHorasExtras>
                    <noov:LVacaciones>nom:vacaciones</noov:LVacaciones>
                    nom:primas
                    nom:cesantias
                    nom:dotacion
                    nom:indemnizacion
                    <noov:LIncapacidades>nom:incapacidad</noov:LIncapacidades>
                    <noov:LLicencias>nom:licencias</noov:LLicencias>
                    <noov:LBonificaciones>nom:bonificacion</noov:LBonificaciones>
                    <noov:LAuxilios>nom:auxilios</noov:LAuxilios>
                    <noov:LHuelgasLegales>nom:huelgas</noov:LHuelgasLegales>
                    <noov:LBonoEPCTVs>nom:bonos</noov:LBonoEPCTVs>
                    <noov:LComisiones>nom:comision</noov:LComisiones>
                    <noov:LOtrosConceptos>nom:otro_devengados</noov:LOtrosConceptos>
                </noov:Devengados>
                <noov:Deducciones>
                    nom:salud
                    nom:pension
                    nom:fondosp
                    <noov:LSindicatos>nom:sindicato</noov:LSindicatos>
                    <noov:LSanciones>nom:sancion</noov:LSanciones>
                    <noov:LLibranzas>nom:libranzas</noov:LLibranzas>
                    nom:pag_ter
                    nom:anticipos
                    nom:pvoluntaria
                    nom:retencionfuente
                    nom:ahorrofomentoconstr
                    nom:cooperativa
                    nom:embargofiscal
                    nom:plancomplementarios
                    nom:educacion
                    nom:reintegro
                    nom:deuda
                    nom:otras_ded
                </noov:Deducciones>
                nom:predecesor
                </noov:nomina>
        </noov:SetNomNominas>
    </soapenv:Body>
</soapenv:Envelope>'
WHERE (CODIGO = 'NOOVA_NE');


COMMIT WORK;

UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1005, 'nom:nomina', 'DOC', '-- Encabezado NE 969 Nomina 31/Oct/2024 v1
WITH V_Pref
AS (SELECT IIF(TRIM(COALESCE(Codprefijo, ''_'')) = ''_'', ''NE'', TRIM(Codprefijo)) Prefi
    FROM Val_Documentos
    WHERE Coddocumento = ''NE'')

SELECT 1 "noov:Nvsuc_codi",
       ''NE'' "noov:Nvnom_pref",
       :Numero: "noov:Nvnom_cons",
       ''NE'' || :Numero: "noov:Nvnom_nume",
       ''NM'' "noov:Nvope_tipo",
       EXTRACT(YEAR FROM CAST(:Fecha_Hasta: AS DATE)) || ''-'' || RIGHT(EXTRACT(MONTH FROM CAST(:Fecha_Hasta: AS DATE)) + 100, 2) || ''-'' || RIGHT(EXTRACT(DAY FROM CAST(:Fecha_Hasta: AS DATE)) + 100, 2) "noov:Nvnom_fpag",
       2000 "noov:Nvnom_redo",
       COALESCE(Adicion, 0) "noov:Nvnom_devt",
       COALESCE(Deduccion, 0) "noov:Nvnom_dedt",
       COALESCE(Neto, 0) "noov:Nvnom_comt",
       IIF(:Clase_Fe: = ''FACTURA'', '''', ''CUNE'') "noov:Nvnom_cnov",
       (SELECT Ajuste
        FROM Pz_Ne_Ajuste(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:)) "noov:Nvnom_tipo"
FROM Pz_Ne_Resumen(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:)
JOIN V_Pref Ne ON (1 = 1)', 'S', 5, 'NOOVA_NE', 'FACTURA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1010, 'nom:periodo', 'DOC', '-- Periodo

WITH V_Fechas
AS (SELECT Codpersonal,
           Desde,
           Hasta,
           MAX(Ingreso) Ingreso,
           MAX(Retiro) Retiro
    FROM (SELECT S.Codpersonal,
                 CAST(:Fecha_Desde: AS DATE) Desde,
                 CAST(:Fecha_Hasta: AS DATE) Hasta,
                 MAX(S.Desde) Ingreso,
                 CAST(''01/01/1900'' AS DATE) Retiro
          FROM Salarios S
          WHERE S.Codpersonal = :Receptor:
                AND S.Columna_Desde = ''INGRESO''
                AND S.Desde <= :Fecha_Hasta:
          GROUP BY 1, 2, 3
          UNION
          SELECT S.Codpersonal,
                 CAST(:Fecha_Desde: AS DATE),
                 CAST(:Fecha_Hasta: AS DATE),
                 CAST(''01/01/1900'' AS DATE),
                 MAX(S.Hasta)
          FROM Salarios S
          WHERE S.Codpersonal = :Receptor:
                AND S.Columna_Hasta = ''RETIRO''
                AND S.Hasta <= :Fecha_Hasta:
          GROUP BY 1, 2, 3)
    GROUP BY 1, 2, 3)

SELECT EXTRACT(YEAR FROM Fe.Ingreso) || ''-'' || RIGHT(EXTRACT(MONTH FROM Fe.Ingreso) + 100, 2) || ''-'' || RIGHT(EXTRACT(DAY FROM Fe.Ingreso) + 100, 2) "noov:Nvper_fing",
       IIF(Fe.Retiro >= Fe.Desde AND Fe.Retiro <= Fe.Hasta, EXTRACT(YEAR FROM Fe.Retiro) || ''-'' || RIGHT(EXTRACT(MONTH FROM Fe.Retiro) + 100, 2) || ''-'' || RIGHT(EXTRACT(DAY FROM Fe.Retiro) + 100, 2), EXTRACT(YEAR FROM Fe.Hasta) || ''-'' || RIGHT(EXTRACT(MONTH FROM Fe.Hasta) + 100, 2) || ''-'' || RIGHT(EXTRACT(DAY FROM Fe.Hasta) + 100, 2)) "noov:Nvper_fret",
       EXTRACT(YEAR FROM Fe.Desde) || ''-'' || RIGHT(EXTRACT(MONTH FROM Fe.Desde) + 100, 2) || ''-'' || RIGHT(EXTRACT(DAY FROM Fe.Desde) + 100, 2) "noov:Nvper_fpin",
       EXTRACT(YEAR FROM Fe.Hasta) || ''-'' || RIGHT(EXTRACT(MONTH FROM Fe.Hasta) + 100, 2) || ''-'' || RIGHT(EXTRACT(DAY FROM Fe.Hasta) + 100, 2) "noov:Nvper_fpfi",
       DATEDIFF(DAY FROM Fe.Ingreso TO Fe.Hasta)+1 "noov:Nvper_tlab"
FROM V_Fechas Fe', 'S', 10, 'NOOVA_NE', 'FACTURA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1015, 'nom:inf_general', 'DOC', '-- Inf general

SELECT (SELECT Tipo_Nomina
        FROM Pz_Ne_Ajuste(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:)) "noov:Nvinf_tnom",
        P.Codtipoperiodo "noov:Nvinf_pnom",
        ''COP'' "noov:Nvinf_tmon"
FROM Personal P
WHERE P.Codigo = :Receptor:', 'S', 15, 'NOOVA_NE', 'FACTURA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1020, 'nom:notas', 'DOC', '-- Notas

SELECT DISTINCT N.Nombre "noov:string"
FROM Nominas N
LEFT JOIN Planillas P ON (N.Codigo = P.Codnomina)
LEFT JOIN Planillas_Ne Ps ON (N.Codigo = Ps.Codnomina)
WHERE (P.Codpersonal = :Receptor: OR (Ps.Codpersonal = :Receptor:))
      AND N.Fecha >= :Fecha_Desde:
      AND N.Fecha <= :Fecha_Hasta:', 'S', 20, 'NOOVA_NE', 'FACTURA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1025, 'nom:empleador', 'DOC', '-- Empleador

SELECT FIRST 1 T.Nombre "noov:Nvemp_nomb",
               T.Codigo "noov:Nvemp_nnit",
               T.Dv "noov:Nvemp_endv",
               Pa.Codigo_Fe "noov:Nvemp_pais",
               SUBSTRING(T.Codmunicipio FROM 1 FOR 2) "noov:Nvemp_depa",
               T.Codmunicipio "noov:Nvemp_ciud",
               T.Direccion "noov:Nvemp_dire"
FROM Terceros T
JOIN Paises Pa ON (T.Codpais = Pa.Codigo)
WHERE T.Codigo = :Emisor:', 'S', 25, 'NOOVA_NE', 'FACTURA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1030, 'nom:trabajador', 'DOC', '-- Trabajador

WITH V_Correo
AS (SELECT FIRST 1 Codigo,
                   TRIM(Email) Email,
                   COUNT(1) Cant
    FROM Contactos
    WHERE Codcargo = ''NECOR''
          AND Codtercero = :Emisor:
    GROUP BY 1, 2
    ORDER BY Codigo)
SELECT LPAD(IIF(TRIM(E.Codcotizante) = ''20'', ''23'', TRIM(E.Codcotizante)), 2, 0) "noov:Nvtra_tipo",
       LPAD(IIF(TRIM(E.Codsubcotizante) <> ''0'', ''1'', TRIM(E.Codsubcotizante)), 2, 0) "noov:Nvtra_stip",
       IIF(COALESCE(E.Alto_Riesgo, 0) = ''N'', ''false'', ''true'') "noov:Nvtra_arpe", --Actividades de alto riesgo
       IIF(E.Codidentidad = ''48'', ''47'', E.Codidentidad) "noov:Nvtra_dtip",
       E.Codigo "noov:Nvtra_ndoc",
       E.Apl1 "noov:Nvtra_pape",
       IIF(COALESCE(E.Apl2, '''') = '''', ''.'', E.Apl2) "noov:Nvtra_sape",
       E.Nom1 "noov:Nvtra_pnom",
       E.Nom2 "noov:Nvtra_onom",
       ''CO'' "noov:Nvtra_ltpa",
       SUBSTRING(E.Codmunicipio FROM 1 FOR 2) "noov:Nvtra_ltde",
       E.Codmunicipio "noov:Nvtra_ltci",
       E.Direccion "noov:Nvtra_ltdi",
       IIF(E.Salario_Integral = ''X'', ''true'', ''false'') "noov:Nvtra_sint",
       C.Codtipocontrato "noov:Nvtra_tcon", --Tipo Contrato
       COALESCE((SELECT FIRST 1 Basico
                 FROM Nom_Pila_Salarios(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:)), 0) "noov:Nvtra_suel",
       E.Codigo "noov:Nvtra_codt",
       IIF(Ne.Cant > 0, COALESCE(Ne.Email, E.Email), ''ne.mekano@gmail.com'') "noov:Nvtra_mail"
FROM Personal E
JOIN Identidades I ON (E.Codidentidad = I.Codigo)
LEFT JOIN Contratacion C ON (E.Codigo = C.Codpersonal)
LEFT JOIN V_Correo Ne ON 1 = 1
WHERE E.Codigo = :Receptor:
      AND ((C.Inicio <= :Fecha_Desde:
      AND COALESCE(C.Fin, DATEADD(1 MONTH TO CURRENT_DATE)) >= :Fecha_Hasta:)

      OR (C.Inicio >= :Fecha_Desde:
      AND COALESCE(C.Fin, DATEADD(1 MONTH TO CURRENT_DATE)) <= :Fecha_Hasta:)

      OR (C.Inicio >= :Fecha_Desde:
      AND COALESCE(C.Fin, DATEADD(1 MONTH TO CURRENT_DATE)) >= :Fecha_Hasta:)

      OR (C.Inicio <= :Fecha_Desde:
      AND COALESCE(C.Fin, DATEADD(1 MONTH TO CURRENT_DATE)) <= :Fecha_Hasta:))', 'S', 30, 'NOOVA_NE', 'FACTURA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1035, 'nom:pago', 'DOC', '--  Pagos

WITH V_Medio
AS (SELECT COALESCE((SELECT FIRST 1 F.Codigo_Fe
                     FROM Medios_Pago M
                     JOIN Formaspago F ON (M.Codformaspago = F.Codigo)
                     WHERE M.Codpersonal = :Receptor:),
           (SELECT FIRST 1 F.Codigo_Fe
            FROM Medios_Pago M
            JOIN Formaspago F ON (M.Codformaspago = F.Codigo)
            WHERE Codpersonal IS NULL)) Medio
    FROM Rdb$Database)
SELECT 1 "noov:Nvpag_form",
       M.Medio "noov:Nvpag_meto", --Metodo de pago
       B.Nombre "noov:Nvpag_banc", --Nombre banco
       Tc.Nombre "noov:Nvpag_tcue",
       E.Banco "noov:Nvpag_ncue"
FROM Personal E
LEFT JOIN Entidades B ON (E.Codentidad = B.Codigo)
LEFT JOIN Tipocuentas Tc ON (E.Codtipocuenta = Tc.Codigo)
JOIN V_Medio M ON (1 = 1)
WHERE E.Codigo = :Receptor:', 'S', 35, 'NOOVA_NE', 'FACTURA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1040, 'nom:basico', 'DOC', '-- Basico

SELECT COALESCE((SELECT IIF(ROUND(Valor) = 0, 1, ROUND(Valor))
                 FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''DTRA'')), 0) "noov:Nvbas_dtra",
       COALESCE((SELECT Valor
                 FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''STRA'')), 0) "noov:Nvbas_stra"
FROM Rdb$Database', 'S', 40, 'NOOVA_NE', 'FACTURA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1042, 'nom:transporte', 'DOC', '-- Transporte NE

SELECT SUM(COALESCE(Valor, 0)) "noov:Nvtrn_auxt",
       SUM(0) "noov:Nvbon_vias",
       SUM(0) "noov:Nvbon_vins"
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''AUX'')
HAVING SUM(Valor) > 0

UNION ALL

SELECT SUM(0),
       SUM(COALESCE(Valor, 0)),
       SUM(0)
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''VIAS'')
HAVING SUM(Valor) > 0

UNION ALL

SELECT SUM(0),
       SUM(0),
       SUM(COALESCE(Valor, 0))
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''VINS'')
HAVING SUM(Valor) > 0', 'S', 42, 'NOOVA_NE', 'FACTURA', 'noov:Transporte', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1045, 'nom:horas_extras', 'DOC', '-- Horas Extras NE

SELECT
       --'''' "noov:Nvcom_fini",
       -- '''' "noov:Nvcom_ffin",
       Cantidad "noov:Nvcom_cant",
       Valor "noov:Nvcom_pago",
       Codgrupo_Ne "noov:Nvhor_tipo",
       Codigo_Ne "noov:Nvhor_porc"
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''%'')
WHERE Codgrupo_Ne = ''HEN''
      AND Valor > 0

UNION ALL

SELECT
       --'''' "noov:Nvcom_fini",
       -- '''' "noov:Nvcom_ffin",
       Cantidad "noov:Nvcom_cant",
       Valor "noov:Nvcom_pago",
       Codgrupo_Ne "noov:Nvhor_tipo",
       Codigo_Ne "noov:Nvhor_porc"
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''%'')
WHERE Codgrupo_Ne = ''HENDF''
      AND Valor > 0

UNION ALL

SELECT
       --'''' "noov:Nvcom_fini",
       -- '''' "noov:Nvcom_ffin",
       Cantidad "noov:Nvcom_cant",
       Valor "noov:Nvcom_pago",
       Codgrupo_Ne "noov:Nvhor_tipo",
       Codigo_Ne "noov:Nvhor_porc"
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''%'')
WHERE Codgrupo_Ne = ''HED''
      AND Valor > 0

UNION ALL

SELECT
       --'''' "noov:Nvcom_fini",
       -- '''' "noov:Nvcom_ffin",
       Cantidad "noov:Nvcom_cant",
       Valor "noov:Nvcom_pago",
       Codgrupo_Ne "noov:Nvhor_tipo",
       Codigo_Ne "noov:Nvhor_porc"
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''%'')
WHERE Codgrupo_Ne = ''HEDDF''
      AND Valor > 0

UNION ALL

SELECT
       --'''' "noov:Nvcom_fini",
       -- '''' "noov:Nvcom_ffin",
       Cantidad "noov:Nvcom_cant",
       Valor "noov:Nvcom_pago",
       Codgrupo_Ne "noov:Nvhor_tipo",
       Codigo_Ne "noov:Nvhor_porc"
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''%'')
WHERE Codgrupo_Ne = ''HRN''
      AND Valor > 0

UNION ALL

SELECT
       --'''' "noov:Nvcom_fini",
       -- '''' "noov:Nvcom_ffin",
       Cantidad "noov:Nvcom_cant",
       Valor "noov:Nvcom_pago",
       Codgrupo_Ne "noov:Nvhor_tipo",
       Codigo_Ne "noov:Nvhor_porc"
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''%'')
WHERE Codgrupo_Ne = ''HRDDF''
      AND Valor > 0

UNION ALL

SELECT
       --'''' "noov:Nvcom_fini",
       -- '''' "noov:Nvcom_ffin",
       Cantidad "noov:Nvcom_cant",
       Valor "noov:Nvcom_pago",
       Codgrupo_Ne "noov:Nvhor_tipo",
       Codigo_Ne "noov:Nvhor_porc"
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''%'')
WHERE Codgrupo_Ne = ''HRNDF''
      AND Valor > 0', 'S', 45, 'NOOVA_NE', 'FACTURA', 'noov:DTOHorasExtras', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1160, 'nom:vacaciones', 'DOC', '-- Vacaciones NE

SELECT
--'''' "noov:Nvcom_fini",
--      '''' "noov:Nvcom_ffin",
       SUM(IIF(Codgrupo_Ne IN (''VAC1Q'', ''VAC2Q''), IIF(ROUND(Valor) = 0, 1, ROUND(Valor)), 0)) "noov:Nvcom_cant",
       SUM(IIF(Codgrupo_Ne IN (''VAC1S'', ''VAC2S''), Valor, 0)) "noov:Nvcom_pago",
       Codigo_Ne "noov:Nvvac_tipo"
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''%'')
WHERE Codgrupo_Ne IN (''VAC1Q'', ''VAC2Q'', ''VAC1S'', ''VAC2S'')
GROUP BY 3
HAVING SUM(Valor) > 0', 'S', 160, 'NOOVA_NE', 'FACTURA', 'noov:DTOVacaciones', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1165, 'nom:primas', 'DOC', '-- Primas NE

SELECT SUM("noov:Nvpri_cant") "noov:Nvpri_cant",
       SUM("noov:Nvpri_pago") "noov:Nvpri_pago",
       SUM("noov:Nvpri_pagn") "noov:Nvpri_pagn"
FROM (SELECT COALESCE(IIF(ROUND(Valor) = 0, 1, ROUND(Valor)), 0) "noov:Nvpri_cant",
             0 "noov:Nvpri_pago",
             0 "noov:Nvpri_pagn"
      FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''%'')
      WHERE (Codgrupo_Ne = ''PRIQ'')

      UNION ALL

      SELECT 0,
             COALESCE(Valor, 0),
             0
      FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''%'')
      WHERE (Codgrupo_Ne = ''PRIP'')
            AND (Valor > 0)

      UNION ALL

      SELECT 0,
             0,
             COALESCE(Valor, 0)
      FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''%'')
      WHERE (Codgrupo_Ne = ''PRIPN'')
            AND (Valor > 0))
HAVING SUM("noov:Nvpri_cant" + "noov:Nvpri_pago" + "noov:Nvpri_pagn") > 1', 'S', 165, 'NOOVA_NE', 'FACTURA', 'noov:Primas', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1170, 'nom:cesantias', 'DOC', '-- Cesantias NE

SELECT COALESCE((SELECT SUM(Valor)
                 FROM Pz_Ne_Grupo_Ces(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:)), 0) "noov:Nvces_pago",
       IIF(COALESCE((SELECT Valor
                     FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''CESPA'')), 0) = 0, 0,
       (SELECT Codigo_Ne
        FROM Grupo_Ne
        WHERE Codigo = ''CESPOR'')) "noov:Nvces_porc",
       COALESCE((SELECT Valor
                 FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''CESIPA'')), 0) "noov:Nvces_pagi"
FROM Rdb$Database
WHERE COALESCE((SELECT SUM(Valor)
                FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''%'')
                WHERE Codgrupo_Ne IN (''CESPA'', ''CESIPA'')), 0) > 0', 'S', 170, 'NOOVA_NE', 'FACTURA', 'noov:Cesantias', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1175, 'nom:incapacidad', 'DOC', '-- Incapacidades NE

SELECT SUM("noov:Nvcom_cant") "noov:Nvcom_cant",
       SUM("noov:Nvcom_pago") "noov:Nvcom_pago",
       "noov:Nvinc_tipo"
FROM (SELECT
             -- '''' "noov:Nvcom_fini",
             --      '''' "noov:Nvcom_ffin",
             IIF(ROUND(SUM(Cantidad)) = 0, 1, ROUND(SUM(Cantidad))) "noov:Nvcom_cant",
             0 "noov:Nvcom_pago",
             Codigo_Ne "noov:Nvinc_tipo"
      FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''%'')
      WHERE Codgrupo_Ne IN (''INCOQ'', ''INPRQ'', ''INLAQ'')
            AND Cantidad > 0
      GROUP BY 3

      UNION ALL

      SELECT
             -- '''' "noov:Nvcom_fini",
             --      '''' "noov:Nvcom_ffin",
             0,
             SUM(Valor),
             Codigo_Ne
      FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''%'')
      WHERE Codgrupo_Ne IN (''INCOV'', ''INPRV'', ''INLAV'')
            AND Valor > 0
      GROUP BY 3)
GROUP BY 3', 'S', 175, 'NOOVA_NE', 'FACTURA', 'noov:DTOIncapacidad', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1195, 'nom:licencias', 'DOC', '-- Licencias NE

SELECT SUM("noov:Nvcom_cant") "noov:Nvcom_cant",
       SUM("noov:Nvcom_pago") "noov:Nvcom_pago",
       "noov:Nvlic_tipo"
FROM (SELECT
             -- '''' "noov:Nvcom_fini",
             --      '''' "noov:Nvcom_ffin",
             IIF(ROUND(SUM(Cantidad)) = 0, 1, ROUND(SUM(Cantidad))) "noov:Nvcom_cant",
             0 "noov:Nvcom_pago",
             Codigo_Ne "noov:Nvlic_tipo"
      FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''%'')
      WHERE Codgrupo_Ne IN (''LMQ'', ''LRQ'', ''LNQ'')
            AND Cantidad > 0
      GROUP BY 3

      UNION ALL

      SELECT
             -- '''' "noov:Nvcom_fini",
             --      '''' "noov:Nvcom_ffin",
             0,
             SUM(Valor),
             Codigo_Ne
      FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''%'')
      WHERE Codgrupo_Ne IN (''LMV'', ''LRV'', ''LNV'')
            AND Valor > 0
      GROUP BY 3)
GROUP BY 3', 'S', 195, 'NOOVA_NE', 'FACTURA', 'noov:DTOLicencia', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1197, 'nom:bonificacion', 'DOC', '-- Bonificacion NE

SELECT COALESCE(Valor, 0) "noov:Nvbon_bofs",
       0 "noov:Nvbon_bons"
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''BOFS'')
WHERE (Valor > 0)

UNION ALL

SELECT 0,
       COALESCE(Valor, 0)
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''BONS'')
WHERE (Valor > 0)', 'S', 197, 'NOOVA_NE', 'FACTURA', 'noov:DTOBonificacion', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1199, 'nom:auxilios', 'DOC', '-- Auxilios

SELECT COALESCE(Valor, 0) "noov:Nvaux_auxs",
       0 "noov:Nvaux_auns"
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''AUXS'')
WHERE (Valor > 0)

UNION ALL

SELECT 0,
       COALESCE(Valor, 0)
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''AUNS'')
WHERE (Valor > 0)', 'S', 199, 'NOOVA_NE', 'FACTURA', 'noov:DTOAuxilio', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1200, 'nom:huelgas', 'DOC', '-- Huelgas NE

WITH V_Pago
AS (SELECT Gn.Codigo Tipo,
           SUM(P.Adicion + P.Deduccion) Valor
    FROM Planillas P
    JOIN Nominas N ON (P.Codnomina = N.Codigo)
    JOIN Rubros R ON (P.Codrubro = R.Codigo)
    JOIN Grupo_Ne Gn ON (R.Codgrupo_Ne = Gn.Codigo)
    LEFT JOIN Generalidades G ON (R.Codigo = G.Codrubro AND P.Codpersonal = G.Codpersonal AND ((N.Fecha BETWEEN G.Inicio AND G.Fin) OR (G.Inicio BETWEEN N.Desde AND N.Hasta) OR (G.Fin BETWEEN N.Desde AND N.Hasta)))
    WHERE P.Codpersonal = :Receptor:
          AND N.Fecha >= :Fecha_Desde:
          AND N.Fecha <= :Fecha_Hasta:
          AND R.Codgrupo_Ne = ''HUV''
    GROUP BY 1)

SELECT G.Inicio "noov:Nvcom_fini",
       G.Fin "noov:Nvcom_ffin",
       P.Valor "noov:Nvcom_cant",
       Pago.Valor "noov:Nvcom_pago"
FROM Planillas P
JOIN Nominas N ON (P.Codnomina = N.Codigo)
JOIN Rubros R ON (P.Codrubro = R.Codigo)
JOIN Grupo_Ne Gn ON (R.Codgrupo_Ne = Gn.Codigo)
JOIN Generalidades G ON (R.Codigo = G.Codrubro AND P.Codpersonal = G.Codpersonal AND ((N.Fecha BETWEEN G.Inicio AND G.Fin) OR (G.Inicio BETWEEN N.Desde AND N.Hasta) OR (G.Fin BETWEEN N.Desde AND N.Hasta)))
JOIN V_Pago Pago ON (Gn.Codigo = Pago.Tipo)
WHERE P.Codpersonal = :Receptor:
      AND N.Fecha >= :Fecha_Desde:
      AND N.Fecha <= :Fecha_Hasta:
      AND R.Codgrupo_Ne = ''HUQ''', 'S', 235, 'NOOVA_NE', 'FACTURA', 'noov:DTOHuelgaLegal', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1205, 'nom:bonos', 'DOC', '-- Bonos

SELECT COALESCE(IIF(Codgrupo_Ne = ''PAGS'', Valor, 0), 0) "noov:Nvbon_pags",
       COALESCE(IIF(Codgrupo_Ne = ''PANS'', Valor, 0), 0) "noov:Nvbon_pans",
       COALESCE(IIF(Codgrupo_Ne = ''ALIS'', Valor, 0), 0) "noov:Nvbon_alis",
       COALESCE(IIF(Codgrupo_Ne = ''ALNS'', Valor, 0), 0) "noov:Nvbon_alns"
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''%'')
WHERE Codgrupo_Ne IN (''PAGS'', ''PANS'', ''ALIS'', ''ALNS'')', 'S', 236, 'NOOVA_NE', 'FACTURA', 'noov:DTOBonoEPCTV', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1210, 'nom:comision', 'DOC', '-- Comisiones

SELECT Valor "noov:decimal"
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''COMIS'')
WHERE Valor > 0', 'S', 210, 'NOOVA_NE', 'FACTURA', '', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1215, 'nom:dotacion', 'DOC', '-- Dotacion

SELECT Valor "noov:Dotacion"
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''DOTAC'')
WHERE Valor > 0', 'S', 215, 'NOOVA_NE', 'FACTURA', '', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1240, 'nom:otro_devengados', 'DOC', '-- Otros devengados

SELECT NOMRUBRO "noov:Nvotr_desc",
       IIF(CODGRUPO_NE = ''ODSA'', VALOR, 0) "noov:Nvotr_pags",
       IIF(CODGRUPO_NE = ''ODNS'', VALOR, 0) "noov:Nvotr_pans"
FROM PZ_NE_GRUPO_BASE(:RECEPTOR:, :FECHA_DESDE:, :FECHA_HASTA:)
WHERE (CODGRUPO_NE IN (''ODSA'', ''ODNS''))
      AND (VALOR > 0)

UNION ALL

SELECT ''CAUSACION VACACIONES'' "noov:Nvotr_desc",
       0 "noov:Nvotr_pags",
       VALOR "noov:Nvotr_pans"
FROM PZ_NE_GRUPO_BASE(:RECEPTOR:, :FECHA_DESDE:, :FECHA_HASTA:)
WHERE CODGRUPO_NE = ''ODNS_V''
      AND VALOR > 0', 'S', 240, 'NOOVA_NE', 'FACTURA', 'noov:DTOOtroDevengado', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1355, 'nom:salud', 'DOC', '-- Salud NE

SELECT COALESCE((SELECT IIF(Codcotizante IN (''51'', ''12'', ''19''), 0, Codigo_Ne)
                 FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''EPSDTO'') P), 0) "noov:Nvsal_porc",
       COALESCE((SELECT P.Valor
                 FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''EPSDTO'') P), 0) "noov:Nvsal_dedu"
FROM Rdb$Database', 'S', 355, 'NOOVA_NE', 'FACTURA', 'noov:Salud', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1360, 'nom:pension', 'DOC', '-- Pension NE

SELECT COALESCE((SELECT Codigo_Ne
                 FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''AFPDTO'') P), 0) "noov:Nvfon_porc",
       COALESCE((SELECT P.Valor
                 FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''AFPDTO'') P), 0) "noov:Nvfon_dedu"
FROM Rdb$Database', 'S', 360, 'NOOVA_NE', 'FACTURA', 'noov:FondoPension', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1365, 'nom:fondosp', 'DOC', 'SELECT "noov:Nvfsp_porc",
       "noov:Nvfsp_dedu",
       "noov:Nvfsp_posb",
       "noov:Nvfsp_desb"
FROM Pz_Ne_Fondo_Sp(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:)
WHERE "noov:Nvfsp_dedu" + "noov:Nvfsp_desb" > 0', 'S', 365, 'NOOVA_NE', 'FACTURA', 'noov:FondoSP', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1370, 'nom:sindicato', 'DOC', '-- Sindicato NE

SELECT Valor "noov:Nvsin_porc",
       0 "noov:Nvsin_dedu"
FROM Pz_Ne_Grupo_Constante(:Receptor:, EXTRACT(YEAR FROM CAST(:Fecha_Desde: AS DATE)))
WHERE Codgrupo_Ne = ''PORSIN''
      AND Valor > 0

UNION ALL

SELECT 0,
       Valor
FROM Pz_Ne_Grupo_Constante(:Receptor:, EXTRACT(YEAR FROM CAST(:Fecha_Desde: AS DATE)))
WHERE Codgrupo_Ne = ''VAL_SIN''
      AND Valor > 0', 'S', 370, 'NOOVA_NE', 'FACTURA', 'noov:DTOSindicato', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1375, 'nom:sancion', 'DOC', '-- Sancion NE

SELECT Valor "noov:Nvsan_sapu",
       0 "noov:Nvsan_sapv"
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''SAPU'')
WHERE Valor > 0

UNION ALL

SELECT 0,
       Valor
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''SAPV'')
WHERE Valor > 0', 'S', 375, 'NOOVA_NE', 'FACTURA', 'noov:DTOSancion', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1380, 'nom:libranzas', 'DOC', '-- Libranzas

SELECT Nomrubro "noov:Nvlib_desc",
       Valor "noov:Nvlib_dedu"
FROM Pz_Ne_Grupo_Base(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:)
WHERE Codgrupo_Ne = ''LIBRA''
      AND Valor > 0', 'S', 380, 'NOOVA_NE', 'FACTURA', 'noov:DTOLibranza', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1382, 'nom:pag_ter', 'DOC', '-- Pagos a Terceros

SELECT Valor "noov:decimal"
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''PAG_TER'')
WHERE Valor > 0', 'S', 382, 'NOOVA_NE', 'FACTURA', 'noov:LPagosTerceros', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1383, 'nom:anticipos', 'DOC', '-- Anticipos

SELECT Valor "noov:decimal"
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''DT02'')
WHERE Valor > 0', 'S', 383, 'NOOVA_NE', 'FACTURA', 'noov:LAnticipos', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1384, 'nom:pvoluntaria', 'DOC', '-- Pension Voluntaria

SELECT Valor "noov:PensionVoluntaria"
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''DT03'')
WHERE Valor > 0', 'S', 384, 'NOOVA_NE', 'FACTURA', '', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1385, 'nom:retencionfuente', 'DOC', '-- RetencionFuente

SELECT Valor "noov:RetencionFuente"
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''DT04'')
WHERE Valor > 0', 'S', 385, 'NOOVA_NE', 'FACTURA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1386, 'nom:ahorrofomentoconstr', 'DOC', '-- Ahorro Fomento Construccion

SELECT Valor "noov:AhorroFomentoConstr"
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''DT06'')
WHERE Valor > 0', 'S', 386, 'NOOVA_NE', 'FACTURA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1387, 'nom:cooperativa', 'DOC', '-- Cooperativa

SELECT Valor "noov:Cooperativa"
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''DT07'')
WHERE Valor > 0', 'S', 387, 'NOOVA_NE', 'FACTURA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1388, 'nom:embargofiscal', 'DOC', '-- Embargo Fiscal

SELECT Valor "noov:EmbargoFiscal"
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''DT08'')
WHERE Valor > 0', 'S', 388, 'NOOVA_NE', 'FACTURA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1389, 'nom:plancomplementarios', 'DOC', '-- Plan Complementarios

SELECT Valor "noov:PlanComplementarios"
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''DT09'')
WHERE Valor > 0', 'S', 389, 'NOOVA_NE', 'FACTURA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1390, 'nom:educacion', 'DOC', '-- Educacion

SELECT Valor "noov:Educacion"
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''DT10'')
WHERE Valor > 0', 'S', 390, 'NOOVA_NE', 'FACTURA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1391, 'nom:reintegro', 'DOC', '-- Reintegro

SELECT Valor "noov:Reintegro"
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''DT11'')
WHERE Valor > 0', 'S', 391, 'NOOVA_NE', 'FACTURA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1392, 'nom:deuda', 'DOC', '-- Deuda

SELECT Valor "noov:Deuda"
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''DT12'')
WHERE Valor > 0', 'S', 392, 'NOOVA_NE', 'FACTURA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1393, 'nom:otras_ded', 'DOC', '-- Otros deducciones

SELECT Valor "noov:decimal"
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''DT00'')
WHERE Valor > 0', 'S', 393, 'NOOVA_NE', 'FACTURA', 'noov:LOtrasDeducciones', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1394, 'nom:predecesor', 'DOC', '-- Predecesor

SELECT Numero "noov:Nvpre_nume",
       Cune "noov:Nvpre_cune",
       EXTRACT(YEAR FROM CAST(Fecha_Ne AS DATE)) || ''-'' || RIGHT(EXTRACT(MONTH FROM CAST(Fecha_Ne AS DATE)) + 100, 2) || ''-'' || RIGHT(EXTRACT(DAY FROM CAST(Fecha_Ne AS DATE)) + 100, 2) "noov:Nvpre_fgen"
FROM Pz_Ne_Ajuste(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:)
WHERE trim(Tipo_Nomina) = 103', 'S', 394, 'NOOVA_NE', 'FACTURA', 'noov:Predecesor', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1395, 'nom:indemnizacion', 'DOC', '--INDEMNIZACION

SELECT Valor "noov:indemnizacion"
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''INDEM'')
WHERE Valor > 0', 'S', 216, 'NOOVA_NE', 'FACTURA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);


COMMIT WORK;
