SET TERM ^ ;

CREATE OR ALTER PROCEDURE Pz_G_Contable2 (
    Fecha_Desde DATE,
    Fecha_Hasta DATE,
    Tipo        VARCHAR(5))
RETURNS (
    Tipo_Doc       VARCHAR(5),
    Prefijo        VARCHAR(5),
    Numero         VARCHAR(10),
    Fecha          DATE,
    Cuenta         VARCHAR(30),
    Tercero        VARCHAR(15),
    Centro         VARCHAR(5),
    Detalle        VARCHAR(80),
    Debito         DOUBLE PRECISION,
    Credito        DOUBLE PRECISION,
    Base           DOUBLE PRECISION,
    Usuario        VARCHAR(10),
    Nombre_Tercero VARCHAR(163),
    Nombre_Centro  VARCHAR(80))
AS
DECLARE VARIABLE V_Cod_Tercero VARCHAR(15);
BEGIN
  FOR SELECT Tipo,
             Prefijo,
             Numero,
             Fecha,
             Codusuario,
             Codtercero
      FROM Comprobantes
      WHERE Fecha >= :Fecha_Desde
            AND Fecha <= :Fecha_Hasta
            AND Tipo = :Tipo
      INTO Tipo_Doc,
           Prefijo,
           Numero,
           Fecha,
           Usuario,
           V_Cod_Tercero
  DO
  BEGIN
    FOR SELECT Codcuenta,
               Codtercero,
               Codcentro,
               Detalle,
               Debito,
               Credito,
               Base
        FROM Reg_Contable
        WHERE Tipo = :Tipo_Doc
              AND Prefijo = :Prefijo
              AND Numero = :Numero
              AND Codcuenta <> ''
        INTO Cuenta,
             Tercero,
             Centro,
             Detalle,
             Debito,
             Credito,
             Base
    DO
    BEGIN
      IF (Tercero IS NULL) THEN
        Tercero = COALESCE(Tercero, '');

      IF (Cuenta = '13050501') THEN
        Tercero = V_Cod_Tercero;

      Centro = COALESCE(Centro, '');
      Detalle = COALESCE(Detalle, '-');

      --MOSTRAMOS NOMBRE TERCERO
      SELECT Nombre_Tercero
      FROM Fn_Nombre_Tercero(:Tercero)
      INTO Nombre_Tercero;
      Nombre_Tercero = COALESCE(Nombre_Tercero, '');

      --MOSTRAMOS NOMBRE CENTRO

      SELECT Nombre_Centro
      FROM Fn_Nombre_Centro(:Centro)
      INTO Nombre_Centro;
      Nombre_Centro = COALESCE(Nombre_Centro, '');
      SUSPEND;

    END
    Detalle = 'JUEGO DE INVENTARIOS';
    FOR SELECT Codcuenta,
               Codtercero,
               Codcentro,
               Debito,
               Credito,
               Base
        FROM Reg_Juego
        WHERE Tipo = :Tipo_Doc
              AND Prefijo = :Prefijo
              AND Numero = :Numero
              AND Codcuenta <> ''
        INTO Cuenta,
             Tercero,
             Centro,
             Debito,
             Credito,
             Base

    DO
    BEGIN
      IF (Tercero IS NULL) THEN
        Tercero = COALESCE(Tercero, '');
      ELSE
        Tercero = V_Cod_Tercero;
      Centro = COALESCE(Centro, '');
      SELECT Nombre_Tercero
      FROM Fn_Nombre_Tercero(:Tercero)
      INTO Nombre_Tercero;
      Nombre_Tercero = COALESCE(Nombre_Tercero, '');

      --MOSTRAMOS NOMBRE CENTRO

      SELECT Nombre_Centro
      FROM Fn_Nombre_Centro(:Centro)
      INTO Nombre_Centro;
      Nombre_Centro = COALESCE(Nombre_Centro, '');

      SUSPEND;
    END
  END
END^

SET TERM ; ^


-----------------------

INSERT INTO INFORMES (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD, DESTINO_FTP, CODREPORTE)
              VALUES ('RD1299_001', 'INTERFACE DE GESTION A HOJA DE CALCULO CON TERCERO DE COMPROBANTE A CUENTA 13', 'Genera la interface a contable del documento filtrado si el documento tiene tercero en movimiento, en la cuenta 13 pone el tercero del encabezado en ese caso del comprobante', 'SELECT *
FROM Pz_G_Contable2(:Fecha_Desde, :Fecha_Hasta, :Tipo)', 'N', 'GESTION', 'N', 'PERSONALIZADOS', 'N', 'LIBRE', 5, 'N', 1, NULL, NULL);


COMMIT WORK;