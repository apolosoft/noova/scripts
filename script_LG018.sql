UPDATE OR INSERT INTO REPORTES (CODIGO, NOMBRE, NOTA, ARCHIVO, ADJUNTO, NATIVO, MODULO, PARAMETROS, PUBLICAR, CODGRUPO, VALIDA_TRANSITO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD)
                        VALUES ('CTPLG018', 'PORTAFOLIO DE REFERENCIAS POR UNA LISTA DE PRECIOS , FOTO, DOBLE COLUMNA', 'Listado de productos con atributo Portafolio, foto, codigo y lista de precios', 'LG018_001.FR3', NULL, 'S', 'PARAMETROS', 'LISTA_PRECIOS,CODLINEA', 'N', 'INVENTARIO', 'N', 'N', 'LIBRE', 5, 'N', 10)
                      MATCHING (CODIGO);


COMMIT WORK;

UPDATE OR INSERT INTO INFORMES (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD, DESTINO_FTP, CODREPORTE)
                        VALUES ('INVROD', 'INVENTARIO RODASERV', NULL, 'SELECT tipo,
       prefijo,
       numero,
       fecha_ultima_compra,
       referencia,
       nombre,
       entradas,
       salidas,
       saldo,
       ponderado,
       valor,
       linea,
       nombre_linea
FROM pz_inv_ultima_compra(:fecha_corte)', 'N', 'GESTION', 'N', 'SALDOS DE INVENTARIO', 'N', 'LIBRE', 5, 'N', 10, NULL, NULL)
                      MATCHING (CODIGO);


COMMIT WORK;

UPDATE OR INSERT INTO REPORTES (CODIGO, NOMBRE, NOTA, ARCHIVO, ADJUNTO, NATIVO, MODULO, PARAMETROS, PUBLICAR, CODGRUPO, VALIDA_TRANSITO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD)
                        VALUES ('STICKFACT', 'STICKERS FACTURAS', 'Imprime stickers de referencias por factura de compra, teniendo en cuenta la entrada', 'LG018_002.FR3', NULL, 'N', 'GESTION', 'TIPO,PREFIJO,NUMERO', 'N', 'PERSONALIZADOS', 'N', 'N', 'LIBRE', 5, 'N', 1)
                      MATCHING (CODIGO);


COMMIT WORK;