UPDATE OR INSERT INTO REPORTES (CODIGO, NOMBRE, NOTA, ARCHIVO, ADJUNTO, NATIVO, MODULO, PARAMETROS, PUBLICAR, CODGRUPO, VALIDA_TRANSITO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD)
                        VALUES ('RE1080_007', 'INFORME AUXILIAR CUENTAS  POR OBRAS  POR MUNICIPIO', NULL, 'RE1080_007.FR3', NULL, 'N', 'CONTABLE', 'FECHA_DESDE,FECHA_HASTA,CUENTA_DESDE,CUENTA_HASTA,SEDE', 'N', 'PERSONALIZADOS', 'N', 'N', 'LIBRE', 5, 'N', 1)
                      MATCHING (CODIGO);


COMMIT WORK;

/*NUEVO ESTADO DE CUENTAS POR PAGAR - ORDEN FECHA*/
UPDATE OR INSERT INTO REPORTES (CODIGO, NOMBRE, NOTA, ARCHIVO, ADJUNTO, NATIVO, MODULO, PARAMETROS, PUBLICAR, CODGRUPO, VALIDA_TRANSITO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD)
                        VALUES ('CXPG010000', 'ESTADO DE CUENTAS POR PAGAR - ORDEN FECHA', 'Muestra estado de cuentas por cliente, discriminando facturas y abonos, con sus correspondientes fechas, saldos por factura y saldos por cliente. No incluye facturas canceladas.', 'CXP001_F.FR3', NULL, 'S', 'GESTION', 'ACTIVIDAD,ZONA,TERCERO', 'N', 'SALDOS CARTERA POR PAGAR', 'N', 'N', 'LIBRE', 5, 'N', 1)
                      MATCHING (CODIGO);


COMMIT WORK;

UPDATE OR INSERT INTO REPORTES (CODIGO, NOMBRE, NOTA, ARCHIVO, ADJUNTO, NATIVO, MODULO, PARAMETROS, PUBLICAR, CODGRUPO, VALIDA_TRANSITO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD)
                        VALUES ('CXPG010050', 'ESTADO DE CUENTAS POR PAGAR HISTORICO - ORDEN FECHA', NULL, 'CXP020_F.FR3', NULL, 'S', 'GESTION', 'FECHA_CORTE,ACTIVIDAD,ZONA,TERCERO', 'N', 'SALDOS CARTERA POR PAGAR', 'N', 'N', 'LIBRE', 5, 'N', 1)
                      MATCHING (CODIGO);


COMMIT WORK;

/******************************************************************************/
/***                           Stored procedures                            ***/
/******************************************************************************/



SET TERM ^ ;

CREATE OR ALTER PROCEDURE PZ_CXC001_F (
    SELZONA CHAR(5),
    SELACTIVIDAD CHAR(5),
    SELTERCERO CHAR(15))
RETURNS (
    TERCERO CHAR(15),
    NOMBRE VARCHAR(163),
    TIPO CHAR(5),
    PREFIJO CHAR(5),
    NUMERO INTEGER,
    TIPOREF CHAR(5),
    PREFIJOREF CHAR(5),
    NUMEROREF INTEGER,
    FECHA DATE,
    VENCE DATE,
    DIRECCION CHAR(80),
    TELEFONO CHAR(50),
    GRUPO CHAR(20),
    RAZON_COMERCIAL CHAR(50),
    DEBITOS NUMERIC(17,4),
    CREDITOS NUMERIC(17,4),
    SALDO NUMERIC(17,4),
    APLICA CHAR(1))
AS
BEGIN
  SUSPEND;
END^





CREATE OR ALTER PROCEDURE PZ_CXC020_F (
    FFIN DATE,
    SELZONA CHAR(5),
    SELACTIVIDAD CHAR(5),
    SELTERCERO CHAR(15))
RETURNS (
    TERCERO CHAR(15),
    NOMBRE VARCHAR(163),
    TIPO CHAR(5),
    PREFIJO CHAR(5),
    NUMERO INTEGER,
    TIPOREF CHAR(5),
    PREFIJOREF CHAR(5),
    NUMEROREF INTEGER,
    FECHA DATE,
    VENCE DATE,
    CIUDAD CHAR(80),
    DIRECCION CHAR(80),
    TELEFONO CHAR(50),
    GRUPO CHAR(20),
    RAZON_COMERCIAL CHAR(50),
    DEBITOS NUMERIC(17,4),
    CREDITOS NUMERIC(17,4),
    SALDO NUMERIC(17,4),
    APLICA CHAR(1))
AS
BEGIN
  SUSPEND;
END^





CREATE OR ALTER PROCEDURE PZ_CXP001_F (
    SELZONA CHAR(5),
    SELACTIVIDAD CHAR(5),
    SELTERCERO CHAR(15))
RETURNS (
    TERCERO CHAR(15),
    NOMBRE VARCHAR(163),
    TIPO CHAR(5),
    PREFIJO CHAR(5),
    NUMERO INTEGER,
    TIPOREF CHAR(5),
    PREFIJOREF CHAR(5),
    NUMEROREF INTEGER,
    FECHA DATE,
    VENCE DATE,
    DIRECCION CHAR(80),
    TELEFONO CHAR(50),
    GRUPO CHAR(20),
    RAZON_COMERCIAL CHAR(50),
    DEBITOS NUMERIC(17,4),
    CREDITOS NUMERIC(17,4),
    SALDO NUMERIC(17,4),
    APLICA CHAR(1))
AS
BEGIN
  SUSPEND;
END^





CREATE OR ALTER PROCEDURE PZ_CXP020_F (
    FFIN DATE,
    SELZONA CHAR(5),
    SELACTIVIDAD CHAR(5),
    SELTERCERO CHAR(15))
RETURNS (
    TERCERO CHAR(15),
    NOMBRE VARCHAR(163),
    TIPO CHAR(5),
    PREFIJO CHAR(5),
    NUMERO INTEGER,
    TIPOREF CHAR(5),
    PREFIJOREF CHAR(5),
    NUMEROREF INTEGER,
    FECHA DATE,
    VENCE DATE,
    CIUDAD CHAR(80),
    DIRECCION CHAR(80),
    TELEFONO CHAR(50),
    GRUPO CHAR(20),
    RAZON_COMERCIAL CHAR(50),
    DEBITOS NUMERIC(17,4),
    CREDITOS NUMERIC(17,4),
    SALDO NUMERIC(17,4),
    APLICA CHAR(1))
AS
BEGIN
  SUSPEND;
END^






SET TERM ; ^



/******************************************************************************/
/***                           Stored procedures                            ***/
/******************************************************************************/



SET TERM ^ ;

CREATE OR ALTER PROCEDURE PZ_CXC001_F (
    SELZONA CHAR(5),
    SELACTIVIDAD CHAR(5),
    SELTERCERO CHAR(15))
RETURNS (
    TERCERO CHAR(15),
    NOMBRE VARCHAR(163),
    TIPO CHAR(5),
    PREFIJO CHAR(5),
    NUMERO INTEGER,
    TIPOREF CHAR(5),
    PREFIJOREF CHAR(5),
    NUMEROREF INTEGER,
    FECHA DATE,
    VENCE DATE,
    DIRECCION CHAR(80),
    TELEFONO CHAR(50),
    GRUPO CHAR(20),
    RAZON_COMERCIAL CHAR(50),
    DEBITOS NUMERIC(17,4),
    CREDITOS NUMERIC(17,4),
    SALDO NUMERIC(17,4),
    APLICA CHAR(1))
AS
BEGIN
  FOR SELECT R.Tercero,
             TRIM(T.Nombre),
             TRIM(R.Tipo),
             TRIM(R.Prefijo),
             CAST(R.Numero AS INT),
             TRIM(R.Tiporef),
             TRIM(R.Prefijoref),
             CAST(R.Numeroref AS INT),
             R.Fecha,
             R.Vence,
             T.Direccion,
             T.Telefono,
             D.Grupo,
             T.Razon_Comercial,
             D.M_Aplica,
             SUM(R.Debito),
             SUM(R.Credito)
      FROM Reg_Cartera R
      JOIN Documentos D ON (R.Tipo = D.Codigo)
      JOIN Terceros T ON (R.Tercero = T.Codigo)
      WHERE D.Grupo = 'VENTA' AND
            TRIM(T.Codzona) LIKE TRIM(:Selzona) AND
            TRIM(T.Codactividad) LIKE TRIM(:Selactividad) AND
            TRIM(R.Tercero) LIKE TRIM(:Seltercero) AND
            R.Debito > 0 AND
            D.M_Aplica = 'N'
      GROUP BY 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15
      ORDER BY 2, 9, 6, 7, 8, 10 DESC
      INTO Tercero, Nombre, Tipo, Prefijo, Numero, Tiporef, Prefijoref,
           Numeroref, Fecha, Vence, Direccion, Telefono, Grupo, Razon_Comercial,
           Aplica, Debitos, Creditos
  DO
  BEGIN
    SELECT SUM(Debito - Credito)
    FROM Reg_Cartera R
    WHERE TRIM(R.Tercero) = TRIM(:Tercero) AND
          Tiporef = :Tipo AND
          Numeroref = :Numero AND
          Prefijoref = :Prefijo
    INTO Saldo;
    IF (Saldo < 1 AND
        Saldo > -1) THEN
      Saldo = 0;

    IF (Saldo > 0) THEN
    BEGIN
      SUSPEND;
      Saldo = 0;

      FOR SELECT TRIM(R.Tipo),
                 TRIM(R.Prefijo),
                 CAST(R.Numero AS INT),
                 R.Fecha,
                 TRIM(R.Tiporef),
                 TRIM(R.Prefijoref),
                 TRIM(R.Numeroref),
                 D.M_Aplica,
                 SUM(R.Debito),
                 SUM(R.Credito)
          FROM Reg_Cartera R
          JOIN Documentos D ON (R.Tipo = D.Codigo)
          WHERE (TRIM(R.Tiporef) = :Tipo AND
                TRIM(R.Prefijoref) = :Prefijo AND
                TRIM(R.Numeroref) = :Numero) AND
                (D.M_Aplica) = 'S'
          GROUP BY 1, 2, 3, 4, 5, 6, 7, 8
          ORDER BY 4, 1, 2, 3
          INTO Tipo, Prefijo, Numero, Fecha, Tiporef, Prefijoref, Numeroref,
               Aplica, Debitos, Creditos
      DO
      BEGIN
        SUSPEND;
      END
    END
  END
END^


CREATE OR ALTER PROCEDURE PZ_CXC020_F (
    FFIN DATE,
    SELZONA CHAR(5),
    SELACTIVIDAD CHAR(5),
    SELTERCERO CHAR(15))
RETURNS (
    TERCERO CHAR(15),
    NOMBRE VARCHAR(163),
    TIPO CHAR(5),
    PREFIJO CHAR(5),
    NUMERO INTEGER,
    TIPOREF CHAR(5),
    PREFIJOREF CHAR(5),
    NUMEROREF INTEGER,
    FECHA DATE,
    VENCE DATE,
    CIUDAD CHAR(80),
    DIRECCION CHAR(80),
    TELEFONO CHAR(50),
    GRUPO CHAR(20),
    RAZON_COMERCIAL CHAR(50),
    DEBITOS NUMERIC(17,4),
    CREDITOS NUMERIC(17,4),
    SALDO NUMERIC(17,4),
    APLICA CHAR(1))
AS
DECLARE VARIABLE V_Codmunicipio CHAR(5);
BEGIN
  FOR SELECT R.Tercero,
             TRIM(T.Nombre),
             TRIM(R.Tipo),
             TRIM(R.Prefijo),
             CAST(R.Numero AS INT),
             TRIM(R.Tiporef),
             TRIM(R.Prefijoref),
             CAST(R.Numeroref AS INT),
             R.Fecha,
             R.Vence,
             T.Codmunicipio,
             TRIM(T.Direccion),
             TRIM(T.Telefono),
             D.Grupo,
             T.Razon_Comercial,
             D.M_Aplica,
             SUM(R.Debito),
             SUM(R.Credito)
      FROM Reg_Cartera R
      JOIN Documentos D ON (R.Tipo = D.Codigo)
      JOIN Terceros T ON (R.Tercero = T.Codigo)
      WHERE (D.Grupo = 'VENTA' AND
            D.Tesoreria <> 'CONTADO') AND
            TRIM(T.Codzona) LIKE TRIM(:Selzona) AND
            TRIM(T.Codactividad) LIKE TRIM(:Selactividad) AND
            TRIM(R.Tercero) LIKE TRIM(:Seltercero) AND
            R.Fecha <= :Ffin AND
            D.M_Aplica = 'N'
      GROUP BY 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16
      ORDER BY 2, 9, 6, 7, 8, 10 DESC
      INTO Tercero, Nombre, Tipo, Prefijo, Numero, Tiporef, Prefijoref,
           Numeroref, Fecha, Vence, V_Codmunicipio, Direccion, Telefono, Grupo,
           Razon_Comercial, Aplica, Debitos, Creditos
  DO
  BEGIN
    SELECT Nombre
    FROM Municipios
    WHERE Codigo = :V_Codmunicipio
    INTO Ciudad;

    SELECT SUM(Debito - Credito)
    FROM Reg_Cartera R
    WHERE TRIM(R.Tercero) = TRIM(:Tercero) AND
          Tiporef = :Tipo AND
          Numeroref = :Numero AND
          Prefijoref = :Prefijo
    INTO Saldo;

    IF (Saldo < 1 AND
        Saldo > -1) THEN
      Saldo = 0;

    SUSPEND;

    Saldo = 0;

    FOR SELECT TRIM(R.Tipo),
               TRIM(R.Prefijo),
               CAST(R.Numero AS INT),
               R.Fecha,
               TRIM(R.Tiporef),
               TRIM(R.Prefijoref),
               TRIM(R.Numeroref),
               D.M_Aplica,
               SUM(R.Debito),
               SUM(R.Credito)
        FROM Reg_Cartera R
        INNER JOIN Documentos D ON (R.Tipo = D.Codigo)
        WHERE R.Tiporef = :Tipo AND
              R.Prefijoref = :Prefijo AND
              R.Numeroref = :Numero AND
              D.M_Aplica = 'S'
        GROUP BY 1, 2, 3, 4, 5, 6, 7, 8
        ORDER BY 4, 1, 2, 3
        INTO Tipo, Prefijo, Numero, Fecha, Tiporef, Prefijoref, Numeroref,
             Aplica, Debitos, Creditos
    DO
    BEGIN
      SUSPEND;
    END
  END
END^


CREATE OR ALTER PROCEDURE PZ_CXP001_F (
    SELZONA CHAR(5),
    SELACTIVIDAD CHAR(5),
    SELTERCERO CHAR(15))
RETURNS (
    TERCERO CHAR(15),
    NOMBRE VARCHAR(163),
    TIPO CHAR(5),
    PREFIJO CHAR(5),
    NUMERO INTEGER,
    TIPOREF CHAR(5),
    PREFIJOREF CHAR(5),
    NUMEROREF INTEGER,
    FECHA DATE,
    VENCE DATE,
    DIRECCION CHAR(80),
    TELEFONO CHAR(50),
    GRUPO CHAR(20),
    RAZON_COMERCIAL CHAR(50),
    DEBITOS NUMERIC(17,4),
    CREDITOS NUMERIC(17,4),
    SALDO NUMERIC(17,4),
    APLICA CHAR(1))
AS
BEGIN
  FOR SELECT R.Tercero,
             TRIM(T.Nombre),
             TRIM(R.Tipo),
             TRIM(R.Prefijo),
             CAST(R.Numero AS INT),
             TRIM(R.Tiporef),
             TRIM(R.Prefijoref),
             CAST(R.Numeroref AS INT),
             R.Fecha,
             R.Vence,
             T.Direccion,
             T.Telefono,
             D.Grupo,
             T.Razon_Comercial,
             D.M_Aplica,
             SUM(R.Debito),
             SUM(R.Credito)
      FROM Reg_Cartera R
      JOIN Documentos D ON (R.Tipo = D.Codigo)
      JOIN Terceros T ON (R.Tercero = T.Codigo)
      WHERE D.Grupo = 'COMPRA' AND
            TRIM(T.Codzona) LIKE TRIM(:Selzona) AND
            TRIM(T.Codactividad) LIKE TRIM(:Selactividad) AND
            TRIM(R.Tercero) LIKE TRIM(:Seltercero) AND
            R.Credito > 0 AND
            D.M_Aplica = 'N'
      GROUP BY 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15
      ORDER BY 2, 9, 6, 7, 8, 10 DESC
      INTO Tercero, Nombre, Tipo, Prefijo, Numero, Tiporef, Prefijoref,
           Numeroref, Fecha, Vence, Direccion, Telefono, Grupo, Razon_Comercial,
           Aplica, Debitos, Creditos
  DO
  BEGIN
    SELECT SUM(Credito - Debito)
    FROM Reg_Cartera R
    WHERE TRIM(R.Tercero) = TRIM(:Tercero) AND
          Tiporef = :Tipo AND
          Numeroref = :Numero AND
          Prefijoref = :Prefijo
    INTO Saldo;
    IF (Saldo < 1 AND
        Saldo > -1) THEN
      Saldo = 0;

    IF (Saldo > 0) THEN
    BEGIN

      SUSPEND;
      Saldo = 0;

      FOR SELECT TRIM(R.Tipo),
                 TRIM(R.Prefijo),
                 CAST(R.Numero AS INT),
                 R.Fecha,
                 TRIM(R.Tiporef),
                 TRIM(R.Prefijoref),
                 TRIM(R.Numeroref),
                 D.M_Aplica,
                 SUM(R.Debito),
                 SUM(R.Credito)
          FROM Reg_Cartera R
          JOIN Documentos D ON (R.Tipo = D.Codigo)
          WHERE (TRIM(R.Tiporef) = :Tipo AND
                TRIM(R.Prefijoref) = :Prefijo AND
                TRIM(R.Numeroref) = :Numero) AND
                (D.M_Aplica) = 'S'
          GROUP BY 1, 2, 3, 4, 5, 6, 7, 8
          ORDER BY 4, 1, 2, 3
          INTO Tipo, Prefijo, Numero, Fecha, Tiporef, Prefijoref, Numeroref,
               Aplica, Debitos, Creditos
      DO
      BEGIN
        SUSPEND;
      END
    END
  END
END^


CREATE OR ALTER PROCEDURE PZ_CXP020_F (
    FFIN DATE,
    SELZONA CHAR(5),
    SELACTIVIDAD CHAR(5),
    SELTERCERO CHAR(15))
RETURNS (
    TERCERO CHAR(15),
    NOMBRE VARCHAR(163),
    TIPO CHAR(5),
    PREFIJO CHAR(5),
    NUMERO INTEGER,
    TIPOREF CHAR(5),
    PREFIJOREF CHAR(5),
    NUMEROREF INTEGER,
    FECHA DATE,
    VENCE DATE,
    CIUDAD CHAR(80),
    DIRECCION CHAR(80),
    TELEFONO CHAR(50),
    GRUPO CHAR(20),
    RAZON_COMERCIAL CHAR(50),
    DEBITOS NUMERIC(17,4),
    CREDITOS NUMERIC(17,4),
    SALDO NUMERIC(17,4),
    APLICA CHAR(1))
AS
DECLARE VARIABLE V_Codmunicipio CHAR(5);
BEGIN
  FOR SELECT R.Tercero,
             TRIM(T.Nombre),
             TRIM(R.Tipo),
             TRIM(R.Prefijo),
             CAST(R.Numero AS INT),
             TRIM(R.Tiporef),
             TRIM(R.Prefijoref),
             CAST(R.Numeroref AS INT),
             R.Fecha,
             R.Vence,
             T.Codmunicipio,
             TRIM(T.Direccion),
             TRIM(T.Telefono),
             D.Grupo,
             T.Razon_Comercial,
             D.M_Aplica,
             SUM(R.Debito),
             SUM(R.Credito)
      FROM Reg_Cartera R
      JOIN Documentos D ON (R.Tipo = D.Codigo)
      JOIN Terceros T ON (R.Tercero = T.Codigo)
      WHERE D.Grupo = 'COMPRA' AND
            TRIM(T.Codzona) LIKE TRIM(:Selzona) AND
            TRIM(T.Codactividad) LIKE TRIM(:Selactividad) AND
            TRIM(R.Tercero) LIKE TRIM(:Seltercero) AND
            R.Fecha <= :Ffin AND
            D.M_Aplica = 'N'
      GROUP BY 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16
      ORDER BY 2, 9, 6, 7, 8, 10 DESC
      INTO Tercero, Nombre, Tipo, Prefijo, Numero, Tiporef, Prefijoref,
           Numeroref, Fecha, Vence, V_Codmunicipio, Direccion, Telefono, Grupo,
           Razon_Comercial, Aplica, Debitos, Creditos
  DO
  BEGIN
    SELECT Nombre
    FROM Municipios
    WHERE Codigo = :V_Codmunicipio
    INTO Ciudad;

    SELECT SUM(Credito - Debito)
    FROM Reg_Cartera R
    WHERE TRIM(R.Tercero) = TRIM(:Tercero) AND
          Tiporef = :Tipo AND
          Numeroref = :Numero AND
          Prefijoref = :Prefijo
    INTO Saldo;

    IF (Saldo < 1 AND
        Saldo > -1) THEN
      Saldo = 0;

    SUSPEND;

    Saldo = 0;

    FOR SELECT TRIM(R.Tipo),
               TRIM(R.Prefijo),
               CAST(R.Numero AS INT),
               R.Fecha,
               TRIM(R.Tiporef),
               TRIM(R.Prefijoref),
               TRIM(R.Numeroref),
               D.M_Aplica,
               SUM(R.Debito),
               SUM(R.Credito)
        FROM Reg_Cartera R
        INNER JOIN Documentos D ON (R.Tipo = D.Codigo)
        WHERE R.Tiporef = :Tipo AND
              R.Prefijoref = :Prefijo AND
              R.Numeroref = :Numero AND
              D.M_Aplica = 'S'
        GROUP BY 1, 2, 3, 4, 5, 6, 7, 8
        ORDER BY 4, 1, 2, 3
        INTO Tipo, Prefijo, Numero, Fecha, Tiporef, Prefijoref, Numeroref,
             Aplica, Debitos, Creditos
    DO
    BEGIN
      SUSPEND;
    END
  END
END^



SET TERM ; ^
COMMIT WORK;