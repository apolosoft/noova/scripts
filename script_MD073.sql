SET TERM ^ ;

CREATE OR ALTER PROCEDURE Pz_Volumen_Total (
    Fecha_Desde DATE,
    Fecha_Hasta DATE)
RETURNS (
    Desde              DATE,
    Hasta              DATE,
    Tipo               VARCHAR(5),
    Prefijo            VARCHAR(5),
    Numero             VARCHAR(10),
    Tercero            VARCHAR(15),
    Nombre_Tercero     VARCHAR(163),
    Linea              VARCHAR(5),
    Nombre_Linea       VARCHAR(80),
    Bodega             VARCHAR(5),
    Nombre_Bodega      VARCHAR(80),
    Referencia         VARCHAR(20),
    Nombre_Referencia  VARCHAR(80),
    Cantidad           DOUBLE PRECISION,
    Volumen_Referencia DOUBLE PRECISION,
    Bruto_Dev          DOUBLE PRECISION,
    Ultimo_Costo       DOUBLE PRECISION,
    Ultimo_Ponderado   DOUBLE PRECISION,
    Costo_Total        DOUBLE PRECISION,
    Volumen_Total      DOUBLE PRECISION)
AS
DECLARE VARIABLE V_Volumen DOUBLE PRECISION;
BEGIN
  Desde = Fecha_Desde;
  Hasta = Fecha_Hasta;

  FOR SELECT Tipo,
             Prefijo,
             Numero,
             Tercero,
             Linea,
             Referencia,
             Bodega,
             SUM(Cantidad),
             SUM(Bruto + Devolucion)
      FROM Fx_Dinamico_Saldos(:Fecha_Desde, :Fecha_Hasta, 'VENTA', '%', '%', '%', '%', '%', '%', '%', '%', '%', '%', '%', 'S', 'N', 'N', 'S', 'S', 'N', 'N', 'S', 'N', 'N', 'N')
      GROUP BY 1, 2, 3, 4, 5, 6, 7
      INTO Tipo,
           Prefijo,
           Numero,
           Tercero,
           Linea,
           Referencia,
           Bodega,
           Cantidad,
           Bruto_Dev
  DO
  BEGIN
    --Buscar nombre_referencia
    SELECT Nombre,
           Volumen
    FROM Referencias
    WHERE Codigo = :Referencia
    INTO Nombre_Referencia,
         V_Volumen;

    --Buscar Nombre_Linea
    SELECT Nombre
    FROM Lineas
    WHERE Codigo = :Linea
    INTO Nombre_Linea;

    --Buscar Nombre Bodega
    SELECT Nombre
    FROM Bodegas
    WHERE Codigo = :Bodega
    INTO Nombre_Bodega;

    --Buscar Nombre_Tercero
    SELECT Nombre
    FROM Terceros
    WHERE Codigo = :Tercero
    INTO Nombre_Tercero;

    -- calculo para volumen multiplicado enc antidad

    Volumen_Total = (V_Volumen * Cantidad);
    --Buscar ultimo_costo y/o Ultimo_Ponderado
    SELECT Ueps,
           Ponderado
    FROM Fn_Ultimos_Valorizados(:Referencia, :Fecha_Hasta)
    INTO Ultimo_Costo,
         Ultimo_Ponderado;

    Costo_Total = (Ultimo_Ponderado * Cantidad);
    Volumen_Referencia = :V_Volumen;

    SUSPEND;
  END

END^

SET TERM ; ^

COMMIT WORK;

UPDATE OR INSERT INTO INFORMES (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, FORMATO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD, DESTINO_FTP, CODREPORTE)
                        VALUES ('MD073_001', 'VENTAS DETALLADAS POR FECHA DOCUMENTO TERCERO Y POR VOLUMEN', 'MUESTRA LAS VENTAS POR TERCERO POR DOCUMENTO Y EL VOLUMEN AGRUPADO POR TERCERO O POR DOCUMENTO EN SUS RESPECTIVAS FECHAS', 'SELECT Desde,
       Hasta,
       Tipo,
       Prefijo,
       Numero,
       Tercero,
       Nombre_Tercero,
       Linea,
       Nombre_Linea,
       Bodega,
       Nombre_Bodega,
       Referencia,
       Nombre_Referencia,
       Cantidad,
       Volumen_Referencia,
       Bruto_Dev,
       Ultimo_Costo,
       Ultimo_Ponderado,
       Costo_Total,
       Volumen_Total
FROM Pz_Volumen_Total(:Fecha_Desde, :Fecha_Hasta)', 'N', 'GESTION', :h00000000_00000000, 'N', 'PERSONALIZADOS', 'N', 'LIBRE', 5, 'N', 1, NULL, NULL)
                      MATCHING (CODIGO);


COMMIT WORK;