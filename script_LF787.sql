UPDATE OR INSERT INTO INFORMES (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD, DESTINO_FTP, CODREPORTE)
                        VALUES ('LF787_012', 'TRANSFERENCIA NOMINA CUENTA BANCO AGRARIO', 'TRANSFERENCIA NOMINA CUENTA BANCO AGRARIO', 'SELECT LPAD(Banco, 4, ''00'') || LPAD(Beneficiario, 15, '' '') || RPAD(Tipo_Identificacion, 1, '''') || LPAD(COALESCE(Cuenta, '' ''), 17, '' '') || Tipo_Cuenta || RPAD(Nombre_Beneficiario, 30, '' '') || LPAD(ROUND(Valor), 12, 0) || ''.00'' || RPAD(Referencia, 42) Plano
FROM (SELECT TRIM(P.Codpersonal) Beneficiario,
             TRIM(E.Nombre) Nombre_Beneficiario,
             TRIM(T.Codidentidad) Tipo_Identificacion,
             TRIM(E.Codentidad) Banco,
             REPLACE(TRIM(E.Banco), ''-'', '''') Cuenta,
             IIF(E.Codtipocuenta = ''A'', ''4'', ''3'') AS Tipo_Cuenta,
             SUM(ROUND(P.Adicion)) Valor,
             TRIM(N.Nombre) Referencia
      FROM Planillas P
      JOIN Personal E ON (P.Codpersonal = E.Codigo)
      JOIN Terceros T ON (T.Codigo = E.Codigo)
      JOIN Nominas N ON (P.Codnomina = N.Codigo)
      JOIN Rubros R ON (P.Codrubro = R.Codigo)
      WHERE P.Codnomina = :Nomina
            AND R.Codconjunto = ''T_PAGAR''
            AND COALESCE(TRIM(E.Banco), '''') <> ''''
            AND E.Codentidad = ''40''
      GROUP BY 1, 2, 3, 4, 5, 6, 8)', 'N', 'NOMINA', 'N', 'PAGOS', 'N', 'LIBRE', 5, 'N', 1, NULL, NULL)
                      MATCHING (CODIGO);

COMMIT WORK;

/* Informe personalizado con base a CONTABLE3E, se realiza cambio en la nota del juego de inventario por nombre de la cuenta. */
UPDATE OR INSERT INTO INFORMES (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, FORMATO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD, DESTINO_FTP, CODREPORTE)
                        VALUES ('LF787_013', 'INTERFACE DE ERP - EXPORTA A TXT PARA SUBIR POR RUTINA PLANO - RANGO FECHA', 'Permite en un rango de fechas mes, exportar todo el movimiento contable a archivo plano TXT, para luego subirlo automáticamente a cualquier empresa o contabilidad mediante la rutina Plano a Contable', 'SELECT C.Tipo,
       C.Prefijo,
       C.Numero,
       '''' AS Secuencia,
       C1.Fecha,
       C.Codcuenta AS Cuenta,
       C.Codtercero AS Tercero,
       C.Codcentro AS Centro,
       C.Nota AS Detalle,
       C.Debito,
       C.Credito,
       C.Base,
       ''N'' AS Aplica,
       C.Tiporef AS Tipoanexo,
       C.Prefijoref AS Prefijoanexo,
       C.Numeroref AS Numeroanexo,
       ''SUPERVISOR'' AS Usuario,
       '''' AS Signo,
       '''' AS Cuentacobrar,
       '''' AS Cuentapagar,
       (SELECT Nombre_Tercero
        FROM Fn_Nombre_Tercero(C.Codtercero)) AS "NOMBRE TERCERO",
       (SELECT Nombre_Centro
        FROM Fn_Nombre_Centro(C.Codcentro)) AS "NOMBRE CENTRO",
       CURRENT_DATE AS Interfaceno
FROM Comovi C
INNER JOIN Cocomp C1 ON ((C.Tipo = C1.Tipo) AND (C.Prefijo = C1.Prefijo) AND (C.Numero = C1.Numero))
WHERE TRIM(C.Codcuenta) <> ''''
      AND ((C1.Fecha >= :Fecha_Desde)
      AND (C1.Fecha <= :Fecha_Hasta))

UNION ALL

SELECT C.Tipo,
       C.Prefijo,
       C.Numero,
       '''' AS Secuencia,
       C.Fecha,
       C.Codcuenta AS Cuenta,
       C.Codtercero AS Tercero,
       C.Codcentro AS Centro,
       C.Detalle,
       C.Debito,
       C.Credito,
       C.Base,
       ''N'' AS Aplica,
       '''' AS Tipoanexo,
       '''' AS Prefijoanexo,
       '''' AS Numeroanexo,
       ''SUPERVISOR'' AS Usuario,
       '''' AS Signo,
       '''' AS Cuentacobrar,
       '''' AS Cuentapagar,
       (SELECT Nombre_Tercero
        FROM Fn_Nombre_Tercero(C.Codtercero)) AS "NOMBRE TERCERO",
       (SELECT Nombre_Centro
        FROM Fn_Nombre_Centro(C.Codcentro)) AS "NOMBRE CENTRO",
       CURRENT_DATE AS Interfaceno
FROM Reg_Contable C
INNER JOIN Comprobantes C1 ON ((C.Tipo = C1.Tipo) AND (C.Prefijo = C1.Prefijo) AND (C.Numero = C1.Numero))
WHERE TRIM(C.Codcuenta) <> ''''
      AND ((C.Fecha >= :Fecha_Desde)
      AND (C.Fecha <= :Fecha_Hasta))

UNION ALL

SELECT C.Tipo,
       C.Prefijo,
       C.Numero,
       '''' AS Secuencia,
       C.Fecha,
       C.Codcuenta AS Cuenta,
       C.Codtercero AS Tercero,
       C.Codcentro AS Centro,
       (SELECT TRIM(Nombre_Cuenta)
        FROM Fn_Nombre_Cuenta(C.Codcuenta)) AS Detalle,
       C.Debito,
       C.Credito,
       C.Base,
       ''N'' AS Aplica,
       '''' AS Tipoanexo,
       '''' AS Prefijoanexo,
       '''' AS Numeroanexo,
       ''SUPERVISOR'' AS Usuario,
       '''' AS Signo,
       '''' AS Cuentacobrar,
       '''' AS Cuentapagar,
       (SELECT Nombre_Tercero
        FROM Fn_Nombre_Tercero(C.Codtercero)) AS "NOMBRE TERCERO",
       (SELECT Nombre_Centro
        FROM Fn_Nombre_Centro(C.Codcentro)) AS "NOMBRE CENTRO",
       CURRENT_DATE AS Interfaceno
FROM Reg_Juego C
INNER JOIN Comprobantes C1 ON ((C.Tipo = C1.Tipo) AND (C.Prefijo = C1.Prefijo) AND (C.Numero = C1.Numero))
WHERE TRIM(C.Codcuenta) <> ''''
      AND ((C1.Fecha >= :Fecha_Desde)
      AND (C1.Fecha <= :Fecha_Hasta))

UNION ALL

SELECT C.Tipo,
       C.Prefijo,
       C.Codnomina,
       '''' AS Secuencia,
       C1.Fecha,
       C.Codcuenta AS Cuenta,
       C.Codtercero AS Tercero,
       C.Codcentro AS Centro,
       C.Detalle,
       C.Debito,
       C.Credito,
       C.Base,
       ''N'' AS Aplica,
       '''' AS Tipoanexo,
       '''' AS Prefijoanexo,
       '''' AS Numeroanexo,
       ''SUPERVISOR'' AS Usuario,
       '''' AS Signo,
       '''' AS Cuentacobrar,
       '''' AS Cuentapagar,
       (SELECT TRIM(Nombre_Tercero)
        FROM Fn_Nombre_Tercero(C.Codtercero)) AS "NOMBRE TERCERO",
       (SELECT Nombre_Centro
        FROM Fn_Nombre_Centro(C.Codcentro)) AS "NOMBRE CENTRO",
       CURRENT_DATE AS Interfaceno
FROM Reg_Nomina C
INNER JOIN Nominas C1 ON (C.Codnomina = C1.Codigo)
WHERE TRIM(C.Codcuenta) <> ''''
      AND ((C1.Fecha >= :Fecha_Desde)
      AND (C1.Fecha <= :Fecha_Hasta))
ORDER BY 1,2,3', 'N', 'GESTION', NULL, 'N', 'RUTINAS MIGRACIONES', 'N', 'LIBRE', 5, 'N', 1, NULL, NULL)
                      MATCHING (CODIGO);


COMMIT WORK;
