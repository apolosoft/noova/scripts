UPDATE OR INSERT INTO INFORMES (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, FORMATO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD, DESTINO_FTP, CODREPORTE)
                        VALUES ('MA508_001', 'STOCK MINIMO INVENTARIO', 'INFORME DE STOCK DE REFERENCIAS CON SALDO FINAL Y VALORIZADO A UNA FECHA DE CORTE', 'SELECT Cod_Linea AS Codigo_Linea,
       Nombre_Linea,
       Referencia,
       Nombre_Referencia,
       Stock_Minimo,
       Tiempo_Min AS Tiempo_Minimo,
       Tiempo_Maximo,
       Saldo,
       Saldo_Final,
       Valorizado
FROM Pz_Inventario_Stock_Min(:Fecha_Corte, :Linea)', 'N', 'GESTION', NULL, 'N', 'AUXILIAR DE INVENTARIOS', 'N', 'LIBRE', 5, 'N', 1, NULL, NULL)
                      MATCHING (CODIGO);


COMMIT WORK;

SET TERM ^ ;
CREATE OR ALTER PROCEDURE Pz_Ultima_Venta (
    Referencia CHAR(20),
    Hasta_ DATE)
RETURNS (
    Tipo CHAR(5),
    Prefijo CHAR(5),
    Numero CHAR(10),
    Fecha DATE,
    Bruto NUMERIC(17,4))
AS
BEGIN
  SUSPEND;
END^
SET TERM ; ^

UPDATE OR INSERT INTO INFORMES (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, FORMATO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD, DESTINO_FTP, CODREPORTE)
                        VALUES ('CMA508_001', 'ANALISIS COMPARATIVO PRECIOS EN COMPRAS Y VENTAS -  REFERENCIAS HOY - PONDERADO', 'Total entradas y total salidas por cada referencia, con promedio, valor maximo y minimo, ultimo ponderado, ultimo costo', 'SELECT Pl.Referencia,
       Pl.Nombre,
       Pl.Entradas,
       Pl.Salidas,
       Pl.Minimo,
       Pl.Maximo,
       Pl.Promedio,
       Pl.Max_Bruto,
       Pl.Min_Bruto,
       Pl.Max_Descuento,
       Pl.Ultimo_Costo,
       Pl.Ultimo_Ponderado,
       (SELECT Bruto
        FROM Pz_Ultima_Venta(Pl.Referencia, :_Hasta)) AS Ultima_Venta,
       Pl.Desde,
       Pl.Hasta
FROM Fx_Ponderados_Lista_Rango(:_Desde, :_Hasta) Pl', 'N', 'GESTION', NULL, 'N', 'PERSONALIZADOS', 'N', 'LIBRE', 5, 'N', 1, NULL, NULL)
                      MATCHING (CODIGO);


COMMIT WORK;

SET TERM ^ ;
CREATE OR ALTER PROCEDURE Pz_Ultima_Venta (
    Referencia CHAR(20),
    Hasta_ DATE)
RETURNS (
    Tipo CHAR(5),
    Prefijo CHAR(5),
    Numero CHAR(10),
    Fecha DATE,
    Bruto NUMERIC(17,4))
AS
BEGIN
  SELECT FIRST 1 C.Tipo,
                 C.Prefijo,
                 C.Numero,
                 C.Fecha,
                 T.Bruto
  FROM Tr_Inventario T
  JOIN Comprobantes C USING (Tipo, Prefijo, Numero)
  JOIN Documentos D ON (C.Tipo = D.Codigo)
  WHERE T.Codreferencia = :Referencia
        AND C.Fecha <= :Hasta_
        AND TRIM(D.Grupo) = 'VENTA'
        AND D.Es_Compra_Venta = 'S'
  ORDER BY C.Fecha DESC
  INTO Tipo,
       Prefijo,
       Numero,
       Fecha,
       Bruto;
  Tipo = COALESCE(:Tipo, '');
  Prefijo = COALESCE(:Prefijo, '');
  Numero = COALESCE(:Numero, '');
  Fecha = COALESCE(:Fecha, :Hasta_);
  Bruto = COALESCE(:Bruto, 0);

  SUSPEND;
END^
SET TERM ; ^

COMMIT WORK;

UPDATE OR INSERT INTO INFORMES (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, FORMATO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD, DESTINO_FTP, CODREPORTE)
                        VALUES ('MA508_003', 'INFORME CARTERA DOC PRIMARIOS Y SECUNDARIOS PER', 'MUESTRA RELACION DE DOCUMENTOS PRIMARIOS CON VALOR FACTURAY DOCUMENTOS SECUNDARIOS CON FECHA DE PAGO', 'SELECT *
       FROM Pz_Aux_Tes_Gan(:Fecha_Desde, :Fecha_Hasta)', 'N', 'GESTION', :h00000000_00000000, 'N', 'RELACION DE MOVIMIENTO', 'N', 'LIBRE', 5, 'N', 1, NULL, NULL)
                      MATCHING (CODIGO);


COMMIT WORK;