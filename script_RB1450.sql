SET TERM ^ ;

CREATE OR ALTER PROCEDURE Pz_Ct01_Primario_Detalle (
    Fecha_Desde DATE,
    Tipo        VARCHAR(15))
RETURNS (
    Tipo_Doc       VARCHAR(5),
    Prefijo_Doc    VARCHAR(5),
    Numero_Doc     VARCHAR(10),
    Fecha          DATE,
    Vence          DATE,
    Tercero        CHAR(15),
    Bodega         VARCHAR(5),
    Centro         VARCHAR(5),
    Categoria      VARCHAR(15),
    Lote           VARCHAR(20),
    Referencia     VARCHAR(20),
    Cantidad       DOUBLE PRECISION,
    Unitario       DOUBLE PRECISION,
    Descuento      DOUBLE PRECISION,
    Nota           VARCHAR(200),
    Nombre_Tercero VARCHAR(164))
AS
DECLARE VARIABLE V_Tipo    CHAR(5);
DECLARE VARIABLE V_Prefijo CHAR(5);
BEGIN

  FOR SELECT Tipo,
             Prefijo,
             Numero,
             Fecha,
             Vence,
             Codtercero
      FROM Comprobantes
      WHERE Fecha >= :Fecha_Desde
            AND Tipo = :Tipo
            AND Numero > '0'
      ORDER BY Numero
      INTO V_Tipo,
           V_Prefijo,
           Numero_Doc,
           Fecha,
           Vence,
           Tercero
  DO
  BEGIN
    --MOSTRAMOS NOMBRE TERCERO
    SELECT Nombre_Tercero
    FROM Fn_Nombre_Tercero(:Tercero)
    INTO Nombre_Tercero;
    Nombre_Tercero = COALESCE(Nombre_Tercero, '');

    Tipo_Doc = :V_Tipo;
    Prefijo_Doc = :V_Prefijo;

    FOR SELECT Codbodega,
               Codcentro,
               Codreferencia,
               Salida,
               Codcategoria,
               Codlote,
               Unitario,
               Descuento,
               Nota
        FROM Tr_Inventario
        WHERE Tipo = :V_Tipo
              AND Prefijo = :V_Prefijo
              AND Numero = :Numero_Doc
        INTO Bodega,
             Centro,
             Referencia,
             Cantidad,
             Categoria,
             Lote,
             Unitario,
             Descuento,
             Nota

    DO
    BEGIN
      Referencia = TRIM(Referencia);
      SUSPEND;
    END

  END

END^

SET TERM ; ^

----------------------------------

UPDATE OR INSERT INTO INFORMES (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD, DESTINO_FTP, CODREPORTE)
                        VALUES ('RB1033_001', 'HOJA CALCULO A GESTION', 'Informe para descargar las cotizaciones CT01 para pasarla a la base de datos NEON, ya viene convertida en tipo FV1 y prefijo NEON y dividida entre 3', 'SELECT * FROM Pz_Ct01_Primario_Detalle(:Fecha_Desde, :Tipo)', 'N', 'GESTION', 'N', 'PERSONALIZADOS', 'N', 'LIBRE', 5, 'N', 1, NULL, NULL)
                      MATCHING (CODIGO);


COMMIT WORK;