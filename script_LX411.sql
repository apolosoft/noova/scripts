/* Reporte basado en CXCG090103, se adiciona parametro vendedor */
UPDATE OR INSERT INTO REPORTES (CODIGO, NOMBRE, NOTA, ARCHIVO, ADJUNTO, NATIVO, MODULO, PARAMETROS, PUBLICAR, CODGRUPO, VALIDA_TRANSITO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD)
                        VALUES ('LX411_002', 'ESTADO DE CUENTAS POR COBRAR CON ANTICIPOS - A HOY - POR VENDEDOR', NULL, 'LX411_002.FR3', NULL, 'N', 'GESTION', 'ACTIVIDAD,ZONA,TERCERO,VENDEDOR', 'N', 'PERSONALIZADOS', 'N', 'N', 'LIBRE', 5, 'N', 1)
                      MATCHING (CODIGO);


COMMIT WORK;

/* Informe resumen de control de bodegas con vendedor */
UPDATE OR INSERT INTO INFORMES (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, FORMATO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD, DESTINO_FTP, CODREPORTE)
                        VALUES ('LX411_001', 'INFORME RESUMEN DE CONTROL DE BODEGAS CON VENDEDOR', '', 'SELECT TRIM(R.Tipo) Tipo,
       TRIM(R.Prefijo) Prefijo,
       TRIM(R.Numero) Numero,
       TRIM(R.Grupo) Grupo,
       TRIM(R.Codtercero) Tercero,
       TRIM(V.Codvendedor) Vedendor,
       TRIM(V.Nombre) Nombre_Vendedor,
       TRIM(R.Codbodega) Bodega,
       TRIM(R.Codreferencia) Referencia,
       TRIM(R.Codmedida) Medida,
       TRIM(R.Nombre_Tercero) Nombre_Tercero,
       TRIM(R.Nombre_Bodega) Nombre_Bodega,
       TRIM(R.Nombre_Referencia) Nombre_Referencia,
       TRIM(R.Fecha_Factura) Fecha_Factura,
       R.Facturado,
       R.Ajuste,
       R.Entregado,
       R.Pendiente
FROM Entregas_Resumen(:Fecha_Desde,:Fecha_Hasta) R
LEFT JOIN(SELECT C.Tipo,
                 C.Prefijo,
                 C.Numero,
                 C.Codvendedor,
                 P.Nombre
          FROM Comprobantes C
          LEFT JOIN Personal P ON (P.Codigo = C.Codvendedor)) V ON (V.Tipo = R.Tipo AND V.Prefijo = R.Prefijo AND V.Numero = R.Numero)', 'N', 'GESTION', NULL, 'N', 'PERSONALIZADOS', 'N', 'LIBRE', 5, 'N', 1, NULL, NULL)
                      MATCHING (CODIGO);


COMMIT WORK;
