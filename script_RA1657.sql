UPDATE OR INSERT INTO INFORMES (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD, DESTINO_FTP, CODREPORTE)
                        VALUES ('_INFORMEPO', 'INFORME POS CON FORMAS DE PAGO                                                  ', '                                                                                                                                                                                                        ', 'EXECUTE BLOCK (
    Desde DATE = :_Desde,
    Hasta DATE = :_Hasta,
    Documento VARCHAR(5) = :Documento,
    Tercero VARCHAR(15) = :Tercero,
    Referencia VARCHAR(20) = :Referencia)
RETURNS (
    Comprobante VARCHAR(80),
    Tipo VARCHAR(5),
    Prefijo VARCHAR(5),
    Numero BIGINT,
    Fecha DATE,
    Codtercero VARCHAR(15),
    Nombre_Tercero VARCHAR(163),
    Codreferencia VARCHAR(20),
    Nombre_Referencia VARCHAR(80),
    Linea VARCHAR(5),
    Nombre_Linea VARCHAR(80),
    Cantidad DOUBLE PRECISION,
    Bruto DOUBLE PRECISION,
    Unitario DOUBLE PRECISION,
    Descuento DOUBLE PRECISION,
    Total_Bruto DOUBLE PRECISION,
    Gravado DOUBLE PRECISION,
    No_Gravado DOUBLE PRECISION,
    Retencion DOUBLE PRECISION,
    Usuario VARCHAR(10),
    Maquina VARCHAR(20),
    Formapago VARCHAR(80))
AS

DECLARE VARIABLE Vsql VARCHAR(1000);

BEGIN

  Vsql = ''SELECT Tipo,
             Prefijo,
             CAST(Numero AS BIGINT) AS Numero,
             Fecha,
             Tercero,
             TRIM(Nombre_Tercero),
             Referencia,
             TRIM(Nombre_Referencia),
             Linea,
             TRIM(Nombre_Linea),
             Cantidad,
             Bruto,
             Unitario,
             Descuento,
             ((Unitario - Descuento) * Cantidad),
             Gravado,
             No_Gravado,
             Retencion,
             Usuario,
             Computador
      FROM Fx_Auxiliar_Pos('''''' || Desde || '''''', '''''' || Hasta || '''''')
      WHERE TRIM(Tipo) LIKE '''''' || Documento || ''''''
            AND TRIM(Tercero) LIKE '''''' || Tercero || ''''''
            AND TRIM(Referencia) LIKE '''''' || Referencia || ''''''  '';
  FOR EXECUTE STATEMENT Vsql
          INTO Tipo, Prefijo, Numero, Fecha, Codtercero, Nombre_Tercero, Codreferencia, Nombre_Referencia, Linea,
          Nombre_Linea, Cantidad, Bruto, Unitario, Descuento, Total_Bruto, Gravado, No_Gravado, Retencion, Usuario, Maquina

  DO
  BEGIN

    Vsql = ''SELECT FIRST 1 TRIM(Fp.Nombre)
    FROM Reg_Pagos Re
    JOIN Formaspago Fp ON (Fp.Codigo = Re.Codformaspago)
    WHERE Re.Tipo = '''''' || Tipo || ''''''
          AND Re.Prefijo = '''''' || Prefijo || ''''''
          AND Re.Numero = '''''' || Numero || '''''''';
    EXECUTE STATEMENT Vsql
        INTO Formapago;

    Vsql = ''SELECT TRIM(Nombre)
    FROM Documentos D
    WHERE D.Registro_Pago = ''''S''''
          AND D.Codigo = '''''' || Tipo || '''''''';
    EXECUTE STATEMENT Vsql
        INTO Comprobante;

    SUSPEND;

  END

END', 'N', 'GESTION   ', 'N', NULL, 'N', 'LIBRE               ', 5, 'N', 1, NULL, NULL)
                      MATCHING (CODIGO);

COMMIT WORK;