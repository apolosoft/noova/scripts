

SET TERM ^ ;

CREATE OR ALTER PROCEDURE PZ_DIARIO_VENTAS (
    FECHA_ DATE,
    TIPO_ VARCHAR(5),
    COMPU_ VARCHAR(20))
RETURNS (
    TIPO VARCHAR(5),
    PREFIJO VARCHAR(5),
    NUMERO VARCHAR(10),
    RENGLON INTEGER,
    REFERENCIA VARCHAR(20),
    CANTIDAD DOUBLE PRECISION,
    BRUTO DOUBLE PRECISION,
    DESCUENTO DOUBLE PRECISION,
    GRVADO DOUBLE PRECISION,
    NO_GRAVADO DOUBLE PRECISION,
    LINEA VARCHAR(5),
    USUARIO VARCHAR(10),
    COMPUTADOR VARCHAR(20),
    NOMBRE_LINEA VARCHAR(80),
    TARIFA DOUBLE PRECISION,
    FECHA DATE,
    MINIMO INTEGER,
    MAXIMO INTEGER)
AS
BEGIN
  SUSPEND;
END^





CREATE OR ALTER PROCEDURE PZ_DIARIO_VENTAS_MAQ (
    FECHA_ DATE,
    TIPO_ VARCHAR(5),
    COMPU_ VARCHAR(20))
RETURNS (
    TRANSACCION VARCHAR(20),
    COMPUTADOR VARCHAR(20),
    TRANSACCIONES INTEGER,
    VALOR DOUBLE PRECISION)
AS
BEGIN
  SUSPEND;
END^






SET TERM ; ^



/******************************************************************************/
/***                           Stored procedures                            ***/
/******************************************************************************/



SET TERM ^ ;

CREATE OR ALTER PROCEDURE PZ_DIARIO_VENTAS (
    FECHA_ DATE,
    TIPO_ VARCHAR(5),
    COMPU_ VARCHAR(20))
RETURNS (
    TIPO VARCHAR(5),
    PREFIJO VARCHAR(5),
    NUMERO VARCHAR(10),
    RENGLON INTEGER,
    REFERENCIA VARCHAR(20),
    CANTIDAD DOUBLE PRECISION,
    BRUTO DOUBLE PRECISION,
    DESCUENTO DOUBLE PRECISION,
    GRVADO DOUBLE PRECISION,
    NO_GRAVADO DOUBLE PRECISION,
    LINEA VARCHAR(5),
    USUARIO VARCHAR(10),
    COMPUTADOR VARCHAR(20),
    NOMBRE_LINEA VARCHAR(80),
    TARIFA DOUBLE PRECISION,
    FECHA DATE,
    MINIMO INTEGER,
    MAXIMO INTEGER)
AS
DECLARE VARIABLE V_Personal VARCHAR(15);
DECLARE VARIABLE V_Maquina  VARCHAR(20);
BEGIN
  FOR SELECT C.Codusuario,
             C.Computador,
             TRIM(C.Tipo),
             TRIM(C.Prefijo),
             TRIM(C.Numero),
             T.Renglon,
             T.Codreferencia,
             C.Fecha,
             T.Salida,
             T.Bruto * T.Salida,
             T.Descuento * T.Salida
      FROM Tr_Inventario T
      JOIN Comprobantes C USING (Tipo, Prefijo, Numero)
      JOIN Documentos D ON (T.Tipo = D.Codigo)
      WHERE C.Fecha = :Fecha_
            AND TRIM(C.Tipo) LIKE :Tipo_ || '%'
            AND TRIM(C.Computador) LIKE :Compu_ || '%'
            AND D.Grupo = 'VENTA'
            AND D.Es_Compra_Venta = 'S'
            AND D.Signo = 'CREDITO'
            AND D.M_Aplica <> 'S'
      INTO Usuario,
           Computador,
           Tipo,
           Prefijo,
           Numero,
           Renglon,
           Referencia,
           Fecha,
           Cantidad,
           Bruto,
           Descuento
  DO
  BEGIN

    -- Impuestos
    Grvado = 0;
    No_Gravado = 0;
    SELECT SUM(IIF(Es_Tarifa = 'S', Credito, 0)),
           SUM(IIF(Es_Tarifa = 'N', Credito, 0))
    FROM Reg_Impuestos
    WHERE Tipo = :Tipo
          AND Prefijo = :Prefijo
          AND Numero = :Numero
          AND Renglon = :Renglon
    INTO Grvado,
         No_Gravado;

    --Tarifa
    Tarifa = NULL;
    SELECT FIRST 1 Tarifa
    FROM Reg_Impuestos
    WHERE Tipo = :Tipo
          AND Prefijo = :Prefijo
          AND Numero = :Numero
          AND Renglon = :Renglon
          AND Tarifa > 0
    INTO Tarifa;
    Tarifa = COALESCE(Tarifa, 0);

    -- Linea
    Linea = NULL;
    Nombre_Linea = NULL;
    SELECT Codlinea
    FROM Referencias
    WHERE Codigo = :Referencia
    INTO Linea;

    SELECT Nombre
    FROM Lineas
    WHERE Codigo = :Linea
    INTO Nombre_Linea;

    -- Usuarios
    V_Personal = NULL;
    SELECT Codpersonal
    FROM Usuarios
    WHERE Codigo = :Usuario
    INTO V_Personal;
    V_Personal = COALESCE(V_Personal, '');

    -- Computador
    V_Maquina = NULL;
    SELECT Codigo2
    FROM Personal
    WHERE Codigo = :V_Personal
    INTO V_Maquina;

    IF (TRIM(COALESCE(V_Maquina, '')) <> '') THEN
    BEGIN
      IF (Computador CONTAINING 'MIAMI') THEN
        UPDATE Comprobantes
        SET Computador = :V_Maquina
        WHERE Tipo = :Tipo
              AND Prefijo = :Prefijo
              AND Numero = :Numero;
      Computador = V_Maquina;
    END

    -- Minimo
    SELECT MIN(CAST(Numero AS INTEGER))
    FROM Comprobantes
    WHERE Codusuario = :Usuario
          AND Prefijo = :Prefijo
          AND Fecha = :Fecha_
    INTO Minimo;

    -- Máximo
    SELECT MAX(CAST(Numero AS INTEGER))
    FROM Comprobantes
    WHERE Codusuario = :Usuario
          AND Prefijo = :Prefijo
          AND Fecha = :Fecha_
    INTO Maximo;

    SUSPEND;
  END
END^


CREATE OR ALTER PROCEDURE PZ_DIARIO_VENTAS_MAQ (
    FECHA_ DATE,
    TIPO_ VARCHAR(5),
    COMPU_ VARCHAR(20))
RETURNS (
    TRANSACCION VARCHAR(20),
    COMPUTADOR VARCHAR(20),
    TRANSACCIONES INTEGER,
    VALOR DOUBLE PRECISION)
AS
BEGIN
  FOR SELECT Computador,
             SUM(Bruto - Descuento + Grvado + No_Gravado)
      FROM Pz_Diario_Ventas(:Fecha_, :Tipo_, :Compu_)
      GROUP BY 1
      INTO Computador,
           Valor
  DO
  BEGIN
    Transaccion = 'TRANSACCIONES';
    SELECT COUNT(1)
    FROM Comprobantes
    WHERE Computador = :Computador
          AND Fecha = :Fecha_
    INTO Transacciones;
    SUSPEND;
  END
END^



SET TERM ; ^

UPDATE OR INSERT INTO REPORTES (CODIGO, NOMBRE, NOTA, ARCHIVO, ADJUNTO, NATIVO, MODULO, PARAMETROS, PUBLICAR, CODGRUPO, VALIDA_TRANSITO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD)
                        VALUES ('POZG030202', 'INFORME DIARIO VENTAS POR MAQUINA CON PREFIJO SEGUN FECHA -TIRILLA TERMICA (WEB)', 'Según fecha indicada muestra informe de ventas por documento y por computador, elegir uno o todos.  
Requisitos: El código de forma de pago en efectivo siempre debe ser ''01'' y Computador en Usuarios', 'R_AUX_MOV_05A.FR3', NULL, 'S', 'GESTION', 'FECHA,TIPO_DOCUMENTO,COMPUTADOR', 'N', 'COMPROBANTE Z', 'N', 'N', 'LIBRE', 2, 'N', 1)
                      MATCHING (CODIGO);
UPDATE OR INSERT INTO REPORTES (CODIGO, NOMBRE, NOTA, ARCHIVO, ADJUNTO, NATIVO, MODULO, PARAMETROS, PUBLICAR, CODGRUPO, VALIDA_TRANSITO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD)
                        VALUES ('POZG030201', 'INFORME DIARIO VENTAS POR MAQUINA CON PREFIJO SEGUN FECHA -CARTA (WEB)', 'Según fecha indicada muestra informe de ventas por documento y por computador, elegir uno o todos.  
Requisitos: El código de forma de pago en efectivo siempre debe ser ''01'' y Computador en Usuarios', 'R_AUX_MOV_04A.FR3', NULL, 'S', 'GESTION', 'FECHA,TIPO_DOCUMENTO,COMPUTADOR', 'N', 'COMPROBANTE Z', 'N', 'N', 'LIBRE', 2, 'N', 1)
                      MATCHING (CODIGO);
UPDATE OR INSERT INTO REPORTES (CODIGO, NOMBRE, NOTA, ARCHIVO, ADJUNTO, NATIVO, MODULO, PARAMETROS, PUBLICAR, CODGRUPO, VALIDA_TRANSITO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD)
                        VALUES ('POZG030203', 'INFORME DIARIO VENTAS POR MAQUINA SEGUN FECHA -TIRILLA MATRIZ (WEB)', 'Según fecha indicada muestra informe de ventas por documento y por computador, elegir uno o todos.  
Requisitos: El código de forma de pago en efectivo siempre debe ser ''01'' y Computador en Usuarios', 'R_AUX_MOV_05B.FR3', NULL, 'S', 'GESTION', 'FECHA,TIPO_DOCUMENTO,COMPUTADOR', 'N', 'COMPROBANTE Z', 'N', 'N', 'LIBRE', 2, 'N', 1)
                      MATCHING (CODIGO);


COMMIT WORK;
