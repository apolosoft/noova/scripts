/*------------- Informe saldo de inventario por referencia, linea y categoria con costo y unidades -------------------*/ 

UPDATE OR INSERT INTO INFORMES (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD, DESTINO_FTP, CODREPORTE)
VALUES ('LX278_002 ', 'SALDO DE INVENTARIO POR REFERENCIA LINEA Y CATEGORIA CON COSTO Y UNIDADES ', NULL, 'WITH V_Saldo
AS (SELECT Referencia,
SUM(Entradas - Salidas) Existencia,
Categoria
FROM Fx_Inventario
GROUP BY 1, 3),

V_Comprobantes
AS (SELECT Tr.Codbodega,
Tr.Codreferencia,
Tr.Entrada,
Tr.Salida,
Tr.Codcategoria
FROM Comprobantes Co
JOIN Tr_Inventario Tr ON (Co.Tipo = Tr.Tipo AND Co.Prefijo = Tr.Prefijo AND Co.Numero = Tr.Numero)
JOIN Documentos D ON (D.Codigo = Co.Tipo)
WHERE D.Grupo = ''VENTA''
AND D.Nombre LIKE ''FACTURA%''
AND D.Inventario = ''SALIDA''
AND Co.Fecha BETWEEN :_Desde AND :_Hasta)

SELECT R.Codlinea,
TRIM(L.Nombre) AS "NOMBRE LINEA",
TRIM(P.Referencia) Referencia,
TRIM(R.Nombre) AS "NOMBRE REFERENCIA",
TRIM(P.Categoria) AS Categoria,
SUM(P.Inicial) AS Vienen,
SUM(P.Entradas) AS Entradas,
SUM(P.Salidas) AS Salidas,
V1.Existencia,
COALESCE(R.Costo, 0) Costo_Referencia,
SUM(Vco.Salida) Total_Salida,
(SUM(Vco.Salida) * 30) / (DATEDIFF(DAY FROM :_Desde TO :_Hasta) + 1) Unidades_Proyectadas,
IIF(R.Activa = ''S'', ''ACTIVA'', ''INACTIVA'') Estado,
R.Unidad_Compra Unidades_Compra
FROM Fx_Inventario_Rango(:_Desde, :_Hasta) P
JOIN V_Comprobantes Vco ON (Vco.Codreferencia = P.Referencia) AND (Vco.Codcategoria = P.Categoria)
JOIN V_Saldo V1 ON (V1.Referencia = P.Referencia) AND (V1.Referencia = Vco.Codreferencia) AND (V1.Categoria = P.Categoria)
JOIN Bodegas B ON (P.Bodega = B.Codigo) AND (B.Codigo = Vco.Codbodega)
JOIN Referencias R ON (P.Referencia = R.Codigo) AND (R.Activa LIKE :Estado)
JOIN Lineas L ON (R.Codlinea = L.Codigo)
WHERE R.Saldos = ''S''
GROUP BY 1, 2, 3, 4, 5, 9, 10, 13, 14

UNION ALL

SELECT R.Codlinea,
TRIM(L.Nombre) AS "NOMBRE LINEA",
TRIM(P.Referencia) Referencia,
TRIM(R.Nombre) AS "NOMBRE REFERENCIA",
TRIM(P.Categoria) AS Categoria,
SUM(P.Inicial) AS Vienen,
SUM(P.Entradas) AS Entradas,
SUM(P.Salidas) AS Salidas,
V1.Existencia,
COALESCE(R.Costo, 0) Costo_Referencia,
0 Total_Salida,
0 Unidades_Proyectadas,
IIF(R.Activa = ''S'', ''ACTIVA'', ''INACTIVA'') Estado,
R.Unidad_Compra Unidades_Compra
FROM Fx_Inventario_Rango(:_Desde, :_Hasta) P
JOIN V_Saldo V1 ON (V1.Referencia = P.Referencia) AND (V1.Categoria = P.Categoria)
JOIN Bodegas B ON (P.Bodega = B.Codigo)
JOIN Referencias R ON (P.Referencia = R.Codigo) AND (R.Activa LIKE :Estado)
JOIN Lineas L ON (R.Codlinea = L.Codigo)
WHERE R.Saldos = ''S''
AND (P.Entradas + P.Salidas) = 0
GROUP BY 1, 2, 3, 4, 5, 9, 10, 13, 14
ORDER BY 2, 4 ', 'N', 'GESTION ', 'N', 'SALDOS DE INVENTARIO ', 'N', 'LIBRE ', 5, 'N', 1, NULL, NULL)
MATCHING (CODIGO);

COMMIT WORK;


/*------------ Informe de saldo de Inventario por Referencia, Linea y Categoría con costo y unidades incluyendo Referencias nuevas -----------------*/

UPDATE OR INSERT INTO INFORMES (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD, DESTINO_FTP, CODREPORTE)
VALUES ('LX278_003', 'INVENTARIO POR REFERENCIA LINEA Y CATEGORIA CON REFERENCIAS NUEVAS', NULL, 'WITH V_Saldo
AS (SELECT Referencia,
SUM(Entradas - Salidas) Existencia,
Categoria
FROM Fx_Inventario
GROUP BY 1, 3),

V_Comprobantes
AS (SELECT Tr.Codbodega,
Tr.Codreferencia,
Tr.Entrada,
Tr.Salida,
Tr.Codcategoria
FROM Comprobantes Co
JOIN Tr_Inventario Tr ON (Co.Tipo = Tr.Tipo AND Co.Prefijo = Tr.Prefijo AND Co.Numero = Tr.Numero)
JOIN Documentos D ON (D.Codigo = Co.Tipo)
WHERE D.Grupo = ''VENTA''
AND D.Nombre LIKE ''FACTURA%''
AND D.Inventario = ''SALIDA''
AND Co.Fecha BETWEEN :_Desde AND :_Hasta)

SELECT R.Codlinea,
TRIM(L.Nombre) AS "NOMBRE LINEA",
TRIM(P.Referencia) Referencia,
TRIM(R.Nombre) AS "NOMBRE REFERENCIA",
TRIM(P.Categoria) AS Categoria,
SUM(P.Inicial) AS Vienen,
SUM(P.Entradas) AS Entradas,
SUM(P.Salidas) AS Salidas,
V1.Existencia,
COALESCE(R.Costo, 0) Costo_Referencia,
SUM(Vco.Salida) Total_Salida,
(SUM(Vco.Salida) * 30) / (DATEDIFF(DAY FROM :_Desde TO :_Hasta) + 1) Unidades_Proyectadas,
IIF(R.Activa = ''S'', ''ACTIVA'', ''INACTIVA'') Estado,
R.Unidad_Compra Unidades_Compra
FROM Fx_Inventario_Rango(:_Desde, :_Hasta) P
JOIN V_Comprobantes Vco ON (Vco.Codreferencia = P.Referencia) AND (Vco.Codcategoria = P.Categoria)
JOIN V_Saldo V1 ON (V1.Referencia = P.Referencia) AND (V1.Referencia = Vco.Codreferencia) AND (V1.Categoria = P.Categoria)
JOIN Bodegas B ON (P.Bodega = B.Codigo) AND (B.Codigo = Vco.Codbodega)
JOIN Referencias R ON (P.Referencia = R.Codigo) AND (R.Activa LIKE :Estado)
JOIN Lineas L ON (R.Codlinea = L.Codigo)
WHERE R.Saldos = ''S''
GROUP BY 1, 2, 3, 4, 5, 9, 10, 13, 14

UNION ALL

SELECT R.Codlinea,
TRIM(L.Nombre) AS "NOMBRE LINEA",
TRIM(P.Referencia) Referencia,
TRIM(R.Nombre) AS "NOMBRE REFERENCIA",
TRIM(P.Categoria) AS Categoria,
SUM(P.Inicial) AS Vienen,
SUM(P.Entradas) AS Entradas,
SUM(P.Salidas) AS Salidas,
V1.Existencia,
COALESCE(R.Costo, 0) Costo_Referencia,
0 Total_Salida,
0 Unidades_Proyectadas,
IIF(R.Activa = ''S'', ''ACTIVA'', ''INACTIVA'') Estado,
R.Unidad_Compra Unidades_Compra
FROM Fx_Inventario_Rango(:_Desde, :_Hasta) P
JOIN V_Saldo V1 ON (V1.Referencia = P.Referencia) AND (V1.Categoria = P.Categoria)
JOIN Bodegas B ON (P.Bodega = B.Codigo)
JOIN Referencias R ON (P.Referencia = R.Codigo) AND (R.Activa LIKE :Estado)
JOIN Lineas L ON (R.Codlinea = L.Codigo)
WHERE R.Saldos = ''S''
AND (P.Entradas + P.Salidas) = 0
GROUP BY 1, 2, 3, 4, 5, 9, 10, 13, 14

UNION ALL

SELECT R.Codlinea,
TRIM(L.Nombre) AS "NOMBRE LINEA",
TRIM(Tr.Codreferencia) Referencia,
TRIM(R.Nombre) AS "NOMBRE REFERENCIA",
TRIM(Tr.Codcategoria) AS Categoria,
0 AS Vienen,
IIF(Co.Tipo = ''FCCR'', Tr.Entrada, 0) AS Entradas,
Tr.Salida AS Salidas,
IIF(Co.Tipo = ''FCCR'',
(Tr.Entrada - Tr.Salida), 0) Existencia,
COALESCE(R.Costo, 0) Costo_Referencia,
0 Total_Salida,
0 Unidades_Proyectadas,
IIF(R.Activa = ''S'', ''ACTIVA'', ''INACTIVA'') Estado,
R.Unidad_Compra Unidades_Compra
FROM Comprobantes Co
JOIN Tr_Inventario Tr ON (Co.Tipo = Tr.Tipo AND Co.Prefijo = Tr.Prefijo AND Co.Numero = Tr.Numero)
JOIN Documentos D ON (D.Codigo = Co.Tipo)
JOIN Referencias R ON (Tr.Codreferencia = R.Codigo) AND (R.Activa LIKE :Estado)
JOIN Lineas L ON (R.Codlinea = L.Codigo)
WHERE Co.Fecha BETWEEN :_Desde AND :_Hasta
AND D.Grupo = ''COMPRA''
AND D.Inventario = ''ENTRADA''
ORDER BY 2, 4', 'N', 'GESTION', 'N', 'SALDOS DE INVENTARIO', 'N', 'LIBRE', 5, 'N', 1, NULL, NULL)
MATCHING (CODIGO);

COMMIT WORK;


/*------------------------ PARAMETROS -------------------------------*/

UPDATE OR INSERT INTO PARAMETROS (CODIGO, NOMBRE, PUNTERO)
                          VALUES ('MARCA', 'MARCA', 'MARCAS')
                        MATCHING (CODIGO);
UPDATE OR INSERT INTO PARAMETROS (CODIGO, NOMBRE, PUNTERO)
                          VALUES ('UBICACION', 'UBICACION', 'REFERENCIAS')
                        MATCHING (CODIGO);
UPDATE OR INSERT INTO PARAMETROS (CODIGO, NOMBRE, PUNTERO)
                          VALUES ('LINEA', 'LINEA', 'LINEAS');
COMMIT WORK;

/*--------------------------- PROCESO LX278_004 -------------------------------*/

UPDATE OR INSERT INTO PROCESOS (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, CODGRUPO, NIVEL, TABLERO, PRIORIDAD)
                        VALUES ('LX278_004', 'MODIFICAR LA UBICACION DE LAS REFERENCIAS POR LINEA Y MARCA', 'Proceso para modificar la columna UBICACION de las Referencias de acuerdo a una Linea y Marca ya definidas', 'EXECUTE BLOCK (
    Pubicacion CHAR(5) = :Ubicacion,
    Pcodlinea CHAR(5) = :Linea,
    Pcodmarca CHAR(10) = :Marca)
AS
DECLARE VARIABLE Vsql CHAR(1000);
BEGIN
  Vsql = ''UPDATE Referencias
 SET Ubicacion = TRIM('''''' || Pubicacion || '''''')
 WHERE Codlinea = TRIM('''''' || Pcodlinea || '''''')
       AND Codmarca = TRIM('''''' || Pcodmarca || '''''')
       '';

    EXECUTE STATEMENT Vsql;

END', 'N', 'PARAMETROS', 'PROCESOS TECNICOS', 5, 'N', 1)
                      MATCHING (CODIGO);

COMMIT WORK;

/*--------------------------PROCESO LX278_005 -----------------------------------*/

UPDATE OR INSERT INTO PROCESOS (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, CODGRUPO, NIVEL, TABLERO, PRIORIDAD)
                        VALUES ('LX278_005', 'MODIFICAR LA UBICACION DE LAS REFERENCIAS POR LINEA', 'Proceso para modificar la columna UBICACION de las Referencias de acuerdo a una Linea ya definida', '/* Proceso para modificar la ubicacion de una referencia por Linea introducido por el usuario */
EXECUTE BLOCK (
    Pubicacion CHAR(5) = :Ubicacion,
    Pcodlinea CHAR(5) = :Linea)
AS
DECLARE VARIABLE Vsql CHAR(1000);
BEGIN
  Vsql = ''UPDATE Referencias
 SET Ubicacion = TRIM('''''' || Pubicacion || '''''')
 WHERE Codlinea = TRIM('''''' || Pcodlinea || '''''')
       '';

  EXECUTE STATEMENT Vsql;

END', 'N', 'PARAMETROS', 'PROCESOS TECNICOS', 5, 'N', 1)
                      MATCHING (CODIGO);

COMMIT WORK;