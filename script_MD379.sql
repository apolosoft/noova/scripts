
UPDATE OR INSERT INTO DESTINOS (CODIGO, NOMBRE) VALUES ('CICENTRO', 'CICENTRO');


COMMIT WORK;

UPDATE OR INSERT INTO AUTOMATOR (CODIGO, NOMBRE, MODO, ACTIVA, DELIMITADOR, CODDESTINO) VALUES ('PERCI0001', 'Relacion de ventas', 'EMAIL', 'S', ',', 'CICENTRO');
UPDATE OR INSERT INTO AUTOMATOR (CODIGO, NOMBRE, MODO, ACTIVA, DELIMITADOR, CODDESTINO) VALUES ('PERLISTA', 'Lista de precios', 'EMAIL', 'S', ',', 'CICENTRO');
UPDATE OR INSERT INTO AUTOMATOR (CODIGO, NOMBRE, MODO, ACTIVA, DELIMITADOR, CODDESTINO) VALUES ('PERCI0002', 'Relacion de ventas por cliente', 'EMAIL', 'S', ',', 'CICENTRO');
UPDATE OR INSERT INTO AUTOMATOR (CODIGO, NOMBRE, MODO, ACTIVA, DELIMITADOR, CODDESTINO) VALUES ('PERCI0003', 'Seguimiento recaudos', 'EMAIL', 'S', ',', 'CICENTRO');
UPDATE OR INSERT INTO AUTOMATOR (CODIGO, NOMBRE, MODO, ACTIVA, DELIMITADOR, CODDESTINO) VALUES ('PERCI0004', 'Relacion compras combustible', 'EMAIL', 'S', ',', 'CICENTRO');


COMMIT WORK;



/******************************************************************************/
/***                              Primary keys                              ***/
/******************************************************************************/

ALTER TABLE AUTOMATOR ADD CONSTRAINT PK_AUTOMATOR_1 PRIMARY KEY (CODIGO);
ALTER TABLE DESTINOS ADD CONSTRAINT PK_DESTINOS PRIMARY KEY (CODIGO);


/******************************************************************************/
/***                              Foreign keys                              ***/
/******************************************************************************/

ALTER TABLE AUTOMATOR ADD CONSTRAINT FK_AUTOMATOR_1 FOREIGN KEY (CODDESTINO) REFERENCES DESTINOS (CODIGO) ON UPDATE CASCADE;

/******************************************************************************/
		    DATOS PARA TABLAS AUTOMATOR Y DESTINOS
/******************************************************************************/

UPDATE OR INSERT INTO AUTOMATOR (CODIGO, NOMBRE, MODO, SQL_CONSULTA, ACTIVA, DELIMITADOR, CODDESTINO)
                         VALUES ('PERCI0001', 'Relacion de ventas', 'EMAIL', 'WITH Vt_Estructura
AS (SELECT Codlista,
           Referencia,
           MAX(Precio) AS Precio,
           MAX(Transporte_Visible) AS Transporte_Visible,
           MAX(Fleoculto) AS Fleoculto,
           MAX(Finoculto) AS Finoculto,
           MAX(Fcostobio) Fcostobio
    FROM (SELECT P.Codlista,
                 ''BIODI'' AS Referencia,
                 IIF(P.Codreferencia IN (''ACPM'', ''BIODI''), P.Precio, 0) AS Precio,
                 IIF(P.Codreferencia IN (''TRANS'', ''TRANSS''), P.Precio, 0) AS Transporte_Visible,
                 IIF(P.Codreferencia IN (''FLEOCULTO''), P.Precio, 0) AS Fleoculto,
                 IIF(P.Codreferencia IN (''FINOCULTO''), P.Precio, 0) AS Finoculto,
                 IIF(P.Codreferencia IN (''FCOSTOBIO''), P.Precio, 0) AS Fcostobio
          FROM Precios P
          INNER JOIN Referencias R ON (P.Codreferencia = R.Codigo)
          WHERE (P.Codreferencia IN (''ACPM'', ''BIODI'', ''TRANS'', ''TRANSS'', ''FCOSTOBIO'', ''FINOCULTO'', ''FLEOCULTO'', ''EXTRA'', ''GASOL'')))
    GROUP BY 1, 2)
SELECT R.Tipo,
       R.Prefijo,
       R.Numero,
       C.Codlista,
       R.Renglon,
       R.Fecha,
       R.Numero AS "# FACTURA#",
       D.Nombre AS "TIPO DOCUMENTO",
       R.Codreferencia,
       COALESCE(R1.Codigo2, ''99'') AS Sku,
       R.Nombre_Referencia AS "NOMBRE SKU",
       R.Codtercero AS "CODIGO CLIENTE",
       T.Nombre AS Nombre_Cliente,
       S.Nombre AS "ZONA ENTREGA",
       (R.Salidas) Cantidad,
       R.Codbodega Bodega,
       P.Nombre AS Planta,
       C.Codreferido AS Vehiculo,
       T2.Codactividad AS Capacidad,
       T2.Direccion AS Cudad_Vehiculo,
       R.Salidas * (R.Unitario - R.Bruto) AS Impuestos,
       IIF(R.Salidas < 0, -R.Descuentos, R.Descuentos) AS Descuento,

       R.Total - COALESCE(R.Salidas * Es.Fleoculto, 0) AS Venta_Neta,
       R.Consumo AS Costo_Combustible,
       (R.Salidas * (R.Unitario)) AS Venta_Facturada,
       R.Total - R.Consumo - COALESCE(R.Salidas * Es.Fleoculto, 0) AS Margen,
       COALESCE(Es.Transporte_Visible, 0) AS Transporte_Visible,
       COALESCE(R.Salidas * Es.Fleoculto, 0) AS Fleoculto,
       COALESCE(R.Salidas * Es.Finoculto, 0) AS Finoculto,
       COALESCE(R.Salidas * Es.Fleoculto, 0) AS Costoflete,
       COALESCE(Es.Transporte_Visible, 0) + COALESCE(R.Salidas * Es.Fleoculto, 0) AS Fletetotal
FROM Consumos_Rango(:primer_dia_mes_anterior, :ultimo_dia_mes_anterior) R
--FROM Consumos_Rango(''01.07.2022'', ''30.07.2022'') R
INNER JOIN Referencias R1 ON (R1.Codigo = R.Codreferencia)
INNER JOIN Documentos D ON (R.Tipo = D.Codigo)
INNER JOIN Terceros T ON (R.Codtercero = T.Codigo)
INNER JOIN Comprobantes C ON (R.Tipo = C.Tipo AND
      R.Prefijo = C.Prefijo AND
      R.Numero = C.Numero)
LEFT JOIN Sucursales S ON (C.Codsucursal = S.Codigo)
LEFT JOIN Vt_Estructura Es ON (Es.Codlista = C.Codlista AND
      Es.Referencia = R.Codreferencia)
INNER JOIN Personal P ON (C.Codvendedor = P.Codigo)
LEFT JOIN Terceros T2 ON (T2.Codigo = C.Codreferido)
WHERE D.Grupo = ''VENTA''
      AND (R.Salidas <> 0)
      -- AND TRIM(R.Tipo) LIKE :Ktipodoc AND
      -- COALESCE(TRIM(R.Codtercero), '''') LIKE :Tercero
      AND R.Tipo NOT IN (''AT'')
      AND R.Nombre_Referencia NOT LIKE ''%IVA%''
ORDER BY R.Fecha, R.Tipo, R.Prefijo, CAST(R.Numero AS INT), R.Codtercero, R.Codreferencia, R.Renglon', 'S', ',', 'CICENTRO')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO AUTOMATOR (CODIGO, NOMBRE, MODO, SQL_CONSULTA, ACTIVA, DELIMITADOR, CODDESTINO)
                         VALUES ('PERLISTA', 'Lista de precios', 'EMAIL', 'SELECT Codlista,
       SUM(Precio) Total
FROM Precios
WHERE Codreferencia IN (''IVA19'', ''IVA5'', ''FMARGEN'', ''FLEOCULTO'', ''FINOCULTO'')
      AND Codlista = ''0501''
GROUP BY Codlista', 'S', ',', 'CICENTRO')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO AUTOMATOR (CODIGO, NOMBRE, MODO, SQL_CONSULTA, ACTIVA, DELIMITADOR, CODDESTINO)
                         VALUES ('PERCI0002', 'Relacion de ventas por cliente', 'EMAIL', 'SELECT R.Fecha,
       R.Numero AS "# FACTURA",
       R.Codreferencia AS Sku,
       R.Nombre_Referencia AS "NOMBRE SKU",
       R.Codtercero AS "CODIGO CLIENTE",
       T.Nombre AS Nombre_Cliente,
       (R.Salidas) Cantidad,
       (R.Bruto) AS Unitario,
       R.Unitario - R.Bruto AS Impuestos,
       IIF(R.Salidas < 0, -R.Descuentos, R.Descuentos) AS Descuento,
       R.Total,
       R.Total - R.Consumo AS Utilidad
FROM Consumos_Rango(:primer_dia_mes_anterior, :ultimo_dia_mes_anterior) R
--FROM Consumos_Rango(''01.07.2022'', ''30.07.2022'') R
INNER JOIN Documentos D ON (R.Tipo = D.Codigo)
INNER JOIN Terceros T ON (R.Codtercero = T.Codigo)
INNER JOIN Comprobantes C ON (R.Tipo = C.Tipo AND
      R.Prefijo = C.Prefijo AND
      R.Numero = C.Numero)
LEFT JOIN Sucursales S ON (C.Codsucursal = S.Codigo)
WHERE D.Grupo = ''VENTA''
      AND (R.Salidas <> 0)
     -- AND TRIM(R.Tipo) LIKE :Ktipodoc
     -- AND COALESCE(TRIM(R.Codtercero), '''') LIKE :Tercero
      AND R.Tipo NOT IN (''AT'')
      AND R.Nombre_Referencia NOT LIKE ''%IVA%''
ORDER BY R.Fecha, R.Tipo, R.Prefijo, CAST(R.Numero AS INT), R.Codtercero, R.Codreferencia, R.Renglon', 'S', ',', 'CICENTRO')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO AUTOMATOR (CODIGO, NOMBRE, MODO, SQL_CONSULTA, ACTIVA, DELIMITADOR, CODDESTINO)
                         VALUES ('PERCI0003', 'Seguimiento recaudos', 'EMAIL', 'WITH Vt_Recaudos
AS (SELECT DISTINCT C.Fecha AS Fecha_Recaudo,
                    R.Tiporef,
                    R.Prefijoref,
                    R.Numeroref
    FROM Comprobantes C
    INNER JOIN Reg_Cartera R ON ((C.Tipo = R.Tipo) AND
          (C.Prefijo = R.Prefijo) AND
          (C.Numero = R.Numero))
    WHERE (C.Fecha >= :primer_dia_mes_anterior
          AND C.Fecha <= :ultimo_dia_mes_anterior)
 /*WHERE (C.Fecha >= ''01.07.2022''
          AND C.Fecha <= ''30.07.2022'')*/
          AND (TRIM(C.Tipo) LIKE ''RC%'')),
Vt_Financiacion
AS (SELECT Codlista,
           Precio
    FROM Precios
    WHERE Codreferencia = ''FINOCULTO'')
SELECT Vr.Tiporef,
       Vr.Prefijoref,
       Vr.Numeroref,
       TRIM(T.Nombre) AS Tercero,
       TRIM(S.Nombre) AS Sucursal,
       C.Fecha,
       C.Vence,
       C.Vence - C.Fecha AS Plazo,
       Vr.Fecha_Recaudo,
       Vr.Fecha_Recaudo - C.Vence AS Dias,
       COALESCE(Vf.Precio, 0) AS Vlr_Flete,
       Vg.Cantidad AS Galones,
       COALESCE(Vf.Precio, 0) * Vg.Cantidad AS Flete_Oculto,
       -((COALESCE(Vf.Precio, 0) * Vg.Cantidad) / (IIF(C.Vence - C.Fecha = 0, 1, C.Vence - C.Fecha)) * (Vr.Fecha_Recaudo - C.Vence)) AS Financiacion_Calculada
FROM Comprobantes C
INNER JOIN Vt_Recaudos Vr ON (C.Tipo = Vr.Tiporef AND
      C.Prefijo = Vr.Prefijoref AND
      C.Numero = Vr.Numeroref)
LEFT JOIN Vt_Financiacion Vf ON (C.Codsucursal = Vf.Codlista)
INNER JOIN(SELECT T.Tipo,
                  T.Prefijo,
                  T.Numero,
                  SUM(T.Salida) AS Cantidad
           FROM Tr_Inventario T
           INNER JOIN Vt_Recaudos Vr1 ON (T.Tipo = Vr1.Tiporef AND
                 T.Prefijo = Vr1.Prefijoref AND
                 T.Numero = Vr1.Numeroref)
           WHERE T.Codreferencia IN (''BIODI'', ''COMBU'', ''DISEL'', ''EXTRA'', ''GASOL'', ''QUEROSENO'')
           GROUP BY T.Tipo, T.Prefijo, T.Numero) Vg ON (Vg.Tipo = C.Tipo AND
      Vg.Prefijo = C.Prefijo AND
      Vg.Numero = C.Numero)
INNER JOIN Terceros T ON (C.Codtercero = T.Codigo)
LEFT JOIN Sucursales S ON (S.Codigo = C.Codsucursal)
ORDER BY Vr.Fecha_Recaudo, 4, 5, Vr.Tiporef, Vr.Prefijoref, Vr.Numeroref', 'S', ',', 'CICENTRO')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO AUTOMATOR (CODIGO, NOMBRE, MODO, SQL_CONSULTA, ACTIVA, DELIMITADOR, CODDESTINO)
                         VALUES ('PERCI0004', 'Relacion compras combustible', 'EMAIL', 'SELECT C.Tipo,
       C.Prefijo,
       C.Numero,
       C.Codtercero,
       TRIM(T1.Nombre) AS Tercero,
       TRIM(P.Nombre) AS Planta,
       C.Fecha,
       T.Codbodega,
       B.Nombre AS Bodega,
       T.Codreferencia,
       TRIM(R.Nombre) AS Referencia,
       T.Entrada AS Cantidad,
       T.Bruto AS Unitario,
       T.Entrada * T.Bruto AS Total
FROM Comprobantes C
INNER JOIN Terceros T1 ON (C.Codtercero = T1.Codigo)
INNER JOIN Personal P ON (C.Codvendedor = P.Codigo)
INNER JOIN Tr_Inventario T ON (C.Tipo = T.Tipo AND
      C.Prefijo = T.Prefijo AND
      C.Numero = T.Numero)
INNER JOIN Referencias R ON (T.Codreferencia = R.Codigo)
INNER JOIN Bodegas B ON (T.Codbodega = B.Codigo)
WHERE C.Tipo = ''CO''
      AND (C.Fecha >= :primer_dia_mes_anterior
      AND C.Fecha <= :ultimo_dia_mes_anterior)
    /* AND (C.Fecha >= ''01.07.2022''
      AND C.Fecha <= ''30.07.2022'')*/
      AND R.Codigo IN (''BIODI'', ''COMBU'', ''DISEL'', ''EXTRA'', ''GASOL'', ''QUEROSENO'')
ORDER BY C.Fecha, C.Codtercero, C.Tipo, C.Prefijo, C.Numero', 'S', ',', 'CICENTRO')
                       MATCHING (CODIGO);


COMMIT WORK;


UPDATE OR INSERT INTO DESTINOS (CODIGO, NOMBRE, EMAIL)
                        VALUES ('CICENTRO', 'CICENTRO', 'cartera@cicentro.com.co
smtp.gmail.com
25
Contraseña de aplicacion
Envio informacion data
Envio informacion data
comercial@cicentro.com.co
S')
                      MATCHING (CODIGO);


COMMIT WORK;

