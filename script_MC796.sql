
UPDATE OR INSERT INTO CARGOS (CODIGO, NOMBRE, MODIFICADO)
                      VALUES ('ING', 'INGRESO', '2022-11-28')
                    MATCHING (CODIGO);
UPDATE OR INSERT INTO CARGOS (CODIGO, NOMBRE, MODIFICADO)
                      VALUES ('RET', 'RETIRO', '2022-11-28')
                    MATCHING (CODIGO);


COMMIT WORK;


/******************************************************************************/
/***                           Stored procedures                            ***/
/******************************************************************************/



SET TERM ^ ;

CREATE OR ALTER PROCEDURE PZ_CXCFECHAS (
    PROTOTIPO VARCHAR(1))
RETURNS (
    TERCERO VARCHAR(15),
    NOMBRE VARCHAR(163),
    DIRECCION VARCHAR(80),
    TELEFONO VARCHAR(50),
    ESTADO VARCHAR(10),
    FECHA_INGRESO DATE,
    FECHA_RETIRO DATE,
    SALDO DOUBLE PRECISION)
AS
BEGIN
  SUSPEND;
END^




SET TERM ; ^



/******************************************************************************/
/***                           Stored procedures                            ***/
/******************************************************************************/



SET TERM ^ ;

CREATE OR ALTER PROCEDURE PZ_CXCFECHAS (
    PROTOTIPO VARCHAR(1))
RETURNS (
    TERCERO VARCHAR(15),
    NOMBRE VARCHAR(163),
    DIRECCION VARCHAR(80),
    TELEFONO VARCHAR(50),
    ESTADO VARCHAR(10),
    FECHA_INGRESO DATE,
    FECHA_RETIRO DATE,
    SALDO DOUBLE PRECISION)
AS
DECLARE VARIABLE Codtercero VARCHAR(15);
BEGIN
  FOR SELECT TRIM(C.Tercero) Tercero,
             TRIM(T.Nombre) Nombre,
             TRIM(COALESCE(T.Direccion, '')) Direccion,
             TRIM(COALESCE(T.Telefono, '')) Telefono,
             CASE
               WHEN CAST(:Prototipo AS CHAR(1)) = 'S' THEN 'Activos'
               WHEN CAST(:Prototipo AS CHAR(1)) = 'N' THEN 'Inactivos'
               WHEN COALESCE(CAST(:Prototipo AS CHAR(1)), '') = '%' THEN 'Todos'
             END Estado,
             SUM(C.Debito - C.Credito) Saldo
      FROM Reg_Cartera C
      JOIN Terceros T ON (T.Codigo = C.Tercero)
      WHERE TRIM(T.Prototipo) LIKE :Prototipo
      GROUP BY 1, 2, 3, 4, 5
      INTO Codtercero,
           Nombre,
           Direccion,
           Telefono,
           Estado,
           Saldo

  DO
  BEGIN

    Fecha_Ingreso = NULL;
    Fecha_Retiro = NULL;

    SELECT MAX(Fecha)
    FROM Contactos
    WHERE Codtercero = :Codtercero
          AND Codcargo = 'ING'
    INTO Fecha_Ingreso;

    SELECT MAX(Fecha)
    FROM Contactos
    WHERE Codtercero = :Codtercero
          AND Codcargo = 'RET'
          AND Fecha > :Fecha_Ingreso
    INTO Fecha_Retiro;

    Tercero = :Codtercero;

    SUSPEND;
  END

END^



SET TERM ; ^

