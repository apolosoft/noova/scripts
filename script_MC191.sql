/* Informe de saldos de inventario por bodegas con categorías + tres listas de precios */
UPDATE OR INSERT INTO INFORMES (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD, DESTINO_FTP, CODREPORTE)
VALUES ('MC191_001 ', 'SALDO DE INVENTARIO POR BODEGAS CON CATEGORIAS + 3 LISTAS DE PRECIO ', NULL, 'WITH Vt_Lista1
AS (SELECT ''LISTA1'' AS Lista1,
P.Referencia AS Referencia1,
P.Bruto,
P.Unitario AS Precio1,
P.Maxdescuento AS Maxdescuento1
FROM Fx_Precios(:Precio1, ''VENTA'', EXTRACT(YEAR FROM CURRENT_DATE)) P
WHERE P.Unitario > 0),

Vt_Lista2
AS (SELECT ''LISTA2'' AS Lista2,
P.Referencia AS Referencia2,
P.Bruto,
P.Unitario AS Precio2,
P.Maxdescuento AS Maxdescuento2
FROM Fx_Precios(:Precio2, ''VENTA'', EXTRACT(YEAR FROM CURRENT_DATE)) P
WHERE P.Unitario > 0),

Vt_Lista3
AS (SELECT ''LISTA3'' AS Lista3,
P.Referencia AS Referencia3,
P.Bruto,
P.Unitario AS Precio3,
P.Maxdescuento AS Maxdescuento3
FROM Fx_Precios(:Precio3, ''VENTA'', EXTRACT(YEAR FROM CURRENT_DATE)) P
WHERE P.Unitario > 0)

SELECT R.Codigo Referencia,
R.Nombre AS Nombre_Referencia,
I.Categoria,
--R.Codmedida,
(I.Entradas - I.Salidas) AS Existencia,
I.Ponderado,
--P.Lista1,
COALESCE(P.Precio1, 0) Precio1,
-- P.Maxdescuento1,
-- P1.Lista2,
COALESCE(P1.Precio2, 0) Precio2,
--P1.Maxdescuento2,
--P2.Lista3,
COALESCE(P2.Precio3, 0) Precio3,
--P2.Maxdescuento3,
R.Codlinea Linea,
TRIM(L.Nombre) AS Nombre_Linea,
I.Bodega
FROM Referencias R
JOIN Fx_Inventario_Fecha(:Fecha) I ON (I.Referencia = R.Codigo) AND (I.Bodega LIKE TRIM(:Bodega) || ''%'')
JOIN Lineas L ON (R.Codlinea = L.Codigo) AND (L.Codigo LIKE TRIM(:Linea) || ''%'')
JOIN Vt_Lista1 P ON (R.Codigo = P.Referencia1)
JOIN Vt_Lista2 P1 ON (R.Codigo = P1.Referencia2)
JOIN Vt_Lista3 P2 ON (R.Codigo = P2.Referencia3)
WHERE R.Categoria = ''S''
AND L.Tipo <> ''GASTO''
ORDER BY 4, 2 ', 'N', 'GESTION ', 'N', 'SALDOS DE INVENTARIO ', 'N', 'LIBRE ', 5, 'N', 1, NULL, NULL)
MATCHING (CODIGO);

COMMIT WORK;