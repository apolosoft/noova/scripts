/******************************************************************************/
/***                           Apis Rest Server		                    ***/
/******************************************************************************/


UPDATE OR INSERT INTO INTERFACE_REST (CLAVE, CONSULTA, METHOD_HTTP, DESCRIPCION, REGISTRO)
                              VALUES ('Get_Terceros', 'SELECT Codigo, Naturaleza, Codigo2, Dv, Empresa, Nom1, Nom2, Apl1, Apl2, Razon_Comercial, Codigo_Postal, Barrio, Direccion, Envio, Telefono, Movil, Email, Gerente, Website, Fecha, Habeas_Data, Codsociedad, Codidentidad, Codmunicipio, Codactividad, Codzona, Codlista, Codpersonal, Codpais, Codlista_Compra, Codtipologia, Nombre FROM Rest_Select_Terceros(:Codigo)', 'POST', 'Obtiene información del Tercero', 313851328775202758)
                            MATCHING (CLAVE);

UPDATE OR INSERT INTO INTERFACE_REST (CLAVE, CONSULTA, METHOD_HTTP, DESCRIPCION, REGISTRO)
                              VALUES ('Set_Terceros', 'EXECUTE PROCEDURE Rest_Insert_Terceros(:Codigo, :Dv, :Naturaleza, :Nom1, :Nom2, :Apl1, :Apl2, :Empresa, :Razon_Comercial, :Direccion, :Telefono, :Movil, :Email, :Codigo_Postal, :Gerente, :Codidentidad, :Codsociedad, :Codpais, :Codactividad, :Codzona, :Codmunicipio)', 'POST', 'Inserta un nuevo Tercero', 6041199247570830422)
                            MATCHING (CLAVE);

UPDATE OR INSERT INTO INTERFACE_REST (CLAVE,CONSULTA, METHOD_HTTP, DESCRIPCION, REGISTRO) VALUES ('Set_Gestion_Primario', 'EXECUTE PROCEDURE Rest_Insert_Gestion_Primario (:Tipo,:Prefijo,:Numero,:Fecha,:Vence,:Tercero,:Vendedor,:Lista,:Banco,:Usuario,:Centro,:Bodega,:Referencia,:Entrada,:Salida,:Unitario,:Nota)','POST','Inserta un nuevo documento primario en Gestión','-1580117744305090000');

UPDATE OR INSERT INTO INTERFACE_REST (CLAVE, CONSULTA, METHOD_HTTP, DESCRIPCION, REGISTRO)
                              VALUES ('Set_Referencias', 'EXECUTE PROCEDURE Rest_Insert_Referencias(:Codigo,:Codigo2,:Nombre,:Nombre2,:Costo,:Precio,:Codlinea,:Codmedida,:Cod_Esqimpuesto,:Cod_Esqretencion,:Cod_Esqcontable)', 'POST', 'Inserta una nueva Referencia', 7128319897263492630)
                            MATCHING (CLAVE);

UPDATE OR INSERT INTO INTERFACE_REST (CLAVE, CONSULTA, METHOD_HTTP, DESCRIPCION, REGISTRO)
                              VALUES ('Get_Referencias', 'SELECT Codigo, Codigo2, Nombre, Nombre2, Ubicacion, Unidad_Compra, Stock, Tiempo, Tiempo_Maximo, Costo, Precio, Peso, Alto, Ancho, Fondo, Medida_Rastreo, Valoriza, Costea, Saldos, Alterna, Balanza, Ensamble, Es_Ensamble, Lote, Serial, Categoria, Pymo, Portafolio, Descripcion, Ean_128, Url_Foto, Bloquear_Esquema, Activa, Crm, Nota, Foto, Analisis_Costos, Interes, Grados, Volumen, Rentabilidad, Subpartida, Codlinea, Codmedida, Cod_Esqimpuesto, Cod_Esqretencion, Cod_Esqcontable, Cod_Esqcontable2, Codgenero, Codmarca FROM Rest_Select_Referencia(:Codigo)', 'POST', 'Obtiene información de la Referencia', 4962192749843726838)
                            MATCHING (CLAVE);

UPDATE OR INSERT INTO INTERFACE_REST (CLAVE, CONSULTA, METHOD_HTTP, DESCRIPCION, REGISTRO)
                              VALUES ('Get_Gestion_Primario', 'SELECT Ctipo, Cprefijo, Cnumero, Fecha, Plazo, Vence, Nota, Tercero, Sucursal, Vendedor, Lista, Banco, Bruto, Descuentos, Impuestos, Retenciones, Total, Sede, Concepto, Trm FROM Rest_Select_Gestion_Primario(:Tipo, :Prefijo, :Numero)', 'POST', 'Obtiene información de un documento primario en Gestión', -581306840052394038)
                            MATCHING (CLAVE);

UPDATE OR INSERT INTO INTERFACE_REST (CLAVE, CONSULTA, METHOD_HTTP, DESCRIPCION, REGISTRO)
                              VALUES ('Set_Gestion_Secundario', 'EXECUTE PROCEDURE Rest_Insert_Gestion_Secundario(:Tipo,:Prefijo,:Numero,:Fecha,:Vence,:Nota,:Tercero,:Vendedor,:Banco,:Usuario,:Tipo_Ref,:Prefijo_Ref,:Numero_Ref,:Abono,:Forma_pago)', 'POST', 'Inserta un nuevo documento secundario en GestiÃ³n', 35349251411694522)
                            MATCHING (CLAVE);


UPDATE OR INSERT INTO INTERFACE_REST (CLAVE,CONSULTA, METHOD_HTTP, DESCRIPCION, REGISTRO) VALUES('Set_Contable_Primario', 'EXECUTE PROCEDURE Rest_Insert_Contable_Primario(:Tipo,:Prefijo,:Numero,:Fecha,:Cuenta,:Tercero,:Centro,:Activo,:Empleado,:Debito,:Credito,:Base,:Nota,:Usuario);','POST', 'Inserta un nuevo documento primario en Contable', '-247844668081813000');


UPDATE OR INSERT INTO INTERFACE_REST (CLAVE,CONSULTA, METHOD_HTTP, DESCRIPCION, REGISTRO) VALUES('Get_Contable_Primario', 'SELECT Ctipo, Cprefijo, Cnumero, Fecha, nota FROM Rest_Select_Contable_Primario (:Tipo, :Prefijo, :Numero)','POST', 'Obtiene información de documento primario en Contable', '1446778453709995562');



UPDATE OR INSERT INTO INTERFACE_REST (CLAVE, CONSULTA, METHOD_HTTP, DESCRIPCION, REGISTRO)
                              VALUES ('Set_Envio_Dian', 'EXECUTE PROCEDURE Nuevo_Comprobante (:Tipo, :Prefijo, :Numero)', 'POST', 'Envio inmediato de pendientes', -190046764462703429)
                            MATCHING (CLAVE);


UPDATE OR INSERT INTO INTERFACE_REST (CLAVE, CONSULTA, METHOD_HTTP, DESCRIPCION, REGISTRO)
                              VALUES ('Get_Datos_FE', 'SELECT Cufe, Qrdata, Validacion, Signature FROM Rest_Select_Datos_Fe(:Tipo, :Prefijo, :Numero)', 'POST', 'Consultar datos de tabla Facturas', 858648167935310148)
                            MATCHING (CLAVE);

UPDATE OR INSERT INTO INTERFACE_REST (CLAVE, CONSULTA, METHOD_HTTP, DESCRIPCION, REGISTRO)
                              VALUES ('Set_Gestion_Primario_B', 'EXECUTE PROCEDURE Rest_insert_Gestion_Primario_B(:Tipo,:Prefijo,:Numero,:Fecha,:Vence,:Tercero,:Vendedor,:Lista,:Banco,:Usuario,:Nota,:Detalle)', 'POST', 'Inserta un nuevo documento primario en GestiÃƒÂ³n', 4223202227833029207)
                            MATCHING (CLAVE);                           


COMMIT WORK;

