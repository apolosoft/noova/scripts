UPDATE OR INSERT INTO INTERFACE_REST (CLAVE, CONSULTA, METHOD_HTTP, DESCRIPCION, REGISTRO)
                              VALUES ('Get_Terceros', 'SELECT Codigo, Naturaleza, Codigo2, Dv, Empresa, Nom1, Nom2, Apl1, Apl2, Razon_Comercial, Codigo_Postal, Barrio, Direccion, Envio, Telefono, Movil, Email, Gerente, Website, Fecha, Habeas_Data, Codsociedad, Codidentidad, Codmunicipio, Codactividad, Codzona, Codlista, Codpersonal, Codpais, Codlista_Compra, Codtipologia, Nombre FROM Rest_Select_Terceros(:Codigo)', 'POST', 'Obtiene informacion del Tercero', 313851328775202758)
                            MATCHING (CLAVE);
UPDATE OR INSERT INTO INTERFACE_REST (CLAVE, CONSULTA, METHOD_HTTP, DESCRIPCION, REGISTRO)
                              VALUES ('Set_Terceros', 'EXECUTE PROCEDURE Rest_Insert_Terceros(:Codigo, :Dv, :Naturaleza, :Nom1, :Nom2, :Apl1, :Apl2, :Empresa, :Razon_Comercial, :Direccion, :Telefono, :Movil, :Email, :Codigo_Postal, :Gerente, :Codidentidad, :Codsociedad, :Codpais, :Codactividad, :Codzona, :Codmunicipio)', 'POST', 'Inserta un nuevo Tercero', 6041199247570830422)
                            MATCHING (CLAVE);
UPDATE OR INSERT INTO INTERFACE_REST (CLAVE, CONSULTA, METHOD_HTTP, DESCRIPCION, REGISTRO)
                              VALUES ('Set_Gestion_Primario_B', 'EXECUTE PROCEDURE Rest_insert_Gestion_Primario_G(:Tipo,:Prefijo,:Numero,:Fecha,:Vence,:Tercero,:Vendedor,:Lista,:Banco,:Usuario,:Nota,:Detalle,:Bloqueado,:Enviado)', 'POST', 'Inserta un nuevo documento primario en Gestion', 4170875030687787863)
                            MATCHING (CLAVE);
UPDATE OR INSERT INTO INTERFACE_REST (CLAVE, CONSULTA, METHOD_HTTP, DESCRIPCION, REGISTRO)
                              VALUES ('Get_Inventario_Precio', 'SELECT referencia,nombre_referencia,saldo,precio FROM rest_select_inventario_precio(:fecha_corte_, :referencia_)', 'POST', 'Obtiene informacion del saldo y precio de la referencia consultada', -16724658224128646)
                            MATCHING (CLAVE);


COMMIT WORK;