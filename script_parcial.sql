

/************************************************************/
/**** MODULO GESTION                                     ****/
/************************************************************/

/* EXPORTADOS GESTION */

DELETE FROM EXPORTADOS
WHERE PT = 'NOOVA';

UPDATE PTA SET 
    XML_TEMPLATE = '<?xml version="1.0" encoding="UTF-8"?>
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:noov="http://Noova/">
    <soapenv:Header>
        <noov:AuthHeader>
            <!--Usuario base64:-->
            <noov:Username>UsuarioNoova</noov:Username>
            <!--Password base64:-->
            <noov:Password>ClaveNoova</noov:Password>
        </noov:AuthHeader>
    </soapenv:Header>
    <!--971 Gestion 06/Mar/2025 v1-->
    <soapenv:Body>
        <noov:SetFacFacturas>
            <noov:dtoFacFacturasInput>
            ges:comprobante
            <noov:lDetalle>
               ges:detalle
            </noov:lDetalle>
            <noov:lImpuestos>
               ges:impuestos
            </noov:lImpuestos>
            <noov:CustomExtensionContent>
            ges:comprador
            ges:caja_venta
            </noov:CustomExtensionContent>
            ges:sector_salud
            ges:tasa_cambio
            ges:espera_pdf
            </noov:dtoFacFacturasInput>
        </noov:SetFacFacturas>
    </soapenv:Body>
</soapenv:Envelope>'
WHERE (CODIGO = 'NOOVA');


COMMIT WORK;

UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (261, 'ges:comprobante', 'DOC', '-- Comprobante Gestion Factura 970 06/Feb/2025 v1

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT Nvfac_Orig "noov:Nvfac_orig",
       Nvemp_Nnit "noov:Nvemp_nnit",
       Nvres_Nume "noov:Nvres_nume",
       Nvfac_Tipo "noov:Nvfac_tipo",
       Nvres_Pref "noov:Nvres_pref",
       Nvfac_Nume "noov:Nvfac_nume",
       Nvfac_Fech "noov:Nvfac_fech",
       Nvfac_Venc "noov:Nvfac_venc",
       Nvsuc_Codi "noov:Nvsuc_codi",
       Nvmon_Codi "noov:Nvmon_codi",
       Nvfor_Codi "noov:Nvfor_codi",
       Nvven_Nomb "noov:Nvven_nomb",
       Nvfac_Fpag "noov:Nvfac_fpag",
       Nvfac_Conv "noov:Nvfac_conv",
       Nvcli_Cper "noov:Nvcli_cper",
       Nvcli_Cdoc "noov:Nvcli_cdoc",
       Nvcli_Docu "noov:Nvcli_docu",
       Nvcli_Pais "noov:Nvcli_pais",
       Nvcli_Depa "noov:Nvcli_depa",
       Nvcli_Ciud "noov:Nvcli_ciud",
       Nvcli_Loca "noov:Nvcli_loca",
       Nvcli_Dire "noov:Nvcli_dire",
       Nvcli_Ntel "noov:Nvcli_ntel",
       Nvcli_Regi "noov:Nvcli_regi",
       Nvcli_Fisc "noov:Nvcli_fisc",
       Nvcli_Nomb "noov:Nvcli_nomb",
       Nvcli_Pnom "noov:Nvcli_pnom",
       Nvcli_Snom "noov:Nvcli_snom",
       Nvcli_Apel "noov:Nvcli_apel",
       Nvcli_Mail "noov:Nvcli_mail",
       Nvema_Copi "noov:Nvema_copi",
       Nvfac_Obse "noov:Nvfac_obse",
       Nvfac_Orde "noov:Nvfac_orde",
       Nvfac_Remi "noov:Nvfac_remi",
       Nvfac_Rece "noov:Nvfac_rece",
       Nvfac_Entr "noov:Nvfac_entr",
       Nvfac_Ccos "noov:Nvfac_ccos",
       Nvfac_Cdet "noov:Nvfac_cdet",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Stot, redondeo)) "noov:Nvfac_stot",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Desc, Redondeo)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Anti, Redondeo)) "noov:Nvfac_anti",
       Nvfac_Carg "noov:Nvfac_carg",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Tota, Redondeo)) "noov:Nvfac_tota",
       (SELECT Valor
        FROM Redondeo_Dian(Com_Total, Redondeo)) "noov:Nvfac_totp",

       (SELECT COALESCE(Valor, 0)
        FROM Redondeo_Dian(Com_Total, Redondeo)) - (SELECT COALESCE(Valor, 0)
                                             FROM Redondeo_Dian(Nvfac_Stot, Redondeo)) - (SELECT COALESCE(Valor, 0)
                                                                                   FROM Redondeo_Dian((SELECT SUM(COALESCE(Valor_Red, 0))
                                                                                                       FROM Pz_Fe_Impuestos_Sum(:Tipo: , :Prefijo: , :Numero: )
                                                                                                       WHERE "noov:Nvimp_oper" = ''S''), Redondeo)) + (SELECT COALESCE(Valor, 0)
                                                                                                                                              FROM Redondeo_Dian(Nvfac_Desc, Redondeo)) "noov:Nvfac_roun",
       Nvfac_Vcop "noov:Nvfac_vcop",
       Nvfac_Timp "noov:Nvfac_timp",

       Nvfac_Coid "noov:Nvfac_coid",
       Nvfac_Obsb "noov:Nvfac_obsb",
       Nvcli_Ncon "noov:Nvcli_ncon",
       Nvfor_Oper "noov:Nvfor_oper"
FROM Pz_Fe_Comprobantes(:Tipo: , :Prefijo: , :Numero: , :Emisor: , :Receptor: , :Clase_Fe: )
JOIN V_Redondeo R ON (1 = 1)', 'S', 5, 'NOOVA', 'FACTURA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (262, 'ges:detalle', 'DOC', '-- Detalle Gestion Factura

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT Renglon,
       Consecutivo,
       "noov:Nvpro_codi",
       "noov:Nvpro_nomb",
       "noov:Nvuni_desc",
       "noov:Nvfac_cant",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_valo", Redondeo)) "noov:Nvfac_valo",
       "noov:Nvfac_pdes",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc", Redondeo)) "noov:Nvfac_desc",
       "noov:Nvdet_pref",
       (SELECT Valor
        FROM Redondeo_Dian(Stot_Red, Redondeo)) "noov:Nvfac_stot",
       "noov:Nvimp_cdia",
       "noov:Nvdet_piva",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvdet_viva", Redondeo)) "noov:Nvdet_viva",
       "noov:Nvdet_tcod",
       "noov:Nvpro_cean",
       "noov:Nvdet_entr",
       "noov:Nvuni_quan",
       "noov:Nvdet_nota",
       "noov:Nvdet_padr",
       "noov:Nvdet_marc",
       "noov:Nvdet_mode",
       "noov:Nvdet_tran"
FROM Pz_Fe_Detalle(:Tipo:, :Prefijo: , :Numero: )
JOIN V_Redondeo R ON (1 = 1)', 'S', 10, 'NOOVA', 'FACTURA', 'noov:DTOFacDetallefacturas', NULL, 'noov:Nvfac_dcop', NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (263, 'ges:imp_detalle', 'DOC', '-- Impuesto Detalle Factura


WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_desc",
       "noov:Nvimp_oper",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base", Redondeo)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo", Redondeo)) "noov:Nvimp_valo"
FROM Pz_Fe_Impuestos_Detalle(:Tipo:, :Prefijo:, :Numero:, :Renglon:)
JOIN V_Redondeo R ON (1 = 1)', 'S', 15, 'NOOVA', 'FACTURA', 'noov:lImpuestosDetalle', 'noov:DTOFacImpuestosdetallefacturas', NULL, 'ges:detalle')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (264, 'ges:mandatario', 'DOC', '-- Mandatario Gestion Factura

SELECT
       CASE T.Naturaleza
         WHEN ''J'' THEN 1
         WHEN ''N'' THEN 2
         ELSE ''''
       END "noov:Nvman_cper",
       Codidentidad "noov:Nvman_cdoc",
       TRIM(C.Codtercero) || IIF(TRIM(T.Codidentidad) = ''31'', IIF(T.Dv IS NULL, '''', ''-'' || TRIM(T.Dv)), '''') "noov:Nvman_docu",
       T.Email "noov:Nvman_mail",
       T.Direccion "noov:Nvman_dire",
       S.Codigo_Fe "noov:Nvman_regi",
       COALESCE(T.Empresa, '''') "noov:Nvman_nomb",
       COALESCE(T.Nom1, '''') "noov:Nvman_pnom",
       COALESCE(T.Nom2, '''') "noov:Nvman_snom",
       TRIM(COALESCE(T.Apl1, '''')) || '' '' || TRIM(COALESCE(T.Apl2, '''')) "noov:Nvman_apel",
       COALESCE(T.Telefono, '''') "noov:Nvman_ntel"
FROM Tr_Inventario M
JOIN Centros C ON (M.Codcentro = C.Codigo)
LEFT JOIN Terceros T ON (C.Codtercero = T.Codigo)
LEFT JOIN Sociedades S ON (T.Codsociedad = S.Codigo)
WHERE Tipo = :Tipo:
      AND Prefijo = :Prefijo:
      AND Numero = :Numero:
      AND Renglon = :Renglon:
      AND COALESCE(C.Codtercero, '''') <> ''''', 'S', 20, 'NOOVA', 'FACTURA', 'noov:Nvdet_mand', NULL, NULL, 'ges:detalle')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (265, 'ges:impuestos', 'DOC', '-- Impuestos Gestion Factura

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_oper",
       "noov:Nvimp_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base", Redondeo)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo", Redondeo)) "noov:Nvimp_valo"
FROM Pz_Fe_Impuestos_Sum(:Tipo:, :Prefijo:, :Numero:)
JOIN V_Redondeo R ON (1 = 1)    
WHERE "noov:Nvimp_cdia" NOT IN (''07'', ''99'')', 'S', 25, 'NOOVA', 'FACTURA', 'noov:DTOFacImpuestosfacturas', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (266, 'ges:tasa_cambio', 'DOC', '--Tasa Cambio Gestion Factura

SELECT FIRST 1 ''COP'' "noov:Nvtas_sour",
               Codigo "noov:Nvtas_targ",
               Valor "noov:Nvtas_calc",
               Fecha "noov:Nvtas_fech"
FROM Pz_Fe_Trm(:Tipo:, :Prefijo:, :Numero:)
WHERE Codigo <> ''COP''', 'S', 30, 'NOOVA', 'FACTURA', 'noov:TasaCambio', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (267, 'ges:sector_salud', 'DOC', '-- Sector Salud Gestion Factura

SELECT Codreferido Renglon,
       SUBSTRING(Desde FROM 1 FOR 10) "noov:Nvsal_fini",
       SUBSTRING(Hasta FROM 1 FOR 10) "noov:Nvsal_ffin"
FROM Pz_Fe_Vector_Salud(:Tipo:, :Prefijo:, :Numero:, ''GESTION'', :Receptor:) S', 'S', 35, 'NOOVA', 'FACTURA', 'noov:SectorSalud', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (268, 'ges:proveedor', 'DOC', 'SELECT 1
FROM Rdb$Database
WHERE 1 = 0', 'S', 40, 'NOOVA', 'FACTURA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (272, 'ges:transporte1', 'DOC', '-- Transporte 1
 SELECT Atri "noov:Nvfac_atri",
               Natr "noov:Nvfac_natr",
               Vatr "noov:Nvfac_vatr"
        FROM Fe_Vector_Transporte(:Tipo:, :Prefijo:, :Numero:, ''GESTION'')
        WHERE Renglon = :Renglon:
    AND  Natr IN (''01'', ''02'')', 'S', 32, 'NOOVA', 'FACTURA', 'noov:lAdicionalDetalle', 'noov:DTOFacAdicionalesdetallefacturas', NULL, 'ges:detalle')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (273, 'ges:transporte2', 'DOC', '-- Transporte 2
 
SELECT   Atri "noov:Nvfac_atri",
               Natr "noov:Nvfac_natr",
               Vatr "noov:Nvfac_vatr",
               Uatr "noov:Nvfac_uatr",
               Catr "noov:Nvfac_catr"
FROM Pz_Fe_Transporte(:Tipo:, :Prefijo:, :Numero:, :Renglon:)
WHERE  Natr =''03''', 'S', 33, 'NOOVA', 'FACTURA', 'noov:lAdicionalDetalle', 'noov:DTOFacAdicionalesdetallefacturas', NULL, 'ges:detalle')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (274, 'ges:salud_usu', 'DOC', '-- Sector Salud usuario

SELECT Reps "noov:Nvsal_cpsa",
       Mcon "noov:Nvsal_mcon",
       Cobe "noov:Nvsal_cobe",
       '''' "noov:Nvsal_naut",
       Numero_Contrato "noov:Nvsal_ncon",
       Poliza "noov:Nvsal_npol",
       Copago "noov:Nvsal_cpag",
       Cuota_Moderadora "noov:Nvsal_cmod",
       0.00 "noov:Nvsal_crec",
       Pagos_Compartidos "noov:Nvsal_pcom"
FROM Pz_Fe_Vector_Salud_Usu(:Tipo:, :Prefijo:, :Numero:, ''GESTION'', :Receptor:)', 'S', 36, 'NOOVA', 'FACTURA', 'noov:lUsuariosSalud', 'noov:DTOUsuarioSalud', NULL, 'ges:sector_salud')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (281, 'ges:comprobante', 'DOC', '-- Comprobante Gestion Exportacion

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT Nvfac_Orig "noov:Nvfac_orig",
       Nvemp_Nnit "noov:Nvemp_nnit",
       Nvres_Nume "noov:Nvres_nume",
       Nvfac_Tipo "noov:Nvfac_tipo",
       Nvres_Pref "noov:Nvres_pref",
       Nvfac_Nume "noov:Nvfac_nume",
       Nvfac_Fech "noov:Nvfac_fech",
       Nvfac_Venc "noov:Nvfac_venc",
       Nvsuc_Codi "noov:Nvsuc_codi",
       Nvmon_Codi "noov:Nvmon_codi",
       Nvfor_Codi "noov:Nvfor_codi",
       Nvven_Nomb "noov:Nvven_nomb",
       Nvfac_Fpag "noov:Nvfac_fpag",
       Nvfac_Conv "noov:Nvfac_conv",
       Nvcli_Cper "noov:Nvcli_cper",
       Nvcli_Cdoc "noov:Nvcli_cdoc",
       Nvcli_Docu "noov:Nvcli_docu",
       Nvcli_Pais "noov:Nvcli_pais",
       Nvcli_Depa "noov:Nvcli_depa",
       Nvcli_Ciud "noov:Nvcli_ciud",
       Nvcli_Loca "noov:Nvcli_loca",
       Nvcli_Dire "noov:Nvcli_dire",
       Nvcli_Ntel "noov:Nvcli_ntel",
       Nvcli_Regi "noov:Nvcli_regi",
       Nvcli_Fisc "noov:Nvcli_fisc",
       Nvcli_Nomb "noov:Nvcli_nomb",
       Nvcli_Pnom "noov:Nvcli_pnom",
       Nvcli_Snom "noov:Nvcli_snom",
       Nvcli_Apel "noov:Nvcli_apel",
       Nvcli_Mail "noov:Nvcli_mail",
       Nvema_Copi "noov:Nvema_copi",
       Nvfac_Obse "noov:Nvfac_obse",
       Nvfac_Orde "noov:Nvfac_orde",
       Nvfac_Remi "noov:Nvfac_remi",
       Nvfac_Rece "noov:Nvfac_rece",
       Nvfac_Entr "noov:Nvfac_entr",
       Nvfac_Ccos "noov:Nvfac_ccos",
       Nvfac_Cdet "noov:Nvfac_cdet",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Stot, Redondeo)) "noov:Nvfac_stot",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Desc, Redondeo)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Anti, Redondeo)) "noov:Nvfac_anti",
       Nvfac_Carg "noov:Nvfac_carg",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Tota, Redondeo)) "noov:Nvfac_tota",
       (SELECT Valor
        FROM Redondeo_Dian(Com_Total, Redondeo)) "noov:Nvfac_totp",

       (SELECT COALESCE(Valor, 0)
        FROM Redondeo_Dian(Com_Total, Redondeo)) - (SELECT COALESCE(Valor, 0)
                                                    FROM Redondeo_Dian(Nvfac_Stot, Redondeo)) - (SELECT COALESCE(Valor, 0)
                                                                                                 FROM Redondeo_Dian((SELECT SUM(COALESCE(Valor_Red, 0))
                                                                                                                     FROM Pz_Fe_Impuestos_Sum(:Tipo:, :Prefijo:, :Numero:)
                                                                                                                     WHERE "noov:Nvimp_oper" = ''S''), Redondeo)) + (SELECT COALESCE(Valor, 0)
                                                                                                                                                                   FROM Redondeo_Dian(Nvfac_Desc, Redondeo)) "noov:Nvfac_roun",
       Nvfac_Vcop "noov:Nvfac_vcop",
       Nvfac_Timp "noov:Nvfac_timp",

       Nvfac_Coid "noov:Nvfac_coid",
       Nvfac_Obsb "noov:Nvfac_obsb",
       Nvcli_Ncon "noov:Nvcli_ncon",
       Nvfor_Oper "noov:Nvfor_oper"
FROM Pz_Fe_Comprobantes(:Tipo:, :Prefijo:, :Numero:, :Emisor:, :Receptor:, :Clase_Fe:)
JOIN V_Redondeo R ON (1 = 1)', 'S', 5, 'NOOVA', 'EXPORTACION', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (282, 'ges:detalle', 'DOC', '-- Detalle Gestion Exportacion

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT Renglon,
       Consecutivo,
       "noov:Nvpro_codi",
       "noov:Nvpro_nomb",
       "noov:Nvuni_desc",
       "noov:Nvfac_cant",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_valo", Redondeo)) "noov:Nvfac_valo",
       "noov:Nvfac_pdes",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc", Redondeo)) "noov:Nvfac_desc",
       "noov:Nvdet_pref",
       (SELECT Valor
        FROM Redondeo_Dian(Stot_Red, Redondeo)) "noov:Nvfac_stot",
       "noov:Nvimp_cdia",
       "noov:Nvdet_piva",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvdet_viva", Redondeo)) "noov:Nvdet_viva",
       "noov:Nvdet_tcod",
       "noov:Nvpro_cean",
       "noov:Nvdet_entr",
       "noov:Nvuni_quan",
       "noov:Nvdet_nota",
       "noov:Nvdet_padr",
       "noov:Nvdet_marc",
       "noov:Nvdet_mode"
FROM Pz_Fe_Detalle(:Tipo:, :Prefijo:, :Numero:)
JOIN V_Redondeo R ON (1 = 1)', 'S', 10, 'NOOVA', 'EXPORTACION', 'noov:DTOFacDetallefacturas', NULL, 'noov:Nvfac_dcop', NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (283, 'ges:adicional', 'DOC', '-- Adicionales Gestion Exportacion

WITH V_Trm
AS (SELECT Valor AS Trm
    FROM Pz_Fe_Trm(:Tipo:, :Prefijo:, :Numero:))

SELECT 1 "noov:Nvfac_atri",
       (SELECT Valor
        FROM Redondeo_Dian(stot / Trm, 2)) "noov:Nvfac_vatr",
       ''ValorUSD'' "noov:Nvfac_natr"
FROM Pz_Fe_Detalle_Base(:Tipo:, :Prefijo:, :Numero:)
JOIN V_Trm ON (1 = 1)
WHERE Renglon = :Renglon:

UNION ALL

SELECT 2 "noov:Nvfac_atri",
       (SELECT Valor
        FROM Redondeo_Dian(stot, 2)) "noov:Nvfac_vatr",
       ''ValorCOP'' "noov:Nvfac_natr"
FROM Pz_Fe_Detalle_base(:Tipo:, :Prefijo:, :Numero:)
WHERE Renglon = :Renglon:', 'S', 15, 'NOOVA', 'EXPORTACION', 'noov:lAdicionalDetalle', 'noov:DTOFacAdicionalesdetallefacturas', NULL, 'ges:detalle')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (284, 'ges:impuestos', 'DOC', '-- Impuestos Gestion Exportacion

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_oper",
       "noov:Nvimp_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base", Redondeo)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo", Redondeo)) "noov:Nvimp_valo"
FROM Pz_Fe_Impuestos_Sum(:Tipo:, :Prefijo:, :Numero:)
JOIN V_Redondeo R ON (1 = 1)
WHERE "noov:Nvimp_cdia" NOT IN (''07'', ''99'')', 'S', 25, 'NOOVA', 'EXPORTACION', 'noov:DTOFacImpuestosfacturas', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (285, 'ges:tasa_cambio', 'DOC', '-- Tasa Cambio Gestion Exportacion

SELECT FIRST 1 ''COP'' "noov:Nvtas_sour",
               Codigo "noov:Nvtas_targ",
               Valor "noov:Nvtas_calc",
               Fecha "noov:Nvtas_fech"
FROM Pz_Fe_Trm(:Tipo:, :Prefijo:, :Numero:)
WHERE Codigo <> ''COP''', 'S', 30, 'NOOVA', 'EXPORTACION', 'noov:TasaCambio', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (286, 'ges:proveedor', 'DOC', 'SELECT 1
FROM Rdb$Database
WHERE 1 = 0', 'S', 40, 'NOOVA', 'EXPORTACION', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (301, 'ges:comprobante', 'DOC', '-- Comprobante Gestion Contingencia

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT Nvfac_Orig "noov:Nvfac_orig",
       Nvemp_Nnit "noov:Nvemp_nnit",
       Nvres_Nume "noov:Nvres_nume",
       Nvfac_Tipo "noov:Nvfac_tipo",
       Nvres_Pref "noov:Nvres_pref",
       Nvfac_Nume "noov:Nvfac_nume",
       Nvfac_Fech "noov:Nvfac_fech",
       Nvfac_Venc "noov:Nvfac_venc",
       Nvsuc_Codi "noov:Nvsuc_codi",
       Nvmon_Codi "noov:Nvmon_codi",
       Nvfor_Codi "noov:Nvfor_codi",
       Nvven_Nomb "noov:Nvven_nomb",
       Nvfac_Fpag "noov:Nvfac_fpag",
       Nvfac_Conv "noov:Nvfac_conv",
       Nvcli_Cper "noov:Nvcli_cper",
       Nvcli_Cdoc "noov:Nvcli_cdoc",
       Nvcli_Docu "noov:Nvcli_docu",
       Nvcli_Pais "noov:Nvcli_pais",
       Nvcli_Depa "noov:Nvcli_depa",
       Nvcli_Ciud "noov:Nvcli_ciud",
       Nvcli_Loca "noov:Nvcli_loca",
       Nvcli_Dire "noov:Nvcli_dire",
       Nvcli_Ntel "noov:Nvcli_ntel",
       Nvcli_Regi "noov:Nvcli_regi",
       Nvcli_Fisc "noov:Nvcli_fisc",
       Nvcli_Nomb "noov:Nvcli_nomb",
       Nvcli_Pnom "noov:Nvcli_pnom",
       Nvcli_Snom "noov:Nvcli_snom",
       Nvcli_Apel "noov:Nvcli_apel",
       Nvcli_Mail "noov:Nvcli_mail",
       Nvema_Copi "noov:Nvema_copi",
       Nvfac_Obse "noov:Nvfac_obse",
       Nvfac_Orde "noov:Nvfac_orde",
       Nvfac_Remi "noov:Nvfac_remi",
       Nvfac_Rece "noov:Nvfac_rece",
       Nvfac_Entr "noov:Nvfac_entr",
       Nvfac_Ccos "noov:Nvfac_ccos",
       Nvfac_Cdet "noov:Nvfac_cdet",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Stot, Redondeo)) "noov:Nvfac_stot",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Desc, Redondeo)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Anti, Redondeo)) "noov:Nvfac_anti",
       Nvfac_Carg "noov:Nvfac_carg",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Tota, Redondeo)) "noov:Nvfac_tota",
       (SELECT Valor
        FROM Redondeo_Dian(Com_Total, Redondeo)) "noov:Nvfac_totp",

       (SELECT COALESCE(Valor, 0)
        FROM Redondeo_Dian(Com_Total, Redondeo)) - (SELECT COALESCE(Valor, 0)
                                                    FROM Redondeo_Dian(Nvfac_Stot, 2)) - (SELECT COALESCE(Valor, 0)
                                                                                          FROM Redondeo_Dian((SELECT SUM(COALESCE(Valor_Red, 0))
                                                                                                              FROM Pz_Fe_Impuestos_Sum(:Tipo:, :Prefijo:, :Numero:)
                                                                                                              WHERE "noov:Nvimp_oper" = ''S''), Redondeo)) + (SELECT COALESCE(Valor, 0)
                                                                                                                                                            FROM Redondeo_Dian(Nvfac_Desc, Redondeo)) "noov:Nvfac_roun",
       Nvfac_Vcop "noov:Nvfac_vcop",
       Nvfac_Timp "noov:Nvfac_timp",

       Nvfac_Coid "noov:Nvfac_coid",
       Nvfac_Obsb "noov:Nvfac_obsb",
       Nvcli_Ncon "noov:Nvcli_ncon",
       Nvfor_Oper "noov:Nvfor_oper"
FROM Pz_Fe_Comprobantes(:Tipo:, :Prefijo:, :Numero:, :Emisor:, :Receptor:, :Clase_Fe:)
JOIN V_Redondeo R ON (1 = 1)', 'S', 5, 'NOOVA', 'CONTINGENCIA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (302, 'ges:detalle', 'DOC', '-- Detalle Gestion Contingencia

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT Renglon,
       Consecutivo,
       "noov:Nvpro_codi",
       "noov:Nvpro_nomb",
       "noov:Nvuni_desc",
       "noov:Nvfac_cant",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_valo", Redondeo)) "noov:Nvfac_valo",
       "noov:Nvfac_pdes",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc", Redondeo)) "noov:Nvfac_desc",
       "noov:Nvdet_pref",
       (SELECT Valor
        FROM Redondeo_Dian(Stot_Red, Redondeo)) "noov:Nvfac_stot",
       "noov:Nvimp_cdia",
       "noov:Nvdet_piva",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvdet_viva", Redondeo)) "noov:Nvdet_viva",
       "noov:Nvdet_tcod",
       "noov:Nvpro_cean",
       "noov:Nvdet_entr",
       "noov:Nvuni_quan",
       "noov:Nvdet_nota",
       "noov:Nvdet_padr",
       "noov:Nvdet_marc",
       "noov:Nvdet_mode"
FROM Pz_Fe_Detalle(:Tipo:, :Prefijo:, :Numero:)
JOIN V_Redondeo R ON (1 = 1)', 'S', 10, 'NOOVA', 'CONTINGENCIA', 'noov:DTOFacDetallefacturas', NULL, 'noov:Nvfac_dcop', NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (303, 'ges:imp_detalle', 'DOC', '-- Impuesto Detalle Contingencia

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_desc",
       "noov:Nvimp_oper",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base", Redondeo)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo", Redondeo)) "noov:Nvimp_valo"
FROM Pz_Fe_Impuestos_Detalle(:Tipo:, :Prefijo:, :Numero:, :Renglon:)
JOIN V_Redondeo R ON (1 = 1)', 'S', 15, 'NOOVA', 'CONTINGENCIA', 'noov:lImpuestosDetalle', NULL, NULL, 'ges:detalle')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (304, 'ges:mandatario', 'DOC', '-- Mandatario Gestion Contingencia

SELECT
       CASE T.Naturaleza
         WHEN ''J'' THEN 1
         WHEN ''N'' THEN 2
         ELSE ''''
       END "noov:Nvman_cper",
       Codidentidad "noov:Nvman_cdoc",
       TRIM(C.Codtercero) || IIF(TRIM(T.Codidentidad) = ''31'', IIF(T.Dv IS NULL, '''', ''-'' || TRIM(T.Dv)), '''') "noov:Nvman_docu",
       T.Email "noov:Nvman_mail",
       T.Direccion "noov:Nvman_dire",
       S.Codigo_Fe "noov:Nvman_regi",
       COALESCE(T.Empresa, '''') "noov:Nvman_nomb",
       COALESCE(T.Nom1, '''') "noov:Nvman_pnom",
       COALESCE(T.Nom2, '''') "noov:Nvman_snom",
       TRIM(COALESCE(T.Apl1, '''')) || '' '' || TRIM(COALESCE(T.Apl2, '''')) "noov:Nvman_apel",
       COALESCE(T.Telefono, '''') "noov:Nvman_ntel"
FROM Tr_Inventario M
JOIN Centros C ON (M.Codcentro = C.Codigo)
LEFT JOIN Terceros T ON (C.Codtercero = T.Codigo)
LEFT JOIN Sociedades S ON (T.Codsociedad = S.Codigo)
WHERE Tipo = :Tipo:
      AND Prefijo = :Prefijo:
      AND Numero = :Numero:
      AND Renglon = :Renglon:
      AND COALESCE(C.Codtercero, '''') <> ''''', 'S', 20, 'NOOVA', 'CONTINGENCIA', 'noov:Nvdet_mand', NULL, NULL, 'ges:detalle')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (305, 'ges:impuestos', 'DOC', '-- Impuestos Gestion Contingencia

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_oper",
       "noov:Nvimp_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base", Redondeo)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo", Redondeo)) "noov:Nvimp_valo"
FROM Pz_Fe_Impuestos_Sum(:Tipo:, :Prefijo:, :Numero:)
JOIN V_Redondeo R ON (1 = 1)
WHERE "noov:Nvimp_cdia" NOT IN (''07'', ''99'')', 'S', 25, 'NOOVA', 'CONTINGENCIA', 'noov:DTOFacImpuestosfacturas', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (306, 'ges:tasa_cambio', 'DOC', '-- Tasa Cambio Gestion Contingencia

SELECT FIRST 1 ''COP'' "noov:Nvtas_sour",
               Codigo "noov:Nvtas_targ",
               Valor "noov:Nvtas_calc",
               Fecha "noov:Nvtas_fech"
FROM Pz_Fe_Trm(:Tipo:, :Prefijo:, :Numero:)
WHERE Codigo <> ''COP''', 'S', 30, 'NOOVA', 'CONTINGENCIA', 'noov:TasaCambio', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (307, 'ges:sector_salud', 'DOC', '-- Sector Salud Gestion Contingencia

SELECT SUBSTRING(Nvfac_fech FROM 1 FOR 10) "noov:Nvsal_fini",
       SUBSTRING(Nvfac_fech FROM 1 FOR 10) "noov:Nvsal_ffin"
FROM Pz_Fe_Comprobantes(:Tipo:, :Prefijo:, :Numero:, :Emisor:, :Receptor:, :Clase_Fe:)
WHERE Nvfor_oper = ''SS-CUFE''', 'S', 35, 'NOOVA', 'CONTINGENCIA', 'noov:SectorSalud', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (308, 'ges:proveedor', 'DOC', 'SELECT 1
FROM Rdb$Database
WHERE 1 = 0', 'S', 40, 'NOOVA', 'CONTINGENCIA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (321, 'ges:comprobante', 'DOC', '-- Comprobante Gestion Nota Debito

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT Nvfac_Orig "noov:Nvfac_orig",
       Nvemp_Nnit "noov:Nvemp_nnit",
       Nvres_Nume "noov:Nvres_nume",
       Nvfac_Tipo "noov:Nvfac_tipo",
       Nvres_Pref "noov:Nvres_pref",
       Nvfac_Nume "noov:Nvfac_nume",
       Nvfac_Fech "noov:Nvfac_fech",
       Nvfac_Venc "noov:Nvfac_venc",
       Nvsuc_Codi "noov:Nvsuc_codi",
       Nvmon_Codi "noov:Nvmon_codi",
       Nvfor_Codi "noov:Nvfor_codi",
       Nvven_Nomb "noov:Nvven_nomb",
       Nvfac_Fpag "noov:Nvfac_fpag",
       Nvfac_Conv "noov:Nvfac_conv",
       Nvcli_Cper "noov:Nvcli_cper",
       Nvcli_Cdoc "noov:Nvcli_cdoc",
       Nvcli_Docu "noov:Nvcli_docu",
       Nvcli_Pais "noov:Nvcli_pais",
       Nvcli_Depa "noov:Nvcli_depa",
       Nvcli_Ciud "noov:Nvcli_ciud",
       Nvcli_Loca "noov:Nvcli_loca",
       Nvcli_Dire "noov:Nvcli_dire",
       Nvcli_Ntel "noov:Nvcli_ntel",
       Nvcli_Regi "noov:Nvcli_regi",
       Nvcli_Fisc "noov:Nvcli_fisc",
       Nvcli_Nomb "noov:Nvcli_nomb",
       Nvcli_Pnom "noov:Nvcli_pnom",
       Nvcli_Snom "noov:Nvcli_snom",
       Nvcli_Apel "noov:Nvcli_apel",
       Nvcli_Mail "noov:Nvcli_mail",
       Nvema_Copi "noov:Nvema_copi",
       Nvfac_Obse "noov:Nvfac_obse",
       Nvfac_Orde "noov:Nvfac_orde",
       Nvfac_Remi "noov:Nvfac_remi",
       Nvfac_Rece "noov:Nvfac_rece",
       Nvfac_Entr "noov:Nvfac_entr",
       Nvfac_Ccos "noov:Nvfac_ccos",
       Nvfac_Cdet "noov:Nvfac_cdet",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Stot, Redondeo)) "noov:Nvfac_stot",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Desc, Redondeo)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Anti, Redondeo)) "noov:Nvfac_anti",
       Nvfac_Carg "noov:Nvfac_carg",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Tota, Redondeo)) "noov:Nvfac_tota",
       (SELECT Valor
        FROM Redondeo_Dian(Com_Total, Redondeo)) "noov:Nvfac_totp",

       (SELECT COALESCE(Valor, 0)
        FROM Redondeo_Dian(Com_Total, Redondeo)) - (SELECT COALESCE(Valor, 0)
                                                    FROM Redondeo_Dian(Nvfac_Stot, Redondeo)) - (SELECT COALESCE(Valor, 0)
                                                                                                 FROM Redondeo_Dian((SELECT SUM(COALESCE(Valor_Red, 0))
                                                                                                                     FROM Pz_Fe_Impuestos_Sum(:Tipo:, :Prefijo:, :Numero:)
                                                                                                                     WHERE "noov:Nvimp_oper" = ''S''), Redondeo)) + (SELECT COALESCE(Valor, 0)
                                                                                                                                                                   FROM Redondeo_Dian(Nvfac_Desc, Redondeo)) "noov:Nvfac_roun",
       Nvfac_Vcop "noov:Nvfac_vcop",
       Nvfac_Timp "noov:Nvfac_timp",
       Nvfac_Coid "noov:Nvfac_coid",
       Nvfac_Obsb "noov:Nvfac_obsb",
       Nvcli_Ncon "noov:Nvcli_ncon",
       Nvfor_Oper "noov:Nvfor_oper",
       Nvfac_Numb "noov:Nvfac_numb",
       Nvfac_Fecb "noov:Nvfac_fecb",
       Nvfac_Tcru "noov:Nvfac_tcru",
       Nvcon_Codi "noov:Nvcon_codi",
       Nvcon_Desc "noov:Nvcon_desc"
FROM Pz_Fe_Comprobantes(:Tipo:, :Prefijo:, :Numero:, :Emisor:, :Receptor:, :Clase_Fe:)
JOIN V_Redondeo R ON (1 = 1)', 'S', 5, 'NOOVA', 'NOTA DEBITO', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (322, 'ges:detalle', 'DOC', '-- Detalle Gestion Nota Debito

SELECT Renglon,
       Consecutivo,
       "noov:Nvpro_codi",
       "noov:Nvpro_nomb",
       "noov:Nvuni_desc",
       "noov:Nvfac_cant",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_valo", 2)) "noov:Nvfac_valo",
       "noov:Nvfac_pdes",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc", 2)) "noov:Nvfac_desc",
       "noov:Nvdet_pref",
       (SELECT Valor
        FROM Redondeo_Dian(Stot_Red, 2)) "noov:Nvfac_stot",
       "noov:Nvimp_cdia",
       "noov:Nvdet_piva",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvdet_viva", 2)) "noov:Nvdet_viva",
       "noov:Nvdet_tcod",
       "noov:Nvpro_cean",
       "noov:Nvdet_entr",
       "noov:Nvuni_quan",
       "noov:Nvdet_nota",
       "noov:Nvdet_padr",
       "noov:Nvdet_marc",
       "noov:Nvdet_mode"
FROM Pz_Fe_Detalle(:Tipo:, :Prefijo:, :Numero:)', 'S', 10, 'NOOVA', 'NOTA DEBITO', 'noov:DTOFacDetallefacturas', NULL, 'noov:Nvfac_dcop', NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (323, 'ges:imp_detalle', 'DOC', '-- Impuesto Detalle Gestion Nota Debito

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_desc",
       "noov:Nvimp_oper",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base", Redondeo)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo", Redondeo)) "noov:Nvimp_valo"
FROM Pz_Fe_Impuestos_Detalle(:Tipo:, :Prefijo:, :Numero:, :Renglon:)
JOIN V_Redondeo R ON (1 = 1)', 'S', 15, 'NOOVA', 'NOTA DEBITO', 'noov:lImpuestosDetalle', 'noov:DTOFacImpuestosdetallefacturas', NULL, 'ges:detalle')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (324, 'ges:impuestos', 'DOC', '-- Impuestos Gestion Nota Debito

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_oper",
       "noov:Nvimp_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base", Redondeo)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo", Redondeo)) "noov:Nvimp_valo"
FROM Pz_Fe_Impuestos_Sum(:Tipo:, :Prefijo:, :Numero:)
JOIN V_Redondeo R ON (1 = 1)
WHERE "noov:Nvimp_cdia" NOT IN (''07'', ''99'')', 'S', 25, 'NOOVA', 'NOTA DEBITO', 'noov:DTOFacImpuestosfacturas', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (325, 'ges:tasa_cambio', 'DOC', '-- Tasa Cambio Gestion Nota Debito

SELECT FIRST 1 ''COP'' "noov:Nvtas_sour",
               Codigo "noov:Nvtas_targ",
               Valor "noov:Nvtas_calc",
               Fecha "noov:Nvtas_fech"
FROM Pz_Fe_Trm(:Tipo:, :Prefijo:, :Numero:)
WHERE Codigo <> ''COP''', 'S', 30, 'NOOVA', 'NOTA DEBITO', 'noov:TasaCambio', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (326, 'ges:proveedor', 'DOC', 'SELECT 1
FROM Rdb$Database
WHERE 1 = 0', 'S', 40, 'NOOVA', 'NOTA DEBITO', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (341, 'ges:comprobante', 'DOC', '-- Comprobante Gestion Nota Credito

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT Nvfac_Orig "noov:Nvfac_orig",
       Nvemp_Nnit "noov:Nvemp_nnit",
       Nvres_Nume "noov:Nvres_nume",
       Nvfac_Tipo "noov:Nvfac_tipo",
       Nvres_Pref "noov:Nvres_pref",
       Nvfac_Nume "noov:Nvfac_nume",
       Nvfac_Fech "noov:Nvfac_fech",
       Nvfac_Venc "noov:Nvfac_venc",
       Nvsuc_Codi "noov:Nvsuc_codi",
       Nvmon_Codi "noov:Nvmon_codi",
       IIF(Nvfor_Codi = ''13'', ''2'', Nvfor_Codi) "noov:Nvfor_codi",
       Nvven_Nomb "noov:Nvven_nomb",
       Nvfac_Fpag "noov:Nvfac_fpag",
       Nvfac_Conv "noov:Nvfac_conv",
       Nvcli_Cper "noov:Nvcli_cper",
       Nvcli_Cdoc "noov:Nvcli_cdoc",
       Nvcli_Docu "noov:Nvcli_docu",
       Nvcli_Pais "noov:Nvcli_pais",
       Nvcli_Depa "noov:Nvcli_depa",
       Nvcli_Ciud "noov:Nvcli_ciud",
       Nvcli_Loca "noov:Nvcli_loca",
       Nvcli_Dire "noov:Nvcli_dire",
       Nvcli_Ntel "noov:Nvcli_ntel",
       Nvcli_Regi "noov:Nvcli_regi",
       Nvcli_Fisc "noov:Nvcli_fisc",
       Nvcli_Nomb "noov:Nvcli_nomb",
       Nvcli_Pnom "noov:Nvcli_pnom",
       Nvcli_Snom "noov:Nvcli_snom",
       Nvcli_Apel "noov:Nvcli_apel",
       Nvcli_Mail "noov:Nvcli_mail",
       Nvema_Copi "noov:Nvema_copi",
       Nvfac_Obse "noov:Nvfac_obse",
       Nvfac_Orde "noov:Nvfac_orde",
       Nvfac_Remi "noov:Nvfac_remi",
       Nvfac_Rece "noov:Nvfac_rece",
       Nvfac_Entr "noov:Nvfac_entr",
       Nvfac_Ccos "noov:Nvfac_ccos",
       Nvfac_Cdet "noov:Nvfac_cdet",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Stot, Redondeo)) "noov:Nvfac_stot",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Desc, Redondeo)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Anti, Redondeo)) "noov:Nvfac_anti",
       Nvfac_Carg "noov:Nvfac_carg",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Tota, Redondeo)) "noov:Nvfac_tota",
       (SELECT Valor
        FROM Redondeo_Dian(Com_Total, Redondeo)) "noov:Nvfac_totp",

       (SELECT COALESCE(Valor, 0)
        FROM Redondeo_Dian(Com_Total, Redondeo)) - (SELECT COALESCE(Valor, 0)
                                                    FROM Redondeo_Dian(Nvfac_Stot, Redondeo)) - (SELECT COALESCE(Valor, 0)
                                                                                                 FROM Redondeo_Dian((SELECT SUM(COALESCE(Valor_Red, 0))
                                                                                                                     FROM Pz_Fe_Impuestos_Sum(:Tipo:, :Prefijo:, :Numero:)
                                                                                                                     WHERE "noov:Nvimp_oper" = ''S''), Redondeo)) + (SELECT COALESCE(Valor, 0)
                                                                                                                                                                   FROM Redondeo_Dian(Nvfac_Desc, Redondeo)) "noov:Nvfac_roun",
       Nvfac_Vcop "noov:Nvfac_vcop",
       Nvfac_Timp "noov:Nvfac_timp",
       Nvfac_Coid "noov:Nvfac_coid",
       Nvfac_Obsb "noov:Nvfac_obsb",
       Nvcli_Ncon "noov:Nvcli_ncon",
       IIF(TRIM(Nvfor_Oper) = ''SS-CUFE'', ''20'', Nvfor_Oper) "noov:Nvfor_oper",
       Nvfac_Numb "noov:Nvfac_numb",
       Nvfac_Fecb "noov:Nvfac_fecb",
       Nvfac_Tcru "noov:Nvfac_tcru",
       Nvcon_Codi "noov:Nvcon_codi",
       Nvcon_Desc "noov:Nvcon_desc"

FROM Pz_Fe_Comprobantes(:Tipo:, :Prefijo:, :Numero:, :Emisor:, :Receptor:, :Clase_Fe:)
JOIN V_Redondeo R ON (1 = 1)', 'S', 5, 'NOOVA', 'NOTA CREDITO', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (342, 'ges:detalle', 'DOC', '-- Detalle Gestion Nota Credito

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT Renglon,
       Consecutivo,
       "noov:Nvpro_codi",
       "noov:Nvpro_nomb",
       "noov:Nvuni_desc",
       "noov:Nvfac_cant",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_valo", Redondeo)) "noov:Nvfac_valo",
       "noov:Nvfac_pdes",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc", Redondeo)) "noov:Nvfac_desc",
       "noov:Nvdet_pref",
       (SELECT Valor
        FROM Redondeo_Dian(Stot_Red, Redondeo)) "noov:Nvfac_stot",
       "noov:Nvimp_cdia",
       "noov:Nvdet_piva",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvdet_viva", Redondeo)) "noov:Nvdet_viva",
       "noov:Nvdet_tcod",
       "noov:Nvpro_cean",
       "noov:Nvdet_entr",
       "noov:Nvuni_quan",
       "noov:Nvdet_nota",
       "noov:Nvdet_padr",
       "noov:Nvdet_marc",
       "noov:Nvdet_mode"
FROM Pz_Fe_Detalle(:Tipo:, :Prefijo:, :Numero:)
JOIN V_Redondeo R ON (1 = 1)', 'S', 10, 'NOOVA', 'NOTA CREDITO', 'noov:DTOFacDetallefacturas', NULL, 'noov:Nvfac_dcop', '')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (343, 'ges:imp_detalle', 'DOC', '-- Impuesto Detalle Gestion Nota Credito

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_desc",
       "noov:Nvimp_oper",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base", Redondeo)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo", Redondeo)) "noov:Nvimp_valo"
FROM Pz_Fe_Impuestos_Detalle(:Tipo:, :Prefijo:, :Numero:, :Renglon:)
JOIN V_Redondeo R ON (1 = 1)', 'S', 15, 'NOOVA', 'NOTA CREDITO', 'noov:lImpuestosDetalle', 'noov:DTOFacImpuestosdetallefacturas', NULL, 'ges:detalle')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (344, 'ges:impuestos', 'DOC', '-- Impuestos Gestion Nota Credito

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_oper",
       "noov:Nvimp_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base", Redondeo)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo", Redondeo)) "noov:Nvimp_valo"
FROM Pz_Fe_Impuestos_Sum(:Tipo:, :Prefijo:, :Numero:)
JOIN V_Redondeo R ON (1 = 1)
WHERE "noov:Nvimp_cdia" NOT IN (''07'', ''99'')', 'S', 25, 'NOOVA', 'NOTA CREDITO', 'noov:DTOFacImpuestosfacturas', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (345, 'ges:tasa_cambio', 'DOC', '-- Tasa Cambio Gestion Nota Credito

SELECT FIRST 1 ''COP'' "noov:Nvtas_sour",
               Codigo "noov:Nvtas_targ",
               Valor "noov:Nvtas_calc",
               Fecha "noov:Nvtas_fech"
FROM Pz_Fe_Trm(:Tipo:, :Prefijo:, :Numero:)
WHERE Codigo <> ''COP''', 'S', 30, 'NOOVA', 'NOTA CREDITO', 'noov:TasaCambio', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (346, 'ges:sector_salud', 'DOC', '-- Sector Salud Gestion Factura

SELECT Codreferido Renglon,
       SUBSTRING(Desde FROM 1 FOR 10) "noov:Nvsal_fini",
       SUBSTRING(Hasta FROM 1 FOR 10) "noov:Nvsal_ffin"
FROM Pz_Fe_Vector_Salud(:Tipo:, :Prefijo:, :Numero:, ''GESTION'', :Receptor:) S', 'S', 35, 'NOOVA', 'NOTA CREDITO', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (347, 'ges:proveedor', 'DOC', 'SELECT 1
FROM Rdb$Database
WHERE 1 = 0', 'S', 40, 'NOOVA', 'NOTA CREDITO', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (361, 'ges:comprobante', 'DOC', '-- Comprobante Gestion Doc Soporte

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT Nvfac_Orig "noov:Nvfac_orig",
       Nvemp_Nnit "noov:Nvemp_nnit",
       Nvres_Nume "noov:Nvres_nume",
       Nvfac_Tipo "noov:Nvfac_tipo",
       Nvres_Pref "noov:Nvres_pref",
       Nvfac_Nume "noov:Nvfac_nume",
       Nvfac_Fech "noov:Nvfac_fech",
       Nvfac_Venc "noov:Nvfac_venc",
       Nvsuc_Codi "noov:Nvsuc_codi",
       Nvmon_Codi "noov:Nvmon_codi",
       Nvfor_Codi "noov:Nvfor_codi",
       Nvven_Nomb "noov:Nvven_nomb",
       Nvfac_Fpag "noov:Nvfac_fpag",
       Nvfac_Conv "noov:Nvfac_conv",
       Nvema_Copi "noov:Nvema_copi",
       Nvfac_Obse "noov:Nvfac_obse",
       Nvfac_Orde "noov:Nvfac_orde",
       Nvfac_Remi "noov:Nvfac_remi",
       Nvfac_Rece "noov:Nvfac_rece",
       Nvfac_Entr "noov:Nvfac_entr",
       Nvfac_Ccos "noov:Nvfac_ccos",
       Nvfac_Cdet "noov:Nvfac_cdet",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Stot, Redondeo)) "noov:Nvfac_stot",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Desc, Redondeo)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Anti, Redondeo)) "noov:Nvfac_anti",
       Nvfac_Carg "noov:Nvfac_carg",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Tota, Redondeo)) "noov:Nvfac_tota",
       (SELECT Valor
        FROM Redondeo_Dian(Com_Total, Redondeo)) "noov:Nvfac_totp",

       (SELECT COALESCE(Valor, 0)
        FROM Redondeo_Dian(COALESCE(Com_Total, 0), Redondeo)) - (SELECT COALESCE(Valor, 0)
                                                                 FROM Redondeo_Dian(COALESCE(Nvfac_Stot, 0), Redondeo)) - (SELECT COALESCE(Valor, 0)
                                                                                                                           FROM Redondeo_Dian((SELECT SUM(COALESCE(Valor_Red, 0))
                                                                                                                                               FROM Pz_Fe_Impuestos_Sum(:Tipo:, :Prefijo:, :Numero:)
                                                                                                                                               WHERE "noov:Nvimp_oper" = ''S''), Redondeo)) + (SELECT COALESCE(Valor, 0)
                                                                                                                                                                                             FROM Redondeo_Dian(COALESCE(Nvfac_Desc, 0), Redondeo)) "noov:Nvfac_roun",
       Nvfac_Vcop "noov:Nvfac_vcop",
       Nvfac_Timp "noov:Nvfac_timp",

       Nvfac_Coid "noov:Nvfac_coid",
       Nvfac_Obsb "noov:Nvfac_obsb",
       Nvfor_Oper "noov:Nvfor_oper"
FROM Pz_Fe_Comprobantes(:Tipo:, :Prefijo:, :Numero:, :Emisor:, :Receptor:, :Clase_Fe:)
JOIN V_Redondeo R ON (1 = 1)', 'S', 5, 'NOOVA', 'DOCUMENTO SOPORTE', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (362, 'ges:proveedor', 'DOC', '-- Proveedor Doc Soporte

SELECT Nvpro_Cper "noov:Nvpro_cper",
       Nvpro_Cdoc "noov:Nvpro_cdoc",
       Nvpro_Docu "noov:Nvpro_docu",
       Nvpro_Dive "noov:Nvpro_dive",
       Nvpro_Pais "noov:Nvpro_pais",
       Nvpro_Depa "noov:Nvpro_depa",
       Nvpro_Ciud "noov:Nvpro_ciud",
       Nvpro_Loca "noov:Nvpro_loca",
       Nvpro_Dire "noov:Nvpro_dire",
       Nvpro_Ntel "noov:Nvpro_ntel",
       Nvpro_Regi "noov:Nvpro_regi",
       Nvpro_Fisc "noov:Nvpro_fisc",
       Nvpro_Nomb "noov:Nvpro_nomb",
       Nvpro_Pnom "noov:Nvpro_pnom",
       Nvpro_Snom "noov:Nvpro_snom",
       Nvpro_Apel "noov:Nvpro_apel",
       Nvpro_Mail "noov:Nvpro_mail",
       Nvpro_Ncon "noov:Nvpro_ncon"
FROM Pz_Fe_Comprobantes(:Tipo:, :Prefijo:, :Numero:, :Emisor:, :Receptor:, :Clase_Fe:)', 'S', 10, 'NOOVA', 'DOCUMENTO SOPORTE', 'noov:Proveedor', '', NULL, 'ges:comprobante')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (363, 'ges:detalle', 'DOC', '-- Detalle Gestion Doc Soporte

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT Renglon,
       Consecutivo,
       "noov:Nvpro_codi",
       "noov:Nvpro_nomb",
       "noov:Nvuni_desc",
       "noov:Nvfac_cant",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_valo", Redondeo)) "noov:Nvfac_valo",
       "noov:Nvfac_pdes",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc", Redondeo)) "noov:Nvfac_desc",
       "noov:Nvdet_pref",
       (SELECT Valor
        FROM Redondeo_Dian(Stot_Red, Redondeo)) "noov:Nvfac_stot",
       "noov:Nvimp_cdia",
       "noov:Nvdet_piva",
       "noov:Nvdet_viva",
       "noov:Nvdet_tcod",
       "noov:Nvpro_cean",
       "noov:Nvdet_entr",
       "noov:Nvuni_quan",
       "noov:Nvdet_nota",
       "noov:Nvdet_padr",
       "noov:Nvdet_marc",
       "noov:Nvdet_mode"
FROM Pz_Fe_Detalle(:Tipo:, :Prefijo:, :Numero:)
JOIN V_Redondeo R ON (1 = 1)', 'S', 15, 'NOOVA', 'DOCUMENTO SOPORTE', 'noov:DTOFacDetallefacturas', NULL, 'noov:Nvfac_dcop', NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (364, 'ges:impuestos', 'DOC', '-- Impuestos Gestion Documento Soporte

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_oper",
       "noov:Nvimp_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base", Redondeo)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo", Redondeo)) "noov:Nvimp_valo"
FROM Pz_Fe_Impuestos_Sum(:Tipo:, :Prefijo:, :Numero:)
JOIN V_Redondeo R ON (1 = 1)
WHERE "noov:Nvimp_cdia" NOT IN (''07'', ''99'')', 'S', 25, 'NOOVA', 'DOCUMENTO SOPORTE', 'noov:DTOFacImpuestosfacturas', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (365, 'ges:tasa_cambio', 'DOC', '-- Tasa Cambio Gestion Documento Soporte

SELECT FIRST 1 ''COP'' "noov:Nvtas_sour",
               Codigo "noov:Nvtas_targ",
               Valor "noov:Nvtas_calc",
               Fecha "noov:Nvtas_fech"
FROM Pz_Fe_Trm(:Tipo:, :Prefijo:, :Numero:)
WHERE Codigo <> ''COP''', 'S', 30, 'NOOVA', 'DOCUMENTO SOPORTE', 'noov:TasaCambio', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (366, 'ges:sector_salud', 'DOC', 'SELECT 1
FROM Rdb$Database
WHERE 1 = 0', 'S', 35, 'NOOVA', 'DOCUMENTO SOPORTE', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (381, 'ges:comprobante', 'DOC', '-- Comprobante Gestion Nota Ajuste A Ds

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT Nvfac_Orig "noov:Nvfac_orig",
       Nvemp_Nnit "noov:Nvemp_nnit",
       Nvres_Nume "noov:Nvres_nume",
       Nvfac_Tipo "noov:Nvfac_tipo",
       Nvres_Pref "noov:Nvres_pref",
       Nvfac_Nume "noov:Nvfac_nume",
       Nvfac_Fech "noov:Nvfac_fech",
       Nvfac_Venc "noov:Nvfac_venc",
       Nvsuc_Codi "noov:Nvsuc_codi",
       Nvmon_Codi "noov:Nvmon_codi",
       IIF(Nvfor_Codi = ''13'', ''2'', Nvfor_Codi) "noov:Nvfor_codi",
       Nvven_Nomb "noov:Nvven_nomb",
       Nvfac_Fpag "noov:Nvfac_fpag",
       Nvfac_Conv "noov:Nvfac_conv",
       Nvema_Copi "noov:Nvema_copi",
       Nvfac_Obse "noov:Nvfac_obse",
       Nvfac_Orde "noov:Nvfac_orde",
       Nvfac_Remi "noov:Nvfac_remi",
       Nvfac_Rece "noov:Nvfac_rece",
       Nvfac_Entr "noov:Nvfac_entr",
       Nvfac_Ccos "noov:Nvfac_ccos",
       Nvfac_Cdet "noov:Nvfac_cdet",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Stot, Redondeo)) "noov:Nvfac_stot",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Desc, Redondeo)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Anti, Redondeo)) "noov:Nvfac_anti",
       Nvfac_Carg "noov:Nvfac_carg",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Tota, Redondeo)) "noov:Nvfac_tota",
       (SELECT Valor
        FROM Redondeo_Dian(Com_Total, Redondeo)) "noov:Nvfac_totp",

       (SELECT COALESCE(Valor, 0)
        FROM Redondeo_Dian(Com_Total, Redondeo)) - (SELECT COALESCE(Valor, 0)
                                                    FROM Redondeo_Dian(Nvfac_Stot, Redondeo)) - (SELECT COALESCE(Valor, 0)
                                                                                                 FROM Redondeo_Dian((SELECT SUM(COALESCE(Valor_Red, 0))
                                                                                                                     FROM Pz_Fe_Impuestos_Sum(:Tipo:, :Prefijo:, :Numero:)
                                                                                                                     WHERE "noov:Nvimp_oper" = ''S''), Redondeo)) + (SELECT COALESCE(Valor, 0)
                                                                                                                                                                   FROM Redondeo_Dian(Nvfac_Desc, Redondeo)) "noov:Nvfac_roun",
       Nvfac_Vcop "noov:Nvfac_vcop",
       Nvfac_Timp "noov:Nvfac_timp",
       Nvfac_Coid "noov:Nvfac_coid",
       Nvfac_Obsb "noov:Nvfac_obsb",
       Nvcli_Ncon "noov:Nvcli_ncon",
       Nvfor_Oper "noov:Nvfor_oper",
       Nvfac_Numb "noov:Nvfac_numb",
       Nvfac_Fecb "noov:Nvfac_fecb",
       Nvfac_Tcru "noov:Nvfac_tcru",
       Nvcon_Codi "noov:Nvcon_codi",
       Nvcon_Desc "noov:Nvcon_desc"
FROM Pz_Fe_Comprobantes(:Tipo:, :Prefijo:, :Numero:, :Emisor:, :Receptor:, :Clase_Fe:)
JOIN V_Redondeo R ON (1 = 1)', 'S', 5, 'NOOVA', 'NOTA DE AJUSTE A DS', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (382, 'ges:proveedor', 'DOC', '-- Proveedor Nota Ajuste a DS

SELECT Nvpro_Cper "noov:Nvpro_cper",
       Nvpro_Cdoc "noov:Nvpro_cdoc",
       Nvpro_Docu "noov:Nvpro_docu",
       Nvpro_Dive "noov:Nvpro_dive",
       Nvpro_Pais "noov:Nvpro_pais",
       Nvpro_Depa "noov:Nvpro_depa",
       Nvpro_Ciud "noov:Nvpro_ciud",
       Nvpro_Zipc "noov:Nvpro_zipc",
       Nvpro_Loca "noov:Nvpro_loca",
       Nvpro_Dire "noov:Nvpro_dire",
       Nvpro_Ntel "noov:Nvpro_ntel",
       Nvpro_Regi "noov:Nvpro_regi",
       Nvpro_Fisc "noov:Nvpro_fisc",
       Nvpro_Nomb "noov:Nvpro_nomb",
       Nvpro_Pnom "noov:Nvpro_pnom",
       Nvpro_Snom "noov:Nvpro_snom",
       Nvpro_Apel "noov:Nvpro_apel",
       Nvpro_Mail "noov:Nvpro_mail",
       Nvpro_Ncon "noov:Nvpro_ncon"
FROM Pz_Fe_Comprobantes(:Tipo:, :Prefijo:, :Numero:, :Emisor:, :Receptor:, :Clase_Fe:)', 'S', 20, 'NOOVA', 'NOTA DE AJUSTE A DS', 'noov:Proveedor', NULL, NULL, 'ges:comprobante')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (383, 'ges:detalle', 'DOC', '-- Detalle Gestion Nota Ajuste a DS

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT Renglon,
       Consecutivo,
       "noov:Nvpro_codi",
       "noov:Nvpro_nomb",
       "noov:Nvuni_desc",
       "noov:Nvfac_cant",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_valo", Redondeo)) "noov:Nvfac_valo",
       "noov:Nvfac_pdes",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc", Redondeo)) "noov:Nvfac_desc",
       "noov:Nvdet_pref",
       (SELECT Valor
        FROM Redondeo_Dian(Stot_Red, Redondeo)) "noov:Nvfac_stot",
       "noov:Nvimp_cdia",
       "noov:Nvdet_piva",
       "noov:Nvdet_viva",
       "noov:Nvdet_tcod",
       "noov:Nvpro_cean",
       "noov:Nvdet_entr",
       "noov:Nvuni_quan",
       "noov:Nvdet_nota",
       "noov:Nvdet_padr",
       "noov:Nvdet_marc",
       "noov:Nvdet_mode"
FROM Pz_Fe_Detalle(:Tipo:, :Prefijo:, :Numero:)
JOIN V_Redondeo R ON (1 = 1)', 'S', 10, 'NOOVA', 'NOTA DE AJUSTE A DS', 'noov:DTOFacDetallefacturas', NULL, 'noov:Nvfac_dcop', NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (384, 'ges:impuestos', 'DOC', '-- Impuestos Gestion Nota Ajuste a DS

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_oper",
       "noov:Nvimp_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base", Redondeo)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo", Redondeo)) "noov:Nvimp_valo"
FROM Pz_Fe_Impuestos_Sum(:Tipo:, :Prefijo:, :Numero:)
JOIN V_Redondeo R ON (1 = 1)
WHERE "noov:Nvimp_cdia" NOT IN (''07'', ''99'')', 'S', 25, 'NOOVA', 'NOTA DE AJUSTE A DS', 'noov:DTOFacImpuestosfacturas', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (385, 'ges:tasa_cambio', 'DOC', '-- Tasa Cambio Gestion Nota de Ajuste a DS

SELECT FIRST 1 ''COP'' "noov:Nvtas_sour",
               Codigo "noov:Nvtas_targ",
               Valor "noov:Nvtas_calc",
               Fecha "noov:Nvtas_fech"
FROM Pz_Fe_Trm(:Tipo:, :Prefijo:, :Numero:)
WHERE Codigo <> ''COP''', 'S', 20, 'NOOVA', 'NOTA DE AJUSTE A DS', 'noov:TasaCambio', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (386, 'ges:sector_salud', 'DOC', 'SELECT 1
FROM Rdb$Database
WHERE 1 = 0', 'S', 25, 'NOOVA', 'NOTA DE AJUSTE A DS', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1561, 'ges:comprobante', 'DOC', '-- Comprobante Gestion Pos

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT Nvfac_Orig "noov:Nvfac_orig",
       Nvemp_Nnit "noov:Nvemp_nnit",
       Nvres_Nume "noov:Nvres_nume",
       Nvfac_Tipo "noov:Nvfac_tipo",
       Nvres_Pref "noov:Nvres_pref",
       Nvfac_Nume "noov:Nvfac_nume",
       Nvfac_Fech "noov:Nvfac_fech",
       Nvfac_Venc "noov:Nvfac_venc",
       Nvsuc_Codi "noov:Nvsuc_codi",
       Nvmon_Codi "noov:Nvmon_codi",
       Nvfor_Codi "noov:Nvfor_codi",
       Nvven_Nomb "noov:Nvven_nomb",
       Nvfac_Fpag "noov:Nvfac_fpag",
       Nvfac_Conv "noov:Nvfac_conv",
       Nvcli_Cper "noov:Nvcli_cper",
       Nvcli_Cdoc "noov:Nvcli_cdoc",
       Nvcli_Docu "noov:Nvcli_docu",
       Nvcli_Pais "noov:Nvcli_pais",
       Nvcli_Depa "noov:Nvcli_depa",
       Nvcli_Ciud "noov:Nvcli_ciud",
       Nvcli_Loca "noov:Nvcli_loca",
       Nvcli_Dire "noov:Nvcli_dire",
       Nvcli_Ntel "noov:Nvcli_ntel",
       Nvcli_Regi "noov:Nvcli_regi",
       Nvcli_Fisc "noov:Nvcli_fisc",
       Nvcli_Nomb "noov:Nvcli_nomb",
       Nvcli_Pnom "noov:Nvcli_pnom",
       Nvcli_Snom "noov:Nvcli_snom",
       Nvcli_Apel "noov:Nvcli_apel",
       ''N'' "noov:Nvcli_mail",
       Nvema_Copi "noov:Nvema_copi",
       Nvfac_Obse "noov:Nvfac_obse",
       Nvfac_Orde "noov:Nvfac_orde",
       Nvfac_Remi "noov:Nvfac_remi",
       Nvfac_Rece "noov:Nvfac_rece",
       Nvfac_Entr "noov:Nvfac_entr",
       Nvfac_Ccos "noov:Nvfac_ccos",
       Nvfac_Cdet "noov:Nvfac_cdet",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Stot, Redondeo)) "noov:Nvfac_stot",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Desc, Redondeo)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Anti, Redondeo)) "noov:Nvfac_anti",
       Nvfac_Carg "noov:Nvfac_carg",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Tota, Redondeo)) "noov:Nvfac_tota",
       (SELECT Valor
        FROM Redondeo_Dian(Com_Total, Redondeo)) "noov:Nvfac_totp",

       (SELECT COALESCE(Valor, 0)
        FROM Redondeo_Dian(Com_Total, Redondeo)) - (SELECT COALESCE(Valor, 0)
                                                    FROM Redondeo_Dian(Nvfac_Stot, Redondeo)) - (SELECT COALESCE(Valor, 0)
                                                                                                 FROM Redondeo_Dian((SELECT SUM(COALESCE(Valor_Red, 0))
                                                                                                                     FROM Pz_Fe_Impuestos_Sum(:Tipo:, :Prefijo:, :Numero:)
                                                                                                                     WHERE "noov:Nvimp_oper" = ''S''), Redondeo)) + (SELECT COALESCE(Valor, 0)
                                                                                                                                                                   FROM Redondeo_Dian(Nvfac_Desc, Redondeo)) "noov:Nvfac_roun",
       Nvfac_Vcop "noov:Nvfac_vcop",
       Nvfac_Timp "noov:Nvfac_timp",

       Nvfac_Coid "noov:Nvfac_coid",
       Nvfac_Obsb "noov:Nvfac_obsb",
       Nvcli_Ncon "noov:Nvcli_ncon",
       Nvfor_Oper "noov:Nvfor_oper"
FROM Pz_Fe_Comprobantes(:Tipo:, :Prefijo:, :Numero:, :Emisor:, :Receptor:, :Clase_Fe:)
JOIN V_Redondeo R ON (1 = 1)', 'S', 5, 'NOOVA', 'POS ELECTRONICO', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1562, 'ges:detalle', 'DOC', '-- Detalle Gestion Factura

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT Renglon,
       Consecutivo,
       "noov:Nvpro_codi",
       "noov:Nvpro_nomb",
       "noov:Nvuni_desc",
       "noov:Nvfac_cant",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_valo", Redondeo)) "noov:Nvfac_valo",
       "noov:Nvfac_pdes",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc", Redondeo)) "noov:Nvfac_desc",
       "noov:Nvdet_pref",
       (SELECT Valor
        FROM Redondeo_Dian(Stot_Red, Redondeo)) "noov:Nvfac_stot",
       "noov:Nvimp_cdia",
       "noov:Nvdet_piva",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvdet_viva", Redondeo)) "noov:Nvdet_viva",
       "noov:Nvdet_tcod",
       "noov:Nvpro_cean",
       "noov:Nvdet_entr",
       "noov:Nvuni_quan",
       "noov:Nvdet_nota",
       "noov:Nvdet_padr",
       "noov:Nvdet_marc",
       "noov:Nvdet_mode",
       "noov:Nvdet_tran"
FROM Pz_Fe_Detalle(:Tipo:, :Prefijo:, :Numero:)
JOIN V_Redondeo R ON (1 = 1)', 'S', 10, 'NOOVA', 'POS ELECTRONICO', 'noov:DTOFacDetallefacturas', NULL, 'noov:Nvfac_dcop', NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1563, 'ges:imp_detalle', 'DOC', '-- Impuesto Detalle Factura

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_desc",
       "noov:Nvimp_oper",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base", Redondeo)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo", Redondeo)) "noov:Nvimp_valo"
FROM Pz_Fe_Impuestos_Detalle(:Tipo:, :Prefijo:, :Numero:, :Renglon:)
JOIN V_Redondeo R ON (1 = 1)', 'S', 15, 'NOOVA', 'POS ELECTRONICO', 'noov:lImpuestosDetalle', 'noov:DTOFacImpuestosdetallefacturas', NULL, 'ges:detalle')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1564, 'ges:mandatario', 'DOC', '-- Mandatario Gestion Factura

SELECT
       CASE T.Naturaleza
         WHEN ''J'' THEN 1
         WHEN ''N'' THEN 2
         ELSE ''''
       END "noov:Nvman_cper",
       Codidentidad "noov:Nvman_cdoc",
       TRIM(C.Codtercero) || IIF(TRIM(T.Codidentidad) = ''31'', IIF(T.Dv IS NULL, '''', ''-'' || TRIM(T.Dv)), '''') "noov:Nvman_docu",
       T.Email "noov:Nvman_mail",
       T.Direccion "noov:Nvman_dire",
       S.Codigo_Fe "noov:Nvman_regi",
       COALESCE(T.Empresa, '''') "noov:Nvman_nomb",
       COALESCE(T.Nom1, '''') "noov:Nvman_pnom",
       COALESCE(T.Nom2, '''') "noov:Nvman_snom",
       TRIM(COALESCE(T.Apl1, '''')) || '' '' || TRIM(COALESCE(T.Apl2, '''')) "noov:Nvman_apel",
       COALESCE(T.Telefono, '''') "noov:Nvman_ntel"
FROM Tr_Inventario M
JOIN Centros C ON (M.Codcentro = C.Codigo)
LEFT JOIN Terceros T ON (C.Codtercero = T.Codigo)
LEFT JOIN Sociedades S ON (T.Codsociedad = S.Codigo)
WHERE Tipo = :Tipo:
      AND Prefijo = :Prefijo:
      AND Numero = :Numero:
      AND Renglon = :Renglon:
      AND COALESCE(C.Codtercero, '''') <> ''''', 'S', 20, 'NOOVA', 'POS ELECTRONICO', 'noov:Nvdet_mand', NULL, NULL, 'ges:detalle')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1565, 'ges:impuestos', 'DOC', '-- Impuestos Gestion Factura

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_oper",
       "noov:Nvimp_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base", Redondeo)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo", Redondeo)) "noov:Nvimp_valo"
FROM Pz_Fe_Impuestos_Sum(:Tipo:, :Prefijo:, :Numero:)
JOIN V_Redondeo R ON (1 = 1)
WHERE "noov:Nvimp_cdia" NOT IN (''07'', ''99'')', 'S', 25, 'NOOVA', 'POS ELECTRONICO', 'noov:DTOFacImpuestosfacturas', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1566, 'ges:tasa_cambio', 'DOC', '--Tasa Cambio Gestion Factura

SELECT FIRST 1 ''COP'' "noov:Nvtas_sour",
               Codigo "noov:Nvtas_targ",
               Valor "noov:Nvtas_calc",
               Fecha "noov:Nvtas_fech"
FROM Pz_Fe_Trm(:Tipo:, :Prefijo:, :Numero:)
WHERE Codigo <> ''COP''', 'S', 30, 'NOOVA', 'POS ELECTRONICO', 'noov:TasaCambio', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1567, 'ges:sector_salud', 'DOC', '-- Sector Salud Gestion Factura

SELECT Codreferido Renglon,
       SUBSTRING(Desde FROM 1 FOR 10) "noov:Nvsal_fini",
       SUBSTRING(Hasta FROM 1 FOR 10) "noov:Nvsal_ffin"
FROM Pz_Fe_Vector_Salud(:Tipo:, :Prefijo:, :Numero:, ''GESTION'', :Receptor:) S', 'S', 35, 'NOOVA', 'POS ELECTRONICO', 'noov:SectorSalud', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1568, 'ges:comprador', 'DOC', '-- Beneficios Comprador

SELECT Codigo "noov:Codigo",
       Nombresapellidos "noov:NombresApellidos",
       Cant_Puntos "noov:Puntos"
FROM Pz_Fe_Benef_Comprador(:Tipo:, :Prefijo:, :Numero:)', 'S', 40, 'NOOVA', 'POS ELECTRONICO', 'noov:InformacionBeneficiosComprador', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1569, 'ges:caja_venta', 'DOC', '-- Informacion Caja Venta

SELECT Placa "noov:PlacaCaja",
       Ubicacion "noov:UbicacionCaja",
       Cajero "noov:Cajero",
       Tipo_Caja "noov:TipoCaja",
       Codigo_Venta "noov:CodigoVenta",
       (SELECT Valor
        FROM Redondeo_Dian(Subtotal, 2)) "noov:Subtotal"
FROM Pz_Fe_Datos_Caja(:Tipo:, :Prefijo:, :Numero:)
WHERE TRIM(Codigo_Fe) = ''POS ELECTRONICO''', 'S', 45, 'NOOVA', 'POS ELECTRONICO', 'noov:InformacionCajaVenta', NULL, NULL, '')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1570, 'ges:espera_pdf', 'DOC', '-- Espera PDF

SELECT 1  "noov:Nvwai_tiem"
FROM Rdb$Database', 'S', 50, 'NOOVA', 'POS ELECTRONICO', 'noov:Waitpdf', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1571, 'ges:espera_pdf', 'DOC', '-- Espera PDF

SELECT 2880  "noov:Nvwai_tiem"
FROM Rdb$Database', 'S', 50, 'NOOVA', 'FACTURA', 'noov:Waitpdf', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1572, 'ges:espera_pdf', 'DOC', '-- Espera PDF

SELECT 2880  "noov:Nvwai_tiem"
FROM Rdb$Database', 'S', 50, 'NOOVA', 'EXPORTACION', 'noov:Waitpdf', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1573, 'ges:espera_pdf', 'DOC', '-- Espera PDF

SELECT 2880  "noov:Nvwai_tiem"
FROM Rdb$Database', 'S', 50, 'NOOVA', 'CONTINGENCIA', 'noov:Waitpdf', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1574, 'ges:espera_pdf', 'DOC', '-- Espera PDF

SELECT 2880  "noov:Nvwai_tiem"
FROM Rdb$Database', 'S', 50, 'NOOVA', 'NOTA DEBITO', 'noov:Waitpdf', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1575, 'ges:espera_pdf', 'DOC', '-- Espera PDF

SELECT 2880  "noov:Nvwai_tiem"
FROM Rdb$Database', 'S', 50, 'NOOVA', 'NOTA CREDITO', 'noov:Waitpdf', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1576, 'ges:espera_pdf', 'DOC', '-- Espera PDF

SELECT 2880  "noov:Nvwai_tiem"
FROM Rdb$Database', 'S', 50, 'NOOVA', 'DOCUMENTO SOPORTE', 'noov:Waitpdf', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1577, 'ges:espera_pdf', 'DOC', '-- Espera PDF

SELECT 2880  "noov:Nvwai_tiem"
FROM Rdb$Database', 'S', 50, 'NOOVA', 'NOTA DE AJUSTE A DS', 'noov:Waitpdf', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1578, 'ges:comprobante', 'DOC', '-- Comprobante Gestion Nota Credito POS

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT ''E'' "noov:Nvfac_orig",
       Nvemp_Nnit "noov:Nvemp_nnit",
       ''3'' "noov:Nvres_nume",
       ''EC'' "noov:Nvfac_tipo",
       Nvres_Pref "noov:Nvres_pref",
       Nvfac_Nume "noov:Nvfac_nume",
       Nvfac_Fech "noov:Nvfac_fech",
       Nvfac_Venc "noov:Nvfac_venc",
       Nvsuc_Codi "noov:Nvsuc_codi",
       Nvmon_Codi "noov:Nvmon_codi",
       ''23'' "noov:Nvfor_codi",
       Nvven_Nomb "noov:Nvven_nomb",
       Nvfac_Fpag "noov:Nvfac_fpag",
       Nvfac_Conv "noov:Nvfac_conv",
       Nvcli_Cper "noov:Nvcli_cper",
       Nvcli_Cdoc "noov:Nvcli_cdoc",
       Nvcli_Docu "noov:Nvcli_docu",
       Nvcli_Pais "noov:Nvcli_pais",
       Nvcli_Depa "noov:Nvcli_depa",
       Nvcli_Ciud "noov:Nvcli_ciud",
       Nvcli_Loca "noov:Nvcli_loca",
       Nvcli_Dire "noov:Nvcli_dire",
       Nvcli_Ntel "noov:Nvcli_ntel",
       Nvcli_Regi "noov:Nvcli_regi",
       Nvcli_Fisc "noov:Nvcli_fisc",
       Nvcli_Nomb "noov:Nvcli_nomb",
       Nvcli_Pnom "noov:Nvcli_pnom",
       Nvcli_Snom "noov:Nvcli_snom",
       Nvcli_Apel "noov:Nvcli_apel",
       ''N'' "noov:Nvcli_mail",
       Nvema_Copi "noov:Nvema_copi",
       Nvfac_Obse "noov:Nvfac_obse",
       Nvfac_Orde "noov:Nvfac_orde",
       Nvfac_Remi "noov:Nvfac_remi",
       Nvfac_Rece "noov:Nvfac_rece",
       Nvfac_Entr "noov:Nvfac_entr",
       Nvfac_Ccos "noov:Nvfac_ccos",
       Nvfac_Cdet "noov:Nvfac_cdet",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Stot, Redondeo)) "noov:Nvfac_stot",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Desc, Redondeo)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Anti, Redondeo)) "noov:Nvfac_anti",
       Nvfac_Carg "noov:Nvfac_carg",
       (SELECT Valor
        FROM Redondeo_Dian(Nvfac_Tota, Redondeo)) "noov:Nvfac_tota",
       (SELECT Valor
        FROM Redondeo_Dian(Com_Total, Redondeo)) "noov:Nvfac_totp",

       (SELECT COALESCE(Valor, 0)
        FROM Redondeo_Dian(Com_Total, Redondeo)) - (SELECT COALESCE(Valor, 0)
                                                    FROM Redondeo_Dian(Nvfac_Stot, Redondeo)) - (SELECT COALESCE(Valor, 0)
                                                                                                 FROM Redondeo_Dian((SELECT SUM(COALESCE(Valor_Red, 0))
                                                                                                                     FROM Pz_Fe_Impuestos_Sum(:Tipo:, :Prefijo:, :Numero:)
                                                                                                                     WHERE "noov:Nvimp_oper" = ''S''), Redondeo)) + (SELECT COALESCE(Valor, 0)
                                                                                                                                                                   FROM Redondeo_Dian(Nvfac_Desc, Redondeo)) "noov:Nvfac_roun",
       Nvfac_Vcop "noov:Nvfac_vcop",
       Nvfac_Timp "noov:Nvfac_timp",
       Nvfac_Coid "noov:Nvfac_coid",
       Nvfac_Obsb "noov:Nvfac_obsb",
       Nvcli_Ncon "noov:Nvcli_ncon",
       ''20'' "noov:Nvfor_oper",
       Nvfac_Numb "noov:Nvfac_numb",
       Nvfac_Fecb "noov:Nvfac_fecb",
       Nvfac_Tcru "noov:Nvfac_tcru",
       Nvcon_Codi "noov:Nvcon_codi",
       Nvcon_Desc "noov:Nvcon_desc"

FROM Pz_Fe_Comprobantes(:Tipo:, :Prefijo:, :Numero:, :Emisor:, :Receptor:, :Clase_Fe:)
JOIN V_Redondeo R ON (1 = 1)', 'S', 5, 'NOOVA', 'NOTA CREDITO POS', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1579, 'ges:detalle', 'DOC', '-- Detalle Gestion Nota Credito

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT Renglon,
       Consecutivo,
       "noov:Nvpro_codi",
       "noov:Nvpro_nomb",
       "noov:Nvuni_desc",
       "noov:Nvfac_cant",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_valo", Redondeo)) "noov:Nvfac_valo",
       "noov:Nvfac_pdes",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc", Redondeo)) "noov:Nvfac_desc",
       "noov:Nvdet_pref",
       (SELECT Valor
        FROM Redondeo_Dian(Stot_Red, Redondeo)) "noov:Nvfac_stot",
       "noov:Nvimp_cdia",
       "noov:Nvdet_piva",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvdet_viva", Redondeo)) "noov:Nvdet_viva",
       "noov:Nvdet_tcod",
       "noov:Nvpro_cean",
       "noov:Nvdet_entr",
       "noov:Nvuni_quan",
       "noov:Nvdet_nota",
       "noov:Nvdet_padr",
       "noov:Nvdet_marc",
       "noov:Nvdet_mode"
FROM Pz_Fe_Detalle(:Tipo:, :Prefijo:, :Numero:)
JOIN V_Redondeo R ON (1 = 1)', 'S', 10, 'NOOVA', 'NOTA CREDITO POS', 'noov:DTOFacDetallefacturas', NULL, 'noov:Nvfac_dcop', '')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1580, 'ges:imp_detalle', 'DOC', '-- Impuesto Detalle Gestion Nota Credito

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))
SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_desc",
       "noov:Nvimp_oper",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base", Redondeo)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo", Redondeo)) "noov:Nvimp_valo"
FROM Pz_Fe_Impuestos_Detalle(:Tipo:, :Prefijo:, :Numero:, :Renglon:)
JOIN V_Redondeo R ON (1 = 1)', 'S', 15, 'NOOVA', 'NOTA CREDITO POS', 'noov:lImpuestosDetalle', 'noov:DTOFacImpuestosdetallefacturas', NULL, 'ges:detalle')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1581, 'ges:impuestos', 'DOC', '-- Impuestos Gestion Nota Credito

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_oper",
       "noov:Nvimp_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base", Redondeo)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo", Redondeo)) "noov:Nvimp_valo"
FROM Pz_Fe_Impuestos_Sum(:Tipo:, :Prefijo:, :Numero:)
JOIN V_Redondeo R ON (1 = 1)
WHERE "noov:Nvimp_cdia" NOT IN (''07'', ''99'')', 'S', 25, 'NOOVA', 'NOTA CREDITO POS', 'noov:DTOFacImpuestosfacturas', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1582, 'ges:tasa_cambio', 'DOC', '-- Tasa Cambio Gestion Nota Credito

SELECT FIRST 1 ''COP'' "noov:Nvtas_sour",
               Codigo "noov:Nvtas_targ",
               Valor "noov:Nvtas_calc",
               Fecha "noov:Nvtas_fech"
FROM Pz_Fe_Trm(:Tipo:, :Prefijo:, :Numero:)
WHERE Codigo <> ''COP''', 'S', 30, 'NOOVA', 'NOTA CREDITO POS', 'noov:TasaCambio', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1583, 'ges:sector_salud', 'DOC', 'SELECT 1
FROM Rdb$Database
WHERE 1 = 0', 'S', 35, 'NOOVA', 'NOTA CREDITO POS', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1584, 'ges:proveedor', 'DOC', 'SELECT 1
FROM Rdb$Database
WHERE 1 = 0', 'S', 40, 'NOOVA', 'NOTA CREDITO POS', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1586, 'ges:espera_pdf', 'DOC', '-- Espera PDF

SELECT 1  "noov:Nvwai_tiem"
FROM Rdb$Database', 'S', 50, 'NOOVA', 'NOTA CREDITO POS', 'noov:Waitpdf', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1587, 'ges:comprador', 'DOC', 'SELECT 1
FROM Rdb$Database
WHERE 1 = 0', 'S', 40, 'NOOVA', 'FACTURA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1588, 'ges:caja_venta', 'DOC', 'SELECT 1
FROM Rdb$Database
WHERE 1 = 0', 'S', 45, 'NOOVA', 'FACTURA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1589, 'ges:comprador', 'DOC', 'SELECT 1
FROM Rdb$Database
WHERE 1 = 0', 'S', 40, 'NOOVA', 'EXPORTACION', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1590, 'ges:caja_venta', 'DOC', 'SELECT 1
FROM Rdb$Database
WHERE 1 = 0', 'S', 45, 'NOOVA', 'EXPORTACION', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1591, 'ges:comprador', 'DOC', 'SELECT 1
FROM Rdb$Database
WHERE 1 = 0', 'S', 40, 'NOOVA', 'CONTINGENCIA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1592, 'ges:caja_venta', 'DOC', 'SELECT 1
FROM Rdb$Database
WHERE 1 = 0', 'S', 45, 'NOOVA', 'CONTINGENCIA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1593, 'ges:comprador', 'DOC', 'SELECT 1
FROM Rdb$Database
WHERE 1 = 0', 'S', 40, 'NOOVA', 'NOTA DEBITO', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1594, 'ges:caja_venta', 'DOC', 'SELECT 1
FROM Rdb$Database
WHERE 1 = 0', 'S', 45, 'NOOVA', 'NOTA DEBITO', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1595, 'ges:comprador', 'DOC', 'SELECT 1
FROM Rdb$Database
WHERE 1 = 0', 'S', 40, 'NOOVA', 'NOTA CREDITO', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1596, 'ges:caja_venta', 'DOC', 'SELECT 1
FROM Rdb$Database
WHERE 1 = 0', 'S', 45, 'NOOVA', 'NOTA CREDITO', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1597, 'ges:comprador', 'DOC', 'SELECT 1
FROM Rdb$Database
WHERE 1 = 0', 'S', 40, 'NOOVA', 'DOCUMENTO SOPORTE', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1598, 'ges:caja_venta', 'DOC', 'SELECT 1
FROM Rdb$Database
WHERE 1 = 0', 'S', 45, 'NOOVA', 'DOCUMENTO SOPORTE', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1599, 'ges:comprador', 'DOC', 'SELECT 1
FROM Rdb$Database
WHERE 1 = 0', 'S', 40, 'NOOVA', 'NOTA DE AJUSTE A DS', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1600, 'ges:caja_venta', 'DOC', 'SELECT 1
FROM Rdb$Database
WHERE 1 = 0', 'S', 45, 'NOOVA', 'NOTA DE AJUSTE A DS', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1601, 'ges:comprador', 'DOC', 'SELECT 1
FROM Rdb$Database
WHERE 1 = 0', 'S', 45, 'NOOVA', 'NOTA CREDITO POS', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1602, 'ges:caja_venta', 'DOC', 'SELECT 1
FROM Rdb$Database
WHERE 1 = 0', 'S', 45, 'NOOVA', 'NOTA CREDITO POS', NULL, NULL, NULL, NULL)
                        MATCHING (ID);


COMMIT WORK;

/* EXPORTADOS GESTION FIN*/

UPDATE EXPORTADOS SET 
    ACTIVO = 'N'
WHERE (ID = 275);

UPDATE EXPORTADOS SET 
    ACTIVO = 'N'
WHERE (ID = 276);

COMMIT WORK;


/**** INSERTA REPORTES CIERRE DE CJA POR TURNOS ****/

UPDATE OR INSERT INTO REPORTES (CODIGO, NOMBRE, NOTA, ARCHIVO, ADJUNTO, NATIVO, MODULO, PARAMETROS, PUBLICAR, CODGRUPO, VALIDA_TRANSITO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD)
                        VALUES ('ARQG100510', 'CIERRE DE CAJA POR TURNOS -POR UN USUARIO -RANGO FECHA Y HORA', 'Muestra resumen de caja por turnos, con medios de pago agrupados por un usuario (cajero), segun rango de fecha y horario registrado', 'R_AUX_MOV_08A.FR3', NULL, 'S', 'GESTION', 'FECHA_DESDE,HORA_DESDE,FECHA_HASTA,HORA_HASTA,CODUSUARIO', 'N', 'ARQUEO DE CAJA', 'N', 'N', 'LIBRE', 5, 'N', 1)
                      MATCHING (CODIGO);
UPDATE OR INSERT INTO REPORTES (CODIGO, NOMBRE, NOTA, ARCHIVO, ADJUNTO, NATIVO, MODULO, PARAMETROS, PUBLICAR, CODGRUPO, VALIDA_TRANSITO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD)
                        VALUES ('ARQG100520', 'CIERRE DE CAJA POR TURNOS -POR UNA MAQUINA POS -RANGO FECHA Y HORA', 'Muestra resumen de caja por turnos, con medios de pago agrupados por una maquina, segun rango de fecha y horario registrado', 'R_AUX_MOV_09A.FR3', NULL, 'S', 'GESTION', 'FECHA_DESDE,HORA_DESDE,FECHA_HASTA,HORA_HASTA,COMPUTADOR', 'N', 'ARQUEO DE CAJA', 'N', 'N', 'LIBRE', 5, 'N', 1)
                      MATCHING (CODIGO);

COMMIT WORK;

/**** INSERTA ARQUEO - TESORERIA POR UNA CUENTA DE CAJA POS ****/

UPDATE OR INSERT INTO REPORTES (CODIGO, NOMBRE, NOTA, ARCHIVO, ADJUNTO, NATIVO, MODULO, PARAMETROS, PUBLICAR, CODGRUPO,VALIDA_TRANSITO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD)
                        VALUES ('ARQG050500', 'ANEXO ARQUEO - TESORERIA POR UNA CUENTA DE CAJA POS -CON FORMAS DE PAGO', 'Muestra el movimiento de tesoreria detallado por una caja, segun rango de fecha indicado, incluye total de ingresos y egresos, con medios de pago.  **Complemento a la rutina llamada ARQUEOS** ','R_TES_ARQ_04A.FR3', NULL, 'S', 'GESTION', 'FECHA_DESDE,FECHA_HASTA,CAJA', 'N', 'ARQUEO DE CAJA', 'N', 'N','LIBRE', 5, 'N', 1)
                      MATCHING (CODIGO);
COMMIT WORK;


/*  INFORME LOG ENSAMBLES  */
UPDATE OR INSERT INTO INFORMES (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD, DESTINO_FTP, CODREPORTE)
                        VALUES ('LOG010101', 'RELACION LOG DE CAMBIOS TABLA ENSAMBLES', 'Muestra cambios sobre la tabla Ensambes.  Basta con indicar rango de fecha, solo ensamble o por todos', 'SELECT id,
       codreferencia ensamble,
       (SELECT nombre_referencia
        FROM fn_nombre_referencia(codreferencia)) nombre_ensamble,
       codreferencia2 componente,
       (SELECT nombre_referencia
        FROM fn_nombre_referencia(codreferencia2)) nombre_componente,
       cantidad,
       codusuario usuario,
       SUBSTRING(fecha_accion FROM 1 FOR 19) fecha_accion,
       accion
FROM log_ensambles
WHERE fecha_accion >= CAST(:_desde AS DATE) || '' 00:00:00'' AND
      fecha_accion <= CAST(:_hasta AS DATE) || '' 23:59:59'' AND
      trim(codreferencia) LIKE :ensamble
ORDER BY id', 'S', 'HERRAMIENT', 'N', 'AUDITORIAS', 'N', 'LIBRE', 5, 'N', 1, NULL, NULL)
                      MATCHING (CODIGO);


COMMIT WORK;


/**** MODULO GESTION FINAL                               ****/



/************************************************************/
/**** MODULO CONTABLE                                    ****/
/************************************************************/

/* EXPORTADOS CONTABLE  */

DELETE FROM EXPORTADOS
WHERE PT = 'NOOVAC';

UPDATE PTA SET 
    XML_TEMPLATE = '<?xml version="1.0" encoding="UTF-8"?>
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:noov="http://Noova/">
    <soapenv:Header>
        <noov:AuthHeader>
            <!--Usuario base64:-->
            <noov:Username>UsuarioNoova</noov:Username>
            <!--Password base64:-->
            <noov:Password>ClaveNoova</noov:Password>
        </noov:AuthHeader>
    </soapenv:Header>
    <!--971 Contable 06/Mar/2025 v1-->
    <soapenv:Body>
        <noov:SetFacFacturas>
            <noov:dtoFacFacturasInput>
            con:comprobante
            <noov:lDetalle>
                con:detalle
            </noov:lDetalle>
            <noov:lImpuestos>
                con:impuestos
            </noov:lImpuestos>
            con:tasa_cambio
            <noov:Waitpdf>
                <noov:Nvwai_tiem>2880</noov:Nvwai_tiem>
            </noov:Waitpdf>
            </noov:dtoFacFacturasInput>
        </noov:SetFacFacturas>
    </soapenv:Body>
</soapenv:Envelope>'
WHERE (CODIGO = 'NOOVAC');

COMMIT WORK;

UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (461, 'con:comprobante', 'DOC', '-- Comprobante Contable Factura 969 03/Dic/2024 v2

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT "noov:Nvfac_orig",
       "noov:Nvemp_nnit",
       "noov:Nvres_nume",
       "noov:Nvfac_tipo",
       "noov:Nvres_pref",
       "noov:Nvfac_nume",
       "noov:Nvfac_fech",
       "noov:Nvfac_cdet",
       "noov:Nvfac_venc",
       "noov:Nvsuc_codi",
       "noov:Nvmon_codi",
       "noov:Nvfor_codi",
       "noov:Nvven_nomb",
       "noov:Nvfac_fpag",
       "noov:Nvfac_conv",
       "noov:Nvcli_cper",
       "noov:Nvcli_cdoc",
       "noov:Nvcli_docu",
       "noov:Nvcli_pais",
       "noov:Nvcli_depa",
       "noov:Nvcli_ciud",
       "noov:Nvcli_loca",
       "noov:Nvcli_dire",
       "noov:Nvcli_ntel",
       "noov:Nvcli_regi",
       "noov:Nvcli_fisc",
       "noov:Nvcli_nomb",
       "noov:Nvcli_pnom",
       "noov:Nvcli_snom",
       "noov:Nvcli_apel",
       "noov:Nvcli_mail",
       "noov:Nvema_copi",
       "noov:Nvfac_obse",
       "noov:Nvfac_orde",
       "noov:Nvfac_remi",
       "noov:Nvfac_rece",
       "noov:Nvfac_entr",
       "noov:Nvfac_ccos",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_stot", Redondeo)) "noov:Nvfac_stot",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc", Redondeo)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_anti", Redondeo)) "noov:Nvfac_anti",
       "noov:Nvfac_carg",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_tota", Redondeo)) "noov:Nvfac_tota",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_totp", Redondeo)) "noov:Nvfac_totp",
       --Ajuste al redondeo
       --totp
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_totp", Redondeo)) -
       --stot
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_stot", Redondeo)) -
       --Impuestos
       COALESCE((SELECT Valor
                 FROM Redondeo_Dian((SELECT SUM((SELECT Valor
                                                 FROM Redondeo_Dian("noov:Nvimp_valo", Redondeo)))
                                     FROM Pz_Fe_Auto_Imp(:Tipo: , :Prefijo: , :Numero: )
                                     WHERE "noov:Nvimp_oper" = ''S''), Redondeo)), 0) +
       --descuentos
       COALESCE((SELECT Valor
                 FROM Redondeo_Dian("noov:Nvfac_desc", Redondeo)), 0) + COALESCE((SELECT Valor
                                                                           FROM Redondeo_Dian("noov:Nvfac_anti", Redondeo)), 0) "noov:Nvfac_roun",
       "noov:Nvfac_vcop",
       "noov:Nvfac_timp",
       "noov:Nvfac_coid",
       "noov:Nvfac_obsb",
       "noov:Nvcli_ncon",
       '''' "noov:Nvfor_oper"
FROM Pz_Fe_Auto_Comp(:Tipo: , :Prefijo: , :Numero: , :Emisor: , :Receptor: )
JOIN V_Redondeo R ON (1 = 1)', 'S', 5, 'NOOVAC', 'FACTURA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (462, 'con:detalle', 'DOC', '-- Detalle Contable Factura

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT   RENGLON,  Consecutivo,
       "noov:Nvpro_codi",
       "noov:Nvpro_nomb",
       "noov:Nvuni_desc",
       "noov:Nvfac_cant",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_valo", Redondeo)) "noov:Nvfac_valo",
       "noov:Nvfac_pdes",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc", Redondeo)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian(Stot_Red, Redondeo)) "noov:Nvfac_stot",
       "noov:Nvimp_cdia",
       "noov:Nvdet_piva",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvdet_viva", Redondeo)) "noov:Nvdet_viva",
       "noov:Nvdet_tcod",
       "noov:Nvpro_cean",
       "noov:Nvdet_entr",
       "noov:Nvuni_quan",
       "noov:Nvdet_nota",
       "noov:Nvdet_padr",
       "noov:Nvdet_marc",
       "noov:Nvdet_mode",
       "noov:Nvdet_tran"
FROM Pz_Fe_Auto_Det(:Tipo:, :Prefijo:, :Numero:)
JOIN V_Redondeo R ON (1 = 1)', 'S', 10, 'NOOVAC', 'FACTURA', 'noov:DTOFacDetallefacturas', NULL, 'noov:Nvfac_dcop', NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (463, 'con:impuestos', 'DOC', '-- Impuestos Contable Factura

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_oper",
       "noov:Nvimp_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base", Redondeo)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo", Redondeo)) "noov:Nvimp_valo"
FROM Pz_Fe_Auto_Imp(:Tipo:, :Prefijo:, :Numero:)
JOIN V_Redondeo R ON (1 = 1)
WHERE "noov:Nvimp_cdia" NOT IN (''07'', ''99'')', 'S', 15, 'NOOVAC', 'FACTURA', 'noov:DTOFacImpuestosfacturas', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (464, 'con:mandatario', 'DOC', '-- Mandatario CONTABLE FACTURA

SELECT
       CASE T.Naturaleza
         WHEN ''J'' THEN 1
         WHEN ''N'' THEN 2
         ELSE ''''
       END "noov:Nvman_cper",
       Codidentidad "noov:Nvman_cdoc",
       TRIM(C.Codtercero) || IIF(TRIM(T.Codidentidad) = ''31'', IIF(T.Dv IS NULL, '''', ''-'' || TRIM(T.Dv)), '''') "noov:Nvman_docu",
       T.Email "noov:Nvman_mail",
       T.Direccion "noov:Nvman_dire",
       S.Codigo_Fe "noov:Nvman_regi",
       COALESCE(T.Empresa, '''') "noov:Nvman_nomb",
       COALESCE(T.Nom1, '''') "noov:Nvman_pnom",
       COALESCE(T.Nom2, '''') "noov:Nvman_snom",
       TRIM(COALESCE(T.Apl1, '''')) || '' '' || TRIM(COALESCE(T.Apl2, '''')) "noov:Nvman_apel",
       COALESCE(T.Telefono, '''') "noov:Nvman_ntel"
FROM Auto_Comp M
JOIN Centros C ON (M.Codcentro = C.Codigo)
LEFT JOIN Terceros T ON (C.Codtercero = T.Codigo)
LEFT JOIN Sociedades S ON (T.Codsociedad = S.Codigo)
WHERE Tipo = :Tipo:
      AND Prefijo = :Prefijo:
      AND Numero = :Numero:
      AND COALESCE(C.Codtercero, '''') <> ''''', 'S', 20, 'NOOVAC', 'FACTURA', 'noov:Nvdet_mand', NULL, NULL, 'con:detalle')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (465, 'con:tasa_cambio', 'DOC', '--Tasa Cambio Contable Factura

SELECT FIRST 1  ''COP'' "noov:Nvtas_sour",
               Codigo "noov:Nvtas_targ",
               Valor "noov:Nvtas_calc",
               Fecha "noov:Nvtas_fech"
FROM Pz_Fe_Auto_Trm(:Tipo:, :Prefijo:, :Numero:)
WHERE Codigo <> ''COP''', 'S', 25, 'NOOVAC', 'FACTURA', 'noov:TasaCambio', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (466, 'con:transporte1', 'DOC', '-- Vector Transporte

SELECT "noov:Nvfac_atri",
       "noov:Nvfac_natr",
       "noov:Nvfac_vatr"
FROM Pz_Transporte(:Tipo:, :Prefijo:, :Numero:)
WHERE "noov:Nvfac_natr" IN (''01'', ''02'')
  AND Renglon = :Renglon:', 'S', 26, 'NOOVAC', 'FACTURA', 'noov:lAdicionalDetalle', 'noov:DTOFacAdicionalesdetallefacturas', NULL, 'con:detalle')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (467, 'con:transporte2', 'DOC', '-- Vector Transporte

SELECT "noov:Nvfac_atri",
       "noov:Nvfac_natr",
       "noov:Nvfac_vatr",
       "noov:Nvfac_uatr",
       "noov:Nvfac_catr"
FROM Pz_Transporte(:Tipo:, :Prefijo:, :Numero:)
WHERE "noov:Nvfac_natr" =''03''
  AND Renglon = :Renglon:', 'S', 27, 'NOOVAC', 'FACTURA', 'noov:lAdicionalDetalle', 'noov:DTOFacAdicionalesdetallefacturas', NULL, 'con:detalle')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (481, 'con:comprobante', 'DOC', '-- Comprobante Contable Exportacion

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT "noov:Nvfac_orig",
       "noov:Nvemp_nnit",
       "noov:Nvres_nume",
       "noov:Nvfac_tipo",
       "noov:Nvres_pref",
       "noov:Nvfac_nume",
       "noov:Nvfac_fech",
       "noov:Nvfac_cdet",
       "noov:Nvfac_venc",
       "noov:Nvsuc_codi",
       "noov:Nvmon_codi",
       "noov:Nvfor_codi",
       "noov:Nvven_nomb",
       "noov:Nvfac_fpag",
       "noov:Nvfac_conv",
       "noov:Nvcli_cper",
       "noov:Nvcli_cdoc",
       "noov:Nvcli_docu",
       "noov:Nvcli_pais",
       "noov:Nvcli_depa",
       "noov:Nvcli_ciud",
       "noov:Nvcli_loca",
       "noov:Nvcli_dire",
       "noov:Nvcli_ntel",
       "noov:Nvcli_regi",
       "noov:Nvcli_fisc",
       "noov:Nvcli_nomb",
       "noov:Nvcli_pnom",
       "noov:Nvcli_snom",
       "noov:Nvcli_apel",
       "noov:Nvcli_mail",
       "noov:Nvema_copi",
       "noov:Nvfac_obse",
       "noov:Nvfac_orde",
       "noov:Nvfac_remi",
       "noov:Nvfac_rece",
       "noov:Nvfac_entr",
       "noov:Nvfac_ccos",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_stot", Redondeo)) "noov:Nvfac_stot",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc", Redondeo)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_anti", Redondeo)) "noov:Nvfac_anti",
       "noov:Nvfac_carg",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_tota", Redondeo)) "noov:Nvfac_tota",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_totp", Redondeo)) "noov:Nvfac_totp",
       --Ajuste al redondeo
       --totp
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_totp", Redondeo)) -
       --stot
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_stot", Redondeo)) -
       --Impuestos
       COALESCE((SELECT Valor
                 FROM Redondeo_Dian((SELECT SUM("noov:Nvimp_valo")
                                     FROM Pz_Fe_Auto_Imp(:Tipo:, :Prefijo:, :Numero:)
                                     WHERE "noov:Nvimp_oper" = ''S''), Redondeo)), 0) +
       --descuentos
       COALESCE((SELECT Valor
                 FROM Redondeo_Dian("noov:Nvfac_desc", Redondeo)), 0) + COALESCE((SELECT Valor
                                                                                  FROM Redondeo_Dian("noov:Nvfac_anti", Redondeo)), 0) "noov:Nvfac_roun",
       "noov:Nvfac_vcop",
       "noov:Nvfac_timp",
       "noov:Nvfac_coid",
       "noov:Nvfac_obsb",
       "noov:Nvcli_ncon",
       '''' "noov:Nvfor_oper"
FROM Pz_Fe_Auto_Comp(:Tipo:, :Prefijo:, :Numero:, :Emisor:, :Receptor:)
JOIN V_Redondeo R ON (1 = 1)', 'S', 5, 'NOOVAC', 'EXPORTACION', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (482, 'con:detalle', 'DOC', '-- Detalle Contable Exportacion

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT Consecutivo,
       "noov:Nvpro_codi",
       "noov:Nvpro_nomb",
       "noov:Nvuni_desc",
       "noov:Nvfac_cant",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_valo", Redondeo)) "noov:Nvfac_valo",
       "noov:Nvfac_pdes",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc", Redondeo)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian(Stot_Red, Redondeo)) "noov:Nvfac_stot",
       "noov:Nvimp_cdia",
       "noov:Nvdet_piva",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvdet_viva", Redondeo)) "noov:Nvdet_viva",
       "noov:Nvdet_tcod",
       "noov:Nvpro_cean",
       "noov:Nvdet_entr",
       "noov:Nvuni_quan",
       "noov:Nvdet_nota",
       "noov:Nvdet_padr",
       "noov:Nvdet_marc",
       "noov:Nvdet_mode"
FROM Pz_Fe_Auto_Det(:Tipo:, :Prefijo:, :Numero:)
JOIN V_Redondeo R ON (1 = 1)', 'S', 10, 'NOOVAC', 'EXPORTACION', 'noov:DTOFacDetallefacturas', NULL, 'noov:Nvfac_dcop', NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (483, 'con:impuestos', 'DOC', '--Impuestos CONTABLE EXPORTACION

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_oper",
       "noov:Nvimp_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base", Redondeo)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo", Redondeo)) "noov:Nvimp_valo"
FROM Pz_Fe_Auto_Imp(:Tipo:, :Prefijo:, :Numero:)
JOIN V_Redondeo R ON (1 = 1)
WHERE "noov:Nvimp_cdia" NOT IN (''07'', ''99'')', 'S', 15, 'NOOVAC', 'EXPORTACION', 'noov:DTOFacImpuestosfacturas', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (484, 'con:tasa_cambio', 'DOC', '--Tasa Cambio CONTABLE EXPORTACION

SELECT FIRST 1  ''COP'' "noov:Nvtas_sour",
               Codigo "noov:Nvtas_targ",
               Valor "noov:Nvtas_calc",
               Fecha "noov:Nvtas_fech"
FROM Pz_Fe_Auto_Trm(:Tipo:, :Prefijo:, :Numero:)
WHERE Codigo <> ''COP''', 'S', 20, 'NOOVAC', 'EXPORTACION', 'noov:TasaCambio', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (501, 'con:comprobante', 'DOC', '--Comprobante CONTABLE CONTINGENCIA

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT "noov:Nvfac_orig",
       "noov:Nvemp_nnit",
       "noov:Nvres_nume",
       "noov:Nvfac_tipo",
       "noov:Nvres_pref",
       "noov:Nvfac_nume",
       "noov:Nvfac_fech",
       "noov:Nvfac_cdet",
       "noov:Nvfac_venc",
       "noov:Nvsuc_codi",
       "noov:Nvmon_codi",
       "noov:Nvfor_codi",
       "noov:Nvven_nomb",
       "noov:Nvfac_fpag",
       "noov:Nvfac_conv",
       "noov:Nvcli_cper",
       "noov:Nvcli_cdoc",
       "noov:Nvcli_docu",
       "noov:Nvcli_pais",
       "noov:Nvcli_depa",
       "noov:Nvcli_ciud",
       "noov:Nvcli_loca",
       "noov:Nvcli_dire",
       "noov:Nvcli_ntel",
       "noov:Nvcli_regi",
       "noov:Nvcli_fisc",
       "noov:Nvcli_nomb",
       "noov:Nvcli_pnom",
       "noov:Nvcli_snom",
       "noov:Nvcli_apel",
       "noov:Nvcli_mail",
       "noov:Nvema_copi",
       "noov:Nvfac_obse",
       "noov:Nvfac_orde",
       "noov:Nvfac_remi",
       "noov:Nvfac_rece",
       "noov:Nvfac_entr",
       "noov:Nvfac_ccos",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_stot", Redondeo)) "noov:Nvfac_stot",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc", Redondeo)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_anti", Redondeo)) "noov:Nvfac_anti",
       "noov:Nvfac_carg",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_tota", Redondeo)) "noov:Nvfac_tota",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_totp", Redondeo)) "noov:Nvfac_totp",
       --Ajuste al redondeo
       --totp
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_totp", Redondeo)) -
       --stot
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_stot", Redondeo)) -
       --Impuestos
       COALESCE((SELECT Valor
                 FROM Redondeo_Dian((SELECT SUM("noov:Nvimp_valo")
                                     FROM Pz_Fe_Auto_Imp(:Tipo:, :Prefijo:, :Numero:)
                                     WHERE "noov:Nvimp_oper" = ''S''), Redondeo)), 0) +
       --descuentos
       COALESCE((SELECT Valor
                 FROM Redondeo_Dian("noov:Nvfac_desc", Redondeo)), 0) + COALESCE((SELECT Valor
                                                                                  FROM Redondeo_Dian("noov:Nvfac_anti", Redondeo)), 0) "noov:Nvfac_roun",
       "noov:Nvfac_vcop",
       "noov:Nvfac_timp",
       "noov:Nvfac_coid",
       "noov:Nvfac_obsb",
       "noov:Nvcli_ncon",
       '''' "noov:Nvfor_oper"
FROM Pz_Fe_Auto_Comp(:Tipo:, :Prefijo:, :Numero:, :Emisor:, :Receptor:)
JOIN V_Redondeo R ON (1 = 1)', 'S', 5, 'NOOVAC', 'CONTINGENCIA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (502, 'con:detalle', 'DOC', '--Detalle CONTABLE CONTINGENCIA

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT Consecutivo,
       "noov:Nvpro_codi",
       "noov:Nvpro_nomb",
       "noov:Nvuni_desc",
       "noov:Nvfac_cant",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_valo", Redondeo)) "noov:Nvfac_valo",
       "noov:Nvfac_pdes",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc", Redondeo)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian(Stot_Red, Redondeo)) "noov:Nvfac_stot",
       "noov:Nvimp_cdia",
       "noov:Nvdet_piva",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvdet_viva", Redondeo)) "noov:Nvdet_viva",
       "noov:Nvdet_tcod",
       "noov:Nvpro_cean",
       "noov:Nvdet_entr",
       "noov:Nvuni_quan",
       "noov:Nvdet_nota",
       "noov:Nvdet_padr",
       "noov:Nvdet_marc",
       "noov:Nvdet_mode"
FROM Pz_Fe_Auto_Det(:Tipo:, :Prefijo:, :Numero:)
JOIN V_Redondeo R ON (1 = 1)', 'S', 10, 'NOOVAC', 'CONTINGENCIA', 'noov:DTOFacDetallefacturas', NULL, 'noov:Nvfac_dcop', NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (503, 'con:impuestos', 'DOC', '--Impuestos CONTABLE CONTINGENCIA

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_oper",
       "noov:Nvimp_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base", Redondeo)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo", Redondeo)) "noov:Nvimp_valo"
FROM Pz_Fe_Auto_Imp(:Tipo:, :Prefijo:, :Numero:)
JOIN V_Redondeo R ON (1 = 1)
WHERE "noov:Nvimp_cdia" NOT IN (''07'', ''99'')', 'S', 15, 'NOOVAC', 'CONTINGENCIA', 'noov:DTOFacImpuestosfacturas', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (504, 'con:mandatario', 'DOC', '-- Mandatario CONTABLE CONTINGENCIA

SELECT
       CASE T.Naturaleza
         WHEN ''J'' THEN 1
         WHEN ''N'' THEN 2
         ELSE ''''
       END "noov:Nvman_cper",
       Codidentidad "noov:Nvman_cdoc",
       TRIM(C.Codtercero) || IIF(TRIM(T.Codidentidad) = ''31'', IIF(T.Dv IS NULL, '''', ''-'' || TRIM(T.Dv)), '''') "noov:Nvman_docu",
       T.Email "noov:Nvman_mail",
       T.Direccion "noov:Nvman_dire",
       S.Codigo_Fe "noov:Nvman_regi",
       COALESCE(T.Empresa, '''') "noov:Nvman_nomb",
       COALESCE(T.Nom1, '''') "noov:Nvman_pnom",
       COALESCE(T.Nom2, '''') "noov:Nvman_snom",
       TRIM(COALESCE(T.Apl1, '''')) || '' '' || TRIM(COALESCE(T.Apl2, '''')) "noov:Nvman_apel",
       COALESCE(T.Telefono, '''') "noov:Nvman_ntel"
FROM Auto_Comp M
JOIN Centros C ON (M.Codcentro = C.Codigo)
LEFT JOIN Terceros T ON (C.Codtercero = T.Codigo)
LEFT JOIN Sociedades S ON (T.Codsociedad = S.Codigo)
WHERE Tipo = :Tipo:
      AND Prefijo = :Prefijo:
      AND Numero = :Numero:
      AND COALESCE(C.Codtercero, '''') <> ''''', 'S', 20, 'NOOVAC', 'CONTINGENCIA', 'noov:Nvdet_mand', NULL, NULL, 'con:detalle')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (521, 'con:comprobante', 'DOC', '-- Comprobante Contable Nota Debito

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT "noov:Nvfac_orig",
       "noov:Nvemp_nnit",
       "noov:Nvres_nume",
       "noov:Nvfac_tipo",
       "noov:Nvres_pref",
       "noov:Nvfac_nume",
       "noov:Nvfac_fech",
       "noov:Nvfac_cdet",
       "noov:Nvfac_venc",
       "noov:Nvsuc_codi",
       "noov:Nvmon_codi",
       "noov:Nvfor_codi",
       "noov:Nvven_nomb",
       "noov:Nvfac_fpag",
       "noov:Nvfac_conv",
       "noov:Nvcli_cper",
       "noov:Nvcli_cdoc",
       "noov:Nvcli_docu",
       "noov:Nvcli_pais",
       "noov:Nvcli_depa",
       "noov:Nvcli_ciud",
       "noov:Nvcli_loca",
       "noov:Nvcli_dire",
       "noov:Nvcli_ntel",
       "noov:Nvcli_regi",
       "noov:Nvcli_fisc",
       "noov:Nvcli_nomb",
       "noov:Nvcli_pnom",
       "noov:Nvcli_snom",
       "noov:Nvcli_apel",
       "noov:Nvcli_mail",
       "noov:Nvema_copi",
       "noov:Nvfac_obse",
       "noov:Nvfac_orde",
       "noov:Nvfac_remi",
       "noov:Nvfac_rece",
       "noov:Nvfac_entr",
       "noov:Nvfac_ccos",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_stot", Redondeo)) "noov:Nvfac_stot",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc", Redondeo)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_anti", Redondeo)) "noov:Nvfac_anti",
       "noov:Nvfac_carg",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_tota", Redondeo)) "noov:Nvfac_tota",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_totp", Redondeo)) "noov:Nvfac_totp",
       --Ajuste al redondeo

       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_totp", Redondeo)) -
       --stot
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_stot", Redondeo)) -
       --Impuestos
       COALESCE((SELECT Valor
                 FROM Redondeo_Dian((SELECT SUM("noov:Nvimp_valo")
                                     FROM Pz_Fe_Auto_Imp(:Tipo:, :Prefijo:, :Numero:)
                                     WHERE "noov:Nvimp_oper" = ''S''), Redondeo)), 0) +
       --descuentos
       COALESCE((SELECT Valor
                 FROM Redondeo_Dian(("noov:Nvfac_desc" + (SELECT SUM(IIF("noov:Nvfac_pdes" = 100, "noov:Nvfac_desc", 0))
                                                          FROM Pz_Fe_Detalle(:Tipo:, :Prefijo:, :Numero:))), Redondeo)), 0) "noov:Nvfac_roun",
       "noov:Nvfac_vcop",
       "noov:Nvfac_timp",
       "noov:Nvfac_coid",
       "noov:Nvfac_numb",
       "noov:Nvfac_fecb",
       "noov:Nvfac_obsb",
       "noov:Nvcli_ncon",
       "noov:Nvfac_tcru",
       "noov:Nvcon_codi",
       "noov:Nvcon_desc"
FROM Pz_Fe_Auto_Comp(:Tipo:, :Prefijo:, :Numero:, :Emisor:, :Receptor:)
JOIN V_Redondeo R ON (1 = 1)', 'S', 5, 'NOOVAC', 'NOTA DEBITO', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (522, 'con:detalle', 'DOC', '-- Detalle Contable Nota Debito

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT Consecutivo,
       "noov:Nvpro_codi",
       "noov:Nvpro_nomb",
       "noov:Nvuni_desc",
       "noov:Nvfac_cant",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_valo", Redondeo)) "noov:Nvfac_valo",
       "noov:Nvfac_pdes",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc", Redondeo)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian(Stot_Red, Redondeo)) "noov:Nvfac_stot",
       "noov:Nvimp_cdia",
       "noov:Nvdet_piva",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvdet_viva", Redondeo)) "noov:Nvdet_viva",
       "noov:Nvdet_tcod",
       "noov:Nvpro_cean",
       "noov:Nvdet_entr",
       "noov:Nvuni_quan",
       "noov:Nvdet_nota",
       "noov:Nvdet_padr",
       "noov:Nvdet_marc",
       "noov:Nvdet_mode"
FROM Pz_Fe_Auto_Det(:Tipo:, :Prefijo:, :Numero:)
JOIN V_Redondeo R ON (1 = 1)', 'S', 10, 'NOOVAC', 'NOTA DEBITO', 'noov:DTOFacDetallefacturas', NULL, 'noov:Nvfac_dcop', NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (523, 'con:impuestos', 'DOC', '-- Impuestos Contable Nota Debito

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_oper",
       "noov:Nvimp_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base", Redondeo)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo", Redondeo)) "noov:Nvimp_valo"
FROM Pz_Fe_Auto_Imp(:Tipo:, :Prefijo:, :Numero:)
JOIN V_Redondeo R ON (1 = 1)
WHERE "noov:Nvimp_cdia" NOT IN (''07'', ''99'')', 'S', 15, 'NOOVAC', 'NOTA DEBITO', 'noov:DTOFacImpuestosfacturas', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (524, 'con:tasa_cambio', 'DOC', '-- Tasa Cambio Contable Nota Debito

SELECT FIRST 1  ''COP'' "noov:Nvtas_sour",
                Codigo "noov:Nvtas_targ",
               Valor "noov:Nvtas_calc",
               Fecha "noov:Nvtas_fech"
FROM Pz_Fe_Auto_Trm(:Tipo:, :Prefijo:, :Numero:)
WHERE Codigo <> ''COP''', 'S', 20, 'NOOVAC', 'NOTA DEBITO', 'noov:TasaCambio', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (525, 'con:proveedor', 'DOC', '-- Proveedor Doc Soporte

SELECT "noov:Nvpro_cper",
       "noov:Nvpro_cdoc",
       "noov:Nvpro_docu",
       "noov:Nvpro_dive",
       "noov:Nvpro_pais",
       "noov:Nvpro_depa",
       "noov:Nvpro_ciud",
       "noov:Nvpro_loca",
       "noov:Nvpro_dire",
       "noov:Nvpro_ntel",
       "noov:Nvpro_regi",
       "noov:Nvpro_fisc",
       "noov:Nvpro_nomb",
       "noov:Nvpro_pnom",
       "noov:Nvpro_snom",
       "noov:Nvpro_apel",
       "noov:Nvpro_mail",
       "noov:Nvpro_ncon"
FROM Pz_Fe_Auto_Comp(:Tipo:, :Prefijo:, :Numero:, :Emisor:, :Receptor:)
WHERE "noov:Nvpro_docu" <> ''''', 'S', 40, 'NOOVAC', 'NOTA DEBITO', 'noov:Proveedor', NULL, NULL, 'con:comprobante')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (541, 'con:comprobante', 'DOC', '-- Comprobante Contable Nota Credito

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT "noov:Nvfac_orig",
       "noov:Nvemp_nnit",
       "noov:Nvres_nume",
       "noov:Nvfac_tipo",
       "noov:Nvres_pref",
       "noov:Nvfac_nume",
       "noov:Nvfac_fech",
       "noov:Nvfac_cdet",
       "noov:Nvfac_venc",
       "noov:Nvsuc_codi",
       "noov:Nvmon_codi",
       IIF("noov:Nvfor_codi" = ''13'', ''2'', "noov:Nvfor_codi") "noov:Nvfor_codi",
       "noov:Nvven_nomb",
       "noov:Nvfac_fpag",
       "noov:Nvfac_conv",
       "noov:Nvcli_cper",
       "noov:Nvcli_cdoc",
       "noov:Nvcli_docu",
       "noov:Nvcli_pais",
       "noov:Nvcli_depa",
       "noov:Nvcli_ciud",
       "noov:Nvcli_loca",
       "noov:Nvcli_dire",
       "noov:Nvcli_ntel",
       "noov:Nvcli_regi",
       "noov:Nvcli_fisc",
       "noov:Nvcli_nomb",
       "noov:Nvcli_pnom",
       "noov:Nvcli_snom",
       "noov:Nvcli_apel",
       "noov:Nvcli_mail",
       "noov:Nvema_copi",
       "noov:Nvfac_obse",
       "noov:Nvfac_orde",
       "noov:Nvfac_remi",
       "noov:Nvfac_rece",
       "noov:Nvfac_entr",
       "noov:Nvfac_ccos",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_stot", Redondeo)) "noov:Nvfac_stot",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc", Redondeo)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_anti", Redondeo)) "noov:Nvfac_anti",
       "noov:Nvfac_carg",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_tota", Redondeo)) "noov:Nvfac_tota",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_totp", Redondeo)) "noov:Nvfac_totp",
       --Ajuste al redondeo

       --totp
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_totp", Redondeo)) -
       --stot
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_stot", Redondeo)) -
       --Impuestos
       COALESCE((SELECT Valor
                 FROM Redondeo_Dian((SELECT SUM("noov:Nvimp_valo")
                                     FROM Pz_Fe_Auto_Imp(:Tipo:, :Prefijo:, :Numero:)
                                     WHERE "noov:Nvimp_oper" = ''S''), Redondeo)), 0) +
       --descuentos
       COALESCE((SELECT Valor
                 FROM Redondeo_Dian("noov:Nvfac_desc", Redondeo)), 0) + COALESCE((SELECT Valor
                                                                                  FROM Redondeo_Dian("noov:Nvfac_anti", Redondeo)), 0) "noov:Nvfac_roun",
       "noov:Nvfac_vcop",
       "noov:Nvfac_timp",
       "noov:Nvfac_coid",
       "noov:Nvfac_numb",
       "noov:Nvfac_fecb",
       "noov:Nvfac_obsb",
       "noov:Nvcli_ncon",
       "noov:Nvfac_tcru",
       "noov:Nvcon_codi",
       "noov:Nvcon_desc"
FROM Pz_Fe_Auto_Comp(:Tipo:, :Prefijo:, :Numero:, :Emisor:, :Receptor:)
JOIN V_Redondeo R ON (1 = 1)', 'S', 5, 'NOOVAC', 'NOTA CREDITO', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (542, 'con:detalle', 'DOC', '-- Detalle Contable Nota Credito

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))
SELECT Consecutivo,
       "noov:Nvpro_codi",
       "noov:Nvpro_nomb",
       "noov:Nvuni_desc",
       "noov:Nvfac_cant",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_valo", Redondeo)) "noov:Nvfac_valo",
       "noov:Nvfac_pdes",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc", Redondeo)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian(Stot_Red, Redondeo)) "noov:Nvfac_stot",
       "noov:Nvimp_cdia",
       "noov:Nvdet_piva",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvdet_viva", Redondeo)) "noov:Nvdet_viva",
       "noov:Nvdet_tcod",
       "noov:Nvpro_cean",
       "noov:Nvdet_entr",
       "noov:Nvuni_quan",
       "noov:Nvdet_nota",
       "noov:Nvdet_padr",
       "noov:Nvdet_marc",
       "noov:Nvdet_mode"
FROM Pz_Fe_Auto_Det(:Tipo:, :Prefijo:, :Numero:)
JOIN V_Redondeo R ON (1 = 1)', 'S', 10, 'NOOVAC', 'NOTA CREDITO', 'noov:DTOFacDetallefacturas', NULL, 'noov:Nvfac_dcop', NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (543, 'con:impuestos', 'DOC', '-- Impuestos Contable Nota Credito

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_oper",
       "noov:Nvimp_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base", Redondeo)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo", Redondeo)) "noov:Nvimp_valo"
FROM Pz_Fe_Auto_Imp(:Tipo:, :Prefijo:, :Numero:)
JOIN V_Redondeo R ON (1 = 1)
WHERE "noov:Nvimp_cdia" NOT IN (''07'', ''99'')', 'S', 15, 'NOOVAC', 'NOTA CREDITO', 'noov:DTOFacImpuestosfacturas', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (544, 'con:tasa_cambio', 'DOC', '-- Tasa Cambio Contable Nota Credito

SELECT FIRST 1  ''COP'' "noov:Nvtas_sour",
               Codigo "noov:Nvtas_targ",
               Valor "noov:Nvtas_calc",
               Fecha "noov:Nvtas_fech"
FROM Pz_Fe_Auto_Trm(:Tipo:, :Prefijo:, :Numero:)
WHERE Codigo <> ''COP''', 'S', 20, 'NOOVAC', 'NOTA CREDITO', 'noov:TasaCambio', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (545, 'con:proveedor', 'DOC', '-- Proveedor Doc Soporte

SELECT "noov:Nvpro_cper",
       "noov:Nvpro_cdoc",
       "noov:Nvpro_docu",
       "noov:Nvpro_dive",
       "noov:Nvpro_pais",
       "noov:Nvpro_depa",
       "noov:Nvpro_ciud",
       "noov:Nvpro_loca",
       "noov:Nvpro_dire",
       "noov:Nvpro_ntel",
       "noov:Nvpro_regi",
       "noov:Nvpro_fisc",
       "noov:Nvpro_nomb",
       "noov:Nvpro_pnom",
       "noov:Nvpro_snom",
       "noov:Nvpro_apel",
       "noov:Nvpro_mail",
       "noov:Nvpro_ncon"
FROM Pz_Fe_Auto_Comp(:Tipo:, :Prefijo:, :Numero:, :Emisor:, :Receptor:)
WHERE "noov:Nvpro_docu" <> ''''', 'S', 40, 'NOOVAC', 'NOTA CREDITO', 'noov:Proveedor', NULL, NULL, 'con:comprobante')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (561, 'con:comprobante', 'DOC', '-- Comprobante Contable Doc Soporte

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT "noov:Nvfac_orig",
       "noov:Nvemp_nnit",
       "noov:Nvres_nume",
       "noov:Nvfac_tipo",
       "noov:Nvres_pref",
       "noov:Nvfac_nume",
       "noov:Nvfac_fech",
       "noov:Nvfac_cdet",
       "noov:Nvfac_venc",
       "noov:Nvsuc_codi",
       "noov:Nvmon_codi",
       "noov:Nvfor_codi",
       "noov:Nvven_nomb",
       "noov:Nvfac_fpag",
       "noov:Nvfac_conv",
       "noov:Nvema_copi",
       "noov:Nvfac_obse",
       "noov:Nvfac_orde",
       "noov:Nvfac_remi",
       "noov:Nvfac_rece",
       "noov:Nvfac_entr",
       "noov:Nvfac_ccos",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_stot", Redondeo)) "noov:Nvfac_stot",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc", Redondeo)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_anti", Redondeo)) "noov:Nvfac_anti",
       "noov:Nvfac_carg",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_tota", Redondeo)) "noov:Nvfac_tota",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_totp", Redondeo)) "noov:Nvfac_totp",
       --Ajuste al redondeo
       --totp
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_totp", Redondeo)) -
       --stot
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_stot", Redondeo)) -
       --Impuestos
       COALESCE((SELECT Valor
                 FROM Redondeo_Dian((SELECT SUM("noov:Nvimp_valo")
                                     FROM Pz_Fe_Auto_Imp(:Tipo:, :Prefijo:, :Numero:)
                                     WHERE "noov:Nvimp_oper" = ''S''), Redondeo)), 0) +
       --descuentos
       COALESCE((SELECT Valor
                 FROM Redondeo_Dian("noov:Nvfac_desc", Redondeo)), 0) + COALESCE((SELECT Valor
                                                                                  FROM Redondeo_Dian("noov:Nvfac_anti", Redondeo)), 0) "noov:Nvfac_roun",
       "noov:Nvfac_vcop",
       "noov:Nvfac_timp",
       "noov:Nvfac_coid",
       "noov:Nvfac_obsb",
       '''' "noov:Nvfor_oper"
FROM Pz_Fe_Auto_Comp(:Tipo:, :Prefijo:, :Numero:, :Emisor:, :Receptor:)
JOIN V_Redondeo R ON (1 = 1)', 'S', 5, 'NOOVAC', 'DOCUMENTO SOPORTE', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (562, 'con:proveedor', 'DOC', '-- Proveedor Doc Soporte

SELECT "noov:Nvpro_cper",
       "noov:Nvpro_cdoc",
       "noov:Nvpro_docu",
       "noov:Nvpro_dive",
       "noov:Nvpro_pais",
       "noov:Nvpro_depa",
       "noov:Nvpro_ciud",
       "noov:Nvpro_loca",
       "noov:Nvpro_dire",
       "noov:Nvpro_ntel",
       "noov:Nvpro_regi",
       "noov:Nvpro_fisc",
       "noov:Nvpro_nomb",
       "noov:Nvpro_pnom",
       "noov:Nvpro_snom",
       "noov:Nvpro_apel",
       "noov:Nvpro_mail",
       "noov:Nvpro_ncon"
FROM Pz_Fe_Auto_Comp(:Tipo:, :Prefijo:, :Numero:, :Emisor:, :Receptor:)', 'S', 40, 'NOOVAC', 'DOCUMENTO SOPORTE', 'noov:Proveedor', NULL, '', 'con:comprobante')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (563, 'con:detalle', 'DOC', '-- Detalle Contable Doc Soporte

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT Consecutivo,
       "noov:Nvpro_codi",
       "noov:Nvpro_nomb",
       "noov:Nvuni_desc",
       "noov:Nvfac_cant",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_valo", Redondeo)) "noov:Nvfac_valo",
       "noov:Nvfac_pdes",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc", Redondeo)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian(Stot_Red, Redondeo)) "noov:Nvfac_stot",
       ''00'' "noov:Nvimp_cdia",
       0 "noov:Nvdet_piva",
       0 "noov:Nvdet_viva",
       "noov:Nvdet_tcod",
       "noov:Nvpro_cean",
       "noov:Nvdet_entr",
       "noov:Nvuni_quan",
       "noov:Nvdet_nota",
       "noov:Nvdet_padr",
       "noov:Nvdet_marc",
       "noov:Nvdet_mode"
FROM Pz_Fe_Auto_Det(:Tipo:, :Prefijo:, :Numero:)
JOIN V_Redondeo R ON (1 = 1)', 'S', 15, 'NOOVAC', 'DOCUMENTO SOPORTE', 'noov:DTOFacDetallefacturas', NULL, 'noov:Nvfac_dcop', NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (564, 'con:impuestos', 'DOC', '-- Impuestos Contable Documento Soporte

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_oper",
       "noov:Nvimp_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base", Redondeo)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo", Redondeo)) "noov:Nvimp_valo"
FROM Pz_Fe_Auto_Imp(:Tipo:, :Prefijo:, :Numero:)
JOIN V_Redondeo R ON (1 = 1)
WHERE "noov:Nvimp_oper" = ''R''
      AND "noov:Nvimp_cdia" NOT IN (''07'', ''99'')', 'S', 25, 'NOOVAC', 'DOCUMENTO SOPORTE', 'noov:DTOFacImpuestosfacturas', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (565, 'con:tasa_cambio', 'DOC', '--Tasa Cambio Contable Documento Soporte

SELECT FIRST 1  ''COP'' "noov:Nvtas_sour",
               Codigo "noov:Nvtas_targ",
               Valor "noov:Nvtas_calc",
               Fecha "noov:Nvtas_fech"
FROM Pz_Fe_Auto_Trm(:Tipo:, :Prefijo:, :Numero:)
WHERE Codigo <> ''COP''', 'S', 30, 'NOOVAC', 'DOCUMENTO SOPORTE', 'noov:TasaCambio', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (581, 'con:comprobante', 'DOC', '-- Comprobante Contable Nota Ajuste a DS

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT "noov:Nvfac_orig",
       "noov:Nvemp_nnit",
       "noov:Nvres_nume",
       "noov:Nvfac_tipo",
       "noov:Nvres_pref",
       "noov:Nvfac_nume",
       "noov:Nvfac_fech",
       "noov:Nvfac_cdet",
       "noov:Nvfac_venc",
       "noov:Nvsuc_codi",
       "noov:Nvmon_codi",
       IIF("noov:Nvfor_codi" = ''13'', ''2'', "noov:Nvfor_codi") "noov:Nvfor_codi",
       "noov:Nvven_nomb",
       "noov:Nvfac_fpag",
       "noov:Nvfac_conv",
       "noov:Nvema_copi",
       "noov:Nvfac_obse",
       "noov:Nvfac_orde",
       "noov:Nvfac_remi",
       "noov:Nvfac_rece",
       "noov:Nvfac_entr",
       "noov:Nvfac_ccos",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_stot", Redondeo)) "noov:Nvfac_stot",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc", Redondeo)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_anti", Redondeo)) "noov:Nvfac_anti",
       "noov:Nvfac_carg",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_tota", Redondeo)) "noov:Nvfac_tota",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_totp", Redondeo)) "noov:Nvfac_totp",
       --Ajuste al redondeo

       --totp
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_totp", Redondeo)) -
       --stot
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_stot", Redondeo)) -
       --Impuestos
       COALESCE((SELECT Valor
                 FROM Redondeo_Dian((SELECT SUM("noov:Nvimp_valo")
                                     FROM Pz_Fe_Auto_Imp(:Tipo:, :Prefijo:, :Numero:)
                                     WHERE "noov:Nvimp_oper" = ''S''), Redondeo)), 0) +
       --descuentos
       COALESCE((SELECT Valor
                 FROM Redondeo_Dian("noov:Nvfac_desc", Redondeo)), 0) + COALESCE((SELECT Valor
                                                                                  FROM Redondeo_Dian("noov:Nvfac_anti", Redondeo)), 0) "noov:Nvfac_roun",
       "noov:Nvfac_vcop",
       "noov:Nvfac_timp",
       "noov:Nvfac_coid",
       "noov:Nvfac_numb",
       "noov:Nvfac_fecb",
       "noov:Nvfac_obsb",
       "noov:Nvcli_ncon",
       "noov:Nvfac_tcru",
       "noov:Nvcon_codi",
       "noov:Nvcon_desc"
FROM Pz_Fe_Auto_Comp(:Tipo:, :Prefijo:, :Numero:, :Emisor:, :Receptor:)
JOIN V_Redondeo R ON (1 = 1)', 'S', 5, 'NOOVAC', 'NOTA DE AJUSTE A DS', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (582, 'con:proveedor', 'DOC', '-- Proveedor Nota Ajuste a DS

SELECT "noov:Nvpro_cper",
       "noov:Nvpro_cdoc",
       "noov:Nvpro_docu",
       "noov:Nvpro_dive",
       "noov:Nvpro_pais",
       "noov:Nvpro_depa",
       "noov:Nvpro_ciud",
       "noov:Nvpro_zipc",
       "noov:Nvpro_loca",
       "noov:Nvpro_dire",
       "noov:Nvpro_ntel",
       "noov:Nvpro_regi",
       "noov:Nvpro_fisc",
       "noov:Nvpro_nomb",
       "noov:Nvpro_pnom",
       "noov:Nvpro_snom",
       "noov:Nvpro_apel",
       "noov:Nvpro_mail",
       "noov:Nvpro_ncon"
FROM Pz_Fe_Auto_Comp(:Tipo:, :Prefijo:, :Numero, :Emisor:, :Receptor:)', 'S', 10, 'NOOVAC', 'NOTA DE AJUSTE A DS', 'noov:Proveedor', NULL, NULL, 'con:comprobante')
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (583, 'con:detalle', 'DOC', '-- Detalle Contable Nota Ajuste a DS

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT Consecutivo,
       "noov:Nvpro_codi",
       "noov:Nvpro_nomb",
       "noov:Nvuni_desc",
       "noov:Nvfac_cant",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_valo", Redondeo)) "noov:Nvfac_valo",
       "noov:Nvfac_pdes",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvfac_desc", Redondeo)) "noov:Nvfac_desc",
       (SELECT Valor
        FROM Redondeo_Dian(Stot_Red, Redondeo)) "noov:Nvfac_stot",
       ''00'' "noov:Nvimp_cdia",
       0 "noov:Nvdet_piva",
       0 "noov:Nvdet_viva",
       "noov:Nvdet_tcod",
       "noov:Nvpro_cean",
       "noov:Nvdet_entr",
       "noov:Nvuni_quan",
       "noov:Nvdet_nota",
       "noov:Nvdet_padr",
       "noov:Nvdet_marc",
       "noov:Nvdet_mode"
FROM Pz_Fe_Auto_Det(:Tipo:, :Prefijo:, :Numero:)
JOIN V_Redondeo R ON (1 = 1)', 'S', 10, 'NOOVAC', 'NOTA DE AJUSTE A DS', 'noov:DTOFacDetallefacturas', NULL, 'noov:Nvfac_dcop', NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (584, 'con:impuestos', 'DOC', '-- Impuestos Contable Nota de Ajuste a DS

WITH V_Redondeo
AS (SELECT Redondeo
    FROM Pz_Fe_Redondeado(:Receptor:))

SELECT "noov:Nvimp_cdia",
       "noov:Nvimp_oper",
       "noov:Nvimp_desc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_base", Redondeo)) "noov:Nvimp_base",
       "noov:Nvimp_porc",
       (SELECT Valor
        FROM Redondeo_Dian("noov:Nvimp_valo", Redondeo)) "noov:Nvimp_valo"
FROM Pz_Fe_Auto_Imp(:Tipo:, :Prefijo:, :Numero:)
JOIN V_Redondeo R ON (1 = 1)
WHERE "noov:Nvimp_oper" = ''R''
      AND "noov:Nvimp_cdia" NOT IN (''07'', ''99'')', 'S', 25, 'NOOVAC', 'NOTA DE AJUSTE A DS', 'noov:DTOFacImpuestosfacturas', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (585, 'con:tasa_cambio', 'DOC', '--Tasa Cambio Contable Nota de Ajuste a DS

SELECT FIRST 1  ''COP'' "noov:Nvtas_sour",
               Codigo "noov:Nvtas_targ",
               Valor "noov:Nvtas_calc",
               Fecha "noov:Nvtas_fech"
FROM Pz_Fe_Auto_Trm(:Tipo:, :Prefijo:, :Numero:)
WHERE Codigo <> ''COP''', 'S', 20, 'NOOVAC', 'NOTA DE AJUSTE A DS', 'noov:TasaCambio', NULL, NULL, NULL)
                        MATCHING (ID);


COMMIT WORK;


/* EXPORTADOS CONTABLE FIN*/

/**** MODULO CONTABLE FINAL                              ****/

/************************************************************/
/**** MODULO NOMINA                                      ****/
/************************************************************/

/* EXPORTADOS NOMINA INICIO*/

DELETE FROM Exportados WHERE Pt = 'NOOVA_NE';

COMMIT WORK;

UPDATE PTA SET 
    XML_TEMPLATE = '<?xml version="1.0" encoding="UTF-8"?>
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
    xmlns:noov="http://noova/">
    <soapenv:Header>
        <noov:AuthHeader>
            <noov:Username>UsuarioNoova</noov:Username>
            <noov:Password>ClaveNoova</noov:Password>
        </noov:AuthHeader>
    </soapenv:Header>
    <!--971 Nomina 06/Mar/2025 v1-->
    <soapenv:Body>
        <noov:SetNomNominas>
            <noov:nomina>
                nom:nomina
                <noov:Periodo>
                    nom:periodo
                </noov:Periodo>
                <noov:InformacionGeneral>nom:inf_general</noov:InformacionGeneral>
                <noov:LNotas>nom:notas</noov:LNotas>
                <noov:Empleador>nom:empleador</noov:Empleador>
                <noov:Trabajador>nom:trabajador</noov:Trabajador>
                <noov:Pago>nom:pago</noov:Pago>
                <noov:Devengados>
                    <noov:Basico>nom:basico</noov:Basico>
                    nom:transporte
                    <noov:LHorasExtras>nom:horas_extras</noov:LHorasExtras>
                    <noov:LVacaciones>nom:vacaciones</noov:LVacaciones>
                    nom:primas
                    nom:cesantias
                    nom:dotacion
                    nom:indemnizacion
                    <noov:LIncapacidades>nom:incapacidad</noov:LIncapacidades>
                    <noov:LLicencias>nom:licencias</noov:LLicencias>
                    <noov:LBonificaciones>nom:bonificacion</noov:LBonificaciones>
                    <noov:LAuxilios>nom:auxilios</noov:LAuxilios>
                    <noov:LHuelgasLegales>nom:huelgas</noov:LHuelgasLegales>
                    <noov:LBonoEPCTVs>nom:bonos</noov:LBonoEPCTVs>
                    <noov:LComisiones>nom:comision</noov:LComisiones>
                    <noov:LOtrosConceptos>nom:otro_devengados</noov:LOtrosConceptos>
                </noov:Devengados>
                <noov:Deducciones>
                    nom:salud
                    nom:pension
                    nom:fondosp
                    <noov:LSindicatos>nom:sindicato</noov:LSindicatos>
                    <noov:LSanciones>nom:sancion</noov:LSanciones>
                    <noov:LLibranzas>nom:libranzas</noov:LLibranzas>
                    nom:pag_ter
                    nom:anticipos
                    nom:pvoluntaria
                    nom:retencionfuente
                    nom:ahorrofomentoconstr
                    nom:cooperativa
                    nom:embargofiscal
                    nom:plancomplementarios
                    nom:educacion
                    nom:reintegro
                    nom:deuda
                    nom:otras_ded
                </noov:Deducciones>
                nom:predecesor
                </noov:nomina>
        </noov:SetNomNominas>
    </soapenv:Body>
</soapenv:Envelope>'
WHERE (CODIGO = 'NOOVA_NE');


COMMIT WORK;

UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1005, 'nom:nomina', 'DOC', '-- Encabezado NE 969 Nomina 31/Oct/2024 v1
WITH V_Pref
AS (SELECT IIF(TRIM(COALESCE(Codprefijo, ''_'')) = ''_'', ''NE'', TRIM(Codprefijo)) Prefi
    FROM Val_Documentos
    WHERE Coddocumento = ''NE'')

SELECT 1 "noov:Nvsuc_codi",
       ''NE'' "noov:Nvnom_pref",
       :Numero: "noov:Nvnom_cons",
       ''NE'' || :Numero: "noov:Nvnom_nume",
       ''NM'' "noov:Nvope_tipo",
       EXTRACT(YEAR FROM CAST(:Fecha_Hasta: AS DATE)) || ''-'' || RIGHT(EXTRACT(MONTH FROM CAST(:Fecha_Hasta: AS DATE)) + 100, 2) || ''-'' || RIGHT(EXTRACT(DAY FROM CAST(:Fecha_Hasta: AS DATE)) + 100, 2) "noov:Nvnom_fpag",
       2000 "noov:Nvnom_redo",
       COALESCE(Adicion, 0) "noov:Nvnom_devt",
       COALESCE(Deduccion, 0) "noov:Nvnom_dedt",
       COALESCE(Neto, 0) "noov:Nvnom_comt",
       IIF(:Clase_Fe: = ''FACTURA'', '''', ''CUNE'') "noov:Nvnom_cnov",
       (SELECT Ajuste
        FROM Pz_Ne_Ajuste(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:)) "noov:Nvnom_tipo"
FROM Pz_Ne_Resumen(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:)
JOIN V_Pref Ne ON (1 = 1)', 'S', 5, 'NOOVA_NE', 'FACTURA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1010, 'nom:periodo', 'DOC', '-- Periodo

WITH V_Fechas
AS (SELECT Codpersonal,
           Desde,
           Hasta,
           MAX(Ingreso) Ingreso,
           MAX(Retiro) Retiro
    FROM (SELECT S.Codpersonal,
                 CAST(:Fecha_Desde: AS DATE) Desde,
                 CAST(:Fecha_Hasta: AS DATE) Hasta,
                 MAX(S.Desde) Ingreso,
                 CAST(''01/01/1900'' AS DATE) Retiro
          FROM Salarios S
          WHERE S.Codpersonal = :Receptor:
                AND S.Columna_Desde = ''INGRESO''
                AND S.Desde <= :Fecha_Hasta:
          GROUP BY 1, 2, 3
          UNION
          SELECT S.Codpersonal,
                 CAST(:Fecha_Desde: AS DATE),
                 CAST(:Fecha_Hasta: AS DATE),
                 CAST(''01/01/1900'' AS DATE),
                 MAX(S.Hasta)
          FROM Salarios S
          WHERE S.Codpersonal = :Receptor:
                AND S.Columna_Hasta = ''RETIRO''
                AND S.Hasta <= :Fecha_Hasta:
          GROUP BY 1, 2, 3)
    GROUP BY 1, 2, 3)

SELECT EXTRACT(YEAR FROM Fe.Ingreso) || ''-'' || RIGHT(EXTRACT(MONTH FROM Fe.Ingreso) + 100, 2) || ''-'' || RIGHT(EXTRACT(DAY FROM Fe.Ingreso) + 100, 2) "noov:Nvper_fing",
       IIF(Fe.Retiro >= Fe.Desde AND Fe.Retiro <= Fe.Hasta, EXTRACT(YEAR FROM Fe.Retiro) || ''-'' || RIGHT(EXTRACT(MONTH FROM Fe.Retiro) + 100, 2) || ''-'' || RIGHT(EXTRACT(DAY FROM Fe.Retiro) + 100, 2), EXTRACT(YEAR FROM Fe.Hasta) || ''-'' || RIGHT(EXTRACT(MONTH FROM Fe.Hasta) + 100, 2) || ''-'' || RIGHT(EXTRACT(DAY FROM Fe.Hasta) + 100, 2)) "noov:Nvper_fret",
       EXTRACT(YEAR FROM Fe.Desde) || ''-'' || RIGHT(EXTRACT(MONTH FROM Fe.Desde) + 100, 2) || ''-'' || RIGHT(EXTRACT(DAY FROM Fe.Desde) + 100, 2) "noov:Nvper_fpin",
       EXTRACT(YEAR FROM Fe.Hasta) || ''-'' || RIGHT(EXTRACT(MONTH FROM Fe.Hasta) + 100, 2) || ''-'' || RIGHT(EXTRACT(DAY FROM Fe.Hasta) + 100, 2) "noov:Nvper_fpfi",
       DATEDIFF(DAY FROM Fe.Ingreso TO Fe.Hasta)+1 "noov:Nvper_tlab"
FROM V_Fechas Fe', 'S', 10, 'NOOVA_NE', 'FACTURA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1015, 'nom:inf_general', 'DOC', '-- Inf general

SELECT (SELECT Tipo_Nomina
        FROM Pz_Ne_Ajuste(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:)) "noov:Nvinf_tnom",
        P.Codtipoperiodo "noov:Nvinf_pnom",
        ''COP'' "noov:Nvinf_tmon"
FROM Personal P
WHERE P.Codigo = :Receptor:', 'S', 15, 'NOOVA_NE', 'FACTURA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1020, 'nom:notas', 'DOC', '-- Notas

SELECT DISTINCT N.Nombre "noov:string"
FROM Nominas N
LEFT JOIN Planillas P ON (N.Codigo = P.Codnomina)
LEFT JOIN Planillas_Ne Ps ON (N.Codigo = Ps.Codnomina)
WHERE (P.Codpersonal = :Receptor: OR (Ps.Codpersonal = :Receptor:))
      AND N.Fecha >= :Fecha_Desde:
      AND N.Fecha <= :Fecha_Hasta:', 'S', 20, 'NOOVA_NE', 'FACTURA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1025, 'nom:empleador', 'DOC', '-- Empleador

SELECT FIRST 1 T.Nombre "noov:Nvemp_nomb",
               T.Codigo "noov:Nvemp_nnit",
               T.Dv "noov:Nvemp_endv",
               Pa.Codigo_Fe "noov:Nvemp_pais",
               SUBSTRING(T.Codmunicipio FROM 1 FOR 2) "noov:Nvemp_depa",
               T.Codmunicipio "noov:Nvemp_ciud",
               T.Direccion "noov:Nvemp_dire"
FROM Terceros T
JOIN Paises Pa ON (T.Codpais = Pa.Codigo)
WHERE T.Codigo = :Emisor:', 'S', 25, 'NOOVA_NE', 'FACTURA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1030, 'nom:trabajador', 'DOC', '-- Trabajador

WITH V_Correo
AS (SELECT FIRST 1 Codigo,
                   TRIM(Email) Email,
                   COUNT(1) Cant
    FROM Contactos
    WHERE Codcargo = ''NECOR''
          AND Codtercero = :Emisor:
    GROUP BY 1, 2
    ORDER BY Codigo)
SELECT LPAD(IIF(TRIM(E.Codcotizante) = ''20'', ''23'', TRIM(E.Codcotizante)), 2, 0) "noov:Nvtra_tipo",
       LPAD(IIF(TRIM(E.Codsubcotizante) <> ''0'', ''1'', TRIM(E.Codsubcotizante)), 2, 0) "noov:Nvtra_stip",
       IIF(COALESCE(E.Alto_Riesgo, 0) = ''N'', ''false'', ''true'') "noov:Nvtra_arpe", --Actividades de alto riesgo
       IIF(E.Codidentidad = ''48'', ''47'', E.Codidentidad) "noov:Nvtra_dtip",
       E.Codigo "noov:Nvtra_ndoc",
       E.Apl1 "noov:Nvtra_pape",
       IIF(COALESCE(E.Apl2, '''') = '''', ''.'', E.Apl2) "noov:Nvtra_sape",
       E.Nom1 "noov:Nvtra_pnom",
       E.Nom2 "noov:Nvtra_onom",
       ''CO'' "noov:Nvtra_ltpa",
       SUBSTRING(E.Codmunicipio FROM 1 FOR 2) "noov:Nvtra_ltde",
       E.Codmunicipio "noov:Nvtra_ltci",
       E.Direccion "noov:Nvtra_ltdi",
       IIF(E.Salario_Integral = ''X'', ''true'', ''false'') "noov:Nvtra_sint",
       C.Codtipocontrato "noov:Nvtra_tcon", --Tipo Contrato
       COALESCE((SELECT FIRST 1 Basico
                 FROM Nom_Pila_Salarios(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:)), 0) "noov:Nvtra_suel",
       E.Codigo "noov:Nvtra_codt",
       IIF(Ne.Cant > 0, COALESCE(Ne.Email, E.Email), ''ne.mekano@gmail.com'') "noov:Nvtra_mail"
FROM Personal E
JOIN Identidades I ON (E.Codidentidad = I.Codigo)
LEFT JOIN Contratacion C ON (E.Codigo = C.Codpersonal)
LEFT JOIN V_Correo Ne ON 1 = 1
WHERE E.Codigo = :Receptor:
      AND ((C.Inicio <= :Fecha_Desde:
      AND COALESCE(C.Fin, DATEADD(1 MONTH TO CURRENT_DATE)) >= :Fecha_Hasta:)

      OR (C.Inicio >= :Fecha_Desde:
      AND COALESCE(C.Fin, DATEADD(1 MONTH TO CURRENT_DATE)) <= :Fecha_Hasta:)

      OR (C.Inicio >= :Fecha_Desde:
      AND COALESCE(C.Fin, DATEADD(1 MONTH TO CURRENT_DATE)) >= :Fecha_Hasta:)

      OR (C.Inicio <= :Fecha_Desde:
      AND COALESCE(C.Fin, DATEADD(1 MONTH TO CURRENT_DATE)) <= :Fecha_Hasta:))', 'S', 30, 'NOOVA_NE', 'FACTURA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1035, 'nom:pago', 'DOC', '--  Pagos

WITH V_Medio
AS (SELECT COALESCE((SELECT FIRST 1 F.Codigo_Fe
                     FROM Medios_Pago M
                     JOIN Formaspago F ON (M.Codformaspago = F.Codigo)
                     WHERE M.Codpersonal = :Receptor:),
           (SELECT FIRST 1 F.Codigo_Fe
            FROM Medios_Pago M
            JOIN Formaspago F ON (M.Codformaspago = F.Codigo)
            WHERE Codpersonal IS NULL)) Medio
    FROM Rdb$Database)
SELECT 1 "noov:Nvpag_form",
       M.Medio "noov:Nvpag_meto", --Metodo de pago
       B.Nombre "noov:Nvpag_banc", --Nombre banco
       Tc.Nombre "noov:Nvpag_tcue",
       E.Banco "noov:Nvpag_ncue"
FROM Personal E
LEFT JOIN Entidades B ON (E.Codentidad = B.Codigo)
LEFT JOIN Tipocuentas Tc ON (E.Codtipocuenta = Tc.Codigo)
JOIN V_Medio M ON (1 = 1)
WHERE E.Codigo = :Receptor:', 'S', 35, 'NOOVA_NE', 'FACTURA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1040, 'nom:basico', 'DOC', '-- Basico

SELECT COALESCE((SELECT IIF(ROUND(Valor) = 0, 1, ROUND(Valor))
                 FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''DTRA'')), 0) "noov:Nvbas_dtra",
       COALESCE((SELECT Valor
                 FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''STRA'')), 0) "noov:Nvbas_stra"
FROM Rdb$Database', 'S', 40, 'NOOVA_NE', 'FACTURA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1042, 'nom:transporte', 'DOC', '-- Transporte NE

SELECT SUM(COALESCE(Valor, 0)) "noov:Nvtrn_auxt",
       SUM(0) "noov:Nvbon_vias",
       SUM(0) "noov:Nvbon_vins"
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''AUX'')
HAVING SUM(Valor) > 0

UNION ALL

SELECT SUM(0),
       SUM(COALESCE(Valor, 0)),
       SUM(0)
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''VIAS'')
HAVING SUM(Valor) > 0

UNION ALL

SELECT SUM(0),
       SUM(0),
       SUM(COALESCE(Valor, 0))
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''VINS'')
HAVING SUM(Valor) > 0', 'S', 42, 'NOOVA_NE', 'FACTURA', 'noov:Transporte', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1045, 'nom:horas_extras', 'DOC', '-- Horas Extras NE

SELECT
       --'''' "noov:Nvcom_fini",
       -- '''' "noov:Nvcom_ffin",
       Cantidad "noov:Nvcom_cant",
       Valor "noov:Nvcom_pago",
       Codgrupo_Ne "noov:Nvhor_tipo",
       Codigo_Ne "noov:Nvhor_porc"
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''%'')
WHERE Codgrupo_Ne = ''HEN''
      AND Valor > 0

UNION ALL

SELECT
       --'''' "noov:Nvcom_fini",
       -- '''' "noov:Nvcom_ffin",
       Cantidad "noov:Nvcom_cant",
       Valor "noov:Nvcom_pago",
       Codgrupo_Ne "noov:Nvhor_tipo",
       Codigo_Ne "noov:Nvhor_porc"
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''%'')
WHERE Codgrupo_Ne = ''HENDF''
      AND Valor > 0

UNION ALL

SELECT
       --'''' "noov:Nvcom_fini",
       -- '''' "noov:Nvcom_ffin",
       Cantidad "noov:Nvcom_cant",
       Valor "noov:Nvcom_pago",
       Codgrupo_Ne "noov:Nvhor_tipo",
       Codigo_Ne "noov:Nvhor_porc"
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''%'')
WHERE Codgrupo_Ne = ''HED''
      AND Valor > 0

UNION ALL

SELECT
       --'''' "noov:Nvcom_fini",
       -- '''' "noov:Nvcom_ffin",
       Cantidad "noov:Nvcom_cant",
       Valor "noov:Nvcom_pago",
       Codgrupo_Ne "noov:Nvhor_tipo",
       Codigo_Ne "noov:Nvhor_porc"
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''%'')
WHERE Codgrupo_Ne = ''HEDDF''
      AND Valor > 0

UNION ALL

SELECT
       --'''' "noov:Nvcom_fini",
       -- '''' "noov:Nvcom_ffin",
       Cantidad "noov:Nvcom_cant",
       Valor "noov:Nvcom_pago",
       Codgrupo_Ne "noov:Nvhor_tipo",
       Codigo_Ne "noov:Nvhor_porc"
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''%'')
WHERE Codgrupo_Ne = ''HRN''
      AND Valor > 0

UNION ALL

SELECT
       --'''' "noov:Nvcom_fini",
       -- '''' "noov:Nvcom_ffin",
       Cantidad "noov:Nvcom_cant",
       Valor "noov:Nvcom_pago",
       Codgrupo_Ne "noov:Nvhor_tipo",
       Codigo_Ne "noov:Nvhor_porc"
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''%'')
WHERE Codgrupo_Ne = ''HRDDF''
      AND Valor > 0

UNION ALL

SELECT
       --'''' "noov:Nvcom_fini",
       -- '''' "noov:Nvcom_ffin",
       Cantidad "noov:Nvcom_cant",
       Valor "noov:Nvcom_pago",
       Codgrupo_Ne "noov:Nvhor_tipo",
       Codigo_Ne "noov:Nvhor_porc"
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''%'')
WHERE Codgrupo_Ne = ''HRNDF''
      AND Valor > 0', 'S', 45, 'NOOVA_NE', 'FACTURA', 'noov:DTOHorasExtras', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1160, 'nom:vacaciones', 'DOC', '-- Vacaciones NE

SELECT
--'''' "noov:Nvcom_fini",
--      '''' "noov:Nvcom_ffin",
       SUM(IIF(Codgrupo_Ne IN (''VAC1Q'', ''VAC2Q''), IIF(ROUND(Valor) = 0, 1, ROUND(Valor)), 0)) "noov:Nvcom_cant",
       SUM(IIF(Codgrupo_Ne IN (''VAC1S'', ''VAC2S''), Valor, 0)) "noov:Nvcom_pago",
       Codigo_Ne "noov:Nvvac_tipo"
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''%'')
WHERE Codgrupo_Ne IN (''VAC1Q'', ''VAC2Q'', ''VAC1S'', ''VAC2S'')
GROUP BY 3
HAVING SUM(Valor) > 0', 'S', 160, 'NOOVA_NE', 'FACTURA', 'noov:DTOVacaciones', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1165, 'nom:primas', 'DOC', '-- Primas NE

SELECT SUM("noov:Nvpri_cant") "noov:Nvpri_cant",
       SUM("noov:Nvpri_pago") "noov:Nvpri_pago",
       SUM("noov:Nvpri_pagn") "noov:Nvpri_pagn"
FROM (SELECT COALESCE(IIF(ROUND(Valor) = 0, 1, ROUND(Valor)), 0) "noov:Nvpri_cant",
             0 "noov:Nvpri_pago",
             0 "noov:Nvpri_pagn"
      FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''%'')
      WHERE (Codgrupo_Ne = ''PRIQ'')

      UNION ALL

      SELECT 0,
             COALESCE(Valor, 0),
             0
      FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''%'')
      WHERE (Codgrupo_Ne = ''PRIP'')
            AND (Valor > 0)

      UNION ALL

      SELECT 0,
             0,
             COALESCE(Valor, 0)
      FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''%'')
      WHERE (Codgrupo_Ne = ''PRIPN'')
            AND (Valor > 0))
HAVING SUM("noov:Nvpri_cant" + "noov:Nvpri_pago" + "noov:Nvpri_pagn") > 1', 'S', 165, 'NOOVA_NE', 'FACTURA', 'noov:Primas', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1170, 'nom:cesantias', 'DOC', '-- Cesantias NE

SELECT COALESCE((SELECT SUM(Valor)
                 FROM Pz_Ne_Grupo_Ces(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:)), 0) "noov:Nvces_pago",
       IIF(COALESCE((SELECT Valor
                     FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''CESPA'')), 0) = 0, 0,
       (SELECT Codigo_Ne
        FROM Grupo_Ne
        WHERE Codigo = ''CESPOR'')) "noov:Nvces_porc",
       COALESCE((SELECT Valor
                 FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''CESIPA'')), 0) "noov:Nvces_pagi"
FROM Rdb$Database
WHERE COALESCE((SELECT SUM(Valor)
                FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''%'')
                WHERE Codgrupo_Ne IN (''CESPA'', ''CESIPA'')), 0) > 0', 'S', 170, 'NOOVA_NE', 'FACTURA', 'noov:Cesantias', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1175, 'nom:incapacidad', 'DOC', '-- Incapacidades NE

SELECT SUM("noov:Nvcom_cant") "noov:Nvcom_cant",
       SUM("noov:Nvcom_pago") "noov:Nvcom_pago",
       "noov:Nvinc_tipo"
FROM (SELECT
             -- '''' "noov:Nvcom_fini",
             --      '''' "noov:Nvcom_ffin",
             IIF(ROUND(SUM(Cantidad)) = 0, 1, ROUND(SUM(Cantidad))) "noov:Nvcom_cant",
             0 "noov:Nvcom_pago",
             Codigo_Ne "noov:Nvinc_tipo"
      FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''%'')
      WHERE Codgrupo_Ne IN (''INCOQ'', ''INPRQ'', ''INLAQ'')
            AND Cantidad > 0
      GROUP BY 3

      UNION ALL

      SELECT
             -- '''' "noov:Nvcom_fini",
             --      '''' "noov:Nvcom_ffin",
             0,
             SUM(Valor),
             Codigo_Ne
      FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''%'')
      WHERE Codgrupo_Ne IN (''INCOV'', ''INPRV'', ''INLAV'')
            AND Valor > 0
      GROUP BY 3)
GROUP BY 3', 'S', 175, 'NOOVA_NE', 'FACTURA', 'noov:DTOIncapacidad', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1195, 'nom:licencias', 'DOC', '-- Licencias NE

SELECT SUM("noov:Nvcom_cant") "noov:Nvcom_cant",
       SUM("noov:Nvcom_pago") "noov:Nvcom_pago",
       "noov:Nvlic_tipo"
FROM (SELECT
             -- '''' "noov:Nvcom_fini",
             --      '''' "noov:Nvcom_ffin",
             IIF(ROUND(SUM(Cantidad)) = 0, 1, ROUND(SUM(Cantidad))) "noov:Nvcom_cant",
             0 "noov:Nvcom_pago",
             Codigo_Ne "noov:Nvlic_tipo"
      FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''%'')
      WHERE Codgrupo_Ne IN (''LMQ'', ''LRQ'', ''LNQ'')
            AND Cantidad > 0
      GROUP BY 3

      UNION ALL

      SELECT
             -- '''' "noov:Nvcom_fini",
             --      '''' "noov:Nvcom_ffin",
             0,
             SUM(Valor),
             Codigo_Ne
      FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''%'')
      WHERE Codgrupo_Ne IN (''LMV'', ''LRV'', ''LNV'')
            AND Valor > 0
      GROUP BY 3)
GROUP BY 3', 'S', 195, 'NOOVA_NE', 'FACTURA', 'noov:DTOLicencia', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1197, 'nom:bonificacion', 'DOC', '-- Bonificacion NE

SELECT COALESCE(Valor, 0) "noov:Nvbon_bofs",
       0 "noov:Nvbon_bons"
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''BOFS'')
WHERE (Valor > 0)

UNION ALL

SELECT 0,
       COALESCE(Valor, 0)
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''BONS'')
WHERE (Valor > 0)', 'S', 197, 'NOOVA_NE', 'FACTURA', 'noov:DTOBonificacion', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1199, 'nom:auxilios', 'DOC', '-- Auxilios

SELECT COALESCE(Valor, 0) "noov:Nvaux_auxs",
       0 "noov:Nvaux_auns"
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''AUXS'')
WHERE (Valor > 0)

UNION ALL

SELECT 0,
       COALESCE(Valor, 0)
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''AUNS'')
WHERE (Valor > 0)', 'S', 199, 'NOOVA_NE', 'FACTURA', 'noov:DTOAuxilio', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1200, 'nom:huelgas', 'DOC', '-- Huelgas NE

WITH V_Pago
AS (SELECT Gn.Codigo Tipo,
           SUM(P.Adicion + P.Deduccion) Valor
    FROM Planillas P
    JOIN Nominas N ON (P.Codnomina = N.Codigo)
    JOIN Rubros R ON (P.Codrubro = R.Codigo)
    JOIN Grupo_Ne Gn ON (R.Codgrupo_Ne = Gn.Codigo)
    LEFT JOIN Generalidades G ON (R.Codigo = G.Codrubro AND P.Codpersonal = G.Codpersonal AND ((N.Fecha BETWEEN G.Inicio AND G.Fin) OR (G.Inicio BETWEEN N.Desde AND N.Hasta) OR (G.Fin BETWEEN N.Desde AND N.Hasta)))
    WHERE P.Codpersonal = :Receptor:
          AND N.Fecha >= :Fecha_Desde:
          AND N.Fecha <= :Fecha_Hasta:
          AND R.Codgrupo_Ne = ''HUV''
    GROUP BY 1)

SELECT G.Inicio "noov:Nvcom_fini",
       G.Fin "noov:Nvcom_ffin",
       P.Valor "noov:Nvcom_cant",
       Pago.Valor "noov:Nvcom_pago"
FROM Planillas P
JOIN Nominas N ON (P.Codnomina = N.Codigo)
JOIN Rubros R ON (P.Codrubro = R.Codigo)
JOIN Grupo_Ne Gn ON (R.Codgrupo_Ne = Gn.Codigo)
JOIN Generalidades G ON (R.Codigo = G.Codrubro AND P.Codpersonal = G.Codpersonal AND ((N.Fecha BETWEEN G.Inicio AND G.Fin) OR (G.Inicio BETWEEN N.Desde AND N.Hasta) OR (G.Fin BETWEEN N.Desde AND N.Hasta)))
JOIN V_Pago Pago ON (Gn.Codigo = Pago.Tipo)
WHERE P.Codpersonal = :Receptor:
      AND N.Fecha >= :Fecha_Desde:
      AND N.Fecha <= :Fecha_Hasta:
      AND R.Codgrupo_Ne = ''HUQ''', 'S', 235, 'NOOVA_NE', 'FACTURA', 'noov:DTOHuelgaLegal', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1205, 'nom:bonos', 'DOC', '-- Bonos

SELECT COALESCE(IIF(Codgrupo_Ne = ''PAGS'', Valor, 0), 0) "noov:Nvbon_pags",
       COALESCE(IIF(Codgrupo_Ne = ''PANS'', Valor, 0), 0) "noov:Nvbon_pans",
       COALESCE(IIF(Codgrupo_Ne = ''ALIS'', Valor, 0), 0) "noov:Nvbon_alis",
       COALESCE(IIF(Codgrupo_Ne = ''ALNS'', Valor, 0), 0) "noov:Nvbon_alns"
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''%'')
WHERE Codgrupo_Ne IN (''PAGS'', ''PANS'', ''ALIS'', ''ALNS'')', 'S', 236, 'NOOVA_NE', 'FACTURA', 'noov:DTOBonoEPCTV', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1210, 'nom:comision', 'DOC', '-- Comisiones

SELECT Valor "noov:decimal"
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''COMIS'')
WHERE Valor > 0', 'S', 210, 'NOOVA_NE', 'FACTURA', '', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1215, 'nom:dotacion', 'DOC', '-- Dotacion

SELECT Valor "noov:Dotacion"
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''DOTAC'')
WHERE Valor > 0', 'S', 215, 'NOOVA_NE', 'FACTURA', '', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1240, 'nom:otro_devengados', 'DOC', '-- Otros devengados

SELECT NOMRUBRO "noov:Nvotr_desc",
       IIF(CODGRUPO_NE = ''ODSA'', VALOR, 0) "noov:Nvotr_pags",
       IIF(CODGRUPO_NE = ''ODNS'', VALOR, 0) "noov:Nvotr_pans"
FROM PZ_NE_GRUPO_BASE(:RECEPTOR:, :FECHA_DESDE:, :FECHA_HASTA:)
WHERE (CODGRUPO_NE IN (''ODSA'', ''ODNS''))
      AND (VALOR > 0)

UNION ALL

SELECT ''CAUSACION VACACIONES'' "noov:Nvotr_desc",
       0 "noov:Nvotr_pags",
       VALOR "noov:Nvotr_pans"
FROM PZ_NE_GRUPO_BASE(:RECEPTOR:, :FECHA_DESDE:, :FECHA_HASTA:)
WHERE CODGRUPO_NE = ''ODNS_V''
      AND VALOR > 0', 'S', 240, 'NOOVA_NE', 'FACTURA', 'noov:DTOOtroDevengado', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1355, 'nom:salud', 'DOC', '-- Salud NE

SELECT COALESCE((SELECT IIF(Codcotizante IN (''51'', ''12'', ''19''), 0, Codigo_Ne)
                 FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''EPSDTO'') P), 0) "noov:Nvsal_porc",
       COALESCE((SELECT P.Valor
                 FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''EPSDTO'') P), 0) "noov:Nvsal_dedu"
FROM Rdb$Database', 'S', 355, 'NOOVA_NE', 'FACTURA', 'noov:Salud', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1360, 'nom:pension', 'DOC', '-- Pension NE

SELECT COALESCE((SELECT Codigo_Ne
                 FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''AFPDTO'') P), 0) "noov:Nvfon_porc",
       COALESCE((SELECT P.Valor
                 FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''AFPDTO'') P), 0) "noov:Nvfon_dedu"
FROM Rdb$Database', 'S', 360, 'NOOVA_NE', 'FACTURA', 'noov:FondoPension', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1365, 'nom:fondosp', 'DOC', 'SELECT "noov:Nvfsp_porc",
       "noov:Nvfsp_dedu",
       "noov:Nvfsp_posb",
       "noov:Nvfsp_desb"
FROM Pz_Ne_Fondo_Sp(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:)
WHERE "noov:Nvfsp_dedu" + "noov:Nvfsp_desb" > 0', 'S', 365, 'NOOVA_NE', 'FACTURA', 'noov:FondoSP', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1370, 'nom:sindicato', 'DOC', '-- Sindicato NE

SELECT Valor "noov:Nvsin_porc",
       0 "noov:Nvsin_dedu"
FROM Pz_Ne_Grupo_Constante(:Receptor:, EXTRACT(YEAR FROM CAST(:Fecha_Desde: AS DATE)))
WHERE Codgrupo_Ne = ''PORSIN''
      AND Valor > 0

UNION ALL

SELECT 0,
       Valor
FROM Pz_Ne_Grupo_Constante(:Receptor:, EXTRACT(YEAR FROM CAST(:Fecha_Desde: AS DATE)))
WHERE Codgrupo_Ne = ''VAL_SIN''
      AND Valor > 0', 'S', 370, 'NOOVA_NE', 'FACTURA', 'noov:DTOSindicato', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1375, 'nom:sancion', 'DOC', '-- Sancion NE

SELECT Valor "noov:Nvsan_sapu",
       0 "noov:Nvsan_sapv"
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''SAPU'')
WHERE Valor > 0

UNION ALL

SELECT 0,
       Valor
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''SAPV'')
WHERE Valor > 0', 'S', 375, 'NOOVA_NE', 'FACTURA', 'noov:DTOSancion', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1380, 'nom:libranzas', 'DOC', '-- Libranzas

SELECT Nomrubro "noov:Nvlib_desc",
       Valor "noov:Nvlib_dedu"
FROM Pz_Ne_Grupo_Base(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:)
WHERE Codgrupo_Ne = ''LIBRA''
      AND Valor > 0', 'S', 380, 'NOOVA_NE', 'FACTURA', 'noov:DTOLibranza', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1382, 'nom:pag_ter', 'DOC', '-- Pagos a Terceros

SELECT Valor "noov:decimal"
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''PAG_TER'')
WHERE Valor > 0', 'S', 382, 'NOOVA_NE', 'FACTURA', 'noov:LPagosTerceros', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1383, 'nom:anticipos', 'DOC', '-- Anticipos

SELECT Valor "noov:decimal"
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''DT02'')
WHERE Valor > 0', 'S', 383, 'NOOVA_NE', 'FACTURA', 'noov:LAnticipos', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1384, 'nom:pvoluntaria', 'DOC', '-- Pension Voluntaria

SELECT Valor "noov:PensionVoluntaria"
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''DT03'')
WHERE Valor > 0', 'S', 384, 'NOOVA_NE', 'FACTURA', '', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1385, 'nom:retencionfuente', 'DOC', '-- RetencionFuente

SELECT Valor "noov:RetencionFuente"
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''DT04'')
WHERE Valor > 0', 'S', 385, 'NOOVA_NE', 'FACTURA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1386, 'nom:ahorrofomentoconstr', 'DOC', '-- Ahorro Fomento Construccion

SELECT Valor "noov:AhorroFomentoConstr"
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''DT06'')
WHERE Valor > 0', 'S', 386, 'NOOVA_NE', 'FACTURA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1387, 'nom:cooperativa', 'DOC', '-- Cooperativa

SELECT Valor "noov:Cooperativa"
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''DT07'')
WHERE Valor > 0', 'S', 387, 'NOOVA_NE', 'FACTURA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1388, 'nom:embargofiscal', 'DOC', '-- Embargo Fiscal

SELECT Valor "noov:EmbargoFiscal"
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''DT08'')
WHERE Valor > 0', 'S', 388, 'NOOVA_NE', 'FACTURA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1389, 'nom:plancomplementarios', 'DOC', '-- Plan Complementarios

SELECT Valor "noov:PlanComplementarios"
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''DT09'')
WHERE Valor > 0', 'S', 389, 'NOOVA_NE', 'FACTURA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1390, 'nom:educacion', 'DOC', '-- Educacion

SELECT Valor "noov:Educacion"
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''DT10'')
WHERE Valor > 0', 'S', 390, 'NOOVA_NE', 'FACTURA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1391, 'nom:reintegro', 'DOC', '-- Reintegro

SELECT Valor "noov:Reintegro"
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''DT11'')
WHERE Valor > 0', 'S', 391, 'NOOVA_NE', 'FACTURA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1392, 'nom:deuda', 'DOC', '-- Deuda

SELECT Valor "noov:Deuda"
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''DT12'')
WHERE Valor > 0', 'S', 392, 'NOOVA_NE', 'FACTURA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1393, 'nom:otras_ded', 'DOC', '-- Otros deducciones

SELECT Valor "noov:decimal"
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''DT00'')
WHERE Valor > 0', 'S', 393, 'NOOVA_NE', 'FACTURA', 'noov:LOtrasDeducciones', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1394, 'nom:predecesor', 'DOC', '-- Predecesor

SELECT Numero "noov:Nvpre_nume",
       Cune "noov:Nvpre_cune",
       EXTRACT(YEAR FROM CAST(Fecha_Ne AS DATE)) || ''-'' || RIGHT(EXTRACT(MONTH FROM CAST(Fecha_Ne AS DATE)) + 100, 2) || ''-'' || RIGHT(EXTRACT(DAY FROM CAST(Fecha_Ne AS DATE)) + 100, 2) "noov:Nvpre_fgen"
FROM Pz_Ne_Ajuste(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:)
WHERE trim(Tipo_Nomina) = 103', 'S', 394, 'NOOVA_NE', 'FACTURA', 'noov:Predecesor', NULL, NULL, NULL)
                        MATCHING (ID);
UPDATE OR INSERT INTO EXPORTADOS (ID, DESCRIPCION, ARCHIVO, SQL, ACTIVO, ORDEN, PT, CODIGO_FE, ETIQUETA, SUB_ETIQUETA, ETIQUETA_CONSECUTIVO, PADRE)
                          VALUES (1395, 'nom:indemnizacion', 'DOC', '--INDEMNIZACION

SELECT Valor "noov:indemnizacion"
FROM Pz_Ne_Grupo(:Receptor:, :Fecha_Desde:, :Fecha_Hasta:, ''INDEM'')
WHERE Valor > 0', 'S', 216, 'NOOVA_NE', 'FACTURA', NULL, NULL, NULL, NULL)
                        MATCHING (ID);


COMMIT WORK;


/* EXPORTADOS NOMINA FIN*/

/* EXPRESIONES DE TOTALES */
UPDATE OR INSERT INTO EXPRESIONES (CODIGO, NOMBRE, SQL_BLOQUE, ACTIVO, NATIVO)
                           VALUES ('TOT12_DI', 'TOTAL DIAS LABORADOS ULTIMOS 12 MESES', '-- Total ultimos 12 meses
EXECUTE BLOCK
RETURNS (
    Empleado CHAR(15),
    Valor NUMERIC(17,4))
AS
DECLARE V_Nomina     CHAR(5);
DECLARE V_Liq        CHAR(10);
DECLARE V_Desde_Nom  DATE;
DECLARE V_Hasta_Nom  DATE;
DECLARE V_Fecha_Nom  DATE;
DECLARE V_Fecha_Ini  DATE;
DECLARE V_Fecha_Ing  DATE;
DECLARE V_Dias       NUMERIC(17,4);
DECLARE V_Rubro1     CHAR(5) = ''DI015'';
DECLARE V_Rubro2     CHAR(5) = '''';
DECLARE V_Rubro3     CHAR(5) = '''';
DECLARE V_Rubro4     CHAR(5) = '''';
DECLARE V_Rubro5     CHAR(5) = '''';
DECLARE V_Rubro6     CHAR(5) = '''';

BEGIN
  SELECT Rdb$Get_Context(''USER_SESSION'', ''G_NOMINA'')
  FROM Rdb$Database
  INTO V_Nomina;

  SELECT Desde, Hasta, Fecha, Codliquidacion
  FROM Nominas
  WHERE Codigo = :V_Nomina
  INTO V_Desde_Nom, V_Hasta_Nom, V_Fecha_Nom, V_Liq;

  FOR SELECT S.Codpersonal,
             MAX(S.Desde)
      FROM TMP_Empleados Sl
      JOIN Salarios S ON (Sl.Codpersonal = S.Codpersonal)
      WHERE S.Columna_Desde = ''INGRESO''
      GROUP BY 1
      INTO Empleado,
           V_Fecha_Ing

  DO
  BEGIN

    V_Fecha_Ini = DATEADD(-12 MONTH TO :V_Hasta_Nom);
    IF (V_Fecha_Ing > V_Fecha_Ini) THEN
      V_Fecha_Ini = :V_Fecha_Ing;

    SELECT SUM(S.Adicion)
    FROM Planillas S
    JOIN Nominas N ON (S.Codnomina = N.Codigo)
    WHERE N.Fecha >= :V_Fecha_Ini AND
          N.Fecha < :V_Fecha_Nom AND
          S.Codrubro IN (:V_Rubro1, :V_Rubro2, :V_Rubro3, :V_Rubro4, :V_Rubro5, :V_Rubro6) AND
          S.Codpersonal = :Empleado
    INTO Valor;
    Valor = COALESCE(Valor, 0);

    SUSPEND;
  END
END', 'S', 'S')
                         ;
UPDATE OR INSERT INTO EXPRESIONES (CODIGO, NOMBRE, SQL_BLOQUE, ACTIVO, NATIVO)
                           VALUES ('TOT12_HR', 'TOTAL HORAS EXTRAS ULTIMOS 12 MESES', '-- Total ultimos 12 meses
EXECUTE BLOCK
RETURNS (
    Empleado CHAR(15),
    Valor NUMERIC(17,4))
AS
DECLARE V_Nomina     CHAR(5);
DECLARE V_Liq        CHAR(10);
DECLARE V_Desde_Nom  DATE;
DECLARE V_Hasta_Nom  DATE;
DECLARE V_Fecha_Nom  DATE;
DECLARE V_Fecha_Ini  DATE;
DECLARE V_Fecha_Ing  DATE;
DECLARE V_Dias       NUMERIC(17,4);
DECLARE V_Rubro1     CHAR(5) = ''HE024'';
DECLARE V_Rubro2     CHAR(5) = ''HE025'';
DECLARE V_Rubro3     CHAR(5) = ''HE026'';
DECLARE V_Rubro4     CHAR(5) = ''HE027'';
DECLARE V_Rubro5     CHAR(5) = ''HE028'';
DECLARE V_Rubro6     CHAR(5) = ''DV997'';

BEGIN
  SELECT Rdb$Get_Context(''USER_SESSION'', ''G_NOMINA'')
  FROM Rdb$Database
  INTO V_Nomina;

  SELECT Desde, Hasta, Fecha, Codliquidacion
  FROM Nominas
  WHERE Codigo = :V_Nomina
  INTO V_Desde_Nom, V_Hasta_Nom, V_Fecha_Nom, V_Liq;

  FOR SELECT S.Codpersonal,
             MAX(S.Desde)
      FROM TMP_Empleados Sl
      JOIN Salarios S ON (Sl.Codpersonal = S.Codpersonal)
      WHERE S.Columna_Desde = ''INGRESO''
      GROUP BY 1
      INTO Empleado,
           V_Fecha_Ing

  DO
  BEGIN

    V_Fecha_Ini = DATEADD(-12 MONTH TO :V_Hasta_Nom);
    IF (V_Fecha_Ing > V_Fecha_Ini) THEN
      V_Fecha_Ini = :V_Fecha_Ing;

    SELECT SUM(S.Adicion)
    FROM Planillas S
    JOIN Nominas N ON (S.Codnomina = N.Codigo)
    WHERE N.Fecha >= :V_Fecha_Ini AND
          N.Fecha < :V_Fecha_Nom AND
          S.Codrubro IN (:V_Rubro1, :V_Rubro2, :V_Rubro3, :V_Rubro4, :V_Rubro5, :V_Rubro6) AND
          S.Codpersonal = :Empleado
    INTO Valor;
    Valor = COALESCE(Valor, 0);

    SUSPEND;
  END
END', 'S', 'S')
                         ;


COMMIT WORK;

UPDATE OR INSERT INTO EXPRESIONES (CODIGO, NOMBRE, SQL_BLOQUE, ACTIVO, NATIVO)
                           VALUES ('TOTA_SAREP', 'TOTAL RETROACTIVO SALARIAL PARA PRIMA', '--Total Retroactivo
EXECUTE BLOCK
RETURNS (
    Empleado CHAR(15),
    Valor    NUMERIC(17,4))
AS
DECLARE V_Nomina       CHAR(5);
DECLARE V_Liq          CHAR(10);
DECLARE V_Desde_Nom    DATE;
DECLARE V_Hasta_Nom    DATE;
DECLARE V_Fecha_Nom    DATE;
DECLARE V_Fecha_Ini    DATE;
DECLARE V_Fecha_Ing    DATE;
DECLARE V_Fecha_Corte  DATE;
DECLARE V_Dias         NUMERIC(17,4);
DECLARE V_Rubro1       CHAR(5) = ''DV300'';
DECLARE V_Rubro2       CHAR(5) = '''';
DECLARE V_Rubro_Corte1 CHAR(5) = ''LD070'';
DECLARE V_Rubro_Corte2 CHAR(5) = ''LD075'';

BEGIN
  SELECT Rdb$Get_Context(''USER_SESSION'', ''G_NOMINA'')
  FROM Rdb$Database
  INTO V_Nomina;

  SELECT Desde,
         Hasta,
         Fecha,
         Codliquidacion
  FROM Nominas
  WHERE Codigo = :V_Nomina
  INTO V_Desde_Nom,
       V_Hasta_Nom,
       V_Fecha_Nom,
       V_Liq;

  FOR SELECT S.Codpersonal,
             MAX(S.Desde)
      FROM Tmp_Empleados Sl
      JOIN Salarios S ON (Sl.Codpersonal = S.Codpersonal)
      WHERE S.Columna_Desde = ''INGRESO''
      GROUP BY 1
      INTO Empleado,
           V_Fecha_Ing
  DO
  BEGIN

    V_Fecha_Ini = DATEADD(-12 MONTH TO :V_Hasta_Nom);
    IF (V_Desde_Nom > V_Fecha_Ini) THEN
      V_Fecha_Ini = :V_Desde_Nom;
    IF (V_Fecha_Ing > V_Fecha_Ini) THEN
      V_Fecha_Ini = :V_Fecha_Ing;

    SELECT MAX(N.Hasta)
    FROM Nominas N
    JOIN Planillas S ON (N.Codigo = S.Codnomina)
    WHERE S.Codrubro IN (:V_Rubro_Corte1, :V_Rubro_Corte2)
          AND N.Codigo <> :V_Nomina
          AND N.Fecha <= :V_Fecha_Nom
          AND N.Fecha >= :V_Fecha_Ing
          AND S.Codpersonal = :Empleado
    INTO V_Fecha_Corte;
    V_Fecha_Corte = COALESCE(V_Fecha_Corte, V_Fecha_Ini);

    IF (V_Fecha_Corte > V_Fecha_Ini) THEN
      V_Fecha_Ini = V_Fecha_Corte;

    SELECT SUM(S.Adicion)
    FROM Planillas S
    JOIN Nominas N ON (S.Codnomina = N.Codigo)
    WHERE N.Fecha >= :V_Fecha_Ini
          AND N.Fecha <= :V_Fecha_Nom
          AND S.Codrubro IN (:V_Rubro1, :V_Rubro2)
          AND S.Codpersonal = :Empleado
    INTO Valor;
    Valor = COALESCE(Valor, 0);
    SUSPEND;
  END
END', 'S', 'S')
                         ;


COMMIT WORK;


/* CONSTANTES 2025 */
EXECUTE BLOCK
AS
DECLARE VARIABLE V_Constante CHAR(15);
DECLARE VARIABLE V_Ano       CHAR(4);
DECLARE VARIABLE V_Valor     NUMERIC(18,5);
DECLARE VARIABLE V_Texto     CHAR(100);
DECLARE VARIABLE V_nuevo      INTEGER;
BEGIN
  FOR SELECT Codconstante,
             Ano,
             Valor,
             Texto
      FROM Constantesvalor
      WHERE Ano = '2024'
            AND TRIM(Codconstante) <> 'C01_SMLV'
            AND TRIM(Codconstante) <> 'C02_AUXT'
      INTO V_Constante,
           V_Ano,
           V_Valor,
           V_Texto
  DO
  BEGIN
    SELECT COUNT(1)
    FROM Constantesvalor
    WHERE Codconstante = :V_Constante
          AND Ano = '2025'
    INTO V_nuevo;

    IF (V_nuevo = 0) THEN
    BEGIN
      INSERT INTO Constantesvalor
      VALUES (:V_Constante, '2025', :V_Valor, :V_Texto);
    END
  END
END; 

EXECUTE BLOCK
AS
DECLARE VARIABLE V_Codpersonal CHAR(15);
DECLARE VARIABLE V_Constante   CHAR(15);
DECLARE VARIABLE V_Ano         CHAR(4);
DECLARE VARIABLE V_Valor       NUMERIC(18,5);
DECLARE VARIABLE V_Texto       CHAR(100);
DECLARE VARIABLE V_nuevo        INTEGER;
BEGIN
  FOR SELECT Codpersonal,
             Codconstante,
             Ano,
             Valor,
             Texto
      FROM Constantes_Personal
      WHERE Ano = '2024'
      INTO V_Codpersonal,
           V_Constante,
           V_Ano,
           V_Valor,
           V_Texto
  DO
  BEGIN
    SELECT COUNT(1)
    FROM Constantes_Personal
    WHERE Codconstante = :V_Constante
          AND Codpersonal = :V_Codpersonal
          AND Ano = '2025'
    INTO V_nuevo;

    IF (V_nuevo = 0) THEN
    BEGIN
      INSERT INTO constantes_personal
      VALUES (:v_codpersonal,:V_Constante, '2025', :V_Valor, :V_Texto);
    END
  END
END; 

COMMIT WORK;


-- Proceso P80300 de salario minimo 2025
UPDATE OR INSERT INTO PROCESOS (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, CODGRUPO, NIVEL, TABLERO, PRIORIDAD)
                        VALUES ('P80300', 'ACTUALIZA EL SALARIO MINIMO PARA 2025  (1.423.500)', 'Crea nuevos registros de salarios solo a los que tengan salario minimo', 'EXECUTE BLOCK
AS
DECLARE VARIABLE v_nuevo       INTEGER;
DECLARE VARIABLE v_codpersonal VARCHAR(15);
DECLARE VARIABLE v_sql         VARCHAR(500);
BEGIN
  FOR SELECT codpersonal
      FROM salarios
      WHERE basico = 1300000 AND
            desde >= ''01/01/2024'' AND
            hasta IN (''31.12.2024'', ''30.12.2024'')
            AND
            TRIM(COALESCE(columna_hasta, '''')) <> ''RETIRO''
      INTO v_codpersonal
  DO
  BEGIN
    -- Valida si ya tiene datos para 2025
    v_sql = ''SELECT COUNT(1)
    FROM Salarios
    WHERE Codpersonal = '''''' || v_codpersonal || ''''''
          AND Desde = ''''01/01/2025'''''';

    EXECUTE STATEMENT(v_sql)
        INTO v_nuevo;
    IF (v_nuevo = 0) THEN
    BEGIN
      v_sql = ''INSERT INTO Salarios (Codpersonal, Renglon, Basico, Desde, Hasta,
                            Columna_Desde)
      VALUES ('''''' || v_codpersonal || '''''', (SELECT GEN_ID(Gen_Aportes, 1)
                               FROM Rdb$Database), 1423500, ''''01.01.2025'''',
              ''''31.12.2025'''', ''''VARIACION PERMANENTE'''')'';
      EXECUTE STATEMENT(v_sql);
    END
  END
END', 'N', 'NOMINA', 'RUTINAS ESPECIALES', 5, 'N', 1)
                      MATCHING (CODIGO);


COMMIT WORK;



/**** MODULO NOMINA FINAL                                ****/


/************************************************************/
/**** OTROS                                              ****/
/************************************************************/


/* HOJAS */

UPDATE OR INSERT INTO HOJAS (CODIGO, NOMBRE, TABLA, CAMPOS, URL, OBLIGATORIOS)
                     VALUES ('PL_001', 'GENERALIDADES - NOMINA', 'GENERALIDADES', 'ITEM, CODPERSONAL, CODRUBRO, CODCENTRO, NOTA, TEXTO, INICIO, FIN, VALOR, CODUSUARIO', 'https://n9.cl/pl_001_generalidades', 'CODPERSONAL, CODRUBRO, INICIO, FIN, VALOR, CODUSUARIO, ITEM')
                   MATCHING (CODIGO);
UPDATE OR INSERT INTO HOJAS (CODIGO, NOMBRE, TABLA, CAMPOS, URL, OBLIGATORIOS)
                     VALUES ('PL_002', 'NOVEDADES - NOMINA', 'NOVEDADES', 'ITEM,CODPERSONAL, DIA, CODRUBRO, CODCENTRO, NOTA, VALOR, CODPRENOMINA', 'https://n9.cl/pl_002_novedades', 'ITEM, CODPERSONAL, DIA, CODRUBRO,  VALOR, CODPRENOMINA')
                   MATCHING (CODIGO);
UPDATE OR INSERT INTO HOJAS (CODIGO, NOMBRE, TABLA, CAMPOS, URL, OBLIGATORIOS)
                     VALUES ('PL_003', 'TERCEROS', 'TERCEROS', 'CODIGO, DV, NATURALEZA, NOM1, NOM2, APL1, APL2, EMPRESA, RAZON_COMERCIAL, DIRECCION, TELEFONO, MOVIL, EMAIL, GERENTE, CODSOCIEDAD, CODIDENTIDAD, CODMUNICIPIO, CODPAIS, CODACTIVIDAD, CODZONA', 'https://n9.cl/pl_003_terceros', 'CODIGO, NATURALEZA, DIRECCION, EMAIL, CODSOCIEDAD, CODIDENTIDAD, CODMUNICIPIO, CODPAIS, CODZONA, CODACTIVIDAD')
                   MATCHING (CODIGO);
UPDATE OR INSERT INTO HOJAS (CODIGO, NOMBRE, TABLA, CAMPOS, URL, OBLIGATORIOS)
                     VALUES ('PL_004', 'EMPLEADOS - NOMINA', 'PERSONAL', 'CODIGO,NOM1, NOM2, APL1, APL2, DIRECCION, TELEFONO, MOVIL, EMAIL, NACIMIENTO, CODCARGO, CODPROFESION, CODCENTRO, CODSOCIEDAD, CODMUNICIPIO,  CODIDENTIDAD, CODESQNOMINA, CODCOTIZANTE, CODSUBCOTIZANTE, CODENTIDAD, CODTIPOCUENTA, BANCO,SALARIO_INTEGRAL,CODTIPOPERIODO, ALTO_RIESGO', 'https://n9.cl/pl_004_empleados', 'CODIGO,NOM1, APL1, CODCARGO, CODPROFESION, CODCENTRO, CODMUNICIPIO,  CODIDENTIDAD, CODESQNOMINA, CODSOCIEDAD, CODCOTIZANTE, CODSUBCOTIZANTE, SALARIO_INTEGRAL, CODTIPOPERIODO')
                   MATCHING (CODIGO);
UPDATE OR INSERT INTO HOJAS (CODIGO, NOMBRE, TABLA, CAMPOS, URL, OBLIGATORIOS)
                     VALUES ('PL_005', 'CARGOS - EMPLEADOS', 'CARGOS', 'CODIGO, NOMBRE', 'https://n9.cl/pl_005_cargos', 'CODIGO, NOMBRE')
                   MATCHING (CODIGO);
UPDATE OR INSERT INTO HOJAS (CODIGO, NOMBRE, TABLA, CAMPOS, URL, OBLIGATORIOS)
                     VALUES ('PL_006', 'CONTRATOS - EMPLEADOS', 'CONTRATACION', 'ID, CODPERSONAL, INICIO, FIN, CODTIPOCONTRATO, DESCRIPCION, ACTIVO, CODUSUARIO', 'https://n9.cl/pl_006_contratos', 'ID, CODPERSONAL, INICIO, FIN, CODTIPOCONTRATO, ACTIVO, CODUSUARIO')
                   MATCHING (CODIGO);
UPDATE OR INSERT INTO HOJAS (CODIGO, NOMBRE, TABLA, CAMPOS, URL, OBLIGATORIOS)
                     VALUES ('PL_007', 'APORTES SEGURIDAD SOCIAL Y OTROS - EMPLEADOS', 'APORTES_PERSONAL', 'RENGLON, CODPERSONAL, CODTIPOAPORTE, DESDE, HASTA', 'https://n9.cl/pl_007_aportes', 'RENGLON, CODPERSONAL, CODTIPOAPORTE, DESDE, HASTA')
                   MATCHING (CODIGO);
UPDATE OR INSERT INTO HOJAS (CODIGO, NOMBRE, TABLA, CAMPOS, URL, OBLIGATORIOS)
                     VALUES ('PL_008', 'SALARIOS - EMPLEADOS', 'SALARIOS', 'Renglon, Codpersonal, Basico, Desde, Hasta,  Columna_Desde, Columna_Hasta', NULL, 'Renglon, Codpersonal, Basico, Desde, Hasta')
                   MATCHING (CODIGO);
UPDATE OR INSERT INTO HOJAS (CODIGO, NOMBRE, TABLA, CAMPOS, URL, OBLIGATORIOS)
                     VALUES ('PL_010', 'PRECIOS', 'PRECIOS', 'CODLISTA,CODREFERENCIA,PRECIO,MINIMO,MAXDESCUENTO', 'https://xurl.es/pl_010_precios', 'CODLISTA,CODREFERENCIA,PRECIO')
                   MATCHING (CODIGO);
UPDATE OR INSERT INTO HOJAS (CODIGO, NOMBRE, TABLA, CAMPOS, URL, OBLIGATORIOS)
                     VALUES ('PL_011', 'CUENTAS', 'CUENTAS', 'CODIGO,NOMBRE,NIVEL,PRESUPUESTO,TERCERO,ACTIVO,EMPLEADO,CENTRO,BASE,PORCENTAJE,CORRIENTE,EFE,ANALISIS_COSTOS', 'https://xurl.es/pl_011_cuentas', 'CODIGO,NOMBRE,NIVEL,TERCERO,ACTIVO,EMPLEADO,CENTRO,BASE,PORCENTAJE')
                   MATCHING (CODIGO);
UPDATE OR INSERT INTO HOJAS (CODIGO, NOMBRE, TABLA, CAMPOS, URL, OBLIGATORIOS)
                     VALUES ('PL_012', 'PRESUPUESTO COMERCIAL - CONTABLE', 'INI_PRESUPUESTO', 'ITEM,DEBITO,CREDITO,CODCUENTA,CODCENTRO,CODPRESUPUESTO', NULL, 'ITEM,DEBITO,CREDITO,CODCUENTA,CODCENTRO,CODPRESUPUESTO')
                   MATCHING (CODIGO);
UPDATE OR INSERT INTO HOJAS (CODIGO, NOMBRE, TABLA, CAMPOS, URL, OBLIGATORIOS)
                     VALUES ('PL_013', 'REFERENCIAS - STOCK', 'REFERENCIAS_STOCKS', 'CODREFERENCIA,CODBODEGA,STOCK,CODTIPO,CODPREFIJO,CODSEDE,CODTERCERO', NULL, 'CODREFERENCIA,CODBODEGA,STOCK,CODTIPO,CODPREFIJO,CODSEDE,CODTERCERO')
                   MATCHING (CODIGO);
UPDATE OR INSERT INTO HOJAS (CODIGO, NOMBRE, TABLA, CAMPOS, URL, OBLIGATORIOS)
                     VALUES ('PL_009', 'REFERENCIAS - GASTOS Y/O SERVICIOS', 'REFERENCIAS', 'CODIGO,NOMBRE,CODLINEA,CODMEDIDA,COSTO,PRECIO,COD_ESQCONTABLE,COD_ESQIMPUESTO,COD_ESQRETENCION', 'https://xurl.es/pl_009_ref_ser', 'CODIGO,NOMBRE,CODLINEA,CODMEDIDA,COSTO,PRECIO,COD_ESQCONTABLE,COD_ESQIMPUESTO,COD_ESQRETENCION')
                   MATCHING (CODIGO);
UPDATE OR INSERT INTO HOJAS (CODIGO, NOMBRE, TABLA, CAMPOS, URL, OBLIGATORIOS)
                     VALUES ('PL_009.1', 'REFERENCIAS - CAMPOS COMPLETOS', 'REFERENCIAS', 'CODIGO,CODIGO2,NOMBRE,NOMBRE2,UBICACION,UNIDAD_COMPRA,STOCK,TIEMPO,TIEMPO_MAXIMO,COSTO,PRECIO,PESO,ALTO,ANCHO,FONDO,MEDIDA_RASTREO,VALORIZA,COSTEA,SALDOS,ALTERNA,BALANZA,LOTE,SERIAL,CATEGORIA,PORTAFOLIO,ACTIVA,INTERES,GRADOS,VOLUMEN,RENTABILIDAD,SUBPARTIDA,CODLINEA,CODMEDIDA,COD_ESQIMPUESTO,COD_ESQRETENCION,COD_ESQCONTABLE,CODGENERO,CODMARCA,CODSECTOR,ES_ENSAMBLE,ES_PRODUCCION,ENSAMBLE_EDITABLE,SALUD', 'https://xurl.es/pl_009.1_ref_todo', 'CODIGO,NOMBRE,CODLINEA,CODMEDIDA,COD_ESQIMPUESTO,COD_ESQRETENCION,COD_ESQCONTABLE,CODGENERO,CODMARCA,CODSECTOR,VALORIZA,COSTEA,SALDOS')
                   MATCHING (CODIGO);
UPDATE OR INSERT INTO HOJAS (CODIGO, NOMBRE, TABLA, CAMPOS, URL, OBLIGATORIOS)
                     VALUES ('PL_014A', 'COMPROBANTE REEMBOLSO GESTION PASO 1', 'COMPROBANTES', 'TIPO,PREFIJO,NUMERO,CODESCENARIO,FECHA,VENCE,CODTERCERO,CODVENDEDOR,CODBANCO,CODLISTA,NOTA', 'https://xurl.es/pl_014_reemb_C', 'CODTERCERO,CODVENDEDOR,CODBANCO')
                   MATCHING (CODIGO);
UPDATE OR INSERT INTO HOJAS (CODIGO, NOMBRE, TABLA, CAMPOS, URL, OBLIGATORIOS)
                     VALUES ('PL_014B', 'MOVIMIENTO REEMBOLSO GESTION PASO 2', 'TR_INVENTARIO', 'TIPO,PREFIJO,NUMERO,RENGLON,CODTERCERO,CODBODEGA,CODCENTRO,CODREFERENCIA,ENTRADA,UNITARIO,NOTA', 'https://xurl.es/pl_014_reemb_M', 'CODTERCERO,CODCENTRO,CODBODEGA,CODREFERENCIA')
                   MATCHING (CODIGO);
UPDATE OR INSERT INTO HOJAS (CODIGO, NOMBRE, TABLA, CAMPOS, URL, OBLIGATORIOS)
                     VALUES ('PL_015', 'SUCURSALES TERCEROS', 'SUCURSALES', 'CODIGO,NOMBRE,CODZONA,PROTOTIPO,DIRECCION,TELEFONO,MOVIL,EMAIL,CODMUNICIPIO,CODTERCERO', 'https://xurl.es/pl_015_sucur', 'CODIGO,NOMBRE,DIRECCION,EMAIL,CODMUNICIPIO,CODTERCERO')
                   MATCHING (CODIGO);                                      

COMMIT WORK;

-- SECTOR POR DEFECTO
UPDATE OR INSERT INTO SECTORES (CODIGO, NOMBRE, IMPRESORA)
                        VALUES ('NA', 'NO APLICA', 'NA')
                      MATCHING (CODIGO);

COMMIT WORK;


/* FORMA DE PAGO 01 */
EXECUTE BLOCK
AS
DECLARE VARIABLE V_01 INTEGER;
BEGIN
  SELECT COUNT(1)
  FROM Formaspago
  WHERE Codigo = '01'
  INTO V_01;

  IF (:V_01 = 0) THEN
    UPDATE Formaspago
    SET Codigo = '01'
    WHERE Codigo = 'EF';
END;
COMMIT WORK;


/*MODIFICACION DE CAMPO VALOR POR VALOR_PAGO EN ARQUEOS*/


UPDATE OR INSERT INTO INFORMES (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD, DESTINO_FTP, CODREPORTE)
                        VALUES ('ARQG00010', 'ARQUEO DE CAJA RESUMIDO POR MAQUINA - RANGO FECHA', NULL, 'SELECT SUM(R.Transacciones) AS Transacciones,
       SUM(R.Valor_Pago) AS Valor,
       C.Fecha,
       TRIM(R.Forma_Pago) AS Forma_Pago,
       TRIM(R.Nombre_Forma_Pago) AS Nombre_Forma_Pago,
       TRIM(R.Computador) AS Maquina,
       TRIM(C.Codbanco) AS Cod_Caja,
       TRIM(B.Nombre) AS Nom_Caja,
       TRIM(B.Tipo) AS Tipo_Caja,
       TRIM(R.Tercero) AS Tercero,
       TRIM(R.Nombre_Tercero) AS "NOMBRE TERCERO"
FROM Fx_Arqueo_Rango(:_Desde, :_Hasta) R
INNER JOIN Comprobantes C ON ((C.Tipo = R.Tipo) AND
      (C.Prefijo = R.Prefijo) AND
      (C.Numero = R.Numero))
LEFT JOIN Bancos B ON (C.Codbanco = B.Codigo)
WHERE TRIM(R.Computador) LIKE :Computador
GROUP BY 3, 4, 5, 6, 7, 8, 9, 10, 11
ORDER BY 3, 4', 'S', 'GESTION', 'N', 'ARQUEO DE CAJA', 'N', 'LIBRE', 5, 'N', 1, NULL, NULL)
                      MATCHING (CODIGO);
UPDATE OR INSERT INTO INFORMES (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD, DESTINO_FTP, CODREPORTE)
                        VALUES ('ARQG00000', 'ARQUEO DE CAJA RESUMIDO - RANGO FECHA', NULL, 'SELECT * FROM FX_ARQUEO_RESUMEN(:_DESDE,:_HASTA)', 'S', 'GESTION', 'N', 'ARQUEO DE CAJA', 'N', 'LIBRE', 5, 'N', 10, NULL, NULL)
                      MATCHING (CODIGO);
UPDATE OR INSERT INTO INFORMES (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD, DESTINO_FTP, CODREPORTE)
                        VALUES ('ARQG00100', 'ARQUEO DE CAJA DETALLADO POR CAJEROS, FORMAS DE PAGO Y MAQUINA POS - RANGO FECHA', 'Permite en un rango de fecha, generar el arqueo de caja agrupado por una máquina pos o por todas. 
Muestra formas de pago, documento, horario, cajero, vendedor y cliente', 'SELECT TRIM(C.Tipo) AS Tipo,
       TRIM(C.Prefijo) AS Prefijo,
       TRIM(C.Numero) AS Numero,
       C.Fecha,
       R.Valor_Pago,
       R.Transacciones,
       TRIM(R.Forma_Pago) AS Forma_Pago,
       TRIM(R.Nombre_Forma_Pago) AS Nombre_Forma_Pago,
       TRIM(R.Computador) AS Maquina,
       C.Impreso,
       C.Hora,
       TRIM(C.Codbanco) AS Tesoreria,
       TRIM(B.Nombre) AS Nom_Tesoreria,
       TRIM(B.Tipo) AS Tipo_Caja,
       TRIM(R.Tercero) AS Tercero,
       TRIM(R.Nombre_Tercero) AS "NOMBRE TERCERO",
       TRIM(C.Codusuario) AS Codcajero,
       (SELECT TRIM(Nombre_Usuario)
        FROM Fn_Nombre_Usuario(C.Codusuario)) AS "NOMBRE CAJERO",
       TRIM(R.Vendedor) AS Vendedor,
       TRIM(R.Nombre_Vendedor) AS "NOMBRE VENDEDOR",
       TRIM(C.Codsede) AS Codsede,
       TRIM(C.Tipo) || '' '' || TRIM(C.Prefijo) || '' '' || TRIM(C.Numero) AS "DOCUMENTO"
FROM Fx_Arqueo_Rango(:_Desde, :_Hasta) R
INNER JOIN Comprobantes C ON ((C.Tipo = R.Tipo) AND
      (C.Prefijo = R.Prefijo) AND
      (C.Numero = R.Numero))
LEFT JOIN Bancos B ON (C.Codbanco = B.Codigo)
WHERE TRIM(R.Computador) LIKE :Computador
      AND C.Bloqueado = ''S''
ORDER BY 4, 1, 2, 3', 'S', 'GESTION', 'N', 'ARQUEO DE CAJA', 'N', 'LIBRE', 5, 'N', 1, NULL, NULL)
                      MATCHING (CODIGO);
UPDATE OR INSERT INTO INFORMES (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD, DESTINO_FTP, CODREPORTE)
                        VALUES ('ARQG040200', 'ARQUEO DE CAJA DETALLADO PARA MEDIO DE PAGO DIFERENTE A EFECTIVO - RANGO FECHA', 'Muestra por rango de fecha los medios de pago diferentes a efectivo, por un documento, maquina, 
Incluye datos descriptivos de transacciones como autorización pagos con tarjeta y número de cheque', 'SELECT trim(R.TIPO) AS TIPO, TRIM(R.PREFIJO) AS PREFIJO, TRIM(R.NUMERO) AS NUMERO, C.FECHA, TRIM(R.CODFORMASPAGO) AS CODFORMASPAGO,  TRIM(R.DOCUMENTO) AS DOCUMENTO,  C.COMPUTADOR,   
      TRIM(F.Nombre) AS NOM_FORMA_PAGO, R.Valor, C.CODTERCERO,  (SELECT TRIM(NOMBRE_TERCERO) FROM FN_NOMBRE_TERCERO (C.CODTERCERO)) AS "NOMBRE TERCERO", C.CODUSUARIO, C.CODVENDEDOR, C.CODBANCO, C.IMPRESO
      FROM Formaspago F
      JOIN Reg_Pagos R ON (R.Codformaspago = F.Codigo)
      JOIN Comprobantes C ON (R.Tipo = C.Tipo AND R.Prefijo = C.Prefijo AND R.Numero = C.Numero)
      JOIN Documentos D ON (C.Tipo = D.Codigo)
      WHERE TRIM(C.Tipo) LIKE :DOCUMENTO || ''%''
            AND ((C.Fecha >= :_Desde) and (C.fecha<=:_hasta)) 
            AND TRIM(C.Computador) LIKE :Computador || ''%''
            AND D.Grupo = ''VENTA''
            AND D.Es_Compra_Venta = ''S''
            AND D.SIGNO=''CREDITO''    AND TRIM(R.CODFORMASPAGO)<>''01'' AND TRIM(C.CODBANCO) LIKE :CAJA  AND C.BLOQUEADO=''S''         
ORDER BY 1,2,3,4,5,6,7', 'S', 'GESTION', 'N', 'ARQUEO DE CAJA', 'N', 'LIBRE', 5, 'N', 10, NULL, NULL)
                      MATCHING (CODIGO);
UPDATE OR INSERT INTO INFORMES (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD, DESTINO_FTP, CODREPORTE)
                        VALUES ('ARQG040101', 'ARQUEO DE CAJA POR MAQUINA Y FORMAS DE PAGO - SOLO CAJERO ACTUAL RANGO FECHA', 'Permite hacer el arqueo por un rango de fecha. Incluye cajeros, formas de pago y vendedor.
Sólo por el usuario o cajero actual', 'SELECT SUM(R.Transacciones) AS Transacciones,
       SUM(R.Valor_PAGO) AS Valor,
       TRIM(C.Tipo) AS Tipo,
       TRIM(C.Prefijo) AS Prefijo,
       TRIM(C.Numero) AS Numero,
       C.Fecha,
       C.Impreso,
       C.Codsede,
       TRIM(R.Forma_Pago) || '': '' || TRIM(R.Nombre_Forma_Pago) AS Forma_Pago,
       TRIM(R.Computador) AS Maquina_Pos,
       TRIM(C.Codbanco) AS Tesoreria,
       TRIM(B.Nombre) AS Nom_Tesoreria,
       TRIM(B.Tipo) AS Tipo_Caja,
       ''PERIODO: '' || EXTRACT(YEAR FROM C.Fecha) || '' / '' || RIGHT(EXTRACT(MONTH FROM C.Fecha) + 100, 2) AS "AÑO Y MES",
       ''MES: '' ||(
       CASE EXTRACT(MONTH FROM C.Fecha)
         WHEN 1 THEN ''01. ENE''
         WHEN 2 THEN ''02. FEB''
         WHEN 3 THEN ''03. MAR''
         WHEN 4 THEN ''04. ABR''
         WHEN 5 THEN ''05. MAY''
         WHEN 6 THEN ''06. JUN''
         WHEN 7 THEN ''07. JUL''
         WHEN 8 THEN ''08. AGO''
         WHEN 9 THEN ''09. SEP''
         WHEN 10 THEN ''10. OCT''
         WHEN 11 THEN ''11. NOV''
         WHEN 12 THEN ''12. DIC''
       END) AS Mes,
       C.Codusuario AS Cajero,
       TRIM(P.Nom1) || '' '' || TRIM(P.Apl1) AS Vendedor,
       TRIM(R.Tercero) AS Tercero,
       TRIM(R.Nombre_Tercero) AS "NOMBRE TERCERO"
FROM Fx_Arqueo_Rango(:_Desde, :_Hasta) R
INNER JOIN Comprobantes C ON ((C.Tipo = R.Tipo) AND
      (C.Prefijo = R.Prefijo) AND
      (C.Numero = R.Numero))
LEFT JOIN Bancos B ON (C.Codbanco = B.Codigo)
JOIN Personal P ON (C.Codvendedor = P.Codigo)
WHERE TRIM(C.Codusuario) = :Usuario
GROUP BY 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19', 'S', 'GESTION', 'N', 'ARQUEO DE CAJA', 'N', 'LIBRE', 5, 'N', 1, NULL, NULL)
                      MATCHING (CODIGO);


COMMIT WORK;


/*organizacion de cubos arqueos*/
UPDATE OR INSERT INTO CUBOS (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD)
                     VALUES ('ARQG010103', 'ARQUEO DE CAJA POR CAJEROS, FORMAS DE PAGO Y MAQUINA POS - RANGO FECHA', 'Permite hacer el arqueo por un rango de fecha y por una máquina o por todas. Incluye cajeros, formas de pago y vendedor.', 'SELECT SUM(R.Transacciones) AS Transacciones,
       SUM(R.Valor_Pago) AS Valor,
       C.Fecha,
       TRIM(R.Forma_Pago) || '': '' || TRIM(R.Nombre_Forma_Pago) AS Forma_Pago,
       TRIM(R.Computador) AS Maquina_Pos,
       TRIM(C.Codbanco) AS Cod_Caja,
       TRIM(B.Nombre) AS Nom_Caja,
       TRIM(B.Tipo) AS Tipo_Caja,
       ''AÑO: '' || EXTRACT(YEAR FROM C.Fecha) AS "AÑO",
       ''MES: '' ||(
       CASE EXTRACT(MONTH FROM C.Fecha)
         WHEN 1 THEN ''01. ENE''
         WHEN 2 THEN ''02. FEB''
         WHEN 3 THEN ''03. MAR''
         WHEN 4 THEN ''04. ABR''
         WHEN 5 THEN ''05. MAY''
         WHEN 6 THEN ''06. JUN''
         WHEN 7 THEN ''07. JUL''
         WHEN 8 THEN ''08. AGO''
         WHEN 9 THEN ''09. SEP''
         WHEN 10 THEN ''10. OCT''
         WHEN 11 THEN ''11. NOV''
         WHEN 12 THEN ''12. DIC''
       END) AS Mes,
       ''SEMANA: '' || LPAD(TRIM(EXTRACT(WEEK FROM C.Fecha)), 2, ''0'') AS Semana,
       ''DIA: '' || LPAD(TRIM(EXTRACT(DAY FROM C.Fecha)), 2, ''0'') AS Dia,
       C.Codusuario AS Cajero,
       TRIM(P.Nom1) || '' '' || TRIM(P.Apl1) AS Vendedor
FROM Fx_Arqueo_Rango(:_Desde, :_Hasta) R
INNER JOIN Comprobantes C ON ((C.Tipo = R.Tipo) AND
      (C.Prefijo = R.Prefijo) AND
      (C.Numero = R.Numero))
JOIN Bancos B ON (C.Codbanco = B.Codigo)
JOIN Personal P ON (C.Codvendedor = P.Codigo)
WHERE TRIM(R.Computador) LIKE :Computador
      AND R.Grupo = ''VENTA''
GROUP BY 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14', 'S', 'GESTION', 'N', 'ARQUEO DE CAJA', 'N', 'LIBRE', 5, 'N', 1)
                   MATCHING (CODIGO);
UPDATE OR INSERT INTO CUBOS (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD)
                     VALUES ('ARQG010101', 'ARQUEO DE CAJA POR CAJEROS, FORMAS DE PAGO Y MAQUINA POS - HOY', 'Permite hacer el arqueo de caja a hoy y agrupado por cajeros. Incluye máquina pos y formas de pago', 'SELECT SUM(R.Transacciones) AS Transacciones,
       SUM(R.Valor_Pago) AS Valor,
       C.Fecha,
       TRIM(R.Forma_Pago) || '': '' || TRIM(R.Nombre_Forma_Pago) AS Forma_Pago,
       TRIM(R.Computador) AS Maquina_Pos,
       TRIM(C.Codbanco) AS Cod_Caja,
       TRIM(B.Nombre) AS Nom_Caja,
       TRIM(B.Tipo) AS Tipo_Caja,
       C.Codusuario AS Cajero,
       TRIM(P.Nom1) || '' '' || TRIM(P.Apl1) AS Vendedor
FROM Fx_Arqueo_Rango(CURRENT_DATE, CURRENT_DATE) R
INNER JOIN Comprobantes C ON ((C.Tipo = R.Tipo) AND
      (C.Prefijo = R.Prefijo) AND
      (C.Numero = R.Numero))
JOIN Bancos B ON (C.Codbanco = B.Codigo)
JOIN Personal P ON (C.Codvendedor = P.Codigo)
WHERE R.Grupo = ''VENTA''
GROUP BY 3, 4, 5, 6, 7, 8, 9, 10', 'S', 'GESTION', 'N', 'ARQUEO DE CAJA', 'N', 'LIBRE', 5, 'N', 10)
                   MATCHING (CODIGO);
UPDATE OR INSERT INTO CUBOS (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD)
                     VALUES ('ARQG010102', 'ARQUEO DE CAJA POR CAJEROS, FORMAS DE PAGO Y MAQUINA POS - UNA FECHA', 'Permite hacer el arqueo de caja agrupado por cajeros según la fecha indicada. Incluye máquina pos y forma de pago', 'SELECT SUM(R.Transacciones) AS Transacciones,
       SUM(R.Valor_Pago) AS Valor,
       C.Fecha,
       TRIM(R.Forma_Pago) || '': '' || TRIM(R.Nombre_Forma_Pago) AS Forma_Pago,
       TRIM(R.Computador) AS Maquina_Pos,
       TRIM(C.Codbanco) AS Cod_Caja,
       TRIM(B.Nombre) AS Nom_Caja,
       TRIM(B.Tipo) AS Tipo_Caja,
       C.Codusuario AS Cajero,
       TRIM(P.Nom1) || '' '' || TRIM(P.Apl1) AS Vendedor
FROM Fx_Arqueo_Rango(:Fecha, :Fecha) R
INNER JOIN Comprobantes C ON ((C.Tipo = R.Tipo) AND
      (C.Prefijo = R.Prefijo) AND
      (C.Numero = R.Numero))
JOIN Bancos B ON (C.Codbanco = B.Codigo)
JOIN Personal P ON (C.Codvendedor = P.Codigo)
WHERE R.Grupo = ''VENTA''
GROUP BY 3, 4, 5, 6, 7, 8, 9, 10', 'S', 'GESTION', 'N', 'ARQUEO DE CAJA', 'N', 'LIBRE', 5, 'N', 10)
                   MATCHING (CODIGO);


COMMIT WORK;



/*cambio de parametros por error en doble codlinea*/
UPDATE OR INSERT INTO REPORTES (CODIGO, NOMBRE, NOTA, ARCHIVO, ADJUNTO, NATIVO, MODULO, PARAMETROS, PUBLICAR, CODGRUPO, VALIDA_TRANSITO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD)
                        VALUES ('CTP100511', 'PORTAFOLIO DE REFERENCIAS CON FOTO', 'Listado de productos y servicios con atributo Portafolio, incluye foto y descripción. El precio es tomado de la ventana Referencias
Indicar o buscar código de la Lista de Precios a utilizar', 'CT0310.FR3', NULL, 'S', 'PARAMETROS', 'CODLINEA', 'S', 'INVENTARIO', 'N', 'N', 'LIBRE', 5, 'N', 1)
                      MATCHING (CODIGO);
UPDATE OR INSERT INTO REPORTES (CODIGO, NOMBRE, NOTA, ARCHIVO, ADJUNTO, NATIVO, MODULO, PARAMETROS, PUBLICAR, CODGRUPO, VALIDA_TRANSITO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD)
                        VALUES ('CTP100511B', 'PORTAFOLIO DE REFERENCIAS POR UNA LISTA DE PRECIOS Y CON FOTO..', 'Listado de productos y servicios con atributo Portafolio, incluye foto y descripción. 
El precio con o sin impuestos depende de la Lista de Precios seleccionada
Por una o todas las Lineas', 'CT0310B.FR3', NULL, 'S', 'PARAMETROS', 'LISTA_PRECIOS,CODLINEA', 'N', 'INVENTARIO', 'N', 'N', 'LIBRE', 5, 'N', 10)
                      MATCHING (CODIGO);


COMMIT WORK;

/* ENVIADO NOMINA E */
UPDATE Envios_Ne SET Enviado = 'N'
   WHERE COALESCE(Enviado, '') <> 'S'; 

COMMIT WORK;

UPDATE OR INSERT INTO INFORMES (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD, DESTINO_FTP, CODREPORTE)
                        VALUES ('CONTABLE2G', 'INTERFACE DE GESTION -EXPORTA A EXCEL RUTINA HOJA CALCULO CONTABLE COPIAR PEGAR', 'Permite en un rango de fechas mes, exportar todo el movimiento contable para descargar a excel para luego subirlo a cualquier empresa copiando y pegando por hoja calculo a contable', 'SELECT *
FROM Pz_G_Contable2(:Fecha_Desde, :Fecha_Hasta)', 'S', 'GESTION', 'N', 'RUTINAS MIGRACIONES', 'N', 'LIBRE', 5, 'N', 1, NULL, NULL)
                      MATCHING (CODIGO);


COMMIT WORK;


/* Conceptos NC y ND Anexo 1.9 */
UPDATE Notas_Cre
SET Nombre = 'Devolucion parcial de los bienes y/o no aceptacion parcial del servicio'
WHERE Codigo = '1';
UPDATE Notas_Cre
SET Nombre = 'Rebaja o descuento parcial o total'
WHERE Codigo = '3';
UPDATE Notas_Cre
SET Nombre = 'Ajuste de precio'
WHERE Codigo = '4';
UPDATE Notas_Cre
SET Nombre = 'Descuento comercial por pronto pago'
WHERE Codigo = '5';
UPDATE Notas_Cre
SET Nombre = 'Descuento comercial por volumen de ventas'
WHERE Codigo = '6';

DELETE FROM Notas_Deb
WHERE TRIM(Codigo) = '4';
COMMIT WORK;


/*notas pos*/
EXECUTE BLOCK
AS
DECLARE VARIABLE V_Existe_Doc INTEGER;
DECLARE VARIABLE V_Existe_Gen INTEGER;
BEGIN
  V_Existe_Doc = 0;
  V_Existe_Gen = 0;

  UPDATE OR INSERT INTO Prefijos (Codigo, Nombre, Modificado)
  VALUES ('NPOS', 'NOTAS CREDITO POS', '2024-06-25');

  SELECT COUNT(1)
  FROM Documentos
  WHERE TRIM(Codigo) = 'NC7'
  INTO V_Existe_Doc;

  IF (:V_Existe_Doc = 0) THEN
  BEGIN
    INSERT INTO Documentos (Codigo, Nombre, Nombre_Fe, Descripcion, Formato,
                            Formato1, Formato2, Formato3, Formato4, Activo,
                            Activo_Contabilidad, Niif, Excluir_Niif, Signo,
                            Grupo, Es_Compra_Venta, Es_Activo, Tesoreria,
                            Inventario, Cartera, Nomina, Registro_Pago,
                            Impuestos, Retenciones, Comision, Contabilidad,
                            C_Vence, C_Condicionado, C_Codtercero,
                            C_Codvendedor, C_Codlista, C_Nota, C_Codrp,
                            C_Codingreso, C_Codpoliza, C_Codreferido, M_Aplica,
                            M_Cuota, M_Documento, M_Descuento, M_Interes,
                            M_Pronto_Pago, M_Codtercero, M_Nota, Afecta,
                            Valoriza, Costea, Valida_Saldos, Con_Impuestos,
                            Codinteres_Debito, Codinteres_Credito,
                            Codpronto_Pagos, Codcopago, Publica_Retenciones,
                            Base_Dia, Pooling, Duracion, Ensamble,
                            Es_Produccion, Puntos, Rastreo, Exportar, Compartir,
                            Dimensiones, Es_Presupuesto, Analisis_Costos,
                            Rotacion, Deterioro, Logistica, Mapeado, Nativo,
                            Impuesto_Alterno, Juego, Codigo_Fe, Modificado, Aiu,
                            Dia_Sin_Iva, Copago, Anexo, Valida_Fe, Limite_Uvt,
                            Valida_Trm, Conector, Radicacion, Escanear, Salud,
                            Tipo_Operacion, Desglose, Ruta_Fe)
    VALUES ('NC7', 'NOTA CREDITO POS',
            'Nota de Ajuste del Documento Equivalente Electronico',
            'Util para Manejo de Devoluciones o nota para disminución en las ventas de contado POS Electronico',
            'F_NPOS_GES_01A.FR3', 'F_NPOS_GES_01A.FR3', 'F_NPOS_GES_02A.FR3',
            'F_NCE_GES_01A.FR3', '', 'S', 'N', 'N', 'N', 'DEBITO', 'VENTA', 'S',
            'N', 'CONTADO', 'ENTRADA', 'N', 'N', 'S', 'S', 'S', 'S', 'S', 'N',
            'N', 'S', 'S', 'S', 'S', 'N', 'N', 'N', 'N', 'S', 'N', 'N', 'S',
            'N', 'N', 'N', 'S', 'S', 'N', 'S', 'S', 'S', NULL, NULL, NULL, NULL,
            'S', 'N', 0, 3000, 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N',
            'N', '', 'S', 'N', 'N', 'N', 'NOTA CREDITO POS', '2024-06-27', 'N',
            'N', 'N', 'N', 'S', 0, 'N', 'N', 'N', 'N', 'N', NULL, 'N', NULL);

    INSERT INTO Val_Documentos (Coddocumento, Codprefijo, Codescenario, Notac,
                                Notam)
    VALUES ('NC7', 'NPOS', 'NA', '-', '-');

    SELECT COUNT(1)
    FROM Consecutivos
    WHERE Generador = 'PAR_95'
    INTO V_Existe_Gen;

    IF (:V_Existe_Gen = 0) THEN
      INSERT INTO Consecutivos (Coddocumento, Codprefijo, Item, Fecha,
                                Generador)
      VALUES ('NC7', 'NPOS', 26, '2024-05-01', 'PAR_95');
    ELSE
      INSERT INTO Consecutivos (Coddocumento, Codprefijo, Item, Fecha)
      VALUES ('NC7', 'NPOS', 26, '2024-05-01');

  END

END;
COMMIT WORK; 

UPDATE OR INSERT INTO NOTAS_EQ (CODIGO, NOMBRE)
              VALUES ('1', 'Devolucion parcial de los bienes y/o no aceptacion parcial del servicio');
UPDATE OR INSERT INTO NOTAS_EQ (CODIGO, NOMBRE)
              VALUES ('2', 'Anulacion del documento equivalente electronico');
UPDATE OR INSERT INTO NOTAS_EQ (CODIGO, NOMBRE)
              VALUES ('3', 'Rebaja o descuento parcial o total');
UPDATE OR INSERT INTO NOTAS_EQ (CODIGO, NOMBRE)
              VALUES ('4', 'Ajuste de precio');
UPDATE OR INSERT INTO NOTAS_EQ (CODIGO, NOMBRE)
              VALUES ('5', 'Otros');

COMMIT WORK;

UPDATE OR INSERT INTO INFORMES (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, FORMATO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD, DESTINO_FTP, CODREPORTE)
                        VALUES ('NOM020155', 'TRANSFERENCIA NOMINA A CUENTA BANCOLOMBIA -EXPORTAR A ARCHIVO PLANO', 'Requisitos: 
En Tesorería campo titular, asignar A o C y cuenta empresa sin puntos ni comas (ej: C70505874552)
En Personal Asignar campos Entidad Bancaria, Tipo de Cuenta y Numero de cuenta.', 'WITH Vt_Saldos
AS (SELECT P.Codnomina,
           SUM(P.Adicion) AS Valor,
           COUNT(P.Codrubro) AS Cantidad
    FROM Planillas P
    INNER JOIN Personal P1 ON (P.Codpersonal = P1.Codigo)
    JOIN Rubros R ON (P.Codrubro = R.Codigo)
    WHERE P.Codnomina = :Cod_Nomina
          AND R.Codconjunto = ''T_PAGAR''
          AND COALESCE(TRIM(P1.Banco), '''') <> ''''
    GROUP BY P.Codnomina)

SELECT ''1'' || LPAD(TRIM(T.Codigo), 15, ''0'') || ''I'' || LPAD('''', 15, '' '') || ''225'' || RPAD(''NOMINA'', 10, '' '') || EXTRACT(YEAR FROM CURRENT_DATE) || RIGHT(EXTRACT(MONTH FROM CURRENT_DATE) + 100, 2) || RIGHT(EXTRACT(DAY FROM CURRENT_DATE) + 100, 2) || RIGHT(TRIM(V.Codnomina), 2) || EXTRACT(YEAR FROM CURRENT_DATE) || RIGHT(EXTRACT(MONTH FROM CURRENT_DATE) + 100, 2) || RIGHT(EXTRACT(DAY FROM CURRENT_DATE) + 100, 2) || LPAD(V.Cantidad, 6, 0) || LPAD(''0'', 17, ''0'') || LPAD(V.Valor, 15, 0) || ''00'' || LPAD(TRIM(SUBSTRING(B.Titular FROM 2)), 11, 0) ||
       CASE LEFT(B.Titular, 1)
         WHEN ''A'' THEN ''S''
         WHEN ''C'' THEN ''D''
         ELSE ''D''
       END || LPAD('''', 149, '' '') Plano

FROM Terceros T, Vt_Saldos V, Bancos B
WHERE Datos_Empresa = ''S''
      AND B.Codigo = :Banco

UNION ALL

SELECT ''6'' || RPAD(TRIM(Beneficiario), 15, '' '') || RPAD(TRIM(Nombre), 30, '' '') || LPAD(COALESCE(''0'' || TRIM(Banco), 0), 9, 0) || RPAD(COALESCE(TRIM(Cuenta), '' ''), 17, '' '') || '' '' || Tipo_Transaccion || LPAD(ROUND(Valor), 15, 0) || ''00'' || EXTRACT(YEAR FROM CURRENT_DATE) || RIGHT(EXTRACT(MONTH FROM CURRENT_DATE) + 100, 2) || RIGHT(EXTRACT(DAY FROM CURRENT_DATE) + 100, 2) || RPAD(Referencia, 21, '' '') || '' '' || LPAD(0, 5, 0) || RPAD('' '', 15, '' '') || RPAD(Email, 80, '' '') || RPAD('' '', 15, '' '') || RPAD('' '', 27, '' '')
FROM (SELECT TRIM(P.Codpersonal) Beneficiario,
             TRIM(E.Nombre) Nombre,
             COALESCE(''0'' || TRIM(E.Codentidad), 0) Banco,
             TRIM(E.Banco) Cuenta,
             IIF(E.Codtipocuenta = ''C'', ''27'', ''37'') AS Tipo_Transaccion,
             SUM(ROUND(P.Adicion)) Valor,
             CURRENT_DATE Fecha,
             N.Nombre Referencia,
             IIF(E.Email CONTAINING ''@'', COALESCE(E.Email, '' ''), '''') Email

      FROM Planillas P
      JOIN Personal E ON (P.Codpersonal = E.Codigo)
      JOIN Nominas N ON (P.Codnomina = N.Codigo)
      JOIN Rubros R ON (P.Codrubro = R.Codigo)
      WHERE P.Codnomina = :Nomina
            AND R.Codconjunto = ''T_PAGAR''
            AND COALESCE(TRIM(E.Banco), '''') <> ''''
      GROUP BY 1, 2, 3, 4, 5, 7, 8, 9)', 'S', 'NOMINA', NULL, 'N', 'PAGOS', 'N', 'LIBRE', 5, 'N', 1, NULL, NULL)
                      MATCHING (CODIGO);


COMMIT WORK;

/* SISTEMA */

UPDATE OR INSERT INTO SISTEMA (CODIGO, VALOR)
                       VALUES ('FE_DIAS', '60')
                     MATCHING (CODIGO);

UPDATE OR INSERT INTO SISTEMA (CODIGO, VALOR)
                       VALUES ('VALIDA_TRANSITO', '0')
                     MATCHING (CODIGO);

/* QUITAR COMILLAS DE REFERENCIAS*/
UPDATE Referencias
SET Nombre = REPLACE(Nombre,'''', ' ');

COMMIT WORK;

/* VIGENCIA PARA ENVIAR CON CURRENT_DATE */
UPDATE OR INSERT INTO CODIGOS_FE (CODIGO, VIGENCIA)
                          VALUES ('FACTURA', 5)
                        MATCHING (CODIGO);
UPDATE OR INSERT INTO CODIGOS_FE (CODIGO, VIGENCIA)
                          VALUES ('EXPORTACION', 5)
                        MATCHING (CODIGO);
UPDATE OR INSERT INTO CODIGOS_FE (CODIGO, VIGENCIA)
                          VALUES ('POS ELECTRONICO', 5)
                        MATCHING (CODIGO);
UPDATE OR INSERT INTO CODIGOS_FE (CODIGO, VIGENCIA)
                          VALUES ('NOTA CREDITO', 5)
                        MATCHING (CODIGO);      
UPDATE OR INSERT INTO CODIGOS_FE (CODIGO, VIGENCIA)
                          VALUES ('NOTA CREDITO POS', 5)
                        MATCHING (CODIGO);                                          
UPDATE OR INSERT INTO CODIGOS_FE (CODIGO, VIGENCIA)
                          VALUES ('CONTINGENCIA', 60)
                        MATCHING (CODIGO);                                          

COMMIT WORK;

/* CREACION EN GRUPOS DE INTERFACE A GESTION*/
UPDATE OR INSERT INTO GRUPOS (CODIGO, NOMBRE, VALIDA_TRANSITO)
                      VALUES ('RUTINAS MIGRACIONES GESTION', 'INTERFACE A GESTION', 'N')
                    MATCHING (CODIGO);


COMMIT WORK;

/* CREACION DE INFORMES PARA INTERFACEA GESTION*/

UPDATE OR INSERT INTO INFORMES (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, FORMATO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD, DESTINO_FTP, CODREPORTE)
                        VALUES ('GESTION1G', 'INTERFACE DE GESTON -EXPORTA A EXCEL RUTINA HOJA CALCULO GESTION COPIAR PEGAR', 'Permite en un rango de fechas mes, exportar todo el movimiento gestion para descargar a excel para luego subirlo a cualquier empresa copiando y pegando por hoja calculo a gestion', 'SELECT *
FROM Pz_Gestion_Gestion(:Fecha_Desde, :Fecha_Hasta)', 'S', 'GESTION', NULL, 'N', 'RUTINAS MIGRACIONES GESTION', 'N', 'LIBRE', 5, 'N', 1, NULL, NULL)
                      MATCHING (CODIGO);


COMMIT WORK;

/* INFORMES DE RETENCION CON NATURALEZA */
UPDATE OR INSERT INTO INFORMES (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, FORMATO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD, DESTINO_FTP, CODREPORTE)
                        VALUES ('TRI004_JN', 'ANEXOS DE RETENCION EN LA FUENTE POR CUENTA Y NATURALEZA TERCERO', 'Anexos de retención en un rango de  cuentas, por uno o todos los terceros, discriminando personas naturales y jurídicas por un rango de fechas dado.', 'SELECT P.Cuenta,
       P.Nombre_Cuenta AS "NOMBRE CUENTA",
       P.Desde,
       P.Hasta,
       IIF(P.Tercero IS NULL, P.Empleado, P.Tercero) AS Tercero,
       IIF(P.Tercero IS NULL, P.Nombre_Empleado, P.Nombre_Tercero) AS "NOMBRE TERCERO",
       IIF(T.Naturaleza=''N'',''PERSONA NATURAL'',''PERSONA JURIDICA'') "NATURALEZA",
       SUM(P.Debito - P.Credito) AS Creditos,
       SUM(P.Base) AS Base
FROM Conta_Auxiliar(:_DESDE, :_HASTA) P
JOIN Terceros T ON (P.Tercero = T.Codigo)
WHERE (P.Saldo_Inicial = ''N'')
      AND ((P.Cuenta >= :Cuenta_Desde)
      AND (P.Cuenta <= :Cuenta_Hasta))
      AND (TRIM(IIF(P.Tercero IS NULL, P.Empleado, P.Tercero)) LIKE :Tercero)
GROUP BY 1, 2, 3, 4, 5, 6, 7
ORDER BY 1, 2, 7', 'S', 'CONTABLE', X'796F39361119100D121A0C513833160C0B1E796D2B1C07380D161B3B3D2B1E1D131A29161A087D7679793910100B1A0D7D77797B2B0D0A1A7975380D100A0F3D063D10077D77797B2B0D0A1A7973380D100A0F3910100B1A0D0C7D7D797E7E7975311A08360B1A122D10087D77797A391E130C1A79723B1E0B1E2D1008371A1618170B7D797D7F79673A1B160B39100D122A0C1A3B1A191E0A130B331E06100A0B7D77797B2B0D0A1A79733916130B1A0D3E1C0B16091A7D77797A391E130C1A79793916130B1A0D7D777969371A0745393A3939393939394F4F4F4F4F4F4F4F4F4F7978291A0D0C1610117D7D797E7E7D6A79733833160C0B1E3C2A3A312B3E79702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D7F797A28161B0B177D797D2D79723E13161811121A110B37100D057D7D797E7F797A36111B1A077D797D7F797829160C161D131A7D77797A391E130C1A79762C100D0B300D1B1A0D7D7779740C103E0C1C1A111B16111879762C100D0B36111B1A077D797D7E7967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797B2B0D0A1A7D7F796D3833160C0B1E3130323D2D3A3C2A3A312B3E79702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D80797A28161B0B177D797CD17E79723E13161811121A110B37100D057D7D797E7F797A36111B1A077D797D7E797829160C161D131A7D77797B2B0D0A1A79762C100D0B300D1B1A0D7D7779790C103110111A79762C100D0B36111B1A077D797D807967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797A391E130C1A7D7F79743833160C0B1E3B3A2C3B3A79702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D80797A28161B0B177D797D3F79723E13161811121A110B37100D057D7D797E7F797A36111B1A077D797D7D797829160C161D131A7D77797B2B0D0A1A79762C100D0B300D1B1A0D7D7779790C103110111A79762C100D0B36111B1A077D797D807967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797A391E130C1A7D7F79743833160C0B1E373E2C2B3E79702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D80797A28161B0B177D797D3F79723E13161811121A110B37100D057D7D797E7F797A36111B1A077D797D7C797829160C161D131A7D77797B2B0D0A1A79762C100D0B300D1B1A0D7D7779790C103110111A79762C100D0B36111B1A077D797D807967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797A391E130C1A7D7F79723833160C0B1E2B3A2D3C3A2D3079702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D80797A28161B0B177D797D2179723E13161811121A110B37100D057D7D797E7F797A36111B1A077D797D7B797829160C161D131A7D77797B2B0D0A1A79762C100D0B300D1B1A0D7D7779790C103110111A79762C100D0B36111B1A077D797D807967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797A391E130C1A7D7F796C3833160C0B1E3130323D2D3A2B3A2D3C3A2D3079702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D80797A28161B0B177D797C0C7E79723E13161811121A110B37100D057D7D797E7F797A36111B1A077D797D7A797829160C161D131A7D77797B2B0D0A1A79762C100D0B300D1B1A0D7D7779790C103110111A79762C100D0B36111B1A077D797D807967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797A391E130C1A7D7F796F3833160C0B1E313E2B2A2D3E333A253E79702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D7E797A28161B0B177D797D1779723E13161811121A110B37100D057D7D797E7F797A36111B1A077D797D79797829160C161D131A7D77797A391E130C1A79762C100D0B300D1B1A0D7D7779740C103E0C1C1A111B16111879762C100D0B36111B1A077D797D7F7967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797B2B0D0A1A7D7F79713833160C0B1E3C2D3A3B362B302C79702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D80797A28161B0B177D797D2D79723E13161811121A110B37100D057D7D797E7E797A36111B1A077D797D78797829160C161D131A7D77797B2B0D0A1A79762C100D0B300D1B1A0D7D7779790C103110111A79762C100D0B36111B1A077D797D807967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797A391E130C1A7D7F79753833160C0B1E3D3E2C3A79702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D80797A28161B0B177D797D2779723E13161811121A110B37100D057D7D797E7E797A36111B1A077D797D77797829160C161D131A7D77797B2B0D0A1A79762C100D0B300D1B1A0D7D7779790C103110111A79762C100D0B36111B1A077D797D807967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797A391E130C1A7D7F796D3910100B1A0D2C0A12121E0D06360B1A124F79662B1C07380D161B3B3D2B1E1D131A2C0A12121E0D06360B1A127D7579793C10130A12117D7779713833160C0B1E3C2D3A3B362B302C797939100D121E0B7D777971534F514F4F44575F534F514F4F56797B3416111B7D77797A0C142C0A1279772F100C160B1610117D7779770C0F3910100B1A0D797C2B1E187D797D7F797639161A131B311E121A7D77797F79793C10130A12117D7779713833160C0B1E3C2D3A3B362B302C79743B160C0F131E062B1A070B7D77797F79792C100D0B1A1B7D77797A391E130C1A796829160C161D131A39100D3C0A0C0B101216051E0B1610117D77797B2B0D0A1A7D7F796D3910100B1A0D2C0A12121E0D06360B1A124E79662B1C07380D161B3B3D2B1E1D131A2C0A12121E0D06360B1A127D7579793C10130A12117D7779753833160C0B1E3D3E2C3A797939100D121E0B7D777971534F514F4F44575F534F514F4F56797B3416111B7D77797A0C142C0A1279772F100C160B1610117D7779770C0F3910100B1A0D797C2B1E187D797D7F797639161A131B311E121A7D77797F79793C10130A12117D7779753833160C0B1E3D3E2C3A79743B160C0F131E062B1A070B7D77797F79792C100D0B1A1B7D77797A391E130C1A796829160C161D131A39100D3C0A0C0B101216051E0B1610117D77797B2B0D0A1A7D7F796D3910100B1A0D2C0A12121E0D06360B1A124D79662B1C07380D161B3B3D2B1E1D131A2C0A12121E0D06360B1A127D7579793C10130A12117D77797F797939100D121E0B7D777971534F514F4F44575F534F514F4F56797B3416111B7D77797A0C142C0A1279772F100C160B1610117D7779770C0F3910100B1A0D797C2B1E187D797D7F797639161A131B311E121A7D77797F79793C10130A12117D77797F79743B160C0F131E062B1A070B7D77797F79792C100D0B1A1B7D77797A391E130C1A796829160C161D131A39100D3C0A0C0B101216051E0B1610117D77797B2B0D0A1A7D7F796D3910100B1A0D2C0A12121E0D06360B1A124C79662B1C07380D161B3B3D2B1E1D131A2C0A12121E0D06360B1A127D7579793C10130A12117D77797F797939100D121E0B7D777971534F514F4F44575F534F514F4F56797B3416111B7D77797A0C142C0A1279772F100C160B1610117D7779770C0F3910100B1A0D797C2B1E187D797D7F797639161A131B311E121A7D77797F79793C10130A12117D77797F79743B160C0F131E062B1A070B7D77797F79792C100D0B1A1B7D77797A391E130C1A796829160C161D131A39100D3C0A0C0B101216051E0B1610117D77797B2B0D0A1A7D7F796D3910100B1A0D2C0A12121E0D06360B1A124B79662B1C07380D161B3B3D2B1E1D131A2C0A12121E0D06360B1A127D7579793C10130A12117D77797F797939100D121E0B7D777971534F514F4F44575F534F514F4F56797B3416111B7D77797A0C142C0A1279772F100C160B1610117D7779770C0F3910100B1A0D797C2B1E187D797D7F797639161A131B311E121A7D77797F79793C10130A12117D77797F79743B160C0F131E062B1A070B7D77797F79792C100D0B1A1B7D77797A391E130C1A796829160C161D131A39100D3C0A0C0B101216051E0B1610117D77797B2B0D0A1A7D7F796D3910100B1A0D2C0A12121E0D06360B1A124A79662B1C07380D161B3B3D2B1E1D131A2C0A12121E0D06360B1A127D7579793C10130A12117D77797F797939100D121E0B7D777971534F514F4F44575F534F514F4F56797B3416111B7D77797A0C142C0A1279772F100C160B1610117D7779770C0F3910100B1A0D797C2B1E187D797D7F797639161A131B311E121A7D77797F79793C10130A12117D77797F79743B160C0F131E062B1A070B7D77797F79792C100D0B1A1B7D77797A391E130C1A796829160C161D131A39100D3C0A0C0B101216051E0B1610117D77797B2B0D0A1A7D7F79673B1A191E0A130B380D100A0F2C0A12121E0D06360B1A124F79662B1C07380D161B3B3D2B1E1D131A2C0A12121E0D06360B1A127D7579793C10130A12117D7779713833160C0B1E3C2D3A3B362B302C797939100D121E0B7D777971534F514F4F44575F534F514F4F56797B3416111B7D77797A0C142C0A1279772F100C160B1610117D7779770C0F3910100B1A0D797C2B1E187D797D7F797639161A131B311E121A7D77797F79793C10130A12117D7779713833160C0B1E3C2D3A3B362B302C79743B160C0F131E062B1A070B7D77797F79792C100D0B1A1B7D77797A391E130C1A796829160C161D131A39100D3C0A0C0B101216051E0B1610117D77797B2B0D0A1A7D7F79673B1A191E0A130B380D100A0F2C0A12121E0D06360B1A124E79662B1C07380D161B3B3D2B1E1D131A2C0A12121E0D06360B1A127D7579793C10130A12117D7779753833160C0B1E3D3E2C3A797939100D121E0B7D777971534F514F4F44575F534F514F4F56797B3416111B7D77797A0C142C0A1279772F100C160B1610117D7779770C0F3910100B1A0D797C2B1E187D797D7F797639161A131B311E121A7D77797F79793C10130A12117D7779753833160C0B1E3D3E2C3A79743B160C0F131E062B1A070B7D77797F79792C100D0B1A1B7D77797A391E130C1A796829160C161D131A39100D3C0A0C0B101216051E0B1610117D77797B2B0D0A1A7D7F79673B1A191E0A130B380D100A0F2C0A12121E0D06360B1A124D79662B1C07380D161B3B3D2B1E1D131A2C0A12121E0D06360B1A127D7579793C10130A12117D77797F797939100D121E0B7D777971534F514F4F44575F534F514F4F56797B3416111B7D77797A0C142C0A1279772F100C160B1610117D7779770C0F3910100B1A0D797C2B1E187D797D7F797639161A131B311E121A7D77797F79793C10130A12117D77797F79743B160C0F131E062B1A070B7D77797F79792C100D0B1A1B7D77797A391E130C1A796829160C161D131A39100D3C0A0C0B101216051E0B1610117D77797B2B0D0A1A7D7F79673B1A191E0A130B380D100A0F2C0A12121E0D06360B1A124C79662B1C07380D161B3B3D2B1E1D131A2C0A12121E0D06360B1A127D7579793C10130A12117D77797F797939100D121E0B7D777971534F514F4F44575F534F514F4F56797B3416111B7D77797A0C142C0A1279772F100C160B1610117D7779770C0F3910100B1A0D797C2B1E187D797D7F797639161A131B311E121A7D77797F79793C10130A12117D77797F79743B160C0F131E062B1A070B7D77797F79792C100D0B1A1B7D77797A391E130C1A796829160C161D131A39100D3C0A0C0B101216051E0B1610117D77797B2B0D0A1A7D7F79673B1A191E0A130B380D100A0F2C0A12121E0D06360B1A124B79662B1C07380D161B3B3D2B1E1D131A2C0A12121E0D06360B1A127D7579793C10130A12117D77797F797939100D121E0B7D777971534F514F4F44575F534F514F4F56797B3416111B7D77797A0C142C0A1279772F100C160B1610117D7779770C0F3910100B1A0D797C2B1E187D797D7F797639161A131B311E121A7D77797F79793C10130A12117D77797F79743B160C0F131E062B1A070B7D77797F79792C100D0B1A1B7D77797A391E130C1A796829160C161D131A39100D3C0A0C0B101216051E0B1610117D77797B2B0D0A1A7D7F79673B1A191E0A130B380D100A0F2C0A12121E0D06360B1A124A79662B1C07380D161B3B3D2B1E1D131A2C0A12121E0D06360B1A127D7579793C10130A12117D77797F797939100D121E0B7D777971534F514F4F44575F534F514F4F56797B3416111B7D77797A0C142C0A1279772F100C160B1610117D7779770C0F3910100B1A0D797C2B1E187D797D7F797639161A131B311E121A7D77797F79793C10130A12117D77797F79743B160C0F131E062B1A070B7D77797F79792C100D0B1A1B7D77797A391E130C1A796829160C161D131A39100D3C0A0C0B101216051E0B1610117D77797B2B0D0A1A7D7F', 'N', 'TRIBUTARIOS', 'N', 'LIBRE', 5, 'N', 1, NULL, NULL)
                      MATCHING (CODIGO);
UPDATE OR INSERT INTO INFORMES (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, FORMATO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD, DESTINO_FTP, CODREPORTE)
                        VALUES ('IMPG020301', 'TOTAL RETENCIONES POR GRUPO', 'Relación de retenciones, agrupadas por Grupo Y Naturaleza. Muestra el concepto de retención, la base y el valor de la retención, por un rango de fecha dado.', 'SELECT F.Grupo,
       TRIM(F.Retencion) || ''   '' || F.Nombre_Retencion AS Retencion,
       IIF(T.Naturaleza = ''N'', ''PERSONA NATURAL'', ''PERSONA JURIDICA'') Naturaleza,
       SUM(F.Base) AS Base,
       SUM(F.Credito) AS Credito,
       SUM(F.Debito) AS Debito
FROM Fx_Retenciones(:Fecha_Desde, :Fecha_Hasta) F
JOIN Terceros T ON (F.Tercero = T.Codigo)
GROUP BY 1, 2, 3
ORDER BY 1, 2, 3', 'S', 'GESTION', X'796F39361119100D121A0C513833160C0B1E796D2B1C07380D161B3B3D2B1E1D131A29161A087D7679793910100B1A0D7D77797B2B0D0A1A7975380D100A0F3D063D10077D77797B2B0D0A1A7973380D100A0F3910100B1A0D0C7D7D797E7E7975311A08360B1A122D10087D77797A391E130C1A79723B1E0B1E2D1008371A1618170B7D797D7F79673A1B160B39100D122A0C1A3B1A191E0A130B331E06100A0B7D77797B2B0D0A1A79733916130B1A0D3E1C0B16091A7D77797A391E130C1A79793916130B1A0D7D777969371A0745393A3939393939394F4F4F4F4F4F4F4F4F4F7978291A0D0C1610117D7D797E7E7D7379743833160C0B1E382D2A2F3079702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D7F797A28161B0B177D797D4E79723E13161811121A110B37100D057D7D797E7F797A36111B1A077D797D7F797829160C161D131A7D77797A391E130C1A79762C100D0B300D1B1A0D7D7779740C103E0C1C1A111B16111879762C100D0B36111B1A077D797D7F7967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797B2B0D0A1A7D7F79703833160C0B1E2D3A2B3A313C36303179702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D80797A28161B0B177D797C827F79723E13161811121A110B37100D057D7D797E7E797A36111B1A077D797D7E797829160C161D131A7D77797B2B0D0A1A79762C100D0B300D1B1A0D7D7779790C103110111A79762C100D0B36111B1A077D797D807967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797A391E130C1A7D7F79753833160C0B1E3D3E2C3A79702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D80797A28161B0B177D797D1279723E13161811121A110B37100D057D7D797E7E797A36111B1A077D797D7D797829160C161D131A7D77797B2B0D0A1A79762C100D0B300D1B1A0D7D7779790C103110111A79762C100D0B36111B1A077D797D807967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797A391E130C1A7D7F79733833160C0B1E3B3A3D362B3079702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D80797A28161B0B177D797D0D79723E13161811121A110B37100D057D7D797E7E797A36111B1A077D797D7C797829160C161D131A7D77797B2B0D0A1A79762C100D0B300D1B1A0D7D7779790C103110111A79762C100D0B36111B1A077D797D807967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797A391E130C1A7D7F79723833160C0B1E3C2D3A3B362B3079702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D80797A28161B0B177D797D0179723E13161811121A110B37100D057D7D797E7E797A36111B1A077D797D7B797829160C161D131A7D77797B2B0D0A1A79762C100D0B300D1B1A0D7D7779790C103110111A79762C100D0B36111B1A077D797D807967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797A391E130C1A7D7F796F3833160C0B1E313E2B2A2D3E333A253E79702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D7E797A28161B0B177D797D1B79723E13161811121A110B37100D057D7D797E7F797A36111B1A077D797D7A797829160C161D131A7D77797A391E130C1A79762C100D0B300D1B1A0D7D7779740C103E0C1C1A111B16111879762C100D0B36111B1A077D797D7E7967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797B2B0D0A1A7D7F796D3910100B1A0D2C0A12121E0D06360B1A124F79662B1C07380D161B3B3D2B1E1D131A2C0A12121E0D06360B1A127D7579793C10130A12117D7779753833160C0B1E3D3E2C3A797939100D121E0B7D777971534F514F4F44575F534F514F4F56797B3416111B7D77797A0C142C0A1279772F100C160B1610117D7779770C0F3910100B1A0D797C2B1E187D797D7F797639161A131B311E121A7D77797F79793C10130A12117D7779753833160C0B1E3D3E2C3A79743B160C0F131E062B1A070B7D77797F79792C100D0B1A1B7D77797A391E130C1A796829160C161D131A39100D3C0A0C0B101216051E0B1610117D77797B2B0D0A1A7D7F796D3910100B1A0D2C0A12121E0D06360B1A124E79662B1C07380D161B3B3D2B1E1D131A2C0A12121E0D06360B1A127D7579793C10130A12117D7779733833160C0B1E3B3A3D362B30797939100D121E0B7D777971534F514F4F44575F534F514F4F56797B3416111B7D77797A0C142C0A1279772F100C160B1610117D7779770C0F3910100B1A0D797C2B1E187D797D7F797639161A131B311E121A7D77797F79793C10130A12117D7779733833160C0B1E3B3A3D362B3079743B160C0F131E062B1A070B7D77797F79792C100D0B1A1B7D77797A391E130C1A796829160C161D131A39100D3C0A0C0B101216051E0B1610117D77797B2B0D0A1A7D7F796D3910100B1A0D2C0A12121E0D06360B1A124D79662B1C07380D161B3B3D2B1E1D131A2C0A12121E0D06360B1A127D7579793C10130A12117D7779723833160C0B1E3C2D3A3B362B30797939100D121E0B7D777971534F514F4F44575F534F514F4F56797B3416111B7D77797A0C142C0A1279772F100C160B1610117D7779770C0F3910100B1A0D797C2B1E187D797D7F797639161A131B311E121A7D77797F79793C10130A12117D7779723833160C0B1E3C2D3A3B362B3079743B160C0F131E062B1A070B7D77797F79792C100D0B1A1B7D77797A391E130C1A796829160C161D131A39100D3C0A0C0B101216051E0B1610117D77797B2B0D0A1A7D7F79673B1A191E0A130B380D100A0F2C0A12121E0D06360B1A124F79662B1C07380D161B3B3D2B1E1D131A2C0A12121E0D06360B1A127D7579793C10130A12117D7779753833160C0B1E3D3E2C3A797939100D121E0B7D777971534F514F4F44575F534F514F4F56797B3416111B7D77797A0C142C0A1279772F100C160B1610117D7779770C0F3910100B1A0D797C2B1E187D797D7F797639161A131B311E121A7D77797F79793C10130A12117D7779753833160C0B1E3D3E2C3A79743B160C0F131E062B1A070B7D77797F79792C100D0B1A1B7D77797A391E130C1A796829160C161D131A39100D3C0A0C0B101216051E0B1610117D77797B2B0D0A1A7D7F79673B1A191E0A130B380D100A0F2C0A12121E0D06360B1A124E79662B1C07380D161B3B3D2B1E1D131A2C0A12121E0D06360B1A127D7579793C10130A12117D7779733833160C0B1E3B3A3D362B30797939100D121E0B7D777971534F514F4F44575F534F514F4F56797B3416111B7D77797A0C142C0A1279772F100C160B1610117D7779770C0F3910100B1A0D797C2B1E187D797D7F797639161A131B311E121A7D77797F79793C10130A12117D7779733833160C0B1E3B3A3D362B3079743B160C0F131E062B1A070B7D77797F79792C100D0B1A1B7D77797A391E130C1A796829160C161D131A39100D3C0A0C0B101216051E0B1610117D77797B2B0D0A1A7D7F79673B1A191E0A130B380D100A0F2C0A12121E0D06360B1A124D79662B1C07380D161B3B3D2B1E1D131A2C0A12121E0D06360B1A127D7579793C10130A12117D7779723833160C0B1E3C2D3A3B362B30797939100D121E0B7D777971534F514F4F44575F534F514F4F56797B3416111B7D77797A0C142C0A1279772F100C160B1610117D7779770C0F3910100B1A0D797C2B1E187D797D7F797639161A131B311E121A7D77797F79793C10130A12117D7779723833160C0B1E3C2D3A3B362B3079743B160C0F131E062B1A070B7D77797F79792C100D0B1A1B7D77797A391E130C1A796829160C161D131A39100D3C0A0C0B101216051E0B1610117D77797B2B0D0A1A7D7F', 'N', 'RETENCIONES', 'N', 'LIBRE', 5, 'N', 1, NULL, NULL)
                      MATCHING (CODIGO);
UPDATE OR INSERT INTO INFORMES (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, FORMATO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD, DESTINO_FTP, CODREPORTE)
                        VALUES ('IMPG020302', 'TOTAL RETENCIONES POR TERCERO', 'RelaciÃ³n de retenciones, agrupadas por Tercero y Naturaleza. Muestra el concepto de retenciÃ³n, la base y el valor de la retenciÃ³n, por un rango de fecha dado.', 'SELECT TRIM(F.Retencion) || ''   '' || F.Nombre_Retencion AS Retencion,
       IIF(T.Naturaleza = ''N'', ''PERSONA NATURAL'', ''PERSONA JURIDICA'') Naturaleza,
       TRIM(F.Tercero) || ''   '' || TRIM(F.Nombre_Tercero) AS Tercero,
       SUM(F.Base) AS Base,
       SUM(F.Credito) AS Credito,
       SUM(F.Debito) AS Debito
FROM Fx_Retenciones(:Fecha_Desde, :Fecha_Hasta) F
JOIN Terceros T ON (F.Tercero = T.Codigo)
GROUP BY 1, 2, 3
ORDER BY 1, 2, 3', 'S', 'GESTION', X'796F39361119100D121A0C513833160C0B1E796D2B1C07380D161B3B3D2B1E1D131A29161A087D7479793910100B1A0D7D77797B2B0D0A1A7975380D100A0F3D063D10077D77797B2B0D0A1A7973380D100A0F3910100B1A0D0C7D7D797E7E7975311A08360B1A122D10087D77797A391E130C1A79723B1E0B1E2D1008371A1618170B7D797D7F79673A1B160B39100D122A0C1A3B1A191E0A130B331E06100A0B7D77797B2B0D0A1A79733916130B1A0D3E1C0B16091A7D77797B2B0D0A1A79793916130B1A0D7D777969371A0745393A3939393939394F4F4F4F4F4F4F4F4F4F79713916130B1A0D322D2A360B1A124E7D777909371A0745393A3939393939394F4F4F4E4F4F4F4F4F4F4F4F4B4B4A474B4E4B494B3B4A4B4F4F4F4B4F4F4F4F4F4F4C4F4D3A4C4F4C4F4F4B4F4F4F4F4F4F4F3B4F4F4F4F4F4F4B484B3C4946484C484B494E4B4C4A4D4B4A4B4B4B464A4B4B394F4A4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F79713916130B1A0D322D2A360B1A124F7D77790B371A0745393A3939393939394F4F4F4E4F4F4F4F4F4F4F4F4B4B4A474B4E4B494B3B4A4B4F4F4F4B4F4F4F4F4F4F4C4F4D3A4C4F4C4F4F4A4F4F4F4F4F4F4F3C4F4F4F4F4F4F4B484B3C4946484C484B494E4B4B4B4A4B4D4B464A4B4B394F4A4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F7978291A0D0C1610117D7D797E7E7D7979703833160C0B1E2D3A2B3A313C36303179702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D7F797A28161B0B177D797C297E79723E13161811121A110B37100D057D7D797E7E797A36111B1A077D797D7F797829160C161D131A7D77797A391E130C1A79762C100D0B300D1B1A0D7D7779740C103E0C1C1A111B16111879762C100D0B36111B1A077D797D7C7967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797B2B0D0A1A7D7F79723833160C0B1E2B3A2D3C3A2D3079702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D80797A28161B0B177D797C737E79723E13161811121A110B37100D057D7D797E7F797A36111B1A077D797D7E797829160C161D131A7D77797B2B0D0A1A79762C100D0B300D1B1A0D7D7779740C103E0C1C1A111B16111879762C100D0B36111B1A077D797D7F7967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797B2B0D0A1A7D7F79753833160C0B1E3D3E2C3A79702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D80797A28161B0B177D797CF67F79723E13161811121A110B37100D057D7D797E7E797A36111B1A077D797D7D797829160C161D131A7D77797B2B0D0A1A79762C100D0B300D1B1A0D7D7779790C103110111A79762C100D0B36111B1A077D797D807967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797A391E130C1A7D7F79733833160C0B1E3B3A3D362B3079702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D80797A28161B0B177D797D0B79723E13161811121A110B37100D057D7D797E7E797A36111B1A077D797D7C797829160C161D131A7D77797B2B0D0A1A79762C100D0B300D1B1A0D7D7779740C103E0C1C1A111B16111879762C100D0B36111B1A077D797D7E7967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797A391E130C1A7D7F79723833160C0B1E3C2D3A3B362B3079702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D80797A28161B0B177D797CEE7F79723E13161811121A110B37100D057D7D797E7E797A36111B1A077D797D7B797829160C161D131A7D77797B2B0D0A1A79762C100D0B300D1B1A0D7D7779790C103110111A79762C100D0B36111B1A077D797D807967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797A391E130C1A7D7F796F3833160C0B1E313E2B2A2D3E333A253E79702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D7E797A28161B0B177D797D1B79723E13161811121A110B37100D057D7D797E7F797A36111B1A077D797D7A797829160C161D131A7D77797A391E130C1A79762C100D0B300D1B1A0D7D7779740C103E0C1C1A111B16111879762C100D0B36111B1A077D797D7D7967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797B2B0D0A1A7D7F', 'N', 'RETENCIONES', 'N', 'LIBRE', 5, 'N', 1, NULL, NULL)
                      MATCHING (CODIGO);
UPDATE OR INSERT INTO INFORMES (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, FORMATO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD, DESTINO_FTP, CODREPORTE)
                        VALUES ('IMPG020303', 'TOTAL RETENCIONES POR CUENTA', 'Relación de retenciones, agrupadas por Cuenta y Naturaleza. Muestra el concepto de retención, la base y el valor de la retención, por un rango de fecha dado.', 'SELECT TRIM(Cuenta) || ''   '' || Nombre_Cuenta AS Cuenta,
       TRIM(Retencion) || ''   '' || Nombre_Retencion AS Retencion,
       IIF(T.Naturaleza = ''N'', ''PERSONA NATURAL'', ''PERSONA JURIDICA'') Naturaleza,
       SUM(Base) AS Base,
       SUM(Credito) AS Credito,
       SUM(Debito) AS Debito
FROM Fx_Retenciones(:Fecha_Desde, :Fecha_Hasta) F
JOIN Terceros T ON (F.Tercero = T.Codigo)
GROUP BY 1, 2, 3
ORDER BY 1, 2, 3', 'S', 'GESTION', X'796F39361119100D121A0C513833160C0B1E796D2B1C07380D161B3B3D2B1E1D131A29161A087D7679793910100B1A0D7D77797B2B0D0A1A7975380D100A0F3D063D10077D77797B2B0D0A1A7973380D100A0F3910100B1A0D0C7D7D797E7E7975311A08360B1A122D10087D77797A391E130C1A79723B1E0B1E2D1008371A1618170B7D797D7F79673A1B160B39100D122A0C1A3B1A191E0A130B331E06100A0B7D77797B2B0D0A1A79733916130B1A0D3E1C0B16091A7D77797A391E130C1A79793916130B1A0D7D777969371A0745393A3939393939394F4F4F4F4F4F4F4F4F4F7978291A0D0C1610117D7D797E7E7D7379733833160C0B1E3C2A3A312B3E79702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D7F797A28161B0B177D797CD57E79723E13161811121A110B37100D057D7D797E7F797A36111B1A077D797D7F797829160C161D131A7D77797A391E130C1A79762C100D0B300D1B1A0D7D7779740C103E0C1C1A111B16111879762C100D0B36111B1A077D797D7F7967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797B2B0D0A1A7D7F79703833160C0B1E2D3A2B3A313C36303179702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D80797A28161B0B177D797C367E79723E13161811121A110B37100D057D7D797E7E797A36111B1A077D797D7E797829160C161D131A7D77797B2B0D0A1A79762C100D0B300D1B1A0D7D7779790C103110111A79762C100D0B36111B1A077D797D807967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797A391E130C1A7D7F79753833160C0B1E3D3E2C3A79702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D80797A28161B0B177D797D0179723E13161811121A110B37100D057D7D797E7E797A36111B1A077D797D7D797829160C161D131A7D77797B2B0D0A1A79762C100D0B300D1B1A0D7D7779790C103110111A79762C100D0B36111B1A077D797D807967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797A391E130C1A7D7F79733833160C0B1E3B3A3D362B3079702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D80797A28161B0B177D797D0C79723E13161811121A110B37100D057D7D797E7E797A36111B1A077D797D7C797829160C161D131A7D77797B2B0D0A1A79762C100D0B300D1B1A0D7D7779740C103E0C1C1A111B16111879762C100D0B36111B1A077D797D7E7967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797A391E130C1A7D7F79723833160C0B1E3C2D3A3B362B3079702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D80797A28161B0B177D797D0A79723E13161811121A110B37100D057D7D797E7E797A36111B1A077D797D7B797829160C161D131A7D77797B2B0D0A1A79762C100D0B300D1B1A0D7D7779790C103110111A79762C100D0B36111B1A077D797D807967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797A391E130C1A7D7F796F3833160C0B1E313E2B2A2D3E333A253E79702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D7E797A28161B0B177D797D1B79723E13161811121A110B37100D057D7D797E7F797A36111B1A077D797D7A797829160C161D131A7D77797A391E130C1A79762C100D0B300D1B1A0D7D7779740C103E0C1C1A111B16111879762C100D0B36111B1A077D797D807967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797B2B0D0A1A7D7F796D3910100B1A0D2C0A12121E0D06360B1A124F79662B1C07380D161B3B3D2B1E1D131A2C0A12121E0D06360B1A127D7579793C10130A12117D7779753833160C0B1E3D3E2C3A797939100D121E0B7D777971534F514F4F44575F534F514F4F56797B3416111B7D77797A0C142C0A1279772F100C160B1610117D7779770C0F3910100B1A0D797C2B1E187D797D7F797639161A131B311E121A7D77797F79793C10130A12117D7779753833160C0B1E3D3E2C3A79743B160C0F131E062B1A070B7D77797F79792C100D0B1A1B7D77797A391E130C1A796829160C161D131A39100D3C0A0C0B101216051E0B1610117D77797B2B0D0A1A7D7F796D3910100B1A0D2C0A12121E0D06360B1A124E79662B1C07380D161B3B3D2B1E1D131A2C0A12121E0D06360B1A127D7579793C10130A12117D7779733833160C0B1E3B3A3D362B30797939100D121E0B7D777971534F514F4F44575F534F514F4F56797B3416111B7D77797A0C142C0A1279772F100C160B1610117D7779770C0F3910100B1A0D797C2B1E187D797D7F797639161A131B311E121A7D77797F79793C10130A12117D7779733833160C0B1E3B3A3D362B3079743B160C0F131E062B1A070B7D77797F79792C100D0B1A1B7D77797A391E130C1A796829160C161D131A39100D3C0A0C0B101216051E0B1610117D77797B2B0D0A1A7D7F796D3910100B1A0D2C0A12121E0D06360B1A124D79662B1C07380D161B3B3D2B1E1D131A2C0A12121E0D06360B1A127D7579793C10130A12117D7779723833160C0B1E3C2D3A3B362B30797939100D121E0B7D777971534F514F4F44575F534F514F4F56797B3416111B7D77797A0C142C0A1279772F100C160B1610117D7779770C0F3910100B1A0D797C2B1E187D797D7F797639161A131B311E121A7D77797F79793C10130A12117D7779723833160C0B1E3C2D3A3B362B3079743B160C0F131E062B1A070B7D77797F79792C100D0B1A1B7D77797A391E130C1A796829160C161D131A39100D3C0A0C0B101216051E0B1610117D77797B2B0D0A1A7D7F79673B1A191E0A130B380D100A0F2C0A12121E0D06360B1A124F79662B1C07380D161B3B3D2B1E1D131A2C0A12121E0D06360B1A127D7579793C10130A12117D7779753833160C0B1E3D3E2C3A797939100D121E0B7D777971534F514F4F44575F534F514F4F56797B3416111B7D77797A0C142C0A1279772F100C160B1610117D7779770C0F3910100B1A0D797C2B1E187D797D7F797639161A131B311E121A7D77797F79793C10130A12117D7779753833160C0B1E3D3E2C3A79743B160C0F131E062B1A070B7D77797F79792C100D0B1A1B7D77797A391E130C1A796829160C161D131A39100D3C0A0C0B101216051E0B1610117D77797B2B0D0A1A7D7F79673B1A191E0A130B380D100A0F2C0A12121E0D06360B1A124E79662B1C07380D161B3B3D2B1E1D131A2C0A12121E0D06360B1A127D7579793C10130A12117D7779733833160C0B1E3B3A3D362B30797939100D121E0B7D777971534F514F4F44575F534F514F4F56797B3416111B7D77797A0C142C0A1279772F100C160B1610117D7779770C0F3910100B1A0D797C2B1E187D797D7F797639161A131B311E121A7D77797F79793C10130A12117D7779733833160C0B1E3B3A3D362B3079743B160C0F131E062B1A070B7D77797F79792C100D0B1A1B7D77797A391E130C1A796829160C161D131A39100D3C0A0C0B101216051E0B1610117D77797B2B0D0A1A7D7F79673B1A191E0A130B380D100A0F2C0A12121E0D06360B1A124D79662B1C07380D161B3B3D2B1E1D131A2C0A12121E0D06360B1A127D7579793C10130A12117D7779723833160C0B1E3C2D3A3B362B30797939100D121E0B7D777971534F514F4F44575F534F514F4F56797B3416111B7D77797A0C142C0A1279772F100C160B1610117D7779770C0F3910100B1A0D797C2B1E187D797D7F797639161A131B311E121A7D77797F79793C10130A12117D7779723833160C0B1E3C2D3A3B362B3079743B160C0F131E062B1A070B7D77797F79792C100D0B1A1B7D77797A391E130C1A796829160C161D131A39100D3C0A0C0B101216051E0B1610117D77797B2B0D0A1A7D7F', 'N', 'RETENCIONES', 'N', 'LIBRE', 5, 'N', 1, NULL, NULL)
                      MATCHING (CODIGO);
UPDATE OR INSERT INTO INFORMES (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, FORMATO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD, DESTINO_FTP, CODREPORTE)
                        VALUES ('IMPG020304', 'TOTAL RETENCIONES POR TIPO DE DOCUMENTO', 'Relación de retenciones, agrupadas por Tipo de Documento y Naturaleza. Muestra el concepto de retención, la base y el valor de la retención, por un rango de fecha dado.', 'SELECT TRIM(F.Tipo) || ''   '' || TRIM(D.Nombre) AS Documento,
       TRIM(F.Retencion) || ''   '' || F.Nombre_Retencion AS Retencion,
       IIF(T.Naturaleza = ''N'', ''PERSONA NATURAL'', ''PERSONA JURIDICA'') Naturaleza,
       SUM(F.Base) AS Base,
       SUM(F.Credito) AS Credito,
       SUM(F.Debito) AS Debito
FROM Fx_Retenciones(:Fecha_Desde, :Fecha_Hasta) F
JOIN Documentos D ON (F.Tipo = D.Codigo)
JOIN Terceros T ON (F.Tercero = T.Codigo)
GROUP BY 1, 2, 3
ORDER BY 1, 2, 3', 'S', 'GESTION', X'796F39361119100D121A0C513833160C0B1E796D2B1C07380D161B3B3D2B1E1D131A29161A087D7679793910100B1A0D7D77797B2B0D0A1A7975380D100A0F3D063D10077D77797B2B0D0A1A7973380D100A0F3910100B1A0D0C7D7D797E7E7975311A08360B1A122D10087D77797A391E130C1A79723B1E0B1E2D1008371A1618170B7D797D7F79673A1B160B39100D122A0C1A3B1A191E0A130B331E06100A0B7D77797B2B0D0A1A79733916130B1A0D3E1C0B16091A7D77797A391E130C1A79793916130B1A0D7D777969371A0745393A3939393939394F4F4F4F4F4F4F4F4F4F7978291A0D0C1610117D7D797E7E7D6D79703833160C0B1E3B303C2A323A312B3079702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D7F797A28161B0B177D797CC67F79723E13161811121A110B37100D057D7D797E7F797A36111B1A077D797D7F797829160C161D131A7D77797A391E130C1A79762C100D0B300D1B1A0D7D7779740C103E0C1C1A111B16111879762C100D0B36111B1A077D797D7E7967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797B2B0D0A1A7D7F79703833160C0B1E2D3A2B3A313C36303179702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D80797A28161B0B177D797C4F7E79723E13161811121A110B37100D057D7D797E7E797A36111B1A077D797D7E797829160C161D131A7D77797B2B0D0A1A79762C100D0B300D1B1A0D7D7779790C103110111A79762C100D0B36111B1A077D797D807967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797A391E130C1A7D7F79753833160C0B1E3D3E2C3A79702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D80797A28161B0B177D797CF37F79723E13161811121A110B37100D057D7D797E7E797A36111B1A077D797D7D797829160C161D131A7D77797B2B0D0A1A79762C100D0B300D1B1A0D7D7779740C103E0C1C1A111B16111879762C100D0B36111B1A077D797D7F7967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797A391E130C1A7D7F79733833160C0B1E3B3A3D362B3079702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D80797A28161B0B177D797D0079723E13161811121A110B37100D057D7D797E7E797A36111B1A077D797D7C797829160C161D131A7D77797B2B0D0A1A79762C100D0B300D1B1A0D7D7779790C103110111A79762C100D0B36111B1A077D797D807967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797A391E130C1A7D7F79723833160C0B1E3C2D3A3B362B3079702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D80797A28161B0B177D797D1379723E13161811121A110B37100D057D7D797E7E797A36111B1A077D797D7B797829160C161D131A7D77797B2B0D0A1A79762C100D0B300D1B1A0D7D7779790C103110111A79762C100D0B36111B1A077D797D807967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797A391E130C1A7D7F796F3833160C0B1E313E2B2A2D3E333A253E79702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D7E797A28161B0B177D797D1B79723E13161811121A110B37100D057D7D797E7F797A36111B1A077D797D7A797829160C161D131A7D77797A391E130C1A79762C100D0B300D1B1A0D7D7779740C103E0C1C1A111B16111879762C100D0B36111B1A077D797D807967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797B2B0D0A1A7D7F796D3910100B1A0D2C0A12121E0D06360B1A124F79662B1C07380D161B3B3D2B1E1D131A2C0A12121E0D06360B1A127D7579793C10130A12117D77797F797939100D121E0B7D777971534F514F4F44575F534F514F4F56797B3416111B7D77797A0C142C0A1279772F100C160B1610117D7779770C0F3910100B1A0D797C2B1E187D797D7F797639161A131B311E121A7D77797F79793C10130A12117D77797F79743B160C0F131E062B1A070B7D77797F79792C100D0B1A1B7D77797A391E130C1A796829160C161D131A39100D3C0A0C0B101216051E0B1610117D77797B2B0D0A1A7D7F796D3910100B1A0D2C0A12121E0D06360B1A124E79662B1C07380D161B3B3D2B1E1D131A2C0A12121E0D06360B1A127D7579793C10130A12117D77797F797939100D121E0B7D777971534F514F4F44575F534F514F4F56797B3416111B7D77797A0C142C0A1279772F100C160B1610117D7779770C0F3910100B1A0D797C2B1E187D797D7F797639161A131B311E121A7D77797F79793C10130A12117D77797F79743B160C0F131E062B1A070B7D77797F79792C100D0B1A1B7D77797A391E130C1A796829160C161D131A39100D3C0A0C0B101216051E0B1610117D77797B2B0D0A1A7D7F796D3910100B1A0D2C0A12121E0D06360B1A124D79662B1C07380D161B3B3D2B1E1D131A2C0A12121E0D06360B1A127D7579793C10130A12117D77797F797939100D121E0B7D777971534F514F4F44575F534F514F4F56797B3416111B7D77797A0C142C0A1279772F100C160B1610117D7779770C0F3910100B1A0D797C2B1E187D797D7F797639161A131B311E121A7D77797F79793C10130A12117D77797F79743B160C0F131E062B1A070B7D77797F79792C100D0B1A1B7D77797A391E130C1A796829160C161D131A39100D3C0A0C0B101216051E0B1610117D77797B2B0D0A1A7D7F796D3910100B1A0D2C0A12121E0D06360B1A124C79662B1C07380D161B3B3D2B1E1D131A2C0A12121E0D06360B1A127D7579793C10130A12117D7779753833160C0B1E3D3E2C3A797939100D121E0B7D777971534F514F4F44575F534F514F4F56797B3416111B7D77797A0C142C0A1279772F100C160B1610117D7779770C0F3910100B1A0D797C2B1E187D797D7F797639161A131B311E121A7D77797F79793C10130A12117D7779753833160C0B1E3D3E2C3A79743B160C0F131E062B1A070B7D77797F79792C100D0B1A1B7D77797A391E130C1A796829160C161D131A39100D3C0A0C0B101216051E0B1610117D77797B2B0D0A1A7D7F796D3910100B1A0D2C0A12121E0D06360B1A124B79662B1C07380D161B3B3D2B1E1D131A2C0A12121E0D06360B1A127D7579793C10130A12117D7779733833160C0B1E3B3A3D362B30797939100D121E0B7D777971534F514F4F44575F534F514F4F56797B3416111B7D77797A0C142C0A1279772F100C160B1610117D7779770C0F3910100B1A0D797C2B1E187D797D7F797639161A131B311E121A7D77797F79793C10130A12117D7779733833160C0B1E3B3A3D362B3079743B160C0F131E062B1A070B7D77797F79792C100D0B1A1B7D77797A391E130C1A796829160C161D131A39100D3C0A0C0B101216051E0B1610117D77797B2B0D0A1A7D7F796D3910100B1A0D2C0A12121E0D06360B1A124A79662B1C07380D161B3B3D2B1E1D131A2C0A12121E0D06360B1A127D7579793C10130A12117D7779723833160C0B1E3C2D3A3B362B30797939100D121E0B7D777971534F514F4F44575F534F514F4F56797B3416111B7D77797A0C142C0A1279772F100C160B1610117D7779770C0F3910100B1A0D797C2B1E187D797D7F797639161A131B311E121A7D77797F79793C10130A12117D7779723833160C0B1E3C2D3A3B362B3079743B160C0F131E062B1A070B7D77797F79792C100D0B1A1B7D77797A391E130C1A796829160C161D131A39100D3C0A0C0B101216051E0B1610117D77797B2B0D0A1A7D7F79673B1A191E0A130B380D100A0F2C0A12121E0D06360B1A124F79662B1C07380D161B3B3D2B1E1D131A2C0A12121E0D06360B1A127D7579793C10130A12117D77797F797939100D121E0B7D777971534F514F4F44575F534F514F4F56797B3416111B7D77797A0C142C0A1279772F100C160B1610117D7779770C0F3910100B1A0D797C2B1E187D797D7F797639161A131B311E121A7D77797F79793C10130A12117D77797F79743B160C0F131E062B1A070B7D77797F79792C100D0B1A1B7D77797A391E130C1A796829160C161D131A39100D3C0A0C0B101216051E0B1610117D77797B2B0D0A1A7D7F79673B1A191E0A130B380D100A0F2C0A12121E0D06360B1A124E79662B1C07380D161B3B3D2B1E1D131A2C0A12121E0D06360B1A127D7579793C10130A12117D77797F797939100D121E0B7D777971534F514F4F44575F534F514F4F56797B3416111B7D77797A0C142C0A1279772F100C160B1610117D7779770C0F3910100B1A0D797C2B1E187D797D7F797639161A131B311E121A7D77797F79793C10130A12117D77797F79743B160C0F131E062B1A070B7D77797F79792C100D0B1A1B7D77797A391E130C1A796829160C161D131A39100D3C0A0C0B101216051E0B1610117D77797B2B0D0A1A7D7F79673B1A191E0A130B380D100A0F2C0A12121E0D06360B1A124D79662B1C07380D161B3B3D2B1E1D131A2C0A12121E0D06360B1A127D7579793C10130A12117D77797F797939100D121E0B7D777971534F514F4F44575F534F514F4F56797B3416111B7D77797A0C142C0A1279772F100C160B1610117D7779770C0F3910100B1A0D797C2B1E187D797D7F797639161A131B311E121A7D77797F79793C10130A12117D77797F79743B160C0F131E062B1A070B7D77797F79792C100D0B1A1B7D77797A391E130C1A796829160C161D131A39100D3C0A0C0B101216051E0B1610117D77797B2B0D0A1A7D7F79673B1A191E0A130B380D100A0F2C0A12121E0D06360B1A124C79662B1C07380D161B3B3D2B1E1D131A2C0A12121E0D06360B1A127D7579793C10130A12117D7779753833160C0B1E3D3E2C3A797939100D121E0B7D777971534F514F4F44575F534F514F4F56797B3416111B7D77797A0C142C0A1279772F100C160B1610117D7779770C0F3910100B1A0D797C2B1E187D797D7F797639161A131B311E121A7D77797F79793C10130A12117D7779753833160C0B1E3D3E2C3A79743B160C0F131E062B1A070B7D77797F79792C100D0B1A1B7D77797A391E130C1A796829160C161D131A39100D3C0A0C0B101216051E0B1610117D77797B2B0D0A1A7D7F79673B1A191E0A130B380D100A0F2C0A12121E0D06360B1A124B79662B1C07380D161B3B3D2B1E1D131A2C0A12121E0D06360B1A127D7579793C10130A12117D7779733833160C0B1E3B3A3D362B30797939100D121E0B7D777971534F514F4F44575F534F514F4F56797B3416111B7D77797A0C142C0A1279772F100C160B1610117D7779770C0F3910100B1A0D797C2B1E187D797D7F797639161A131B311E121A7D77797F79793C10130A12117D7779733833160C0B1E3B3A3D362B3079743B160C0F131E062B1A070B7D77797F79792C100D0B1A1B7D77797A391E130C1A796829160C161D131A39100D3C0A0C0B101216051E0B1610117D77797B2B0D0A1A7D7F79673B1A191E0A130B380D100A0F2C0A12121E0D06360B1A124A79662B1C07380D161B3B3D2B1E1D131A2C0A12121E0D06360B1A127D7579793C10130A12117D7779723833160C0B1E3C2D3A3B362B30797939100D121E0B7D777971534F514F4F44575F534F514F4F56797B3416111B7D77797A0C142C0A1279772F100C160B1610117D7779770C0F3910100B1A0D797C2B1E187D797D7F797639161A131B311E121A7D77797F79793C10130A12117D7779723833160C0B1E3C2D3A3B362B3079743B160C0F131E062B1A070B7D77797F79792C100D0B1A1B7D77797A391E130C1A796829160C161D131A39100D3C0A0C0B101216051E0B1610117D77797B2B0D0A1A7D7F', 'N', 'RETENCIONES', 'N', 'LIBRE', 5, 'N', 1, NULL, NULL)
                      MATCHING (CODIGO);

UPDATE OR INSERT INTO REPORTES (CODIGO, NOMBRE, NOTA, ARCHIVO, ADJUNTO, NATIVO, MODULO, PARAMETROS, PUBLICAR, CODGRUPO, VALIDA_TRANSITO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD)
                        VALUES ('TRI004JN', 'ANEXOS DE RETENCION EN LA FUENTE POR CUENTA Y NATURALEZA TERCERO', 'Permite imprimir los anexos de retención en un rango de  cuentas, por uno o todos los terceros, discriminando personas naturales y jurídicas por un rango de fechas dado.', 'TRI0004_JN.FR3', NULL, 'S', 'CONTABLE', 'FECHA_INICIAL,FECHA_FINAL,CUENTA_DESDE,CUENTA_HASTA,TERCERO', 'N', 'TRIBUTARIOS', 'N', 'N', 'LIBRE', 5, 'N', 1)
                      MATCHING (CODIGO);

UPDATE OR INSERT INTO INFORMES (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, FORMATO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD, DESTINO_FTP, CODREPORTE)
                        VALUES ('IMPG020201', 'RELACION RETENCIONES EN COMPRA POR TERCERO', 'Muestra la relación de retenciones en Grupo Compra, agrupado por tercero, muestra por cada documento las retenciones y sus bases por un rango de fecha dado.', 'SELECT Grupo,
       IIF(Naturaleza = ''N'', ''PERSONA NATURAL'', ''PERSONA JURIDICA'') Naturaleza,
       Tipo,
       Prefijo,
       Numero,
       Fecha,
       Tercero,
       Nombre_Tercero,
       Desde,
       Hasta,
       SUM(Base) AS Baseretefte,
       SUM(Retefuente) AS Retefuente,
       SUM(Baseriva) AS Basereteiva,
       SUM(Reteiva) AS Reteiva,
       SUM(Baseica) AS Baseica,
       SUM(Reteica) AS Reteica
FROM (SELECT F.Grupo,
             F.Tipo,
             F.Prefijo,
             LPAD(TRIM(F.Numero), 10) AS Numero,
             F.Fecha,
             TRIM(F.Tercero) AS Tercero,
             TRIM(F.Nombre_Tercero) AS Nombre_Tercero,
             Te.Naturaleza,
             F.Desde,
             F.Hasta,
             IIF(T.Clase = ''RETEFUENTE'', F.Base, 0) AS Base,
             IIF(T.Clase = ''RETEIVA'', F.Base, 0) AS Baseriva,
             IIF(T.Clase = ''RETEICA'', F.Base, 0) AS Baseica,
             IIF(T.Clase = ''RETEFUENTE'', F.No_Ligada, 0) AS Retefuente,
             IIF(T.Clase = ''RETEIVA'', F.Ligada, 0) AS Reteiva,
             IIF(T.Clase = ''RETEICA'', F.No_Ligada, 0) AS Reteica
      FROM Fx_Retenciones(:Fecha_Desde, :Fecha_Hasta) F
      JOIN Tiporetenciones T ON (F.Retencion = T.Codigo)
      JOIN Terceros Te ON (F.Tercero = Te.Codigo)
      WHERE (F.Grupo = ''COMPRA''))
GROUP BY 1, 2, 3, 4, 5, 6, 7, 8, 9, 10
ORDER BY 2, Fecha, Tipo, Prefijo, Numero, Tercero', 'S', 'GESTION', X'796F39361119100D121A0C513833160C0B1E796D2B1C07380D161B3B3D2B1E1D131A29161A087D7679793910100B1A0D7D77797B2B0D0A1A7975380D100A0F3D063D10077D77797B2B0D0A1A7973380D100A0F3910100B1A0D0C7D7D797E7E7975311A08360B1A122D10087D77797A391E130C1A79723B1E0B1E2D1008371A1618170B7D797D7F79673A1B160B39100D122A0C1A3B1A191E0A130B331E06100A0B7D77797B2B0D0A1A79733916130B1A0D3E1C0B16091A7D77797A391E130C1A79793916130B1A0D7D777969371A0745393A3939393939394F4F4F4F4F4F4F4F4F4F7978291A0D0C1610117D7D797E7E7D6379743833160C0B1E382D2A2F3079702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D80797A28161B0B177D797D4E79723E13161811121A110B37100D057D7D797E7F797A36111B1A077D797D7F797829160C161D131A7D77797B2B0D0A1A79762C100D0B300D1B1A0D7D7779790C103110111A79762C100D0B36111B1A077D797D807967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797A391E130C1A7D7F796F3833160C0B1E313E2B2A2D3E333A253E79702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D7F797A28161B0B177D797D1B79723E13161811121A110B37100D057D7D797E7F797A36111B1A077D797D7E797829160C161D131A7D77797A391E130C1A79762C100D0B300D1B1A0D7D7779740C103E0C1C1A111B16111879762C100D0B36111B1A077D797D7F7967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797B2B0D0A1A7D7F79753833160C0B1E2B362F3079702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D80797A28161B0B177D797D5779723E13161811121A110B37100D057D7D797E7F797A36111B1A077D797D7D797829160C161D131A7D77797B2B0D0A1A79762C100D0B300D1B1A0D7D7779790C103110111A79762C100D0B36111B1A077D797D807967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797A391E130C1A7D7F79723833160C0B1E2F2D3A3936353079702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D80797A28161B0B177D797D4679723E13161811121A110B37100D057D7D797E7F797A36111B1A077D797D7C797829160C161D131A7D77797B2B0D0A1A79762C100D0B300D1B1A0D7D7779790C103110111A79762C100D0B36111B1A077D797D807967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797A391E130C1A7D7F79733833160C0B1E312A323A2D3079702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D80797A28161B0B177D797D3E79723E13161811121A110B37100D057D7D797E7E797A36111B1A077D797D7B797829160C161D131A7D77797B2B0D0A1A79762C100D0B300D1B1A0D7D7779790C103110111A79762C100D0B36111B1A077D797D807967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797A391E130C1A7D7F79743833160C0B1E393A3C373E79702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D80797A28161B0B177D797D3B79723E13161811121A110B37100D057D7D797E7F797A36111B1A077D797D7A797829160C161D131A7D77797B2B0D0A1A79762C100D0B300D1B1A0D7D7779790C103110111A79762C100D0B36111B1A077D797D807967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797A391E130C1A7D7F79723833160C0B1E2B3A2D3C3A2D3079702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D80797A28161B0B177D797D3579723E13161811121A110B37100D057D7D797E7F797A36111B1A077D797D79797829160C161D131A7D77797B2B0D0A1A79762C100D0B300D1B1A0D7D7779790C103110111A79762C100D0B36111B1A077D797D807967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797B2B0D0A1A7D7F796B3833160C0B1E3130323D2D3A202B3A2D3C3A2D3079702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D80797A28161B0B177D797C7C7E79723E13161811121A110B37100D057D7D797E7F797A36111B1A077D797D78797829160C161D131A7D77797B2B0D0A1A79762C100D0B300D1B1A0D7D7779790C103110111A79762C100D0B36111B1A077D797D807967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797A391E130C1A7D7F79743833160C0B1E3B3A2C3B3A79702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D80797A28161B0B177D797D4179723E13161811121A110B37100D057D7D797E7F797A36111B1A077D797D77797829160C161D131A7D77797B2B0D0A1A79762C100D0B300D1B1A0D7D7779790C103110111A79762C100D0B36111B1A077D797D807967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797A391E130C1A7D7F79743833160C0B1E373E2C2B3E79702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D80797A28161B0B177D797D4179723E13161811121A110B37100D057D7D797E7F797A36111B1A077D797D76797829160C161D131A7D77797B2B0D0A1A79762C100D0B300D1B1A0D7D7779790C103110111A79762C100D0B36111B1A077D797D807967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797A391E130C1A7D7F796E3833160C0B1E3D3E2C3A2D3A2B3A392B3A79702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D80797A28161B0B177D797D0679723E13161811121A110B37100D057D7D797E7E797A36111B1A077D797D75797829160C161D131A7D77797B2B0D0A1A79762C100D0B300D1B1A0D7D7779790C103110111A79762C100D0B36111B1A077D797D807967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797A391E130C1A7D7F796F3833160C0B1E2D3A2B3A392A3A312B3A79702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D80797A28161B0B177D797D1179723E13161811121A110B37100D057D7D797E7E797A36111B1A077D797D74797829160C161D131A7D77797B2B0D0A1A79762C100D0B300D1B1A0D7D7779790C103110111A79762C100D0B36111B1A077D797D807967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797A391E130C1A7D7F796E3833160C0B1E3D3E2C3A2D3A2B3A36293E79702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D80797A28161B0B177D797D0979723E13161811121A110B37100D057D7D797E7E797A36111B1A077D797D73797829160C161D131A7D77797B2B0D0A1A79762C100D0B300D1B1A0D7D7779790C103110111A79762C100D0B36111B1A077D797D807967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797A391E130C1A7D7F79723833160C0B1E2D3A2B3A36293E79702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D80797A28161B0B177D797D3579723E13161811121A110B37100D057D7D797E7E797A36111B1A077D797D72797829160C161D131A7D77797B2B0D0A1A79762C100D0B300D1B1A0D7D7779790C103110111A79762C100D0B36111B1A077D797D807967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797A391E130C1A7D7F79723833160C0B1E3D3E2C3A363C3E79702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D80797A28161B0B177D797D1F79723E13161811121A110B37100D057D7D797E7E797A36111B1A077D797D71797829160C161D131A7D77797B2B0D0A1A79762C100D0B300D1B1A0D7D7779790C103110111A79762C100D0B36111B1A077D797D807967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797A391E130C1A7D7F79723833160C0B1E2D3A2B3A363C3E79702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D80797A28161B0B177D797D2379723E13161811121A110B37100D057D7D797E7E797A36111B1A077D797D70797829160C161D131A7D77797B2B0D0A1A79762C100D0B300D1B1A0D7D7779790C103110111A79762C100D0B36111B1A077D797D807967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797A391E130C1A7D7F796D3910100B1A0D2C0A12121E0D06360B1A124F79662B1C07380D161B3B3D2B1E1D131A2C0A12121E0D06360B1A127D7579793C10130A12117D77796E3833160C0B1E3D3E2C3A2D3A2B3A392B3A797939100D121E0B7D777971534F514F4F44575F534F514F4F56797B3416111B7D77797A0C142C0A1279772F100C160B1610117D7779770C0F3910100B1A0D797C2B1E187D797D7F797639161A131B311E121A7D77797F79793C10130A12117D77796E3833160C0B1E3D3E2C3A2D3A2B3A392B3A79743B160C0F131E062B1A070B7D77797F79792C100D0B1A1B7D77797A391E130C1A796829160C161D131A39100D3C0A0C0B101216051E0B1610117D77797B2B0D0A1A7D7F796D3910100B1A0D2C0A12121E0D06360B1A124E79662B1C07380D161B3B3D2B1E1D131A2C0A12121E0D06360B1A127D7579793C10130A12117D77796F3833160C0B1E2D3A2B3A392A3A312B3A797939100D121E0B7D777971534F514F4F44575F534F514F4F56797B3416111B7D77797A0C142C0A1279772F100C160B1610117D7779770C0F3910100B1A0D797C2B1E187D797D7F797639161A131B311E121A7D77797F79793C10130A12117D77796F3833160C0B1E2D3A2B3A392A3A312B3A79743B160C0F131E062B1A070B7D77797F79792C100D0B1A1B7D77797A391E130C1A796829160C161D131A39100D3C0A0C0B101216051E0B1610117D77797B2B0D0A1A7D7F796D3910100B1A0D2C0A12121E0D06360B1A124D79662B1C07380D161B3B3D2B1E1D131A2C0A12121E0D06360B1A127D7579793C10130A12117D77796E3833160C0B1E3D3E2C3A2D3A2B3A36293E797939100D121E0B7D777971534F514F4F44575F534F514F4F56797B3416111B7D77797A0C142C0A1279772F100C160B1610117D7779770C0F3910100B1A0D797C2B1E187D797D7F797639161A131B311E121A7D77797F79793C10130A12117D77796E3833160C0B1E3D3E2C3A2D3A2B3A36293E79743B160C0F131E062B1A070B7D77797F79792C100D0B1A1B7D77797A391E130C1A796829160C161D131A39100D3C0A0C0B101216051E0B1610117D77797B2B0D0A1A7D7F796D3910100B1A0D2C0A12121E0D06360B1A124C79662B1C07380D161B3B3D2B1E1D131A2C0A12121E0D06360B1A127D7579793C10130A12117D7779723833160C0B1E2D3A2B3A36293E797939100D121E0B7D777971534F514F4F44575F534F514F4F56797B3416111B7D77797A0C142C0A1279772F100C160B1610117D7779770C0F3910100B1A0D797C2B1E187D797D7F797639161A131B311E121A7D77797F79793C10130A12117D7779723833160C0B1E2D3A2B3A36293E79743B160C0F131E062B1A070B7D77797F79792C100D0B1A1B7D77797A391E130C1A796829160C161D131A39100D3C0A0C0B101216051E0B1610117D77797B2B0D0A1A7D7F796D3910100B1A0D2C0A12121E0D06360B1A124B79662B1C07380D161B3B3D2B1E1D131A2C0A12121E0D06360B1A127D7579793C10130A12117D7779723833160C0B1E3D3E2C3A363C3E797939100D121E0B7D777971534F514F4F44575F534F514F4F56797B3416111B7D77797A0C142C0A1279772F100C160B1610117D7779770C0F3910100B1A0D797C2B1E187D797D7F797639161A131B311E121A7D77797F79793C10130A12117D7779723833160C0B1E3D3E2C3A363C3E79743B160C0F131E062B1A070B7D77797F79792C100D0B1A1B7D77797A391E130C1A796829160C161D131A39100D3C0A0C0B101216051E0B1610117D77797B2B0D0A1A7D7F796D3910100B1A0D2C0A12121E0D06360B1A124A79662B1C07380D161B3B3D2B1E1D131A2C0A12121E0D06360B1A127D7579793C10130A12117D7779723833160C0B1E2D3A2B3A363C3E797939100D121E0B7D777971534F514F4F44575F534F514F4F56797B3416111B7D77797A0C142C0A1279772F100C160B1610117D7779770C0F3910100B1A0D797C2B1E187D797D7F797639161A131B311E121A7D77797F79793C10130A12117D7779723833160C0B1E2D3A2B3A363C3E79743B160C0F131E062B1A070B7D77797F79792C100D0B1A1B7D77797A391E130C1A796829160C161D131A39100D3C0A0C0B101216051E0B1610117D77797B2B0D0A1A7D7F79673B1A191E0A130B380D100A0F2C0A12121E0D06360B1A124F79662B1C07380D161B3B3D2B1E1D131A2C0A12121E0D06360B1A127D7579793C10130A12117D77796E3833160C0B1E3D3E2C3A2D3A2B3A392B3A797939100D121E0B7D777971534F514F4F44575F534F514F4F56797B3416111B7D77797A0C142C0A1279772F100C160B1610117D7779770C0F3910100B1A0D797C2B1E187D797D7F797639161A131B311E121A7D77797F79793C10130A12117D77796E3833160C0B1E3D3E2C3A2D3A2B3A392B3A79743B160C0F131E062B1A070B7D77797F79792C100D0B1A1B7D77797A391E130C1A796829160C161D131A39100D3C0A0C0B101216051E0B1610117D77797B2B0D0A1A7D7F79673B1A191E0A130B380D100A0F2C0A12121E0D06360B1A124E79662B1C07380D161B3B3D2B1E1D131A2C0A12121E0D06360B1A127D7579793C10130A12117D77796F3833160C0B1E2D3A2B3A392A3A312B3A797939100D121E0B7D777971534F514F4F44575F534F514F4F56797B3416111B7D77797A0C142C0A1279772F100C160B1610117D7779770C0F3910100B1A0D797C2B1E187D797D7F797639161A131B311E121A7D77797F79793C10130A12117D77796F3833160C0B1E2D3A2B3A392A3A312B3A79743B160C0F131E062B1A070B7D77797F79792C100D0B1A1B7D77797A391E130C1A796829160C161D131A39100D3C0A0C0B101216051E0B1610117D77797B2B0D0A1A7D7F79673B1A191E0A130B380D100A0F2C0A12121E0D06360B1A124D79662B1C07380D161B3B3D2B1E1D131A2C0A12121E0D06360B1A127D7579793C10130A12117D77796E3833160C0B1E3D3E2C3A2D3A2B3A36293E797939100D121E0B7D777971534F514F4F44575F534F514F4F56797B3416111B7D77797A0C142C0A1279772F100C160B1610117D7779770C0F3910100B1A0D797C2B1E187D797D7F797639161A131B311E121A7D77797F79793C10130A12117D77796E3833160C0B1E3D3E2C3A2D3A2B3A36293E79743B160C0F131E062B1A070B7D77797F79792C100D0B1A1B7D77797A391E130C1A796829160C161D131A39100D3C0A0C0B101216051E0B1610117D77797B2B0D0A1A7D7F79673B1A191E0A130B380D100A0F2C0A12121E0D06360B1A124C79662B1C07380D161B3B3D2B1E1D131A2C0A12121E0D06360B1A127D7579793C10130A12117D7779723833160C0B1E2D3A2B3A36293E797939100D121E0B7D777971534F514F4F44575F534F514F4F56797B3416111B7D77797A0C142C0A1279772F100C160B1610117D7779770C0F3910100B1A0D797C2B1E187D797D7F797639161A131B311E121A7D77797F79793C10130A12117D7779723833160C0B1E2D3A2B3A36293E79743B160C0F131E062B1A070B7D77797F79792C100D0B1A1B7D77797A391E130C1A796829160C161D131A39100D3C0A0C0B101216051E0B1610117D77797B2B0D0A1A7D7F79673B1A191E0A130B380D100A0F2C0A12121E0D06360B1A124B79662B1C07380D161B3B3D2B1E1D131A2C0A12121E0D06360B1A127D7579793C10130A12117D7779723833160C0B1E3D3E2C3A363C3E797939100D121E0B7D777971534F514F4F44575F534F514F4F56797B3416111B7D77797A0C142C0A1279772F100C160B1610117D7779770C0F3910100B1A0D797C2B1E187D797D7F797639161A131B311E121A7D77797F79793C10130A12117D7779723833160C0B1E3D3E2C3A363C3E79743B160C0F131E062B1A070B7D77797F79792C100D0B1A1B7D77797A391E130C1A796829160C161D131A39100D3C0A0C0B101216051E0B1610117D77797B2B0D0A1A7D7F79673B1A191E0A130B380D100A0F2C0A12121E0D06360B1A124A79662B1C07380D161B3B3D2B1E1D131A2C0A12121E0D06360B1A127D7579793C10130A12117D7779723833160C0B1E2D3A2B3A363C3E797939100D121E0B7D777971534F514F4F44575F534F514F4F56797B3416111B7D77797A0C142C0A1279772F100C160B1610117D7779770C0F3910100B1A0D797C2B1E187D797D7F797639161A131B311E121A7D77797F79793C10130A12117D7779723833160C0B1E2D3A2B3A363C3E79743B160C0F131E062B1A070B7D77797F79792C100D0B1A1B7D77797A391E130C1A796829160C161D131A39100D3C0A0C0B101216051E0B1610117D77797B2B0D0A1A7D7F', 'N', 'RETENCIONES', 'N', 'LIBRE', 5, 'N', 1, NULL, NULL)
                      MATCHING (CODIGO);
UPDATE OR INSERT INTO INFORMES (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, FORMATO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD, DESTINO_FTP, CODREPORTE)
                        VALUES ('IMPG020203', 'RELACION RETENCIONES EN COMPRA POR CUENTA', 'Muestra la relación de retenciones en Grupo Compra, agrupadas por cuentas y concepto de retención. Muestra el documento,la base, el crédito y debito, por un rango de fecha dado.', 'SELECT IIF(T.Naturaleza = ''N'', ''PERSONA NATURAL'', ''PERSONA JURIDICA'') Naturaleza,
       F.Tipo,
       F.Prefijo,
       LPAD(TRIM(F.Numero), 10) AS Numero,
       TRIM(F.Retencion) || ''   '' || F.Nombre_Retencion AS Retencion,
       TRIM(F.Cuenta) || ''   '' || F.Nombre_Cuenta AS Cuenta,
       F.Base,
       F.Credito,
       F.Debito
FROM Fx_Retenciones(:Fecha_Desde, :Fecha_Hasta) F
JOIN Documentos D ON (F.Tipo = D.Codigo)
JOIN Terceros T ON (F.Tercero = T.Codigo)
WHERE (D.Grupo = ''COMPRA''
      AND D.Publica_Retenciones = ''S'')
ORDER BY 1, 6, 5, 2, 3, 4', 'S', 'GESTION', X'796F39361119100D121A0C513833160C0B1E796D2B1C07380D161B3B3D2B1E1D131A29161A087D7679793910100B1A0D7D77797A391E130C1A7975380D100A0F3D063D10077D77797B2B0D0A1A7973380D100A0F3910100B1A0D0C7D7D797E7F7975311A08360B1A122D10087D77797A391E130C1A79723B1E0B1E2D1008371A1618170B7D797D7F79673A1B160B39100D122A0C1A3B1A191E0A130B331E06100A0B7D77797B2B0D0A1A79733916130B1A0D3E1C0B16091A7D77797A391E130C1A79793916130B1A0D7D777969371A0745393A3939393939394F4F4F4F4F4F4F4F4F4F7978291A0D0C1610117D7D797E7E7D7679703833160C0B1E2D3A2B3A313C36303179702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D80797A28161B0B177D797C277E79723E13161811121A110B37100D057D7D797E7F797A36111B1A077D797D7F797829160C161D131A7D77797B2B0D0A1A79762C100D0B300D1B1A0D7D7779740C103E0C1C1A111B16111879762C100D0B36111B1A077D797D7E7967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797B2B0D0A1A7D7F79753833160C0B1E2B362F3079702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D80797A28161B0B177D797D6279723E13161811121A110B37100D057D7D797E7F797A36111B1A077D797D7E797829160C161D131A7D77797B2B0D0A1A79762C100D0B300D1B1A0D7D7779790C103110111A79762C100D0B36111B1A077D797D807967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797A391E130C1A7D7F79723833160C0B1E2F2D3A3936353079702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D80797A28161B0B177D797D4479723E13161811121A110B37100D057D7D797E7F797A36111B1A077D797D7D797829160C161D131A7D77797B2B0D0A1A79762C100D0B300D1B1A0D7D7779790C103110111A79762C100D0B36111B1A077D797D807967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797A391E130C1A7D7F79733833160C0B1E312A323A2D3079702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D80797A28161B0B177D797D3E79723E13161811121A110B37100D057D7D797E7E797A36111B1A077D797D7C797829160C161D131A7D77797B2B0D0A1A79762C100D0B300D1B1A0D7D7779790C103110111A79762C100D0B36111B1A077D797D807967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797A391E130C1A7D7F79753833160C0B1E3D3E2C3A79702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D80797A28161B0B177D797D2379723E13161811121A110B37100D057D7D797E7E797A36111B1A077D797D7B797829160C161D131A7D77797B2B0D0A1A79762C100D0B300D1B1A0D7D7779790C103110111A79762C100D0B36111B1A077D797D807967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797A391E130C1A7D7F79733833160C0B1E3B3A3D362B3079702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D80797A28161B0B177D797D1379723E13161811121A110B37100D057D7D797E7E797A36111B1A077D797D7A797829160C161D131A7D77797B2B0D0A1A79762C100D0B300D1B1A0D7D7779790C103110111A79762C100D0B36111B1A077D797D807967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797A391E130C1A7D7F79723833160C0B1E3C2D3A3B362B3079702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D80797A28161B0B177D797D1B79723E13161811121A110B37100D057D7D797E7E797A36111B1A077D797D79797829160C161D131A7D77797B2B0D0A1A79762C100D0B300D1B1A0D7D7779790C103110111A79762C100D0B36111B1A077D797D807967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797A391E130C1A7D7F79733833160C0B1E3C2A3A312B3E79702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D7F797A28161B0B177D797CD57E79723E13161811121A110B37100D057D7D797E7F797A36111B1A077D797D78797829160C161D131A7D77797A391E130C1A79762C100D0B300D1B1A0D7D7779740C103E0C1C1A111B16111879762C100D0B36111B1A077D797D7F7967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797B2B0D0A1A7D7F796F3833160C0B1E313E2B2A2D3E333A253E79702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D7E797A28161B0B177D797D1B79723E13161811121A110B37100D057D7D797E7F797A36111B1A077D797D77797829160C161D131A7D77797A391E130C1A79762C100D0B300D1B1A0D7D7779740C103E0C1C1A111B16111879762C100D0B36111B1A077D797D807967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797B2B0D0A1A7D7F', 'N', 'RETENCIONES', 'N', 'LIBRE', 5, 'N', 1, NULL, NULL)
                      MATCHING (CODIGO);
UPDATE OR INSERT INTO INFORMES (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, FORMATO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD, DESTINO_FTP, CODREPORTE)
                        VALUES ('IMPG020202', 'RELACION RETENCIONES EN COMPRA POR CONCEPTO', 'Muestra la relación de retenciones en Grupo Compra, agrupadas por concepto de retención. Muestra el documento,la base, el crédito y debito, por tercero y por un rango de fecha dado.', 'SELECT T.Clase,
       IIF(Te.Naturaleza = ''N'', ''PERSONA NATURAL'', ''PERSONA JURIDICA'') Naturaleza,
       F.Fecha,
       F.Tipo,
       F.Prefijo,
       LPAD(TRIM(F.Numero), 10) AS Numero,
       F.Tercero,
       TRIM(F.Nombre_Tercero) AS "NOMBRE TERCERO",
       (T.Codigo || ''   '' || T.Nombre) AS "TIPO RETENCION",
       F.Base,
       F.Debito,
       F.Credito
FROM Fx_Retenciones(:Fecha_Desde, :Fecha_Hasta) F
JOIN Tiporetenciones T ON (F.Retencion = T.Codigo)
JOIN Terceros Te ON (F.Tercero = Te.Codigo)
WHERE (F.Grupo = ''COMPRA'')
ORDER BY 7, F.Fecha, F.Tipo, F.Prefijo, LPAD(TRIM(F.Numero), 10), F.Nombre_Tercero, F.Tercero', 'S', 'GESTION', X'796F39361119100D121A0C513833160C0B1E796D2B1C07380D161B3B3D2B1E1D131A29161A087D7679793910100B1A0D7D77797A391E130C1A7975380D100A0F3D063D10077D77797B2B0D0A1A7973380D100A0F3910100B1A0D0C7D7D797E7F7975311A08360B1A122D10087D77797A391E130C1A79723B1E0B1E2D1008371A1618170B7D797D7F79673A1B160B39100D122A0C1A3B1A191E0A130B331E06100A0B7D77797B2B0D0A1A79733916130B1A0D3E1C0B16091A7D77797A391E130C1A79793916130B1A0D7D777969371A0745393A3939393939394F4F4F4F4F4F4F4F4F4F7978291A0D0C1610117D7D797E7E7D7379743833160C0B1E3C333E2C3A79702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D80797A28161B0B177D797D0379723E13161811121A110B37100D057D7D797E7F797A36111B1A077D797D7F797829160C161D131A7D77797B2B0D0A1A79762C100D0B300D1B1A0D7D7779790C103110111A79762C100D0B36111B1A077D797D807967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797A391E130C1A7D7F796F3833160C0B1E313E2B2A2D3E333A253E79702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D7E797A28161B0B177D797D1B79723E13161811121A110B37100D057D7D797E7F797A36111B1A077D797D7E797829160C161D131A7D77797A391E130C1A79762C100D0B300D1B1A0D7D7779740C103E0C1C1A111B16111879762C100D0B36111B1A077D797D807967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797B2B0D0A1A7D7F79743833160C0B1E393A3C373E79702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D80797A28161B0B177D797D3F79723E13161811121A110B37100D057D7D797E7F797A36111B1A077D797D7D797829160C161D131A7D77797B2B0D0A1A79762C100D0B300D1B1A0D7D7779790C103110111A79762C100D0B36111B1A077D797D807967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797A391E130C1A7D7F79753833160C0B1E2B362F3079702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D80797A28161B0B177D797D5D79723E13161811121A110B37100D057D7D797E7F797A36111B1A077D797D7C797829160C161D131A7D77797B2B0D0A1A79762C100D0B300D1B1A0D7D7779790C103110111A79762C100D0B36111B1A077D797D807967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797A391E130C1A7D7F79723833160C0B1E2F2D3A3936353079702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D80797A28161B0B177D797D5D79723E13161811121A110B37100D057D7D797E7F797A36111B1A077D797D7B797829160C161D131A7D77797B2B0D0A1A79762C100D0B300D1B1A0D7D7779790C103110111A79762C100D0B36111B1A077D797D807967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797A391E130C1A7D7F79733833160C0B1E312A323A2D3079702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D80797A28161B0B177D797D3F79723E13161811121A110B37100D057D7D797E7E797A36111B1A077D797D7A797829160C161D131A7D77797B2B0D0A1A79762C100D0B300D1B1A0D7D7779790C103110111A79762C100D0B36111B1A077D797D807967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797A391E130C1A7D7F79723833160C0B1E2B3A2D3C3A2D3079702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D80797A28161B0B177D797D2179723E13161811121A110B37100D057D7D797E7F797A36111B1A077D797D79797829160C161D131A7D77797B2B0D0A1A79762C100D0B300D1B1A0D7D7779790C103110111A79762C100D0B36111B1A077D797D807967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797A391E130C1A7D7F796C3833160C0B1E3130323D2D3A2B3A2D3C3A2D3079702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D80797A28161B0B177D797CA37C79723E13161811121A110B37100D057D7D797E7F797A36111B1A077D797D78797829160C161D131A7D77797B2B0D0A1A79762C100D0B300D1B1A0D7D7779790C103110111A79762C100D0B36111B1A077D797D807967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797A391E130C1A7D7F796C3833160C0B1E2B362F302D3A2B3A313C36303179702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D7F797A28161B0B177D797C6B7D79723E13161811121A110B37100D057D7D797E7F797A36111B1A077D797D77797829160C161D131A7D77797A391E130C1A79762C100D0B300D1B1A0D7D7779740C103E0C1C1A111B16111879762C100D0B36111B1A077D797D807967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797B2B0D0A1A7D7F79753833160C0B1E3D3E2C3A79702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D80797A28161B0B177D797D3F79723E13161811121A110B37100D057D7D797E7E797A36111B1A077D797D76797829160C161D131A7D77797B2B0D0A1A79762C100D0B300D1B1A0D7D7779790C103110111A79762C100D0B36111B1A077D797D807967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797A391E130C1A7D7F79733833160C0B1E3B3A3D362B3079702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D80797A28161B0B177D797D3F79723E13161811121A110B37100D057D7D797E7E797A36111B1A077D797D75797829160C161D131A7D77797B2B0D0A1A79762C100D0B300D1B1A0D7D7779790C103110111A79762C100D0B36111B1A077D797D807967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797A391E130C1A7D7F79723833160C0B1E3C2D3A3B362B3079702B1C07380D161B3B3D3C10130A12117D777975380D100A0F36111B1A077D797D80797A28161B0B177D797D3F79723E13161811121A110B37100D057D7D797E7E797A36111B1A077D797D74797829160C161D131A7D77797B2B0D0A1A79762C100D0B300D1B1A0D7D7779790C103110111A79762C100D0B36111B1A077D797D807967281E0C29160C161D131A3D1A19100D1A380D100A0F1611187D77797A391E130C1A7D7F', 'N', 'RETENCIONES', 'N', 'LIBRE', 5, 'N', 1, NULL, NULL)
                      MATCHING (CODIGO);


COMMIT WORK;

UPDATE OR INSERT INTO PROCESOS (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, CODGRUPO, NIVEL, TABLERO, PRIORIDAD)
                        VALUES ('P90000', 'ORGANIZAR NUMERO DE CONTROL PARA VOLVER A EJECUTAR EL AUTOCONTENIDO', 'Luego el supervisor debera cerrar el programa totalmente, reiniciando el mekano server.
por ultimo, volver a ingresar a la empresa y tan solo esperar a que el proceso de autocontenido concluya', 'UPDATE COMPUTADORES SET ID_CONTENIDO=ID_CONTENIDO-1 WHERE COMPUTADOR=''SCRIPT'';', 'N', 'HERRAMIENT', 'RUTINAS ESTANDAR', 5, 'N', 1)
                      MATCHING (CODIGO);


COMMIT WORK;


UPDATE OR INSERT INTO REPORTES (CODIGO, NOMBRE, NOTA, ARCHIVO, ADJUNTO, NATIVO, MODULO, PARAMETROS, PUBLICAR, CODGRUPO, VALIDA_TRANSITO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD)
                        VALUES ('LOGTER030', 'RELACION LOG DE CAMBIOS TABLA TERCEROS x USUARIO Y RANGO FECHA', 'Muestra cambios sobre la tabla Terceros.  Basta con indicar rango de fecha, por un usuario o un tercero o por todos', 'LOG_CAMBIOS_TER.FR3', NULL, 'S', 'HERRAMIENT', '_DESDE,_HASTA,CODUSUARIO,TERCERO', 'N', 'AUDITORIAS', 'N', 'N', 'LIBRE', 5, 'N', 1)
                      MATCHING (CODIGO);


COMMIT WORK;

UPDATE OR INSERT INTO INFORMES (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, FORMATO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD, DESTINO_FTP, CODREPORTE)
                        VALUES ('LOGTER030', 'RELACION LOG DE CAMBIOS TABLA TERCEROS x USUARIO Y RANGO FECHA', 'Muestra cambios sobre la tabla Terceros.  Basta con indicar rango de fecha, por un usuario o un tercero o por todos', 'SELECT L.Fecha AS "FECHA Y HORA",
       L.USUARIO,L.TERCERO,
       CAST(REPLACE(REPLACE(SUBSTRING(TRIM(L.Cambio) FROM 1 FOR 1000), ASCII_CHAR(10), '' ''), ASCII_CHAR(13), ''   -   '') AS CHAR(3000)) AS Cambio
FROM Log_Terceros L
WHERE ((CAST(L.Fecha AS DATE) >= :_Desde)
      AND (CAST(L.Fecha AS DATE) <= :_Hasta))
      AND (TRIM(L.Usuario) LIKE :Codusuario)
      AND (TRIM(L.Tercero) LIKE :Tercero)', 'S', 'HERRAMIENT', NULL, 'N', 'AUDITORIAS', 'N', 'LIBRE', 5, 'N', 1, NULL, NULL)
                      MATCHING (CODIGO);


COMMIT WORK;


/* BILLETES */
DELETE FROM BILLETES;

COMMIT WORK;

INSERT INTO BILLETES(NOMBRE,FACTOR) VALUES('BILLETE DE CIEN MIL','100000');
INSERT INTO BILLETES(NOMBRE,FACTOR) VALUES('BILLETE DE CINCUENTA MIL','50000');
INSERT INTO BILLETES(NOMBRE,FACTOR) VALUES('BILLETE DE VEINTE MIL','20000');
INSERT INTO BILLETES(NOMBRE,FACTOR) VALUES('BILLETE DE DIEZ MIL','10000');
INSERT INTO BILLETES(NOMBRE,FACTOR) VALUES('BILLETE DE CINCO MIL','5000');
INSERT INTO BILLETES(NOMBRE,FACTOR) VALUES('BILLETE DE DOS MIL','2000');
INSERT INTO BILLETES(NOMBRE,FACTOR) VALUES('BILLETE DE MIL','1000');
INSERT INTO BILLETES(NOMBRE,FACTOR) VALUES('MONEDA DE MIL','1000');
INSERT INTO BILLETES(NOMBRE,FACTOR) VALUES('MONEDA DE QUINIENTOS','500');
INSERT INTO BILLETES(NOMBRE,FACTOR) VALUES('MONEDA DE DOSCIENTOS','200');
INSERT INTO BILLETES(NOMBRE,FACTOR) VALUES('MONEDA DE CIEN','100');
INSERT INTO BILLETES(NOMBRE,FACTOR) VALUES('MONEDA DE CINCUENTA','50');

COMMIT WORK;


-- Sector Salud
UPDATE Documentos D
SET D.Valida_Saldos = 'S', D.Codigo_Fe = 'POS ELECTRONICO',
    D.Nombre = 'SISTEMA P.O.S. / SS-RECAUDO POS',
    D.Formato1 = 'F_FVE_T76_02A.FR3', D.Formato2 = 'F_FVE_T80_02A.FR3', D.Formato3 = 'F_FVE_T57_02A.FR3'
WHERE Codigo = 'FEVS2';

UPDATE Documentos D
SET D.C_Codreferido = 'N'
WHERE D.Salud = 'S';

COMMIT WORK;

-- CARGOS
UPDATE OR INSERT INTO CARGOS (CODIGO, NOMBRE)
                      VALUES ('XMLRE', 'XML DIAN REDONDEADO')
                    MATCHING (CODIGO);
UPDATE OR INSERT INTO CARGOS (CODIGO, NOMBRE)
                      VALUES ('FECRE', 'FACTURACION CONTADO CUANDO SOLO FE CREDITO')
                    MATCHING (CODIGO);
UPDATE OR INSERT INTO CARGOS (CODIGO, NOMBRE)
                      VALUES ('DSCOR', 'CORREO PARA DOCUMENTO SOPORTE')
                    MATCHING (CODIGO);
UPDATE OR INSERT INTO CARGOS (CODIGO, NOMBRE)
                      VALUES ('NECOR', 'CORREO PARA NE')
                    MATCHING (CODIGO);
UPDATE OR INSERT INTO CARGOS (CODIGO, NOMBRE)
                      VALUES ('XMLSU', 'XML SIN SUCURSALES')
                    MATCHING (CODIGO);
UPDATE OR INSERT INTO CARGOS (CODIGO, NOMBRE)
                      VALUES ('RFISC', 'RESPONSABILIDAD FISCAL')
                    MATCHING (CODIGO);


COMMIT WORK;


/******************************************************************************/
/***                     Certificado de Declaracion 350                     ***/
/******************************************************************************/
UPDATE OR INSERT INTO DECLARACIONES (CODIGO, NOMBRE, REPORTE)
                             VALUES ('350_2024', 'Declaracion Mensual de Retenciones en la Fuente', 'DECLA_350_2024.FR3')
                           MATCHING (CODIGO);


COMMIT WORK;


UPDATE OR INSERT INTO RENGLONES (CODIGO, NOMBRE, SALDO, FORMULA, CODDECLARACION)
                         VALUES ('RET24_029', 'J - Honorarios', 'BASE', NULL, '350_2024')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO RENGLONES (CODIGO, NOMBRE, SALDO, FORMULA, CODDECLARACION)
                         VALUES ('RET24_030', 'J - Comisiones', 'BASE', NULL, '350_2024')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO RENGLONES (CODIGO, NOMBRE, SALDO, FORMULA, CODDECLARACION)
                         VALUES ('RET24_031', 'J - Servicios', 'BASE', NULL, '350_2024')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO RENGLONES (CODIGO, NOMBRE, SALDO, FORMULA, CODDECLARACION)
                         VALUES ('RET24_032', 'J - Rendimientos financieros e intereses', 'BASE', NULL, '350_2024')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO RENGLONES (CODIGO, NOMBRE, SALDO, FORMULA, CODDECLARACION)
                         VALUES ('RET24_033', 'J - Arrendamientos (Muebles e inmuebles)', 'BASE', NULL, '350_2024')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO RENGLONES (CODIGO, NOMBRE, SALDO, FORMULA, CODDECLARACION)
                         VALUES ('RET24_034', 'J - Regalias y explotacion de la propiedad intelectual', 'BASE', NULL, '350_2024')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO RENGLONES (CODIGO, NOMBRE, SALDO, FORMULA, CODDECLARACION)
                         VALUES ('RET24_035', 'J - Dividendos y participaciones', 'BASE', NULL, '350_2024')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO RENGLONES (CODIGO, NOMBRE, SALDO, FORMULA, CODDECLARACION)
                         VALUES ('RET24_036', 'J - Compras', 'BASE', NULL, '350_2024')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO RENGLONES (CODIGO, NOMBRE, SALDO, FORMULA, CODDECLARACION)
                         VALUES ('RET24_037', 'J - Transacciones con tarjetas debito y credito', 'BASE', NULL, '350_2024')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO RENGLONES (CODIGO, NOMBRE, SALDO, FORMULA, CODDECLARACION)
                         VALUES ('RET24_038', 'J - Contratos de construccion', 'BASE', NULL, '350_2024')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO RENGLONES (CODIGO, NOMBRE, SALDO, FORMULA, CODDECLARACION)
                         VALUES ('RET24_039', 'J - Loterias  rifas  apuestas y similares', 'BASE', NULL, '350_2024')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO RENGLONES (CODIGO, NOMBRE, SALDO, FORMULA, CODDECLARACION)
                         VALUES ('RET24_040', 'J - Hidrocarburos, carbon y demas productos', 'BASE', NULL, '350_2024')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO RENGLONES (CODIGO, NOMBRE, SALDO, FORMULA, CODDECLARACION)
                         VALUES ('RET24_041', 'J - Otras pagos sujetos a retencion', 'BASE', NULL, '350_2024')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO RENGLONES (CODIGO, NOMBRE, SALDO, FORMULA, CODDECLARACION)
                         VALUES ('RET24_042', 'J - Honorarios', 'MOVIMIENTO', NULL, '350_2024')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO RENGLONES (CODIGO, NOMBRE, SALDO, FORMULA, CODDECLARACION)
                         VALUES ('RET24_043', 'J - Comisiones', 'MOVIMIENTO', NULL, '350_2024')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO RENGLONES (CODIGO, NOMBRE, SALDO, FORMULA, CODDECLARACION)
                         VALUES ('RET24_044', 'J - Servicios', 'MOVIMIENTO', NULL, '350_2024')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO RENGLONES (CODIGO, NOMBRE, SALDO, FORMULA, CODDECLARACION)
                         VALUES ('RET24_045', 'J - Rendimientos financieros', 'MOVIMIENTO', NULL, '350_2024')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO RENGLONES (CODIGO, NOMBRE, SALDO, FORMULA, CODDECLARACION)
                         VALUES ('RET24_046', 'J - Arrendamientos (Muebles e inmuebles)', 'MOVIMIENTO', NULL, '350_2024')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO RENGLONES (CODIGO, NOMBRE, SALDO, FORMULA, CODDECLARACION)
                         VALUES ('RET24_047', 'J - Regalias y explotacion de la propiedad intelectual', 'MOVIMIENTO', NULL, '350_2024')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO RENGLONES (CODIGO, NOMBRE, SALDO, FORMULA, CODDECLARACION)
                         VALUES ('RET24_048', 'J - Dividendos y participaciones', 'MOVIMIENTO', NULL, '350_2024')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO RENGLONES (CODIGO, NOMBRE, SALDO, FORMULA, CODDECLARACION)
                         VALUES ('RET24_049', 'J - Compras', 'MOVIMIENTO', NULL, '350_2024')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO RENGLONES (CODIGO, NOMBRE, SALDO, FORMULA, CODDECLARACION)
                         VALUES ('RET24_050', 'J - Transacciones con tarjetas debito y credito', 'MOVIMIENTO', NULL, '350_2024')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO RENGLONES (CODIGO, NOMBRE, SALDO, FORMULA, CODDECLARACION)
                         VALUES ('RET24_051', 'J - Contratos de construccion', 'MOVIMIENTO', NULL, '350_2024')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO RENGLONES (CODIGO, NOMBRE, SALDO, FORMULA, CODDECLARACION)
                         VALUES ('RET24_052', 'J - Loterias, rifas, apuestas y similares', 'MOVIMIENTO', NULL, '350_2024')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO RENGLONES (CODIGO, NOMBRE, SALDO, FORMULA, CODDECLARACION)
                         VALUES ('RET24_053', 'J - Hidrocarburos, carbon y demas productos', 'MOVIMIENTO', NULL, '350_2024')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO RENGLONES (CODIGO, NOMBRE, SALDO, FORMULA, CODDECLARACION)
                         VALUES ('RET24_054', 'J - Otros pagos sujetos a retencion', 'MOVIMIENTO', NULL, '350_2024')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO RENGLONES (CODIGO, NOMBRE, SALDO, FORMULA, CODDECLARACION)
                         VALUES ('RET24_055', 'J - Pagos o abonos en cuenta al exterior a paises sin convenio', 'BASE', NULL, '350_2024')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO RENGLONES (CODIGO, NOMBRE, SALDO, FORMULA, CODDECLARACION)
                         VALUES ('RET24_056', 'J - Pagos o abonos en cuenta al exterior a paises con convenio vigente', 'BASE', NULL, '350_2024')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO RENGLONES (CODIGO, NOMBRE, SALDO, FORMULA, CODDECLARACION)
                         VALUES ('RET24_057', 'J - Pagos o abonos en cuenta al exterior a paises sin convenio', 'MOVIMIENTO', NULL, '350_2024')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO RENGLONES (CODIGO, NOMBRE, SALDO, FORMULA, CODDECLARACION)
                         VALUES ('RET24_058', 'J - Pagos o abonos en cuenta al exterior a paises con convenio vigente', 'MOVIMIENTO', NULL, '350_2024')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO RENGLONES (CODIGO, NOMBRE, SALDO, FORMULA, CODDECLARACION)
                         VALUES ('RET24_059', 'J - Contribuyentes exonerados de aportes (art. 114-1 E.T.)', 'BASE', NULL, '350_2024')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO RENGLONES (CODIGO, NOMBRE, SALDO, FORMULA, CODDECLARACION)
                         VALUES ('RET24_060', 'J - Ventas', 'BASE', NULL, '350_2024')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO RENGLONES (CODIGO, NOMBRE, SALDO, FORMULA, CODDECLARACION)
                         VALUES ('RET24_061', 'J - Honorarios', 'BASE', NULL, '350_2024')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO RENGLONES (CODIGO, NOMBRE, SALDO, FORMULA, CODDECLARACION)
                         VALUES ('RET24_062', 'J - Comisiones', 'BASE', NULL, '350_2024')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO RENGLONES (CODIGO, NOMBRE, SALDO, FORMULA, CODDECLARACION)
                         VALUES ('RET24_063', 'J - Servicios', 'BASE', NULL, '350_2024')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO RENGLONES (CODIGO, NOMBRE, SALDO, FORMULA, CODDECLARACION)
                         VALUES ('RET24_064', 'J - Rendimientos financieros', 'BASE', NULL, '350_2024')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO RENGLONES (CODIGO, NOMBRE, SALDO, FORMULA, CODDECLARACION)
                         VALUES ('RET24_065', 'J - Pagos men. provisionales de car vol (hidrocarburos y demas prod mineros)', 'BASE', NULL, '350_2024')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO RENGLONES (CODIGO, NOMBRE, SALDO, FORMULA, CODDECLARACION)
                         VALUES ('RET24_066', 'J - Exportacion de hidrocarburos, carbon y demas pruductos mineros', 'BASE', NULL, '350_2024')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO RENGLONES (CODIGO, NOMBRE, SALDO, FORMULA, CODDECLARACION)
                         VALUES ('RET24_067', 'J - Otros conceptos', 'BASE', NULL, '350_2024')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO RENGLONES (CODIGO, NOMBRE, SALDO, FORMULA, CODDECLARACION)
                         VALUES ('RET24_068', 'J - Contribuyentes exonerados de aportes (art. 114-1 E.T.)', 'MOVIMIENTO', NULL, '350_2024')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO RENGLONES (CODIGO, NOMBRE, SALDO, FORMULA, CODDECLARACION)
                         VALUES ('RET24_069', 'J - Ventas', 'MOVIMIENTO', NULL, '350_2024')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO RENGLONES (CODIGO, NOMBRE, SALDO, FORMULA, CODDECLARACION)
                         VALUES ('RET24_070', 'J - Honorarios', 'MOVIMIENTO', NULL, '350_2024')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO RENGLONES (CODIGO, NOMBRE, SALDO, FORMULA, CODDECLARACION)
                         VALUES ('RET24_071', 'J - Comisiones', 'MOVIMIENTO', NULL, '350_2024')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO RENGLONES (CODIGO, NOMBRE, SALDO, FORMULA, CODDECLARACION)
                         VALUES ('RET24_072', 'J - Servicios', 'MOVIMIENTO', NULL, '350_2024')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO RENGLONES (CODIGO, NOMBRE, SALDO, FORMULA, CODDECLARACION)
                         VALUES ('RET24_073', 'J - Rendimientos financieros', 'MOVIMIENTO', NULL, '350_2024')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO RENGLONES (CODIGO, NOMBRE, SALDO, FORMULA, CODDECLARACION)
                         VALUES ('RET24_074', 'J - Pagos men. provisionales de car vol (hidrocarburos y demas prod mineros)', 'MOVIMIENTO', NULL, '350_2024')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO RENGLONES (CODIGO, NOMBRE, SALDO, FORMULA, CODDECLARACION)
                         VALUES ('RET24_075', 'J - Exportacion de hidrocarburos, carbon y demas pruductos mineros', 'MOVIMIENTO', NULL, '350_2024')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO RENGLONES (CODIGO, NOMBRE, SALDO, FORMULA, CODDECLARACION)
                         VALUES ('RET24_076', 'J - Otros conceptos', 'MOVIMIENTO', NULL, '350_2024')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO RENGLONES (CODIGO, NOMBRE, SALDO, FORMULA, CODDECLARACION)
                         VALUES ('RET24_077', 'N - Rentas de trabajo', 'BASE', NULL, '350_2024')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO RENGLONES (CODIGO, NOMBRE, SALDO, FORMULA, CODDECLARACION)
                         VALUES ('RET24_078', 'N - Rentas de pensiones', 'BASE', NULL, '350_2024')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO RENGLONES (CODIGO, NOMBRE, SALDO, FORMULA, CODDECLARACION)
                         VALUES ('RET24_079', 'N - Honorarios', 'BASE', NULL, '350_2024')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO RENGLONES (CODIGO, NOMBRE, SALDO, FORMULA, CODDECLARACION)
                         VALUES ('RET24_080', 'N - Comisiones', 'BASE', NULL, '350_2024')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO RENGLONES (CODIGO, NOMBRE, SALDO, FORMULA, CODDECLARACION)
                         VALUES ('RET24_081', 'N - Servicios', 'BASE', NULL, '350_2024')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO RENGLONES (CODIGO, NOMBRE, SALDO, FORMULA, CODDECLARACION)
                         VALUES ('RET24_082', 'N - Rendimientos financieros e intereses', 'BASE', NULL, '350_2024')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO RENGLONES (CODIGO, NOMBRE, SALDO, FORMULA, CODDECLARACION)
                         VALUES ('RET24_083', 'N - Arrendamientos (Muebles e inmuebles)', 'BASE', NULL, '350_2024')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO RENGLONES (CODIGO, NOMBRE, SALDO, FORMULA, CODDECLARACION)
                         VALUES ('RET24_087', 'N - Transacciones con tarjetas debito y credito', 'BASE', NULL, '350_2024')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO RENGLONES (CODIGO, NOMBRE, SALDO, FORMULA, CODDECLARACION)
                         VALUES ('RET24_088', 'N - Contratos de construccion', 'BASE', NULL, '350_2024')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO RENGLONES (CODIGO, NOMBRE, SALDO, FORMULA, CODDECLARACION)
                         VALUES ('RET24_089', 'N - Enajenacion de activos fijos de per. naturales', 'BASE', NULL, '350_2024')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO RENGLONES (CODIGO, NOMBRE, SALDO, FORMULA, CODDECLARACION)
                         VALUES ('RET24_980', 'PAGO TOTAL (digitar manualmente en el campo formula el valor a pagar)', '', NULL, '350_2024')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO RENGLONES (CODIGO, NOMBRE, SALDO, FORMULA, CODDECLARACION)
                         VALUES ('RET24_086', 'N - Compras', 'BASE', NULL, '350_2024')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO RENGLONES (CODIGO, NOMBRE, SALDO, FORMULA, CODDECLARACION)
                         VALUES ('RET24_085', 'N - Dividendos y participaciones', 'BASE', NULL, '350_2024')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO RENGLONES (CODIGO, NOMBRE, SALDO, FORMULA, CODDECLARACION)
                         VALUES ('RET24_084', 'N - Regalias y explotacion de la propiedad intelectual', 'BASE', NULL, '350_2024')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO RENGLONES (CODIGO, NOMBRE, SALDO, FORMULA, CODDECLARACION)
                         VALUES ('RET24_095', 'N - Honorarios', 'MOVIMIENTO', NULL, '350_2024')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO RENGLONES (CODIGO, NOMBRE, SALDO, FORMULA, CODDECLARACION)
                         VALUES ('RET24_096', 'N - Comisiones', 'MOVIMIENTO', NULL, '350_2024')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO RENGLONES (CODIGO, NOMBRE, SALDO, FORMULA, CODDECLARACION)
                         VALUES ('RET24_097', 'N - Servicios', 'MOVIMIENTO', NULL, '350_2024')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO RENGLONES (CODIGO, NOMBRE, SALDO, FORMULA, CODDECLARACION)
                         VALUES ('RET24_098', 'N - Rendimientos financieros e intereses', 'MOVIMIENTO', NULL, '350_2024')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO RENGLONES (CODIGO, NOMBRE, SALDO, FORMULA, CODDECLARACION)
                         VALUES ('RET24_099', 'N - Arrendamientos (Muebles e inmuebles)', 'MOVIMIENTO', NULL, '350_2024')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO RENGLONES (CODIGO, NOMBRE, SALDO, FORMULA, CODDECLARACION)
                         VALUES ('RET24_100', 'N - Regalias y explotacion de la propiedad', 'MOVIMIENTO', NULL, '350_2024')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO RENGLONES (CODIGO, NOMBRE, SALDO, FORMULA, CODDECLARACION)
                         VALUES ('RET24_101', 'N - Dividendos y participaciones', 'MOVIMIENTO', NULL, '350_2024')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO RENGLONES (CODIGO, NOMBRE, SALDO, FORMULA, CODDECLARACION)
                         VALUES ('RET24_102', 'N - Compras', 'MOVIMIENTO', NULL, '350_2024')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO RENGLONES (CODIGO, NOMBRE, SALDO, FORMULA, CODDECLARACION)
                         VALUES ('RET24_103', 'N - Transacciones con tarjetas debito y credito', 'MOVIMIENTO', NULL, '350_2024')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO RENGLONES (CODIGO, NOMBRE, SALDO, FORMULA, CODDECLARACION)
                         VALUES ('RET24_104', 'N - Contratos de construccion', 'MOVIMIENTO', NULL, '350_2024')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO RENGLONES (CODIGO, NOMBRE, SALDO, FORMULA, CODDECLARACION)
                         VALUES ('RET24_105', 'N - Enajenacion de activos fijos de per. naturales', 'MOVIMIENTO', NULL, '350_2024')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO RENGLONES (CODIGO, NOMBRE, SALDO, FORMULA, CODDECLARACION)
                         VALUES ('RET24_090', 'N - Loterias, rifas, apuestas y similares', 'BASE', NULL, '350_2024')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO RENGLONES (CODIGO, NOMBRE, SALDO, FORMULA, CODDECLARACION)
                         VALUES ('RET24_106', 'N - Loterias, rifas, apuestas y similares', 'MOVIMIENTO', NULL, '350_2024')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO RENGLONES (CODIGO, NOMBRE, SALDO, FORMULA, CODDECLARACION)
                         VALUES ('RET24_091', 'N - Hidrocarburos, carbon y demas productos', 'BASE', NULL, '350_2024')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO RENGLONES (CODIGO, NOMBRE, SALDO, FORMULA, CODDECLARACION)
                         VALUES ('RET24_107', 'N - Hidrocarburos, carbon y demas productos', 'MOVIMIENTO', NULL, '350_2024')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO RENGLONES (CODIGO, NOMBRE, SALDO, FORMULA, CODDECLARACION)
                         VALUES ('RET24_093', 'N - Rentas de trabajo', 'MOVIMIENTO', NULL, '350_2024')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO RENGLONES (CODIGO, NOMBRE, SALDO, FORMULA, CODDECLARACION)
                         VALUES ('RET24_094', 'N - Rentas de pensiones', 'MOVIMIENTO', NULL, '350_2024')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO RENGLONES (CODIGO, NOMBRE, SALDO, FORMULA, CODDECLARACION)
                         VALUES ('RET24_092', 'N - Otros pagos sujetos a retencion', 'BASE', NULL, '350_2024')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO RENGLONES (CODIGO, NOMBRE, SALDO, FORMULA, CODDECLARACION)
                         VALUES ('RET24_108', 'N - Otros pagos sujetos a retencion', 'MOVIMIENTO', NULL, '350_2024')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO RENGLONES (CODIGO, NOMBRE, SALDO, FORMULA, CODDECLARACION)
                         VALUES ('RET24_109', 'N - Pagos o abonos en cuenta al exterior a paises sin convenio', 'BASE', NULL, '350_2024')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO RENGLONES (CODIGO, NOMBRE, SALDO, FORMULA, CODDECLARACION)
                         VALUES ('RET24_111', 'N - Pagos o abonos en cuenta al exterior a paises sin convenio', 'MOVIMIENTO', NULL, '350_2024')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO RENGLONES (CODIGO, NOMBRE, SALDO, FORMULA, CODDECLARACION)
                         VALUES ('RET24_110', 'N - Pagos o abonos en cuenta al exterior a paises con convenio vigente', 'BASE', NULL, '350_2024')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO RENGLONES (CODIGO, NOMBRE, SALDO, FORMULA, CODDECLARACION)
                         VALUES ('RET24_112', 'N - Pagos o abonos en cuenta al exterior a paises con convenio vigente', 'MOVIMIENTO', NULL, '350_2024')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO RENGLONES (CODIGO, NOMBRE, SALDO, FORMULA, CODDECLARACION)
                         VALUES ('RET24_113', 'N - Ventas', 'BASE', NULL, '350_2024')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO RENGLONES (CODIGO, NOMBRE, SALDO, FORMULA, CODDECLARACION)
                         VALUES ('RET24_121', 'N - Ventas', 'MOVIMIENTO', NULL, '350_2024')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO RENGLONES (CODIGO, NOMBRE, SALDO, FORMULA, CODDECLARACION)
                         VALUES ('RET24_114', 'N - Honorarios', 'BASE', NULL, '350_2024')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO RENGLONES (CODIGO, NOMBRE, SALDO, FORMULA, CODDECLARACION)
                         VALUES ('RET24_122', 'N - Honorarios', 'MOVIMIENTO', NULL, '350_2024')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO RENGLONES (CODIGO, NOMBRE, SALDO, FORMULA, CODDECLARACION)
                         VALUES ('RET24_115', 'N - Comisiones', 'BASE', NULL, '350_2024')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO RENGLONES (CODIGO, NOMBRE, SALDO, FORMULA, CODDECLARACION)
                         VALUES ('RET24_123', 'N - Comisiones', 'MOVIMIENTO', NULL, '350_2024')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO RENGLONES (CODIGO, NOMBRE, SALDO, FORMULA, CODDECLARACION)
                         VALUES ('RET24_116', 'N - Servicios', 'BASE', NULL, '350_2024')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO RENGLONES (CODIGO, NOMBRE, SALDO, FORMULA, CODDECLARACION)
                         VALUES ('RET24_124', 'N - Servicios', 'MOVIMIENTO', NULL, '350_2024')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO RENGLONES (CODIGO, NOMBRE, SALDO, FORMULA, CODDECLARACION)
                         VALUES ('RET24_117', 'N - Rendimientos financieros', 'BASE', NULL, '350_2024')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO RENGLONES (CODIGO, NOMBRE, SALDO, FORMULA, CODDECLARACION)
                         VALUES ('RET24_125', 'N - Rendimientos financieros', 'MOVIMIENTO', NULL, '350_2024')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO RENGLONES (CODIGO, NOMBRE, SALDO, FORMULA, CODDECLARACION)
                         VALUES ('RET24_118', 'N - Pagos men. provisionales de car vol (hidrocarburos y demas prod mineros)', 'BASE', NULL, '350_2024')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO RENGLONES (CODIGO, NOMBRE, SALDO, FORMULA, CODDECLARACION)
                         VALUES ('RET24_126', 'N - Pagos men. provisionales de car vol (hidrocarburos y demas prod mineros)', 'MOVIMIENTO', NULL, '350_2024')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO RENGLONES (CODIGO, NOMBRE, SALDO, FORMULA, CODDECLARACION)
                         VALUES ('RET24_119', 'N - Exportacion de hidrocarburos, carbon y demas pruductos mineros', 'BASE', NULL, '350_2024')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO RENGLONES (CODIGO, NOMBRE, SALDO, FORMULA, CODDECLARACION)
                         VALUES ('RET24_127', 'N - Exportacion de hidrocarburos, carbon y demas pruductos mineros', 'MOVIMIENTO', NULL, '350_2024')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO RENGLONES (CODIGO, NOMBRE, SALDO, FORMULA, CODDECLARACION)
                         VALUES ('RET24_120', 'N - Otros conceptos', 'BASE', NULL, '350_2024')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO RENGLONES (CODIGO, NOMBRE, SALDO, FORMULA, CODDECLARACION)
                         VALUES ('RET24_128', 'N - Otros conceptos', 'MOVIMIENTO', NULL, '350_2024')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO RENGLONES (CODIGO, NOMBRE, SALDO, FORMULA, CODDECLARACION)
                         VALUES ('RET24_129', 'Menos retenciones practicadas en exceso o indebidas o por operaciones anuladas, rescindidas o resueltas', 'SALDO', NULL, '350_2024')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO RENGLONES (CODIGO, NOMBRE, SALDO, FORMULA, CODDECLARACION)
                         VALUES ('RET24_130', 'Total retenciones renta y complementario', 'FORMULA', '(RET24_042+RET24_043+RET24_044+RET24_045+RET24_046+RET24_047+RET24_048+RET24_049+RET24_050+RET24_051+RET24_052+RET24_053+RET24_054+RET24_057+RET24_058+RET24_068+RET24_069+RET24_070+RET24_071+RET24_072+RET24_073+RET24_074+RET24_075+RET24_076+RET24_093+RET24_094+RET24_095+RET24_096+RET24_097+RET24_098+RET24_099+RET24_100+RET24_101+RET24_102+RET24_103+RET24_104+RET24_105+RET24_106+RET24_107+RET24_108+RET24_111+RET24_112+RET24_121+RET24_122+RET24_123+RET24_124+RET24_125+RET24_126+RET24_127+RET24_128)-RET24_129', '350_2024')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO RENGLONES (CODIGO, NOMBRE, SALDO, FORMULA, CODDECLARACION)
                         VALUES ('RET24_131', 'A responsables del impuesto sobre las ventas', 'MOVIMIENTO', NULL, '350_2024')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO RENGLONES (CODIGO, NOMBRE, SALDO, FORMULA, CODDECLARACION)
                         VALUES ('RET24_132', 'Practicadas por servicios a no residentes o no domiciliados', 'MOVIMIENTO', NULL, '350_2024')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO RENGLONES (CODIGO, NOMBRE, SALDO, FORMULA, CODDECLARACION)
                         VALUES ('RET24_133', 'Menos retenciones practicadas en exceso o indebidas o por operaciones anuladas, rescindidas o resueltas', 'SALDO', NULL, '350_2024')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO RENGLONES (CODIGO, NOMBRE, SALDO, FORMULA, CODDECLARACION)
                         VALUES ('RET24_134', 'Total retenciones IVA', 'FORMULA', '(RET24_131+RET24_132)-RET24_133', '350_2024')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO RENGLONES (CODIGO, NOMBRE, SALDO, FORMULA, CODDECLARACION)
                         VALUES ('RET24_135', 'Retenciones impuesto timbre nacional', 'MOVIMIENTO', NULL, '350_2024')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO RENGLONES (CODIGO, NOMBRE, SALDO, FORMULA, CODDECLARACION)
                         VALUES ('RET24_136', 'Total retenciones', 'FORMULA', '((RET24_042+RET24_043+RET24_044+RET24_045+RET24_046+RET24_047+RET24_048+RET24_049+RET24_050+RET24_051+RET24_052+RET24_053+RET24_054+RET24_057+RET24_058+RET24_068+RET24_069+RET24_070+RET24_071+RET24_072+RET24_073+RET24_074+RET24_075+RET24_076+RET24_093+RET24_094+RET24_095+RET24_096+RET24_097+RET24_098+RET24_099+RET24_100+RET24_101+RET24_102+RET24_103+RET24_104+RET24_105+RET24_106+RET24_107+RET24_108+RET24_111+RET24_112+RET24_121+RET24_122+RET24_123+RET24_124+RET24_125+RET24_126+RET24_127+RET24_128)-RET24_129)+((RET24_131+RET24_132)-RET24_133)+RET24_135', '350_2024')
                       MATCHING (CODIGO);


COMMIT WORK;


--- Presupuesto
UPDATE OR INSERT INTO Informes (Codigo, Nombre, Nivel, Modulo, Sql_Consulta,
                                Codgrupo, Rango, Nativo, Codreporte, Categoria,
                                Nota)
VALUES ('PPTO220CC',
        'EJECUCION PRESUPUESTAL DE GASTOS - POR UN CENTRO Y CON NIVELES -RANGO FECHA',
        '5', 'PRESUPUEST',
        'SELECT * FROM GO_ARBOL_PRESUPUESTO_CC(:P_GASTOS,:CENTRO, :_DESDE, :_HASTA )',
        'PRESUPUESTO', 'N', 'S', NULL, 'LIBRE',
        'Ejecucion x rubros auxiliares y de nivel x 1 centro, con apropiacion inicial, credito y contracredito, aprobado (cdp), compromiso (rp), obligacion y pago. Indicar rango fecha y proyecto gastos');
UPDATE OR INSERT INTO Informes (Codigo, Nombre, Nivel, Modulo, Sql_Consulta,
                                Codgrupo, Rango, Nativo, Codreporte, Categoria,
                                Nota)
VALUES ('PPTO240CC',
        'EJECUCION PRESUPUESTAL DE GASTOS x CENTROS - SOLO RUBROS AUXILIARES -RANGO FECHA',
        '5', 'PRESUPUEST',
        'SELECT * FROM GO_PRESUPUESTO_INFORME_CC(:P_GASTOS,:_DESDE,:_HASTA)',
        'PRESUPUESTO', 'N', 'S', NULL, 'LIBRE',
        'Ejecucion solo por rubros auxiliares y por centros, con planeado, aprobado(cdp), comprometido(rp), obligacion, pago, liberacion. Indicar rango fecha y proyecto gastos respectivo');

UPDATE OR INSERT INTO Cubos (Codigo, Nombre, Nivel, Modulo, Sql_Consulta,
                             Codgrupo, Nativo, Nota)
VALUES ('PPTO245CC',
        'EJECUCION PRESUPUESTAL x CENTROS - SOLO RUBROS AUXILIARES -FECHA CORTE.',
        '5', 'PRESUPUEST', 'SELECT G.PERIODO, ''0'' MES, G.CODCENTRO, G.CODP_PPTO,  G.NOMBRE_CENTRO, G.NOMBRE_RUBRO, G.PLANEADO, G.APROBADO, G.COMPROMISO, G.OBLIGACION, G.PAGO, G.LIBERACION_CDP, G.LIBERACION_RP
FROM GO_PRESUPUESTO_CUBO_CC(:P_GASTOS,:_HASTA) G
WHERE G.PLANEADO>0
UNION ALL
SELECT G.PERIODO, G.MES, G.CODCENTRO, G.CODP_PPTO,  G.NOMBRE_CENTRO, G.NOMBRE_RUBRO, G.PLANEADO, G.APROBADO, G.COMPROMISO, G.OBLIGACION, G.PAGO, G.LIBERACION_CDP, G.LIBERACION_RP
FROM GO_PRESUPUESTO_CUBO_CC(:P_GASTOS,:_HASTA) G
WHERE G.PLANEADO=0
ORDER BY 1,2,3', 'PRESUPUESTO', 'S',
        'Ejecucion solo por rubros auxiliares y por centros, con planeado, aprobado(cdp), comprometido(rp), obligacion, pago, liberacion.  Indicar rango fecha y proyecto gastos');

COMMIT WORK;

UPDATE OR INSERT INTO CUBOS (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD)
                     VALUES ('CUEC010101', 'SALDOS DE CUENTAS AUXILIARES MES POR MES - SALDO INICIAL Y FINAL', 'Muestra los saldos de las cuentas auxiliares por mes con su respectivo saldo inicial, debitos, creditos y saldo final por rango de fechas y cuentas especificas', 'SELECT Ano,
       Mes,
       Cuenta,
       Centro,
       Tercero,
       Inicial,
       Debito,
       Credito,
       Final
FROM Pz_Cubo_Saldos(:Fecha_Desde, :Fecha_Hasta, :Cuenta_Desde, :Cuenta_Hasta)', 'S', 'CONTABLE', 'N', 'AUXILIAR DE CUENTAS', 'N', 'LIBRE', 5, 'N', 1)
                   MATCHING (CODIGO);


COMMIT WORK;

----- VALIDACIONES
UPDATE OR INSERT INTO VALIDACIONES (ID, TABLA, CAMPO, SQL, POSIBLES_VALORES, MODULO)
                            VALUES (1, 'TERCEROS', 'NATURALEZA', 'SELECT Naturaleza
FROM Terceros
WHERE Codigo = '':TERCERO''', 'N,J', 'GESTION')
                          MATCHING (ID);
UPDATE OR INSERT INTO VALIDACIONES (ID, TABLA, CAMPO, SQL, POSIBLES_VALORES, MODULO)
                            VALUES (2, 'TERCEROS', 'NATURALEZA', 'SELECT Naturaleza
FROM Terceros
WHERE Codigo = '':TERCERO''', 'N,J', 'CONTABLE')
                          MATCHING (ID);
UPDATE OR INSERT INTO VALIDACIONES (ID, TABLA, CAMPO, SQL, POSIBLES_VALORES, MODULO)
                            VALUES (3, 'TERCEROS', 'CODIDENTIDAD', 'SELECT Codidentidad
FROM Terceros
WHERE Codigo = '':TERCERO''', '11,12,13,21,22,31,41,42,47,48,50,91', 'GESTION')
                          MATCHING (ID);
UPDATE OR INSERT INTO VALIDACIONES (ID, TABLA, CAMPO, SQL, POSIBLES_VALORES, MODULO)
                            VALUES (4, 'TERCEROS', 'CODIDENTIDAD', 'SELECT Codidentidad
FROM Terceros
WHERE Codigo = '':TERCERO''', '11,12,13,21,22,31,41,42,47,48,50,91', 'CONTABLE')
                          MATCHING (ID);
UPDATE OR INSERT INTO VALIDACIONES (ID, TABLA, CAMPO, SQL, POSIBLES_VALORES, MODULO)
                            VALUES (7, 'TIPOIMPUESTOS', 'CODIGO_FE', 'SELECT T.Codigo_Fe
FROM Reg_Impuestos I
JOIN Tipoimpuestos T ON (I.Tipo_Impuesto = T.Codigo)
WHERE TRIM(I.Tipo) = '':TIPO''
      AND TRIM(I.Prefijo) = '':PREFIJO''
      AND TRIM(I.Numero) = '':NUMERO''', '00,01,02,03,04,21,22,23,24,25,26,33,34,35,ZY,ZZ,99', 'GESTION')
                          MATCHING (ID);
UPDATE OR INSERT INTO VALIDACIONES (ID, TABLA, CAMPO, SQL, POSIBLES_VALORES, MODULO)
                            VALUES (8, 'TIPOIMPUESTOS', 'CODIGO_FE', 'SELECT T.Codigo_Fe
FROM Fe_Apo_Impuestos('':TIPO'', '':PREFIJO'', '':NUMERO'') I
JOIN Tipoimpuestos T ON (T.Codigo = I.Codtipoimpuesto)', '00,01,02,03,04,21,22,23,24,25,26,33,34,35,ZY,ZZ,99', 'CONTABLE')
                          MATCHING (ID);
UPDATE OR INSERT INTO VALIDACIONES (ID, TABLA, CAMPO, SQL, POSIBLES_VALORES, MODULO)
                            VALUES (9, 'TIPORETENCIONES', 'CODIGO_FE', 'SELECT T.Codigo_Fe
FROM Reg_Retenciones R
JOIN Tiporetenciones T ON (R.Tipo_Retencion = T.Codigo)
WHERE TRIM(R.Tipo) = '':TIPO''
      AND TRIM(R.Prefijo) = '':PREFIJO''
      AND TRIM(R.Numero) = '':NUMERO''', '05,06,07,20,25,26,ZZ,99', 'GESTION')
                          MATCHING (ID);
UPDATE OR INSERT INTO VALIDACIONES (ID, TABLA, CAMPO, SQL, POSIBLES_VALORES, MODULO)
                            VALUES (10, 'TIPORETENCIONES', 'CODIGO_FE', 'SELECT T.Codigo_Fe
FROM Fe_Apo_Retenciones('':TIPO'', '':PREFIJO'', '':NUMERO'') I
JOIN Tiporetenciones T ON (T.Codigo = I.Codtiporetencion)', '05,06,07,20,25,26,ZZ,99', 'CONTABLE')
                          MATCHING (ID);
UPDATE OR INSERT INTO VALIDACIONES (ID, TABLA, CAMPO, SQL, POSIBLES_VALORES, MODULO)
                            VALUES (5, 'SOCIEDADES', 'CODIGO_FE', 'SELECT S.Codigo_Fe
FROM Terceros T
JOIN Sociedades S ON (T.Codsociedad = S.Codigo)
WHERE T.Codigo = '':TERCERO''', '48,49', 'GESTION')
                          MATCHING (ID);
UPDATE OR INSERT INTO VALIDACIONES (ID, TABLA, CAMPO, SQL, POSIBLES_VALORES, MODULO)
                            VALUES (6, 'SOCIEDADES', 'CODIGO_FE', 'SELECT S.Codigo_Fe
FROM Terceros T
JOIN Sociedades S ON (T.Codsociedad = S.Codigo)
WHERE T.Codigo = '':TERCERO''', '48,49', 'CONTABLE')
                          MATCHING (ID);


COMMIT WORK;

/************************************************************/
/****                MODULO PARAMETROS                   ****/
/************************************************************/

UPDATE OR INSERT INTO INFORMES (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD, DESTINO_FTP, CODREPORTE)
                        VALUES ('CTP020101', 'CATALOGO DE TIPOS DE DOCUMENTOS', 'Muestra el listado de los diferentes tipos de documentos discriminados por Grupo, relacionando sus principales atributos.', 'SELECT D.Codigo,
       D.Nombre,
       D.Formato1,
       D.Formato2,
       D.Formato3,
       D.Formato4,
       D.Activo,
       D.Activo_Contabilidad,
       D.Niif,
       D.Signo,
       D.Grupo,
       D.Tesoreria,
       D.Inventario,
       D.Nomina,
       D.Registro_Pago,
       D.Impuestos,
       D.Retenciones,
       D.Comision,
       D.Contabilidad,
       TRIM(D.C_Vence) AS Doc_Vence,
       TRIM(D.C_Condicionado) AS Doc_Desc_Condicionado,
       TRIM(D.C_Codtercero) AS Doc_Tercero,
       TRIM(D.C_Codvendedor) AS Doc_Vendedor,
       TRIM(D.C_Codlista) AS Doc_Lista_Precios,
       TRIM(D.C_Nota) AS Doc_Nota,
       TRIM(D.M_Aplica) AS Mov_Aplica,
       TRIM(D.M_Cuota) AS Mov_Cuota,
       TRIM(D.M_Documento) AS Mov_Documento,
       TRIM(D.M_Descuento) AS Mov_Descuento,
       TRIM(D.M_Interes) AS Mov_Intereses,
       TRIM(D.M_Pronto_Pago) AS Mov_Pronto_Pago,
       TRIM(D.M_Codtercero) AS Mov_Tercero,
       TRIM(D.M_Nota) AS Mov_Nota,
       TRIM(D.Afecta) AS Afecta_Saldos,
       D.Valoriza,
       D.Costea,
       D.Valida_Saldos,
       D.Con_Impuestos,
       D.Codinteres_Debito,
       D.Codinteres_Credito,
       D.Codpronto_Pagos,
       D.Publica_Retenciones,
       D.Base_Dia,
       D.Pooling,
       D.Duracion,
       D.Ensamble,
       TRIM(D.Es_Produccion) AS Produccion,
       D.Puntos,
       D.Rastreo,
       D.Exportar,
       D.Compartir,
       D.Dimensiones,
       TRIM(D.Es_Presupuesto) AS Presupuesto,
       D.Analisis_Costos,
       D.Rotacion,
       TRIM(D.Deterioro) AS Deterioro_Niif
FROM Documentos D
ORDER BY 1, 2', 'S', 'PARAMETROS', 'N', 'DOCUMENTOS', 'N', 'LIBRE', 5, 'N', 1, NULL, NULL)
                      MATCHING (CODIGO);


COMMIT WORK;

/************************************************************/
/****             MODULO PARAMETROS FINAL                ****/
/************************************************************/


-- FESTIVOS 2025
UPDATE OR INSERT INTO FESTIVOS (CODIGO,NOMBRE) VALUES(45658,'Ano Nuevo');
UPDATE OR INSERT INTO FESTIVOS (CODIGO,NOMBRE) VALUES(45662,'Domingo');
UPDATE OR INSERT INTO FESTIVOS (CODIGO,NOMBRE) VALUES(45663,'Dia de los Reyes Magos');
UPDATE OR INSERT INTO FESTIVOS (CODIGO,NOMBRE) VALUES(45669,'Domingo');
UPDATE OR INSERT INTO FESTIVOS (CODIGO,NOMBRE) VALUES(45676,'Domingo');
UPDATE OR INSERT INTO FESTIVOS (CODIGO,NOMBRE) VALUES(45683,'Domingo');
UPDATE OR INSERT INTO FESTIVOS (CODIGO,NOMBRE) VALUES(45690,'Domingo');
UPDATE OR INSERT INTO FESTIVOS (CODIGO,NOMBRE) VALUES(45697,'Domingo');
UPDATE OR INSERT INTO FESTIVOS (CODIGO,NOMBRE) VALUES(45704,'Domingo');
UPDATE OR INSERT INTO FESTIVOS (CODIGO,NOMBRE) VALUES(45711,'Domingo');
UPDATE OR INSERT INTO FESTIVOS (CODIGO,NOMBRE) VALUES(45718,'Domingo');
UPDATE OR INSERT INTO FESTIVOS (CODIGO,NOMBRE) VALUES(45725,'Domingo');
UPDATE OR INSERT INTO FESTIVOS (CODIGO,NOMBRE) VALUES(45732,'Domingo');
UPDATE OR INSERT INTO FESTIVOS (CODIGO,NOMBRE) VALUES(45739,'Domingo');
UPDATE OR INSERT INTO FESTIVOS (CODIGO,NOMBRE) VALUES(45740,'Dia de San Jose');
UPDATE OR INSERT INTO FESTIVOS (CODIGO,NOMBRE) VALUES(45746,'Domingo');
UPDATE OR INSERT INTO FESTIVOS (CODIGO,NOMBRE) VALUES(45753,'Domingo');
UPDATE OR INSERT INTO FESTIVOS (CODIGO,NOMBRE) VALUES(45760,'Domingo');
UPDATE OR INSERT INTO FESTIVOS (CODIGO,NOMBRE) VALUES(45764,'Jueves Santo');
UPDATE OR INSERT INTO FESTIVOS (CODIGO,NOMBRE) VALUES(45765,'Viernes Santo');
UPDATE OR INSERT INTO FESTIVOS (CODIGO,NOMBRE) VALUES(45767,'Domingo');
UPDATE OR INSERT INTO FESTIVOS (CODIGO,NOMBRE) VALUES(45774,'Domingo');
UPDATE OR INSERT INTO FESTIVOS (CODIGO,NOMBRE) VALUES(45778,'Dia del trabajo');
UPDATE OR INSERT INTO FESTIVOS (CODIGO,NOMBRE) VALUES(45781,'Domingo');
UPDATE OR INSERT INTO FESTIVOS (CODIGO,NOMBRE) VALUES(45788,'Domingo');
UPDATE OR INSERT INTO FESTIVOS (CODIGO,NOMBRE) VALUES(45795,'Domingo');
UPDATE OR INSERT INTO FESTIVOS (CODIGO,NOMBRE) VALUES(45802,'Domingo');
UPDATE OR INSERT INTO FESTIVOS (CODIGO,NOMBRE) VALUES(45809,'Domingo');
UPDATE OR INSERT INTO FESTIVOS (CODIGO,NOMBRE) VALUES(45810,'Dia de la Ascension');
UPDATE OR INSERT INTO FESTIVOS (CODIGO,NOMBRE) VALUES(45816,'Domingo');
UPDATE OR INSERT INTO FESTIVOS (CODIGO,NOMBRE) VALUES(45823,'Domingo');
UPDATE OR INSERT INTO FESTIVOS (CODIGO,NOMBRE) VALUES(45830,'Domingo');
UPDATE OR INSERT INTO FESTIVOS (CODIGO,NOMBRE) VALUES(45831,'Dia de Corpus Christi');
UPDATE OR INSERT INTO FESTIVOS (CODIGO,NOMBRE) VALUES(45837,'Domingo');
UPDATE OR INSERT INTO FESTIVOS (CODIGO,NOMBRE) VALUES(45838,'Dia de San Pedro y San Pablo');
UPDATE OR INSERT INTO FESTIVOS (CODIGO,NOMBRE) VALUES(45844,'Domingo');
UPDATE OR INSERT INTO FESTIVOS (CODIGO,NOMBRE) VALUES(45851,'Domingo');
UPDATE OR INSERT INTO FESTIVOS (CODIGO,NOMBRE) VALUES(45858,'Domingo');
UPDATE OR INSERT INTO FESTIVOS (CODIGO,NOMBRE) VALUES(45865,'Domingo');
UPDATE OR INSERT INTO FESTIVOS (CODIGO,NOMBRE) VALUES(45872,'Domingo');
UPDATE OR INSERT INTO FESTIVOS (CODIGO,NOMBRE) VALUES(45876,'Batalla de Boyaca');
UPDATE OR INSERT INTO FESTIVOS (CODIGO,NOMBRE) VALUES(45879,'Domingo');
UPDATE OR INSERT INTO FESTIVOS (CODIGO,NOMBRE) VALUES(45886,'Domingo');
UPDATE OR INSERT INTO FESTIVOS (CODIGO,NOMBRE) VALUES(45887,'Asuncion de la Virgen');
UPDATE OR INSERT INTO FESTIVOS (CODIGO,NOMBRE) VALUES(45893,'Domingo');
UPDATE OR INSERT INTO FESTIVOS (CODIGO,NOMBRE) VALUES(45900,'Domingo');
UPDATE OR INSERT INTO FESTIVOS (CODIGO,NOMBRE) VALUES(45907,'Domingo');
UPDATE OR INSERT INTO FESTIVOS (CODIGO,NOMBRE) VALUES(45914,'Domingo');
UPDATE OR INSERT INTO FESTIVOS (CODIGO,NOMBRE) VALUES(45921,'Domingo');
UPDATE OR INSERT INTO FESTIVOS (CODIGO,NOMBRE) VALUES(45928,'Domingo');
UPDATE OR INSERT INTO FESTIVOS (CODIGO,NOMBRE) VALUES(45935,'Domingo');
UPDATE OR INSERT INTO FESTIVOS (CODIGO,NOMBRE) VALUES(45942,'Domingo');
UPDATE OR INSERT INTO FESTIVOS (CODIGO,NOMBRE) VALUES(45943,'Dia de la Raza');
UPDATE OR INSERT INTO FESTIVOS (CODIGO,NOMBRE) VALUES(45949,'Domingo');
UPDATE OR INSERT INTO FESTIVOS (CODIGO,NOMBRE) VALUES(45956,'Domingo');
UPDATE OR INSERT INTO FESTIVOS (CODIGO,NOMBRE) VALUES(45963,'Domingo');
UPDATE OR INSERT INTO FESTIVOS (CODIGO,NOMBRE) VALUES(45964,'Dia de todos los Santos');
UPDATE OR INSERT INTO FESTIVOS (CODIGO,NOMBRE) VALUES(45970,'Domingo');
UPDATE OR INSERT INTO FESTIVOS (CODIGO,NOMBRE) VALUES(45977,'Domingo');
UPDATE OR INSERT INTO FESTIVOS (CODIGO,NOMBRE) VALUES(45978,'Independencia de Cartagena');
UPDATE OR INSERT INTO FESTIVOS (CODIGO,NOMBRE) VALUES(45984,'Domingo');
UPDATE OR INSERT INTO FESTIVOS (CODIGO,NOMBRE) VALUES(45991,'Domingo');
UPDATE OR INSERT INTO FESTIVOS (CODIGO,NOMBRE) VALUES(45998,'Domingo');
UPDATE OR INSERT INTO FESTIVOS (CODIGO,NOMBRE) VALUES(45999,'Inmaculada Concepcion');
UPDATE OR INSERT INTO FESTIVOS (CODIGO,NOMBRE) VALUES(46005,'Domingo');
UPDATE OR INSERT INTO FESTIVOS (CODIGO,NOMBRE) VALUES(46012,'Domingo');
UPDATE OR INSERT INTO FESTIVOS (CODIGO,NOMBRE) VALUES(46016,'Navidad');
UPDATE OR INSERT INTO FESTIVOS (CODIGO,NOMBRE) VALUES(46019,'Domingo');


UPDATE OR INSERT INTO FESTIVOS_FE (FECHA,DESCRIPCION) VALUES('01.01.2025','Ano Nuevo');
UPDATE OR INSERT INTO FESTIVOS_FE (FECHA,DESCRIPCION) VALUES('06.01.2025','Dia de los Reyes Magos');
UPDATE OR INSERT INTO FESTIVOS_FE (FECHA,DESCRIPCION) VALUES('24.03.2025','Dia de San Jose');
UPDATE OR INSERT INTO FESTIVOS_FE (FECHA,DESCRIPCION) VALUES('17.04.2025','Jueves Santo');
UPDATE OR INSERT INTO FESTIVOS_FE (FECHA,DESCRIPCION) VALUES('18.04.2025','Viernes Santo');
UPDATE OR INSERT INTO FESTIVOS_FE (FECHA,DESCRIPCION) VALUES('01.05.2025','Dia del trabajo');
UPDATE OR INSERT INTO FESTIVOS_FE (FECHA,DESCRIPCION) VALUES('02.06.2025','Dia de la Ascension');
UPDATE OR INSERT INTO FESTIVOS_FE (FECHA,DESCRIPCION) VALUES('23.06.2025','Dia de Corpus Christi');
UPDATE OR INSERT INTO FESTIVOS_FE (FECHA,DESCRIPCION) VALUES('30.06.2025','Dia de San Pedro y San Pablo');
UPDATE OR INSERT INTO FESTIVOS_FE (FECHA,DESCRIPCION) VALUES('07.08.2025','Batalla de Boyaca');
UPDATE OR INSERT INTO FESTIVOS_FE (FECHA,DESCRIPCION) VALUES('18.08.2025','Asuncion de la Virgen');
UPDATE OR INSERT INTO FESTIVOS_FE (FECHA,DESCRIPCION) VALUES('13.10.2025','Dia de la Raza');
UPDATE OR INSERT INTO FESTIVOS_FE (FECHA,DESCRIPCION) VALUES('03.11.2025','Dia de todos los Santos');
UPDATE OR INSERT INTO FESTIVOS_FE (FECHA,DESCRIPCION) VALUES('17.11.2025','Independencia de Cartagena');
UPDATE OR INSERT INTO FESTIVOS_FE (FECHA,DESCRIPCION) VALUES('08.12.2025','Inmaculada Concepcion');
UPDATE OR INSERT INTO FESTIVOS_FE (FECHA,DESCRIPCION) VALUES('25.12.2025','Navidad');

COMMIT WORK;


/* SALARIO MINIMO 2025 */
UPDATE OR INSERT INTO Constantesvalor (Codconstante, Ano, Valor)
VALUES ('C01_SMLV', '2025', 1423500);
UPDATE OR INSERT INTO Constantesvalor (Codconstante, Ano, Valor)
VALUES ('C02_AUXT', '2025', 200000);
COMMIT WORK;

/* IMPUESTOS 2025 */
EXECUTE BLOCK
AS
DECLARE VARIABLE V_Impuesto CHAR(5);
DECLARE VARIABLE V_Ano      INTEGER;
DECLARE VARIABLE V_Valor    NUMERIC(17,4);
DECLARE VARIABLE V_Tarifa   NUMERIC(15,4);
DECLARE VARIABLE V_2025     INTEGER;
BEGIN
  FOR SELECT Codtipoimpuesto,
             Ano,
             Valor,
             Tarifa
      FROM Data_Impuestos
      WHERE Ano = 2024
      INTO V_Impuesto,
           V_Ano,
           V_Valor,
           V_Tarifa
  DO
  BEGIN
    SELECT COUNT(1)
    FROM Data_Impuestos
    WHERE Codtipoimpuesto = :V_Impuesto
          AND Ano = 2025
    INTO V_2025;

    IF (V_2025 = 0) THEN
    BEGIN
      INSERT INTO Data_Impuestos
      VALUES (:V_Impuesto, 2025, :V_Valor, :V_Tarifa);
    END

  END
END;

COMMIT WORK;

/* RETENCIONES 2025 */
EXECUTE BLOCK
AS
DECLARE VARIABLE V_Impuesto CHAR(5);
DECLARE VARIABLE V_Ano      INTEGER;
DECLARE VARIABLE V_Base     NUMERIC(17,4);
DECLARE VARIABLE V_Valor    NUMERIC(17,4);
DECLARE VARIABLE V_Tarifa   NUMERIC(15,4);
DECLARE VARIABLE V_2025     INTEGER;
BEGIN
  FOR SELECT Codtiporetencion,
             Ano,
             Base,
             Valor,
             Tarifa
      FROM Data_Retenciones
      WHERE Ano = 2024
      INTO V_Impuesto,
           V_Ano,
           V_Base,
           V_Valor,
           V_Tarifa
  DO
  BEGIN
    SELECT COUNT(1)
    FROM Data_Retenciones
    WHERE Codtiporetencion = :V_Impuesto
          AND Ano = 2025
    INTO V_2025;

    IF (V_2025 = 0) THEN
    BEGIN
      INSERT INTO Data_Retenciones
      VALUES (:V_Impuesto, 2025, :V_Base, :V_Valor, :V_Tarifa);
    END

  END
END;

UPDATE Data_Retenciones
SET Base = 199000
WHERE Base = 188000
      AND Ano = 2025;
UPDATE Data_Retenciones
SET Base = 1345000
WHERE Base = 1271000
      AND Ano = 2025;
UPDATE Data_Retenciones
SET Base = 7968000
WHERE Base = 7530000
      AND Ano = 2025;
UPDATE Data_Retenciones
SET Base = 4582000
WHERE Base = 4330000
      AND Ano = 2025;
UPDATE Data_Retenciones
SET Base = 2390000
WHERE Base = 2259000
      AND Ano = 2025;

COMMIT WORK;

/* Reenviar PDF por la contingencia de DIAN */
UPDATE facturas f
SET pdf = 'N',
    resultado = 0
WHERE fecha_envio >= '09.01.2025 00:00:00' AND
      f.resultado = 2 AND
      EXISTS (SELECT d.codigo
             FROM documentos d
             WHERE d.codigo = f.tipo AND
                   TRIM(d.codigo_fe) NOT IN ('POS ELECTRONICO', 'NOTA CREDITO POS'));
COMMIT WORK;

update or insert into COBERTURAS (CODIGO,NOMBRE) VALUES ('01','Plan de beneficios en salud financiado con UPC');
update or insert into COBERTURAS (CODIGO,NOMBRE) VALUES ('02','Presupuesto maximo');
update or insert into COBERTURAS (CODIGO,NOMBRE) VALUES ('03','Prima EPS, no asegurados SOAT');
update or insert into COBERTURAS (CODIGO,NOMBRE) VALUES ('04','Cobertura Poliza SOAT');
update or insert into COBERTURAS (CODIGO,NOMBRE) VALUES ('05','Cobertura ARL  ');
update or insert into COBERTURAS (CODIGO,NOMBRE) VALUES ('06','Cobertura ADRES');
update or insert into COBERTURAS (CODIGO,NOMBRE) VALUES ('07','Cobertura salud publica');
update or insert into COBERTURAS (CODIGO,NOMBRE) VALUES ('08','Cobertura entidad territorial, recursos  de oferta');
update or insert into COBERTURAS (CODIGO,NOMBRE) VALUES ('09','Urgencias poblacion migrante');
update or insert into COBERTURAS (CODIGO,NOMBRE) VALUES ('10','Plan complementario en salud');
update or insert into COBERTURAS (CODIGO,NOMBRE) VALUES ('11','Plan medicina prepagada');
update or insert into COBERTURAS (CODIGO,NOMBRE) VALUES ('12','Polizas en salud');
update or insert into COBERTURAS (CODIGO,NOMBRE) VALUES ('13','Cobertura Regimen Especial o Excepcion');
update or insert into COBERTURAS (CODIGO,NOMBRE) VALUES ('14','Cobertura Fondo Nacional de Salud  de las  Personas Privadas de la Libertad');
update or insert into COBERTURAS (CODIGO,NOMBRE) VALUES ('15','Particular');
COMMIT WORK;

/** EXOGENA AG2024 **/
EXECUTE BLOCK
AS
DECLARE VARIABLE v_mm INTEGER;
BEGIN

  v_mm = 0;
  SELECT COUNT(1)
  FROM mm_formatos
  WHERE codigo = '1004_V8'
  INTO v_mm;
  IF (:v_mm = 0) THEN
  BEGIN
    UPDATE OR INSERT INTO mm_formatos (codigo, nombre, entinfo, cpt, tdoc, nid,
                                       dv, apl1, apl2, nom1, nom2, raz, dir,
                                       dpto, mun, pais, email, valor1, valor2,
                                       valor3, valor4, valor5, valor6, valor7,
                                       valor8, valor9, valor0, valor10, valor11,
                                       valor12, valor13, valor14, valor15,
                                       valor16, valor17, valor18, valor19,
                                       valor20, valor21, valor22, valor23,
                                       valor24, valor25, valor26, total, idem,
                                       tdocm, nitm, dvm, apl1m, apl2m, nom1m,
                                       nom2m, razm, informar, con_informados,
                                       nelemento, nvalor1, nvalor2, nvalor3,
                                       nvalor4, nvalor5, nvalor6, nvalor7,
                                       nvalor8, nvalor9, nvalor0, nvalor10,
                                       nvalor11, nvalor12, nvalor13, nvalor14,
                                       nvalor15, nvalor16, nvalor17, nvalor18,
                                       nvalor19, nvalor20, nvalor21, nvalor22,
                                       nvalor23, nvalor24, nvalor25, nvalor26,
                                       nidem, ncpt, referencia, revision,
                                       cadena_adicional, equivalencia, valor27,
                                       valor28, valor29, nvalor27, nvalor28,
                                       nvalor29)
    VALUES ('1004_V8', 'DESCUENTOS TRIBUTARIOS SOLICITADOS', NULL, 'S', 'S',
            'S', 'N', 'S', 'S', 'S', 'S', 'S', 'S', 'S', 'S', 'S', 'S', 'S',
            'S', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N',
            'N', 'N', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N',
            'descuentos', 'vdesc', 'vdescsol', NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'cpt',
            'DESCUENTOS TRIBUTARIOS SOLICITADOS', 8, NULL,
            'nid=nit,nom1=pno,nom2=ono,apl1=pap,apl2=sap', NULL, NULL, NULL,
            NULL, NULL, NULL);

    UPDATE OR INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre,
                                        saldo, valor, porcentaje, tope, forzoso,
                                        mandatario, revelar, tendencia_credito,
                                        empleado)
    VALUES ('1004_V8', '8303', 3913,
            'POR IMP. PAG. EN EL EXT. SOLIC. POR LOS CONTRI. NACIONALES QUE PERCIBAN RENTAS DE FUENTE EXTRANJERA',
            'FINAL', 'UNO', 100, 0, 'N', 'N', 'N', 'N', 'N')
    MATCHING (codformato, codigo, renglon);
    UPDATE OR INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre,
                                        saldo, valor, porcentaje, tope, forzoso,
                                        mandatario, revelar, tendencia_credito,
                                        empleado)
    VALUES ('1004_V8', '8303', 3914,
            'POR IMP. PAG. EN EL EXT. SOLIC. POR LOS CONTRI. NACIONALES QUE PERCIBAN RENTAS DE FUENTE EXTRANJERA',
            'FINAL', 'DOS', 100, 0, 'N', 'N', 'N', 'N', 'N')
    MATCHING (codformato, codigo, renglon);
    UPDATE OR INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre,
                                        saldo, valor, porcentaje, tope, forzoso,
                                        mandatario, revelar, tendencia_credito,
                                        empleado)
    VALUES ('1004_V8', '8305', 3915,
            'EMPRESAS DE SERVICIOS PUBLICOS DOMICILIARIOS QUE PRESTEN SERVICIOS DE ACUEDUCTO Y ALCANTARILLADO.',
            'FINAL', 'UNO', 100, 0, 'N', 'N', 'N', 'N', 'N')
    MATCHING (codformato, codigo, renglon);
    UPDATE OR INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre,
                                        saldo, valor, porcentaje, tope, forzoso,
                                        mandatario, revelar, tendencia_credito,
                                        empleado)
    VALUES ('1004_V8', '8305', 3916,
            'EMPRESAS DE SERVICIOS PUBLICOS DOMICILIARIOS QUE PRESTEN SERVICIOS DE ACUEDUCTO Y ALCANTARILLADO.',
            'FINAL', 'DOS', 100, 0, 'N', 'N', 'N', 'N', 'N')
    MATCHING (codformato, codigo, renglon);
    UPDATE OR INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre,
                                        saldo, valor, porcentaje, tope, forzoso,
                                        mandatario, revelar, tendencia_credito,
                                        empleado)
    VALUES ('1004_V8', '8316', 3917,
            'POR DONACIONES DIRIGIDA A PROGRAMAS DE BECAS O CREDITOS CONDONABLES.',
            'FINAL', 'UNO', 100, 0, 'N', 'N', 'N', 'N', 'N')
    MATCHING (codformato, codigo, renglon);
    UPDATE OR INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre,
                                        saldo, valor, porcentaje, tope, forzoso,
                                        mandatario, revelar, tendencia_credito,
                                        empleado)
    VALUES ('1004_V8', '8316', 3918,
            'POR DONACIONES DIRIGIDA A PROGRAMAS DE BECAS O CREDITOS CONDONABLES.',
            'FINAL', 'DOS', 100, 0, 'N', 'N', 'N', 'N', 'N')
    MATCHING (codformato, codigo, renglon);
    UPDATE OR INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre,
                                        saldo, valor, porcentaje, tope, forzoso,
                                        mandatario, revelar, tendencia_credito,
                                        empleado)
    VALUES ('1004_V8', '8317', 3919,
            'POR INVERSIONES EN INVESTIGACION, DESARROLLO TECNOLOGICO E INNOVACION.',
            'FINAL', 'UNO', 100, 0, 'N', 'N', 'N', 'N', 'N')
    MATCHING (codformato, codigo, renglon);
    UPDATE OR INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre,
                                        saldo, valor, porcentaje, tope, forzoso,
                                        mandatario, revelar, tendencia_credito,
                                        empleado)
    VALUES ('1004_V8', '8317', 3920,
            'POR INVERSIONES EN INVESTIGACION, DESARROLLO TECNOLOGICO E INNOVACION.',
            'FINAL', 'DOS', 100, 0, 'N', 'N', 'N', 'N', 'N')
    MATCHING (codformato, codigo, renglon);
    UPDATE OR INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre,
                                        saldo, valor, porcentaje, tope, forzoso,
                                        mandatario, revelar, tendencia_credito,
                                        empleado)
    VALUES ('1004_V8', '8318', 3921,
            'POR DONACIONES EFECTUADAS A ENTIDADES SIN ANIMO DE LUCRO PERTENECIENTES AL REG. TRIBUTARIO ESPECIAL',
            'FINAL', 'UNO', 100, 0, 'N', 'N', 'N', 'N', 'N')
    MATCHING (codformato, codigo, renglon);
    UPDATE OR INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre,
                                        saldo, valor, porcentaje, tope, forzoso,
                                        mandatario, revelar, tendencia_credito,
                                        empleado)
    VALUES ('1004_V8', '8318', 3922,
            'POR DONACIONES EFECTUADAS A ENTIDADES SIN ANIMO DE LUCRO PERTENECIENTES AL REG. TRIBUTARIO ESPECIAL',
            'FINAL', 'DOS', 100, 0, 'N', 'N', 'N', 'N', 'N')
    MATCHING (codformato, codigo, renglon);
    UPDATE OR INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre,
                                        saldo, valor, porcentaje, tope, forzoso,
                                        mandatario, revelar, tendencia_credito,
                                        empleado)
    VALUES ('1004_V8', '8319', 3923,
            'POR DONACIONES EFECTUADAS A ENTIDADES S.A.L. NO CONTRIB. DE QUE TRATAN LOS ART. 22 Y 23 DEL E. T.',
            'FINAL', 'UNO', 100, 0, 'N', 'N', 'N', 'N', 'N')
    MATCHING (codformato, codigo, renglon);
    UPDATE OR INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre,
                                        saldo, valor, porcentaje, tope, forzoso,
                                        mandatario, revelar, tendencia_credito,
                                        empleado)
    VALUES ('1004_V8', '8319', 3924,
            'POR DONACIONES EFECTUADAS A ENTIDADES S.A.L. NO CONTRIB. DE QUE TRATAN LOS ART. 22 Y 23 DEL E. T.',
            'FINAL', 'DOS', 100, 0, 'N', 'N', 'N', 'N', 'N')
    MATCHING (codformato, codigo, renglon);
    UPDATE OR INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre,
                                        saldo, valor, porcentaje, tope, forzoso,
                                        mandatario, revelar, tendencia_credito,
                                        empleado)
    VALUES ('1004_V8', '8320', 3925,
            'PARA INVERSIONES REALIZADAS EN CONTROL, CONSERVACION Y MEJORAMIENTO DEL MEDIO AMBIENTE',
            'FINAL', 'UNO', 100, 0, 'N', 'N', 'N', 'N', 'N')
    MATCHING (codformato, codigo, renglon);
    UPDATE OR INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre,
                                        saldo, valor, porcentaje, tope, forzoso,
                                        mandatario, revelar, tendencia_credito,
                                        empleado)
    VALUES ('1004_V8', '8320', 3926,
            'PARA INVERSIONES REALIZADAS EN CONTROL, CONSERVACION Y MEJORAMIENTO DEL MEDIO AMBIENTE',
            'FINAL', 'DOS', 100, 0, 'N', 'N', 'N', 'N', 'N')
    MATCHING (codformato, codigo, renglon);
    UPDATE OR INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre,
                                        saldo, valor, porcentaje, tope, forzoso,
                                        mandatario, revelar, tendencia_credito,
                                        empleado)
    VALUES ('1004_V8', '8321', 3927,
            'POR DONACIONES EN LA RED NACIONAL DE BIBLIOTECAS PUBLICAS Y BIBLIOTECA NACIONAL',
            'FINAL', 'UNO', 100, 0, 'N', 'N', 'N', 'N', 'N')
    MATCHING (codformato, codigo, renglon);
    UPDATE OR INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre,
                                        saldo, valor, porcentaje, tope, forzoso,
                                        mandatario, revelar, tendencia_credito,
                                        empleado)
    VALUES ('1004_V8', '8321', 3928,
            'POR DONACIONES EN LA RED NACIONAL DE BIBLIOTECAS PUBLICAS Y BIBLIOTECA NACIONAL',
            'FINAL', 'DOS', 100, 0, 'N', 'N', 'N', 'N', 'N')
    MATCHING (codformato, codigo, renglon);
    UPDATE OR INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre,
                                        saldo, valor, porcentaje, tope, forzoso,
                                        mandatario, revelar, tendencia_credito,
                                        empleado)
    VALUES ('1004_V8', '8322', 3929,
            'POR DONACIONES A FAVOR DE FONDO PARA REPARACION DE VICTIMAS',
            'FINAL', 'UNO', 100, 0, 'N', 'N', 'N', 'N', 'N')
    MATCHING (codformato, codigo, renglon);
    UPDATE OR INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre,
                                        saldo, valor, porcentaje, tope, forzoso,
                                        mandatario, revelar, tendencia_credito,
                                        empleado)
    VALUES ('1004_V8', '8322', 3930,
            'POR DONACIONES A FAVOR DE FONDO PARA REPARACION DE VICTIMAS',
            'FINAL', 'DOS', 100, 0, 'N', 'N', 'N', 'N', 'N')
    MATCHING (codformato, codigo, renglon);
    UPDATE OR INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre,
                                        saldo, valor, porcentaje, tope, forzoso,
                                        mandatario, revelar, tendencia_credito,
                                        empleado)
    VALUES ('1004_V8', '8323', 3931,
            'POR IMPUESTOS PAGADOS EN EL EXTERIOR POR LA ENTIDAD CONTROLADA DEL EXTERIOR (ECE)',
            'FINAL', 'UNO', 100, 0, 'N', 'N', 'N', 'N', 'N')
    MATCHING (codformato, codigo, renglon);
    UPDATE OR INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre,
                                        saldo, valor, porcentaje, tope, forzoso,
                                        mandatario, revelar, tendencia_credito,
                                        empleado)
    VALUES ('1004_V8', '8323', 3932,
            'POR IMPUESTOS PAGADOS EN EL EXTERIOR POR LA ENTIDAD CONTROLADA DEL EXTERIOR (ECE)',
            'FINAL', 'DOS', 100, 0, 'N', 'N', 'N', 'N', 'N')
    MATCHING (codformato, codigo, renglon);
    UPDATE OR INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre,
                                        saldo, valor, porcentaje, tope, forzoso,
                                        mandatario, revelar, tendencia_credito,
                                        empleado)
    VALUES ('1004_V8', '8324', 3933,
            'POR DONACION A LA CORP. GUSTAVO MATAMOROS Y DEMAS FUND. DEDICADAS A LA DEFENSA, PROTECCION DE D.H.',
            'FINAL', 'UNO', 100, 0, 'N', 'N', 'N', 'N', 'N')
    MATCHING (codformato, codigo, renglon);
    UPDATE OR INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre,
                                        saldo, valor, porcentaje, tope, forzoso,
                                        mandatario, revelar, tendencia_credito,
                                        empleado)
    VALUES ('1004_V8', '8324', 3934,
            'POR DONACION A LA CORP. GUSTAVO MATAMOROS Y DEMAS FUND. DEDICADAS A LA DEFENSA, PROTECCION DE D.H.',
            'FINAL', 'DOS', 100, 0, 'N', 'N', 'N', 'N', 'N')
    MATCHING (codformato, codigo, renglon);
    UPDATE OR INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre,
                                        saldo, valor, porcentaje, tope, forzoso,
                                        mandatario, revelar, tendencia_credito,
                                        empleado)
    VALUES ('1004_V8', '8325', 3935,
            'POR DONACION A ORGANISMOS DE DEPORTE AFICIONADO. E.T., ART. 126- 2, INCISO.2.',
            'FINAL', 'UNO', 100, 0, 'N', 'N', 'N', 'N', 'N')
    MATCHING (codformato, codigo, renglon);
    UPDATE OR INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre,
                                        saldo, valor, porcentaje, tope, forzoso,
                                        mandatario, revelar, tendencia_credito,
                                        empleado)
    VALUES ('1004_V8', '8325', 3936,
            'POR DONACION A ORGANISMOS DE DEPORTE AFICIONADO. E.T., ART. 126- 2, INCISO.2.',
            'FINAL', 'DOS', 100, 0, 'N', 'N', 'N', 'N', 'N')
    MATCHING (codformato, codigo, renglon);
    UPDATE OR INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre,
                                        saldo, valor, porcentaje, tope, forzoso,
                                        mandatario, revelar, tendencia_credito,
                                        empleado)
    VALUES ('1004_V8', '8326', 3937,
            'POR DONACION A ORG. DEPORTIVOS Y RECREATIVOS O CULTURALES PERS. JURIDICAS SIN ANIMO DE LUCRO',
            'FINAL', 'UNO', 100, 0, 'N', 'N', 'N', 'N', 'N')
    MATCHING (codformato, codigo, renglon);
    UPDATE OR INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre,
                                        saldo, valor, porcentaje, tope, forzoso,
                                        mandatario, revelar, tendencia_credito,
                                        empleado)
    VALUES ('1004_V8', '8326', 3938,
            'POR DONACION A ORG. DEPORTIVOS Y RECREATIVOS O CULTURALES PERS. JURIDICAS SIN ANIMO DE LUCRO',
            'FINAL', 'DOS', 100, 0, 'N', 'N', 'N', 'N', 'N')
    MATCHING (codformato, codigo, renglon);
    UPDATE OR INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre,
                                        saldo, valor, porcentaje, tope, forzoso,
                                        mandatario, revelar, tendencia_credito,
                                        empleado)
    VALUES ('1004_V8', '8327', 3939,
            'POR DONACIONES EFECTUADAS PARA EL APADRINAMIENTO DE PARQUES NATURALES Y CONSER. DE BOSQUES NATURALES',
            'FINAL', 'UNO', 100, 0, 'N', 'N', 'N', 'N', 'N')
    MATCHING (codformato, codigo, renglon);
    UPDATE OR INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre,
                                        saldo, valor, porcentaje, tope, forzoso,
                                        mandatario, revelar, tendencia_credito,
                                        empleado)
    VALUES ('1004_V8', '8327', 3940,
            'POR DONACIONES EFECTUADAS PARA EL APADRINAMIENTO DE PARQUES NATURALES Y CONSER. DE BOSQUES NATURALES',
            'FINAL', 'DOS', 100, 0, 'N', 'N', 'N', 'N', 'N')
    MATCHING (codformato, codigo, renglon);
    UPDATE OR INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre,
                                        saldo, valor, porcentaje, tope, forzoso,
                                        mandatario, revelar, tendencia_credito,
                                        empleado)
    VALUES ('1004_V8', '8328', 3941,
            'POR APORTES AL S.G.P. A CARGO DEL EMPLEADOR CONTRIBUYENTE DEL IMP. UNIF. DEL R.S.T.',
            'FINAL', 'UNO', 100, 0, 'N', 'N', 'N', 'N', 'N')
    MATCHING (codformato, codigo, renglon);
    UPDATE OR INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre,
                                        saldo, valor, porcentaje, tope, forzoso,
                                        mandatario, revelar, tendencia_credito,
                                        empleado)
    VALUES ('1004_V8', '8328', 3942,
            'POR APORTES AL S.G.P. A CARGO DEL EMPLEADOR CONTRIBUYENTE DEL IMP. UNIF. DEL R.S.T.',
            'FINAL', 'DOS', 100, 0, 'N', 'N', 'N', 'N', 'N')
    MATCHING (codformato, codigo, renglon);
    UPDATE OR INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre,
                                        saldo, valor, porcentaje, tope, forzoso,
                                        mandatario, revelar, tendencia_credito,
                                        empleado)
    VALUES ('1004_V8', '8329', 3943,
            'POR VENTAS DE BIENES O SERVICIOS CON TARJETAS Y OTROS MECANISMOS ELECTRONICOS DE PAGOS',
            'FINAL', 'UNO', 100, 0, 'N', 'N', 'N', 'N', 'N')
    MATCHING (codformato, codigo, renglon);
    UPDATE OR INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre,
                                        saldo, valor, porcentaje, tope, forzoso,
                                        mandatario, revelar, tendencia_credito,
                                        empleado)
    VALUES ('1004_V8', '8329', 3944,
            'POR VENTAS DE BIENES O SERVICIOS CON TARJETAS Y OTROS MECANISMOS ELECTRONICOS DE PAGOS',
            'FINAL', 'DOS', 100, 0, 'N', 'N', 'N', 'N', 'N')
    MATCHING (codformato, codigo, renglon);
    UPDATE OR INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre,
                                        saldo, valor, porcentaje, tope, forzoso,
                                        mandatario, revelar, tendencia_credito,
                                        empleado)
    VALUES ('1004_V8', '8331', 3945,
            'POR IVA DE VENTAS EN LA IMPORT/ FORMACION/CONSTRUC. O ADQ. DE ACTIVOS FIJOS REALES PRODUCTIVOS',
            'FINAL', 'UNO', 100, 0, 'N', 'N', 'N', 'N', 'N')
    MATCHING (codformato, codigo, renglon);
    UPDATE OR INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre,
                                        saldo, valor, porcentaje, tope, forzoso,
                                        mandatario, revelar, tendencia_credito,
                                        empleado)
    VALUES ('1004_V8', '8331', 3946,
            'POR IVA DE VENTAS EN LA IMPORT/ FORMACION/CONSTRUC. O ADQ. DE ACTIVOS FIJOS REALES PRODUCTIVOS',
            'FINAL', 'DOS', 100, 0, 'N', 'N', 'N', 'N', 'N')
    MATCHING (codformato, codigo, renglon);
    UPDATE OR INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre,
                                        saldo, valor, porcentaje, tope, forzoso,
                                        mandatario, revelar, tendencia_credito,
                                        empleado)
    VALUES ('1004_V8', '8332', 3947,
            'POR CONV. CON COLDEPORTES PARA BECAS DE ESTUDIO Y MANUT. A DEPORTISTAS TALENTO O RESERVA DEPORTIVA',
            'FINAL', 'UNO', 100, 0, 'N', 'N', 'N', 'N', 'N')
    MATCHING (codformato, codigo, renglon);
    UPDATE OR INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre,
                                        saldo, valor, porcentaje, tope, forzoso,
                                        mandatario, revelar, tendencia_credito,
                                        empleado)
    VALUES ('1004_V8', '8332', 3948,
            'POR CONV. CON COLDEPORTES PARA BECAS DE ESTUDIO Y MANUT. A DEPORTISTAS TALENTO O RESERVA DEPORTIVA',
            'FINAL', 'DOS', 100, 0, 'N', 'N', 'N', 'N', 'N')
    MATCHING (codformato, codigo, renglon);
    UPDATE OR INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre,
                                        saldo, valor, porcentaje, tope, forzoso,
                                        mandatario, revelar, tendencia_credito,
                                        empleado)
    VALUES ('1004_V8', '8333', 3949,
            'PARA INVERSIONES EN CONTROL,CONSERVACION Y MEJORAMIENTO DEL MEDIO AMBIENTE EN ACTIVIDADES TURISTICAS',
            'FINAL', 'UNO', 100, 0, 'N', 'N', 'N', 'N', 'N')
    MATCHING (codformato, codigo, renglon);
    UPDATE OR INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre,
                                        saldo, valor, porcentaje, tope, forzoso,
                                        mandatario, revelar, tendencia_credito,
                                        empleado)
    VALUES ('1004_V8', '8333', 3950,
            'PARA INVERSIONES EN CONTROL,CONSERVACION Y MEJORAMIENTO DEL MEDIO AMBIENTE EN ACTIVIDADES TURISTICAS',
            'FINAL', 'DOS', 100, 0, 'N', 'N', 'N', 'N', 'N')
    MATCHING (codformato, codigo, renglon);
    UPDATE OR INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre,
                                        saldo, valor, porcentaje, tope, forzoso,
                                        mandatario, revelar, tendencia_credito,
                                        empleado)
    VALUES ('1004_V8', '8334', 3951,
            'POR DONACIONES REALIZADAS A LA AGENCIA DE EMPRENDIMIENTO E INNOVACION DEL GOBIERNO NACIONAL INNPULSA',
            'FINAL', 'UNO', 100, 0, 'N', 'N', 'N', 'N', 'N')
    MATCHING (codformato, codigo, renglon);
    UPDATE OR INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre,
                                        saldo, valor, porcentaje, tope, forzoso,
                                        mandatario, revelar, tendencia_credito,
                                        empleado)
    VALUES ('1004_V8', '8334', 3952,
            'POR DONACIONES REALIZADAS A LA AGENCIA DE EMPRENDIMIENTO E INNOVACION DEL GOBIERNO NACIONAL INNPULSA',
            'FINAL', 'DOS', 100, 0, 'N', 'N', 'N', 'N', 'N')
    MATCHING (codformato, codigo, renglon);
    UPDATE OR INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre,
                                        saldo, valor, porcentaje, tope, forzoso,
                                        mandatario, revelar, tendencia_credito,
                                        empleado)
    VALUES ('1004_V8', '8336', 3953,
            'POR DONAC. RECIB. POR EL FONDO NAL.DE FIN.PARA LA CIENCIA TECN. Y LA INNOV.,FRANCISCO JOSE DE CALDAS',
            'FINAL', 'UNO', 100, 0, 'N', 'N', 'N', 'N', 'N')
    MATCHING (codformato, codigo, renglon);
    UPDATE OR INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre,
                                        saldo, valor, porcentaje, tope, forzoso,
                                        mandatario, revelar, tendencia_credito,
                                        empleado)
    VALUES ('1004_V8', '8336', 3954,
            'POR DONAC. RECIB. POR EL FONDO NAL.DE FIN.PARA LA CIENCIA TECN. Y LA INNOV.,FRANCISCO JOSE DE CALDAS',
            'FINAL', 'DOS', 100, 0, 'N', 'N', 'N', 'N', 'N')
    MATCHING (codformato, codigo, renglon);
    UPDATE OR INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre,
                                        saldo, valor, porcentaje, tope, forzoso,
                                        mandatario, revelar, tendencia_credito,
                                        empleado)
    VALUES ('1004_V8', '8337', 3955,
            'POR REMUN. POR VINCULACION DE PERSONAL CON TITULO DE DOCTORADO EN LAS EMPRESAS CONTRIB. DE RENTA',
            'FINAL', 'UNO', 100, 0, 'N', 'N', 'N', 'N', 'N')
    MATCHING (codformato, codigo, renglon);
    UPDATE OR INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre,
                                        saldo, valor, porcentaje, tope, forzoso,
                                        mandatario, revelar, tendencia_credito,
                                        empleado)
    VALUES ('1004_V8', '8337', 3956,
            'POR REMUN. POR VINCULACION DE PERSONAL CON TITULO DE DOCTORADO EN LAS EMPRESAS CONTRIB. DE RENTA',
            'FINAL', 'DOS', 100, 0, 'N', 'N', 'N', 'N', 'N')
    MATCHING (codformato, codigo, renglon);
    UPDATE OR INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre,
                                        saldo, valor, porcentaje, tope, forzoso,
                                        mandatario, revelar, tendencia_credito,
                                        empleado)
    VALUES ('1004_V8', '8338', 3957,
            'POR DONACIONES RECIB. POR INTERMEDIO DEL ICETEX,BECAS QUE FINANCIEN LA EDUCACION A LA FUERZA PUBLICA',
            'FINAL', 'UNO', 100, 0, 'N', 'N', 'N', 'N', 'N')
    MATCHING (codformato, codigo, renglon);
    UPDATE OR INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre,
                                        saldo, valor, porcentaje, tope, forzoso,
                                        mandatario, revelar, tendencia_credito,
                                        empleado)
    VALUES ('1004_V8', '8338', 3958,
            'POR DONACIONES RECIB. POR INTERMEDIO DEL ICETEX,BECAS QUE FINANCIEN LA EDUCACION A LA FUERZA PUBLICA',
            'FINAL', 'DOS', 100, 0, 'N', 'N', 'N', 'N', 'N')
    MATCHING (codformato, codigo, renglon);

  END
END;

EXECUTE BLOCK
AS
DECLARE VARIABLE v_mm INTEGER;
BEGIN

  v_mm = 0;
  SELECT COUNT(1)
  FROM mm_formatos
  WHERE codigo = '1005_V8'
  INTO v_mm;

  IF (:v_mm = 0) THEN
  BEGIN
    UPDATE OR INSERT INTO mm_formatos (codigo, nombre, entinfo, cpt, tdoc, nid,
                                       dv, apl1, apl2, nom1, nom2, raz, dir,
                                       dpto, mun, pais, email, valor1, valor2,
                                       valor3, valor4, valor5, valor6, valor7,
                                       valor8, valor9, valor0, valor10, valor11,
                                       valor12, valor13, valor14, valor15,
                                       valor16, valor17, valor18, valor19,
                                       valor20, valor21, valor22, valor23,
                                       valor24, valor25, valor26, total, idem,
                                       tdocm, nitm, dvm, apl1m, apl2m, nom1m,
                                       nom2m, razm, informar, con_informados,
                                       nelemento, nvalor1, nvalor2, nvalor3,
                                       nvalor4, nvalor5, nvalor6, nvalor7,
                                       nvalor8, nvalor9, nvalor0, nvalor10,
                                       nvalor11, nvalor12, nvalor13, nvalor14,
                                       nvalor15, nvalor16, nvalor17, nvalor18,
                                       nvalor19, nvalor20, nvalor21, nvalor22,
                                       nvalor23, nvalor24, nvalor25, nvalor26,
                                       nidem, ncpt, referencia, revision,
                                       cadena_adicional, equivalencia, valor27,
                                       valor28, valor29, nvalor27, nvalor28,
                                       nvalor29)
    VALUES ('1005_V8', 'IMPUESTO A LAS VENTAS POR PAGAR DESCONTABLE', NULL, 'N',
            'S', 'S', 'S', 'S', 'S', 'S', 'S', 'S', 'N', 'N', 'N', 'N', 'N',
            'S', 'S', 'S', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N',
            'N', 'N', 'N', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N',
            'N', 'impventas', 'vimp', 'ivade', 'ivavcg', NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            'IMPUESTO SOBRE LAS VENTAS DESCONTABLE, INCLUIDO EL VALOR DE LAS DEVOLUCIONES EN VENTAS, REBAJAS Y DESCUENTOS ART. 631 E.T. RESOLUCIÓN NO10147/05 ART. 9',
            8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

    UPDATE OR INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre,
                                        saldo, valor, porcentaje, tope, forzoso,
                                        mandatario, revelar, tendencia_credito,
                                        empleado)
    VALUES ('1005_V8', '10058', 3959, 'IMPUESTO DESCONTABLE', 'FINAL', 'UNO',
            100, 0, 'N', 'N', 'N', 'N', 'N')
    MATCHING (codformato, codigo, renglon);
    UPDATE OR INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre,
                                        saldo, valor, porcentaje, tope, forzoso,
                                        mandatario, revelar, tendencia_credito,
                                        empleado)
    VALUES ('1005_V8', '10058', 3960, 'IMPUESTO DESCONTABLE', 'FINAL', 'UNO',
            100, 0, 'N', 'N', 'N', 'N', 'N')
    MATCHING (codformato, codigo, renglon);
    UPDATE OR INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre,
                                        saldo, valor, porcentaje, tope, forzoso,
                                        mandatario, revelar, tendencia_credito,
                                        empleado)
    VALUES ('1005_V8', '10058', 3961, 'IMPUESTO DESCONTABLE', 'FINAL', 'UNO',
            100, 0, 'N', 'N', 'N', 'N', 'N')
    MATCHING (codformato, codigo, renglon);
    UPDATE OR INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre,
                                        saldo, valor, porcentaje, tope, forzoso,
                                        mandatario, revelar, tendencia_credito,
                                        empleado)
    VALUES ('1005_V8', '10058', 3962, 'IMPUESTO DESCONTABLE', 'FINAL', 'UNO',
            100, 0, 'N', 'N', 'N', 'N', 'N')
    MATCHING (codformato, codigo, renglon);
    UPDATE OR INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre,
                                        saldo, valor, porcentaje, tope, forzoso,
                                        mandatario, revelar, tendencia_credito,
                                        empleado)
    VALUES ('1005_V8', '10058', 3963, 'IMPUESTO DESCONTABLE', 'FINAL', 'UNO',
            100, 0, 'N', 'N', 'N', 'N', 'N')
    MATCHING (codformato, codigo, renglon);
    UPDATE OR INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre,
                                        saldo, valor, porcentaje, tope, forzoso,
                                        mandatario, revelar, tendencia_credito,
                                        empleado)
    VALUES ('1005_V8', '10058', 3964, 'IMPUESTO DESCONTABLE', 'FINAL', 'UNO',
            100, 0, 'N', 'N', 'N', 'N', 'N')
    MATCHING (codformato, codigo, renglon);
    UPDATE OR INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre,
                                        saldo, valor, porcentaje, tope, forzoso,
                                        mandatario, revelar, tendencia_credito,
                                        empleado)
    VALUES ('1005_V8', '10058', 3965, 'IMPUESTO DESCONTABLE', 'FINAL', 'UNO',
            100, 0, 'N', 'N', 'N', 'N', 'N')
    MATCHING (codformato, codigo, renglon);
    UPDATE OR INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre,
                                        saldo, valor, porcentaje, tope, forzoso,
                                        mandatario, revelar, tendencia_credito,
                                        empleado)
    VALUES ('1005_V8', '10058', 3966, 'IMPUESTO DESCONTABLE', 'FINAL', 'UNO',
            100, 0, 'N', 'N', 'N', 'N', 'N')
    MATCHING (codformato, codigo, renglon);
    UPDATE OR INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre,
                                        saldo, valor, porcentaje, tope, forzoso,
                                        mandatario, revelar, tendencia_credito,
                                        empleado)
    VALUES ('1005_V8', '10058', 3967, 'IMPUESTO DESCONTABLE', 'FINAL', 'UNO',
            100, 0, 'N', 'N', 'N', 'N', 'N')
    MATCHING (codformato, codigo, renglon);
    UPDATE OR INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre,
                                        saldo, valor, porcentaje, tope, forzoso,
                                        mandatario, revelar, tendencia_credito,
                                        empleado)
    VALUES ('1005_V8', '10058', 3968, 'IMPUESTO DESCONTABLE', 'FINAL', 'UNO',
            100, 0, 'N', 'N', 'N', 'N', 'N')
    MATCHING (codformato, codigo, renglon);
    UPDATE OR INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre,
                                        saldo, valor, porcentaje, tope, forzoso,
                                        mandatario, revelar, tendencia_credito,
                                        empleado)
    VALUES ('1005_V8', '10058', 3969,
            'IVA RESULTANTE POR DEVOLUCIONES EN VENTAS ANULADAS,RESCINDIDAS O RESUELTAS',
            'FINAL', 'DOS', 100, 0, 'N', 'N', 'N', 'N', 'N')
    MATCHING (codformato, codigo, renglon);
    UPDATE OR INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre,
                                        saldo, valor, porcentaje, tope, forzoso,
                                        mandatario, revelar, tendencia_credito,
                                        empleado)
    VALUES ('1005_V8', '10058', 3970,
            'IVA RESULTANTE POR DEVOLUCIONES EN VENTAS ANULADAS,RESCINDIDAS O RESUELTAS',
            'FINAL', 'DOS', 100, 0, 'N', 'N', 'N', 'N', 'N')
    MATCHING (codformato, codigo, renglon);
    UPDATE OR INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre,
                                        saldo, valor, porcentaje, tope, forzoso,
                                        mandatario, revelar, tendencia_credito,
                                        empleado)
    VALUES ('1005_V8', '10058', 3971,
            'IVA RESULTANTE POR DEVOLUCIONES EN VENTAS ANULADAS,RESCINDIDAS O RESUELTAS',
            'FINAL', 'DOS', 100, 0, 'N', 'N', 'N', 'N', 'N')
    MATCHING (codformato, codigo, renglon);
    UPDATE OR INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre,
                                        saldo, valor, porcentaje, tope, forzoso,
                                        mandatario, revelar, tendencia_credito,
                                        empleado)
    VALUES ('1005_V8', '10058', 3972,
            'IVA RESULTANTE POR DEVOLUCIONES EN VENTAS ANULADAS,RESCINDIDAS O RESUELTAS',
            'FINAL', 'DOS', 100, 0, 'N', 'N', 'N', 'N', 'N')
    MATCHING (codformato, codigo, renglon);
    UPDATE OR INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre,
                                        saldo, valor, porcentaje, tope, forzoso,
                                        mandatario, revelar, tendencia_credito,
                                        empleado)
    VALUES ('1005_V8', '10058', 3973,
            'IVA RESULTANTE POR DEVOLUCIONES EN VENTAS ANULADAS,RESCINDIDAS O RESUELTAS',
            'FINAL', 'DOS', 100, 0, 'N', 'N', 'N', 'N', 'N')
    MATCHING (codformato, codigo, renglon);
    UPDATE OR INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre,
                                        saldo, valor, porcentaje, tope, forzoso,
                                        mandatario, revelar, tendencia_credito,
                                        empleado)
    VALUES ('1005_V8', '10058', 3974,
            'IVA RESULTANTE POR DEVOLUCIONES EN VENTAS ANULADAS,RESCINDIDAS O RESUELTAS',
            'FINAL', 'DOS', 100, 0, 'N', 'N', 'N', 'N', 'N')
    MATCHING (codformato, codigo, renglon);
    UPDATE OR INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre,
                                        saldo, valor, porcentaje, tope, forzoso,
                                        mandatario, revelar, tendencia_credito,
                                        empleado)
    VALUES ('1005_V8', '10058', 3975,
            'IVA RESULTANTE POR DEVOLUCIONES EN VENTAS ANULADAS,RESCINDIDAS O RESUELTAS',
            'FINAL', 'DOS', 100, 0, 'N', 'N', 'N', 'N', 'N')
    MATCHING (codformato, codigo, renglon);
    UPDATE OR INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre,
                                        saldo, valor, porcentaje, tope, forzoso,
                                        mandatario, revelar, tendencia_credito,
                                        empleado)
    VALUES ('1005_V8', '10058', 3976,
            'IVA RESULTANTE POR DEVOLUCIONES EN VENTAS ANULADAS,RESCINDIDAS O RESUELTAS',
            'FINAL', 'DOS', 100, 0, 'N', 'N', 'N', 'N', 'N')
    MATCHING (codformato, codigo, renglon);
    UPDATE OR INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre,
                                        saldo, valor, porcentaje, tope, forzoso,
                                        mandatario, revelar, tendencia_credito,
                                        empleado)
    VALUES ('1005_V8', '10058', 3977,
            'IVA TRATADO COMO MAYOR VALOR DEL COSTO O GASTO - ART 490 E.T.',
            'FINAL', 'TRES', 100, 0, 'N', 'N', 'N', 'N', 'N')
    MATCHING (codformato, codigo, renglon);
    UPDATE OR INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre,
                                        saldo, valor, porcentaje, tope, forzoso,
                                        mandatario, revelar, tendencia_credito,
                                        empleado)
    VALUES ('1005_V8', '10058', 3978,
            'IVA TRATADO COMO MAYOR VALOR DEL COSTO O GASTO - ART 490 E.T.',
            'FINAL', 'TRES', 100, 0, 'N', 'N', 'N', 'N', 'N')
    MATCHING (codformato, codigo, renglon);
    UPDATE OR INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre,
                                        saldo, valor, porcentaje, tope, forzoso,
                                        mandatario, revelar, tendencia_credito,
                                        empleado)
    VALUES ('1005_V8', '10058', 3979,
            'IVA TRATADO COMO MAYOR VALOR DEL COSTO O GASTO - ART 490 E.T.',
            'FINAL', 'TRES', 100, 0, 'N', 'N', 'N', 'N', 'N')
    MATCHING (codformato, codigo, renglon);
    UPDATE OR INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre,
                                        saldo, valor, porcentaje, tope, forzoso,
                                        mandatario, revelar, tendencia_credito,
                                        empleado)
    VALUES ('1005_V8', '10058', 3980,
            'IVA TRATADO COMO MAYOR VALOR DEL COSTO O GASTO - ART 490 E.T.',
            'FINAL', 'TRES', 100, 0, 'N', 'N', 'N', 'N', 'N')
    MATCHING (codformato, codigo, renglon);
    UPDATE OR INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre,
                                        saldo, valor, porcentaje, tope, forzoso,
                                        mandatario, revelar, tendencia_credito,
                                        empleado)
    VALUES ('1005_V8', '10058', 3981,
            'IVA TRATADO COMO MAYOR VALOR DEL COSTO O GASTO - ART 490 E.T.',
            'FINAL', 'TRES', 100, 0, 'N', 'N', 'N', 'N', 'N')
    MATCHING (codformato, codigo, renglon);
    UPDATE OR INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre,
                                        saldo, valor, porcentaje, tope, forzoso,
                                        mandatario, revelar, tendencia_credito,
                                        empleado)
    VALUES ('1005_V8', '10058', 3982,
            'IVA TRATADO COMO MAYOR VALOR DEL COSTO O GASTO - ART 490 E.T.',
            'FINAL', 'TRES', 100, 0, 'N', 'N', 'N', 'N', 'N')
    MATCHING (codformato, codigo, renglon);
    UPDATE OR INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre,
                                        saldo, valor, porcentaje, tope, forzoso,
                                        mandatario, revelar, tendencia_credito,
                                        empleado)
    VALUES ('1005_V8', '10058', 3983,
            'IVA TRATADO COMO MAYOR VALOR DEL COSTO O GASTO - ART 490 E.T.',
            'FINAL', 'TRES', 100, 0, 'N', 'N', 'N', 'N', 'N')
    MATCHING (codformato, codigo, renglon);
    UPDATE OR INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre,
                                        saldo, valor, porcentaje, tope, forzoso,
                                        mandatario, revelar, tendencia_credito,
                                        empleado)
    VALUES ('1005_V8', '10058', 3984,
            'IVA TRATADO COMO MAYOR VALOR DEL COSTO O GASTO - ART 490 E.T.',
            'FINAL', 'TRES', 100, 0, 'N', 'N', 'N', 'N', 'N')
    MATCHING (codformato, codigo, renglon);
    UPDATE OR INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre,
                                        saldo, valor, porcentaje, tope, forzoso,
                                        mandatario, revelar, tendencia_credito,
                                        empleado)
    VALUES ('1005_V8', '10058', 3985,
            'IVA TRATADO COMO MAYOR VALOR DEL COSTO O GASTO - ART 490 E.T.',
            'FINAL', 'TRES', 100, 0, 'N', 'N', 'N', 'N', 'N')
    MATCHING (codformato, codigo, renglon);
    UPDATE OR INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre,
                                        saldo, valor, porcentaje, tope, forzoso,
                                        mandatario, revelar, tendencia_credito,
                                        empleado)
    VALUES ('1005_V8', '10058', 3986,
            'IVA TRATADO COMO MAYOR VALOR DEL COSTO O GASTO - ART 490 E.T.',
            'FINAL', 'TRES', 100, 0, 'N', 'N', 'N', 'N', 'N')
    MATCHING (codformato, codigo, renglon);
    UPDATE OR INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre,
                                        saldo, valor, porcentaje, tope, forzoso,
                                        mandatario, revelar, tendencia_credito,
                                        empleado)
    VALUES ('1005_V8', '10058', 3987,
            'IVA TRATADO COMO MAYOR VALOR DEL COSTO O GASTO - ART 490 E.T.',
            'FINAL', 'TRES', 100, 0, 'N', 'N', 'N', 'N', 'N')
    MATCHING (codformato, codigo, renglon);
    UPDATE OR INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre,
                                        saldo, valor, porcentaje, tope, forzoso,
                                        mandatario, revelar, tendencia_credito,
                                        empleado)
    VALUES ('1005_V8', '10058', 3988,
            'IVA TRATADO COMO MAYOR VALOR DEL COSTO O GASTO - ART 490 E.T.',
            'FINAL', 'TRES', 100, 0, 'N', 'N', 'N', 'N', 'N')
    MATCHING (codformato, codigo, renglon);
    UPDATE OR INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre,
                                        saldo, valor, porcentaje, tope, forzoso,
                                        mandatario, revelar, tendencia_credito,
                                        empleado)
    VALUES ('1005_V8', '10058', 3989,
            'IVA TRATADO COMO MAYOR VALOR DEL COSTO O GASTO - ART 490 E.T.',
            'FINAL', 'TRES', 100, 0, 'N', 'N', 'N', 'N', 'N')
    MATCHING (codformato, codigo, renglon);
    UPDATE OR INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre,
                                        saldo, valor, porcentaje, tope, forzoso,
                                        mandatario, revelar, tendencia_credito,
                                        empleado)
    VALUES ('1005_V8', '10058', 3990,
            'IVA TRATADO COMO MAYOR VALOR DEL COSTO O GASTO - ART 490 E.T.',
            'FINAL', 'TRES', 100, 0, 'N', 'N', 'N', 'N', 'N')
    MATCHING (codformato, codigo, renglon);
    UPDATE OR INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre,
                                        saldo, valor, porcentaje, tope, forzoso,
                                        mandatario, revelar, tendencia_credito,
                                        empleado)
    VALUES ('1005_V8', '10058', 3991,
            'IVA TRATADO COMO MAYOR VALOR DEL COSTO O GASTO - ART 490 E.T.',
            'FINAL', 'TRES', 100, 0, 'N', 'N', 'N', 'N', 'N')
    MATCHING (codformato, codigo, renglon);
    UPDATE OR INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre,
                                        saldo, valor, porcentaje, tope, forzoso,
                                        mandatario, revelar, tendencia_credito,
                                        empleado)
    VALUES ('1005_V8', '10058', 3992,
            'IVA TRATADO COMO MAYOR VALOR DEL COSTO O GASTO - ART 490 E.T.',
            'FINAL', 'TRES', 100, 0, 'N', 'N', 'N', 'N', 'N')
    MATCHING (codformato, codigo, renglon);
    UPDATE OR INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre,
                                        saldo, valor, porcentaje, tope, forzoso,
                                        mandatario, revelar, tendencia_credito,
                                        empleado)
    VALUES ('1005_V8', '10058', 3993,
            'IVA TRATADO COMO MAYOR VALOR DEL COSTO O GASTO - ART 490 E.T.',
            'FINAL', 'TRES', 100, 0, 'N', 'N', 'N', 'N', 'N')
    MATCHING (codformato, codigo, renglon);
    UPDATE OR INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre,
                                        saldo, valor, porcentaje, tope, forzoso,
                                        mandatario, revelar, tendencia_credito,
                                        empleado)
    VALUES ('1005_V8', '10058', 3994,
            'IVA TRATADO COMO MAYOR VALOR DEL COSTO O GASTO - ART 490 E.T.',
            'FINAL', 'TRES', 100, 0, 'N', 'N', 'N', 'N', 'N')
    MATCHING (codformato, codigo, renglon);
    UPDATE OR INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre,
                                        saldo, valor, porcentaje, tope, forzoso,
                                        mandatario, revelar, tendencia_credito,
                                        empleado)
    VALUES ('1005_V8', '10058', 3995,
            'IVA TRATADO COMO MAYOR VALOR DEL COSTO O GASTO - ART 490 E.T.',
            'FINAL', 'TRES', 100, 0, 'N', 'N', 'N', 'N', 'N')
    MATCHING (codformato, codigo, renglon);
    UPDATE OR INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre,
                                        saldo, valor, porcentaje, tope, forzoso,
                                        mandatario, revelar, tendencia_credito,
                                        empleado)
    VALUES ('1005_V8', '10058', 3996,
            'IVA TRATADO COMO MAYOR VALOR DEL COSTO O GASTO - ART 490 E.T.',
            'FINAL', 'TRES', 100, 0, 'N', 'N', 'N', 'N', 'N')
    MATCHING (codformato, codigo, renglon);

  END
END;

EXECUTE BLOCK
AS
DECLARE VARIABLE v_mm INTEGER;
BEGIN

  v_mm = 0;
  SELECT COUNT(1)
  FROM mm_formatos
  WHERE codigo = '1010_V9'
  INTO v_mm;

  IF (:v_mm = 0) THEN
  BEGIN
    UPDATE OR INSERT INTO mm_formatos (codigo, nombre, entinfo, cpt, tdoc, nid,
                                       dv, apl1, apl2, nom1, nom2, raz, dir,
                                       dpto, mun, pais, email, valor1, valor2,
                                       valor3, valor4, valor5, valor6, valor7,
                                       valor8, valor9, valor0, valor10, valor11,
                                       valor12, valor13, valor14, valor15,
                                       valor16, valor17, valor18, valor19,
                                       valor20, valor21, valor22, valor23,
                                       valor24, valor25, valor26, total, idem,
                                       tdocm, nitm, dvm, apl1m, apl2m, nom1m,
                                       nom2m, razm, informar, con_informados,
                                       nelemento, nvalor1, nvalor2, nvalor3,
                                       nvalor4, nvalor5, nvalor6, nvalor7,
                                       nvalor8, nvalor9, nvalor0, nvalor10,
                                       nvalor11, nvalor12, nvalor13, nvalor14,
                                       nvalor15, nvalor16, nvalor17, nvalor18,
                                       nvalor19, nvalor20, nvalor21, nvalor22,
                                       nvalor23, nvalor24, nvalor25, nvalor26,
                                       nidem, ncpt, referencia, revision,
                                       cadena_adicional, equivalencia, valor27,
                                       valor28, valor29, nvalor27, nvalor28,
                                       nvalor29)
    VALUES ('1010_V9',
            'INFORMACION DE SOCIOS, ACCIONISTAS, COMUNEROS Y/O COOPERADOS',
            NULL, 'N', 'S', 'S', 'S', 'S', 'S', 'S', 'S', 'S', 'S', 'S', 'S',
            'S', 'N', 'S', 'S', 'S', 'S', 'N', 'N', 'N', 'N', 'N', 'N', 'N',
            'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, 'N', 'N', 'N', 'N', 'N', 'N', 'N',
            'N', 'N', 'N', 'N', 'socios', 'valnom', 'valprm', 'por', 'dec',
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL,
            'SOCIOS Y ACCIONISTAS LITERAL A) ART. 631 E.T. RESOLUCIÓN NO10147/05 ART. 2',
            9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

    UPDATE OR INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre,
                                        saldo, valor, porcentaje, tope, forzoso,
                                        mandatario, revelar, tendencia_credito,
                                        empleado)
    VALUES ('1010_V9', '10109', 3997,
            'VALOR NOMINAL DE LA ACCION,APORTE O DERECHO SOCIAL A DICIEMBRE 31',
            'FINAL', 'UNO', 100, 0, 'N', 'N', 'N', 'S', 'N')
    MATCHING (codformato, codigo, renglon);
    UPDATE OR INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre,
                                        saldo, valor, porcentaje, tope, forzoso,
                                        mandatario, revelar, tendencia_credito,
                                        empleado)
    VALUES ('1010_V9', '10109', 3998,
            'VALOR NOMINAL DE LA ACCION,APORTE O DERECHO SOCIAL A DICIEMBRE 31',
            'FINAL', 'UNO', 100, 0, 'N', 'N', 'N', 'S', 'N')
    MATCHING (codformato, codigo, renglon);
    UPDATE OR INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre,
                                        saldo, valor, porcentaje, tope, forzoso,
                                        mandatario, revelar, tendencia_credito,
                                        empleado)
    VALUES ('1010_V9', '10109', 3999,
            'VALOR NOMINAL DE LA ACCION,APORTE O DERECHO SOCIAL A DICIEMBRE 31',
            'FINAL', 'UNO', 100, 0, 'N', 'N', 'N', 'S', 'N')
    MATCHING (codformato, codigo, renglon);
    UPDATE OR INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre,
                                        saldo, valor, porcentaje, tope, forzoso,
                                        mandatario, revelar, tendencia_credito,
                                        empleado)
    VALUES ('1010_V9', '10109', 4000,
            'VALOR NOMINAL DE LA ACCION,APORTE O DERECHO SOCIAL A DICIEMBRE 31',
            'FINAL', 'UNO', 100, 0, 'N', 'N', 'N', 'S', 'N')
    MATCHING (codformato, codigo, renglon);
    UPDATE OR INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre,
                                        saldo, valor, porcentaje, tope, forzoso,
                                        mandatario, revelar, tendencia_credito,
                                        empleado)
    VALUES ('1010_V9', '10109', 4001,
            'VALOR PRIMA EN COLOCACION DE ACCIONES A DICIEMBRE 31', 'FINAL',
            'DOS', 100, 0, 'N', 'N', 'N', 'S', 'N')
    MATCHING (codformato, codigo, renglon);
    UPDATE OR INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre,
                                        saldo, valor, porcentaje, tope, forzoso,
                                        mandatario, revelar, tendencia_credito,
                                        empleado)
    VALUES ('1010_V9', '10109', 4002,
            'VALOR PRIMA EN COLOCACION DE ACCIONES A DICIEMBRE 31', 'FINAL',
            'DOS', 100, 0, 'N', 'N', 'N', 'S', 'N')
    MATCHING (codformato, codigo, renglon);
    UPDATE OR INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre,
                                        saldo, valor, porcentaje, tope, forzoso,
                                        mandatario, revelar, tendencia_credito,
                                        empleado)
    VALUES ('1010_V9', '10109', 4003,
            'VALOR PRIMA EN COLOCACION DE ACCIONES A DICIEMBRE 31', 'FINAL',
            'DOS', 100, 0, 'N', 'N', 'N', 'S', 'N')
    MATCHING (codformato, codigo, renglon);
    UPDATE OR INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre,
                                        saldo, valor, porcentaje, tope, forzoso,
                                        mandatario, revelar, tendencia_credito,
                                        empleado)
    VALUES ('1010_V9', '10109', 4004,
            'VALOR PRIMA EN COLOCACION DE ACCIONES A DICIEMBRE 31', 'FINAL',
            'DOS', 100, 0, 'N', 'N', 'N', 'S', 'N')
    MATCHING (codformato, codigo, renglon);
    UPDATE OR INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre,
                                        saldo, valor, porcentaje, tope, forzoso,
                                        mandatario, revelar, tendencia_credito,
                                        empleado)
    VALUES ('1010_V9', '10109', 4005, 'PORCENTAJE DE PARTICIPACION', 'FINAL',
            'TRES', 100, 0, 'N', 'N', 'N', 'S', 'N')
    MATCHING (codformato, codigo, renglon);
    UPDATE OR INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre,
                                        saldo, valor, porcentaje, tope, forzoso,
                                        mandatario, revelar, tendencia_credito,
                                        empleado)
    VALUES ('1010_V9', '10109', 4006, 'PORCENTAJE DE PARTICIPACION', 'FINAL',
            'TRES', 100, 0, 'N', 'N', 'N', 'S', 'N')
    MATCHING (codformato, codigo, renglon);
    UPDATE OR INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre,
                                        saldo, valor, porcentaje, tope, forzoso,
                                        mandatario, revelar, tendencia_credito,
                                        empleado)
    VALUES ('1010_V9', '10109', 4007, 'PORCENTAJE DE PARTICIPACION', 'FINAL',
            'TRES', 100, 0, 'N', 'N', 'N', 'S', 'N')
    MATCHING (codformato, codigo, renglon);
    UPDATE OR INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre,
                                        saldo, valor, porcentaje, tope, forzoso,
                                        mandatario, revelar, tendencia_credito,
                                        empleado)
    VALUES ('1010_V9', '10109', 4008, 'PORCENTAJE DE PARTICIPACION', 'FINAL',
            'TRES', 100, 0, 'N', 'N', 'N', 'S', 'N')
    MATCHING (codformato, codigo, renglon);
    UPDATE OR INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre,
                                        saldo, valor, porcentaje, tope, forzoso,
                                        mandatario, revelar, tendencia_credito,
                                        empleado)
    VALUES ('1010_V9', '10109', 4009,
            'POSICION DECIMAL DEL PORCENTAJE DE PARTICIPACION', 'FINAL',
            'CUATRO', 100, 0, 'N', 'N', 'N', 'S', 'N')
    MATCHING (codformato, codigo, renglon);
    UPDATE OR INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre,
                                        saldo, valor, porcentaje, tope, forzoso,
                                        mandatario, revelar, tendencia_credito,
                                        empleado)
    VALUES ('1010_V9', '10109', 4010,
            'POSICION DECIMAL DEL PORCENTAJE DE PARTICIPACION', 'FINAL',
            'CUATRO', 100, 0, 'N', 'N', 'N', 'S', 'N')
    MATCHING (codformato, codigo, renglon);
    UPDATE OR INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre,
                                        saldo, valor, porcentaje, tope, forzoso,
                                        mandatario, revelar, tendencia_credito,
                                        empleado)
    VALUES ('1010_V9', '10109', 4011,
            'POSICION DECIMAL DEL PORCENTAJE DE PARTICIPACION', 'FINAL',
            'CUATRO', 100, 0, 'N', 'N', 'N', 'S', 'N')
    MATCHING (codformato, codigo, renglon);
    UPDATE OR INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre,
                                        saldo, valor, porcentaje, tope, forzoso,
                                        mandatario, revelar, tendencia_credito,
                                        empleado)
    VALUES ('1010_V9', '10109', 4012,
            'POSICION DECIMAL DEL PORCENTAJE DE PARTICIPACION', 'FINAL',
            'CUATRO', 100, 0, 'N', 'N', 'N', 'S', 'N')
    MATCHING (codformato, codigo, renglon);

  END
END;
COMMIT WORK;

EXECUTE BLOCK
AS
DECLARE VARIABLE v_mm INTEGER;
BEGIN
  v_mm = 0;
  SELECT COUNT(1)
  FROM mm_conceptos
  WHERE codigo = '5086' AND
        codformato = '1001_V10'
  INTO v_mm;
  IF (:v_mm = 0) THEN
  BEGIN
    INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre, saldo, valor,
                              porcentaje, tope, forzoso, mandatario, revelar,
                              tendencia_credito, empleado)
    VALUES ('1001_V10', '5086', '3913',
            'VALOR RETEFUENTE TRASLADADA A TERCEROS POR PARTICIPACIONES O DIVIDENDOS RECIBIDOS DE SOC. NACIONALES',
            'FINAL', 'UNO', '100', '100000', 'N', 'N', 'N', 'N', 'N');
    INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre, saldo, valor,
                              porcentaje, tope, forzoso, mandatario, revelar,
                              tendencia_credito, empleado)
    VALUES ('1001_V10', '5086', '3914',
            'VALOR RETEFUENTE TRASLADADA A TERCEROS POR PARTICIPACIONES O DIVIDENDOS RECIBIDOS DE SOC. NACIONALES',
            'FINAL', 'UNO', '100', '100000', 'N', 'N', 'N', 'N', 'N');
  END
END;

EXECUTE BLOCK
AS
DECLARE VARIABLE v_mm INTEGER;
BEGIN
  v_mm = 0;
  SELECT COUNT(1)
  FROM mm_conceptos
  WHERE codigo = '5087' AND
        codformato = '1001_V10'
  INTO v_mm;
  IF (:v_mm = 0) THEN
  BEGIN
    INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre, saldo, valor,
                              porcentaje, tope, forzoso, mandatario, revelar,
                              tendencia_credito, empleado)
    VALUES ('1001_V10', '5087', '3915',
            'VALOR PUNTOS PREMIO REDIMIDOS EN EL PERIODO QUE AFECTAN EL GASTO,PROCED. DE PROG. DE FIDELIZACION',
            'FINAL', 'UNO', '100', '100000', 'N', 'N', 'N', 'N', 'N');
    INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre, saldo, valor,
                              porcentaje, tope, forzoso, mandatario, revelar,
                              tendencia_credito, empleado)
    VALUES ('1001_V10', '5087', '3916',
            'VALOR PUNTOS PREMIO REDIMIDOS EN EL PERIODO QUE AFECTAN EL GASTO,PROCED. DE PROG. DE FIDELIZACION',
            'FINAL', 'UNO', '100', '100000', 'N', 'N', 'N', 'N', 'N');
  END
END;

EXECUTE BLOCK
AS
DECLARE VARIABLE v_mm INTEGER;
BEGIN
  v_mm = 0;
  SELECT COUNT(1)
  FROM mm_conceptos
  WHERE codigo = '5088' AND
        codformato = '1001_V10'
  INTO v_mm;
  IF (:v_mm = 0) THEN
  BEGIN
    INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre, saldo, valor,
                              porcentaje, tope, forzoso, mandatario, revelar,
                              tendencia_credito, empleado)
    VALUES ('1001_V10', '5088', '3917',
            'COSTOS O GASTOS POR DIFERENCIA EN CAMBIO.  SE DEBE REPORTAR CON EL NIT DEL INFORMANTE',
            'FINAL', 'UNO', '100', '100000', 'S', 'N', 'N', 'N', 'N');
    INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre, saldo, valor,
                              porcentaje, tope, forzoso, mandatario, revelar,
                              tendencia_credito, empleado)
    VALUES ('1001_V10', '5088', '3918',
            'COSTOS O GASTOS POR DIFERENCIA EN CAMBIO.  SE DEBE REPORTAR CON EL NIT DEL INFORMANTE',
            'FINAL', 'UNO', '100', '100000', 'S', 'N', 'N', 'N', 'N');
  END
END;

EXECUTE BLOCK
AS
DECLARE VARIABLE v_mm INTEGER;
BEGIN
  v_mm = 0;
  SELECT COUNT(1)
  FROM mm_conceptos
  WHERE codigo = '5089' AND
        codformato = '1001_V10'
  INTO v_mm;
  IF (:v_mm = 0) THEN
  BEGIN
    INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre, saldo, valor,
                              porcentaje, tope, forzoso, mandatario, revelar,
                              tendencia_credito, empleado)
    VALUES ('1001_V10', '5089', '4012',
            'VALOR COMPRA DE ACCIONES DE SOCIEDADES QUE NO COTIZAN EN BOLSA',
            'FINAL', 'UNO', '100', '141195', 'N', 'N', 'N', 'N', 'N');
    INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre, saldo, valor,
                              porcentaje, tope, forzoso, mandatario, revelar,
                              tendencia_credito, empleado)
    VALUES ('1001_V10', '5089', '4013',
            'VALOR COMPRA DE ACCIONES DE SOCIEDADES QUE NO COTIZAN EN BOLSA',
            'FINAL', 'UNO', '100', '141195', 'N', 'N', 'N', 'N', 'N');
  END
END;

EXECUTE BLOCK
AS
DECLARE VARIABLE v_mm INTEGER;
BEGIN
  v_mm = 0;
  SELECT COUNT(1)
  FROM mm_conceptos
  WHERE codigo = '5090' AND
        codformato = '1001_V10'
  INTO v_mm;
  IF (:v_mm = 0) THEN
  BEGIN
    INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre, saldo, valor,
                              porcentaje, tope, forzoso, mandatario, revelar,
                              tendencia_credito, empleado)
    VALUES ('1001_V10', '5090', '4014',
            'VALOR DONACION DE ACCIONES DE SOCIEDADES QUE NO COTIZAN EN BOLSA',
            'FINAL', 'UNO', '100', '141195', 'N', 'N', 'N', 'N', 'N');
    INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre, saldo, valor,
                              porcentaje, tope, forzoso, mandatario, revelar,
                              tendencia_credito, empleado)
    VALUES ('1001_V10', '5090', '4015',
            'VALOR DONACION DE ACCIONES DE SOCIEDADES QUE NO COTIZAN EN BOLSA',
            'FINAL', 'UNO', '100', '141195', 'N', 'N', 'N', 'N', 'N');
  END
END;

EXECUTE BLOCK
AS
DECLARE VARIABLE v_mm INTEGER;
BEGIN
  v_mm = 0;
  SELECT COUNT(1)
  FROM mm_conceptos
  WHERE codigo = '5091' AND
        codformato = '1001_V10'
  INTO v_mm;
  IF (:v_mm = 0) THEN
  BEGIN
    INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre, saldo, valor,
                              porcentaje, tope, forzoso, mandatario, revelar,
                              tendencia_credito, empleado)
    VALUES ('1001_V10', '5091', '4016',
            'VALOR CESION DE ACCIONES DE SOCIEDADES QUE NO COTIZAN EN BOLSA',
            'FINAL', 'UNO', '100', '141195', 'N', 'N', 'N', 'N', 'N');
    INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre, saldo, valor,
                              porcentaje, tope, forzoso, mandatario, revelar,
                              tendencia_credito, empleado)
    VALUES ('1001_V10', '5091', '4017',
            'VALOR CESION DE ACCIONES DE SOCIEDADES QUE NO COTIZAN EN BOLSA',
            'FINAL', 'UNO', '100', '141195', 'N', 'N', 'N', 'N', 'N');
  END
END;

EXECUTE BLOCK
AS
DECLARE VARIABLE v_mm INTEGER;
BEGIN
  v_mm = 0;
  SELECT COUNT(1)
  FROM mm_conceptos
  WHERE codigo = '5092' AND
        codformato = '1001_V10'
  INTO v_mm;
  IF (:v_mm = 0) THEN
  BEGIN
    INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre, saldo, valor,
                              porcentaje, tope, forzoso, mandatario, revelar,
                              tendencia_credito, empleado)
    VALUES ('1001_V10', '5092', '4018',
            'VALOR DEL PAGO QUE CONSTITUYE INGRESO EN ESPECIE PARA EL BENEFICIARIO - ART 29-1 E.T',
            'FINAL', 'UNO', '100', '141195', 'N', 'N', 'N', 'N', 'N');
    INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre, saldo, valor,
                              porcentaje, tope, forzoso, mandatario, revelar,
                              tendencia_credito, empleado)
    VALUES ('1001_V10', '5092', '4019',
            'VALOR DEL PAGO QUE CONSTITUYE INGRESO EN ESPECIE PARA EL BENEFICIARIO - ART 29-1 E.T',
            'FINAL', 'UNO', '100', '141195', 'N', 'N', 'N', 'N', 'N');
  END
END;

EXECUTE BLOCK
AS
DECLARE VARIABLE v_mm INTEGER;
BEGIN
  v_mm = 0;
  SELECT COUNT(1)
  FROM mm_conceptos
  WHERE codigo = '5093' AND
        codformato = '1001_V10'
  INTO v_mm;
  IF (:v_mm = 0) THEN
  BEGIN
    INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre, saldo, valor,
                              porcentaje, tope, forzoso, mandatario, revelar,
                              tendencia_credito, empleado)
    VALUES ('1001_V10', '5093', '4020',
            'DONACIONES DE BEBIDAS ULTRAPROCESADAS Y AZUCARADAS A LOS BANCOS DE ALIMENTOS',
            'FINAL', 'UNO', '100', '141195', 'N', 'N', 'N', 'N', 'N');
    INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre, saldo, valor,
                              porcentaje, tope, forzoso, mandatario, revelar,
                              tendencia_credito, empleado)
    VALUES ('1001_V10', '5093', '4021',
            'DONACIONES DE BEBIDAS ULTRAPROCESADAS Y AZUCARADAS A LOS BANCOS DE ALIMENTOS',
            'FINAL', 'UNO', '100', '141195', 'N', 'N', 'N', 'N', 'N');
  END
END;

EXECUTE BLOCK
AS
DECLARE VARIABLE v_mm INTEGER;
BEGIN
  v_mm = 0;
  SELECT COUNT(1)
  FROM mm_conceptos
  WHERE codigo = '5094' AND
        codformato = '1001_V10'
  INTO v_mm;
  IF (:v_mm = 0) THEN
  BEGIN
    INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre, saldo, valor,
                              porcentaje, tope, forzoso, mandatario, revelar,
                              tendencia_credito, empleado)
    VALUES ('1001_V10', '5094', '4022',
            'DONACIONES DE PRODUCTOS COMESTIBLES ULTRAPROCESADOS INDUSTRIALMENTE A LOS BANCOS DE ALIMENTOS',
            'FINAL', 'UNO', '100', '141195', 'N', 'N', 'N', 'N', 'N');
    INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre, saldo, valor,
                              porcentaje, tope, forzoso, mandatario, revelar,
                              tendencia_credito, empleado)
    VALUES ('1001_V10', '5094', '4023',
            'DONACIONES DE PRODUCTOS COMESTIBLES ULTRAPROCESADOS INDUSTRIALMENTE A LOS BANCOS DE ALIMENTOS',
            'FINAL', 'UNO', '100', '141195', 'N', 'N', 'N', 'N', 'N');
  END
END;

EXECUTE BLOCK
AS
DECLARE VARIABLE v_mm INTEGER;
BEGIN
  v_mm = 0;
  SELECT COUNT(1)
  FROM mm_conceptos
  WHERE codigo = '5095' AND
        codformato = '1001_V10'
  INTO v_mm;
  IF (:v_mm = 0) THEN
  BEGIN
    INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre, saldo, valor,
                              porcentaje, tope, forzoso, mandatario, revelar,
                              tendencia_credito, empleado)
    VALUES ('1001_V10', '5095', '4024',
            'REGALIAS PAGADAS A LA NACION U OTROS ENTES TERRITORIALES POR LA EXPLOTACION DE HIDROCARBUROS',
            'FINAL', 'UNO', '100', '141195', 'N', 'N', 'N', 'N', 'N');
    INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre, saldo, valor,
                              porcentaje, tope, forzoso, mandatario, revelar,
                              tendencia_credito, empleado)
    VALUES ('1001_V10', '5095', '4025',
            'REGALIAS PAGADAS A LA NACION U OTROS ENTES TERRITORIALES POR LA EXPLOTACION DE HIDROCARBUROS',
            'FINAL', 'UNO', '100', '141195', 'N', 'N', 'N', 'N', 'N');
  END
END;

EXECUTE BLOCK
AS
DECLARE VARIABLE v_mm INTEGER;
BEGIN
  v_mm = 0;
  SELECT COUNT(1)
  FROM mm_conceptos
  WHERE codigo = '5096' AND
        codformato = '1001_V10'
  INTO v_mm;
  IF (:v_mm = 0) THEN
  BEGIN
    INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre, saldo, valor,
                              porcentaje, tope, forzoso, mandatario, revelar,
                              tendencia_credito, empleado)
    VALUES ('1001_V10', '5096', '4026',
            'REGALIAS PAGADAS A LA NACION U OTROS ENTES TERRITORIALES POR LA EXPLOTACION DE GAS',
            'FINAL', 'UNO', '100', '141195', 'N', 'N', 'N', 'N', 'N');
    INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre, saldo, valor,
                              porcentaje, tope, forzoso, mandatario, revelar,
                              tendencia_credito, empleado)
    VALUES ('1001_V10', '5096', '4027',
            'REGALIAS PAGADAS A LA NACION U OTROS ENTES TERRITORIALES POR LA EXPLOTACION DE GAS',
            'FINAL', 'UNO', '100', '141195', 'N', 'N', 'N', 'N', 'N');
  END
END;

EXECUTE BLOCK
AS
DECLARE VARIABLE v_mm INTEGER;
BEGIN
  v_mm = 0;
  SELECT COUNT(1)
  FROM mm_conceptos
  WHERE codigo = '5097' AND
        codformato = '1001_V10'
  INTO v_mm;
  IF (:v_mm = 0) THEN
  BEGIN
    INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre, saldo, valor,
                              porcentaje, tope, forzoso, mandatario, revelar,
                              tendencia_credito, empleado)
    VALUES ('1001_V10', '5097', '4028',
            'REGALIAS PAGADAS A LA NACION U OTROS ENTES TERRITORIALES POR LA EXPLOTACION DE CARBON',
            'FINAL', 'UNO', '100', '141195', 'N', 'N', 'N', 'N', 'N');
    INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre, saldo, valor,
                              porcentaje, tope, forzoso, mandatario, revelar,
                              tendencia_credito, empleado)
    VALUES ('1001_V10', '5097', '4029',
            'REGALIAS PAGADAS A LA NACION U OTROS ENTES TERRITORIALES POR LA EXPLOTACION DE CARBON',
            'FINAL', 'UNO', '100', '141195', 'N', 'N', 'N', 'N', 'N');
  END
END;

EXECUTE BLOCK
AS
DECLARE VARIABLE v_mm INTEGER;
BEGIN
  v_mm = 0;
  SELECT COUNT(1)
  FROM mm_conceptos
  WHERE codigo = '5098' AND
        codformato = '1001_V10'
  INTO v_mm;
  IF (:v_mm = 0) THEN
  BEGIN
    INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre, saldo, valor,
                              porcentaje, tope, forzoso, mandatario, revelar,
                              tendencia_credito, empleado)
    VALUES ('1001_V10', '5098', '4030',
            'REGALIAS PAGADAS A LA NACION U OTROS ENTES TERRITORIALES POR LA EXPLOTACION DE OTROS MINERALES',
            'FINAL', 'UNO', '100', '141195', 'N', 'N', 'N', 'N', 'N');
    INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre, saldo, valor,
                              porcentaje, tope, forzoso, mandatario, revelar,
                              tendencia_credito, empleado)
    VALUES ('1001_V10', '5098', '4031',
            'REGALIAS PAGADAS A LA NACION U OTROS ENTES TERRITORIALES POR LA EXPLOTACION DE OTROS MINERALES',
            'FINAL', 'UNO', '100', '141195', 'N', 'N', 'N', 'N', 'N');
  END
END;

EXECUTE BLOCK
AS
DECLARE VARIABLE v_mm INTEGER;
BEGIN
  v_mm = 0;
  SELECT COUNT(1)
  FROM mm_conceptos
  WHERE codigo = '5099' AND
        codformato = '1001_V10'
  INTO v_mm;
  IF (:v_mm = 0) THEN
  BEGIN
    INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre, saldo, valor,
                              porcentaje, tope, forzoso, mandatario, revelar,
                              tendencia_credito, empleado)
    VALUES ('1001_V10', '5099', '4032',
            'REGALIAS PAGADAS A LA NACION U OTROS ENTES TERRITORIALES POR LA EXP. DE SAL Y MAT. DE CONSTRUCCION',
            'FINAL', 'UNO', '100', '141195', 'N', 'N', 'N', 'N', 'N');
    INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre, saldo, valor,
                              porcentaje, tope, forzoso, mandatario, revelar,
                              tendencia_credito, empleado)
    VALUES ('1001_V10', '5099', '4033',
            'REGALIAS PAGADAS A LA NACION U OTROS ENTES TERRITORIALES POR LA EXP. DE SAL Y MAT. DE CONSTRUCCION',
            'FINAL', 'UNO', '100', '141195', 'N', 'N', 'N', 'N', 'N');

  END
END;

EXECUTE BLOCK
AS
DECLARE VARIABLE v_mm INTEGER;
BEGIN
  v_mm = 0;
  SELECT COUNT(1)
  FROM mm_conceptos
  WHERE codigo = '4018' AND
        codformato = '1007_V9'
  INTO v_mm;
  IF (:v_mm = 0) THEN
  BEGIN
    INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre, saldo, valor,
                              porcentaje, tope, forzoso, mandatario, revelar,
                              tendencia_credito, empleado)
    VALUES ('1007_V9', '4018', '3967',
            'INGRESOS POR DIFERENCIA EN CAMBIO. SE DEBE REPORTAR CON EL NIT DEL INFORMANTE',
            'FINAL', 'UNO', '100', '0', 'S', 'N', 'N', 'S', 'N');
    INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre, saldo, valor,
                              porcentaje, tope, forzoso, mandatario, revelar,
                              tendencia_credito, empleado)
    VALUES ('1007_V9', '4018', '3968',
            'INGRESOS POR DIFERENCIA EN CAMBIO. SE DEBE REPORTAR CON EL NIT DEL INFORMANTE',
            'FINAL', 'UNO', '100', '0', 'S', 'N', 'N', 'S', 'N');
  END
END;

EXECUTE BLOCK
AS
DECLARE VARIABLE v_mm INTEGER;
BEGIN
  v_mm = 0;
  SELECT COUNT(1)
  FROM mm_conceptos
  WHERE codigo = '4019' AND
        codformato = '1007_V9'
  INTO v_mm;
  IF (:v_mm = 0) THEN
  BEGIN
    INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre, saldo, valor,
                              porcentaje, tope, forzoso, mandatario, revelar,
                              tendencia_credito, empleado)
    VALUES ('1007_V9', '4019', '4034',
            'INGRESOS BRUTOS CONSTITUTIVOS DE GANANCIA OCASIONAL', 'FINAL',
            'UNO', '100', '0', 'N', 'N', 'N', 'S', 'N');
    INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre, saldo, valor,
                              porcentaje, tope, forzoso, mandatario, revelar,
                              tendencia_credito, empleado)
    VALUES ('1007_V9', '4019', '4035',
            'INGRESOS BRUTOS CONSTITUTIVOS DE GANANCIA OCASIONAL', 'FINAL',
            'UNO', '100', '0', 'N', 'N', 'N', 'S', 'N');
  END
END;

EXECUTE BLOCK
AS
DECLARE VARIABLE v_mm INTEGER;
BEGIN
  v_mm = 0;
  SELECT COUNT(1)
  FROM mm_conceptos
  WHERE codigo = '4020' AND
        codformato = '1007_V9'
  INTO v_mm;
  IF (:v_mm = 0) THEN
  BEGIN
    INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre, saldo, valor,
                              porcentaje, tope, forzoso, mandatario, revelar,
                              tendencia_credito, empleado)
    VALUES ('1007_V9', '4020', '4036',
            'INGRESOS BRUTOS POR VENTA DE ACCIONES DE SOCIEDADES O COMPANIAS QUE NO COTIZAN EN BOLSA',
            'FINAL', 'UNO', '100', '0', 'N', 'N', 'N', 'S', 'N');
    INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre, saldo, valor,
                              porcentaje, tope, forzoso, mandatario, revelar,
                              tendencia_credito, empleado)
    VALUES ('1007_V9', '4020', '4037',
            'INGRESOS BRUTOS POR VENTA DE ACCIONES DE SOCIEDADES O COMPANIAS QUE NO COTIZAN EN BOLSA',
            'FINAL', 'UNO', '100', '0', 'N', 'N', 'N', 'S', 'N');
  END
END;

EXECUTE BLOCK
AS
DECLARE VARIABLE v_mm INTEGER;
BEGIN
  v_mm = 0;
  SELECT COUNT(1)
  FROM mm_conceptos
  WHERE codigo = '4021' AND
        codformato = '1007_V9'
  INTO v_mm;
  IF (:v_mm = 0) THEN
  BEGIN
    INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre, saldo, valor,
                              porcentaje, tope, forzoso, mandatario, revelar,
                              tendencia_credito, empleado)
    VALUES ('1007_V9', '4021', '4038',
            'INGRESOS BRUTOS POR VENTA DE ACCIONES DE SOCIEDADES O COMPANIAS QUE NO COTIZAN EN BOLSA -G.OCASIONAL',
            'FINAL', 'UNO', '100', '0', 'N', 'N', 'N', 'S', 'N');
    INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre, saldo, valor,
                              porcentaje, tope, forzoso, mandatario, revelar,
                              tendencia_credito, empleado)
    VALUES ('1007_V9', '4021', '4039',
            'INGRESOS BRUTOS POR VENTA DE ACCIONES DE SOCIEDADES O COMPANIAS QUE NO COTIZAN EN BOLSA -G.OCASIONAL',
            'FINAL', 'UNO', '100', '0', 'N', 'N', 'N', 'S', 'N');
  END
END;

EXECUTE BLOCK
AS
DECLARE VARIABLE v_mm INTEGER;
BEGIN
  v_mm = 0;
  SELECT COUNT(1)
  FROM mm_conceptos
  WHERE codigo = '2216' AND
        codformato = '1009_V7'
  INTO v_mm;
  IF (:v_mm = 0) THEN
  BEGIN
    INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre, saldo, valor,
                              porcentaje, tope, forzoso, mandatario, revelar,
                              tendencia_credito, empleado)
    VALUES ('1009_V7', '2216', '4040',
            'SALDO DE LOS PASIVOS DE ENTIDADES FINANCIERAS DONDE NO SEA POSIBLE IDENTIFICAR AL ACREEDOR NACIONAL',
            'FINAL', 'UNO', '100', '564780', 'S', 'N', 'N', 'S', 'N');
    INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre, saldo, valor,
                              porcentaje, tope, forzoso, mandatario, revelar,
                              tendencia_credito, empleado)
    VALUES ('1009_V7', '2216', '4041',
            'SALDO DE LOS PASIVOS DE ENTIDADES FINANCIERAS DONDE NO SEA POSIBLE IDENTIFICAR AL ACREEDOR NACIONAL',
            'FINAL', 'UNO', '100', '564780', 'S', 'N', 'N', 'S', 'N');
  END
END;

EXECUTE BLOCK
AS
DECLARE VARIABLE v_mm INTEGER;
BEGIN
  v_mm = 0;
  SELECT COUNT(1)
  FROM mm_conceptos
  WHERE codigo = '2217' AND
        codformato = '1009_V7'
  INTO v_mm;
  IF (:v_mm = 0) THEN
  BEGIN
    INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre, saldo, valor,
                              porcentaje, tope, forzoso, mandatario, revelar,
                              tendencia_credito, empleado)
    VALUES ('1009_V7', '2217', '4042',
            'SALDO DE LOS PASIVOS DE ENT. FINANCIERAS DONDE NO SEA POSIBLE IDENTIFICAR AL ACREEDOR DEL EXTERIOR',
            'FINAL', 'UNO', '100', '564780', 'S', 'N', 'N', 'S', 'N');
    INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre, saldo, valor,
                              porcentaje, tope, forzoso, mandatario, revelar,
                              tendencia_credito, empleado)
    VALUES ('1009_V7', '2217', '4043',
            'SALDO DE LOS PASIVOS DE ENT. FINANCIERAS DONDE NO SEA POSIBLE IDENTIFICAR AL ACREEDOR DEL EXTERIOR',
            'FINAL', 'UNO', '100', '564780', 'S', 'N', 'N', 'S', 'N');
  END
END;

EXECUTE BLOCK
AS
DECLARE VARIABLE v_mm INTEGER;
BEGIN
  v_mm = 0;
  SELECT COUNT(1)
  FROM mm_conceptos
  WHERE codigo = '1105' AND
        codformato = '1011_V6'
  INTO v_mm;
  IF (:v_mm = 0) THEN
  BEGIN

    INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre, saldo, valor,
                              porcentaje, tope, forzoso, mandatario, revelar,
                              tendencia_credito, empleado)
    VALUES ('1011_V6', '1105', '4044', 'SALDO AL 31 DE DICIEMBRE EN CAJA',
            'FINAL', 'UNO', '100', '0', 'S', 'N', 'N', 'N', 'N');
    INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre, saldo, valor,
                              porcentaje, tope, forzoso, mandatario, revelar,
                              tendencia_credito, empleado)
    VALUES ('1011_V6', '1105', '4045', 'SALDO AL 31 DE DICIEMBRE EN CAJA',
            'FINAL', 'UNO', '100', '0', 'S', 'N', 'N', 'N', 'N');
  END
END;

EXECUTE BLOCK
AS
DECLARE VARIABLE v_mm INTEGER;
BEGIN
  v_mm = 0;
  SELECT COUNT(1)
  FROM mm_conceptos
  WHERE codigo = '1502' AND
        codformato = '1011_V6'
  INTO v_mm;
  IF (:v_mm = 0) THEN
  BEGIN
    INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre, saldo, valor,
                              porcentaje, tope, forzoso, mandatario, revelar,
                              tendencia_credito, empleado)
    VALUES ('1011_V6', '1502', '4046',
            'VALOR PATRIMONIAL DE VEHICULOS, MAQUINARIA Y EQUIPO', 'FINAL',
            'UNO', '100', '0', 'S', 'N', 'N', 'N', 'N');
    INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre, saldo, valor,
                              porcentaje, tope, forzoso, mandatario, revelar,
                              tendencia_credito, empleado)
    VALUES ('1011_V6', '1502', '4047',
            'VALOR PATRIMONIAL DE VEHICULOS, MAQUINARIA Y EQUIPO', 'FINAL',
            'UNO', '100', '0', 'S', 'N', 'N', 'N', 'N');
  END
END;

EXECUTE BLOCK
AS
DECLARE VARIABLE v_mm INTEGER;
BEGIN
  v_mm = 0;
  SELECT COUNT(1)
  FROM mm_conceptos
  WHERE codigo = '1512' AND
        codformato = '1011_V6'
  INTO v_mm;
  IF (:v_mm = 0) THEN
  BEGIN
    INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre, saldo, valor,
                              porcentaje, tope, forzoso, mandatario, revelar,
                              tendencia_credito, empleado)
    VALUES ('1011_V6', '1512', '4048',
            'VALOR PATRIMONIAL DE ACTIVOS FIJOS INTANGIBLES', 'FINAL', 'UNO',
            '100', '0', 'S', 'N', 'N', 'N', 'N');
    INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre, saldo, valor,
                              porcentaje, tope, forzoso, mandatario, revelar,
                              tendencia_credito, empleado)
    VALUES ('1011_V6', '1512', '4049',
            'VALOR PATRIMONIAL DE ACTIVOS FIJOS INTANGIBLES', 'FINAL', 'UNO',
            '100', '0', 'S', 'N', 'N', 'N', 'N');
  END
END;

EXECUTE BLOCK
AS
DECLARE VARIABLE v_mm INTEGER;
BEGIN
  v_mm = 0;
  SELECT COUNT(1)
  FROM mm_conceptos
  WHERE codigo = '1513' AND
        codformato = '1011_V6'
  INTO v_mm;
  IF (:v_mm = 0) THEN
  BEGIN
    INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre, saldo, valor,
                              porcentaje, tope, forzoso, mandatario, revelar,
                              tendencia_credito, empleado)
    VALUES ('1011_V6', '1513', '4050',
            'VALOR PATRIMONIAL DE ACTIVOS FIJOS AGOTABLES', 'FINAL', 'UNO',
            '100', '0', 'S', 'N', 'N', 'N', 'N');
    INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre, saldo, valor,
                              porcentaje, tope, forzoso, mandatario, revelar,
                              tendencia_credito, empleado)
    VALUES ('1011_V6', '1513', '4051',
            'VALOR PATRIMONIAL DE ACTIVOS FIJOS AGOTABLES', 'FINAL', 'UNO',
            '100', '0', 'S', 'N', 'N', 'N', 'N');
  END
END;

EXECUTE BLOCK
AS
DECLARE VARIABLE v_mm INTEGER;
BEGIN
  v_mm = 0;
  SELECT COUNT(1)
  FROM mm_conceptos
  WHERE codigo = '1519' AND
        codformato = '1011_V6'
  INTO v_mm;
  IF (:v_mm = 0) THEN
  BEGIN
    INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre, saldo, valor,
                              porcentaje, tope, forzoso, mandatario, revelar,
                              tendencia_credito, empleado)
    VALUES ('1011_V6', '1519', '4052',
            'VALOR PATRIMONIAL A 31 DE DICIEMBRE DE ACTIVOS BIOLOGICOS PLANTAS',
            'FINAL', 'UNO', '100', '0', 'S', 'N', 'N', 'N', 'N');
    INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre, saldo, valor,
                              porcentaje, tope, forzoso, mandatario, revelar,
                              tendencia_credito, empleado)
    VALUES ('1011_V6', '1519', '4053',
            'VALOR PATRIMONIAL A 31 DE DICIEMBRE DE ACTIVOS BIOLOGICOS PLANTAS',
            'FINAL', 'UNO', '100', '0', 'S', 'N', 'N', 'N', 'N');
  END
END;

EXECUTE BLOCK
AS
DECLARE VARIABLE v_mm INTEGER;
BEGIN
  v_mm = 0;
  SELECT COUNT(1)
  FROM mm_conceptos
  WHERE codigo = '1520' AND
        codformato = '1011_V6'
  INTO v_mm;
  IF (:v_mm = 0) THEN
  BEGIN
    INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre, saldo, valor,
                              porcentaje, tope, forzoso, mandatario, revelar,
                              tendencia_credito, empleado)
    VALUES ('1011_V6', '1520', '4054',
            'VALOR PATRIMONIAL A 31 DE DICIEMBRE DE ACTIVOS BIOLOGICOS ANIMALES',
            'FINAL', 'UNO', '100', '0', 'S', 'N', 'N', 'N', 'N');
    INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre, saldo, valor,
                              porcentaje, tope, forzoso, mandatario, revelar,
                              tendencia_credito, empleado)
    VALUES ('1011_V6', '1520', '4055',
            'VALOR PATRIMONIAL A 31 DE DICIEMBRE DE ACTIVOS BIOLOGICOS ANIMALES',
            'FINAL', 'UNO', '100', '0', 'S', 'N', 'N', 'N', 'N');
  END
END;

EXECUTE BLOCK
AS
DECLARE VARIABLE v_mm INTEGER;
BEGIN
  v_mm = 0;
  SELECT COUNT(1)
  FROM mm_conceptos
  WHERE codigo = '1521' AND
        codformato = '1011_V6'
  INTO v_mm;
  IF (:v_mm = 0) THEN
  BEGIN
    INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre, saldo, valor,
                              porcentaje, tope, forzoso, mandatario, revelar,
                              tendencia_credito, empleado)
    VALUES ('1011_V6', '1521', '4056',
            'VALOR PATRIMONIAL DE LA CASA O APARTAMENTO DE HABITACION Inciso 2 art 295-3 ET',
            'FINAL', 'UNO', '100', '0', 'S', 'N', 'N', 'N', 'N');
    INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre, saldo, valor,
                              porcentaje, tope, forzoso, mandatario, revelar,
                              tendencia_credito, empleado)
    VALUES ('1011_V6', '1521', '4057',
            'VALOR PATRIMONIAL DE LA CASA O APARTAMENTO DE HABITACION Inciso 2 art 295-3 ET',
            'FINAL', 'UNO', '100', '0', 'S', 'N', 'N', 'N', 'N');
  END
END;

EXECUTE BLOCK
AS
DECLARE VARIABLE v_mm INTEGER;
BEGIN
  v_mm = 0;
  SELECT COUNT(1)
  FROM mm_conceptos
  WHERE codigo = '1522' AND
        codformato = '1011_V6'
  INTO v_mm;
  IF (:v_mm = 0) THEN
  BEGIN
    INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre, saldo, valor,
                              porcentaje, tope, forzoso, mandatario, revelar,
                              tendencia_credito, empleado)
    VALUES ('1011_V6', '1522', '4058',
            'VALOR PATRIMONIAL DE LOTES RURALES Y FINCAS', 'FINAL', 'UNO',
            '100', '0', 'S', 'N', 'N', 'N', 'N');
    INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre, saldo, valor,
                              porcentaje, tope, forzoso, mandatario, revelar,
                              tendencia_credito, empleado)
    VALUES ('1011_V6', '1522', '4059',
            'VALOR PATRIMONIAL DE LOTES RURALES Y FINCAS', 'FINAL', 'UNO',
            '100', '0', 'S', 'N', 'N', 'N', 'N');
  END
END;

EXECUTE BLOCK
AS
DECLARE VARIABLE v_mm INTEGER;
BEGIN
  v_mm = 0;
  SELECT COUNT(1)
  FROM mm_conceptos
  WHERE codigo = '1523' AND
        codformato = '1011_V6'
  INTO v_mm;
  IF (:v_mm = 0) THEN
  BEGIN
    INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre, saldo, valor,
                              porcentaje, tope, forzoso, mandatario, revelar,
                              tendencia_credito, empleado)
    VALUES ('1011_V6', '1523', '4060', 'VALOR PATRIMONIAL DE LOTES URBANOS',
            'FINAL', 'UNO', '100', '0', 'S', 'N', 'N', 'N', 'N');
    INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre, saldo, valor,
                              porcentaje, tope, forzoso, mandatario, revelar,
                              tendencia_credito, empleado)
    VALUES ('1011_V6', '1523', '4061', 'VALOR PATRIMONIAL DE LOTES URBANOS',
            'FINAL', 'UNO', '100', '0', 'S', 'N', 'N', 'N', 'N');
  END
END;

EXECUTE BLOCK
AS
DECLARE VARIABLE v_mm INTEGER;
BEGIN
  v_mm = 0;
  SELECT COUNT(1)
  FROM mm_conceptos
  WHERE codigo = '1524' AND
        codformato = '1011_V6'
  INTO v_mm;
  IF (:v_mm = 0) THEN
  BEGIN
    INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre, saldo, valor,
                              porcentaje, tope, forzoso, mandatario, revelar,
                              tendencia_credito, empleado)
    VALUES ('1011_V6', '1524', '4062',
            'VALOR PATRIMONIAL DE  OTROS BIENES INMUEBLES - NO RELACIONADOS EN EL 1512,1513 Y 1519',
            'FINAL', 'UNO', '100', '0', 'S', 'N', 'N', 'N', 'N');
    INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre, saldo, valor,
                              porcentaje, tope, forzoso, mandatario, revelar,
                              tendencia_credito, empleado)
    VALUES ('1011_V6', '1524', '4063',
            'VALOR PATRIMONIAL DE  OTROS BIENES INMUEBLES - NO RELACIONADOS EN EL 1512,1513 Y 1529',
            'FINAL', 'UNO', '100', '0', 'S', 'N', 'N', 'N', 'N');
  END
END;

EXECUTE BLOCK
AS
DECLARE VARIABLE v_mm INTEGER;
BEGIN
  v_mm = 0;
  SELECT COUNT(1)
  FROM mm_conceptos
  WHERE codigo = '1525' AND
        codformato = '1011_V6'
  INTO v_mm;
  IF (:v_mm = 0) THEN
  BEGIN
    INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre, saldo, valor,
                              porcentaje, tope, forzoso, mandatario, revelar,
                              tendencia_credito, empleado)
    VALUES ('1011_V6', '1525', '4064',
            'VALOR PATRIMONIAL DE  OTROS BIENES MUEBLES ', 'FINAL', 'UNO',
            '100', '0', 'S', 'N', 'N', 'N', 'N');
    INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre, saldo, valor,
                              porcentaje, tope, forzoso, mandatario, revelar,
                              tendencia_credito, empleado)
    VALUES ('1011_V6', '1525', '4065',
            'VALOR PATRIMONIAL DE  OTROS BIENES MUEBLES ', 'FINAL', 'UNO',
            '100', '0', 'S', 'N', 'N', 'N', 'N');
  END
END;

EXECUTE BLOCK
AS
DECLARE VARIABLE v_mm INTEGER;
BEGIN
  v_mm = 0;
  SELECT COUNT(1)
  FROM mm_conceptos
  WHERE codigo = '1526' AND
        codformato = '1011_V6'
  INTO v_mm;
  IF (:v_mm = 0) THEN
  BEGIN
    INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre, saldo, valor,
                              porcentaje, tope, forzoso, mandatario, revelar,
                              tendencia_credito, empleado)
    VALUES ('1011_V6', '1526', '4066',
            'VALOR PATRIMONIAL DE  OTROS ACTIVOS - NO RELACIONADOS EN LOS ART.29y30 NI CONCEPTOS DE ESTE ARTICULO',
            'FINAL', 'UNO', '100', '0', 'S', 'N', 'N', 'N', 'N');
    INSERT INTO mm_conceptos (codformato, codigo, renglon, nombre, saldo, valor,
                              porcentaje, tope, forzoso, mandatario, revelar,
                              tendencia_credito, empleado)
    VALUES ('1011_V6', '1526', '4067',
            'VALOR PATRIMONIAL DE  OTROS ACTIVOS - NO RELACIONADOS EN LOS ART.29y30 NI CONCEPTOS DE ESTE ARTICULO',
            'FINAL', 'UNO', '100', '0', 'S', 'N', 'N', 'N', 'N');
  END
END;

UPDATE mm_conceptos
SET tope = 141195
WHERE codformato = '1001_V10';
UPDATE mm_conceptos
SET tope = 564780
WHERE codformato = '1008_V7';
UPDATE mm_conceptos
SET tope = 564780
WHERE codformato = '1009_V7';

COMMIT WORK;

/************************************************************/
/**** MODULO REST                                     ****/
/************************************************************/


/******************************************************************************/
/***                           Stored procedures                            ***/
/******************************************************************************/



SET TERM ^ ;

CREATE OR ALTER PROCEDURE REST_INSERT_SUCURSALES (
    TERCERO_ VARCHAR(15),
    SUCURSAL_ VARCHAR(15),
    NOMBRE_ CHAR(80),
    DIRECCION_ CHAR(80),
    TELEFONO_ CHAR(15),
    MOVIL_ CHAR(15),
    FAX_ CHAR(15),
    EMAIL_ CHAR(80),
    CODMUNICIPIO_ CHAR(5),
    CODZONA_ CHAR(5))
AS
BEGIN
  EXIT;
END^





CREATE OR ALTER PROCEDURE REST_SELECT_SUCURSALES_TERCERO (
    TERCERO_ VARCHAR(15),
    SUCURSAL_ VARCHAR(25))
RETURNS (
    S_CODIGO VARCHAR(20),
    S_NOMBRE VARCHAR(80),
    S_DIRECCION VARCHAR(80),
    S_TELEFONO VARCHAR(80),
    S_MOVIL VARCHAR(15),
    S_EMAIL VARCHAR(80),
    NOMBRE_MUNICIPIO VARCHAR(80),
    NOMBRE_ZONA VARCHAR(80))
AS
BEGIN
  SUSPEND;
END^






SET TERM ; ^



/******************************************************************************/
/***                           Stored procedures                            ***/
/******************************************************************************/



SET TERM ^ ;

CREATE OR ALTER PROCEDURE REST_INSERT_SUCURSALES (
    TERCERO_ VARCHAR(15),
    SUCURSAL_ VARCHAR(15),
    NOMBRE_ CHAR(80),
    DIRECCION_ CHAR(80),
    TELEFONO_ CHAR(15),
    MOVIL_ CHAR(15),
    FAX_ CHAR(15),
    EMAIL_ CHAR(80),
    CODMUNICIPIO_ CHAR(5),
    CODZONA_ CHAR(5))
AS
BEGIN
  /* Valida que el tercero exista */
  IF ((SELECT Registro
       FROM Rest_Valida_Tercero(:Tercero_)) = 0) THEN
    EXCEPTION Rest_Error '*** EL TERCERO ' || TRIM(Tercero_) || ' NO EXISTE ***';

  /* Valida que lA SUCURSAL NO EXISTA */

  IF (COALESCE(:Sucursal_, '') <> '') THEN
    IF ((SELECT Registro
         FROM Rest_Valida_Sucursal(:Tercero_, :Sucursal_)) > 0) THEN
      EXCEPTION Rest_Error '*** LA SUCURSAL ' || TRIM(Sucursal_) || ' EXISTE  ***';

  INSERT INTO Sucursales (Codigo, Nombre, Direccion, Telefono, Movil, Fax, Email, Codmunicipio, Codtercero,
                                    Codzona)
  VALUES (:Sucursal_, :Nombre_, :Direccion_, :Telefono_, :Movil_, :Fax_, :Email_,
          IIF(TRIM(:Codmunicipio_) = '', 'NA', :Codmunicipio_), :Tercero_, IIF(TRIM(:Codzona_) = '', 'NA', :Codzona_));

END^


CREATE OR ALTER PROCEDURE REST_SELECT_SUCURSALES_TERCERO (
    TERCERO_ VARCHAR(15),
    SUCURSAL_ VARCHAR(25))
RETURNS (
    S_CODIGO VARCHAR(20),
    S_NOMBRE VARCHAR(80),
    S_DIRECCION VARCHAR(80),
    S_TELEFONO VARCHAR(80),
    S_MOVIL VARCHAR(15),
    S_EMAIL VARCHAR(80),
    NOMBRE_MUNICIPIO VARCHAR(80),
    NOMBRE_ZONA VARCHAR(80))
AS
DECLARE VARIABLE V_Sucursal      VARCHAR(15);
DECLARE VARIABLE V_Cod_Municipio VARCHAR(5);
DECLARE VARIABLE V_Codigo_Zona   VARCHAR(5);
BEGIN
  /* Valida que el tercero exista */
  IF ((SELECT Registro
       FROM Rest_Valida_Tercero(:Tercero_)) = 0) THEN
    EXCEPTION Rest_Error '*** EL TERCERO ' || TRIM(Tercero_) || ' NO EXISTE ***';

  /* Valida que la referencia exista */

  IF (COALESCE(:Sucursal_, '') <> '') THEN
    IF ((SELECT Registro
         FROM Rest_Valida_Sucursal(:Tercero_, :Sucursal_)) = 0) THEN
      EXCEPTION Rest_Error '*** LA SUCURSAL ' || TRIM(Sucursal_) || ' NO EXISTE ***';

  /*Buscamos la sucursal en terceros*/

  SELECT Codigo
  FROM Sucursales
  WHERE Codigo = :Sucursal_
  INTO V_Sucursal;

  FOR SELECT Codigo,
             Nombre,
             Direccion,
             Telefono,
             Movil,
             Email,
             Codmunicipio,
             Codzona
      FROM Sucursales
      WHERE /*Codigo = :V_Sucursal
            AND TRIM(Codigo) LIKE :Sucursal_ || '%'   */

            (TRIM(Codigo) LIKE :Sucursal_ || '%')
            AND Codtercero = :Tercero_

      INTO S_Codigo,
           S_Nombre,
           S_Direccion,
           S_Telefono,
           S_Movil,
           S_Email,
           V_Cod_Municipio,
           V_Codigo_Zona

  DO
  BEGIN

    /* selecciona el nombre del municipio*/
    SELECT Nombre
    FROM Municipios
    WHERE Codigo = :V_Cod_Municipio
    INTO Nombre_Municipio;

    /*selecciona nombre de zona*/

    SELECT Nombre
    FROM Zonas
    WHERE Codigo = :V_Codigo_Zona
    INTO Nombre_Zona;

    SUSPEND;
  END
END^



SET TERM ; ^


/******************************************************************************/
/***                           Stored procedures                            ***/
/******************************************************************************/



SET TERM ^ ;

CREATE OR ALTER PROCEDURE REST_SELECT_PRECIOS_TERCERO (
    CODTERCERO_ VARCHAR(15),
    CODREFERENCIA_ VARCHAR(20))
RETURNS (
    CODIGO_REFERENCIA VARCHAR(20),
    NOMBRE_REFERENCIA VARCHAR(80),
    PRECIO NUMERIC(17,4))
AS
BEGIN
  SUSPEND;
END^






SET TERM ; ^



/******************************************************************************/
/***                           Stored procedures                            ***/
/******************************************************************************/



SET TERM ^ ;

CREATE OR ALTER PROCEDURE REST_SELECT_PRECIOS_TERCERO (
    CODTERCERO_ VARCHAR(15),
    CODREFERENCIA_ VARCHAR(20))
RETURNS (
    CODIGO_REFERENCIA VARCHAR(20),
    NOMBRE_REFERENCIA VARCHAR(80),
    PRECIO NUMERIC(17,4))
AS
DECLARE VARIABLE V_Lista VARCHAR(5);
BEGIN

  /* Valida que el tercero exista */
  IF ((SELECT Registro
       FROM Rest_Valida_Tercero(:Codtercero_)) = 0) THEN
    EXCEPTION Rest_Error '*** EL TERCERO ' || TRIM(Codtercero_) || ' NO EXISTE ***';

  /* Valida que la referencia exista */

  IF (COALESCE(:Codreferencia_, '') <> '') THEN
    IF ((SELECT Registro
         FROM Rest_Valida_Referencia(:Codreferencia_)) = 0) THEN
      EXCEPTION Rest_Error '*** LA REFERENCIA ' || TRIM(Codreferencia_) || ' NO EXISTE ***';

  /*Buscamos lista de precios en terceros*/

  SELECT Codlista
  FROM Terceros
  WHERE Codigo = :Codtercero_
  INTO V_Lista;

  FOR SELECT Codreferencia,
             Precio
      FROM Precios
      WHERE Codlista = :V_Lista
            AND Codreferencia LIKE TRIM(COALESCE(:Codreferencia_, '%')) || '%'

      INTO Codigo_Referencia,
           Precio
  DO
  BEGIN
    SELECT Nombre_Referencia
    FROM Fn_Nombre_Referencia(:Codigo_Referencia)
    INTO Nombre_Referencia;

    SUSPEND;
  END

END^



SET TERM ; ^


/******************************************************************************/
/***                           Stored procedures                            ***/
/******************************************************************************/



SET TERM ^ ;

CREATE OR ALTER PROCEDURE REST_SELECT_INVENTARIO_PRECIO (
    FECHA_CORTE_ DATE,
    REFERENCIA_ VARCHAR(20))
RETURNS (
    REFERENCIA VARCHAR(20),
    NOMBRE_REFERENCIA VARCHAR(80),
    SALDO DOUBLE PRECISION,
    PRECIO NUMERIC(17,4))
AS
BEGIN
  SUSPEND;
END^






SET TERM ; ^



/******************************************************************************/
/***                           Stored procedures                            ***/
/******************************************************************************/



SET TERM ^ ;

CREATE OR ALTER PROCEDURE REST_SELECT_INVENTARIO_PRECIO (
    FECHA_CORTE_ DATE,
    REFERENCIA_ VARCHAR(20))
RETURNS (
    REFERENCIA VARCHAR(20),
    NOMBRE_REFERENCIA VARCHAR(80),
    SALDO DOUBLE PRECISION,
    PRECIO NUMERIC(17,4))
AS
BEGIN

  IF (COALESCE(referencia_, '') <> '') THEN
    IF ((SELECT registro
         FROM rest_valida_referencia(:referencia_)) = 0) THEN
      EXCEPTION rest_error '*** LA REFERENCIA ' || TRIM(referencia_) || ' NO EXISTE ***';

  FOR SELECT TRIM(f.referencia),
             r.precio,
             SUM(f.entradas - f.salidas)
      FROM fx_inventario_fecha(:fecha_corte_) f
      JOIN referencias r
            ON (r.codigo = f.referencia)
      WHERE TRIM(f.referencia) LIKE :referencia_ || '%'
      GROUP BY 1, 2
      ORDER BY 1
      INTO referencia,
           precio,
           saldo
  DO
  BEGIN

    SELECT TRIM(nombre_referencia)
    FROM fn_nombre_referencia(:referencia)
    INTO nombre_referencia;


    SUSPEND;
  END
END^



SET TERM ; ^

COMMIT WORK;