/* Scripts Automator Grupo Bit */
UPDATE OR INSERT INTO DESTINOS (CODIGO, NOMBRE, FTP)
                        VALUES ('DIARIO', 'GRUPO BIT ENVIO DIARIO', 'Apps.grupobit.net
LEVAPAN_211547
LVP4Dc7t
LEVAPAN_211547_:A6:M:D
N
HOY
21')
                      MATCHING (CODIGO);
UPDATE OR INSERT INTO DESTINOS (CODIGO, NOMBRE, FTP)
                        VALUES ('MENSUAL', 'GRUPO BIT ENVIO MENSUAL', 'Apps.grupobit.net
LEVAPAN_211547
LVP4Dc7t
LEVAPAN_211547_:A6:M:D
N
ULTIMO_DIA_MES_ANTERIOR
21')
                      MATCHING (CODIGO);
UPDATE OR INSERT INTO DESTINOS (CODIGO, NOMBRE, FTP)
                        VALUES ('COSMOS', 'COSMOS (GRUPO FAMILIA)', 'filetx.grupofamilia.com
dist_distrigolf
*%CorrejuanitoquetealcanzaelCoco1$%
CO-CBIA-DTR-0091_:A:M:D
S
HOY
990')
                      MATCHING (CODIGO);

COMMIT WORK;

UPDATE OR INSERT INTO AUTOMATOR (CODIGO, NOMBRE, MODO, SQL_CONSULTA, ACTIVA, DELIMITADOR, CODDESTINO)
                         VALUES ('006', 'CLIENTES', 'FTP', '--CLIENTES
SELECT DISTINCT TRIM(T.Codigo) Codigo,
                        TRIM(T.Nombre) Nombre,
                        LPAD(EXTRACT(DAY FROM CURRENT_DATE), 2, ''0'') || LPAD(EXTRACT(MONTH FROM CURRENT_DATE), 2, ''0'') || EXTRACT(YEAR FROM CURRENT_DATE) Fecha,
                        TRIM(T.Codigo) Nit,
                        TRIM(COALESCE(T.Direccion, '''')) Direccion,
                        TRIM(COALESCE(T.Telefono, '''')) Telefono,
                        TRIM(COALESCE(T.Gerente, '''')) Representante,
                        TRIM(COALESCE(T.Codmunicipio, '''')) Municipio,
                        TRIM(COALESCE(T.Codtipologia, ''02'')) Tipo_Negocio,
                        '''' Estrato,
                        '''' Barrio,
                        '''' Dane,
                        TRIM(COALESCE(T.Codzona, '''')) Zona
        FROM Fx_Dinamico_Saldos(:primer_dia_mes, :ayer, ''VENTA'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''N'', ''N'', ''N'', ''N'', ''S'', ''N'', ''N'', ''S'', ''N'', ''N'', ''N'') C
        JOIN Terceros T ON (C.Tercero = T.Codigo)
        WHERE TRIM(C.Linea) IN (''14'', ''35'')', 'S', '{', 'DIARIO')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO AUTOMATOR (CODIGO, NOMBRE, MODO, SQL_CONSULTA, ACTIVA, DELIMITADOR, CODDESTINO)
                         VALUES ('101', 'MUNICIPIOS', 'FTP', '--MUNICIPIOS
SELECT DISTINCT TRIM(M.Codigo) Codigo,
                        TRIM(M.Nombre) Nombre
        FROM Fx_Dinamico_Saldos(:primer_dia_mes_anterior, :ultimo_dia_mes_anterior, ''VENTA'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''N'', ''N'', ''N'', ''N'', ''S'', ''N'', ''N'', ''S'', ''N'', ''S'', ''N'') R
        JOIN Terceros T ON (R.Tercero = T.Codigo)
        JOIN Municipios M ON (T.Codmunicipio = M.Codigo)
        WHERE TRIM(R.Linea) IN (''14'', ''35'')', 'S', '{', 'MENSUAL')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO AUTOMATOR (CODIGO, NOMBRE, MODO, SQL_CONSULTA, ACTIVA, DELIMITADOR, CODDESTINO)
                         VALUES ('001', 'MUNICIPIOS', 'FTP', '--MUNICIPIOS
SELECT DISTINCT TRIM(M.Codigo) Codigo,
                        TRIM(M.Nombre) Nombre
        FROM Fx_Dinamico_Saldos(:primer_dia_mes, :ayer, ''VENTA'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''N'', ''N'', ''N'', ''N'', ''S'', ''N'', ''N'', ''S'', ''N'', ''S'', ''N'') R
        JOIN Terceros T ON (R.Tercero = T.Codigo)
        JOIN Municipios M ON (T.Codmunicipio = M.Codigo)
        WHERE TRIM(R.Linea) IN (''14'', ''35'')', 'S', '{', 'DIARIO')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO AUTOMATOR (CODIGO, NOMBRE, MODO, SQL_CONSULTA, ACTIVA, DELIMITADOR, CODDESTINO)
                         VALUES ('002', 'TIPOS DE NEGOCIO', 'FTP', '--TIPOLOGIAS
SELECT DISTINCT *
        FROM (SELECT TRIM(M.Codigo) Codigo,
                     TRIM(M.Nombre) Nombre
              FROM Fx_Dinamico_Saldos(:primer_dia_mes, :ayer, ''VENTA'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''N'', ''N'', ''N'', ''N'', ''S'', ''N'', ''N'', ''S'', ''N'', ''N'', ''N'') R
              JOIN Terceros T ON (R.Tercero = T.Codigo)
              JOIN Tipologias M ON (T.Codtipologia = M.Codigo)
              WHERE TRIM(R.Linea) IN (''14'', ''35'')

              UNION ALL
              SELECT ''02'',
                     ''TIENDAS GRANDES''
              FROM Rdb$Database
              ORDER BY 1)', 'S', '{', 'DIARIO')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO AUTOMATOR (CODIGO, NOMBRE, MODO, SQL_CONSULTA, ACTIVA, DELIMITADOR, CODDESTINO)
                         VALUES ('008', 'VENTAS', 'FTP', '--VENTAS
SELECT Tercero Cliente,
       Vendedor,
       Referencia Producto,
       LPAD(EXTRACT(DAY FROM R.Fecha), 2, ''0'') || LPAD(EXTRACT(MONTH FROM R.Fecha), 2, ''0'') || EXTRACT(YEAR FROM R.Fecha) Fecha,
       IIF(TRIM(R.Prefijo) = ''_'', '''', TRIM(R.Prefijo)) || TRIM(R.Numero) Documento,
       CAST(ABS(R.Cantidad) AS NUMERIC(17,2)) Cantidad,
       CAST(ABS(Bruto) - ABS(Devolucion) - Descuento AS NUMERIC(17,2)) Total,
       IIF(R.Salida > 0, 0, 1) Tipo,
       CAST((SELECT Ponderado
             FROM Fn_Ponderado(R.Referencia, :ayer)) AS NUMERIC(17,2)) Costo,
       TRIM(R1.Codmedida) Medida
FROM Fx_Dinamico_Saldos(:primer_dia_mes, :ayer, ''VENTA'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''N'', ''N'', ''N'', ''S'', ''S'', ''N'', ''N'', ''S'', ''S'', ''N'', ''N'') R
JOIN Referencias R1 ON (R.Referencia = R1.Codigo)
WHERE TRIM(R.Linea) IN (''14'', ''35'')', 'S', '{', 'DIARIO')
                       MATCHING (CODIGO);


COMMIT WORK;

UPDATE OR INSERT INTO AUTOMATOR (CODIGO, NOMBRE, MODO, SQL_CONSULTA, ACTIVA, DELIMITADOR, CODDESTINO)
                         VALUES ('009', 'TOTAL CONTROL', 'FTP', '--CONTROL
SELECT ''TotalValorVenta'' Dato,
       SUM(CAST(ABS(Bruto) - ABS(Devolucion) - Descuento AS NUMERIC(17,2))) Valor
FROM Fx_Dinamico_Saldos(:primer_dia_mes, :ayer, ''VENTA'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''N'', ''N'', ''N'', ''N'', ''S'', ''N'', ''N'', ''N'', ''N'', ''N'', ''N'') R
WHERE TRIM(R.Linea) IN (''14'', ''35'')', 'S', '{', 'DIARIO')
                       MATCHING (CODIGO);


COMMIT WORK;

UPDATE OR INSERT INTO AUTOMATOR (CODIGO, NOMBRE, MODO, SQL_CONSULTA, ACTIVA, DELIMITADOR, CODDESTINO)
                         VALUES ('003', 'SUPERVISORES', 'FTP', '--SUPERVISOR
SELECT DISTINCT TRIM(COALESCE(P.Codigo2, ''00'')) Codigo,
                        (SELECT Nombre_Empleado
                         FROM Fn_Nombre_Empleado(COALESCE(P.Codigo2, ''00''))) Nombre
        FROM Fx_Dinamico_Saldos(:primer_dia_mes, :ayer, ''VENTA'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''N'', ''N'', ''N'', ''N'', ''S'', ''N'', ''N'', ''N'', ''S'', ''N'', ''N'') C
        JOIN Personal P ON (C.Vendedor = P.Codigo)
        WHERE TRIM(C.Linea) IN (''14'', ''35'')', 'S', '{', 'DIARIO')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO AUTOMATOR (CODIGO, NOMBRE, MODO, SQL_CONSULTA, ACTIVA, DELIMITADOR, CODDESTINO)
                         VALUES ('004', 'VENDEDORES', 'FTP', '--VENDEDORES
SELECT DISTINCT TRIM(P.Codigo) Codigo,
                        TRIM(P.Nombre) Nombre,
                        '''' Ubicacion,
                        TRIM(COALESCE(P.Telefono, '''')) Cedula,
                        TRIM(COALESCE(P.Codigo2, ''00'')) Supervisor
        FROM Fx_Dinamico_Saldos(:primer_dia_mes, :ayer, ''VENTA'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''N'', ''N'', ''N'', ''N'', ''S'', ''N'', ''N'', ''N'', ''S'', ''N'', ''N'') C
        JOIN Personal P ON (C.Vendedor = P.Codigo)
        WHERE TRIM(C.Linea) IN (''14'', ''35'')', 'S', '{', 'DIARIO')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO AUTOMATOR (CODIGO, NOMBRE, MODO, SQL_CONSULTA, ACTIVA, DELIMITADOR, CODDESTINO)
                         VALUES ('005', 'PRODUCTOS', 'FTP', '--PRODUCTOS
SELECT DISTINCT TRIM(R1.Codigo) Codigo,
                        TRIM(R1.Nombre) Nombre,
                        CASE R1.Alto
                          WHEN 1 THEN ''RG''
                          WHEN 2 THEN ''OF''
                          WHEN 3 THEN ''OB''
                          ELSE ''RG''
                        END Tipo_Referencia,
                        TRIM(R1.Codmedida) Tipo_Unidad,
                        '''' Codigo_Barras,
                        TRIM(COALESCE(L.Grupo, '''')) Compania
        FROM Fx_Inventario_Fecha(:ayer) I
        JOIN Referencias R1 ON (I.Referencia = R1.Codigo)
        JOIN Fx_Dinamico_Saldos(:primer_dia_mes, :ayer, ''VENTA'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''N'', ''N'', ''N'', ''S'', ''S'', ''N'', ''N'', ''N'', ''N'', ''N'', ''N'') R ON (I.Referencia = R.Referencia)
        JOIN Lineas L ON (R1.Codlinea = L.Codigo)
        WHERE TRIM(R.Linea) IN (''14'', ''35'')', 'S', '{', 'DIARIO')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO AUTOMATOR (CODIGO, NOMBRE, MODO, SQL_CONSULTA, ACTIVA, DELIMITADOR, CODDESTINO)
                         VALUES ('102', 'TIPOS DE NEGOCIO', 'FTP', '--TIPOLOGIAS
SELECT DISTINCT *
        FROM (SELECT TRIM(M.Codigo) Codigo,
                     TRIM(M.Nombre) Nombre
              FROM Fx_Dinamico_Saldos(:primer_dia_mes_anterior, :ultimo_dia_mes_anterior, ''VENTA'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''N'', ''N'', ''N'', ''N'', ''S'', ''N'', ''N'', ''S'', ''N'', ''N'', ''N'') R
              JOIN Terceros T ON (R.Tercero = T.Codigo)
              JOIN Tipologias M ON (T.Codtipologia = M.Codigo)
              WHERE TRIM(R.Linea) IN (''14'', ''35'')
              UNION ALL
              SELECT ''02'',
                     ''TIENDAS GRANDES''
              FROM Rdb$Database
              ORDER BY 1)', 'S', '{', 'MENSUAL')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO AUTOMATOR (CODIGO, NOMBRE, MODO, SQL_CONSULTA, ACTIVA, DELIMITADOR, CODDESTINO)
                         VALUES ('103', 'SUPERVISORES', 'FTP', '--SUPERVISOR
SELECT DISTINCT TRIM(COALESCE(P.Codigo2, ''00'')) Codigo,
                        (SELECT Nombre_Empleado
                         FROM Fn_Nombre_Empleado(COALESCE(P.Codigo2, ''00''))) Nombre
        FROM Fx_Dinamico_Saldos(:primer_dia_mes_anterior, :ultimo_dia_mes_anterior, ''VENTA'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''N'', ''N'', ''N'', ''N'', ''S'', ''N'', ''N'', ''N'', ''S'', ''N'', ''N'') C
        JOIN Personal P ON (C.Vendedor = P.Codigo)
        WHERE TRIM(C.Linea) IN (''14'', ''35'')', 'S', '{', 'MENSUAL')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO AUTOMATOR (CODIGO, NOMBRE, MODO, SQL_CONSULTA, ACTIVA, DELIMITADOR, CODDESTINO)
                         VALUES ('104', 'VENDEDORES', 'FTP', '--VENDEDORES
SELECT DISTINCT TRIM(P.Codigo) Codigo,
                        TRIM(P.Nombre) Nombre,
                        '''' Ubicacion,
                        TRIM(COALESCE(P.Telefono, '''')) Cedula,
                        TRIM(COALESCE(P.Codigo2, ''00'')) Supervisor
        FROM Fx_Dinamico_Saldos(:primer_dia_mes_anterior, :ultimo_dia_mes_anterior, ''VENTA'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''N'', ''N'', ''N'', ''N'', ''S'', ''N'', ''N'', ''N'', ''S'', ''N'', ''N'') C
        JOIN Personal P ON (C.Vendedor = P.Codigo)
        WHERE TRIM(C.Linea) IN (''14'', ''35'')', 'S', '{', 'MENSUAL')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO AUTOMATOR (CODIGO, NOMBRE, MODO, SQL_CONSULTA, ACTIVA, DELIMITADOR, CODDESTINO)
                         VALUES ('105', 'PRODUCTOS', 'FTP', '--PRODUCTOS
 SELECT DISTINCT TRIM(R1.Codigo) Codigo,
                        TRIM(R1.Nombre) Nombre,
                        CASE R1.Alto
                          WHEN 1 THEN ''RG''
                          WHEN 2 THEN ''OF''
                          WHEN 3 THEN ''OB''
                          ELSE ''RG''
                        END Tipo_Referencia,
                        TRIM(R1.Codmedida) Tipo_Unidad,
                        '''' Codigo_Barras,
                        TRIM(COALESCE(L.Grupo, '''')) Compania
        FROM Fx_Inventario_Fecha(:ultimo_dia_mes_anterior) I
        JOIN Referencias R1 ON (I.Referencia = R1.Codigo)
        JOIN Fx_Dinamico_Saldos(:primer_dia_mes_anterior, :ultimo_dia_mes_anterior, ''VENTA'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''N'', ''N'', ''N'', ''S'', ''S'', ''N'', ''N'', ''N'', ''N'', ''N'', ''N'') R ON (I.Referencia = R.Referencia)
        JOIN Lineas L ON (R1.Codlinea = L.Codigo)
        WHERE TRIM(R.Linea) IN (''14'', ''35'')', 'S', '{', 'MENSUAL')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO AUTOMATOR (CODIGO, NOMBRE, MODO, SQL_CONSULTA, ACTIVA, DELIMITADOR, CODDESTINO)
                         VALUES ('106', 'CLIENTES', 'FTP', '--CLIENTES
SELECT DISTINCT TRIM(T.Codigo) Codigo,
                        TRIM(T.Nombre) Nombre,
                        LPAD(EXTRACT(DAY FROM CURRENT_DATE), 2, ''0'') || LPAD(EXTRACT(MONTH FROM CURRENT_DATE), 2, ''0'') || EXTRACT(YEAR FROM CURRENT_DATE) Fecha,
                        TRIM(T.Codigo) Nit,
                        TRIM(COALESCE(T.Direccion, '''')) Direccion,
                        TRIM(COALESCE(T.Telefono, '''')) Telefono,
                        TRIM(COALESCE(T.Gerente, '''')) Representante,
                        TRIM(COALESCE(T.Codmunicipio, '''')) Municipio,
                        TRIM(COALESCE(T.Codtipologia, ''02'')) Tipo_Negocio,
                        '''' Estrato,
                        '''' Barrio,
                        '''' Dane,
                        TRIM(COALESCE(T.Codzona, '''')) Zona
        FROM Fx_Dinamico_Saldos(:primer_dia_mes_anterior, :ultimo_dia_mes_anterior, ''VENTA'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''N'', ''N'', ''N'', ''N'', ''S'', ''N'', ''N'', ''S'', ''N'', ''N'', ''N'') C
        JOIN Terceros T ON (C.Tercero = T.Codigo)
        WHERE TRIM(C.Linea) IN (''14'', ''35'')', 'S', '{', 'MENSUAL')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO AUTOMATOR (CODIGO, NOMBRE, MODO, SQL_CONSULTA, ACTIVA, DELIMITADOR, CODDESTINO)
                         VALUES ('007', 'INVENTARIO', 'FTP', '--INVENTARIO
SELECT DISTINCT LPAD(EXTRACT(DAY FROM CAST(:ayer AS DATE)), 2, ''0'') || LPAD(EXTRACT(MONTH FROM CAST(:ayer AS DATE)), 2, ''0'') || EXTRACT(YEAR FROM CAST(:ayer AS DATE)) Fecha,
                        TRIM(I.Referencia) Codigo,
                        CAST(COALESCE((SELECT SUM(Saldo)
                                       FROM Fx_Inventario_Referencia(I.Referencia)), 0) AS NUMERIC(17,2)) Cantidad,
                        TRIM(R1.Codmedida) Medida
        FROM Fx_Inventario_Fecha(:ayer) I
        JOIN Referencias R1 ON (I.Referencia = R1.Codigo)
        JOIN Fx_Dinamico_Saldos(:primer_dia_mes, :ayer, ''VENTA'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''N'', ''N'', ''N'', ''S'', ''S'', ''N'', ''N'', ''N'', ''N'', ''N'', ''N'') R ON (R1.Codigo = R.Referencia)
        WHERE TRIM(R.Linea) IN (''14'', ''35'')', 'S', '{', 'DIARIO')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO AUTOMATOR (CODIGO, NOMBRE, MODO, SQL_CONSULTA, ACTIVA, DELIMITADOR, CODDESTINO)
                         VALUES ('107', 'INVENTARIO', 'FTP', '--INVENTARIO
SELECT LPAD(EXTRACT(DAY FROM CAST(:ultimo_dia_mes_anterior AS DATE)), 2, ''0'') || LPAD(EXTRACT(MONTH FROM CAST(:ultimo_dia_mes_anterior AS DATE)), 2, ''0'') || EXTRACT(YEAR FROM CAST(:ultimo_dia_mes_anterior AS DATE)) Fecha,
                   TRIM(I.Referencia) Codigo,
                   SUM(I.Entradas - I.Salidas) Cantidad,
                   TRIM(R1.Codmedida) Medida
            FROM Fx_Inventario_Fecha(:ultimo_dia_mes_anterior) I
            JOIN Referencias R1 ON (I.Referencia = R1.Codigo)
            JOIN Fx_Dinamico_Saldos(:primer_dia_mes_anterior, :ultimo_dia_mes_anterior, ''VENTA'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''N'', ''N'', ''N'', ''S'', ''S'', ''N'', ''N'', ''N'', ''N'', ''N'', ''N'') R ON (I.Referencia = R.Referencia)
            WHERE TRIM(R.Linea) IN (''14'', ''35'')
            GROUP BY 1, 2, 4', 'S', '{', 'MENSUAL')
                       MATCHING (CODIGO);

COMMIT WORK;

UPDATE OR INSERT INTO AUTOMATOR (CODIGO, NOMBRE, MODO, SQL_CONSULTA, ACTIVA, DELIMITADOR, CODDESTINO)
                         VALUES ('108', 'VENTAS', 'FTP', '--VENTAS
SELECT Tercero Cliente,
       Vendedor,
       Referencia Producto,
       LPAD(EXTRACT(DAY FROM R.Fecha), 2, ''0'') || LPAD(EXTRACT(MONTH FROM R.Fecha), 2, ''0'') || EXTRACT(YEAR FROM R.Fecha) Fecha,
       IIF(TRIM(R.Prefijo) = ''_'', '''', TRIM(R.Prefijo)) || TRIM(R.Numero) Documento,
       CAST(ABS(R.Cantidad) AS NUMERIC(17,2)) Cantidad,
       CAST(ABS(Bruto) - ABS(Devolucion) - Descuento AS NUMERIC(17,2)) Total,
       IIF(R.Salida > 0, 0, 1) Tipo,
       CAST((SELECT Ponderado
             FROM Fn_Ponderado(R.Referencia, :ultimo_dia_mes_anterior)) AS NUMERIC(17,2)) Costo,
       TRIM(R1.Codmedida) Medida
FROM Fx_Dinamico_Saldos(:primer_dia_mes_anterior, :ultimo_dia_mes_anterior, ''VENTA'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''N'', ''N'', ''N'', ''S'', ''S'', ''N'', ''N'', ''S'', ''S'', ''N'', ''N'') R
JOIN Referencias R1 ON (R.Referencia = R1.Codigo)
WHERE TRIM(R.Linea) IN (''14'', ''35'')', 'S', '{', 'MENSUAL')
                       MATCHING (CODIGO);


COMMIT WORK;


UPDATE OR INSERT INTO AUTOMATOR (CODIGO, NOMBRE, MODO, SQL_CONSULTA, ACTIVA, DELIMITADOR, CODDESTINO)
                         VALUES ('109', 'TOTAL CONTROL', 'FTP', 'SELECT ''TotalValorVenta'' Dato,
                       SUM (CAST(ABS(Bruto) - ABS(Devolucion) - Descuento AS NUMERIC(17,2))) Valor
FROM Fx_Dinamico_Saldos(:primer_dia_mes_anterior, :ultimo_dia_mes_anterior, ''VENTA'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''%'', ''N'', ''N'', ''N'', ''N'', ''S'', ''N'', ''N'', ''N'', ''N'', ''N'', ''N'')
WHERE TRIM(Linea) IN (''14'', ''35'')
GROUP BY 1', 'S', '{', 'MENSUAL')
                       MATCHING (CODIGO);

COMMIT WORK;

/* Scripts Automator Cosmos, se crea SP */
UPDATE OR INSERT INTO DESTINOS (CODIGO, NOMBRE, FTP)
VALUES ('COSMOS', 'COSMOS (GRUPO FAMILIA)', 'filetx.grupofamilia.com
dist_distrigolf
*%CorrejuanitoquetealcanzaelCoco1$%
CO-CBIA-DTR-0091_:A:M:D
S
HOY
990')
MATCHING (CODIGO);

COMMIT WORK;

UPDATE Lineas
SET Grupo = 'FAMILIA'
WHERE Codigo IN ('01', '02', '03', '04', '05', '06', '07', '08', '09', '10');
    
UPDATE OR INSERT INTO AUTOMATOR (CODIGO, NOMBRE, MODO, SQL_CONSULTA, ACTIVA, DELIMITADOR, CODDESTINO)
                         VALUES ('1001', 'CO-CBIA-DTR-0091_MC_:A:M:D', 'FTP', '-- Maestro Clientes

SELECT *
FROM Pz_Autom_Cosmos_Clientes(:ayer-45 ,:ayer, ''FAMILIA'')
--FROM Pz_Autom_Cosmos_Clientes(''01.05.2022'' ,''31.05.2022'')', 'S', '|', 'COSMOS')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO AUTOMATOR (CODIGO, NOMBRE, MODO, SQL_CONSULTA, ACTIVA, DELIMITADOR, CODDESTINO)
                         VALUES ('1002', 'CO-CBIA-DTR-0091_M_:A:M:D', 'FTP', '-- Transaccional Ventas

SELECT *
FROM Pz_Autom_Cosmos_Ventas(:ayer-45 ,:ayer, ''FAMILIA'')
--FROM Pz_Autom_Cosmos_Ventas(''01.05.2022'' ,''31.05.2022'')', 'S', '|', 'COSMOS')
                       MATCHING (CODIGO);
UPDATE OR INSERT INTO AUTOMATOR (CODIGO, NOMBRE, MODO, SQL_CONSULTA, ACTIVA, DELIMITADOR, CODDESTINO)
                         VALUES ('1003', 'CO-CBIA-DTR-0091_I_:A:M:D', 'FTP', '-- Transaccional Inventarios

SELECT *
FROM Pz_Autom_Cosmos_Inventario(:ayer-45 ,:ayer, ''FAMILIA'')
--FROM Pz_Autom_Cosmos_Inventario(''01.05.2022'' ,''31.05.2022'')', 'S', '|', 'COSMOS')
                       MATCHING (CODIGO);

COMMIT WORK;




/******************************************************************************/
/***                           Stored Procedures                            ***/
/******************************************************************************/

SET TERM ^;

CREATE OR ALTER PROCEDURE Pz_Autom_Cosmos_Clientes (
    Desde_ DATE,
    Hasta_ DATE,
    Grupo_Linea_ VARCHAR(20))
RETURNS (
    "Cliente_Cod" VARCHAR(15),
    "Cliente_Nombre" VARCHAR(163),
    "Cliente_Dir" VARCHAR(80),
    "Cliente_Tel" VARCHAR(50),
    "Cliente_FechaCrea" VARCHAR(1),
    "Cliente_FechaDesact" VARCHAR(1),
    "Cliente_Periodicidad" VARCHAR(1),
    "Cliente_MunCod" VARCHAR(5),
    "Cliente_MunDesc" VARCHAR(80),
    "Cliente_DeptoCod" VARCHAR(2),
    "Cliente_DeptoDesc" VARCHAR(80),
    "Cliente_CanalCod" VARCHAR(10),
    "Cliente_CanalDesc" VARCHAR(80),
    "Cliente_SubCanalCod" VARCHAR(1),
    "Cliente_SubCanalDesc" VARCHAR(1),
    "Cliente_GPS_Lat" VARCHAR(1),
    "Cliente_GPS_Long" VARCHAR(1),
    "Cliente_Barrio" VARCHAR(20),
    "Cliente_Contacto_Cedula" VARCHAR(10),
    "Cliente_Contacto_Nombre" VARCHAR(80),
    "Cliente_SectorCod" VARCHAR(1),
    "Cliente_SectorDesc" VARCHAR(1),
    "Cliente_RutaCod" VARCHAR(6),
    "Cliente_RutaDesc" VARCHAR(80),
    "PlanComercial_Cod" VARCHAR(1),
    "PlanComercial_Desc" VARCHAR(1),
    "MaestrasClientes_FechaGeneracio" VARCHAR(19))
AS
BEGIN
  SUSPEND;
END^


CREATE OR ALTER PROCEDURE Pz_Autom_Cosmos_Inventario (
    Desde_ DATE,
    Hasta_ DATE,
    Grupo_Linea_ VARCHAR(20))
RETURNS (
    "Nit_Distribuidor" VARCHAR(15),
    "Inventario_Fecha" VARCHAR(19),
    "Prod_Dist_Cod" VARCHAR(20),
    "Prod_Dist_Desc" VARCHAR(80),
    "Prod_Prov_Cod" VARCHAR(20),
    "Prd_UnidadInventario" VARCHAR(5),
    "Cant_Disponible" DOUBLE PRECISION,
    "Cant_Stock" DOUBLE PRECISION,
    "Costo_Disponible" DOUBLE PRECISION,
    "Costo_Stock" DOUBLE PRECISION,
    "Prov_Cod" VARCHAR(15),
    "Bodega_Cod" VARCHAR(5),
    "Bodega_Desc" VARCHAR(80),
    "Agencia_Cod" VARCHAR(15),
    "Agencia_Desc" VARCHAR(50),
    "Inventario_Estado" VARCHAR(10))
AS
BEGIN
  SUSPEND;
END^

CREATE OR ALTER PROCEDURE Pz_Autom_Cosmos_Ventas (
    Desde_ DATE,
    Hasta_ DATE,
    Grupo_Linea_ VARCHAR(20))
RETURNS (
    "Nit_Distribuidor" VARCHAR(15),
    "Factura_Numero" VARCHAR(15),
    "Factura_Fecha_Movimiento" VARCHAR(19),
    "Prod_Dist_Cod" VARCHAR(20),
    "Prod_Dist_Desc" VARCHAR(80),
    "Prod_Prov_Cod" VARCHAR(20),
    "Cant_Vendida" DOUBLE PRECISION,
    "Prod_UnidadVenta" VARCHAR(5),
    "Precio_Unit" DOUBLE PRECISION,
    "Precio_Total" DOUBLE PRECISION,
    "Costo" DOUBLE PRECISION,
    "Descuento" DOUBLE PRECISION,
    "Transaccion_Codigo" CHAR(2),
    "Transaccion_Descripccion" VARCHAR(10),
    "CausalTransaccion_Codigo" CHAR(1),
    "CausalTransaccion_Descripccion" CHAR(1),
    "Vend_Cedula" VARCHAR(15),
    "Vend_Nombre" VARCHAR(83),
    "Vend_Tel" VARCHAR(15),
    "Vend_Dir" VARCHAR(80),
    "Cliente_Cod" VARCHAR(15),
    "Cliente_Nombre" VARCHAR(220),
    "Cliente_Dir" VARCHAR(80),
    "Cliente_Tel" VARCHAR(50),
    "Cliente_FechaCrea" CHAR(1),
    "Cliente_FechaDesact" CHAR(1),
    "Cliente_Periodicidad" CHAR(1),
    "Cliente_RutaCod" VARCHAR(6),
    "Cliente_RutaDesc" VARCHAR(80),
    "Cliente_MunCod" VARCHAR(6),
    "Cliente_MunDesc" VARCHAR(80),
    "Sup_Cedula" CHAR(1),
    "Sup_Nombre" CHAR(1),
    "Sup_Dir" CHAR(1),
    "Sup_Tel" CHAR(1),
    "Cliente_CanalCod" VARCHAR(10),
    "Cliente_CanalDesc" VARCHAR(80),
    "Cliente_DeptoCod" VARCHAR(2),
    "Cliente_DeptoDesc" VARCHAR(80),
    "Prov_Cod" VARCHAR(15),
    "Prov_Desc" VARCHAR(163),
    "Cliente_SectorCod" CHAR(1),
    "Cliente_SectorDesc" CHAR(1),
    "Factura_Estado" CHAR(1),
    "PlanComercial_Cod" CHAR(1),
    "PlanComercial_Desc" CHAR(1),
    "Cliente_SubCanalCod" CHAR(1),
    "Cliente_SubCanalDesc" CHAR(1),
    "Agencia_Cod" VARCHAR(15),
    "Agencia_Desc" VARCHAR(50),
    "Bodega_Cod" VARCHAR(5),
    "Bodega_Desc" VARCHAR(80),
    "Cliente_GPS_Lat" CHAR(1),
    "Cliente_GPS_Long" CHAR(1),
    "Factura_GPS_Lat" CHAR(1),
    "Factura_GPS_Long" CHAR(1),
    "Cliente_Barrio" VARCHAR(20),
    "Cliente_Contacto_Cedula" CHAR(1),
    "Cliente_Contacto_Nombre" CHAR(1),
    "Ventas_FechaGeneracion" VARCHAR(19))
AS
BEGIN
  SUSPEND;
END^

SET TERM; ^

/******************************************************************************/
/***                           Stored Procedures                            ***/
/******************************************************************************/

SET TERM ^;

CREATE OR ALTER PROCEDURE Pz_Autom_Cosmos_Clientes (
    Desde_ DATE,
    Hasta_ DATE,
    Grupo_Linea_ VARCHAR(20))
RETURNS (
    "Cliente_Cod" VARCHAR(15),
    "Cliente_Nombre" VARCHAR(163),
    "Cliente_Dir" VARCHAR(80),
    "Cliente_Tel" VARCHAR(50),
    "Cliente_FechaCrea" VARCHAR(1),
    "Cliente_FechaDesact" VARCHAR(1),
    "Cliente_Periodicidad" VARCHAR(1),
    "Cliente_MunCod" VARCHAR(5),
    "Cliente_MunDesc" VARCHAR(80),
    "Cliente_DeptoCod" VARCHAR(2),
    "Cliente_DeptoDesc" VARCHAR(80),
    "Cliente_CanalCod" VARCHAR(10),
    "Cliente_CanalDesc" VARCHAR(80),
    "Cliente_SubCanalCod" VARCHAR(1),
    "Cliente_SubCanalDesc" VARCHAR(1),
    "Cliente_GPS_Lat" VARCHAR(1),
    "Cliente_GPS_Long" VARCHAR(1),
    "Cliente_Barrio" VARCHAR(20),
    "Cliente_Contacto_Cedula" VARCHAR(10),
    "Cliente_Contacto_Nombre" VARCHAR(80),
    "Cliente_SectorCod" VARCHAR(1),
    "Cliente_SectorDesc" VARCHAR(1),
    "Cliente_RutaCod" VARCHAR(6),
    "Cliente_RutaDesc" VARCHAR(80),
    "PlanComercial_Cod" VARCHAR(1),
    "PlanComercial_Desc" VARCHAR(1),
    "MaestrasClientes_FechaGeneracio" VARCHAR(19))
AS
DECLARE VARIABLE V_Tercero VARCHAR(15);
DECLARE VARIABLE V_Nombre_Tercero VARCHAR(163);
DECLARE VARIABLE V_Direccion VARCHAR(80);
DECLARE VARIABLE V_Telefono VARCHAR(50);
DECLARE VARIABLE V_Municipio VARCHAR(5);
DECLARE VARIABLE V_Nombre_Municipio VARCHAR(80);
DECLARE VARIABLE V_Departamento VARCHAR(2);
DECLARE VARIABLE V_Nombre_Departamento VARCHAR(80);
DECLARE VARIABLE V_Tipologia VARCHAR(10);
DECLARE VARIABLE V_Nombre_Tipologia VARCHAR(80);
DECLARE VARIABLE V_Barrio VARCHAR(20);
DECLARE VARIABLE V_Contacto VARCHAR(10);
DECLARE VARIABLE V_Nombre_Contacto VARCHAR(80);
DECLARE VARIABLE V_Razon VARCHAR(50);
DECLARE VARIABLE V_Cliente_Rutacod VARCHAR(6);
DECLARE VARIABLE V_Cliente_Rutadesc VARCHAR(80);
BEGIN

  "Cliente_FechaCrea" = '';
  "Cliente_FechaDesact" = '';
  "Cliente_Periodicidad" = '';
  "Cliente_SubCanalCod" = '';
  "Cliente_SubCanalDesc" = '';
  "Cliente_GPS_Lat" = '';
  "Cliente_GPS_Long" = '';
  "Cliente_SectorCod" = '';
  "Cliente_SectorDesc" = '';
  "PlanComercial_Cod" = '';
  "PlanComercial_Desc" = '';
  "MaestrasClientes_FechaGeneracio" = EXTRACT(YEAR FROM CURRENT_DATE) || '-' || LPAD(EXTRACT(MONTH FROM CURRENT_DATE), 2, '0') || '-' || LPAD(EXTRACT(DAY FROM CURRENT_DATE), 2, '0') || ' ' || SUBSTRING(CURRENT_TIME FROM 1 FOR 8);

  FOR SELECT DISTINCT D.Tercero,
                      D.Nombre_Tercero,
                      D.Municipio,
                      D.Nombre_Municipio
      FROM Fx_Dinamico(:Desde_, :Hasta_, 'VENTA', '%', '%', '%', '%', '%', '%', '%', '%', '%', '%', '%', 'S', 'N', 'N', 'S', 'S', 'N', 'N', 'S', 'S', 'S', 'N', 'N', 'N') D
      --WHERE Linea IN ('01', '02', '03', '04', '05', '06', '07', '08', '09', '10')
      WHERE Linea IN (SELECT Codigo
                      FROM Lineas
                      WHERE Grupo = :Grupo_Linea_)
      INTO V_Tercero,
           V_Nombre_Tercero,
           V_Municipio,
           V_Nombre_Municipio
  DO
  BEGIN

    "Cliente_Cod" = TRIM(V_Tercero);
    "Cliente_Nombre" = TRIM(V_Nombre_Tercero) || IIF(COALESCE(V_Razon, '') = '', '', ' - ' || TRIM(V_Razon));
    "Cliente_MunCod" = TRIM(V_Municipio);
    IF (POSITION('-', V_Nombre_Municipio) > 0) THEN
      "Cliente_MunDesc" = SUBSTRING(V_Nombre_Municipio FROM 1 FOR POSITION('-' IN V_Nombre_Municipio) - 1);
    ELSE
      "Cliente_MunDesc" = TRIM(V_Nombre_Municipio);

    -- Datos Tercero
    SELECT COALESCE(T.Direccion, ''),
           COALESCE(T.Telefono, ''),
           COALESCE(T.Codtipologia, '01'),
           COALESCE(T.Barrio, ''),
           T.Razon_Comercial,
           TRIM(COALESCE(T.Codzona, ''))
    FROM Terceros T
    WHERE Codigo = :V_Tercero
    INTO V_Direccion,
         V_Telefono,
         V_Tipologia,
         V_Barrio,
         V_Razon,
         V_Cliente_Rutacod;

    "Cliente_Dir" = TRIM(V_Direccion);
    "Cliente_Tel" = TRIM(V_Telefono);
    "Cliente_CanalCod" = TRIM(V_Tipologia);
    "Cliente_Barrio" = TRIM(V_Barrio);
    "Cliente_RutaCod" = TRIM(V_Cliente_Rutacod);

    --Zonas
    SELECT Nombre
    FROM Zonas
    WHERE Codigo = :V_Cliente_Rutacod
    INTO V_Cliente_Rutadesc;
    "Cliente_RutaDesc" = TRIM(V_Cliente_Rutadesc);

    -- Tipologia
    SELECT Tp.Nombre
    FROM Tipologias Tp
    WHERE Tp.Codigo = :V_Tipologia
    INTO V_Nombre_Tipologia;
    "Cliente_CanalDesc" = TRIM(V_Nombre_Tipologia);

    -- Departamento
    V_Departamento = SUBSTRING(V_Municipio FROM 1 FOR 2);
    "Cliente_DeptoCod" = TRIM(V_Departamento);

    SELECT M.Nombre
    FROM Municipios M
    WHERE M.Codigo = :V_Departamento
    INTO V_Nombre_Departamento;

    "Cliente_DeptoDesc" = TRIM(V_Nombre_Departamento);

    -- Contactos
    SELECT FIRST 1 C.Codigo,
                   C.Nombre
    FROM Contactos C
    WHERE C.Codtercero = :V_Tercero
          AND C.Activo = 'S'
    INTO V_Contacto,
         V_Nombre_Contacto;

    "Cliente_Contacto_Cedula" = '';
    "Cliente_Contacto_Nombre" = '';

    SUSPEND;
  END

END^

CREATE OR ALTER PROCEDURE Pz_Autom_Cosmos_Inventario (
    Desde_ DATE,
    Hasta_ DATE,
    Grupo_Linea_ VARCHAR(20))
RETURNS (
    "Nit_Distribuidor" VARCHAR(15),
    "Inventario_Fecha" VARCHAR(19),
    "Prod_Dist_Cod" VARCHAR(20),
    "Prod_Dist_Desc" VARCHAR(80),
    "Prod_Prov_Cod" VARCHAR(20),
    "Prd_UnidadInventario" VARCHAR(5),
    "Cant_Disponible" DOUBLE PRECISION,
    "Cant_Stock" DOUBLE PRECISION,
    "Costo_Disponible" DOUBLE PRECISION,
    "Costo_Stock" DOUBLE PRECISION,
    "Prov_Cod" VARCHAR(15),
    "Bodega_Cod" VARCHAR(5),
    "Bodega_Desc" VARCHAR(80),
    "Agencia_Cod" VARCHAR(15),
    "Agencia_Desc" VARCHAR(50),
    "Inventario_Estado" VARCHAR(10))
AS
DECLARE VARIABLE V_Empresa VARCHAR(15);
DECLARE VARIABLE V_Razon_Comercial VARCHAR(50);
DECLARE VARIABLE V_Fecha DATE;
DECLARE VARIABLE V_Referencia VARCHAR(20);
DECLARE VARIABLE V_Nombre_Referencia VARCHAR(80);
DECLARE VARIABLE V_Referencia_Codigo2 VARCHAR(20);
DECLARE VARIABLE V_Medida VARCHAR(5);
DECLARE VARIABLE V_Saldo DOUBLE PRECISION;
DECLARE VARIABLE V_Costo DOUBLE PRECISION;
DECLARE VARIABLE V_Bodega VARCHAR(5);
DECLARE VARIABLE V_Nombre_Bodega VARCHAR(80);
DECLARE VARIABLE V_Estado VARCHAR(10);
BEGIN

  -- Tercero Empresa
  SELECT T.Codigo,
         T.Razon_Comercial
  FROM Terceros T
  WHERE T.Datos_Empresa = 'S'
  INTO V_Empresa,
       V_Razon_Comercial;

  "Nit_Distribuidor" = TRIM(V_Empresa);
  "Prov_Cod" = '890900161';
  "Agencia_Cod" = TRIM(V_Empresa);
  "Agencia_Desc" = TRIM(V_Razon_Comercial);

  -- Recorrido por Referencia
  FOR SELECT R.Fecha,
             I.Referencia,
             (I.Entradas - I.Salidas),
             I.Bodega
      FROM Fx_Inventario_Fecha(:Hasta_) I
      JOIN Fx_Dinamico(:Desde_, :Hasta_, 'VENTA', '%', '%', '%', '%', '%', '%', '%', '%', '%', '%', '%', 'N', 'N', 'N', 'S', 'S', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'S') R ON (R.Referencia = I.Referencia)
      -- WHERE R.Linea IN ('01', '02', '03', '04', '05', '06', '07', '08', '09', '10')
      WHERE Linea IN (SELECT Codigo
                      FROM Lineas
                      WHERE Grupo = :Grupo_Linea_)
      INTO V_Fecha,
           V_Referencia,
           V_Saldo,
           V_Bodega

  DO
  BEGIN

    "Inventario_Fecha" = EXTRACT(YEAR FROM V_Fecha) || '-' || LPAD(EXTRACT(MONTH FROM V_Fecha), 2, '0') || '-' || LPAD(EXTRACT(DAY FROM V_Fecha), 2, '0') || ' 00:00:00';
    "Prod_Dist_Cod" = TRIM(V_Referencia);

    -- Datos Referencia
    SELECT R.Nombre,
           R.Codigo2,
           R.Codmedida,
           R.Activa
    FROM Referencias R
    WHERE R.Codigo = :V_Referencia
    INTO V_Nombre_Referencia,
         V_Referencia_Codigo2,
         V_Medida,
         V_Estado;

    "Prod_Dist_Desc" = TRIM(V_Nombre_Referencia);
    "Prod_Prov_Cod" = TRIM(COALESCE(V_Referencia, V_Referencia_Codigo2));
    "Prd_UnidadInventario" = TRIM(V_Medida);

    "Inventario_Estado" = 'ACTIVO';
    IF (V_Estado = 'N') THEN
      "Inventario_Estado" = 'INACTIVO';

    -- Saldos
    "Cant_Disponible" = V_Saldo;
    "Cant_Stock" = V_Saldo;

    -- Costos
    V_Costo = (SELECT Ponderado
    FROM Fn_Ponderado(:V_Referencia, :Hasta_)) * :V_Saldo;

    "Costo_Disponible" = V_Costo;
    "Costo_Stock" = V_Costo;

    -- Bodega
    SELECT B.Nombre
    FROM Bodegas B
    WHERE B.Codigo = :V_Bodega
    INTO V_Nombre_Bodega;

    "Bodega_Cod" = TRIM(V_Bodega);
    "Bodega_Desc" = TRIM(V_Nombre_Bodega);

    SUSPEND;
  END

END^

CREATE OR ALTER PROCEDURE Pz_Autom_Cosmos_Ventas (
    Desde_ DATE,
    Hasta_ DATE,
    Grupo_Linea_ VARCHAR(20))
RETURNS (
    "Nit_Distribuidor" VARCHAR(15),
    "Factura_Numero" VARCHAR(15),
    "Factura_Fecha_Movimiento" VARCHAR(19),
    "Prod_Dist_Cod" VARCHAR(20),
    "Prod_Dist_Desc" VARCHAR(80),
    "Prod_Prov_Cod" VARCHAR(20),
    "Cant_Vendida" DOUBLE PRECISION,
    "Prod_UnidadVenta" VARCHAR(5),
    "Precio_Unit" DOUBLE PRECISION,
    "Precio_Total" DOUBLE PRECISION,
    "Costo" DOUBLE PRECISION,
    "Descuento" DOUBLE PRECISION,
    "Transaccion_Codigo" CHAR(2),
    "Transaccion_Descripccion" VARCHAR(10),
    "CausalTransaccion_Codigo" CHAR(1),
    "CausalTransaccion_Descripccion" CHAR(1),
    "Vend_Cedula" VARCHAR(15),
    "Vend_Nombre" VARCHAR(83),
    "Vend_Tel" VARCHAR(15),
    "Vend_Dir" VARCHAR(80),
    "Cliente_Cod" VARCHAR(15),
    "Cliente_Nombre" VARCHAR(220),
    "Cliente_Dir" VARCHAR(80),
    "Cliente_Tel" VARCHAR(50),
    "Cliente_FechaCrea" CHAR(1),
    "Cliente_FechaDesact" CHAR(1),
    "Cliente_Periodicidad" CHAR(1),
    "Cliente_RutaCod" VARCHAR(6),
    "Cliente_RutaDesc" VARCHAR(80),
    "Cliente_MunCod" VARCHAR(6),
    "Cliente_MunDesc" VARCHAR(80),
    "Sup_Cedula" CHAR(1),
    "Sup_Nombre" CHAR(1),
    "Sup_Dir" CHAR(1),
    "Sup_Tel" CHAR(1),
    "Cliente_CanalCod" VARCHAR(10),
    "Cliente_CanalDesc" VARCHAR(80),
    "Cliente_DeptoCod" VARCHAR(2),
    "Cliente_DeptoDesc" VARCHAR(80),
    "Prov_Cod" VARCHAR(15),
    "Prov_Desc" VARCHAR(163),
    "Cliente_SectorCod" CHAR(1),
    "Cliente_SectorDesc" CHAR(1),
    "Factura_Estado" CHAR(1),
    "PlanComercial_Cod" CHAR(1),
    "PlanComercial_Desc" CHAR(1),
    "Cliente_SubCanalCod" CHAR(1),
    "Cliente_SubCanalDesc" CHAR(1),
    "Agencia_Cod" VARCHAR(15),
    "Agencia_Desc" VARCHAR(50),
    "Bodega_Cod" VARCHAR(5),
    "Bodega_Desc" VARCHAR(80),
    "Cliente_GPS_Lat" CHAR(1),
    "Cliente_GPS_Long" CHAR(1),
    "Factura_GPS_Lat" CHAR(1),
    "Factura_GPS_Long" CHAR(1),
    "Cliente_Barrio" VARCHAR(20),
    "Cliente_Contacto_Cedula" CHAR(1),
    "Cliente_Contacto_Nombre" CHAR(1),
    "Ventas_FechaGeneracion" VARCHAR(19))
AS
DECLARE VARIABLE V_Emp_Nit VARCHAR(15);
DECLARE VARIABLE V_Emp_Codigo VARCHAR(15);
DECLARE VARIABLE V_Emp_Razon VARCHAR(50);
DECLARE VARIABLE V_Prefijo VARCHAR(5);
DECLARE VARIABLE V_Numero VARCHAR(10);
DECLARE VARIABLE V_Fecha DATE;
DECLARE VARIABLE V_Codigo2 VARCHAR(20);
DECLARE VARIABLE V_Salida DOUBLE PRECISION;
DECLARE VARIABLE V_Razon VARCHAR(50);
DECLARE VARIABLE V_Nombre_Ter VARCHAR(163);
DECLARE VARIABLE V_Prod_Dist_Cod VARCHAR(20);
DECLARE VARIABLE V_Prod_Dist_Desc VARCHAR(80);
DECLARE VARIABLE V_Cant_Vendida DOUBLE PRECISION;
DECLARE VARIABLE V_Precio_Total DOUBLE PRECISION;
DECLARE VARIABLE V_Descuento DOUBLE PRECISION;
DECLARE VARIABLE V_Vend_Cedula VARCHAR(15);
DECLARE VARIABLE V_Vend_Nombre VARCHAR(83);
DECLARE VARIABLE V_Cliente_Cod VARCHAR(15);
DECLARE VARIABLE V_Cliente_Muncod VARCHAR(6);
DECLARE VARIABLE V_Cliente_Mundesc VARCHAR(80);
DECLARE VARIABLE V_Bodega_Cod VARCHAR(5);
DECLARE VARIABLE V_Bodega_Desc VARCHAR(80);
DECLARE VARIABLE V_Factura_Numero VARCHAR(15);
DECLARE VARIABLE V_Prod_Unidadventa VARCHAR(5);
DECLARE VARIABLE V_Vend_Tel VARCHAR(15);
DECLARE VARIABLE V_Vend_Dir VARCHAR(80);
DECLARE VARIABLE V_Cliente_Dir VARCHAR(80);
DECLARE VARIABLE V_Cliente_Tel VARCHAR(50);
DECLARE VARIABLE V_Cliente_Rutacod VARCHAR(6);
DECLARE VARIABLE V_Cliente_Rutadesc VARCHAR(80);
DECLARE VARIABLE V_Cliente_Canalcod VARCHAR(10);
DECLARE VARIABLE V_Cliente_Canaldesc VARCHAR(80);
DECLARE VARIABLE V_Cliente_Barrio VARCHAR(20);
DECLARE VARIABLE V_Cliente_Deptocod VARCHAR(2);
DECLARE VARIABLE V_Cliente_Deptodesc VARCHAR(80);
DECLARE VARIABLE V_Prod_Prov_Cod VARCHAR(20);
DECLARE VARIABLE V_Precio_Unit DOUBLE PRECISION;
DECLARE VARIABLE V_Costo DOUBLE PRECISION;
BEGIN
  SELECT TRIM(Codigo) Codigo,
         Razon_Comercial
  FROM Terceros
  WHERE Datos_Empresa = 'S'
  INTO V_Emp_Nit,
       V_Emp_Razon;

  "Sup_Cedula" = '';
  "Sup_Nombre" = '';
  "Sup_Dir" = '';
  "Sup_Tel" = '';
  "Prov_Cod" = '890900161';
  "Cliente_SectorCod" = '';
  "Cliente_SectorDesc" = '';
  "Factura_Estado" = '0';
  "PlanComercial_Cod" = '';
  "PlanComercial_Desc" = '';
  "Cliente_SubCanalCod" = '';
  "Cliente_SubCanalDesc" = '';
  "CausalTransaccion_Codigo" = '';
  "CausalTransaccion_Descripccion" = '';
  "Cliente_FechaCrea" = '';
  "Cliente_FechaDesact" = '';
  "Cliente_Periodicidad" = '';
  "Cliente_GPS_Lat" = '';
  "Cliente_GPS_Long" = '';
  "Factura_GPS_Lat" = '';
  "Factura_GPS_Long" = '';
  "Cliente_Contacto_Cedula" = '';
  "Cliente_Contacto_Nombre" = '';
  "Nit_Distribuidor" = TRIM(V_Emp_Nit);
  "Agencia_Cod" = TRIM(V_Emp_Nit);
  "Agencia_Desc" = TRIM(V_Emp_Razon);

  FOR SELECT DISTINCT Prefijo,
                      Numero,
                      Fecha,
                      Referencia,
                      Nombre_Referencia,
                      ABS(Cantidad),
                      Bruto + ABS(Devolucion),
                      ABS(Descuento),
                      Salida,
                      Vendedor,
                      Nombre_Vendedor,
                      Tercero,
                      Nombre_Tercero,
                      Municipio,
                      Nombre_Municipio,
                      Bodega,
                      Nombre_Bodega
      FROM Fx_Dinamico(:Desde_, :Hasta_, 'VENTA', '%', '%', '%', '%', '%', '%', '%', '%', '%', '%', '%', 'S', 'N', 'N', 'S', 'S', 'N', 'N', 'S', 'S', 'S', 'N', 'N', 'N')
      --WHERE Linea IN ('01', '02', '03', '04', '05', '06', '07', '08', '09', '10')
      WHERE Linea IN (SELECT Codigo
                      FROM Lineas
                      WHERE Grupo = :Grupo_Linea_)
      INTO V_Prefijo,
           V_Numero,
           V_Fecha,
           V_Prod_Dist_Cod,
           V_Prod_Dist_Desc,
           V_Cant_Vendida,
           V_Precio_Total,
           V_Descuento,
           V_Salida,
           V_Vend_Cedula,
           V_Vend_Nombre,
           V_Cliente_Cod,
           V_Nombre_Ter,
           V_Cliente_Muncod,
           V_Cliente_Mundesc,
           V_Bodega_Cod,
           V_Bodega_Desc
  DO
  BEGIN
    IF (TRIM(V_Prefijo) = '_') THEN
      V_Factura_Numero = V_Numero;
    ELSE
      V_Factura_Numero = TRIM(V_Prefijo) || TRIM(V_Numero);

    --Referencias
    SELECT Codigo2,
           Codmedida
    FROM Referencias
    WHERE Codigo = :V_Prod_Dist_Cod
    INTO V_Codigo2,
         V_Prod_Unidadventa;

    --Personal
    SELECT COALESCE(Telefono, ''),
           COALESCE(Direccion, '')
    FROM Personal
    WHERE Codigo = :V_Vend_Cedula
    INTO V_Vend_Tel,
         V_Vend_Dir;

    --Terceros
    SELECT Razon_Comercial,
           COALESCE(Direccion, ''),
           COALESCE(Telefono, ''),
           TRIM(COALESCE(Codzona, '')),
           COALESCE(Codtipologia, ''),
           COALESCE(Barrio, '')
    FROM Terceros
    WHERE Codigo = :V_Cliente_Cod
    INTO V_Razon,
         V_Cliente_Dir,
         V_Cliente_Tel,
         V_Cliente_Rutacod,
         V_Cliente_Canalcod,
         V_Cliente_Barrio;

    --Tercero Familia
    SELECT TRIM(Nombre)
    FROM Terceros
    WHERE Codigo = :"Prov_Cod"
    INTO "Prov_Desc";

    --Zonas
    SELECT TRIM(Nombre)
    FROM Zonas
    WHERE Codigo = :V_Cliente_Rutacod
    INTO V_Cliente_Rutadesc;

    --Tipologia
    SELECT Nombre
    FROM Tipologias
    WHERE Codigo = :V_Cliente_Canalcod
    INTO V_Cliente_Canaldesc;
    V_Cliente_Canaldesc = COALESCE(V_Cliente_Canaldesc, '');

    --Departamento
    V_Cliente_Deptocod = SUBSTRING(V_Cliente_Muncod FROM 1 FOR 2);
    SELECT Nombre
    FROM Municipios
    WHERE Codigo = :V_Cliente_Deptocod
    INTO V_Cliente_Deptodesc;

    V_Cliente_Rutacod = TRIM(V_Cliente_Rutacod) || ':';

    IF (COALESCE(V_Codigo2, '') = '') THEN
      V_Prod_Prov_Cod = V_Prod_Dist_Cod;
    ELSE
      V_Prod_Prov_Cod = V_Codigo2;

    IF (V_Cant_Vendida = 0) THEN
      V_Precio_Unit = 0;
    ELSE
      V_Precio_Unit = V_Precio_Total / V_Cant_Vendida;

    SELECT Ponderado * :V_Cant_Vendida
    FROM Fn_Ponderado(:V_Prod_Dist_Cod, :Hasta_)
    INTO V_Costo;

    IF (V_Salida > 0) THEN
      "Transaccion_Codigo" = 'VT';
    ELSE
      "Transaccion_Codigo" = 'DV';

    IF (V_Salida > 0) THEN
      "Transaccion_Descripccion" = 'VENTA';
    ELSE
      "Transaccion_Descripccion" = 'DEVOLUCION';

    IF (COALESCE(V_Razon, '') = '') THEN
      "Cliente_Nombre" = TRIM(V_Nombre_Ter);
    ELSE
      "Cliente_Nombre" = TRIM(V_Nombre_Ter) || '-' || TRIM(COALESCE(V_Razon, ''));

    IF (POSITION('-', V_Cliente_Mundesc) > 0) THEN
      V_Cliente_Mundesc = SUBSTRING(V_Cliente_Mundesc FROM 1 FOR POSITION('-' IN V_Cliente_Mundesc) - 1);
    ELSE
      V_Cliente_Mundesc = TRIM(V_Cliente_Mundesc);

    "Prod_Dist_Cod" = TRIM(V_Prod_Dist_Cod);
    "Prod_Dist_Desc" = TRIM(V_Prod_Dist_Desc);
    "Cant_Vendida" = V_Cant_Vendida;
    "Precio_Total" = V_Precio_Total;
    "Descuento" = V_Descuento;
    "Vend_Cedula" = TRIM(V_Vend_Cedula);
    "Vend_Nombre" = TRIM(V_Vend_Nombre);
    "Cliente_Cod" = TRIM(V_Cliente_Cod);
    "Cliente_MunCod" = TRIM(V_Cliente_Muncod);
    "Cliente_MunDesc" = TRIM(V_Cliente_Mundesc);
    "Bodega_Cod" = TRIM(V_Bodega_Cod);
    "Bodega_Desc" = TRIM(V_Bodega_Desc);
    "Factura_Numero" = TRIM(V_Factura_Numero);
    "Factura_Fecha_Movimiento" = EXTRACT(YEAR FROM V_Fecha) || '-' || LPAD(EXTRACT(MONTH FROM V_Fecha), 2, '0') || '-' || LPAD(EXTRACT(DAY FROM V_Fecha), 2, '0') || ' 00:00:00';
    "Prod_UnidadVenta" = TRIM(V_Prod_Unidadventa);
    "Vend_Tel" = TRIM(V_Vend_Tel);
    "Vend_Dir" = TRIM(V_Vend_Dir);
    "Cliente_Dir" = TRIM(V_Cliente_Dir);
    "Cliente_Tel" = TRIM(V_Cliente_Tel);
    "Cliente_RutaCod" = TRIM(V_Cliente_Rutacod);
    "Cliente_CanalCod" = TRIM(V_Cliente_Canalcod);
    "Cliente_Barrio" = TRIM(V_Cliente_Barrio);
    "Cliente_RutaDesc" = TRIM(V_Cliente_Rutadesc);
    "Cliente_CanalDesc" = TRIM(V_Cliente_Canaldesc);
    "Cliente_DeptoCod" = TRIM(V_Cliente_Deptocod);
    "Cliente_DeptoDesc" = TRIM(V_Cliente_Deptodesc);
    "Prod_Prov_Cod" = TRIM(V_Prod_Prov_Cod);
    "Precio_Unit" = V_Precio_Unit;
    "Costo" = V_Costo;

    "Ventas_FechaGeneracion" = EXTRACT(YEAR FROM CURRENT_DATE) || '-' || LPAD(EXTRACT(MONTH FROM CURRENT_DATE), 2, '0') || '-' || LPAD(EXTRACT(DAY FROM CURRENT_DATE), 2, '0') || ' ' || SUBSTRING(CURRENT_TIME FROM 1 FOR 8);

    SUSPEND;
  END

END^

SET TERM; ^

/******************************************************************************/
/***                               Privileges                               ***/
/******************************************************************************/ 

COMMIT WORK;


/* Script para crear reporte personalizado MC740_001. */
UPDATE OR INSERT INTO REPORTES (CODIGO, NOMBRE, NOTA, ARCHIVO, ADJUNTO, NATIVO, MODULO, PARAMETROS, PUBLICAR, CODGRUPO, VALIDA_TRANSITO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD)
VALUES ('MC740_001', 'RELACION VENDEDOR - PRODUCTO X RANGO NUMERACION', NULL, 'MC740_001.FR3', NULL, 'N', 'GESTION', 'FECHA_INICIAL,FECHA_FINAL,DOCUMENTO,VENDEDOR,TERCERO,BODEGA,LINEA,REFERENCIA,NUMERO_DESDE,NUMERO_HASTA', 'N', 'PERSONALIZADOS', 'N', 'N', 'LIBRE', 5, 'N', 1)
MATCHING (CODIGO);

COMMIT WORK;

SET TERM ^ ;

CREATE OR ALTER PROCEDURE Pz_Autom_Cosmos_Clientes (
    Desde_       DATE,
    Hasta_       DATE,
    Grupo_Linea_ VARCHAR(20))
RETURNS (
    "Cliente_Cod"                     VARCHAR(15),
    "Cliente_Nombre"                  VARCHAR(163),
    "Cliente_Dir"                     VARCHAR(80),
    "Cliente_Tel"                     VARCHAR(50),
    "Cliente_FechaCrea"               VARCHAR(1),
    "Cliente_FechaDesact"             VARCHAR(1),
    "Cliente_Periodicidad"            VARCHAR(1),
    "Cliente_MunCod"                  VARCHAR(5),
    "Cliente_MunDesc"                 VARCHAR(80),
    "Cliente_DeptoCod"                VARCHAR(2),
    "Cliente_DeptoDesc"               VARCHAR(80),
    "Cliente_CanalCod"                VARCHAR(10),
    "Cliente_CanalDesc"               VARCHAR(80),
    "Cliente_SubCanalCod"             VARCHAR(1),
    "Cliente_SubCanalDesc"            VARCHAR(1),
    "Cliente_GPS_Lat"                 VARCHAR(1),
    "Cliente_GPS_Long"                VARCHAR(1),
    "Cliente_Barrio"                  VARCHAR(20),
    "Cliente_Contacto_Cedula"         VARCHAR(10),
    "Cliente_Contacto_Nombre"         VARCHAR(80),
    "Cliente_SectorCod"               VARCHAR(1),
    "Cliente_SectorDesc"              VARCHAR(1),
    "Cliente_RutaCod"                 VARCHAR(6),
    "Cliente_RutaDesc"                VARCHAR(80),
    "PlanComercial_Cod"               VARCHAR(1),
    "PlanComercial_Desc"              VARCHAR(1),
    "MaestrasClientes_FechaGeneracio" VARCHAR(19))
AS
DECLARE VARIABLE V_Tercero             VARCHAR(15);
DECLARE VARIABLE V_Nombre_Tercero      VARCHAR(163);
DECLARE VARIABLE V_Direccion           VARCHAR(80);
DECLARE VARIABLE V_Telefono            VARCHAR(50);
DECLARE VARIABLE V_Municipio           VARCHAR(5);
DECLARE VARIABLE V_Nombre_Municipio    VARCHAR(80);
DECLARE VARIABLE V_Departamento        VARCHAR(2);
DECLARE VARIABLE V_Nombre_Departamento VARCHAR(80);
DECLARE VARIABLE V_Tipologia           VARCHAR(10);
DECLARE VARIABLE V_Nombre_Tipologia    VARCHAR(80);
DECLARE VARIABLE V_Barrio              VARCHAR(20);
DECLARE VARIABLE V_Contacto            VARCHAR(10);
DECLARE VARIABLE V_Nombre_Contacto     VARCHAR(80);
DECLARE VARIABLE V_Razon               VARCHAR(50);
DECLARE VARIABLE V_Cliente_Rutacod     VARCHAR(6);
DECLARE VARIABLE V_Cliente_Rutadesc    VARCHAR(80);
BEGIN

  "Cliente_FechaCrea" = '';
  "Cliente_FechaDesact" = '';
  "Cliente_Periodicidad" = '';
  "Cliente_SubCanalCod" = '';
  "Cliente_SubCanalDesc" = '';
  "Cliente_GPS_Lat" = '';
  "Cliente_GPS_Long" = '';
  "Cliente_SectorCod" = '';
  "Cliente_SectorDesc" = '';
  "PlanComercial_Cod" = '';
  "PlanComercial_Desc" = '';
  "MaestrasClientes_FechaGeneracio" = EXTRACT(YEAR FROM CURRENT_DATE) || '-' || LPAD(EXTRACT(MONTH FROM CURRENT_DATE), 2, '0') || '-' || LPAD(EXTRACT(DAY FROM CURRENT_DATE), 2, '0') || ' ' || SUBSTRING(CURRENT_TIME FROM 1 FOR 8);

  FOR SELECT DISTINCT D.Tercero,
                      D.Nombre_Tercero,
                      D.Municipio,
                      D.Nombre_Municipio
      FROM Fx_Dinamico(:Desde_, :Hasta_, 'VENTA', '%', '%', '%', '%', '%', '%', '%', '%', '%', '%', '%', 'S', 'N', 'N', 'S', 'S', 'N', 'N', 'S', 'S', 'S', 'N', 'N', 'N') D
      --WHERE Linea IN ('01', '02', '03', '04', '05', '06', '07', '08', '09', '10')
      WHERE Linea IN (SELECT Codigo
                      FROM Lineas
                      WHERE Grupo = :Grupo_Linea_)
      INTO V_Tercero,
           V_Nombre_Tercero,
           V_Municipio,
           V_Nombre_Municipio
  DO
  BEGIN

    "Cliente_Cod" = TRIM(V_Tercero);
    "Cliente_Nombre" = TRIM(V_Nombre_Tercero) || IIF(COALESCE(V_Razon, '') = '', '', ' - ' || TRIM(V_Razon));
    "Cliente_MunCod" = TRIM(V_Municipio);
    IF (POSITION('-', V_Nombre_Municipio) > 0) THEN
      "Cliente_MunDesc" = SUBSTRING(V_Nombre_Municipio FROM 1 FOR POSITION('-' IN V_Nombre_Municipio) - 1);
    ELSE
      "Cliente_MunDesc" = TRIM(V_Nombre_Municipio);

    -- Datos Tercero
    SELECT COALESCE(T.Direccion, ''),
           COALESCE(T.Telefono, ''),
           COALESCE(T.Codtipologia, '01'),
           COALESCE(T.Barrio, ''),
           T.Razon_Comercial,
           TRIM(COALESCE(T.Codzona, ''))
    FROM Terceros T
    WHERE Codigo = :V_Tercero
    INTO V_Direccion,
         V_Telefono,
         V_Tipologia,
         V_Barrio,
         V_Razon,
         V_Cliente_Rutacod;

    "Cliente_Dir" = TRIM(V_Direccion);
    "Cliente_Tel" = TRIM(V_Telefono);
    "Cliente_CanalCod" = TRIM(V_Tipologia);
    "Cliente_Barrio" = TRIM(V_Barrio);
    "Cliente_RutaCod" = TRIM(V_Cliente_Rutacod);

    --Zonas
    SELECT Nombre
    FROM Zonas
    WHERE Codigo = :V_Cliente_Rutacod
    INTO V_Cliente_Rutadesc;
    "Cliente_RutaDesc" = TRIM(V_Cliente_Rutadesc);

    -- Tipologia
    SELECT Tp.Nombre
    FROM Tipologias Tp
    WHERE Tp.Codigo = :V_Tipologia
    INTO V_Nombre_Tipologia;
    "Cliente_CanalDesc" = TRIM(V_Nombre_Tipologia);

    -- Departamento
    V_Departamento = SUBSTRING(V_Municipio FROM 1 FOR 2);
    "Cliente_DeptoCod" = TRIM(V_Departamento);

    SELECT M.Nombre
    FROM Municipios M
    WHERE M.Codigo = :V_Departamento
    INTO V_Nombre_Departamento;

    "Cliente_DeptoDesc" = TRIM(V_Nombre_Departamento);

    -- Contactos
    SELECT FIRST 1 C.Codigo,
                   C.Nombre
    FROM Contactos C
    WHERE C.Codtercero = :V_Tercero
          AND C.Activo = 'S'
    INTO V_Contacto,
         V_Nombre_Contacto;

    "Cliente_Contacto_Cedula" = '';
    "Cliente_Contacto_Nombre" = '';

    SUSPEND;
  END

END^

SET TERM ; ^