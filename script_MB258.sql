/* APIs en interface rest */
UPDATE OR INSERT INTO INTERFACE_REST (CLAVE, CONSULTA, METHOD_HTTP, DESCRIPCION, REGISTRO)
                              VALUES ('Set_Terceros', 'EXECUTE PROCEDURE Rest_Insert_Terceros(:Codigo, :Dv, :Naturaleza, :Nom1, :Nom2, :Apl1, :Apl2, :Empresa, :Razon_Comercial, :Direccion, :Telefono, :Movil, :Email, :Codigo_Postal, :Gerente, :Codidentidad, :Codsociedad, :Codpais, :Codactividad, :Codzona, :Codmunicipio)', 'POST', 'Inserta un nuevo Tercero', 6041199247570830422)
                            MATCHING (CLAVE);
UPDATE OR INSERT INTO INTERFACE_REST (CLAVE, CONSULTA, METHOD_HTTP, DESCRIPCION, REGISTRO)
                              VALUES ('Set_Gestion_Primario', 'EXECUTE PROCEDURE Rest_Insert_Gestion_Primario (:Tipo,:Prefijo,:Numero,:Fecha,:Vence,:Tercero,:Vendedor,:Lista,:Banco,:Usuario,:Centro,:Bodega,:Referencia,:Entrada,:Salida,:Unitario,:Porc_descuento,:Nota)', 'POST', 'Inserta un nuevo documento primario en GestiÃ³n', -1595035324359283302)
                            MATCHING (CLAVE);
UPDATE OR INSERT INTO INTERFACE_REST (CLAVE, CONSULTA, METHOD_HTTP, DESCRIPCION, REGISTRO)
                              VALUES ('Get_Datos_FE', 'SELECT Cufe, Qrdata, Validacion FROM Rest_Select_Datos_Fe(:Tipo, :Prefijo, :Numero)', 'POST', 'Consultar datos de tabla Facturas', 858648167935310148)
                            MATCHING (CLAVE);
UPDATE OR INSERT INTO INTERFACE_REST (CLAVE, CONSULTA, METHOD_HTTP, DESCRIPCION, REGISTRO)
                              VALUES ('Set_Envio_Dian', 'EXECUTE PROCEDURE Nuevo_Comprobante(:Tipo, :Prefijo, :Numero)', 'POST', 'Envio inmediato del comprobante', 650294408025152171)
                            MATCHING (CLAVE);
UPDATE OR INSERT INTO INTERFACE_REST (CLAVE, CONSULTA, METHOD_HTTP, DESCRIPCION, REGISTRO)
                              VALUES ('Get_Terceros', 'SELECT Codigo, Naturaleza, Codigo2, Dv, Empresa, Nom1, Nom2, Apl1, Apl2, Razon_Comercial, Codigo_Postal, Barrio, Direccion, Envio, Telefono, Movil, Email, Gerente, Website, Fecha, Habeas_Data, Codsociedad, Codidentidad, Codmunicipio, Codactividad, Codzona, Codlista, Codpersonal, Codpais, Codlista_Compra, Codtipologia, Nombre FROM Rest_Select_Terceros(:Codigo)', 'POST', 'Obtiene informacion del Tercero', 313851328775202758)
                            MATCHING (CLAVE); 
 UPDATE OR INSERT INTO INTERFACE_REST (CLAVE, CONSULTA, METHOD_HTTP, DESCRIPCION, REGISTRO)
                              VALUES ('Set_Gestion_Primario_B', 'EXECUTE PROCEDURE Rest_insert_Gestion_Primario_G(:Tipo,:Prefijo,:Numero,:Fecha,:Vence,:Tercero,:Vendedor,:Lista,:Banco,:Usuario,:Detalle,:Bloqueado,:Enviado)', 'POST', 'Inserta un nuevo documento primario en Gestion', 3953423711895736951)
                            MATCHING (CLAVE);                           

COMMIT WORK;