/* Libro Diario Oficial */
UPDATE OR INSERT INTO REPORTES (CODIGO, NOMBRE, NOTA, ARCHIVO, ADJUNTO, NATIVO, MODULO, PARAMETROS, PUBLICAR, CODGRUPO, VALIDA_TRANSITO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD)
VALUES ('MB185_001', 'LIBRO DIARIO OFICIAL', 'Permite imprimir el informe de Libro Diario Oficial, con sus respectivos debitos y creditos. En caso de querer consultar nivel auxiliar digitar 30.', 'MB185_001.FR3', NULL, 'N', 'CONTABLE', 'FECHA_INICIAL,FECHA_FINAL,NODO', 'N', 'CAJA DIARIO', 'N', 'N', 'LIBRE', 5, 'N', 1)
MATCHING (CODIGO);

COMMIT WORK;