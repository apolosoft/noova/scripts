UPDATE OR INSERT INTO AUTOMATOR (CODIGO, NOMBRE, MODO, SQL_CONSULTA, ACTIVA, DELIMITADOR, CODDESTINO)
                         VALUES ('002', 'INFORME DE VENTAS CON SKU', 'EMAIL', 'SELECT DISTINCT P.Tipo,
                LPAD(TRIM(P.Numero), 10) AS Numero,
                P.Fecha,
                TRIM(P.Referencia) Referencia,
                TRIM(R.Codigo2) Sku,
                (P.Cantidad) AS Cantidad,
                ((P.Cantidad * P.Bruto) + COALESCE(P.Gravado, 0) + COALESCE(P.No_Gravado, 0) - COALESCE(P.Retencion, 0) - (P.Cantidad * P.Descuento)) AS Totales,
                TRIM(T.Codigo) Tercero,
                IIF(TRIM(COALESCE(T.Nom1, '''')) = '''', TRIM(COALESCE(T.Empresa, '''')), TRIM(COALESCE(T.Nom1, '''')) || '' '' || TRIM(COALESCE(T.Nom2, ''''))) Nombre_Tercero,
                IIF(TRIM(COALESCE(T.Apl1, '''')) = '''', '''', TRIM(COALESCE(T.Apl1, '''')) || '' '' || TRIM(COALESCE(T.Apl2, ''''))) Apellido_Tercero,
                TRIM(T.Email) Email,
                T.Ecommerce Casilla
FROM Fx_Auxiliar_Pos((:hoy - 365), :hoy) P
JOIN Documentos D ON (P.Tipo = D.Codigo)
JOIN Referencias R ON (R.Codigo = P.Referencia)
JOIN Terceros T ON (T.Codigo = P.Tercero)
WHERE D.Grupo = ''VENTA''
      AND D.Es_Compra_Venta = ''S''
      AND D.Signo = ''CREDITO''

UNION ALL

SELECT DISTINCT P.Tipo,
                LPAD(TRIM(P.Numero), 10) AS Numero,
                P.Fecha,
                TRIM(P.Referencia) Referencia,
                TRIM(R.Codigo2) Sku,
                (P.Cantidad) AS Cantidad,
                ((P.Cantidad * P.Bruto) + COALESCE(P.Gravado, 0) + COALESCE(P.No_Gravado, 0) - COALESCE(P.Retencion, 0) - (P.Cantidad * P.Descuento)) AS Totales,
                TRIM(T.Codigo) Tercero,
                IIF(TRIM(COALESCE(T.Nom1, '''')) = '''', TRIM(COALESCE(T.Empresa, '''')), TRIM(COALESCE(T.Nom1, '''')) || '' '' || TRIM(COALESCE(T.Nom2, ''''))) Nombre_Tercero,
                IIF(TRIM(COALESCE(T.Apl1, '''')) = '''', '''', TRIM(COALESCE(T.Apl1, '''')) || '' '' || TRIM(COALESCE(T.Apl2, ''''))) Apellido_Tercero,
                TRIM(T.Email) Email,
                T.Ecommerce Casilla
FROM Fx_Auxiliar_Pos((:hoy - 365), :hoy) P
JOIN Documentos D ON (P.Tipo = D.Codigo)
JOIN Referencias R ON (R.Codigo = P.Referencia)
JOIN Terceros T ON (T.Codigo = P.Tercero)
WHERE D.Grupo = ''VENTA''
      AND D.Es_Compra_Venta = ''S''
      AND D.Signo = ''DEBITO''', 'S', ',', 'VENTAS')
                       MATCHING (CODIGO);


COMMIT WORK;

UPDATE OR INSERT INTO INFORMES (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, FORMATO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD, DESTINO_FTP, CODREPORTE)
                        VALUES ('MA378_001', 'PERSONALIZADO RELACION DETALLADA DE VENTAS MENOS DEVOLUCIONES', 'Relación detallada de ventas en un rango de fecha dado.', 'SELECT * FROM PZ_INFO_VTAS_DEV(:Fecha_Desde, :Fecha_Hasta)', 'N', 'GESTION', :h00000000_00000000, 'N', 'SALDOS EN VENTAS', 'N', 'LIBRE', 5, 'N', 1, NULL, NULL)
                      MATCHING (CODIGO);


COMMIT WORK;

UPDATE OR INSERT INTO INFORMES (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD, DESTINO_FTP, CODREPORTE)
                        VALUES ('MA378_002', 'INFORME PERSONALIZADO STOCK', 'INFORME PERSONALIZADO QUE MUESTRA LA INFORMACION DEL STOCK', 'SELECT R1.CODIGO as CODIGO, R1.CODIGO2, R1.NOMBRE as NOMBRE, I.BODEGA as BODEGA,
       (I.ENTRADAS - I.SALIDAS) as EXISTENCIA, R1.NOMBRE2, R1.UBICACION, R1.COSTO, R1.PRECIO, R1.RENTABILIDAD, G.codigo AS "COD_GENERO", G.nombre AS "NOM_GENERO", R1.CODMARCA,
       M.NOMBRE as NOM_MARCA
FROM FX_INVENTARIO I
JOIN REFERENCIAS R1 on (R1.CODIGO = I.REFERENCIA)
LEFT JOIN MARCAS M on (R1.codmarca = M.CODIGO)
LEFT JOIN GENEROS G ON (R1.codgenero = G.codigo)', 'N', 'GESTION', 'N', 'SALDOS EN VENTAS', 'N', 'LIBRE', 5, 'N', 1, NULL, NULL)
                      MATCHING (CODIGO);
UPDATE OR INSERT INTO INFORMES (CODIGO, NOMBRE, NOTA, SQL_CONSULTA, NATIVO, MODULO, PUBLICAR, CODGRUPO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD, DESTINO_FTP, CODREPORTE)
                        VALUES ('MA378_003', 'INFORME PERSONALIZADO KARDEX', 'INFORME PERSONALIZADO QUE MUESTRA LA INFORMACION DE KARDEX', 'SELECT
P.tipo,P.prefijo,P.numero,P.fecha,P.renglon,P.codreferencia,P.codcentro,P.codbodega,P.entradas,P.salidas,P.bruto,
P.descuento,P.ponderado,P.desde,P.hasta, R1.ubicacion, R1.codigo2 as "SKU", R1.nombre2 AS NOMBRE2,R1.costo,R1.precio,R1.rentabilidad,
G.codigo AS "COD_GENERO", G.nombre AS "NOM_GENERO",
R1.codmarca, M.nombre AS NOM_MARCA

FROM FX_PONDERADO_RANGO(:FECHA_DESDE,:FECHA_HASTA)P
inner join REFERENCIAS R1 on (P.codreferencia = R1.codigo)
LEFT JOIN MARCAS M ON (R1.codmarca = M.codigo)
LEFT JOIN GENEROS G ON (R1.codgenero = G.codigo)
where R1.activa = ''S'' AND
      (coalesce(trim(P.codreferencia), '''') like :REFERENCIA)', 'N', 'GESTION', 'N', 'SALDOS EN VENTAS', 'N', 'LIBRE', 5, 'N', 1, NULL, NULL)
                      MATCHING (CODIGO);


COMMIT WORK;

SELECT R1.Codigo AS Codigo,
       R1.Codigo2,
       R1.Nombre AS Nombre,
       I.Bodega AS Bodega,
       (I.Entradas - I.Salidas) AS Existencia,
       R1.Nombre2,
       R1.Ubicacion,
       R1.Costo,
       R1.Precio,
       R1.Rentabilidad,
       G.Codigo AS "COD_GENERO",
       G.Nombre AS "NOM_GENERO",
       R1.Codmarca,
       M.Nombre AS Nom_Marca
FROM Fx_Inventario I
JOIN Referencias R1 ON (R1.Codigo = I.Referencia)
LEFT JOIN Marcas M ON (R1.Codmarca = M.Codigo)
LEFT JOIN Generos G ON (R1.Codgenero = G.Codigo)
WHERE R1.Activa = 'S' ;

COMMIT WORK;

SET TERM ^ ;

CREATE OR ALTER PROCEDURE Pz_Info_Vtas_Dev (
    Fecha_Desde DATE,
    Fecha_Hasta DATE)
RETURNS (
    Clase             VARCHAR(15),
    Cantidad          DOUBLE PRECISION,
    Tipo              VARCHAR(5),
    Prefijo           VARCHAR(5),
    Numero            VARCHAR(10),
    Renglon           INTEGER,
    Bruto             DOUBLE PRECISION,
    Unitario          DOUBLE PRECISION,
    Descuento         DOUBLE PRECISION,
    Gravado           DOUBLE PRECISION,
    No_Gravado        DOUBLE PRECISION,
    Retencion         DOUBLE PRECISION,
    Gran_Total        DOUBLE PRECISION,
    Fecha             DATE,
    Referencia        VARCHAR(20),
    Linea             VARCHAR(5),
    Tercero           VARCHAR(15),
    Usuario           VARCHAR(10),
    Computador        VARCHAR(20),
    Nombre_Referencia VARCHAR(80),
    Nombre_Linea      VARCHAR(80),
    Nombre_Tercero    VARCHAR(164),
    Nombre_Usuario    VARCHAR(80),
    Desde             DATE,
    Hasta             DATE,
    Totales           DOUBLE PRECISION,
    Codigo2           VARCHAR(20),
    Nombre2           VARCHAR(20),
    Ubicacion         VARCHAR(20),
    Costo             DOUBLE PRECISION,
    Precio            DOUBLE PRECISION,
    Rentabilidad      DOUBLE PRECISION,
    Codmarca          VARCHAR(20),
    Genero            VARCHAR(10),
    Nom_Marca         VARCHAR(80),
    Cod_Genero        VARCHAR(10),
    Nom_Genero        VARCHAR(80))
AS
DECLARE VARIABLE V_Signo VARCHAR(10);
BEGIN

  FOR SELECT P.Cantidad,
             P.Tipo,
             P.Prefijo,
             LPAD(TRIM(P.Numero), 10),
             P.Renglon,
             P.Bruto,
             P.Unitario,
             P.Descuento,
             P.Gravado,
             P.No_Gravado,
             P.Retencion,
             P.Fecha,
             P.Referencia,
             P.Linea,
             P.Tercero,
             P.Usuario,
             P.Computador,
             P.Nombre_Referencia,
             P.Nombre_Linea,
             P.Nombre_Tercero,
             P.Nombre_Usuario,
             P.Desde,
             P.Hasta,
             D.Signo

      FROM Fx_Auxiliar_Pos(:Fecha_Desde, :Fecha_Hasta) P
      JOIN Documentos D ON (P.Tipo = D.Codigo)
      WHERE D.Grupo = 'VENTA'
            AND D.Es_Compra_Venta = 'S'

      INTO Cantidad,
           Tipo,
           Prefijo,
           Numero,
           Renglon,
           Bruto,
           Unitario,
           Descuento,
           Gravado,
           No_Gravado,
           Retencion,
           Fecha,
           Referencia,
           Linea,
           Tercero,
           Usuario,
           Computador,
           Nombre_Referencia,
           Nombre_Linea,
           Nombre_Tercero,
           Nombre_Usuario,
           Desde,
           Hasta,
           V_Signo

  DO
  BEGIN

    IF (V_Signo = 'CREDITO') THEN
    BEGIN
      Clase = '+VENTAS';
      Totales = ((Cantidad * Bruto) + COALESCE(Gravado, 0) + COALESCE(No_Gravado, 0) - COALESCE(Retencion, 0) - (Cantidad * Descuento));
      Gran_Total = ((Bruto - Descuento) * Cantidad);
    END
    ELSE
    BEGIN
      IF (V_Signo = 'DEBITO') THEN
        Clase = '-DEVOLUCION';
      Totales = (((Cantidad * Bruto) + COALESCE(Gravado, 0) + COALESCE(No_Gravado, 0) - COALESCE(Retencion, 0) - (Cantidad * Descuento)));
      Gran_Total = (((Bruto - Descuento) * Cantidad) * -1);
      Cantidad = Cantidad * -1;
    END
    --Buscar campos en referencias
    SELECT Codigo2,
           Nombre2,
           Ubicacion,
           Costo,
           Precio,
           Rentabilidad,
           Codmarca,
           Codgenero
    FROM Referencias
    WHERE Codigo = :Referencia
    INTO Codigo2,
         Nombre2,
         Ubicacion,
         Costo,
         Precio,
         Rentabilidad,
         Codmarca,
         Genero;

    --Buscar Nombre_Marca
    SELECT Nombre
    FROM Marcas
    WHERE Codigo = :Codmarca
    INTO Nom_Marca;

    --Buscar Campos Generos
    SELECT Codigo,
           Nombre
    FROM Generos
    WHERE Codigo = :Genero
    INTO Cod_Genero,
         Nom_Genero;

    SUSPEND;
  END
END^

SET TERM ; ^

COMMIT WORK;


