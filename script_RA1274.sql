-- Comprobate Z por esquemas
UPDATE OR INSERT INTO REPORTES (CODIGO, NOMBRE, NOTA, ARCHIVO, ADJUNTO, NATIVO, MODULO, PARAMETROS, PUBLICAR, CODGRUPO, VALIDA_TRANSITO, RANGO, CATEGORIA, NIVEL, TABLERO, PRIORIDAD)
                        VALUES ('RA1274_001', 'INFORME DIARIO FISCAL DE VENTAS', '', 'RA1274_001.FR3', '', 'N', 'GESTION', 'FECHA', 'N', 'COMPROBANTE Z', 'N', 'N', 'LIBRE', 5, 'N', 1)
                      MATCHING (CODIGO);


COMMIT WORK;

-- Proceso para actualizar precios a otras BD
EXECUTE BLOCK
AS
DECLARE VARIABLE V_Tercero CHAR(15);
BEGIN
  SELECT Codigo
  FROM Terceros
  WHERE Datos_Empresa = 'S'
  INTO V_Tercero;
  IF (V_Tercero = '901612754') THEN
  BEGIN
    UPDATE OR INSERT INTO Procesos (Codigo, Nombre, Nota, Sql_Consulta, Nativo,
                                    Modulo, Codgrupo, Nivel, Tablero, Prioridad)
    VALUES ('RA1274_002', 'ACTUALIZA PRECIOS A EMPRESAS ADRIANA y SASPIEDRUNO', 'Actualiza precios de la empresa PIEDRSAS a las empresas ADRIANA  y SASPIEDRUNO
Las referencias deben existir en las otras empresas', 'EXECUTE BLOCK (
    Fecha_Desde DATE = :Fecha_Desde)
AS
DECLARE VARIABLE V_Ref   CHAR(30);
DECLARE VARIABLE V_Price NUMERIC(17,4);
DECLARE VARIABLE V_Ruta1 CHAR(100);
DECLARE VARIABLE V_Ruta2 CHAR(100);
DECLARE VARIABLE V_Sql   CHAR(200);

BEGIN
  V_Ruta1 = ''C:\Apolosoft\MEKANO_SERVER\Empresas\ADRIANA.FDB'';
  V_Ruta2 = ''C:\Apolosoft\MEKANO_SERVER\Empresas\SASPIEDRUNO.FDB'';

  -- Referencias modificadas en primera BD
  FOR SELECT Referencia
      FROM Log_Referencias
      WHERE Fecha >= :Fecha_Desde
      INTO V_Ref
  DO
  BEGIN
    -- Precio a modificar en la otra BD
    V_Sql = ''SELECT Precio FROM Referencias  WHERE Codigo = TRIM('''''' || V_Ref || '''''')'';
    EXECUTE STATEMENT(V_Sql)
        INTO V_Price;

    V_Sql = ''UPDATE Referencias SET Precio = '' || V_Price || '' WHERE Codigo = TRIM('''''' || V_Ref || '''''')'';

    -- Segunda BD
    EXECUTE STATEMENT V_Sql
        ON EXTERNAL V_Ruta1
        AS USER ''CONSULTA'' PASSWORD ''CONSULTA'';

    -- Tercera BD
    EXECUTE STATEMENT V_Sql
        ON EXTERNAL V_Ruta2
        AS USER ''CONSULTA'' PASSWORD ''CONSULTA'';
  END

END', 'N', 'GESTION', 'PERSONALIZADOS', 5, 'N', 1)
    MATCHING (Codigo);
  END
END;

COMMIT WORK;

